/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestExptSetisCompanyMangerTriggerAccount {
    static testmethod void PerformTest() {
        Test.startTest();
        
        Account companyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        individualAccount.Organizational_Function_Expertise__pc = 'Operations;Strategy';
        
        update individualAccount;
        
        NU__Affiliation__c primaryAffil = NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, companyAccount.Id, true);
        
        primaryAffil.NU__Status__c = Constant.AFFILIATION_STATUS_ACTIVE;
        update primaryAffil;
        
        NU__Affiliation__c queriedAffil = [Select id, NU__Status__c, NU__IsPrimary__c, NU__IsCompanyManager__c from NU__Affiliation__c where id = :primaryAffil.Id];
        
        System.assertEquals(false, queriedAffil.NU__IsCompanyManager__c);
        
        individualAccount.Organizational_Function_Expertise__pc =individualAccount.Organizational_Function_Expertise__pc + '; PR/Communications';
        
        update individualAccount;
        
        queriedAffil = [Select id, NU__Status__c, NU__IsPrimary__c, NU__IsCompanyManager__c from NU__Affiliation__c where id = :primaryAffil.Id];
        
        System.assertEquals(true, queriedAffil.NU__IsCompanyManager__c);
        Test.stopTest();
        }
        
        /*
        static testmethod void PerformRevTest() {
        Test.startTest();
        
        Account companyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        individualAccount.Organizational_Function_Expertise__pc = 'PR/Communications';
        
        update individualAccount;
        
        NU__Affiliation__c primaryAffil = NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, companyAccount.Id, true);
        
        primaryAffil.NU__Status__c = Constant.AFFILIATION_STATUS_ACTIVE;
        update primaryAffil;
        
        NU__Affiliation__c queriedAffil = [Select id, NU__Status__c, NU__IsPrimary__c, NU__IsCompanyManager__c from NU__Affiliation__c where id = :primaryAffil.Id];
        
        System.assertEquals(true, queriedAffil.NU__IsCompanyManager__c);
        
        individualAccount.Organizational_Function_Expertise__pc = 'Operations;Strategy ';
        
        update individualAccount;
        
        queriedAffil = [Select id, NU__Status__c, NU__IsPrimary__c, NU__IsCompanyManager__c from NU__Affiliation__c where id = :primaryAffil.Id];
        
        System.assertEquals(false, queriedAffil.NU__IsCompanyManager__c);
        Test.stopTest();
        }
        */
        
        static testmethod void PerformOtherTest() {
        Test.startTest();
        
        Account companyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        individualAccount.Organizational_Function_Expertise__pc = 'PR/Communications';
        
        update individualAccount;
        
        NU__Affiliation__c primaryAffil = NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, companyAccount.Id, true);
        
        primaryAffil.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
        update primaryAffil;
        
        NU__Affiliation__c queriedAffil = [Select id, NU__Status__c, NU__IsPrimary__c, NU__IsCompanyManager__c from NU__Affiliation__c where id = :primaryAffil.Id];
        
        System.assertEquals(true, queriedAffil.NU__IsCompanyManager__c);
        
        primaryAffil.NU__Status__c = Constant.AFFILIATION_STATUS_ACTIVE;
        update primaryAffil;
        
        
        queriedAffil = [Select id, NU__Status__c, NU__IsPrimary__c, NU__IsCompanyManager__c from NU__Affiliation__c where id = :primaryAffil.Id];
        
        System.assertEquals(true, queriedAffil.NU__IsCompanyManager__c);
        Test.stopTest();
        }
}