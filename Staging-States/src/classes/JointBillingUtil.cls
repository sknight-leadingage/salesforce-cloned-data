public class JointBillingUtil {
    public static Boolean ProviderJoinProrated = false;
    
    public static Date ProviderJoinOnToUseForTesting = null;
    
    public static void populateCustomPricingManagerCaches(List<Account> statePartnerPrimaryAffiliationAccounts, Id jointStateProviderMembershipTypeId, Id jointStateProviderProductId){
        populateCustomPricingManagerAccountAndMembershipCaches(statePartnerPrimaryAffiliationAccounts);
        CustomPricingManager.PopulateMembershipTypes(new Set<Id>{ jointStateProviderMembershipTypeId });
        CustomPricingManager.PopulateProducts(new Set<Id>{ jointStateProviderProductId });
    }
    
    public static void populateCustomPricingManagerAccountAndMembershipCaches(List<Account> accounts){
        CustomPricingManager.PopulateAccounts(accounts);
        CustomPricingManager.PopulateAccountMemberships(accounts);
    }
    
    public static Map<Id, Decimal> getProviderPricing(Date transactionDate, Id accountId, Id jointStateProviderProductId, Id jointStateProviderMembershipTypeId, Date membershipStartDate, Date membershipEndDate, Date providerJoinDate){
    	return getProviderPricing(transactionDate, accountId, jointStateProviderProductId, jointStateProviderMembershipTypeId, membershipStartDate, membershipEndDate, providerJoinDate, false);
    }
    
    public static Map<Id, Decimal> getProviderPricing(Date transactionDate, Id accountId, Id jointStateProviderProductId, Id jointStateProviderMembershipTypeId, Date membershipStartDate, Date membershipEndDate, Date providerJoinDate, boolean IsFutureCalc){
        // populate ProductPricingInfos
        List<NU.ProductPricingInfo> productPricingInfos = new List<NU.ProductPricingInfo>();        
        
        NU.ProductPricingInfo ppi = new NU.ProductPricingInfo();
        ppi.ProductId = jointStateProviderProductId;
        ppi.Quantity = 1;
        ppi.StartDate = membershipStartDate;
        ppi.EndDate = membershipEndDate;
        ppi.JoinDate = providerJoinDate;
        productPricingInfos.add(ppi);
        
        // create PriceClassRequest                   
        NU.PriceClassRequest pcr = new NU.PriceClassRequest();
        pcr.TransactionDate = transactionDate;
        pcr.AccountId = accountId;
        
        CustomPricingManager cpm = new CustomPricingManager();
        
        // create ProductPricingRequest
        NU.ProductPricingRequest ppr = new NU.ProductPricingRequest();
        ppr.AccountId = accountId;
        ppr.TransactionDate = pcr.TransactionDate;
        ppr.ProductPricingInfos = productPricingInfos;
        ppr.PriceClassName = cpm.getPriceClass(pcr);
        ppr.MembershipTypeId = jointStateProviderMembershipTypeId;
        
        // get product prices    
        return cpm.GetProductPrices(ppr, IsFutureCalc);
    }
    
    public static Id getJointStateProviderProductId(NU__MembershipType__c jointStateProviderMembershipType){
        return jointStateProviderMembershipType.NU__MembershipTypeProductLinks__r[0].NU__Product__c;
    }
    
    public static Joint_Billing_Item__c createJointBillingItem(Account acct, Joint_Billing__c jb){
        return createJointBillingItem(acct.Id, jb.Id);
    }
    
    public static Joint_Billing_Item__c createJointBillingItem(Id providerAccountId, Id jointBillingId){
        return new Joint_Billing_Item__c(
            Account__c = providerAccountId,
            Joint_Billing__c = jointBillingId
        );
    }
    
    public static Joint_Billing_Item_Line__c createJointBillingItemLine(Decimal initialDuesPrice, Date lineDate, String newOrAdjustmentType){
        return new Joint_Billing_Item_Line__c(
            Amount__c = initialDuesPrice,
            Date__c = lineDate,
            Type__c = newOrAdjustmentType,
            Category__c = ProviderJoinProrated == true ? Constant.JOINT_BILLING_ITEM_LINE_CATEGORY_2Y1 : Constant.JOINT_BILLING_ITEM_LINE_CATEGORY_REG
        );
    }
    
    public static Joint_Billing_Item_Line__c createJointBillingItemLine(Decimal initialDuesPrice, Date lineDate, String newOrAdjustmentType, Account providerOrMsoAccount){
        Joint_Billing_Item_Line__c jbil = createJointBillingItemLine(initialDuesPrice, lineDate, newOrAdjustmentType);
        
        system.debug('    createJointBillingItemLine::providerOrMsoAccount ' + providerOrMsoAccount);
        
        if (providerOrMsoAccount.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID){
            system.debug('    createJointBillingItemLine::provider ' + providerOrMsoAccount);
            
            jbil.Dues_Price__c = providerOrMsoAccount.Dues_Price__c;
            jbil.Program_Service_Revenue__c = providerOrMsoAccount.Program_Service_Revenue__c;
            
        }
        else{
            system.debug('    createJointBillingItemLine::MSO ' + providerOrMsoAccount);
            
            jbil.Dues_Price__c = providerOrMsoAccount.Multi_Site_Dues_Price__c;
            jbil.Program_Service_Revenue__c = providerOrMsoAccount.Multi_Site_Program_Service_Revenue__c;
        }
        
        jbil.Revenue_Year_Submitted__c = providerOrMsoAccount.Revenue_Year_Submitted__c;
        
        system.debug('    createJointBillingItemLine::jbil ' + jbil);
        
        return jbil;
    }
    
    public static Joint_Billing_Item_Line__c createAdjustmentJointBillingItemLine(Decimal adjustmentAmount, Account providerOrMsoAccount, Id jointBillingItemId){
        Joint_Billing_Item_Line__c adjustmentJBIL = createJointBillingItemLine(adjustmentAmount, Date.Today(), Constant.JOINT_BILLING_ITEM_LINE_TYPE_ADJUSTMENT, providerOrMsoAccount);
        
        adjustmentJBIL.Joint_Billing_Item__c = jointBillingItemId;
        
        return adjustmentJBIL;
    }
    
    public static void setBillingItemOnLines(List<JointBillingUtil.JointBillingItemAndLineAssociation> assocs){
        for (JointBillingUtil.JointBillingItemAndLineAssociation assoc : assocs){
            assoc.jointBillingItemLine.Joint_Billing_Item__c = assoc.jointBillingItem.Id;
        }
    }
    
    public class JointBillingItemAndLineAssociation {
        public Joint_Billing_Item__c jointBillingItem { get; set; }
        
        public Joint_Billing_Item_Line__c jointBillingItemLine { get; set; }
    }
    
    public static Map<Id, List<Account>> getMSOCorporateProvidersMap(List<Account> jointBillingAccounts){
        Set<Id> corporateMSOIds = new Set<Id>();
        
        Map<Id, List<Account>> MSOCorporateProvidersMap = new Map<Id, List<Account>>();
        
        for (Account acct : jointBillingAccounts){
            if (isCorporateMSO(acct)){
                corporateMSOIds.add(acct.Id);
            }
        }
        
        if (corporateMSOIds.size() > 0){
            List<Account> msoCorporateProviders = getMSOCorporateProvidersFromMSOs(corporateMSOIds);
            
            for (Account msoCorporateProvider : msoCorporateProviders){
                List<Account> msoCorporateProvs = MSOCorporateProvidersMap.get(msoCorporateProvider.NU__PrimaryAffiliation__c);
                
                if (msoCorporateProvs == null){
                    msoCorporateProvs = new List<Account>();
                    MSOCorporateProvidersMap.put(msoCorporateProvider.NU__PrimaryAffiliation__c, msoCorporateProvs);
                }
                
                msoCorporateProvs.add(msoCorporateProvider);
            }
        }
        
        return MSOCorporateProvidersMap;
    }
    
    public static List<NU__Membership__c> createCorporateProviderMemberships(List<Account> corporateProviders, NU__Membership__c msoMembership){
        List<NU__Membership__c> corporateProviderMemberships = new List<NU__Membership__c>();
        
        if (corporateProviders != null) {
            for (Account corporateProvider : corporateProviders){
                NU__Membership__c corporateProviderMembership = new NU__Membership__c(
                    NU__Account__c = corporateProvider.Id,
                    NU__StartDate__c = msoMembership.NU__StartDate__c,
                    NU__EndDate__c = msoMembership.NU__EndDate__c,
                    NU__MembershipType__c = msoMembership.NU__MembershipType__c,
                    NU__ExternalAmount__c = 0
                );
                
                corporateProviderMemberships.add(corporateProviderMembership);
            }
        }
        
        return corporateProviderMemberships;
    }
    
    public static List<Account> getMSOCorporateProvidersFromMSOs(set<Id> msoIds){
        return [select id,
                       name,
                       NU__PrimaryAffiliation__c
                  from Account
                 where NU__PrimaryAffiliation__c in :msoIds
                   and Multi_Site_Enabled__c = true];
    }
    
    public static Boolean isCorporateMSO(Account acct){
        return acct != null &&
               acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
               acct.Multi_Site_Corporate__c == true;
    }
    
    public static NU.MembershipTermInfo calculateMembershipTermFromJointBilling(Joint_Billing__c jointBilling, NU__MembershipType__c jointStateProviderMembershipType){
        NU.MembershipTermInfo jointBillingMembershipTerm = new NU.MembershipTermInfo();
        
        jointBillingMembershipTerm.StartDate = jointBilling.Start_Date__c;
        
        Integer term = Integer.valueOf(jointStateProviderMembershipType.NU__Term__c);
        jointBillingMembershipTerm.EndDate = jointBillingMembershipTerm.StartDate.addMonths(term).addDays(-1);
        
        system.debug('   calculateMembershipTermFromJointBilling::jointBillingMembershipTerm ' + jointBillingMembershipTerm);
        
        return jointBillingMembershipTerm;
    }
    
    public static Date getProviderJoinOnDate(Account provider){
        Date providerJoinOnDate = Date.Today();
        
        if (provider.Provider_Join_On__c != null){
            providerJoinOnDate = provider.Provider_Join_On__c;
        }
        
        if (Test.isRunningTest() && ProviderJoinOnToUseForTesting != null){
            providerJoinOnDate = ProviderJoinOnToUseForTesting;
        }
        
        return providerJoinOnDate;
    }
}