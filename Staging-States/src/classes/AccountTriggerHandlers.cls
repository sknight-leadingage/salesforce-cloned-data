public class AccountTriggerHandlers extends NU.TriggerHandlersBase {
    Id multiSiteOrgRecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID;
    
    static List<Account> insertedIndividualsInContext = new List<Account>();
    static Set<Id> statesCanEditAccountIds = new Set<Id>();
    static boolean isExecuting = false;

    private User currentUserPriv = null;
    private User CurrentUser {
        get {
            if (currentUserPriv == null){
                currentUserPriv = UserQuerier.getCurrentUserWithStateInformation();
            }
            
            return currentUserPriv;
        }
    }
    
    private Boolean isStatePartnerUser(){
        return CurrentUser.ContactId != null ||
               UserQuerier.UserStatePartnerAccount != null;
    }

    private Id userStatePartnerIdPriv = null;
    private Boolean userStatePartnerIdCalculated = false;
    private Id UserStatePartnerId {
        get {
            if (userStatePartnerIdCalculated == false &&
                isStatePartnerUser() == true){

                userStatePartnerIdPriv = 
                UserQuerier.UserStatePartnerAccount != null ?
                    UserQuerier.UserStatePartnerAccount.Id :
                    CurrentUser.Contact.Account.State_Partner_Id__c;
                
                
                userStatePartnerIdCalculated = true;
            }
            
            return userStatePartnerIdPriv;
        }
    }
    
    private Id StatePartnerId(string Findname){     
                    
            if(Findname != null || Findname == '')      
            {       
                    List<Account> SPI = new List<Account>();        
                            
                    SPI = [Select Id from Account where Name = :Findname limit 1];      
                            
                    if(SPI.size() > 0)      
                    {       
                        return SPI[0].Id;       
                    }       
            }       
                    
             return null;       
        }
    private void populateInsertedIndividualInContext(List<Account> newAccounts){
        for (Account newAcct : newAccounts){            
            if (newAcct.IsPersonAccount == true){
                insertedIndividualsInContext.add(newAcct);
            }
        }
    }
    
    public void validateMultiSiteBillJointly(List<Account> accounts){
        for (Account acct : accounts){
            if (acct.RecordTypeId != multiSiteOrgRecordTypeId &&
                acct.Multi_Site_Bill_Jointly__c == true){
                acct.addError(Constant.BILL_JOINTLY_MULTISITE_ONLY_VAL_RULE_MSG);
            }
        }
    }
    
    public void defaultStatePartnerIDFromEntity(List<Account> newAccounts){     
                
        List<Account> Accounts = [select Id, RecordTypeId, IsPersonAccount, State_Partner_Id__c, State_Partner_Permissions__c, NU__PrimaryEntity__c, NU__PrimaryEntity__r.name from Account where Id in :newAccounts];      
                
        List<Account> accountsToUpdate = new List<Account>();       
        for (Account acct : Accounts){      
            if ((acct.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID ||       
                acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID ||      
                acct.IsPersonAccount == true)       
                && (acct.State_Partner_Id__c == null || acct.State_Partner_Permissions__c == null))     
                {
                 if(acct.NU__PrimaryEntity__c != null && (!acct.NU__PrimaryEntity__r.name.equalsIgnoreCase('LeadingAge') && !acct.NU__PrimaryEntity__r.name.equalsIgnoreCase('International Association of Homes and Services for the Ageing') ))       
                 {      
                    if(acct.State_Partner_Id__c == null && StatePartnerId(acct.NU__PrimaryEntity__r.name) != null){     
                        acct.State_Partner_Id__c = StatePartnerId(acct.NU__PrimaryEntity__r.name);
                        accountsToUpdate.add(acct);     
                    }
                    if(acct.State_Partner_Permissions__c == null && CurrentUser.State != null){
                        acct.State_Partner_Permissions__c = CurrentUser.State;
                    }

                 }          
                    
            }       
        }       
                
        if (accountsToUpdate.size() > 0){       
            update accountsToUpdate;        
        }       
            
    }
    
    
    public void defaultStatePartnerForStatePartnerCreatedAccounts(List<Account> newAccounts){
        if (isStatePartnerUser() == false){
            return;
        }
        
        for (Account acct : newAccounts){
            if (acct.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID ||
                acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID ||
                acct.IsPersonAccount == true){
                
                acct.State_Partner_Id__c = UserStatePartnerId;
                acct.State_Partner_Permissions__c = CurrentUser.State;
            }
        }
    }

    private List<Account> getLatestAccountInfos(List<Account> accounts){
        return [select id,
                        name,
                        NU__PrimaryAffiliation__c,
                        NU__PrimaryAffiliation__r.Id,
                        NU__PrimaryAffiliation__r.Provider_Membership__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__StartDate__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__EndDate__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__MembershipType__c,
                        NU__PrimaryAffiliation__r.RecordTypeId,
                        NU__PrimaryAffiliation__r.Multi_Site_Corporate__c,
                        Provider_Membership__c,
                        Provider_Membership__r.NU__MembershipType__c,
                        Provider_Membership__r.NU__StartDate__c,
                        Provider_Membership__r.NU__EndDate__c,
                        RecordTypeId,
                        (Select id,
                                name,
                                NU__MembershipType__c,
                                NU__StartDate__c,
                                NU__EndDate__c
                           from NU__Memberships__r)
                   from Account
                  where id in :accounts];
    }
    
    private Boolean isCorporateMSOWithProviderMembership(Account account){
        return account.NU__PrimaryAffiliation__r.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
               account.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == true &&
               account.NU__PrimaryAffiliation__r.Provider_Membership__c != null;
    }
    
    private Boolean needsCorporateProviderMembershipCreated(Account account){
        NU__Membership__c parentMSOMembership = account.NU__PrimaryAffiliation__r.Provider_Membership__r;
        
        for (NU__Membership__c accountMembership : account.NU__Memberships__r){
            if (accountMembership.NU__MembershipType__c == parentMSOMembership.NU__MembershipType__c &&
                accountMembership.NU__StartDate__c == parentMSOMembership.NU__StartDate__c &&
                accountMembership.NU__EndDate__c == parentMSOMembership.NU__EndDate__c){
                
                return false;
            }
        }
        
        return true;
    }
    
    private void createCorporateProviderMemberships(List<Account> newCorporateProvidersToCreateMembershipsFor){
        if (newCorporateProvidersToCreateMembershipsFor.size() > 0){
            List<NU__Membership__c> newCorporateProviderMemberships = new List<NU__Membership__c>();
            
            for (Account corporateProvider : newCorporateProvidersToCreateMembershipsFor){
                NU__Membership__c corporateProviderMembership = new NU__Membership__c(
                    NU__Account__c = corporateProvider.Id,
                    NU__StartDate__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__StartDate__c,
                    NU__EndDate__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__EndDate__c,
                    NU__MembershipType__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__MembershipType__c,
                    NU__ExternalAmount__c = 0
                );
                
                newCorporateProviderMemberships.add(corporateProviderMembership);
            }
            
            insert newCorporateProviderMemberships;
        }
    }
    
    private void createProviderMembershipsForNewCorporateProvidersOnAfterInsert(List<Account> newAccounts){
        List<Account> potentialNewCorporateProviders = new List<Account>();
        
        for (Account newAccount : newAccounts){
            if (newAccount.NU__PrimaryAffiliation__c != null &&
                newAccount.Multi_Site_Enabled__c == true){
                potentialNewCorporateProviders.add(newAccount);
            }
        }
        
        if (potentialNewCorporateProviders.size() > 0){
            List<Account> latestAccountInfos = getLatestAccountInfos(potentialNewCorporateProviders);
            List<Account> newCorporateProvidersToCreateMembershipsFor = new List<Account>();
            
            for (Account latestAccountInfo : latestAccountInfos){
                if (isCorporateMSOWithProviderMembership(latestAccountInfo)){
                    newCorporateProvidersToCreateMembershipsFor.add(latestAccountInfo);
                }
            }
            
            createCorporateProviderMemberships(newCorporateProvidersToCreateMembershipsFor);
        }
    }
    
    private void createProviderMembershipsForNewCorporateProviders(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
        Boolean bNeedsDeeperInspection = false;
        
        for (Account oldAccount : oldAccounts.values()){
            Account newAccount = newAccounts.get(oldAccount.Id);
            
            if ((oldAccount.NU__PrimaryAffiliation__c != newAccount.NU__PrimaryAffiliation__c &&
                  newAccount.NU__PrimaryAffiliation__c != null) 
                  
                  ||
                
                 (oldAccount.Multi_Site_Enabled__c == false &&
                  newAccount.Multi_Site_Enabled__c == true)
                
                )
                {
                    
                bNeedsDeeperInspection = true;
                break;
            }
        }
        
        
        if (bNeedsDeeperInspection) {
            Map<Id, Account> latestAccountInfos = new Map<Id, Account>( getLatestAccountInfos(newAccounts.values()) );
            List<Account> newCorporateProvidersToCreateMembershipsFor = new List<Account>();
            
            for (Account oldAccount : oldAccounts.values()){
                Account newAccount = newAccounts.get(oldAccount.Id);
                Account latestAccountInfo = latestAccountInfos.get(oldAccount.Id);
                
                if (((oldAccount.NU__PrimaryAffiliation__c != newAccount.NU__PrimaryAffiliation__c &&
                      newAccount.NU__PrimaryAffiliation__c != null) 
                      
                      ||
                    
                     (oldAccount.Multi_Site_Enabled__c == false &&
                      newAccount.Multi_Site_Enabled__c == true)
                    
                    )
                    
                     &&
                    newAccount.Multi_Site_Enabled__c == true &&
                    isCorporateMSOWithProviderMembership(latestAccountInfo) &&
                    needsCorporateProviderMembershipCreated(latestAccountInfo)){
                        
                    newCorporateProvidersToCreateMembershipsFor.add(latestAccountInfo);
                }
            }
            
            createCorporateProviderMemberships(newCorporateProvidersToCreateMembershipsFor);
        }
    }
    
    private void setStatePartnerFieldsForStateUsers(List<Account> newAccounts){
        Account stateUserStatePartnerAccount = UserQuerier.UserStatePartnerAccount;
        
        if (stateUserStatePartnerAccount == null){
            return;
        }
        
        for (Account newAccount : newAccounts){
            newAccount.State_Partner_ID__c = stateUserStatePartnerAccount.Id;
            newAccount.State_Partner_Permissions__c = CurrentUser.State;
        }
    }
    
    private void setCompanyManagerBasedOnExpertise(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts){
        Map<Id, Account> newAccountsAffil = new Map<Id, Account>([SELECT
            Id,
            Organizational_Function_Expertise__pc,
            IsPersonAccount,
            NU__PrimaryAffiliationRecord__c,
            NU__PrimaryAffiliationRecord__r.Id,
            NU__PrimaryAffiliationRecord__r.NU__IsCompanyManager__c
            FROM Account
            WHERE Id IN :newAccounts.keyset()
        ]);
        
        List<NU__Affiliation__c> changes = new List<NU__Affiliation__c>();
        
        NU__Affiliation__c tempAffil;
        for (Account changedAcc : newAccounts.values()){
            Account accWithAff = newAccountsAffil.get(changedAcc.Id);
            if (changedAcc != null && changedAcc.Organizational_Function_Expertise__pc != null && accWithAff != null &&
                changedAcc.IsPersonAccount && (changedAcc.Organizational_Function_Expertise__pc.containsIgnoreCase('Marketing/Sales') ||
                changedAcc.Organizational_Function_Expertise__pc.containsIgnoreCase('PR/Communications')) &&
                accWithAff.NU__PrimaryAffiliationRecord__c != null &&
                accWithAff.NU__PrimaryAffiliationRecord__r.NU__IsCompanyManager__c != true) {
                    tempAffil = new NU__Affiliation__c(Id = accWithAff.NU__PrimaryAffiliationRecord__r.Id, NU__IsCompanyManager__c = true);
                    if (tempAffil.Id != null) {
                        //Above if tests for case when logged in user has permissions to the account but not the affiliation record
                        changes.add(tempAffil);
                    }
                    
            }
           
        }
        
        if (!changes.isEmpty()){
            update changes;
        }
    }
    


    public override void onBeforeInsert(List<Sobject> newRecords){
        List<Account> newAccounts = (List<Account>) newRecords;
        
        populateInsertedIndividualInContext(newAccounts);
        validateMultiSiteBillJointly(newAccounts);
        defaultStatePartnerForStatePartnerCreatedAccounts(newAccounts);
        setStatePartnerFieldsForStateUsers(newAccounts);
    }
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        List<Account> newAccounts = (List<Account>) newRecordMap.values();
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldRecordMap;
        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newRecordMap;
        
        validateMultiSiteBillJointly(newAccounts);
        CorporateAllianceMembershipPriority.setCorporateAllianceMembershipPriority(newAccountsMap);
        
    }
    
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldRecordMap;
        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newRecordMap;
        List<Account> newAccounts = (List<Account>) newRecordMap.values();      
                
        defaultStatePartnerIDFromEntity(newAccounts);
        createProviderMembershipsForNewCorporateProviders(oldAccountsMap, newAccountsMap);
        setCompanyManagerBasedOnExpertise(newAccountsMap, oldAccountsMap);
    }
    
    public override void onAfterInsert(Map<Id, sObject> newRecordMap){
        List<Account> newAccounts = (List<Account>) newRecordMap.values();
        
        defaultStatePartnerIDFromEntity(newAccounts);
        createProviderMembershipsForNewCorporateProvidersOnAfterInsert(newAccounts);
    }
}