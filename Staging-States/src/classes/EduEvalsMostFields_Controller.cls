public with sharing class EduEvalsMostFields_Controller extends EduReportsControllerBase {
    public List<Conference_Session__c> DataList { get;set; }

    public override void SetPageType()
    {
        PageCType = 'contentType="application/vnd.ms-excel#EduEvalsMostFields.xls"';
    }
    
    public override string getPageTitle() {
        return RptRenderAs == '' && Conference != null ? Conference.Name + ': Evals - Most Fields' : '';
    }
    
    public EduEvalsMostFields_Controller() {
        PageCType = '';
        SetConPageSize = 255;
        SetQuery();
    }
    
    public override void SetConRefreshData() {
        DataList = setCon.getRecords();
    }
    
    public void SetQuery() {
        SetConQuery = 'SELECT Session_Number_Numeric__c,Title__c,Timeslot__r.Timeslot_Code__c,Timeslot__r.Timeslot__c,Learning_Objective_1__c,Learning_Objective_2__c,Learning_Objective_3__c,CA_RCFE__r.Name,Florida__r.Name,Kansas__r.Name,Missouri__r.Name,NAB__r.Name,NASBA__r.Name,PTF_Code__c,PTF_Hours__c,Conference_Track__r.Name, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.ShippingCity,Speaker__r.ShippingState,Speaker__r.PersonEmail FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\'),(SELECT Conference_Content_Category__r.Name FROM Conference_Content_Category_Sessions__r),(SELECT CE_Category__r.Name FROM CE_Conference_Sessions__r) FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}