public with sharing class JointStateProviderOrderManagerController {
	public Joint_Billing__c JointBilling { get; set; }
	
	public NU__Cart__c dummyCart { get; set; }
	
	public NU__MembershipType__c JointStateProviderMembershipType { get; set; }
	
	public NU__Order__c managedOrder { get; set; }
	
	public String Mode {
		get{
			if (JointBilling != null){
				return JointBilling.Order__c != null ? 'Update' : 'Create';
			}
			
			return '';
		}
	}
	
	public Map<Id, NU__Batch__c> OpenManualBatchesMap {
		get{
			return new Map<Id, NU__Batch__c>(
			   BatchQuerier.getEntityOpenManualBatches(JointStateProviderMembershipType.NU__Entity__c)
			);
		}
	}
	
	public Boolean IsManualBatchSelected {
		get{
			return dummyCart != null && dummyCart.NU__Batch__c != null;
		}
	}
	
	public List<SelectOption> OpenManualBatchSelectOpts{
		get{
			List<SelectOption> selectOpts = new List<SelectOption>();
			
			for (NU__Batch__c openManualBatch : OpenManualBatchesMap.values()){
				SelectOption selectOpt = new SelectOption(openManualBatch.Id, openManualBatch.NU__DisplayName__c);
				
				selectOpts.add(selectOpt);
			}
			
			return selectOpts;
		}
	}
	
	private List<String> AdditionalFieldsToQuery = new List<String>{
		'Order__c',
		'Adjustment_Amount__c'
	};
	
	
	public JointStateProviderOrderManagerController(ApexPages.StandardController standardController){
		if (Test.isRunningTest() == false){
			standardController.addFields(AdditionalFieldsToQuery);
		}
		
		JointBilling = (Joint_Billing__c) standardController.getRecord();
		
		dummyCart = new NU__Cart__c(
			NU__TransactionDate__c = Date.Today()
		);
		
		JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
	}
	
	public void batchSelected(){
		if (IsManualBatchSelected){
			NU__Batch__c selectedManualBatch = OpenManualBatchesMap.get(dummyCart.NU__Batch__c);
			
			dummyCart.NU__TransactionDate__c = selectedManualBatch.NU__TransactionDate__c;
		}
		else{
			dummyCart.NU__TransactionDate__c = Date.Today();
		}
	}
	
	public PageReference manageOrder(){		
		try{
			JointStateProviderOrderManager orderManager = new JointStateProviderOrderManager(JointBilling.Id, dummyCart.NU__Batch__c, dummyCart.NU__TransactionDate__c);
			
			if (Mode == 'Create'){
				managedOrder = orderManager.createOrder();
			}
			else{
				managedOrder = orderManager.updateOrder();
			}
			
			return new ApexPages.StandardController(managedOrder).view();
		}
		catch (Exception ex){
			ApexPages.addMessages(ex);
			
			if (Test.isRunningTest()){
				throw ex;
			}
		}
		
		return null;
	}
}