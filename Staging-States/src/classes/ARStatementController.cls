public with sharing class ARStatementController {

	private List<Account> privAccounts { get; set; }
	public List<Account> Accounts { get; set; }
	public List<NU__Order__c> Orders { get; set; }
	public Account Attentionto { get; set;}
	public List<NU__Affiliation__c> PrimeContacs { get; set; }
	public String AdditionalInvoiceContactsSelected { get; set; }
	public string Attnon {get; set;}
	public List<SelectOption> AdditionalInvoiceContactsOpts {
        get {
            List<SelectOption> opts = new List<SelectOption>();
            
            for (NU__Affiliation__c paf : PrimeContacs){
                
                SelectOption opt = new SelectOption(paf.NU__Account__r.Name, paf.NU__Account__r.Name);
                opts.add(opt);
            }
            
            return opts;
        }
    }
	public String InvoiceDialogScript { get; set; }
	
	public NU__Order__c DummyOrder { 
    	get {
    		if (DummyOrder == null) {
    			DummyOrder = new NU__Order__c();
    		}
    		return DummyOrder;
    	}
    	set {
    		DummyOrder = value;
    	}
    }
    
    public Decimal MinimumBalanceOverride {
    	get {
    		return ARStatementUtil.minimumBalanceOverride;
    	}
    	set {
    		if (value >= 0.0) {
    			ARStatementUtil.minimumBalanceOverride = value;
    		}
    		else {
    			ARStatementUtil.minimumBalanceOverride = 0.0;
    		}
    	}
    }
    
    public Id EntityId {
    	get {
    		return ARStatementUtil.entityId;
    	} 
    	set {
    		ARStatementUtil.entityId = value;
    	}
    }
    
    public List<SelectOption> EntityOptions {
    	get {
    		return ARStatementUtil.Instance.EntityOptions;
    	}
    }
	
	public Boolean CanRender { get; set; }
	public Boolean RenderPDF { get; set; }
    
	public ARStatementController(ApexPages.StandardSetController controller) {
		Accounts = new List<Account>();
		Attentionto = new Account();
		Attentionto.Name = 'John Smith';
		

		
		CanRender = false;
		Integer selectionSize = controller.getSelected().size();
		
		if (selectionSize == 0 && ApexPages.currentPage().getParameters().get('id') == null) {
        	ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error, 'You did not select any accounts. Please go back and make a selection before continuing.');
	        ApexPages.addMessage(pageMsg);
        }
		
		if (selectionSize > 0) {
			privAccounts = controller.getSelected();
		} else {
			try {
				privAccounts = new List<Account> { new Account(Id = ApexPages.currentPage().getParameters().get('id')) };
			} catch (Exception e) { }
		}
		

		
		PrimeContacs = [SELECT Name,NU__Account__c, NU__Account__r.Name, NU__Account__r.PersonEmail,NU__Role__c,NU__Status__c FROM NU__Affiliation__c WHERE (NU__ParentAccount__c = :ApexPages.currentPage().getParameters().get('id')) AND NU__Role__c INCLUDES('Invoice Contact','LeadingAge Primary Contact')];
		
	}
	
	public String getRender() {
		if (ApexPages.currentPage().getParameters().get('pdf') != null) {
			RenderPDF = Boolean.valueOf(ApexPages.currentPage().getParameters().get('pdf'));
			return 'pdf';
		}
		
		return null;
	}
	
	public PageReference PDF(){
		updateSettings();
		
		PageReference ref = ApexPages.currentPage();
		ref.getParameters().put('pdf','true');
		return ref;
	}
	
	private void updateSettings() {
    	InvoiceDialogScript = null; 
    	
    	if (DummyOrder.NU__InvoiceDate__c != null) {
    		ARStatementUtil.invoiceDateOverride = DummyOrder.NU__InvoiceDate__c;
    	}
    }

	public void Print(){
		updateSettings();

		InvoiceDialogScript = 'print();';
	}
	
	public void updateAttentionTo(){
		try{
			for (NU__Order__c order : Orders){
				order.Invoice_Attention_To__c = dummyOrder.Invoice_Attention_To__c;
			}
			
			update Orders;
		}
		catch(Exception ex){
			ApexPages.addMessages(ex);
		}
	}
	
	public void Generate(){
		// is a valid entity selected?
    	if (entityId == null) {
    		ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please select an entity.');
	        ApexPages.addMessage(pageMsg);
	        
	        
	        CanRender = false;
	        return;
    	}
    	

    	
		updateSettings();
		
		// validate the accounts - do they have orders with an AR balance greater than the minimum balance specified, for the selected Entity?
		Accounts = ARStatementUtil.Instance.getValidAccounts(privAccounts);
		CanRender = (Accounts.size() > 0);
		
		if (privAccounts.size() != Accounts.size()) { // accounts were removed
	        ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Warning, 'Some of the accounts you selected are not displayed below because they do not have an outstanding balance.');
	        ApexPages.addMessage(pageMsg);
	    }
	}
}