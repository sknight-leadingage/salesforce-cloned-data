public with sharing class InvoiceSet {
	public static final String TEMPLATE_INVOICE = 'Custom_Invoice';
	
	public List<NU__Order__c> Orders { get; set; }
	public List<NU__Affiliation__c> PrimeContacs { get; set; }
	
	public String InvoiceDialogScript { get; set; }
	
	public NU__Order__c dummyOrder { get; set; }
	
	public Boolean ShowEmailButton { get; set; }
	
	public Boolean RenderPDF { get; set; }
	
	public Id JobId { get; set; }
	
	public InvoiceSet(ApexPages.StandardController stdController){
		dummyOrder = new NU__Order__c();
		Orders = new List<NU__Order__c>{ new NU__Order__c( Id = stdController.getId() ) };
		
		init();
	}

	public InvoiceSet(ApexPages.StandardSetController controller) {
		dummyOrder = new NU__Order__c();
		Integer selectionSize = controller.getSelected().size();
		
		if (selectionSize > 0) {
			Orders = controller.getSelected();
		} else {
			try {
				Orders = new List<NU__Order__c> { new NU__Order__c(Id = ApexPages.currentPage().getParameters().get('id')) };
			} catch (Exception e) { }
		}
		
		init();
	}
	
	public String AdditionalInvoiceContactsSelected { get; set; }
    public List<SelectOption> AdditionalInvoiceContactsOpts {
        get {
            List<SelectOption> opts = new List<SelectOption>();
            
            for (NU__Affiliation__c paf : PrimeContacs){
                
                SelectOption opt = new SelectOption(paf.NU__Account__r.Name, paf.NU__Account__r.Name);
                opts.add(opt);
            }
            
            return opts;
        }
    }
	
	private void init(){

		Orders = [select Name, NU__InvoiceEmail__c, NU__BillTo__r.NU__PersonEmail__c from NU__Order__c where Id in :Orders];
		
		Set<Id> Billers = new Set<Id>();
		
		for (NU__Order__c Biller : Orders){
		 	Billers.add(Biller.NU__BillTo__r.Id);
		}
		
		
		PrimeContacs = [SELECT Name,NU__Account__c, NU__Account__r.Name, NU__Account__r.PersonEmail,NU__Role__c,NU__Status__c FROM NU__Affiliation__c WHERE (NU__ParentAccount__c in :Billers) AND NU__Role__c INCLUDES('Invoice Contact','LeadingAge Primary Contact')];
		
		NU.OperationResult result = validateInvoiceEmailContact();
		ShowEmailButton = NU.OperationResult.isSuccessful(result);
		
		if (!ShowEmailButton) {
			PageUtil.AddResultMessagesToPage(result);
		}
	}
	
	public String getRender() {
		if (ApexPages.currentPage().getParameters().get('pdf') != null) {
			RenderPDF = Boolean.valueOf(ApexPages.currentPage().getParameters().get('pdf'));
			return 'pdf';
		}
		return null;
	}
	
	public PageReference PDF(){
		NU.OperationResult invoiceResult = new OrderInvoicer().invoiceOrders(Orders, dummyOrder.NU__InvoiceDate__c);
		
		PageReference ref = ApexPages.currentPage();
		
		if (NU.OperationResult.isSuccessful(invoiceResult)){
			ref.getParameters().put('pdf','true');
			return ref;
		}
		else{
			PageUtil.AddResultMessagesToPage(invoiceResult);
			return ref;
		}
	}
	
	public void Email(){
		BatchEmailSender.checkOldResults();
		
		InvoiceDialogScript = null;
		
		List<NU__Order__c> invoiceOrders = Orders.clone();
		for (Integer i = invoiceOrders.size() - 1; i >= 0; i--) {
			NU__Order__c ord = invoiceOrders[i];
			if (String.isBlank(ord.NU__InvoiceEmail__c) && String.isBlank(ord.NU__BillTo__r.NU__PersonEmail__c)) {
				invoiceOrders.remove(i);
				String errorMsg = '<a href="/' + ord.Id + '">' + ord.Name + '</a> -  Neither the Bill To Email nor the Invoice Email are set';
				PageUtil.addErrorMessageToPage(errorMsg);
			}
		}

		NU.OperationResult invoiceResult = new OrderInvoicer().invoiceOrders(invoiceOrders, dummyOrder.NU__InvoiceDate__c);

		if (NU.OperationResult.isSuccessful(invoiceResult) == false){
			PageUtil.AddResultMessagesToPage(invoiceResult);
			return;
		}

		Id invoiceEmailTemplateId = GetTemplateId(TEMPLATE_INVOICE);

    	if (invoiceEmailTemplateId == null){
    		PageUtil.AddErrorMessageToPage('The invoice template, "' + TEMPLATE_INVOICE + '", was not found.');
    		return;
    	}
    	
		Invoice_Settings__c settings = Invoice_Settings__c.getInstance();

    	Id invoiceEmailContactId = (Id) settings.Invoice_Email_To_Contact_Id__c;
    	Id invoiceEmailFromOrgWideId = (Id) settings.Invoice_Email_From_Org_Wide_Id__c;

    	List<Email> invoiceEmails = new List<Email>();

    	for (NU__Order__c emailOrder : invoiceOrders){
    		Email invoiceEmail = new Email();
    		invoiceEmail.TemplateId = invoiceEmailTemplateId;
        	invoiceEmail.WhatId = emailOrder.Id;
        	invoiceEmail.SaveAsActivity = false;
    		invoiceEmail.TargetObjectId = invoiceEmailContactId;

        	List<String> toAddresses = new List<String>();

        	if (!String.isBlank(emailOrder.NU__InvoiceEmail__c))
        	{
        		toAddresses.add(emailOrder.NU__InvoiceEmail__c);
        	}
        	
        	if (!String.isBlank(emailOrder.NU__BillTo__r.NU__PersonEmail__c))
        	{
        		toAddresses.add(emailOrder.NU__BillTo__r.NU__PersonEmail__c);
        	}
        	
        	if (invoiceEmailFromOrgWideId != null)
	        {
	        	invoiceEmail.OrgWideEmailAddressId = invoiceEmailFromOrgWideId;
	        }

        	invoiceEmail.ToAddresses = toAddresses;

    		invoiceEmails.add(invoiceEmail);
    	}

		JobId = InvoiceBatchEmailSender.send(invoiceEmails);
	}

	public void Print(){
		InvoiceDialogScript = null;

		NU.OperationResult invoiceResult = new OrderInvoicer().invoiceOrders(Orders, dummyOrder.NU__InvoiceDate__c);

		if (NU.OperationResult.isSuccessful(invoiceResult)){
			InvoiceDialogScript = 'print();';
		}
		else{
			PageUtil.AddResultMessagesToPage(invoiceResult);
		}
	}
	
	public void updateAttentionTo(){
		try{
			String strAttnTo = '';
			if (dummyOrder.Invoice_Attention_To__c != null) {
				strAttnTo = dummyOrder.Invoice_Attention_To__c; 
			}
			else if (AdditionalInvoiceContactsSelected != null) {
				strAttnTo = AdditionalInvoiceContactsSelected;
			}
			for (NU__Order__c order : Orders){
				order.Invoice_Attention_To__c = strAttnTo;
			}
			
			update Orders;
		}
		catch(Exception ex){
			ApexPages.addMessages(ex);
		}
	}
	
	private NU.OperationResult validateInvoiceEmailContact() {
		NU.OperationResult result = new NU.OperationResult();
		
		String invoiceEmailContactId = Invoice_Settings__c.getInstance().Invoice_Email_To_Contact_Id__c;
		String error = null;
		
		if (String.isBlank(invoiceEmailContactId)) {
			error = 'the Invoice Email Contact Id is not set on the Invoice Settings custom setting.';
		}
		
		List<Contact> contacts = [select Email from Contact where Id = :invoiceEmailContactId];
		if (contacts.size() != 1) {
			error = 'the Invoice Email Contact Id on the Invoice Settings custom setting is not the id of a Contact record.';
		}
		else
		{
			if (String.isBlank(contacts[0].Email)) {
				error = 'the Contact record specified by Invoice Email To Contact Id on the Invoice Settings custom setting must have an email address.';
			}
		}
		
		if (error != null) {
			result.Status = NU.OperationStatus.FAILURE;
			
			NU.OperationMessage om = new NU.OperationMessage();
			om.Message = 'The email button is hidden because ' + error;
			om.Level = NU.OperationMessageLevel.INFO;
			
			result.Messages.add(om);
		}
		
		return result;
	}
	
	public String getClassPrefix() {
		return 'NU.';
	}
	
	public String getFieldPrefix() {
		return 'NU__';
	}
	
	public static Id GetTemplateId(String name) {
    	EmailTemplate result;
    	List<EmailTemplate> templates = [select Id, IsActive from EmailTemplate where DeveloperName = :name and IsActive = true and NamespacePrefix in ('NU', null) order by NamespacePrefix nulls last];
    	if (templates.size() > 0) {
    		return templates[0].Id;
    	}
    	return null;
    }
	
	@RemoteAction
	public static BatchEmailSender.JobStatus getJobStatus(Id jobId) {
		return BatchEmailSender.getJobStatus(jobId);
	} 
}