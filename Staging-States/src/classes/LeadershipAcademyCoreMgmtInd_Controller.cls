public with sharing class LeadershipAcademyCoreMgmtInd_Controller {
    public static final String ACADEMY_YEAR_PARAM = 'acadyear';
    public List<Academy_Applicant__c> AcademyCoreMgmtInd {get; set;}
    
    public LeadershipAcademyCoreMgmtInd_Controller(){
		AcademyCoreMgmtInd = getAcademyCoreMgmtInd();
            
		if (AcademyCoreMgmtInd != null && AcademyCoreMgmtInd.size() > 0) {
			AppStatus = AcademyCoreMgmtInd[0].Application_Status__c;
		}                    
    }
    
    public string AcademyYear
    {
        get
        {
            return ApexPages.CurrentPage().getParameters().get(ACADEMY_YEAR_PARAM);
        }
   
    }
    
    public Id AppId{
        get{
            return ApexPages.CurrentPage().getParameters().get('acc');
        }
    }
    
     
    public string AppStatus {get;set;}
    public List<SelectOption> getAppStatuses(){
            List<SelectOption> options = new List<SelectOption>();
            
            Schema.DescribeFieldResult fieldResult = Schema.Academy_Applicant__c.fields.Application_Status__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
               options.add(new SelectOption('','--None--'));
               for(Schema.PicklistEntry p : ple)
                   options.add(new SelectOption(p.getValue(), p.getValue())); 
               return options;
    }
        
    private List<Academy_Applicant__c> getAcademyCoreMgmtInd(){
		if (AppId != null ) {
        	if (AcademyCoreMgmtInd == null || AcademyCoreMgmtInd.size() <= 0) {
            	AcademyCoreMgmtInd = [SELECT Id, Name, Account__c, Account__r.Name, 
                    Academy_Year__c, Submission_Status__c, Application_Status__c, Submit_Date__c,
                    Academy_Status__c,
                    Academy_Status__r.Academy_Class_Status__c, Academy_Status__r.Academy_Start_Date__c, Academy_Status__r.Academy_End_Date__c, 
                    Academy_Status__r.Academy_Year__c, Academy_Status__r.Cohort__c, Academy_Status__r.Financial_Assistance__c FROM Academy_Applicant__c WHERE Id = :AppId ];
        	}
         	return AcademyCoreMgmtInd;
		}
		else {
      		return null;
		}
    }
    
    public void Save()
    {
        try
        {
	        if (AcademyCoreMgmtInd != null && AcademyCoreMgmtInd.size() > 0) {
	        	AcademyCoreMgmtInd[0].Application_Status__c = AppStatus;
	        	Academy_Applicant__c aa = new Academy_Applicant__c(Id = AppId, Application_Status__c = AppStatus);
	        	update aa;

				if (AcademyCoreMgmtInd[0].Academy_Status__c != null) {
					update AcademyCoreMgmtInd[0].Academy_Status__r;
				}
			}
        }
    
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }

    public void AddtoClass()
    {
        try{
        	Academy_Applicant__c app = getAcademyCoreMgmtInd()[0]; //Get first one in list, because typically there's only one anyway

			if(app.Academy_Status__c == null) {
				//if we don't have a lookup on Applicant to Status then create one
				Academy_Status__c AcademyStatus = new Academy_Status__c(
					Account__c = app.Account__c,
					Academy_Class_Status__c = 'Applicant',
					Academy_Year__c = AcademyYear
				);
				insert AcademyStatus;
               
				//Now we have an Academy Status record, create the link to it in the Applicant record
				app.Academy_Status__c = AcademyStatus.Id;
				update app;
				
				AcademyCoreMgmtInd = null;
				AcademyCoreMgmtInd = getAcademyCoreMgmtInd(); 
			}
        }
        catch(Exception ex){
			ApexPages.addMessages(ex);
        }
    }
}