public with sharing class EduCEsByCategory_Controller extends EduReportsControllerBase {
    public List<CE_Conference_Session__c> CEList { get;set; }
    
    public override string getPageTitle() {
        return 'Sessions By CE Category';
    }
    
    public EduCEsByCategory_Controller() {
        SetConPageSize = 225;
        SetQuery();
    }
    
    public override void SetConRefreshData() {
        CEList = setCon.getRecords();
    }
    
    public void SetQuery() {
        SetConQuery = 'SELECT CE_Category__r.Name,Conference_Session__r.Timeslot__r.Timeslot_Code__c, Conference_Session__r.Session_Number__c, Conference_Session__r.Title__c FROM CE_Conference_Session__c WHERE Conference_Session__r.Conference__c = :EventId ORDER BY CE_Category__r.Name, Conference_Session__r.Timeslot__r.Timeslot_Code__c, Conference_Session__r.Session_Number_Numeric__c';
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}