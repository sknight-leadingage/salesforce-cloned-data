public with sharing class ProviderMembershipPricer extends MembershipPricerBase implements IProductPricer {
    public static final Decimal MIN_DUES = 350;
    public static final Decimal AFTER_FIVE_YEARS_REINSTATEMENT_DISCOUNT = 0.50;
    public static final Decimal INCREASE_RATE = 0.05;
    public static final Integer NUMBER_OF_YEARS_BEFORE_STARTING_DUES_INCREASING = 3;
    public static final Decimal NEW_MEMBER_JOIN_DISCOUNT = 0.50;
    public static final Decimal UNDER_CONSTRUCTION_FIRST_FULL_YEAR_DISCOUNT = 0.50;
    public static final Decimal NEW_MEMBER_JOIN_SECOND_HALF_OF_YEAR_PRICE = 0;
    public static final Decimal JOINED_2ND_HALF_LAST_YEAR_FIRST_FULL_YEAR_DISCOUNT = 0.50;
    public static final Decimal BEFORE_2014_MAX_PROVIDER_DUES_PRICE = 8000;
    public static final Decimal AFTER_2013_MAX_PROVIDER_DUES_PRICE = 9000;
    public static final Decimal FOR_2015_MAX_PROVIDER_DUES_PRICE = 9189;
    public static final Date JAN_1_2014 = Date.newInstance(2014, 1, 1);
    public static final Date JAN_1_2015 = Date.newInstance(2015, 1, 1);
    
    private Date getToday(Boolean IsFutureCalc) {
    	Date dToday = Date.Today();
    	
        if (IsFutureCalc == true) {
        	dToday = dToday.addYears(1);
        }
        
        return dToday;
    }
    
    private Boolean isReinstatingMember(Account customer, List<NU__Membership__c> providerMemberships, Boolean IsFutureCalc){
        Boolean isCurrentlyLapsed = false;
        Boolean hasCurrentMembership = false;
        Date dToday = getToday(IsFutureCalc);
        Date dEnd = dToday; //Init with a value
        if (NU.CollectionUtil.listHasElements(providerMemberships)){
            for(NU__Membership__c m : providerMemberships){
                //We have to make sure that if there's an expired membership, that there isn't also a current one for the same period
                dEnd = m.NU__StartDate__c.addYears(1).addDays(-1);
                if (dToday >= m.NU__StartDate__c && dToday <= dEnd 
                    && m.NU__Status__c == 'Expired' &&
                    (m.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME
                        || m.NU__MembershipType__r.Name == Constant.NON_JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME)){
                    isCurrentlyLapsed = true;
                }
                if (dToday >= m.NU__StartDate__c && dToday <= dEnd
                    && m.NU__Status__c == 'Current' &&
                    (m.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME
                        || m.NU__MembershipType__r.Name == Constant.NON_JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME)){
                    hasCurrentMembership = true;
                }
            }
        }
        
        return customer != null &&
               NU.CollectionUtil.listHasElements(providerMemberships) &&
               ((customer.Provider_Lapsed_On__c != null) || (isCurrentlyLapsed == true && hasCurrentMembership == false));
    }
    
    private Boolean wasUnderConstructionLastMembershipYear(Account customer, List<NU__Membership__c> providerMemberships, Boolean IsFutureCalc){
        Boolean bWasUnderConstruction = false;
        Date dToday = getToday(IsFutureCalc);
        Date dTodayLastYear = dToday.addYears(-1);
        Date dEnd = dToday; //Init with a value
        if (NU.CollectionUtil.listHasElements(providerMemberships)){
            for(NU__Membership__c m : providerMemberships){
                dEnd = m.NU__StartDate__c.addYears(1).addDays(-1);
                if ((dTodayLastYear >= m.NU__StartDate__c && dTodayLastYear <= dEnd && m.Under_Construction__c == true)
                    || (dToday >= m.NU__StartDate__c && dToday <= dEnd && m.Under_Construction__c == true)){
                    bWasUnderConstruction = true;
                }
            }
        }
        return bWasUnderConstruction;
    }
    
    
    
    private Boolean didStartSecondHalfOfLastYear(Account customer, List<NU__Membership__c> providerMemberships, Boolean IsFutureCalc){
        Boolean bRetVal = false;
        Date dToday = getToday(IsFutureCalc);
        Date dTodayLastYear = dToday.addYears(-1);
        Date dEnd = dToday; //Init with a value
        if (NU.CollectionUtil.listHasElements(providerMemberships)){
            for(NU__Membership__c m : providerMemberships){
                dEnd = m.NU__StartDate__c.addYears(1).addDays(-1);
                if ((m.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME) && (dTodayLastYear >= m.NU__StartDate__c && dTodayLastYear <= dEnd)
                    && (m.NU__StartDate__c.addMonths(6) <= m.Membership_Date__c && m.Membership_Date__c <= m.NU__EndDate__c)){
                    bRetVal = true;
                }
            }
        }
        return bRetVal;
    }
    
    private Boolean hasProviderMembershipInTheLast5Years(Account customer, List<NU__Membership__c> providerMemberships, Boolean isYearMinusOne, Boolean IsFutureCalc){
        Boolean bRetVal = false;
        
        Date dToday = getToday(IsFutureCalc);
        Date dEnd = dToday; //Init with a value
        Date dEndPlus5 = dToday; //Init with a value
        if (isYearMinusOne) {
        	dToday = dToday.addYears(-1); 
        }
        if (NU.CollectionUtil.listHasElements(providerMemberships)){
            for(NU__Membership__c m : providerMemberships){
                dEnd = m.NU__StartDate__c.addYears(1).addDays(-1);
                dEndPlus5 = dEnd.addYears(5);
                if (m.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME
                    && dEndPlus5 > dToday
                    && dEnd < dToday){
                    bRetVal = true;
                    break;
                }
            }
        }
        return bRetVal;
    }
    
    private Boolean isNewByMembershipCount(Account customer, List<NU__Membership__c> providerMemberships, Boolean isYearMinusOne, Boolean IsFutureCalc){
        Boolean bRetVal = false;

        if (providerMemberships == null || (providerMemberships != null && providerMemberships.size() <= 0)) {
        	bRetVal = true;
        }
        else {
        	Integer iMCount = 0;
	        Date dToday = getToday(IsFutureCalc);
	        Date dEnd = dToday; //Init with a value
	        Date dEndPlus5 = dToday; //Init with a value
	        if (isYearMinusOne) {
	        	dToday = dToday.addYears(-1); 
	        }
	        if (NU.CollectionUtil.listHasElements(providerMemberships)){
	            for(NU__Membership__c m : providerMemberships){
	                dEnd = m.NU__StartDate__c.addYears(1).addDays(-1);
	                dEndPlus5 = dEnd.addYears(5);
	                if (m.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME
	                    && dEndPlus5 > dToday
	                    && dEnd < dToday
	                    && m.NU__Status__c != Constant.MEMBERSHIP_STATUS_CURRENT){
	                    	iMCount++;
	                    //bRetVal = true;
	                    //break;
	                }
	            }
	        }
	        if (iMCount <= 0) {
	        	bRetVal = true;
	        }
        }
        return bRetVal;
    }
    
    private Boolean isReinstatingAfter5Years(Account customer, Boolean IsFutureCalc){
        Date memberThruDate = customer.Provider_Member_Thru__c;
        if (memberThruDate == null) {
            return false;
        }
        
        Date fiveYearsAfterMemberThru = memberThruDate.addYears(5);
        
        return getToday(IsFutureCalc) > fiveYearsAfterMemberThru;
    }
    
    private Boolean isOutOfDateProgramServiceRevenueIncreaseNeeded(NU.ProductPricingInfo ppi, Account customer, Boolean IsFutureCalc){
        Integer yearCalc = getToday(IsFutureCalc).year();
        if (ppi != null && ppi.StartDate != null){
            yearCalc = ppi.StartDate.year();
        }
        
        return customer.Revenue_Year_Submitted__c != null && (yearCalc - Integer.valueOf(customer.Revenue_Year_Submitted__c)) > NUMBER_OF_YEARS_BEFORE_STARTING_DUES_INCREASING &&
               customer.Program_Service_Revenue__c != null && customer.Program_Service_Revenue__c != 0;
    }
    
    private Boolean isMissingProgramServiceRevenueIncreaseNeeded(Account customer, List<NU__Membership__c> providerMemberships){
        return customer.Program_Service_Revenue__c == null || customer.Program_Service_Revenue__c == 0;
    }
    
    private Boolean isNotProviderServiceFee(NU__MembershipTypeProductLink__c mtpl){
        return mtpl != null && mtpl.Provider_Service_Fee__c == false;
    }
    
    private Boolean excludeServiceFee(NU__MembershipTypeProductLink__c mtpl){
        return mtpl != null && mtpl.Exclude_Provider_Service_Fee__c == true;
    }
    
    private Boolean isProviderServiceFee(NU__MembershipTypeProductLink__c mtpl){
        return mtpl != null && mtpl.Provider_Service_Fee__c == true;
    }
    
    private Boolean isStartDateBeforeJan2014AndDuesPriceExceedingOldDuesPriceMax(NU.ProductPricingInfo ppi, Decimal duesPrice){
        return ppi != null &&
               ppi.StartDate < JAN_1_2014 &&
               duesPrice > BEFORE_2014_MAX_PROVIDER_DUES_PRICE;
    }
    
    private Boolean isStartDateBeforeJan2015AndDuesPriceExceedingOldDuesPriceMax(NU.ProductPricingInfo ppi, Decimal duesPrice){
        return ppi != null &&
               ppi.StartDate < JAN_1_2015 &&
               duesPrice > AFTER_2013_MAX_PROVIDER_DUES_PRICE;
    }
    
    private Boolean isDuesPriceExceedingYearCap(NU.ProductPricingInfo ppi, Decimal duesPrice){
        //Update Year and Max Price each year
        return ppi != null &&
               ppi.StartDate >= JAN_1_2015 &&
               duesPrice > FOR_2015_MAX_PROVIDER_DUES_PRICE;
    }
    
    private Decimal calculateOutOfDateProgramServiceRevenueDuesIncrease(Account customer, List<NU__Membership__c> providerMemberships, Decimal currentDuesPrice, Boolean IsFutureCalc){
        system.debug('  calculateOutOfDateProgramServiceRevenueDuesIncrease::customer ' + customer);
        system.debug('  calculateOutOfDateProgramServiceRevenueDuesIncrease::providerMemberships ' + providerMemberships);
        system.debug('  calculateOutOfDateProgramServiceRevenueDuesIncrease::currentDuesPrice ' + currentDuesPrice);
        
        Decimal duesIncrease = currentDuesPrice;
        
        if (NU.CollectionUtil.listHasElements(providerMemberships)){
            NU__Membership__c lastYearsProviderMembership = getLastYearsMembership(providerMemberships, IsFutureCalc);
            
            system.debug('  calculateOutOfDateProgramServiceRevenueDuesIncrease::lastYearsProviderMembership ' + lastYearsProviderMembership);
            
            Double rate = 1.0 + INCREASE_RATE;
            
            duesIncrease = calculateFutureValue(lastYearsProviderMembership.NU__Amount__c, rate, 1);
        }
        
        return duesIncrease;
    }
    
    private Decimal calculateMissingProgramServiceRevenueDuesIncrease(Account customer, List<NU__Membership__c> providerMemberships, Decimal currentDuesPrice, Boolean IsFutureCalc){
        system.debug('  calculateMissingProgramServiceRevenueDuesIncrease::customer ' + customer);
        system.debug('  calculateMissingProgramServiceRevenueDuesIncrease::providerMemberships ' + providerMemberships);
        system.debug('  calculateMissingProgramServiceRevenueDuesIncrease::currentDuesPrice ' + currentDuesPrice);
        
        Decimal duesIncrease = currentDuesPrice;
        
        if (NU.CollectionUtil.listHasElements(providerMemberships)){
            NU__Membership__c lastYearsProviderMembership = getLastYearsMembership(providerMemberships, IsFutureCalc);
            
            system.debug('  calculateMissingProgramServiceRevenueDuesIncrease::lastYearsProviderMembership ' + lastYearsProviderMembership);

            Double rate = 1.0 + INCREASE_RATE;

            duesIncrease = calculateFutureValue(lastYearsProviderMembership.NU__Amount__c, rate, 1);
        }

        return duesIncrease;
    }
    
    private Decimal calculateFutureValue(Decimal presentDuesPrice, Double increaseRate, Integer numberOfYearsToIncreaseBy){
        system.debug('  calculateFutureValue::presentDuesPrice ' + presentDuesPrice);
        system.debug('  calculateFutureValue::increaseRate ' + increaseRate);
        system.debug('  calculateFutureValue::numberOfYearsToIncreaseBy ' + numberOfYearsToIncreaseBy);
        
        // Basic Future value equation
        // Future Value = Present Value * (1 + rate) ^ (number of periods)
        Decimal duesIncrease = presentDuesPrice * Math.pow (increaseRate, (Double) numberOfYearsToIncreaseBy);
        
        system.debug('  calculateFutureValue::duesIncrease ' + duesIncrease);
        
        return duesIncrease.setScale(2);
    }
    
    private NU__Membership__c getLastYearsMembership(List<NU__Membership__c> providerMemberships, Boolean IsFutureCalc){
        NU__Membership__c lastYearMembership = providerMemberships[0];
        
        Date dToday = getToday(IsFutureCalc);
        Date dTodayLastYear = dToday.addYears(-1);
        Date dEnd = dToday; //Init with a value
        for(NU__Membership__c m : providerMemberships){
            dEnd = m.NU__StartDate__c.addYears(1).addDays(-1);
            if ((m.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME) && (dTodayLastYear >= m.NU__StartDate__c && dTodayLastYear <= dEnd)){
                lastYearMembership = m;
                break;
            }
        }
        
        return lastYearMembership;
        
        /*
        * Original implementation: Sort of see how it works, but prefer to pick by specific date, per new implementation above
        *
        NU__Membership__c lastYearMembership = providerMemberships[0];
        
        Integer numProviderMemberships = providerMemberships.size();
        
        for (Integer index = 1; index < numProviderMemberships; ++index){
            NU__Membership__c currentMembership = providerMemberships[index];
            
            if (lastYearMembership.NU__StartDate__c < currentMembership.NU__StartDate__c
                    && currentMembership.NU__MembershipType__r.Name == Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME){
                lastYearMembership = currentMembership;
            }
        }
        
        return lastYearMembership;
        */
    }
    
    private Boolean isStateDues(NU__MembershipTypeProductLink__c mtpl){
        return mtpl != null &&
               (mtpl.State_Dues__c == true);
    }
    
    public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
        if (productPricingRequest.MembershipTypeId == null){
            system.debug('   ProviderMembershipPricer:: Membership Type Id is null');
            return null;
        }
        
        if(customer.Billing_Type__c == null || customer.Billing_Type__c.containsIgnoreCase('Millage Standard') || customer.Billing_Type__c.containsIgnoreCase('Under Construction') || 
         customer.Billing_Type__c.containsIgnoreCase('Corporate Program') || customer.Billing_Type__c.containsIgnoreCase('Non-Joint')  || customer.Billing_Type__c.containsIgnoreCase('Trial'))
		{
		        Boolean IsFutureCalc = false;
		        
		        if (extraParams.containsKey('FutureCalculationRequested')){
		        	IsFutureCalc = (boolean)extraParams.get('FutureCalculationRequested');
		        }
		        
		    	NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		        
		        NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(membershipType, productToPrice);
		        
		        system.debug('   ProviderMembershipPricer::mtpl ' + mtpl);
		        
		        if (isPrimaryMembershipProductOfMembership(membershipType, productToPrice, Constant.PROVIDER_MEMBERSHIP_TYPE_NAME) == false &&
		            isNotProviderServiceFee(mtpl)){
		            system.debug('   ProviderMembershipPricer:: Is Not Primary Membership Product Of Membership');
		            
		            return null;
		        }
		        
		        if (isStateDues(mtpl)){
		            system.debug('   ProviderMembershipPricer:: Is State Dues');
		            
		            return null;
		        }
		        
		        Decimal providerDuesPrice = customer.Dues_Price__c;
		        
		        system.debug('   ProviderMembershipPricer:: providerDuesPrice' + providerDuesPrice);
		        
		        NU.ProductPricingInfo ppi = findMembershipProductPricingInfo(productToPrice.Id, productPricingRequest);
		        
		        if (isStartDateBeforeJan2014AndDuesPriceExceedingOldDuesPriceMax(ppi, providerDuesPrice)){
		            providerDuesPrice = BEFORE_2014_MAX_PROVIDER_DUES_PRICE;
		        }
		        else if (isStartDateBeforeJan2015AndDuesPriceExceedingOldDuesPriceMax(ppi, providerDuesPrice)){
		            providerDuesPrice = AFTER_2013_MAX_PROVIDER_DUES_PRICE;
		        }
		        
		        // Reinstated Member
		        List<NU__Membership__c> providerMemberships = (List<NU__Membership__c>) extraParams.get('AccountMemberships');
		        List<NU__Membership__c> jointProviderMemberships = (List<NU__Membership__c>) extraParams.get('AccountProviderMemberships');
		        
		        if (jointProviderMemberships != null){
		            providerMemberships = jointProviderMemberships;
		        }
		        
		        
		        if (isReinstatingMember(customer, providerMemberships, IsFutureCalc) &&
		            isReinstatingAfter5Years(customer, IsFutureCalc)){
		            
		            providerDuesPrice = providerDuesPrice * AFTER_FIVE_YEARS_REINSTATEMENT_DISCOUNT;
		        }
		        
		        if (wasUnderConstructionLastMembershipYear(customer, providerMemberships, IsFutureCalc) && customer.Under_Construction__c == false){
		            providerDuesPrice = providerDuesPrice * UNDER_CONSTRUCTION_FIRST_FULL_YEAR_DISCOUNT;
		        }
		        
		        Boolean isOutOfDatePSR = isOutOfDateProgramServiceRevenueIncreaseNeeded(ppi, customer, IsFutureCalc);
		        Boolean isMissingPSR = isMissingProgramServiceRevenueIncreaseNeeded(customer, providerMemberships);
		        
		        system.debug('   ProviderMembershipPricer:: isOutOfDatePSR ' + isOutOfDatePSR);
		        system.debug('   ProviderMembershipPricer:: isMissingPSR ' + isMissingPSR);
		        
		        if (providerDuesPrice > MIN_DUES && isOutOfDatePSR){
		            providerDuesPrice = calculateOutOfDateProgramServiceRevenueDuesIncrease(customer, providerMemberships, providerDuesPrice, IsFutureCalc);
		        }
		        else if (Math.floor(providerDuesPrice) <= 0 && isMissingPSR){
		            providerDuesPrice = calculateMissingProgramServiceRevenueDuesIncrease(customer, providerMemberships, providerDuesPrice, IsFutureCalc);
		        }
		        
		        if (Math.floor(providerDuesPrice) <= 0 && customer.Dues_Price__c != null){
		            providerDuesPrice = customer.Dues_Price__c;
		        }
		        
		        // If there are no membership history record, the account is consider a new member, prorating rules apply
		        if (isNewByMembershipCount(customer, providerMemberships, true, IsFutureCalc)) {
		            system.debug('   ProviderMembershipPricer:: Beginning Join Proration Logic ');
		            system.debug('   ProviderMembershipPricer:: providerDuesPrice ' + providerDuesPrice);
		            
		            system.debug('   ProviderMembershipPricer:: StartDate ' + ppi.StartDate);
		            system.debug('   ProviderMembershipPricer:: EndDate ' + ppi.EndDate);
		            system.debug('   ProviderMembershipPricer:: JoinDate ' + ppi.JoinDate);
		
		            // already queried the product in bulk?
		            if (ppi != null &&
		                ppi.StartDate != null &&
		                ppi.EndDate != null &&
		                ppi.JoinDate != null) {
		
		                // New members who join in the 1st half of the dues year (E.g. between January 1 and June 30) will pay 50% of their first year's membership dues (calculated above). 
		                if(ppi.StartDate <= ppi.JoinDate && ppi.JoinDate < ppi.StartDate.addMonths(6))
		                {
		                    providerDuesPrice = providerDuesPrice * NEW_MEMBER_JOIN_DISCOUNT;
		                    JointBillingUtil.ProviderJoinProrated = true;
		                }
		                // New membership who join in the 2nd half of the dues year (E.g. between July 1 and December 31) will receive complimentary membership.
		                else if(ppi.StartDate.addMonths(6) <= ppi.JoinDate && ppi.JoinDate <= ppi.EndDate) 
		                {
		                    providerDuesPrice = NEW_MEMBER_JOIN_SECOND_HALF_OF_YEAR_PRICE;
		                }
		            }
		        }
		        else {
		            if (didStartSecondHalfOfLastYear(customer, providerMemberships, IsFutureCalc)){
		                providerDuesPrice = providerDuesPrice * JOINED_2ND_HALF_LAST_YEAR_FIRST_FULL_YEAR_DISCOUNT;
		            }
		        }
		        
		        
		        if (excludeServiceFee(mtpl)){
		            system.debug('   ProviderMembershipPricer::calculating exclude Service Fee. Before Provider Dues Price is ' + providerDuesPrice);
		            
		            providerDuesPrice = providerDuesPrice * (1 - Constant.PROVIDER_SERVICE_FEE_PERCENTAGE);
		            
		            system.debug('   ProviderMembershipPricer::calculating exclude Service Fee. After Provider Dues Price is ' + providerDuesPrice);
		        }
		        
		        if (isProviderServiceFee(mtpl)){
		            providerDuesPrice = providerDuesPrice * Constant.PROVIDER_SERVICE_FEE_PERCENTAGE;
		        }
		        
		        if (isStartDateBeforeJan2014AndDuesPriceExceedingOldDuesPriceMax(ppi, providerDuesPrice)){
		            providerDuesPrice = BEFORE_2014_MAX_PROVIDER_DUES_PRICE;
		        }
		        else if (isStartDateBeforeJan2015AndDuesPriceExceedingOldDuesPriceMax(ppi, providerDuesPrice)){
		            providerDuesPrice = AFTER_2013_MAX_PROVIDER_DUES_PRICE;
		        }
		        
		        if (isDuesPriceExceedingYearCap(ppi, providerDuesPrice)){
		            providerDuesPrice = FOR_2015_MAX_PROVIDER_DUES_PRICE;
		        }
		        
		        
		        //Should probably add this 'if' to MembershipPricerBase as a method at some point?
		        if (customer != null && customer.Dues_Price_Override__c != null && customer.Dues_Price_Override__c > 0){
		        	//If we have a dues override amount...
		        	if (extraParams.containsKey('FutureCalculationRequested')){
			        	if (IsFutureCalc == false) {
			        		//...and it's NOT a future calculation, then apply the override!
			        		providerDuesPrice = customer.Dues_Price_Override__c;
			        	}
		        	}
		        	else {
		        		//...and it's not clear if it's a future calc or not, then just apply the override anyway.
		        		providerDuesPrice = customer.Dues_Price_Override__c;
		        	}
		        }
		        		        
		        return providerDuesPrice;
		   }
		else
		{
			return null;
		}
    }
}