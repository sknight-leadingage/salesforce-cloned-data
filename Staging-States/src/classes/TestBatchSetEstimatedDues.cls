@isTest

private class TestBatchSetEstimatedDues {
	
	public static Joint_Billing__c runJointBilling(Id statePartnerId, Date endDate){
		return runJointBilling(statePartnerId, endDate, null);
	}
	
	static Joint_Billing__c runJointBilling(Id statePartnerId, Date endDate, Date joinDate){	
		List<Joint_Billing__c> jbs = runJointBillings(new Set<Id>{ statePartnerId }, endDate, joinDate);
    	
    	return jbs[0];
	}
	
	static List<Joint_Billing__c> runJointBillings(Set<Id> statePartnerIds, Date endDate){
		return runJointBillings(statePartnerIds, endDate, null);
	}
	
	static List<Joint_Billing__c> runJointBillings(Set<Id> statePartnerIds, Date endDate, Date joinDate){
		List<Joint_Billing__c> jointBillings = new List<Joint_Billing__c>();
		

		JointBillingUtil.ProviderJoinOnToUseForTesting = joinDate;
		
		for (Id statePartnerId : statePartnerIds){
			Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling(statePartnerId, endDate);
			jointBillings.add(jb);
			
	    	JointStateProviderBiller jspb = new JointStateProviderBiller(jb);
	    	
	    	Database.executeBatch(jspb);
		}
		

		
		return jointBillings;
	}
	

	static Date calculatePreviousYearEndDate(){
		return Date.newInstance(Date.Today().year() - 1, 12, 31);
	}


    static testmethod void PerformTest() {
    	Test.startTest();

    	
    	Account nonCorporateMSO = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	Account statePartner1 = DataFactoryAccountExt.insertStatePartnerAccount();
 		statePartner1.Dues_Billing_Quarter__c = 1.0;
 		update statePartner1;
 
    	
    	Account provider1 = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(10000000, statePartner1.Id);
    	provider1.Estimated_Dues_Pro_Forma__c = 0;
    	
        NU__MembershipType__c StateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        NU__Membership__c m1 = NU.DataFactoryMembership.createMembership(provider1.Id, StateProviderMT.Id, date.newInstance(date.today().year(), 1, 1), date.newInstance(date.today().year(), 12, 31));
        insert m1;    	

    	NU.DataFactoryAffiliation.insertAffiliation(provider1.Id, statePartner1.Id, true);

    	
    	Date endDate = calculatePreviousYearEndDate();
    	Date joinDate = endDate.addDays(30);
    	
    	List<Joint_Billing__c> jbs = runJointBillings(new Set<Id>{ statePartner1.Id }, endDate, joinDate);
    	
        update provider1;
        
		Account a = [SELECT Id, Estimated_Dues_Pro_Forma__c FROM Account WHERE Id = :provider1.Id];
		DataFactoryJointBilling.createJointBilling(statePartner1.Id, Date.newInstance(Date.Today().year(), 12, 31));
		Decimal dt = 0;
		Account ab = [SELECT Id, Estimated_Dues_Pro_Forma__c FROM Account WHERE Id = :provider1.Id];
		System.assertEquals(dt, ab.Estimated_Dues_Pro_Forma__c);
		
		BatchSetEstimatedDues c = new BatchSetEstimatedDues();
		Database.executeBatch(c);
		Test.stopTest();
		
        Account ac = [SELECT Id, Estimated_Dues_Pro_Forma__c FROM Account WHERE Id = :a.Id];
		System.assertNotEquals(dt, ac.Estimated_Dues_Pro_Forma__c);
    }
    
    static testmethod void PerformTestOnCalculatedDuesAmount() {
    	Test.startTest();

    	
    	Account nonCorporateMSO = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	Account statePartner1 = DataFactoryAccountExt.insertStatePartnerAccount();
 		statePartner1.Dues_Billing_Quarter__c = 1.0;
 		update statePartner1;
 
    	
    	Account provider1 = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(10000000, statePartner1.Id);
    	provider1.Calculated_Dues_Amount__c = 0;
    	
        NU__MembershipType__c StateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        NU__Membership__c m1 = NU.DataFactoryMembership.createMembership(provider1.Id, StateProviderMT.Id, date.newInstance(date.today().year(), 1, 1), date.newInstance(date.today().year(), 12, 31));
        insert m1;

    	NU.DataFactoryAffiliation.insertAffiliation(provider1.Id, statePartner1.Id, true);

    	
    	Date endDate = calculatePreviousYearEndDate();
    	Date joinDate = endDate.addDays(30);
    	
    	List<Joint_Billing__c> jbs = runJointBillings(new Set<Id>{ statePartner1.Id }, endDate, joinDate);
    	
        update provider1;

		Account a = [SELECT Id, Calculated_Dues_Amount__c FROM Account WHERE Id = :provider1.Id];
		DataFactoryJointBilling.createJointBilling(statePartner1.Id, Date.newInstance(Date.Today().year(), 12, 31));
		Decimal dt = 0;
		Account ab = [SELECT Id, Calculated_Dues_Amount__c FROM Account WHERE Id = :provider1.Id];
		System.assertEquals(dt, ab.Calculated_Dues_Amount__c);
       
		JointBillingSetCalculatedDues c = new JointBillingSetCalculatedDues();
		Database.executeBatch(c);
		Test.stopTest();

        Account ac = [SELECT Id, Calculated_Dues_Amount__c FROM Account WHERE Id = :a.Id];
		System.assertNotEquals(dt, ac.Calculated_Dues_Amount__c);
    }
}