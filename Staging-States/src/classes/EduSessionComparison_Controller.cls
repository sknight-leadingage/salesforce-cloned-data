public with sharing class EduSessionComparison_Controller extends EduReportsControllerBase {
    public List<Conference_Session_Speaker__c> CEList { get;set; }
    
    public integer CEListSize { get; set; }
    
    public override string getPageTitle() {
        return 'Session Comparison by Company';
    }
    
    public EduSessionComparison_Controller() {
        SetQuery();
    }
    
    public void SetQuery() {
        CEListSize = 0;
        GroupHeaderController.GroupTotals = new Map<string,integer>();
        List<AggregateResult> Totals = [SELECT COUNT(Session__r.Session_Number_Numeric__c) sc, Speaker__r.NU__PrimaryAffiliation__r.Name afn FROM Conference_Session_Speaker__c WHERE (Role__c = 'Key Contact' OR Role__c = 'Speaker') AND Session__r.Conference__c = :EventId GROUP BY Speaker__r.NU__PrimaryAffiliation__r.Name ORDER BY Speaker__r.NU__PrimaryAffiliation__r.Name];
        string th = '';
        for (AggregateResult ar : Totals ) {
            th = (string)ar.get('afn');
            if (th == null) {
                th = '(Blank)';
            }
            CEListSize++;
            GroupHeaderController.GroupTotals.put(th, (integer)ar.get('sc'));
        }
        CEList = [SELECT Session__r.Session_Number__c, Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Title__c, Session__r.Proposal__r.Proposal_Number__c, Speaker__r.NU__PrimaryAffiliation__r.Name FROM Conference_Session_Speaker__c WHERE (Role__c = 'Key Contact' OR Role__c = 'Speaker') AND Session__r.Conference__c = :EventId ORDER BY Speaker__r.NU__PrimaryAffiliation__r.Name, Session__r.Session_Number_Numeric__c];
    }
}