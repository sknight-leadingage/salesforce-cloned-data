/*
* Test methods for the LeadershipAcademyReports_Controller
*/
@isTest(SeeAllData=true)

private class TestLeadershipAcademyReports_Controller{

    static LeadershipAcademyReports_Controller loadController(){
        PageReference pageRef = Page.LeadershipAcademyReports;
        Test.setCurrentPage(pageRef);
        
        string SelectedAcademyYear = '2016';
        
        pageRef.getParameters().put(LeadershipAcademyControllerBase.ACADEMY_YEAR_PARAM, SelectedAcademyYear);
        
        LeadershipAcademyReports_Controller AppController = new LeadershipAcademyReports_Controller();
        
        return AppController;
     }
    
     static testmethod void DisplayReportsPage(){
        LeadershipAcademyReports_Controller AppController = loadController();
        system.assertEquals('2016', appController.AcademyYear);
    }
   
}