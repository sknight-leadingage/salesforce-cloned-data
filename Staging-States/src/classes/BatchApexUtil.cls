public with sharing class BatchApexUtil {
    public static void sendWarningEmailDuringJobWithIncorrectData(Id apexJobId, string warningReason, List<Account> badData) {
        AsyncApexJob job = getAsyncApexJobById(apexJobId);
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        List<String> toAddresses = new List<String> { 
            job.CreatedBy.Email,
            'dev+batchJobError@nimbleams.com',
            'webtester@leadingage.org'
        };
       
        mail.setToAddresses(toAddresses);
        mail.setSubject( job.ApexClass.Name + ' Job Finished With Warnings' );
        mail.setPlainTextBody( getFormattedWarnings(warningReason, badData) + '\n\n' + getErrorBody(job) );

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private static string getFormattedWarnings(string warningReason, List<Account> badData) {
        string s = warningReason + '\n\n';
        for(Account a : badData) {
            s = s + '    Account: ' + (string)a.Name + ' (' +  (string)a.Id + ')\n\n';
        }
        return s;
    }
    
    public static void sendWarningEmailDuringJobWithIncorrectData(Id apexJobId, string warningReason, List<NU__Membership__c> badData) {
        AsyncApexJob job = getAsyncApexJobById(apexJobId);
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        List<String> toAddresses = new List<String> { 
            job.CreatedBy.Email,
            'dev+batchJobError@nimbleams.com',
            'webtester@leadingage.org'
        };
       
        mail.setToAddresses(toAddresses);
        mail.setSubject( job.ApexClass.Name + ' Job Finished With Warnings' );
        mail.setPlainTextBody( getFormattedWarnings(warningReason, badData) + '\n\n' + getErrorBody(job) );

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private static string getFormattedWarnings(string warningReason, List<NU__Membership__c> badData) {
        string s = warningReason + '\n\n';
        for(NU__Membership__c m : badData) {
            s = s + '    Account Id: ' + (string)m.NU__Account__c + ' (Membership Record: ' +  (string)m.Id + ')\n\n';
        }
        return s;
    }

    public static void sendErrorEmailWhenJobFinishesWithErrors(Id apexJobId){
        AsyncApexJob job = getAsyncApexJobById(apexJobId);
        
        if (job.NumberOfErrors == 0){
            return;
        }
        
        sendErrorEmail(job);
    }

    private static void sendErrorEmail(AsyncApexJob jobWithErrors){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        
        List<String> toAddresses = new List<String> { 
            jobWithErrors.CreatedBy.Email,
            'dev+batchJobError@nimbleams.com',
            'webtester@leadingage.org'
        };
       
        mail.setToAddresses(toAddresses);

        mail.setSubject( jobWithErrors.ApexClass.Name + ' Job Finished With Errors' );
       
        mail.setPlainTextBody( getErrorBody(jobWithErrors) );
       
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private static String getErrorBody(AsyncApexJob jobWithErrors){
        String errorBody = 'The ' + jobWithErrors.ApexClass.Name + ' batch apex job processed '
            + jobWithErrors.TotalJobItems + ' batch(es) with '+ jobWithErrors.NumberOfErrors 
            + ' failed batch(es).\n\n';
        
        errorBody += '   Error Message: ' + jobWithErrors.ExtendedStatus + '\n\n';
        
        errorBody += '   Status: ' + jobWithErrors.Status + '\n';
        errorBody += '   Apex Class Name: ' + jobWithErrors.ApexClass.Name + '\n';
        errorBody += '   Apex Class Namespace Prefix: ' + jobWithErrors.ApexClass.NamespacePrefix + '\n';
        errorBody += '   Created: ' + jobWithErrors.CreatedDate.format() + '\n';
        if (jobWithErrors.CompletedDate != null) {
            errorBody += '   Completed: ' + jobWithErrors.CompletedDate.format() + '\n\n';
        }
        
        errorBody += '   Org: ' + UserInfo.getOrganizationName() + ' (' + UserInfo.getOrganizationId() + ')\n';
        errorBody += '   Current User: ' + UserInfo.getUserName() + ' (' + UserInfo.getUserEmail() + ')\n'; 
        
        return errorBody;
    }
    
    private static AsyncApexJob getAsyncApexJobById(Id apexJobId){
        return [select Id,
                       CompletedDate,
                       CreatedDate,
                       ExtendedStatus,
                       JobType,
                       LastProcessed,
                       Status,
                       NumberOfErrors,
                       JobItemsProcessed,
                       TotalJobItems,
                       ApexClass.Name,
                       ApexClass.NamespacePrefix,
                       CreatedBy.Email
                  from AsyncApexJob
                 where Id = :apexJobId];
    }
}