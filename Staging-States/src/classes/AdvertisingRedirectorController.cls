public with sharing class AdvertisingRedirectorController {
	private Id objectId = null;
	private Boolean IsNew { get { return objectId == null;} }
    
	// if the id is set, then we're editing the record, not creating a new one
	public AdvertisingRedirectorController(ApexPages.StandardController standardController){
    	if (standardController != null){
        	objectId = standardController.getId();
    	}
	}
    
	public virtual PageReference redirect(){
    	if (IsNew){
        	return getNewRedirect();
    	}
   	 
    	return getEditRedirect();
	}
    
	// for creating a new custom record, redirect to the custom page in the order process. Also sets the return URL.
	public PageReference getNewRedirect(){
    	PageReference ref = Page.OrderAdvertisingProducts;
    	ref.getParameters().put('ret', '/' + Schema.SObjectType.Advertising__c.getKeyPrefix() + '/o');
    	return ref;
	}

	// for editing a custom record, redirect to the custom page in the order process, modifying the order item that is assigned to the record.
	public PageReference getEditRedirect(){
    	PageReference ref = Page.OrderAdvertisingProducts;
    	Id orderItemId = null;
   	 
    	if (this.objectId != null) {
        	Advertising__c advertising = [SELECT
            	Order_Item_Line__r.NU__OrderItem__c,
            	Order_Item_Line__r.NU__OrderItem__r.NU__Status__c,
            	Order_Item_Line__r.NU__OrderItem__r.NU__Balance__c
            	FROM Advertising__c WHERE Id = :objectId];
        	orderItemId = advertising.Order_Item_Line__r.NU__OrderItem__c;
       	 
        	// if the status of the order item is cancelled, and we owe them a refund, redirect to the payment/refund page of the order process.
        	if (advertising.Order_Item_Line__r.NU__OrderItem__r.NU__Status__c == 'Cancelled' &&
            	advertising.Order_Item_Line__r.NU__OrderItem__r.NU__Balance__c < 0)
			{
            	ref = Page.NU__OrderPayment;
        	}
    	}
   	 
    	Map<String, String> params = ref.getParameters();
    	params.put('orderItemId', orderItemId);
   	 
    	return ref;
	}
}