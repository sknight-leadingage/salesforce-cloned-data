public with sharing class HardshipPricer extends MembershipPricerBase implements IProductPricer {


    
     public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){

		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(membershipType, productToPrice);
		
		//poduct type should be membership
		if (productPricingRequest.MembershipTypeId == null){
			system.debug('   HardshipPricer:: Membership Type Id is null');
			return null;
		}
		

		
		if(customer.Billing_Type__c == null) {
			system.debug('   HardshipPricer:: Billing Type not set');
			return null;
		}
           
	           
		if(customer.Billing_Type__c.containsIgnoreCase('Hardship $0'))
			{
			   return 0;
			}
	      else if(customer.Billing_Type__c.containsIgnoreCase('Hardship 50%'))
	         {
	           decimal Price = customer.Dues_Price__c;
			   system.debug('   HardshipPricer:: Price is ' + Price);
		       
		       if(Price == null )
		       {
		       
		       return null;
		       }
		       else if(Price >= 500)
		       {
			       
		           return (Price / 2);
		       }
		       else 
		       {
		       	 return 350;
		       }
	         }
	         else
	         {
	         	return null;
	         }
     }
}