public with sharing class ConvertDeal {

	private Map<String, IDealConverter> DealConverters = new Map<String, IDealConverter>{ 
		'Membership' => new MembershipDealConverter(),
		'Donation' => new DonationDealConverter(),
		'Sponsorship' => new SponsorshipDealConverter(),
		'Advertising' => new AdvertisingDealConverter(),
		'Exhibitor' => new ExhibitorDealConverter()
	}; 


    public ConvertDeal(ApexPages.StandardController controller) {

    }
    
    public PageReference redirect() {
        PageReference ref = Page.ConvertDeal;
        List<NU__Deal__c> deals = new List<NU__Deal__c>();
        NU__Deal__c deal = null;
        List<DealItem__c> dealItems = new List<DealItem__c>();
        
        System.SavePoint sp = Database.setSavepoint();
        
        Id dealId;
        try {
            dealId = ApexPages.currentPage().getParameters().get('dealId');
            deals = [SELECT Id,
			                RecordType.Name,
			                NU__BillTo__c,
			                //NU__BillTo__r.NU__JoinOn__c,
			                NU__DealStatus__c,
			                Membership_Type__c,
			                Membership_Type__r.Id,
			                Membership_Type__r.NU__AccountJoinOnField__c,
			                Event__c,
			                NU__Stage__c,
			                Order__c,
			                NU__StartDate__c,
			                NU__EndDate__c
                	   FROM NU__Deal__c
                      WHERE Id = :dealId];
                
            dealItems = [SELECT Id,
                                Booked_By__c,
                                Booked_Date__c,
                                Booth_Number__c,
				                Product__c,
				                Product__r.Name,
				                Product__r.NU__Entity__c,
				                Product__r.Id,
				                Price__c,
				                Quantity__c,
				                Deal__c
			               FROM DealItem__c
			              WHERE Deal__c = :deals[0].Id];
        }
        catch (System.StringException e) {
            //
        }
        
        try{
	        if (deals.size() > 0) {
	        	deal = deals[0];
	        	
	            if (deal.NU__DealStatus__c != Constant.DEAL_STATUS_READY_FOR_CONVERT) { // should never really invalidate, since javascript on the button prevents this
	            	throw new ApplicationException('Only deals marked as "' + Constant.DEAL_STATUS_READY_FOR_CONVERT + '" can be converted.');
	            }
	            else if (deal.Order__c != null) { // should never really invalidate, since javascript on the button prevents this
	            	throw new ApplicationException('There is already an existing order for this deal. A deal may not be reconverted.');
	            }
	            else if (dealItems.size() == 0) { // should never really invalidate, since javascript on the button prevents this
	            	throw new ApplicationException('The deal must contain at least one deal item before it can be converted.');
	            }
	            else {	            	
	            	IDealConverter dealConverter = DealConverters.get(deal.RecordType.Name);
	            	
	            	ref = dealConverter.getOrderPurchasePageToRedirectTo();
	                
	                NU__Cart__c cart = new NU__Cart__c(
	                    NU__BillTo__c = deal.NU__BillTo__c,
	                    NU__TransactionDate__c = Date.today(),
	                    NU__Entity__c = dealItems[0].Product__r.NU__Entity__c,
	                    Deal__c = deal.Id
	                );
	                insert cart;
	                
	                // determine the price class - required
	                NU.PriceClassRequest pcr = new NU.PriceClassRequest();
	                pcr.TransactionDate = DateTime.now();
	                pcr.AccountId = deal.NU__BillTo__c;
	                
	                dealConverter.setPriceClassRequest(deal, pcr);
	                        
	                Map<String, NU__PriceClass__c> priceClassesByName = new Map<String, NU__PriceClass__c>();
	                for (NU__PriceClass__c priceClass : (new Map<Id, NU__PriceClass__c>([SELECT Name FROM NU__PriceClass__c])).values()) {
	                    priceClassesByName.put(priceClass.Name, priceClass);
	                }
	                NU__PriceClass__c priceClass = priceClassesByName.get(NU.PriceClassManager.getPricingManager().getPriceClass(pcr));
	                
	                Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
	                NU__CartItem__c cartItem = new NU__CartItem__c(
	                    NU__Cart__c = cart.Id,
	                    NU__Customer__c = deal.NU__BillTo__c,
	                    NU__PriceClass__c = priceClass.Id,
	                    RecordTypeId = rt.get(deal.RecordType.Name).getRecordTypeId()
	                );
	                
	                dealConverter.onBeforeCartItemInserted(deal, cartItem);
	                
	                insert cartItem;
	                
	                List<NU__CartItemLine__c> cartItemLinesToInsert = new List<NU__CartItemLine__c>();
	                
	                for (DealItem__c dealItem : dealItems) {
	                    NU__CartItemLine__c cartItemLine = new NU__CartItemLine__c(
	                        NU__CartItem__c = cartItem.Id,
	                        NU__IsInCart__c = true,
	                        NU__UnitPrice__c = dealItem.Price__c,
	                        NU__Product__c = dealItem.Product__c,
	                        NU__Quantity__c = dealItem.Quantity__c                            
	                    );
	                    
	                    dealConverter.onBeforeCartItemLineInserted(deal, dealItem, cartItemLine);

	                    cartItemLinesToInsert.add(cartItemLine);
	                }
	                
	                insert cartItemLinesToInsert;
	                
	                Map<string, string> params = ref.getParameters();
	                params.put('id', cart.Id);
	                params.put('cartItemId', cartItem.Id);
	                ref.setRedirect(true);
	                return ref;
	            }
	        }
	        else {
	        	throw new ApplicationException('No deal record was found.');
	        }
        }
        catch(Exception ex){
        	Database.rollback(sp);
        	
        	ApexPages.addMessages(ex);
        }
        
        return null;
    }
    
    private interface IDealConverter{
    	PageReference getOrderPurchasePageToRedirectTo();
    	
    	void onBeforeCartItemInserted(NU__Deal__c deal, NU__CartItem__c cartItemToInsert);
    	
    	void onBeforeCartItemLineInserted(NU__Deal__c deal, DealItem__c dealItem, NU__CartItemLine__c cartItemLineToInsert);
    	
    	void setPriceClassRequest(NU__Deal__c deal, NU.PriceClassRequest priceClassRequest);
    }
    
    private class MembershipDealConverter implements IDealConverter{
    	private Map<Id, NU__MembershipType__c> MembershipTypes = MembershipTypeQuerier.getAllMembershipTypesAndTheirActiveProductLinks();
    	
    	public PageReference getOrderPurchasePageToRedirectTo(){
    		return Page.NU__OrderMembershipProducts;
    	}
    	
    	public void onBeforeCartItemInserted(NU__Deal__c deal, NU__CartItem__c cartItemToInsert){
    		String joinOnField = deal.Membership_Type__r.NU__AccountJoinOnField__c;
    		
    		Date joinOn = null;
    		List<Account> billToAccount = Database.query('SELECT ' + joinOnField + ' FROM Account WHERE Id = \'' + deal.NU__BillTo__c + '\'');
    		if (billToAccount != null && billToAccount.size() > 0) {
    			joinOn = (Date)billToAccount.get(0).get(joinOnField);
    		}
    		
            if (joinOn == null) {
                joinOn = Date.today();
            }
            
            MembershipCartOrderData membershipData = new MembershipCartOrderData();
            membershipData.MembershipType = deal.Membership_Type__r.Id;
            membershipData.JoinOn = joinOn;
            membershipData.StartDate = deal.NU__StartDate__c;
            membershipData.EndDate = deal.NU__EndDate__c;
            cartItemToInsert.NU__Data__c = JSON.serialize(membershipData);
    	}
    	
    	public void onBeforeCartItemLineInserted(NU__Deal__c deal, DealItem__c dealItem, NU__CartItemLine__c cartItemLineToInsert){
    		NU__MembershipType__c mt = MembershipTypes.get(deal.Membership_Type__c);
    		
    		NU__MembershipTypeProductLink__c membershipTypeProductLink = null;
    		
            for (NU__MembershipTypeProductLink__c mtpl : mt.NU__MembershipTypeProductLinks__r) {
                if (mtpl.NU__Product__c == dealItem.Product__c && membershipTypeProductLink == null) { // if membershipTypeProductLink is not null, then we found more than one possible product link - bad
                    membershipTypeProductLink = mtpl;
                }
            }
            
            if (membershipTypeProductLink == null) { // needs to be a valid membership type product link found	                            
                throw new ApplicationException('No valid membership type product link found. Please make sure that there is a membership type product link for the supplied membership type, deal products, and stage.');
            }
            else {
                cartItemLineToInsert.NU__MembershipTypeProductLink__c = membershipTypeProductLink.Id;
            }
    	}
    	
    	public void setPriceClassRequest(NU__Deal__c deal, NU.PriceClassRequest priceClassRequest){
        	priceClassRequest.MembershipTypeId = deal.Membership_Type__r.Id;
    	}
    }
    
    private class DonationDealConverter implements IDealConverter{
    	public PageReference getOrderPurchasePageToRedirectTo(){
    		return Page.NU__OrderDonationProducts;
    	}
    	
    	public void onBeforeCartItemInserted(NU__Deal__c deal, NU__CartItem__c cartItemToInsert){
    		
    	}
    	
    	public void onBeforeCartItemLineInserted(NU__Deal__c deal, DealItem__c dealItem, NU__CartItemLine__c cartItemLineToInsert){
    		
    	}
    	
    	public void setPriceClassRequest(NU__Deal__c deal, NU.PriceClassRequest priceClassRequest){
    		
    	}
    }
    
    private class SponsorshipDealConverter implements IDealConverter{
    	public PageReference getOrderPurchasePageToRedirectTo(){
    		return Page.OrderSponsorshipProducts;
    	}
    	
    	public void onBeforeCartItemInserted(NU__Deal__c deal, NU__CartItem__c cartItemToInsert){
    		
    	}
    	
    	public void onBeforeCartItemLineInserted(NU__Deal__c deal, DealItem__c dealItem, NU__CartItemLine__c cartItemLineToInsert){
            SponsorshipCartItemLineData sponsorshipCartItemLineData = 
               new SponsorshipCartItemLineData
                 (deal.Event__c,
                  null,
                  null);

            cartItemLineToInsert.NU__Data__c = JSON.serialize(sponsorshipCartItemLineData);
    	}
    	
    	public void setPriceClassRequest(NU__Deal__c deal, NU.PriceClassRequest priceClassRequest){
    		
    	}
    }
    
    private class AdvertisingDealConverter implements IDealConverter{
    	public PageReference getOrderPurchasePageToRedirectTo(){
    		return Page.OrderAdvertisingProducts;
    	}
    	
    	public void onBeforeCartItemInserted(NU__Deal__c deal, NU__CartItem__c cartItemToInsert){
    		
    	}
    	
    	public void onBeforeCartItemLineInserted(NU__Deal__c deal, DealItem__c dealItem, NU__CartItemLine__c cartItemLineToInsert){

    	}
    	
    	public void setPriceClassRequest(NU__Deal__c deal, NU.PriceClassRequest priceClassRequest){
    		
    	}
    }
    
    private class ExhibitorDealConverter implements IDealConverter{
    	public PageReference getOrderPurchasePageToRedirectTo(){
    		return Page.OrderExhibitorProducts;
    	}
    	
    	public void onBeforeCartItemInserted(NU__Deal__c deal, NU__CartItem__c cartItemToInsert){
    		
    	}
    	
    	public void onBeforeCartItemLineInserted(NU__Deal__c deal, DealItem__c dealItem, NU__CartItemLine__c cartItemLineToInsert){
    		cartItemLineToInsert.Booked_By__c = dealItem.Booked_By__c;
    		cartItemLineToInsert.Booked_Date__c = dealItem.Booked_Date__c;
    		cartItemLineToInsert.Booth_Number__c = dealItem.Booth_Number__c;
    	}
    	
    	public void setPriceClassRequest(NU__Deal__c deal, NU.PriceClassRequest priceClassRequest){
    		
    	}
    }
    
    private class MembershipCartOrderData {
        public Id MembershipType { get; set; }
        public Date JoinOn { get; set; }
        public Date StartDate { get; set; }
        public Date EndDate { get; set; }
    }
    
    private class SubscriptionCartItemLineData {
        public Date StartDate { get; set; }
        public Date EndDate { get; set; }
    }
}