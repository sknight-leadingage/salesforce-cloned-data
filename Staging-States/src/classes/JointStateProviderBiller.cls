/**
 * @author NimbleUser
 * @date Updated: 2/22/13
 * @description This class runs the joint state provider billing for a given state association or MultiSite Org with joint billing.
 */
global class JointStateProviderBiller implements Database.Batchable<sObject>, Database.Stateful {
	public static final String NULL_JOINT_BILLING_VAL_MSG = 'The joint billing to run is null.';
	public static final String JOINT_BILLING_ALREADY_RUN_VAL_MSG = 'The joint billing to run has already been run.';
	public static final String JOINT_BILLING_BILL_DATE_REQUIRED_VAL_MSG = 'The bill date is required.';
	public static final String JOINT_BILLING_START_DATE_REQUIRED_VAL_MSG = 'The start date is required.';
	public static final String JOINT_BILLING_END_DATE_REQUIRED_VAL_MSG = 'The end date is required.';
	public static final String JOINT_BILLING_START_DAY_NOT_FIRST_OF_MONTH_VAL_MSG = 'The start date must be the first of the month.';
	public static final String JOINT_BILLING_END_DATE_NOT_LAST_DAY_OF_MONTH_VAL_MSG = 'The end date must be the last day of the month.';
	public static final String JOINT_BILLING_START_DATE_NOT_AFTER_END_DATE_VAL_MSG = 'The start date must be after the end date.';
	
	Account StateProvider { get; set; }
	
	Joint_Billing__c JointBilling { get; set; }
	
	NU__MembershipType__c JointStateProviderMembershipType { get; set; }
	
	Id JointStateProviderProductId { get; set; } 
	
	global JointStateProviderBiller(Joint_Billing__c jointBillingToRun){
		runJointStateProviderBiller(jointBillingToRun, true);
	}
	
	global JointStateProviderBiller(Joint_Billing__c jointBillingToRun, Boolean enforceNew){
		runJointStateProviderBiller(jointBillingToRun, enforceNew);
	}
	
	private void runJointStateProviderBiller(Joint_Billing__c jointBillingToRun, Boolean enforceNew){

		if (jointBillingToRun == null){
			throw new ApplicationException(NULL_JOINT_BILLING_VAL_MSG);
		}
		
		if (jointBillingToRun.Id != null && enforceNew == true){
			throw new ApplicationException(JOINT_BILLING_ALREADY_RUN_VAL_MSG);
		}
		
		StateProvider = getStateProviderAccount(jointBillingToRun.State_Partner__c);
		
		if (StateProvider == null){
			String noStateProviderValMsg = getNoStatePartnerValMsg(jointBillingToRun);
			throw new ApplicationException(noStateProviderValMsg);
		}
		
		if (StateProvider.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID &&
		    StateProvider.RecordTypeId != Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
		    StateProvider.Multi_Site_Bill_Jointly__c != true){
			String notStateProviderValMsg = getNotStateProviderValMsg(StateProvider);
			throw new ApplicationException(notStateProviderValMsg);
		}

		if (jointBillingToRun.Bill_Date__c == null){
			throw new ApplicationException(JOINT_BILLING_BILL_DATE_REQUIRED_VAL_MSG);
		}
		
		if (jointBillingToRun.Start_Date__c == null){
			throw new ApplicationException(JOINT_BILLING_START_DATE_REQUIRED_VAL_MSG);
		}
		
		if (jointBillingToRun.End_Date__c == null){
			throw new ApplicationException(JOINT_BILLING_END_DATE_REQUIRED_VAL_MSG);
		}
		
		if (jointBillingToRun.Start_Date__c.day() != 1){
		    throw new ApplicationException(JOINT_BILLING_START_DAY_NOT_FIRST_OF_MONTH_VAL_MSG);
	    }
	    
    	Date lastDayOfMonth = 
    	  Date.newInstance(jointBillingToRun.End_Date__c.year(),
    	                   jointBillingToRun.End_Date__c.month() + 1,
    	                   1).addDays(-1);

		if (jointBillingToRun.End_Date__c != lastDayOfMonth){
			throw new ApplicationException(JOINT_BILLING_END_DATE_NOT_LAST_DAY_OF_MONTH_VAL_MSG);
		}
	    
	    if (jointBillingToRun.Start_Date__c <= jointBillingToRun.End_Date__c){
	        throw new ApplicationException(JOINT_BILLING_START_DATE_NOT_AFTER_END_DATE_VAL_MSG);
        }
		
		
		JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
		
		if (JointStateProviderMembershipType != null){
			JointStateProviderProductId = JointBillingUtil.getJointStateProviderProductId(JointStateProviderMembershipType);
		}
		
		JointBilling = jointBillingToRun;
	}
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		Id statePartnerAccountId = JointBilling.State_Partner__c;
		Date providerMemberThru = JointBilling.End_Date__c;
		

		
		// State Partner query
		// Get all State Partner Primary Affiliation Business Accounts whose provider membership
		// ends on the specified end date or is a new provider.
		String query = 'Select id, '
		             + 'Name, '
		             + 'Multi_Site_Program_Service_Revenue__c, '
		             + 'Multi_Site_Dues_Price__c, '
		             + 'Multi_Site_Corporate__c, '
		             + 'Program_Service_Revenue__c, '
		             + 'Revenue_Year_Submitted__c, '
		             + 'Provider_Join_On__c, '
		             + 'Dues_Price__c, '
		             + 'Provider_Membership__c, '
                     + 'Provider_Membership__r.NU__EndDate__c,'
		             + 'RecordTypeId,'
		             + 'Billing_Type__c '
		             + 'from Account '
		             + 'where State_Partner_ID__c = :statePartnerAccountId ' 
		             + 'and IsPersonAccount = false '
		             + 'and Multi_Site_Enabled__c = false '
		             + 'and Multi_Site_Bill_Jointly__c = false '
		             + 'and NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c = false '
		             + 'and NU__Status__c = ' + '\'' + Constant.ACCOUNT_STATUS_ACTIVE + '\' '
		             + 'and (((Provider_Member_Thru__c = :providerMemberThru and (RecordTypeId = \'' + Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID + '\' or (RecordTypeId = \'' + Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID + '\' and Multi_Site_Corporate__c = true)))) '
		             + 'or (Program_Service_Revenue__c != null and Provider_Member_Thru__c = null and ( '
		             + 'Provider_Lapsed_On__c = null or ((Provider_Membership__c != null AND Provider_Membership__r.Last_Provider_Lapsed_On__c = null) '
		             + 'or (NU__Membership__c != null AND NU__Membership__r.Last_Provider_Lapsed_On__c = null)) '
		             + ')) '
		             + '     OR (Multi_Site_Program_Service_Revenue__c != null and Provider_Member_Thru__c = null and Provider_Lapsed_On__c = null and Multi_Site_Corporate__c = true))';
		
		// Get the MSO who is also billed like a state partner. For example, NCO, VOA, and PAH.
		if (StateProvider.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
			query = 'Select id, '
		             + 'Name, '
		             + 'Multi_Site_Program_Service_Revenue__c, '
		             + 'Multi_Site_Dues_Price__c, '
		             + 'Multi_Site_Corporate__c, '
		             + 'Revenue_Year_Submitted__c, '
		             + 'Program_Service_Revenue__c, '
		             + 'Provider_Join_On__c, '
		             + 'Provider_Membership__c, '
		             + 'Provider_Membership__r.NU__EndDate__c,'
		             + 'Billing_Type__c, '
		             + 'RecordTypeId, '
		             + 'Dues_Price__c '
		             + 'from Account '
		             + 'where id = :statePartnerAccountId '
		             + 'and (Provider_Member_Thru__c = :providerMemberThru or (Multi_Site_Program_Service_Revenue__c != null and Provider_Member_Thru__c = null and Provider_Lapsed_On__c = null))';
		}
		
		
		return Database.getQueryLocator(query);
	}
	
	public static Integer getNotNull(Integer s){
		    if (s == null){
		        return 0;
		    }
		    else {
		        return s;
		    }
    }
	
	global void execute(Database.BatchableContext context, List<Account> jointBillingAccounts) {
		List<Joint_Billing_Item__c> billingItems = new List<Joint_Billing_Item__c>();
		List<Joint_Billing_Item_Line__c> billingItemLines = new List<Joint_Billing_Item_Line__c>();
		List<JointBillingUtil.JointBillingItemAndLineAssociation> billingItemToLineMap = new List<JointBillingUtil.JointBillingItemAndLineAssociation>();
		List<NU__Membership__c> memberships = new List<NU__Membership__c>();
		
		Map<Id, List<Account>> msoCorporateProvidersMap = JointBillingUtil.getMSOCorporateProvidersMap(jointBillingAccounts);
		
		
		// Only insert the joint billing if there are billing items to insert
		if (JointBilling.Id == null){
			insert JointBilling;
		}
		

		JointBillingUtil.PopulateCustomPricingManagerCaches(jointBillingAccounts, JointStateProviderMembershipType.Id, JointStateProviderProductId);

		NU.MembershipTermInfo jointBillingMembershipTerm = JointBillingUtil.calculateMembershipTermFromJointBilling(JointBilling, JointStateProviderMembershipType);

		for (Account jointBillingAccount : jointBillingAccounts){			
			Joint_Billing_Item__c billingItem = JointBillingUtil.createJointBillingItem(jointBillingAccount, JointBilling);
			billingItems.add(billingItem);
			
			JointBillingUtil.ProviderJoinProrated = false;
			
			Date providerJoinDate = JointBillingUtil.getProviderJoinOnDate(jointBillingAccount);
			
			Map<Id, Decimal> duesPricing = JointBillingUtil.getProviderPricing(JointBilling.Bill_Date__c, jointBillingAccount.Id, JointStateProviderProductId, JointStateProviderMembershipType.Id, jointBillingMembershipTerm.StartDate, jointBillingMembershipTerm.EndDate, providerJoinDate);
			
			Decimal jointStateProviderDuesPrice = duesPricing.get(JointStateProviderProductId);
			
			Joint_Billing_Item_Line__c billingItemLine = new Joint_Billing_Item_Line__c();
			
			if(jointBillingAccount.Provider_Membership__c != null )
            {  
            	if(getNotNull(jointBillingAccount.Provider_Membership__r.NU__EndDate__c.year()) < (system.today().year() - 5))
            	{
            	  billingItemLine = JointBillingUtil.createJointBillingItemLine(jointStateProviderDuesPrice, JointBilling.Bill_Date__c, Constant.JOINT_BILLING_ITEM_LINE_TYPE_NEW, jointBillingAccount);
            	}
            	else
	            	{
	            		billingItemLine = JointBillingUtil.createJointBillingItemLine(jointStateProviderDuesPrice, JointBilling.Bill_Date__c, Constant.JOINT_BILLING_ITEM_LINE_TYPE_RENEW, jointBillingAccount);
	            	}
            }
            else
        	{
        		billingItemLine = JointBillingUtil.createJointBillingItemLine(jointStateProviderDuesPrice, JointBilling.Bill_Date__c, Constant.JOINT_BILLING_ITEM_LINE_TYPE_NEW, jointBillingAccount);
        	}
        	
        	/*if(jointBillingAccount.Billing_Type__c != null && (jointBillingAccount.Billing_Type__c.containsIgnoreCase('Complimentary') || jointBillingAccount.Billing_Type__c.containsIgnoreCase('Trial') || jointBillingAccount.Billing_Type__c.containsIgnoreCase('Hardship')))
			{
				jointBillingAccount.Billing_Type__c = 'Millage Standard'; 
			}
			else */ if(jointBillingAccount.Billing_Type__c == null)
			     {
			     	jointBillingAccount.Billing_Type__c = 'Millage Standard'; 
			     }
			
			JointBillingUtil.JointBillingItemAndLineAssociation assoc = new JointBillingUtil.JointBillingItemAndLineAssociation();
			
			assoc.jointBillingItem = billingItem;
			assoc.jointBillingItemLine = billingItemLine;
			
			billingItemToLineMap.add(assoc);
			billingItemLines.add(billingItemLine);
			
			NU__Membership__c renewalProviderMembership = DataFactoryMembershipExt.createMembership(jointBillingAccount, JointBilling, JointStateProviderMembershipType, jointStateProviderDuesPrice, jointBillingMembershipTerm.StartDate, jointBillingMembershipTerm.EndDate);
			memberships.add(renewalProviderMembership);
			
			if (JointBillingUtil.isCorporateMSO(jointBillingAccount)){
				List<Account> corporateProviders = msoCorporateProvidersMap.get(jointBillingAccount.Id);
				List<NU__Membership__c> corporateProviderMemberships = JointBillingUtil.createCorporateProviderMemberships(corporateProviders, renewalProviderMembership);
				memberships.addall(corporateProviderMemberships);
			}
		}

		insert billingItems;
		
		JointBillingUtil.setBillingItemOnLines(billingItemToLineMap);
		
		insert billingItemLines;
		insert memberships;
	}
	
	global void finish(Database.BatchableContext context) {
		if (Test.isRunningTest()){
			return;
		}
		
		// Get the ID of the AsyncApexJob representing this batch job
	    // from Database.BatchableContext.
	    // Query the AsyncApexJob object to retrieve the current job's information.
		AsyncApexJob a = 
		  [SELECT Id, 
		          Status,
		          ExtendedStatus,
		          NumberOfErrors,
		          JobItemsProcessed,
		          TotalJobItems,
		          CreatedBy.Email
	         from AsyncApexJob
	        where Id = :context.getJobId()];

	    // Send an email to the staff who requested the bill run.
		String subject = StateProvider.Name + ' Joint Provider Bill Run Results';
	   
	   	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   	String[] toAddresses = new String[] {a.CreatedBy.Email};
	   	mail.setToAddresses(toAddresses);
	   	mail.setSubject(subject);
	   
	   	String htmlBody = '';
	   
	   	if (a.NumberOfErrors == 0){
	   		if (JointBilling.Id == null){
	   			if (StateProvider.RecordTypeId != Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
	   				htmlBody = 'No Joint Billing was created because the ' + StateProvider.Name + ' has no providers with a provider member thru of ' + JointBilling.End_Date__c.format() + ' or newly added providers to bill or is not a multisite organization that should be billed jointly.';
	   			}
	   			else{
	   				htmlBody = 'No Joint Billing was created because the ' + StateProvider.Name + ' multi-site org does not have a provider member thru of ' + JointBilling.End_Date__c.format() + ' or is not a new joining multi-site org with a multi-site program service revenue.';
	   			}
	   		}
	   		else{
		   		String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
		   		String jointBillingRelativeURL = new ApexPages.Standardcontroller(JointBilling).view().getUrl();
		   		String jointBillingURL = baseURL + jointBillingRelativeURL;
		   		String jointBillingAnchor = String.format('<a href="{0}">Joint Billing Detail</a>', new List<String>{ jointBillingURL });
		   		
				htmlBody = 'The ' + StateProvider.Name + ' bill run has completed successfully. Please visit the ' + jointBillingAnchor + ' for more information.';
	   		}
	   	}
	   	else{
	   		htmlBody = 'The ' + StateProvider.Name + ' bill run has completed with errors and no records were created.' + 'There were ' + a.NumberOfErrors + ' error(s) for job ( ' + a.Id + ' ).';
	   		htmlBody += ' The errors are ' + a.ExtendedStatus;
	   	}
	   		
	   	mail.setHtmlBody(htmlBody);
	   	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
	public static String getNoStatePartnerValMsg(Joint_Billing__c jointBillingToRun){
		return 'No State Provider was found with the id of ' + jointBillingToRun.State_Partner__c;
	}
	
	public static String getNotStateProviderValMsg(Account StateProvider){
		return 'A state provider or multisite organization with bill jointly must be chosen. ' + StateProvider.Name + ' is a ' + StateProvider.RecordType.Name + '.';
	}
	
	public static Account getStateProviderAccount(Id statePartnerId){
		List<Account> accounts = 
		[select id,
		        name,
		        RecordTypeId,
		        RecordType.Name,
				Multi_Site_Bill_Jointly__c		        
		   from Account
		  where id =:statePartnerId];

		if (accounts.size() > 0){
			return accounts[0];
		}
		
		return null;
	}
}