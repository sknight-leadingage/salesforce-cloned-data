/**
 */
@isTest

private class TestCorporateAllianceMPriorityTriggers {
	static NU__Entity__c entity = null; 
    static NU__GLAccount__c gl = null;
    static Account CAcompany = new Account(NU__PersonEmail__c = 'test@test.org');
    static Account contact = new Account(NU__PersonEmail__c = 'test@test.org');
    static NU__PriceClass__c priceClass = null;
    
    static NU__Product__c sponsorshipProduct = null;
    static NU__Deal__c sponsorshipDeal = null;
    static DealItem__c sponsorshipDealItem = null;
    static NU__MembershipType__c CastMembershipType = null;
    static NU__Membership__c CurrentMembership = null;
    
    static NU__Cart__c cart = null;
    static NU__CartItem__c cartItem = null;
    static NU__CartItemLine__c cartItemLine = null;
    
    static Map<String, Schema.RecordTypeInfo> productRecordTypes = null;
    static Map<String, Schema.RecordTypeInfo> dealRecordTypes = null;

   private static void setupBaseData() {
        if (entity != null) return;
        
        entity = NU.DataFactoryEntity.insertEntity();
        gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);

        CAcompany = DataFactoryAccountExt.insertBusinessAccount(Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID);
		CAcompany.Type = Constant.ACCOUNT_RECORD_SUBTYPE_NAME_CORPORATE;
		CAcompany.LeadingAge_Member__c = Constant.STATE_MEMBER;
		update CAcompany;
        contact = DataFactoryAccountExt.insertIndividualAccount();
        
        priceClass = priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        
        // setup specific for sponsorships      
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo sponsorshipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        String strProductName = 'AM' + string.valueof(System.Today().year()) +'/Annual Meeting';
        sponsorshipProduct = DataFactoryProductExt.insertProduct(strProductName, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        sponsorshipDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, CAcompany.Id, contact.Id, Constant.DEAL_STATUS_NEW, sponsorshipDealRTI.getRecordTypeId());
        
        sponsorshipDealItem = DataFactoryDealItem.insertDealItem(sponsorshipDeal.Id, sponsorshipProduct.Id, 500.00, 1);
        
        cart = new NU__Cart__c(
            NU__BillTo__c = CAcompany.Id,
            NU__TransactionDate__c = Date.today(),
            NU__Entity__c = entity.Id,
            Deal__c = sponsorshipDeal.Id
        );
        insert cart;
        
        Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
        cartItem = new NU__CartItem__c(
            NU__Cart__c = cart.Id,
            NU__Customer__c = CAcompany.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = rt.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP).getRecordTypeId()
        );
        insert cartItem;
        
        cartItemLine = new NU__CartItemLine__c(
            NU__CartItem__c = cartItem.Id,
            NU__IsInCart__c = true,
            NU__UnitPrice__c = sponsorshipDealItem.Price__c,
            NU__Product__c = sponsorshipDealItem.Product__c,
            NU__Quantity__c = sponsorshipDealItem.Quantity__c                            
        );
        
        SponsorshipCartItemLineData sponsorshipCartItemLineData = new SponsorshipCartItemLineData(sponsorshipDeal.Event__c, null, null);
        cartItemLine.NU__Data__c = JSON.serialize(sponsorshipCartItemLineData);
        insert cartItemLine;
	}
    
	static OrderPurchaseSponsorship createOrderPurchaseSponsorship(Id billToId, Id entityId){
    	PageReference pageRef = new PageReference('/apex/OrderSponsorshipProducts');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('billTo',billToId);
        ApexPages.currentPage().getParameters().put('entity',entityId);
        OrderPurchaseSponsorship controller = new OrderPurchaseSponsorship();
        
        return controller;
	}
   
	private static void setupSponsorshipData() {
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
 		String strProductName = 'AM' + string.valueof(System.Today().year()) +'/Annual Meeting';
        NU__Product__c newProduct = DataFactoryProductExt.insertProduct(strProductName, entity.Id, gl.Id, productRecordTypes, 'Membership');
        NU__Product__c newProduct2 = DataFactoryProductExt.insertProduct(strProductName, entity.Id, gl.Id, productRecordTypes, 'Sponsorship');
	    
	    
	    Schema.RecordTypeInfo sponsorshipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
	    
	    //Set External_Id__c to force the Status__c formula to return 'Active'
	    Sponsorship__c Sponsorship1 = New Sponsorship__c(Account__c = CAcompany.Id, Product__c = newProduct2.Id, External_Id__c = '123456');
	    
	    insert Sponsorship1;
	    
		setupMembershipData();
	}
	
	static void setupMembershipData() {
        CastMembershipType = DataFactoryMembershipTypeExt.createCASTMembershipType(entity.Id);
        insert CastMembershipType;
        
        CurrentMembership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(CAcompany.Id, CastMembershipType.Id);
        insert CurrentMembership;
         //New Sponsorship Order - billTo == CAcompany & entity supplied      
        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorship(CAcompany.Id, entity.Id);
        
        //Ensure membership lookup is populated
        CAcompany.NU__Membership__c = CurrentMembership.Id;
        update CAcompany; 
        
        // Submit Cart
        PageReference tempRef = controller.Save();
	}
	
	static void setupSecondaryCASTMembershipData() {
    	NU__MembershipType__c castMT = DataFactoryMembershipTypeExt.createDefaultCompanyAnnualMembershipType(entity.Id, 'CAST Focus', Constant.CAST_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	castMT.NU__FlowdownManagerClass__c = Constant.CAST_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	castMT.NU__AccountJoinOnField__c = Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	insert castMT;
		
        NU__Membership__c thisMembership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(CAcompany.Id, castMT.Id);
        thisMembership.MembershipTypeProductName__c = 'CAST Focus';
        insert thisMembership;
        
        //Ensure membership lookup is populated
        CAcompany.CAST_Membership__c = thisMembership.Id;
        update CAcompany; 
	}
	
	static void ForceSetMembershipTypeName(string TypeName) {
        CurrentMembership.MembershipTypeProductName__c = TypeName;
        update CurrentMembership;
        
        CAcompany.Membership_1__c = 'Test#1A';
        
        RefreshCompanyAccount();
	}
	
	static void RefreshCompanyAccount() {
		CAcompany = [SELECT Id, LeadingAge_Member__c, Type, LeadingAge_Member_Company__c, Member_Product_Name__c, NU__Membership__c, CAST_Membership__c, Membership_1__c, Membership_2__c FROM Account WHERE Id = :CAcompany.Id];
	}
   
	static testMethod void testLeadingAgeNonMemberCompany() {
		setupBaseData();
		
		CAcompany.LeadingAge_Member__c = Constant.LEADINGAGE_NONMEMBER;
		update CAcompany;

		setupSponsorshipData();

		ForceSetMembershipTypeName('Test LeadingAge Supporter Membership');

    	String AYearly = 'AM'+ string.valueof(System.Today().year()) +'%Annual%';
    	String SYearly = 'AM'+ string.valueof(System.Today().year()) +'%LeadingAge%Supporter%';
		List<Sponsorship__c> testSponsorships = [SELECT Id, Account__c, ProductDesc__c, Status__c FROM Sponsorship__c WHERE ((ProductDesc__c LIKE :AYearly) OR (ProductDesc__c LIKE :SYearly )) AND (Status__c = 'Active') ORDER BY Sponsorship_Date__c DESC];
		
		//Test fundamental logic of test data setup code
		List<Sponsorship__c> testSponsorshipsAll = [SELECT Id, Account__c, ProductDesc__c, Status__c FROM Sponsorship__c ORDER BY Sponsorship_Date__c DESC];
		system.assertNotEquals(0, testSponsorshipsAll.size(), 'No sponsorships exist!');
		
		//Ensure test data matches expected settings
		String strProductName = 'AM' + string.valueof(System.Today().year()) +'/Annual Meeting';
		for(Sponsorship__c s : testSponsorshipsAll) {
			system.assertEquals('Active', s.Status__c);
			system.assertEquals(s.ProductDesc__c, strProductName);
			system.assertEquals(s.Account__c, CAcompany.Id);
		}
		
		//Fundamental tests for class logic
		system.assertNotEquals(0, testSponsorships.size(), 'Account must have at least one sponsorship matching the test conditions!');
		
		String sMembershipName = 'CAST Business Associate';
		ForceSetMembershipTypeName(sMembershipName);
		system.assertEquals(sMembershipName, CAcompany.Member_Product_Name__c);
		
		system.assertEquals(Constant.LEADINGAGE_NONMEMBER, CAcompany.LeadingAge_Member__c, 'Test account must be a member at this stage of testing!');
		system.assertEquals(Constant.LEADINGAGE_NONMEMBER, CAcompany.LeadingAge_Member_Company__c, 'Test account must be a member at this stage of testing!');
		system.assertNotEquals(null, CAcompany.NU__Membership__c, 'The Membership lookup field must be filled out at this stage of the test!');
		
		//At this point, we've set up the account to the point where it has an AM[year]/Annual Meeting sponsorship & a CAST Business Associate membership
		//	BUT the account is a Non-member.  So the end result should be that M1&2 are empty.  So let's assert that
		RefreshCompanyAccount();
		
		Test.startTest();
		system.assertEquals(null, CAcompany.Membership_1__c, 'Membership 1 should be empty for Non-member accounts.');
		system.assertEquals(null, CAcompany.Membership_2__c, 'Membership 2 should be empty for Non-member accounts.');
		Test.stopTest();
	}
  
	static testMethod void testLeadingAgeMemberAnnualAndCASTBusiness() {
		setupBaseData();
		
		CAcompany.LeadingAge_Member__c = Constant.LEADINGAGE_MEMBER;
		update CAcompany;
		RefreshCompanyAccount();
		
		setupSponsorshipData();
		
		RefreshCompanyAccount();

    	String AYearly = 'AM'+ string.valueof(System.Today().year()) +'%Annual%';
    	String SYearly = 'AM'+ string.valueof(System.Today().year()) +'%LeadingAge%Supporter%';
		List<Sponsorship__c> testSponsorships = [SELECT Id, Account__c, ProductDesc__c, Status__c FROM Sponsorship__c WHERE ((ProductDesc__c LIKE :AYearly) OR (ProductDesc__c LIKE :SYearly )) AND (Status__c = 'Active') ORDER BY Sponsorship_Date__c DESC];
		
		//Test fundamental logic of test data setup code
		List<Sponsorship__c> testSponsorshipsAll = [SELECT Id, Account__c, ProductDesc__c, Status__c FROM Sponsorship__c ORDER BY Sponsorship_Date__c DESC];
		system.assertNotEquals(0, testSponsorshipsAll.size(), 'No sponsorships exist!');
		
		//Ensure test data matches expected settings
		String strProductName = 'AM' + string.valueof(System.Today().year()) +'/Annual Meeting';
		for(Sponsorship__c s : testSponsorshipsAll) {
			system.assertEquals('Active', s.Status__c);
			system.assertEquals(s.ProductDesc__c, strProductName);
			system.assertEquals(s.Account__c, CAcompany.Id);
		}
		
		//Fundamental tests for class logic
		system.assertNotEquals(0, testSponsorships.size(), 'Account must have at least one sponsorship matching the test conditions!');
		
		String sMembershipName = 'CAST Business Associate';
		ForceSetMembershipTypeName(sMembershipName);
		system.assertEquals(sMembershipName, CAcompany.Member_Product_Name__c);
		
		system.assertEquals(Constant.LEADINGAGE_MEMBER, CAcompany.LeadingAge_Member__c, 'Test account must be a non-member at this stage of testing!');
		system.assertEquals(Constant.LEADINGAGE_MEMBER, CAcompany.LeadingAge_Member_Company__c, 'Test account must be a non-member at this stage of testing!');
		system.assertNotEquals(null, CAcompany.NU__Membership__c, 'The Membership lookup field must be filled out at this stage of the test!');
		
		//At this point, we've set up the account to the point where it has an AM[year]/Annual Meeting sponsorship & a CAST Business Associate membership
		//	BUT the account is a Non-member.  So the end result should be that M1&2 are empty.  So let's assert that
		RefreshCompanyAccount();
		
		Test.startTest();
		system.assertEquals(strProductName, CAcompany.Membership_1__c);
		//system.assertEquals(sMembershipName, CAcompany.Membership_2__c, 'Membership 2 should be empty for Non-member accounts.');
		Test.stopTest();
	}
	
	static testMethod void testLeadingAgeMemberAnnualAndCASTFocus() {
		setupBaseData();
		
		CAcompany.LeadingAge_Member__c = Constant.LEADINGAGE_MEMBER;
		update CAcompany;
		RefreshCompanyAccount();
		
		setupMembershipData();
		setupSecondaryCASTMembershipData();
		
		String sTypeName = 'Test LeadingAge Supporter Membership';
		ForceSetMembershipTypeName(sTypeName);
		
		RefreshCompanyAccount();
		
		system.assertEquals(Constant.LEADINGAGE_MEMBER, CAcompany.LeadingAge_Member__c, 'Test account must be a non-member at this stage of testing!');
		system.assertEquals(Constant.LEADINGAGE_MEMBER, CAcompany.LeadingAge_Member_Company__c, 'Test account must be a non-member at this stage of testing!');
		system.assertNotEquals(null, CAcompany.NU__Membership__c, 'The Membership lookup field must be filled out at this stage of the test!');
		system.assertNotEquals(null, CAcompany.CAST_Membership__c, 'The CAST Membership lookup field must be filled out at this stage of the test!');
		
		//At this point, we've set up the account to the point where it has an AM[year]/Annual Meeting sponsorship & a CAST Business Associate membership
		//	BUT the account is a Non-member.  So the end result should be that M1&2 are empty.  So let's assert that
		system.assertEquals(true, CAcompany.Member_Product_Name__c.containsIgnoreCase('LeadingAge Supporter'), 'Member Product Name should contain \'LeadingAge Supporter\' for this test! (actual: ' + CAcompany.Member_Product_Name__c + ')');
		
		Test.startTest();
		//system.assertEquals(sTypeName, CAcompany.Membership_1__c);
		//system.assertEquals('CAST Focus', CAcompany.Membership_2__c);
		Test.stopTest();
	}
}