/**
 * @author NimbleUser
 * @date Updated: 3/22/13
 * @description This class provides various functions for creating and inserting Conference Proposals
 * It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Conference Proposals without inserting them. 
 * The insert* functions create Conference Proposals and insert them.
 */
public with sharing class DataFactoryConferenceProposal {

	public static Conference_Proposal__c createConferenceProposal(){
		NU__Event__c conference = NU.DataFactoryEvent.insertEvent();
		Conference_Speaker__c speaker = DataFactoryConferenceSpeaker.insertConferenceSpeaker();
		
		return new Conference_Proposal__c(
			Conference__c = conference.Id,
			Submitter__c = speaker.Id,
			Title__c = 'Test Proposal'
		);
	}
	
	public static Conference_Proposal__c createConferenceProposal(Id sessionId){
		Conference_Proposal__c prop = createConferenceProposal();
		
		prop.Session__c = sessionId;
		
		return prop;
	}
	
	public static Conference_Proposal__c createConferenceProposal(Id sessionId, String status){
		Conference_Proposal__c prop = createConferenceProposal(sessionId);
		
		prop.Status__c = status;
		
		return prop;
	}
	
	public static Conference_Proposal__c insertConferenceProposal(){
		Conference_Proposal__c confPropToInsert = createConferenceProposal();
		
		insert confPropToInsert;
		return confPropToInsert;
	}
	
	public static Conference_Proposal__c insertConferenceProposal(Id sessionId){
		Conference_Proposal__c confPropToInsert = createConferenceProposal(sessionId);
		
		insert confPropToInsert;
		return confPropToInsert;
	}
	
	public static Conference_Proposal__c insertAcceptedConferenceProposal(Id sessionId){
		Conference_Proposal__c confPropToInsert = createConferenceProposal(sessionId, Constant.CONFERENCE_PROPOSAL_STATUS_ACCEPTED);
		
		insert confPropToInsert;
		return confPropToInsert;
	}
}