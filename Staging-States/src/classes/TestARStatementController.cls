/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestARStatementController {
	
	static NU__Entity__c entity = null;
	
	static Account account = null;
	
	static PageReference pageRef = null;
	static ARStatementController controller = null;
	
	private static void setupTest() {
		entity = NU.DataFactoryEntity.insertEntity();		
		account = NU.DataFactoryAccount.insertOrganizationAccount();
		
		pageRef = new PageReference('/apex/ARStatement');
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',account.Id);
		
		controller = new ARStatementController(new ApexPages.StandardSetController(new List<Account>()));
	}

    static testMethod void BasicTest() {
        setupTest();
        
        basicAsserts(0, 1, null, 0.0, null, false, null);
    }
    
    static testMethod void NoAccountsSelectedTest() {
        setupTest();
        ApexPages.currentPage().getParameters().put('id',null);
        controller = new ARStatementController(new ApexPages.StandardSetController(new List<Account>()));
        
        basicAsserts(0, 1, null, 0.0, null, false, null);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Error)); // no account specified!
    }
    
    static testMethod void NoEntitySelectedTest() {
        setupTest();
        
        controller.Generate();
        
        basicAsserts(0, 1, null, 0.0, null, false, null);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Error)); // no entity specified!
    }
    
    static testMethod void InvalidAccountsTest() {
        setupTest();
        
        controller.EntityId = controller.EntityOptions.get(0).getValue();
        controller.Generate();
        
        basicAsserts(0, 1, entity.Id, 0.0, null, false, null);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); // no valid accounts!
    }
    
    static testMethod void SpecifyOptionalParametersTest() {
        setupTest();
        
        controller.EntityId = controller.EntityOptions.get(0).getValue();
        controller.DummyOrder.NU__InvoiceDate__c = Date.today();
        controller.MinimumBalanceOverride = 100.0;
        controller.PDF();
        controller.Print();
        
        basicAsserts(0, 1, entity.Id, 100.0, Date.today(), false, 'pdf');
    }
    
    static testMethod void ValidAccountTest() {
        setupTest();
        TestViewARStatementController.insertOrderWithBalance(entity.Id, account.Id, 100.0);
        
        controller.EntityId = controller.EntityOptions.get(0).getValue();
        controller.Generate();
        
        basicAsserts(1, 1, entity.Id, 0.0, null, true, null);
        System.assert(!ApexPages.hasMessages()); // no warning messages!
    }
    
    private static void basicAsserts(Integer accountsSize, Integer entitySize, Id entityId, Decimal minimumBalance, Date invoiceDateOverride, Boolean canRender, String renderString) {
    	// assert the size of the valid accounts passed in
    	System.assertEquals(accountsSize, controller.Accounts.size());
    	
    	// assert the number of entity options
    	System.assertEquals(entitySize, controller.EntityOptions.size());
    	
    	// assert the selected entity Id
    	System.assertEquals(entityId, controller.EntityId);
    	
    	// assert the minimum balance specified
    	System.assertEquals(minimumBalance, controller.MinimumBalanceOverride);
    	
    	// assert the invoice date specified
    	System.assertEquals(invoiceDateOverride, controller.DummyOrder.NU__InvoiceDate__c);
    	
    	// should it be able to render?
    	System.assertEquals(canRender, controller.CanRender);
    	
    	// are we generating a PDF?
    	System.assertEquals(renderString, controller.getRender());
    }
}