@isTest
private class TestBatchSetPrimaryContact {

    static testMethod void StartUnitTest() {
    	
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount(10000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        Account providerAccount2 = DataFactoryAccountExt.insertProviderAccount(10000);
        Account individualAccount2 = DataFactoryAccountExt.insertIndividualAccount();
        Account individualAccount3 = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, providerAccount.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(individualAccount2.Id, providerAccount2.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(individualAccount3.Id, providerAccount2.Id, false);

        Account ind = [SELECT Id, RecordTypeId, NU__PrimaryAffiliation__c, NU__PrimaryAffiliationRecord__c FROM Account WHERE Id = :individualAccount.Id];
        Account ind2 = [SELECT Id, RecordTypeId, NU__PrimaryAffiliation__c, NU__PrimaryAffiliationRecord__c FROM Account WHERE Id = :individualAccount2.Id];
        Account ind3 = [SELECT Id, RecordTypeId, NU__PrimaryAffiliation__c, NU__PrimaryAffiliationRecord__c FROM Account WHERE Id = :individualAccount3.Id];
        Account prov = [SELECT Id, NU__PrimaryContact__c FROM Account WHERE Id = :providerAccount.Id];
        
        List<NU__Affiliation__c> aff = new List<NU__Affiliation__c>();
        aff.add(new NU__Affiliation__c(Id = ind.NU__PrimaryAffiliationRecord__c, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT));
        aff.add(new NU__Affiliation__c(Id = ind2.NU__PrimaryAffiliationRecord__c, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT));
        aff.add(new NU__Affiliation__c(Id = ind3.NU__PrimaryAffiliationRecord__c, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT));
        update aff;
        
        System.assertNotEquals(null, ind.NU__PrimaryAffiliation__c);
        System.assertEquals(null, prov.NU__PrimaryContact__c);

		Test.startTest();
        BatchUpdatePrimaryContact c = new BatchUpdatePrimaryContact();
        Database.executeBatch(c);
        Test.stopTest();
        
        Account prov2 = [SELECT Id, NU__PrimaryContact__c FROM Account WHERE Id = :providerAccount.Id];
        System.assertNotEquals(null, prov2.NU__PrimaryContact__c);        
    }
}