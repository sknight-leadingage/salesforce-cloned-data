/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceProposalTriggerHandlers {

    static testMethod void proposalSpeakerRecordCreatedForSubmitterOnConferenceProposalInsert() {
        Conference_Proposal__c newProposal = DataFactoryConferenceProposal.insertConferenceProposal();
        
        List<Conference_Proposal_Speaker__c> submitterProposals =
        [select id,
                name
           from Conference_Proposal_Speaker__c
          where Speaker__c = :newProposal.Submitter__c
            and Proposal__c = :newProposal.Id];
        
        system.assert(submitterProposals.size() > 0, 'No proposal speaker record was created for the submitter.');
    }
}