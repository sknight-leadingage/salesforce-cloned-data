global with sharing class UpdateCredentials implements Database.Batchable<Account> {
   private static String ENCRYPTION_KEY = '4212B25KL09221BG8732B893'; // Go To /Apex/NU__Configure and get the Enryption Key Value first 

    global Iterable<Account> start(Database.BatchableContext context) {
        return (Iterable<Account>)Database.getQueryLocator('Select NU__Username__c, PersonEmail, NU__PasswordSalt__c, Temporary_Password__c from Account Where Temporary_Password__c != null And IsPersonAccount = true And PersonEmail = null and NU__PasswordHash__c = null');
    }
    
    global void execute(Database.BatchableContext context, List<Account> accounts) {
    
        Blob key = Blob.valueOf(ENCRYPTION_KEY);
    
        for (Account account : accounts) {
            String salt = account.NU__PasswordSalt__c;
            if (salt == null) {
                salt = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).toUpperCase();
                Account.NU__PasswordSalt__c = salt;
            }
            
            if(account.NU__Username__c == null && account.PersonEmail != null)
            {
                account.NU__Username__c = account.PersonEmail.toLowerCase();
            }
            
            Blob hashedPassword = Crypto.generateMac('hmacSHA256', Blob.valueOf(account.Temporary_Password__c + salt), key);
            account.NU__PasswordHash__c = EncodingUtil.base64Encode(hashedPassword);
        }
        
        update accounts;
    }
    
    global void finish(Database.BatchableContext context) {
    }
}