public with sharing class GroupHeaderController
{
    public string CurHeader {get;set;}
    public string CurHeaderDisplay {get;set;}
    public static string localstr {get;set;}
    public static Map<string,integer> GroupTotals {get;set;}
    public static Map<string,integer> SecondGroupTotals {get;set;}
    
    public string SecHeader {get;set;}
    public string SecHeaderDisplay {get;set;}
    public static string localSecHeader {get;set;}
    
    public string ThirdHeader {get;set;}
    public static string localThirdHeader {get;set;}
    
    public string getHeaderLocalTitle() {
        if (localstr == null) {
            localstr = '';
        }
        if (CurHeader == null) {
            CurHeader = '(Blank)';
        }
        if (localstr != CurHeader) {
            localstr = CurHeader;
            if (CurHeaderDisplay == null || CurHeaderDisplay == '') {
                CurHeaderDisplay = localstr;
            }
            localSecHeader = null;
            localThirdHeader = null;
            //return '<hr width="100%" size="1" /><h2 style="font-size: larger;">' + CurHeaderDisplay + '</h2><hr width="100%" size="1" />';
            //integer GCount = Database.query(CurHeaderCountSOQL);
            if (GroupTotals != null) {
                integer GCount = GroupTotals.get(localstr);
                if (GCount == null) {
                    GCount = 0;
                }
                return CurHeaderDisplay.replace('~', '"').replace('#SOQLCOUNT#', GCount.format());
            } else {
                return CurHeaderDisplay.replace('~', '"').replace('#SOQLCOUNT#', '');
            }
            //return CurHeaderDisplay.replace('~', '"');
        }
        else {
            return '';
        }
    }
    
    public string getHeaderSecondTitle() {
        if (localSecHeader == null) {
            localSecHeader = '';
        }
        if (localSecHeader != SecHeader) {
            localSecHeader = SecHeader;
            localThirdHeader = null;
            if (localSecHeader == null) {
                return '';
            } else {
                if (SecHeaderDisplay == null || SecHeaderDisplay == '') {
                    SecHeaderDisplay = '<div  style="padding: 6px 0 6px 0;"><h2>' + localSecHeader + '</h2></div>';
                }
                if (SecondGroupTotals != null) {
                    integer GCount = SecondGroupTotals.get(localSecHeader);
                    if (GCount == null) {
                        GCount = 0;
                    }
                    return SecHeaderDisplay.replace('~', '"').replace('#SOQLCOUNT#', GCount.format());
                } else {
                    return SecHeaderDisplay.replace('~', '"').replace('#SOQLCOUNT#', '');
                }
            }
        }
        else {
            return '';
        }
    }
    
    public string getHeaderThirdTitle() {
        if (localThirdHeader == null) {
            localThirdHeader = '';
        }
        
        if (localThirdHeader != ThirdHeader) {
            localThirdHeader = ThirdHeader;
            if (localThirdHeader == null) {
                return '';
            } else {
                return '<div style="padding: 2px 0 6px 0;"><h2>' + localThirdHeader + '</h2></div>';
            }
        }
        else {
            return '';
        }
    }
}