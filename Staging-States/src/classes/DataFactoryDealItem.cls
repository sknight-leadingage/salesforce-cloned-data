public with sharing class DataFactoryDealItem {
	
	public static DealItem__c insertDealItem(Id dealId, Id productId){
		return insertDealItem(dealId, productId, null, 1);
	}

	public static DealItem__c insertDealItem(Id dealId, Id productId, Decimal price, Integer quantity) {
        DealItem__c dealItem = new DealItem__c(
            Deal__c = dealId,
            Product__c = productId,
            Price__c = price,
            Quantity__c = quantity
        );

        insert dealItem;
        
        return dealItem;
    }
}