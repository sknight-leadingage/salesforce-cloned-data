/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
public class TestBatchUpdateAndArchiveYearAwardedPts
{
	static final Integer YEAR = Date.today().year();
	
    public static testMethod void testBatch() 
    {
      //create entity, gl, product record types, event, product
      NU__Entity__c entity = NU.DataFactoryEntity.insertEntity();
      NU__GLAccount__c gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);
      Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
      
      NU__Event__c evt = NU.DataFactoryEvent.insertEvent(entity.Id);
      NU__Product__c prod = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_EXHIBITOR, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_EXHIBITOR);
      
      List<NU__Membership__c> membershipList = new List<NU__Membership__c>();
      List<Exhibitor__c> exhs = new List<Exhibitor__c>();
      
      evt.Current_Year_Annual_Event__c = true;
      evt.NU__StartDate__c = Date.newInstance(YEAR, 10, 21);
      evt.NU__EndDate__c = Date.newInstance(YEAR, 10, 26);
      update evt;
      
      List <Account> accounts = new List<Account>();
    	
       for(integer i = 0; i<75; i++)
       {
             Account a = new Account(Name = 'Test Company', Current_Year_Priority_Points__c = 2, Current_Year_Priority_Point_Adjustments__c = 1, Previous_Years_Priority_Points__c = 0);  
             accounts.add(a);
       }
      
       for(integer i = 0; i<25; i++)
       {
             Account a = new Account(Name = 'Test Company', Current_Year_Priority_Points__c = null, Current_Year_Priority_Point_Adjustments__c = null);  
             accounts.add(a);
       }
       
       NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger');
       insert accounts;
       NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger');
       
       Test.StartTest();
       BatchUpdateAndArchiveYearAwardedPoints batch = new BatchUpdateAndArchiveYearAwardedPoints();
       ID batchprocessid = Database.executeBatch(batch, 100);
       Test.StopTest();
    
       System.AssertEquals(75, database.countquery('SELECT COUNT() FROM Account WHERE Previous_Years_Priority_Points__c = 3'));  
       System.AssertEquals(25, database.countquery('SELECT COUNT() FROM Account WHERE Previous_Years_Priority_Points__c = null'));  
       
   }
}