/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAdvertisingOrderTypeCartSubmitter {
	static NU__Entity__c entity = null; 
    static NU__GLAccount__c gl = null;
    static NU__OrderItemConfiguration__c config = null;
    static NU__EntityOrderItem__c entityOrderItem = null;
    static Account billTo = null;
    static Account contact = null;
    static NU__PriceClass__c priceClass = null;
    
    static NU__Product__c advertisingProduct = null;
    static NU__Deal__c advertisingDeal = null;
    static DealItem__c advertisingDealItem = null;
    
    static NU__Cart__c cart = null;
    static NU__CartItem__c cartItem = null;
    static NU__CartItemLine__c cartItemLine = null;
    
    static Map<String, Schema.RecordTypeInfo> productRecordTypes = null;
    static Map<String, Schema.RecordTypeInfo> dealRecordTypes = null;
    
    private static void setupTest() {
        if (entity != null) return;
        
        entity = NU.DataFactoryEntity.insertEntity();
        gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);
        
        billTo = DataFactoryAccountExt.insertProviderAccount();
        contact = DataFactoryAccountExt.insertIndividualAccount();
        
        priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        
        
        
        // setup specific for advertising cart submitter        
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo advertisingDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_ADVERTISING);
        
        config = DataFactoryOrderItemConfigurationExt.insertAdvertisingOrderItemConfigurationWithEntityOrderItem(entity.Id, gl.Id);
                
        advertisingProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_ADVERTISING, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_ADVERTISING);
        
        advertisingDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_ADVERTISING, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, advertisingDealRTI.getRecordTypeId());
        
        advertisingDealItem = DataFactoryDealItem.insertDealItem(advertisingDeal.Id, advertisingProduct.Id, 500.00, 1);
        
        cart = new NU__Cart__c(
            NU__BillTo__c = billTo.Id,
            NU__TransactionDate__c = Date.today(),
            NU__Entity__c = entity.Id,
            Deal__c = advertisingDeal.Id
        );
        insert cart;
        
        Map<String, Schema.RecordTypeInfo> cartItemRecordTypes = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
        cartItem = new NU__CartItem__c(
            NU__Cart__c = cart.Id,
            NU__Customer__c = billTo.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = cartItemRecordTypes.get(Constant.ORDER_RECORD_TYPE_ADVERTISING).getRecordTypeId()
        );
        insert cartItem;
        
        cartItemLine = new NU__CartItemLine__c(
            NU__CartItem__c = cartItem.Id,
            NU__IsInCart__c = true,
            NU__UnitPrice__c = advertisingDealItem.Price__c,
            NU__Product__c = advertisingDealItem.Product__c,
            NU__Quantity__c = advertisingDealItem.Quantity__c                            
        );

        insert cartItemLine;
    } 
    
    // test methods
    static testMethod void testAdvertisingTypeCartSubmitter() {
        setupTest();
        
        // Test #1: Instantiate AdvertisingOrderItemTypeCartSubmitter, etc.
        AdvertisingOrderItemTypeCartSubmitter cartSubmitter = new AdvertisingOrderItemTypeCartSubmitter();
        System.assert(cartSubmitter != null, 'Advertising Order Item Type Cart Submitter not loaded');
        
        // need to get more information from cart items
        List<NU__CartItem__c> cartItems = [SELECT Id,
            NU__Customer__c,
            (SELECT Id
                FROM NU__CartItemLines__r)
            FROM NU__CartItem__c
            WHERE Id = :cartItem.Id];
        
        NU__Order__c testOrder = new NU__Order__c(
            NU__BillTo__c = billTo.Id,
            NU__TransactionDate__c = cart.NU__TransactionDate__c,
            NU__Entity__c = entity.Id//,
            //Deal__c = advertisingDeal.Id - this is added by a trigger
        );
        insert testOrder;
        
        Map<String, Schema.RecordTypeInfo> orderItemRecordTypes = Schema.SObjectType.NU__OrderItem__c.getRecordTypeInfosByName();
        NU__OrderItem__c orderItem = new NU__OrderItem__c(
            NU__Order__c = testOrder.Id,
            NU__Customer__c = billTo.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = orderItemRecordTypes.get(Constant.ORDER_RECORD_TYPE_ADVERTISING).getRecordTypeId()
        );
        cartSubmitter.beforeOrderItemSaved(cartItems[0], orderItem);
        System.debug(orderItem);
        insert orderItem;
        cartSubmitter.afterOrderItemsSaved();
        
        NU__OrderItemLine__c orderItemLine = new NU__OrderItemLine__c(
            NU__OrderItem__c = orderItem.Id,
            NU__UnitPrice__c = cartItemLine.NU__UnitPrice__c,
            NU__Product__c = cartItemLine.NU__Product__c,
            NU__Quantity__c = cartItemLine.NU__Quantity__c                            
        );
        cartSubmitter.beforeOrderItemLineSaved(cartItemLine, orderItemLine);
        insert orderItemLine;
        cartSubmitter.afterOrderItemLinesSaved();
        
        // Test #2: Was an Advertising history object created?
        List<Advertising__c> advertisings = [SELECT Id
            FROM Advertising__c
            WHERE Order_Item_Line__c = :orderItemLine.Id];
        
        System.assert(advertisings.size() > 0, 'No Advertising history object was created.');
        
        // Test #3: Delete Cart & verify that the Order has a Deal ID and Deal has an Order ID
        delete cartItemLine;
        delete cartItem;
        delete cart;
        
        NU__Deal__c testDeal = [SELECT Id,
            Order__c
            FROM NU__Deal__c
            WHERE Id = :advertisingDeal.Id];
            
        NU__Order__c testOrder2 = [SELECT Id,
            Deal__c
            FROM NU__Order__c
            WHERE Id = :testOrder.Id];
            
        System.assert(testDeal.Order__c == testOrder.Id, 'Deal was not updated with the order Id.');
        System.assert(testOrder2.Deal__c == testDeal.Id, 'Order was not updated with the deal Id.');
    }

}