public with sharing class FixedPricer extends MembershipPricerBase implements IProductPricer{
	

    
     public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
		
		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(membershipType, productToPrice);
		
		//poduct type should be membership
		if (productPricingRequest.MembershipTypeId == null){
			system.debug('   FixedPricer:: Membership Type Id is null');
			return null;
		}
		
		if(customer.Billing_Type__c == null) {
			system.debug('   FixedPricer:: Billing Type not set');
			return null;
		}
		
		if((customer.Billing_Type__c.containsIgnoreCase('Fixed Rate:') && customer.Billing_Type__c.containsIgnoreCase('Public Housing Authorities')) || (customer.Billing_Type__c.containsIgnoreCase('Fixed Rate:') && customer.Billing_Type__c.containsIgnoreCase('Senior Centers')))
			{
			   return 350;
			}
	      else if(customer.Billing_Type__c.containsIgnoreCase('Fixed Rate:') && customer.Billing_Type__c.containsIgnoreCase('Villages'))
				{
				   return 175;
				}
		      else 
		         {
		           return null;
		         }
	         
     }

}