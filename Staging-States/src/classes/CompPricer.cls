public with sharing class CompPricer extends MembershipPricerBase implements IProductPricer {



     public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
     
		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(membershipType, productToPrice);
		
		//poduct type should be membership
		if (productPricingRequest.MembershipTypeId == null){
			system.debug('   CompPricer:: Membership Type Id is null');
			return null;
		}
		

		
		if(customer.Billing_Type__c == null) {
			system.debug('   CompPricer:: Billing Type not set');
			return null;
		}
		
		if(customer.Billing_Type__c.containsIgnoreCase('Complimentary') || customer.Billing_Type__c.containsIgnoreCase('Trial'))
			{
			   return 0;
			}
	      else 
	         {
	         	return null;
	         }
     }
}