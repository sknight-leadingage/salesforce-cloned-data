public with sharing class EduFormatted_Controller extends EduReportsControllerBase {
    public List<Conference_Session__c> SessionData {get;set;}
    
    public EduFormatted_Controller() {
        iWizardMax = 9;
        WizardStep = 1;

/*
        if (Test.isRunningTest()) {
            SetConPageSize = 12;
        } else {
            NU__Configuration__c itmVal = [SELECT NU__Value__c FROM NU__Configuration__c WHERE Name = 'SPDataPaging' LIMIT 1];
            SetConPageSize = integer.valueof(itmVal.NU__Value__c);
        }
*/
    }
    
/*
    public integer getSetConGetPageSize() {
        if (setCon != null) {
            return setCon.getPageSize();
        } else {
            return 0;
        }
    }
*/

    public override string getPageTitle() {
        string sPT = '';
        if (Conference != null) {
            sPT = Conference.Name;
        }
        if (getMType() == 'hub') {
            sPT = sPT + ': Report 7 / Appendix A';
        }
        if (RTypeSel == null) {
            sPT = sPT + ': Session Listing';
        }
        else {
            sPT = sPT + ': ' + RTypeSel;
        }
        return sPT;
    }

    public string SLIST_OTHER = 'No extra elements';
    public string SLIST_CARCFE = 'Display CA-RCFE';
    public string SLIST_NAB = 'Display NAB';
    public string SLIST_NASBA = 'Display NASBA';
    public string SLIST_FL = 'Display Florida';
    public string SLIST_KS = 'Display Kansas';
    public string SLIST_AW = 'Display Activity Workers';
    public string SLIST_CFRE = 'Display CFRE';

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(SLIST_OTHER, SLIST_OTHER));
        options.add(new SelectOption(SLIST_CARCFE, SLIST_CARCFE));
        options.add(new SelectOption(SLIST_NAB, SLIST_NAB));
        options.add(new SelectOption(SLIST_NASBA, SLIST_NASBA));
        options.add(new SelectOption(SLIST_FL, SLIST_FL));
        options.add(new SelectOption(SLIST_KS, SLIST_KS));
        options.add(new SelectOption(SLIST_AW, SLIST_AW));
        options.add(new SelectOption(SLIST_CFRE, SLIST_CFRE));
        return options; 
    }

    public void SetQuery() {
        GroupHeaderController.SecondGroupTotals = null;
        if (true || RTypeSel == SLIST_OTHER) {
            GroupHeaderController.GroupTotals = null;
            if (getMType() == 'hub') {
                SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Timeslot__r.Session_Date__c, Timeslot__r.Session_Time__c, Title__c, PTF_Hours__c, PTF_Code__c, Learning_Objective_1__c, Learning_Objective_2__c, Learning_Objective_3__c, CA_RCFE__r.Name, NAB__r.Name, NASBA__r.Name, Florida__r.Name, Kansas__r.Name, Activity_Workers_Certification__r.Name, CFRE_Certification__r.Name, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail,Speaker__r.Knowledge_and_Professional_Experience__c FROM Conference_Session_Speakers__r WHERE Role__c = 'Speaker' OR Role__c = 'Key Contact') FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c];

/*
        SetConQuery = 'SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Timeslot__r.Session_Date__c, Timeslot__r.Session_Time__c, Title__c, Learning_Objective_1__c, Learning_Objective_2__c, Learning_Objective_3__c, CA_RCFE__r.Name, NAB__r.Name, NASBA__r.Name, Florida__r.Name, Kansas__r.Name, Activity_Workers_Certification__r.Name, CFRE_Certification__r.Name, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail,Speaker__r.Knowledge_and_Professional_Experience__c FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\') FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        setCon.SetPageSize(SetConPageSize);
        setCon.First();
        SetConRefreshData();
*/
                
                //(SELECT Name, (SELECT Speaker__r.First_Name__c,Speaker__r.Last_Name__c,Speaker__r.Knowledge_and_Professional_Experience__c FROM Conference_Proposal_Speakers__r WHERE Role__c = 'Speaker' OR Role__c = 'Key Contact') FROM Proposal__r);
            }
        }
//        else if (RTypeSel == SLIST_SSTATUS) {
//            GroupHeaderController.GroupTotals = new Map<string,integer>();
//          if (getMType() == 'hub') {
//              List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Session_AV_Validated__c g FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Session_AV_Validated__c ORDER BY Session_AV_Validated__c];
//              SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_AV_Validated__c, Session_Number_Numeric__c];
//          }
//        }
    }

/*
    public override void SetConRefreshData() {
        SessionInfo = setCon.GetRecords();
    }
*/

    private string mtype{get;set;}
    public string getMType() {
        if (mtype == null || mtype == '') {
            mtype = ApexPages.CurrentPage().getParameters().get('mt');
            if (mtype == null) {
                mtype = '';
            }
        }
        return mtype;
    }
    public void setMType(string s) {
        mtype = s;
    }

    transient List<Conference_Session__c> SessionInfo;
    public List<Conference_Session__c> getSessionInfo() {
        if (SessionInfo == null || SessionInfo.size() == 0) {
            SetQuery();
        }
        return SessionInfo;
    }
    
    public integer SessionInfoSize {
        get {
            if (SessionInfo == null) {
                return 0;
            } else {
                //return setCon.getResultSize();
                return SessionInfo.size();
            }
        }
    }
    
    transient List<Conference_Proposal__c> ProposalInfo;
    public List<Conference_Proposal__c> getProposalInfo() {
        if (ProposalInfo == null || ProposalInfo.size() == 0) {
            SetQuery();
        }
        return ProposalInfo;
    }
    
    public integer ProposalInfoSize {
        get {
            if (ProposalInfo == null) {
                return 0;
            } else {
                return ProposalInfo.size();
            }
        }
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == SLIST_OTHER) {
            WizardStep = 2;
        } else if (RTypeSel == SLIST_CARCFE) {
            WizardStep = 3;
        } else if (RTypeSel == SLIST_NAB) {
            WizardStep = 4;
        } else if (RTypeSel == SLIST_NASBA) {
            WizardStep = 5;
        } else if (RTypeSel == SLIST_FL) {
            WizardStep = 6;
        } else if (RTypeSel == SLIST_KS) {
            WizardStep = 7;
        } else if (RTypeSel == SLIST_AW) {
            WizardStep = 8;
        } else if (RTypeSel == SLIST_CFRE) {
            WizardStep = 9;
        }
        SetQuery();
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    
}