@IsTest
public class TestBatchSchedUpdateStatusOnEndDateEry
{
    static testmethod void Exercise_Batch_Sched_UpdateStatusOnEndDateExpiry() {
        Batch_Sched_UpdateStatusOnEndDateExpiry bcs = new Batch_Sched_UpdateStatusOnEndDateExpiry();
        bcs.execute(null);
    }
    
    static testmethod void Exercise_Batch_UpdateStatusOnEndDateExpiry() {
          //Create Test Suscriptions
          // Test Entity
          NU__Entity__c TestEntity = new NU__Entity__c(name='Test Entity');
          insert TestEntity;
          
          // Test General Ledger Account
          NU__GLAccount__c myGLAccount = new NU__GLAccount__c(name='xxx-112-xxx', NU__Entity__c = TestEntity.Id );
          insert myGLAccount;
          
          //Online Subscription Record Type
          Id Prod_Record_Type_Id = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName().get('Online Subscription').getRecordTypeId();
          
          // Test Subscription Products        
          NU__Product__c product1 = 
              new NU__Product__c(name='Test Member Default, Not Restricted', 
                       RecordTypeId = Prod_Record_Type_Id,
                       NU__Entity__c = TestEntity.Id,
                       NU__RevenueGLAccount__c = myGLAccount.id,
                       NU__DisplayOrder__c=1, 
                       NU__ListPrice__c=0, 
                       NU__QuantityMax__c=1,
                       Membership_Default__c = false, 
                       Membership_Restricted__c = false);    
    
          insert product1;
        
          //Create New Provider
          Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
          
          //Create Membership
          DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
          
          //Create New Person, Attach to Company  
          Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();

          NU__Affiliation__c aff = new NU__Affiliation__c(NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
            insert aff;
            
          //Give New Person a subscription prior to creating the affiuliation
          Online_Subscription__c ExistingSub = new Online_Subscription__c(Account__c = individualAccount.Id, Product__c = product1.Id, System_End_Date__c = datetime.now(), Status__c = 'Held');
          insert ExistingSub;
          
          individualAccount.PersonEmail = 'sleadknight+ktester@gmail.com';
          individualAccount.PersonEmailBouncedDate = null;
          individualAccount.PersonEmailBouncedReason = null;
          update individualAccount;
        
        Batch_UpdateStatusOnEndDateExpiry b = new Batch_UpdateStatusOnEndDateExpiry();
        List<SObject> sl = new List<SObject>();
        sl.add(ExistingSub);
        Test.StartTest();
        ID batchprocessid = Database.executeBatch(b);
        b.execute(null, sl);
        Test.StopTest();
    }
}