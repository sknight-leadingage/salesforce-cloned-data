public with sharing class SessionPlannerMainController {
    
    private List<NU__Event__c> conferencesPriv = null;
    public List<NU__Event__c> Conferences{
        get{
            if (conferencesPriv == null){
                conferencesPriv = EventQuerier.getConferences();
            }

            return conferencesPriv;
        }
    }
    
    private List<SelectOption> ConferenceSelectOptsPriv = null;
    public List<SelectOption> ConferenceSelectOpts {
        get{
            if (ConferenceSelectOptsPriv == null){
                ConferenceSelectOptsPriv = PageUtil.getSelectOptionsFromSObjects(Conferences);
            }

            return ConferenceSelectOptsPriv;
        }
    }
    
    public Id SelectedConferenceId { get; set; }
    
    public PageReference viewConferenceProposals(){
        return redirectToConferencePage(Page.ConferenceProposals);
    }
    
    public PageReference viewConferenceSessions(){
        return redirectToConferencePage(Page.ConferenceSessions);
    }
    
    public PageReference viewConferenceSessionsHub(){
        return redirectToConferencePage(Page.ConferenceSessionsHub);
    }

    public PageReference viewConferenceSpeakers(){
        return redirectToConferencePage(Page.ConferenceSpeakers);
    }
    
    public PageReference viewConferenceSpeakersHub(){
        return redirectToConferencePage(Page.ConferenceSpeakersHub);
    }

    public PageReference viewConferenceCategories(){
        return redirectToConferencePage(Page.ConferenceCategories);
    }
    
    public PageReference viewConferenceReports(){
        return redirectToConferencePage(Page.ConferenceReports);
    }
    
    private PageReference redirectToConferencePage(PageReference page){
        PageReference conferencePage = page;
        
        setSelectedEvent(conferencePage);
        
        return conferencePage;
    }
    
    private void setSelectedEvent(PageReference conferenceMgmtPage){
        Map<String, String> queryParms = conferenceMgmtPage.getParameters();
        queryParms.put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, SelectedConferenceId);
    }
}