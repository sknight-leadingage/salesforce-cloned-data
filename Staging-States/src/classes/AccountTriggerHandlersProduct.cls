public with sharing class AccountTriggerHandlersProduct extends NU.TriggerHandlersBase {
    public static Set<Id> AllowedPrimaryAffiliationChangedIds = new Set<Id>();
    public static Set<Id> MergedAccountIds = new Set<Id>();

    public static Id CurrentEntityId { get; set; }

    private static Boolean isPrimaryAffiliationChanged(Account newAcct, Account oldAcct){
        return (newAcct.NU__PrimaryAffiliation__c != null &&
                oldAcct == null)

               ||

               (oldAcct != null &&
                oldAcct.NU__PrimaryAffiliation__c != newAcct.NU__PrimaryAffiliation__c);

    }

    private static Boolean isPrimaryAffiliationRecordChanged(Account newAcct, Account oldAcct) {
        return (newAcct.NU__PrimaryAffiliationRecord__c != null && oldAcct == null) ||
                (oldAcct != null && oldAcct.NU__PrimaryAffiliationRecord__c != newAcct.NU__PrimaryAffiliationRecord__c);
    }

    private static String setUsernameToLowerCase(Account acct, String newUsername){
        String usernameLower = newUsername.toLowerCase();
        acct.NU__Username__c = usernameLower;
        return usernameLower;
    }

    private static String getDefaultUserNameFromName(Account personAcct){
        String firstName = (String) personAcct.get('FirstName');
        String lastName = (String) personAcct.get('LastName');

        return getDefaultUserNameFromName(firstName, lastName);
    }

    private static String getDefaultUserNameFromName(String firstName, String lastName) {
        if (lastName == null) {
            return null;
        }
        String name = lastName;
        if (firstName != null && firstName.length() > 0) {
            name = firstName.substring(0, 1) + name;
        }
        return name.toLowerCase();
    }

    private static Set<String> getExistingDefaultUsernamesFromNames(List<Account> newAccts) {
        Set<String> defaultUserNames = new Set<String>();

        for (Account newAcct : newAccts){
            if (Boolean.valueOf(newAcct.get('IsPersonAccount'))) {
                String defaultUserName = getDefaultUserNameFromName(newAcct);
                if (!String.isBlank(defaultUserName)) {
                    defaultUserNames.add(defaultUserName);
                }
            }
        }

/*        List<Account> existingAccts = NU.AccountManager.Instance.getAccountsByUsernames(defaultUserNames); */
        Set<String> existingDefaultUserNames = new Set<String>();
/*        for (Account existingAcct : existingAccts) {
            existingDefaultUserNames.add(existingAcct.NU__Username__c);
        }
*/        return existingDefaultUserNames;
    }

    private static Set<String> getExistingEmails(List<Account> newAccts) {
        Set<String> newEmails = new Set<String>();
        for (Account newAcct : newAccts) {
            String email = (String)newAcct.get('PersonEmail');
            if (email != null) {
                newEmails.add(email);
            }
        }
        Set<String> result = new Set<String>();
        if (newEmails.size() > 0) {
/*            for (Account existingAcct : NU.AccountManager.Instance.getAccountsOfExistingEmails(newEmails, newAccts)) {
                result.add(existingAcct.NU__PersonEmail__c);
            }
*/        }
        return result;
    }

    private static void beforeInsertOrUpdate(List<Account> newAccts, Map<Id, Account> oldRecords){
        Set<Account> invalidAccts = new Set<Account>();
        Boolean isInsert = oldRecords == null;
        Boolean isValid = true;

        String LoginType = 'Email'; //NU.NimbleAMSSettingsManager.get().NU__LoginType__c;
        Set<String> existingDefaultUsernameAccounts = null;
        Set<String> existingEmails = null;
        Boolean inOrderProcessSubmit = false;
        if (LoginType == 'Email' && !inOrderProcessSubmit) {
            existingEmails = getExistingEmails(newAccts);
        } else if(isInsert && !inOrderProcessSubmit){
            existingDefaultUsernameAccounts = getExistingDefaultUsernamesFromNames(newAccts);
        }

        for (Account newAcct : newAccts){

            if (newAcct.NU__Username__c != null) {
                setUsernameToLowerCase(newAcct, newAcct.NU__Username__c);
            }

            if (newAcct.NU__PrimaryAffiliation__c == null) {
                newAcct.NU__PrimaryAffiliationRecord__c = null;
            }

            Boolean isPersonAccount = Boolean.valueOf(newAcct.get('IsPersonAccount'));

/*            NU.AccountAddressManager.coerceStatesWithinAddresses(newAcct, isPersonAccount);
*/
            if (isPersonAccount == true){

                newAcct.NU__PersonEmail__c = (String)newAcct.get('PersonEmail');

                isValid = true; //NU.AccountAddressManager.validatePersonAccount(newAcct);

                //don't worry about username population if we are in the
                //order process
                if (isValid == true && !inOrderProcessSubmit){
/*                    if (LoginType == 'Email' &&
                            NU.StringUtil.isNullOrEmpty(newAcct.NU__PersonEmail__c) == false){

                        if (existingEmails.contains(newAcct.NU__PersonEmail__c)) {
                            newAcct.NU__PersonEmail__c.addError('The email address "' + newAcct.NU__PersonEmail__c + '" is already in use');
                        } else {
                            setUsernameToLowerCase(newAcct, newAcct.NU__PersonEmail__c);
                        }
                    }
*/
/*                    if (LoginType == 'Username' &&
                        StringUtil.isNullOrEmpty(newAcct.NU__Username__c) &&
                        isInsert) {

                        String newUserName = getDefaultUserNameFromName(newAcct);
                        if (!String.isBlank(newUserName) && existingDefaultUsernameAccounts.contains(newUserName) == false) {
                            existingDefaultUsernameAccounts.add(setUsernameToLowerCase(newAcct, newUsername));
                        }
                    }
*/                }
            }

            if (isValid == false){
                invalidAccts.add(newAcct);
            }
        }

/*        NU.AccountAddressManager.beforeInsertOrUpdate(newAccts, invalidAccts);
*/    }

    private static void handlePrimaryAffiliationOrContactInfoChanged(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts) {

        List<NU__Affiliation__c> affilsToUpsert = new List<NU__Affiliation__c>();
        List<Account> updatedAccountsForPrimaryContact = new List<Account>();
        List<NU__Membership__c> updatedMembershipsForPrimaryContact = new List<NU__Membership__c>();
        List<Account> findAffiliations = new List<Account>();
        List<Account> clearAffiliations = new List<Account>();

        for (Account account : newAccounts.values()) {
            Account oldAccount = oldAccounts != null ? oldAccounts.get(account.Id) : null;
            if (!AllowedPrimaryAffiliationChangedIds.contains(account.Id) &&
                    isPrimaryAffiliationChanged(account, oldAccount)) {
                if (account.NU__PrimaryAffiliation__c == null) {
                    clearAffiliations.add(account);
                } else {
                    findAffiliations.add(account);
                }
            }
        }
        if (clearAffiliations.size() > 0) {
            List<NU__Affiliation__c> affils = null; //NU.AffiliationManager.Instance.getActiveAffiliationsIdByAccField(clearAffiliations);
            for (NU__Affiliation__c affil : affils) {
                affil.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
            }
            affilsToUpsert.addAll(affils);
        }

        if (findAffiliations.size() > 0) {
            List<NU__Affiliation__c> affils = null; //NU.AffiliationManager.Instance.getActiveAffiliationsByAccField(findAffiliations);

            Map<Id, Map<Id,NU__Affiliation__c>> affilsByParentByChild = new Map<Id, Map<Id,NU__Affiliation__c>>();
            for (NU__Affiliation__c affiliation : affils) {
                Map<Id,NU__Affiliation__c> affilsByParent = affilsByParentByChild.get(affiliation.NU__Account__c);
                if (affilsByParent == null) {
                    affilsByParent = new Map<Id,NU__Affiliation__c>();
                    affilsByParentByChild.put(affiliation.NU__Account__c, affilsByParent);
                }
                affilsByParent.put(affiliation.NU__ParentAccount__c, affiliation);
            }

            for (Account account : newAccounts.values()) {
                // if an existing active affiliation is found, make it primary
                // otherwise, make a new affiliation
                NU__Affiliation__c current = null;
                Map<Id,NU__Affiliation__c> affilsByParent = affilsByParentByChild.get(account.Id);
                //check for primary contact info changing and if it needs to be updated
                if (affilsByParent != null) {
                    // found active affiliation to mark as primary?
                    current = affilsByParent.remove(account.NU__PrimaryAffiliation__c);
                    // all other affiliations should become inactive
                    for (NU__Affiliation__c affiliation : affilsByParent.values()) {
                        affiliation.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
                        affilsToUpsert.add(affiliation);
                    }
                }
                if (current == null) {
                    affilsToUpsert.add(new NU__Affiliation__c(NU__Account__c = account.Id,
                           NU__ParentAccount__c = account.NU__PrimaryAffiliation__c,
                           NU__IsPrimary__c = true));
                } else {
                    current.NU__IsPrimary__c = true;
                    affilsToUpsert.add(current);
                }
            }
        }
        // Don't restamp accounts if the affiliation is being deleted
        Boolean inOnDeleteForAffils = false;
        
        if(!inOnDeleteForAffils && oldAccounts != null) {
            // find accounts with changed name or email
            Set<Id> changedAccountIds = new Set<Id>();
            for (Account account : newAccounts.values()) {
                Account oldAccount = oldAccounts.get(account.Id);
                if (account.get('FirstName') != oldAccount.get('FirstName') ||
                        account.get('LastName') != oldAccount.get('LastName') ||
                        account.get('Salutation') != oldAccount.get('Salutation') ||
                        account.get('NU__MiddleName__c') != oldAccount.get('NU__MiddleName__c') ||
                        account.get('NU__Suffix__c') != oldAccount.get('NU__Suffix__c') ||
                        account.NU__PersonEmail__c != oldAccount.NU__PersonEmail__c) {
                    changedAccountIds.add(account.Id);
                }
            }

            if (changedAccountIds.size() > 0) {
                List<NU__Affiliation__c> affils = null; //AffiliationManager.Instance.getActiveAffiliationsByAccId(changedAccountIds);

                for (NU__Affiliation__c aff : affils) {
/*                    NU.AffiliationTriggerHandlers.populatePrimaryContactInfo(aff, null, updatedAccountsForPrimaryContact, updatedMembershipsForPrimaryContact, false);
*/                }
            }
        }
        if (affilsToUpsert.size() > 0) {
//            DML.Instance.upsertRecords(affilsToUpsert);
        }

        if (updatedAccountsForPrimaryContact.size() > 0) {
//                DML.Instance.updateRecords(updatedAccountsForPrimaryContact);
        }
        if (updatedMembershipsForPrimaryContact.size() > 0) {
//                DML.Instance.updateRecords(updatedMembershipsForPrimaryContact);
        }
    }

    private static void processMembershipFlowdown(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
/*        Map<Id, NU.IMembershipFlowdownManager2> membershipFlowdownManagers = NU.MembershipFlowdownFactory.create();

        if (membershipFlowdownManagers == null || membershipFlowdownManagers.isEmpty()){
            return;
        }

        Map<Id, Account> newAccountMap = (Map<Id, Account>)newRecordMap;
        Map<Id, Account> oldAccountMap = (Map<Id, Account>)oldRecordMap;

        // are we merging accounts? if so, reset membership lookups & join on fields on the old records, if necessary
        if (oldRecordMap != null && !oldRecordMap.isEmpty() && CollectionUtil.setHasElements(MergedAccountIds)) {
            oldAccountMap = resetMembershipLookupsOnMergingAccounts(newRecordMap, oldRecordMap, membershipFlowdownManagers.keySet());
        }

        for (Id flowdownMembershipTypeId : membershipFlowdownManagers.keySet()){
            NU.IMembershipFlowdownManager2 mgr = membershipFlowdownManagers.get(flowdownMembershipTypeId);
            mgr.onAccountsUpdated(oldAccountMap, newAccountMap, flowdownMembershipTypeId);
        }
*/    }

    private static Map<Id, Account> resetMembershipLookupsOnMergingAccounts(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap, Set<Id> membershipTypeIds) {
        Map<Id, Account> oldAccountMap = new Map<Id, Account>();
/*
        List<NU__MembershipType__c> membershipTypes = NU.MembershipTypeManager.Instance.getMembershipTypesByIds(membershipTypeIds);

        for (sObject oldRecord : oldRecordMap.values()) {
            Account oldAccount = (Account)oldRecord;

            if (MergedAccountIds.contains(oldAccount.Id)) {
                Account newAccount = (Account)newRecordMap.get(oldAccount.Id);
                Account oldAccountUpdated = oldAccount.clone(true, true, true, true);

                oldAccountMap.put(oldAccount.Id, oldAccountUpdated);

                if (newAccount == null) {
                    break;
                }

                /* For the old merged account record, if the membership lookup and join on fields of each membership type are not changing, but they are not null, set them to null
                   this will force membership flowdown to occur for the merged record, and all reparented children will receive the flowdown update
                   (depending what the membership flowdown rules are for the organization) */
/*                for (NU__MembershipType__c membershipType : membershipTypes) {
                    if (oldAccount.get(membershipType.NU__AccountMembershipField__c) != null &&
                        oldAccount.get(membershipType.NU__AccountMembershipField__c) == newAccount.get(membershipType.NU__AccountMembershipField__c)) {

                        oldAccountUpdated.put(membershipType.NU__AccountMembershipField__c, null);
                    }
                    if (oldAccount.get(membershipType.NU__AccountJoinOnField__c) != null &&
                        oldAccount.get(membershipType.NU__AccountJoinOnField__c) == newAccount.get(membershipType.NU__AccountJoinOnField__c)) {

                        oldAccountUpdated.put(membershipType.NU__AccountJoinOnField__c, null);
                    }
                }

                break;
            }

            oldAccountMap.put(oldAccount.Id, oldAccount);
        }
*/
        return oldAccountMap;
    }

    private static void checkPasswordChanged(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts) {
        if (NU__NimbleAmsPublicSettings__c.getInstance().NU__DisableEmails__c) {
            return;
        }

        for (Account account : newAccounts.values()) {
            Account oldAccount = oldAccounts.get(account.Id);
            if (account.NU__PersonEmail__c == null ||
                    account.NU__PasswordHash__c == null ||
                    oldAccount.NU__PasswordHash__c == null ||
                    account.NU__PasswordHash__c.equals(oldAccount.NU__PasswordHash__c)) {
                continue;
            }

/*            NU.EmailTemplates.SendEmail(getPasswordChangedTemplate(account), account.Id); */
        }
    }


    static Map<Id,NU__Entity__c> entities = null;
    static String ssTemplate = null, fallbackTemplate = null;
    public static String getPasswordChangedTemplate(Account account) {
        if (fallbackTemplate == null) {
            entities = null; //new Map<Id,NU__Entity__c>(NU.EntityManager.Instance.getPwdChangedTmpEntities());

            if (entities.containsKey(CurrentEntityId)) {
                ssTemplate = entities.get(CurrentEntityId).NU__PasswordChangedEmailTemplate__c;
            }
            else {
/*                Id defaultEntityId = NimbleAMSSettingsManager.get().NU__DefaultEntity__c;
                fallbackTemplate = entities.containsKey(defaultEntityId) ?
                        entities.get(defaultEntityId).NU__PasswordChangedEmailTemplate__c :
                        NU.EmailTemplates.TEMPLATE_PASSWORD_CHANGED;
*/            }
        }

        String emailTemplate = ssTemplate;
        if (emailTemplate == null) {
            emailTemplate = entities.containsKey(account.NU__PrimaryEntity__c) ?
                    entities.get(account.NU__PrimaryEntity__c).NU__PasswordChangedEmailTemplate__c :
                    fallbackTemplate;
        }
        return emailTemplate;
    }

    public override void onBeforeInsert(List<sObject> newRecords){
/*        NU.GeoLocator.setAccountsForUpdate(newRecords);
        beforeInsertOrUpdate(newRecords, null);
*/    }

    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        beforeInsertOrUpdate(newRecordMap.values(), (Map<Id, Account>) oldRecordMap);
        checkPasswordChanged((Map<Id, Account>)newRecordMap, (Map<Id, Account>)oldRecordMap);
/*        NU.AccountAddressManager.onBeforeUpdate((Map<Id, Account>)newRecordMap, (Map<Id, Account>)oldRecordMap);
*/    }

    private static Map<Id, Account> deletedAccounts = null;

    public override void onBeforeDelete(Map<Id, SObject> oldRecordMap) {
/*        deletedAccounts = new Map<Id, Account>(NU.AccountManager.Instance.getDeletedAccounts(oldRecordMap));
*/    }

    public override void onAfterInsert(Map<Id, SObject> newRecordMap) {
        populatePersonContact((List<Account>)newRecordMap.values());
        handlePrimaryAffiliationOrContactInfoChanged((Map<Id, Account>)newRecordMap, null);
    }

    public override void onAfterDelete(Map<Id, Sobject> oldRecordMap) {
        for (Account account : (List<Account>)oldRecordMap.values()) {
            Account temp = deletedAccounts.get(account.Id);

            if (account.MasterRecordId == null) {

                if (temp.NU__CommitteeMemberships__r.size() > 0 ||
                    temp.NU__Deals__r.size() > 0 ||
                    temp.NU__Donations__r.size() > 0 ||
                    temp.NU__Memberships__r.size() > 0 ||
                    temp.NU__Merchandise__r.size() > 0 ||
                    temp.NU__Miscellaneous__r.size() > 0 ||
                    temp.NU__Registrations2__r.size() > 0 ||
                    temp.NU__Subscriptions__r.size() > 0) {

                    account.addError('Account cannot be deleted because data on child records would be lost.');
                }
            }
            else{
                /* When merging, the merged account id along with the
                   deleted account ids have to be put in the AllowedPrimaryAffiliationChangedIds
                   set so that the Primary Affiliation logic is bypassed.
                */
                AllowedPrimaryAffiliationChangedIds.add(account.Id);
                AllowedPrimaryAffiliationChangedIds.add(account.MasterRecordId);

                /* When merging, if the deleted account had affiliations, store the merged account id
                   so that membership and address flowdown will occur to the reparented child account(s)
                */
                if (NU.CollectionUtil.listHasElements(temp.NU__Affiliates__r)) {
                    MergedAccountIds.add(account.MasterRecordId);
                }
            }
        }
    }

    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
/*        NU.AccountAddressManager.onAfterUpdate(newRecordMap, oldRecordMap);
*/        processMembershipFlowdown(newRecordMap, oldRecordMap);
/*        NU.CustomerEmailTracking.UpdateHistoryEmails((Map<Id, Account>)newRecordMap, (Map<Id, Account>)oldRecordMap);
*/
        handlePrimaryAffiliationOrContactInfoChanged((Map<Id, Account>)newRecordMap, (Map<Id, Account>)oldRecordMap);
    }

    /**
     * @description Populates the custom.NU__PersonContact__c field from the standard PersonContact
     *   field. This is done under a "without sharing" context as PersonContact is a system
     *   field that should be populated regardless of record visibility.
     * @param newAccounts The accounts for which to populate the.NU__PersonContact__c field.
     */
    private void populatePersonContact(List<Account> newAccounts) {
        // behavior with Private Account Sharing Model is tested by NC package

        List<Account> accountsToUpdate = new List<Account>();
        for (Account account : newAccounts) {
            Id personContactId = (Id)account.get('PersonContactId');
            if (personContactId != null) {
                accountsToUpdate.add(new Account(Id = account.Id,NU__PersonContact__c = personContactId));
            }
        }

/*        DML.WithoutSharingInstance.updateRecords(accountsToUpdate);
*/    }
}