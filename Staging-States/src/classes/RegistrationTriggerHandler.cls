public without sharing class RegistrationTriggerHandler extends NU.TriggerHandlersBase
{
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {
		for (sObject oldRegistrationObj: oldRecordMap.values()) {
			NU__Registration2__c oldRegistration = (NU__Registration2__c)oldRegistrationObj; 
			NU__Registration2__c newRegistration = (NU__Registration2__c)newRecordMap.get(oldRegistration.Id);
			
	  		if (newRegistration.NU__Status__c == 'Cancelled' && NewRegistration.Registration_Group__c != null) {
				// If Registration is being cancelled, and the user is part of a group registration
				// Then Retrieve Group Member record, and set the group status to cancelled as well.
				
				List<Registration_Group_Member__c> GroupMembers = [SELECT id,
			             Group_Status__c
			        FROM Registration_Group_Member__c
			       WHERE Account__c = : NewRegistration.NU__Account__c
			         AND Registration_Group__c = :NewRegistration.Registration_Group__c];
			
    		
				for (Registration_Group_Member__c member : GroupMembers){
					member.Group_Status__c = 'Cancelled';
				}
			    
				if (GroupMembers.size() > 0){
					update GroupMembers;
				}		   
			}
		}
    }
}