/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceTimeslotTriggerHandler {
	static Conference_Timeslot__c getConferenceTimeslotById(Id conferenceTimeslotId){
		return [select id,
		               name,
		               Start__c,
		               End__c,
		               Day__c,
		               Day_Sort__c,
		               Session_Date__c,
		               Session_Time__c,
		               Short_Slot__c,
		               Timeslot__c,
		               Timeslot_Code__c
		          from Conference_Timeslot__c
		         where id = :conferenceTimeslotId];
	}
	
	static DateTime mondayAprilFifteenth2013 = DateTime.newInstance(2013, 4, 15);
	
	static Conference_Timeslot__c createAprilFifteenth2013TS(){
		NU__Event__c anyEvent = NU.DataFactoryEvent.insertEvent();
        
        return DataFactoryConferenceTimeslot.createTimeslot(anyEvent.Id, mondayAprilFifteenth2013, mondayAprilFifteenth2013.addHours(12));
	}
	
	static Conference_Timeslot__c insertAprilFifteenth2013TS(){
        Conference_Timeslot__c april152013TS = createAprilFifteenth2013TS();
        insert april152013TS;
        
        return getConferenceTimeslotById(april152013TS.Id);
	}
	

    static testMethod void dayPopulatedFromStartTest() {
        Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
        
        system.assert(april152013TS != null);
        system.assert(april152013TS.Day__c == 'Monday');
    }
    
    static testmethod void sessionDatePopulatedFromStartTest(){
    	Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
        
        system.assert(april152013TS != null);
        system.assert(april152013TS.Session_Date__c == 'Monday, April 15, 2013');
    }

	static testmethod void sessionTimePopulatedFromStartAndEndTest(){
		Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
		
		system.assert(april152013TS != null);
        system.assert(april152013TS.Session_Time__c == '12:00 a.m. - 12:00 p.m.', 'The session time is not 12:00 a.m. - 12:00 p.m.');
	}

	static testmethod void sessionTimeWithoutFirstAMFromStartAndEndTest(){
		Conference_Timeslot__c april152013TS = createAprilFifteenth2013TS();
		
		april152013TS.End__c = april152013TS.Start__c.addHours(2);
		insert april152013TS;
		
		Conference_Timeslot__c april152013TSQueried = getConferenceTimeslotById(april152013TS.Id);
		
		
		system.assert(april152013TSQueried != null);
        system.assert(april152013TSQueried.Session_Time__c == '12:00 - 2:00 a.m.', 'The session time is not 12:00 - 2:00 a.m.');
	}
	
	static testmethod void sessionTimeWithoutFirstPMFromStartAndEndTest(){
		Conference_Timeslot__c april152013TS = createAprilFifteenth2013TS();
		
		april152013TS.Start__c = april152013TS.Start__c.addHours(14);
		april152013TS.End__c = april152013TS.Start__c.addHours(2);
		insert april152013TS;
		
		Conference_Timeslot__c april152013TSQueried = getConferenceTimeslotById(april152013TS.Id);
		
		
		system.assert(april152013TSQueried != null);
        system.assert(april152013TSQueried.Session_Time__c == '2:00 - 4:00 p.m.', 'The session time is not 2:00 - 4:00 p.m.');
	}
	
	static testmethod void timeslotFromStartAndEndTest(){
		Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
		
		system.assert(april152013TS != null);
        system.assert(april152013TS.Timeslot__c == 'Monday, April 15, 2013, 12:00 a.m. - 12:00 p.m.', 'The timeslot is not Monday, April 15, 2013, 12:00 a.m. - 12:00 p.m.');
	}
	
	static testmethod void shortTimeSlotFromStartAndEndTest(){
		Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
		
		system.assert(april152013TS != null);
        system.assert(april152013TS.Short_Slot__c == 'Monday, 12:00 a.m. - 12:00 p.m.', 'The timeslot is not Monday, 12:00 a.m. - 12:00 p.m.');
	}
	
	static testmethod void startTimeUpdatedTest(){
		Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
		
		// Set to 2am
		april152013TS.Start__c = april152013TS.Start__c.addHours(2);
		update april152013TS;
		
		
		Conference_Timeslot__c timeslot = getConferenceTimeslotById(april152013TS.Id);

		system.assert(timeslot != null);
		system.assert(timeslot.Day__c == 'Monday');
		system.assert(timeslot.Session_Date__c == 'Monday, April 15, 2013');
		system.assert(timeslot.Session_Time__c == '2:00 a.m. - 12:00 p.m.', 'The session time, ' + timeslot.Session_Time__c + ', is not 2:00 a.m. - 12:00 p.m.');
		system.assert(timeslot.Timeslot__c == 'Monday, April 15, 2013, 2:00 a.m. - 12:00 p.m.', 'The timeslot, ' + timeslot.Timeslot__c + ', is not Monday, April 15, 2013, 2:00 a.m. - 12:00 p.m.');
		system.assert(timeslot.Short_Slot__c == 'Monday, 2:00 a.m. - 12:00 p.m.', 'The timeslot is not Monday, 2:00 a.m. - 12:00 p.m.');
	}
	
	static testmethod void endTimeUpdatedTest(){
				Conference_Timeslot__c april152013TS = insertAprilFifteenth2013TS();
		
		// Set to 2am
		april152013TS.End__c = april152013TS.Start__c.addHours(2);
		update april152013TS;
		
		Conference_Timeslot__c timeslot = getConferenceTimeslotById(april152013TS.Id);

		system.assert(timeslot != null);
		system.assert(timeslot.Day__c == 'Monday');
		system.assert(timeslot.Session_Date__c == 'Monday, April 15, 2013');
		system.assert(timeslot.Session_Time__c == '12:00 - 2:00 a.m.', 'The session time, ' + timeslot.Session_Time__c + ', is not 12:00 - 2:00 a.m.');
		system.assert(timeslot.Timeslot__c == 'Monday, April 15, 2013, 12:00 - 2:00 a.m.', 'The timeslot, ' + timeslot.Timeslot__c + ', is not Monday, April 15, 2013, 12:00 - 2:00 a.m.');
		system.assert(timeslot.Short_Slot__c == 'Monday, 12:00 - 2:00 a.m.', 'The timeslot is not Monday, 12:00 - 2:00 a.m.');
	}
}