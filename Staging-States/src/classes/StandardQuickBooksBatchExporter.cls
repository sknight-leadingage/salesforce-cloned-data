global with sharing class StandardQuickBooksBatchExporter implements NU.IBatchExporter {
    public static final String EXPORT_FOLDER_NAME = 'GL Export';
    
    public static final String EXPORT_FILE_SPL_COL = '!SPL';
    public static final String EXPORT_FILE_SPL_ID_COL = 'SPLID';
    public static final String EXPORT_FILE_TRANS_TYPE_COL = 'TRNSTYPE';
    public static final String EXPORT_FILE_DATE_COL = 'DATE';
    public static final String EXPORT_FILE_ACCOUNT_COL = 'ACCNT';
    public static final String EXPORT_FILE_NAME_COL = 'NAME';
    public static final String EXPORT_FILE_MEMO_COL = 'MEMO';
    public static final String EXPORT_FILE_AMOUNT_COL = 'AMOUNT';
    
    public static final String EXPORT_FILE_GL_ACCOUNT_COL = 'GL Account';
    public static final String DETAIL_FILE_AMOUNT_COL = 'Amount';
    public static final String DETAIL_FILE_DESCRIPTION_COL = 'Description';
    public static final String DETAIL_FILE_TRANS_DATE_COL = 'TrxDate';
    
    public static final String TRANSACTION_TYPE_AR = 'AR';
    public static final String TRANSACTION_TYPE_PAYMENT = 'Payment';
    public static final String TRANSACTION_TYPE_INCOME = 'Income';
    
    public static final String TRANSACTION_CATEGORY_DEBIT = 'Debit';
    public static final String TRANSACTION_CATEGORY_CREDIT = 'Credit';
    
    public static final String NEWLINE = '\r\n';
    public static final String TAB_DELIMITER = '\t';

    public static final String TRANS = 'Trans';
    public static final String SPL = 'SPL';
    public static final String GENERAL_JOURNAL = 'GENERAL JOURNAL';
    public static final String NIMBLE_AMS = 'NimbleAMS';
    
    public static final String NO_BATCH_IDS_TO_EXPORT_ERR_MSG = 'No batch ids were given to export.';
    
    public class Row
    {
        public Date transactionDate { get; set; }
        public String transactionDateString { get; set; }
        public String batchNumber { get; set; }
        public Integer rowNumber { get; set; }
        public String glAccount { get; set; }       
        public String company { get; set; }
        public String title { get; set; }
        public String detail { get; set; }
        public Decimal debit { get; set; }
        public Decimal credit { get; set; }
        public Decimal value { get; set; }
        
        public String Memo {
        	get {
        		return title + ',' + company + ',' + detail;
        	}
        }
        
        public String TransString { get { return TRANS; } }
        public String SplString { get { return SPL; } }
        public String GeneralJournalString { get { return GENERAL_JOURNAL; } }
        public String NimbleAMSString { get { return NIMBLE_AMS; } }
    }
    
    public List<NU__Batch__c> SelectedBatches { get; set; }
    
    public Decimal TotalDebit { get; set; }
    public Decimal TotalCredit { get; set; }
    
    private String ExportFileContents { get; set; }
    private Document exportDocument { get; set; }
    
    private String FileNameBase {
        get{
            return 'GLExport-' + SelectedBatches[0].NU__Entity__r.Name + '-' +  DateTime.now().format().replace(' ', '-');
        }
    }
    
    private String ExportFileName{
        get{
            return FileNameBase + '.iif';
        }
    }
    
    /**
     * @description Returns the Component Output Panel of the Batch Export Preview View for the Set of Batch Ids passed in.
     * @param Set<Id> Set of Batch Id records
     * @return Component.Apex.OutputPanel
     */
    global Component.Apex.OutputPanel getPreviewComponent(Set<Id> batchesToExportIds){
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        List<NU__Batch__c> batches = getBatchesByIds(batchesToExportIds);
        
        
        Component.Apex.PageBlock detailPB = new Component.Apex.PageBlock();
        detailPB.title = 'Export File';
        

        List<Row> batchesRows = getRowsFromBatches(batchesToExportIds);
        
        
        Component.Apex.PageblockTable transTable = createTransTable(batchesRows[0]);
        Component.Apex.PageblockTable splTable = createSPLTable(batchesRows);
        
        detailPB.childComponents.add(transTable);
        detailPB.childComponents.add(splTable);
        op.ChildComponents.add(detailPB);
        
        return op;
    }

    /**
     * @description Performs the export on the specified Set of Batch Ids and returns a BatchExportResult.
     * If no Ids are passed in, the BatchExportResult returned will contain an error message.
     * @param Set<Id> Set of Batch Id records
     * @return a BatchExportResult
     */
    global NU.BatchExportResult export(Set<Id> batchesToExportIds){
        NU.BatchExportResult ber = new NU.BatchExportResult();
        
        if (NU.CollectionUtil.setHasElements(batchesToExportIds) == false){
            NU.OperationResult.addErrorMessage(ber.Result, NO_BATCH_IDS_TO_EXPORT_ERR_MSG);
            return ber;
        }
        
        
        SelectedBatches = getBatchesByIds(batchesToExportIds);
        
        String exportFileContents = getExportFileContents(batchesToExportIds);
        
        insertExportDocument(exportFileContents);
        
        
        
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        Component.Apex.PageMessage pm = new Component.Apex.PageMessage();
        
        pm.Summary = 'The batch(es) were successfully exported.';
        pm.strength = 2;
        pm.severity = 'Info';
        
        op.ChildComponents.add(pm);
        
        Component.Apex.PageBlock exportFilesPB = new Component.Apex.PageBlock();
        
        exportFilesPB.title = 'Quickbooks Export File';
        
        Component.Apex.OutputLink exportOL = new Component.Apex.OutputLink();
        
        exportOL.value = '/' + exportDocument.Id;
        exportOL.target = '_blank';
        
        Component.Apex.OutputText exportOT = new Component.Apex.OutputText();
        exportOT.value = 'Quickbooks Export File';
        
        exportOL.ChildComponents.add(exportOT);
        
        op.ChildComponents.add(exportFilesPB);
        
        exportFilesPB.ChildComponents.add(exportOL);
        
        ber.SuccessView = op;
        ber.Result = new NU.OperationResult();
        
        return ber;
    }
    
    private Component.Apex.PageblockTable createTransTable(Row firstRow){
    	Component.Apex.PageblockTable previewTable = new Component.Apex.PageblockTable();
        previewTable.value = new List<Row>{ firstRow };
        previewTable.var = 'row';
        
        /*
        
        SPL{!TB}{!TB}
        GENERAL JOURNAL{!TB}
        <apex:outputText value="{0,date,MM/dd/yyyy}"><apex:param value="{!row.transactionDate}"/></apex:outputText>{!TB}
        {!row.glAccount}{!TB}
        Salesforce{!TB}
        {!row.title},{!row.company},{!row.detail}{!TB}
        <apex:outputText value="{0,number,0.00}"><apex:param value="{!row.value}"/></apex:outputText>{!CR}
        */

        addPreviewColumn(previewTable, '{!row.TransString}', TRANS);

        addPreviewColumn(previewTable, '{!row.GeneralJournalString}', GENERAL_JOURNAL);
        
        addPreviewColumn(previewTable, '{!row.transactionDateString}', EXPORT_FILE_DATE_COL);
        
        addPreviewColumn(previewTable, '{!row.glAccount}', EXPORT_FILE_ACCOUNT_COL);
        addPreviewColumn(previewTable, '{!row.NimbleAMSString}', EXPORT_FILE_NAME_COL);
        
        Component.Apex.Column amountColumn = addPreviewColumn(previewTable, '{!row.value}', EXPORT_FILE_AMOUNT_COL);
        rightAlignColumn(amountColumn);
        
        return previewTable;
    }
    
    private Component.Apex.PageblockTable createSPLTable(List<Row> batchesRows){
    	Component.Apex.PageblockTable previewTable = new Component.Apex.PageblockTable();
    	
    	// The first row is the Trans row so remove it so that it's not imported twice.
    	batchesRows.remove(0);
    	
        previewTable.value = batchesRows;
        previewTable.var = 'row';
        
        /*
        
        SPL{!TB}{!TB}
        GENERAL JOURNAL{!TB}
        <apex:outputText value="{0,date,MM/dd/yyyy}"><apex:param value="{!row.transactionDate}"/></apex:outputText>{!TB}
        {!row.glAccount}{!TB}
        Salesforce{!TB}
        {!row.title},{!row.company},{!row.detail}{!TB}
        <apex:outputText value="{0,number,0.00}"><apex:param value="{!row.value}"/></apex:outputText>{!CR}
        */

        addPreviewColumn(previewTable, '{!row.SplString}', 'SPL');

        addPreviewColumn(previewTable, '{!row.GeneralJournalString}', GENERAL_JOURNAL);
        
        addPreviewColumn(previewTable, '{!row.transactionDateString}', EXPORT_FILE_DATE_COL);
        
        addPreviewColumn(previewTable, '{!row.glAccount}', EXPORT_FILE_ACCOUNT_COL);
        addPreviewColumn(previewTable, '{!row.NimbleAMSString}', EXPORT_FILE_NAME_COL);
        
        addPreviewColumn(previewTable, '{!row.memo}', EXPORT_FILE_MEMO_COL);
        
        Component.Apex.Column amountColumn = addPreviewColumn(previewTable, '{!row.value}', EXPORT_FILE_AMOUNT_COL);
        rightAlignColumn(amountColumn);
        
        return previewTable;
    }

    private void rightAlignColumn(Component.Apex.Column column){
        column.headerStyle = 'text-align: right;';
        column.style = column.headerStyle;
    }
    
    private Component.Apex.Column addPreviewColumn(Component.Apex.PageBlockTable previewTable, String columnExpression, String headerValue){
        Component.Apex.Column column = new Component.Apex.Column();
        column.expressions.value = columnExpression;
        
        if (String.isNotBlank(headerValue)){
            column.headerValue = headerValue;
        }

        previewTable.childComponents.add(column);
        
        return column;
    }
    
    private List<Row> getRowsFromBatches(Set<Id> batchIds){
        List<Row> rows = new List<Row>();
        
        List<NU__Transaction__c> transactions =
        [select id,
                name,
                NU__Batch__c,
                NU__Batch__r.Name,
                NU__Date__c,
                NU__GLAccount__c,
                NU__GLAccount__r.Name,
                NU__Amount__c,
                NU__Entity__c,
                NU__Entity__r.Name,
                NU__Type__c,
                NU__ProductCode__c,
                NU__PaymentLine__r.NU__Payment__r.NU__EntityPaymentMethod__r.NU__PaymentMethod__r.Name,
                NU__Category__c
           from NU__Transaction__c
          where NU__Batch__c in :batchIds
          order by NU__Date__c, NU__Batch__r.Name, name];
          
        
        rows = buildRows(transactions);
          
          
        return rows;
    }

    public List<Row> buildRows(List<NU__Transaction__c> transactions)
    {
        List<Row> myrows = new List<Row>();
        
        for (NU__Transaction__c lineItem : transactions)
        {           
            Row row = new Row();
            row.transactionDate = lineItem.NU__Date__c;
            row.transactionDateString = lineItem.NU__Date__c.format();
            
            row.batchNumber = lineItem.NU__Batch__r.Name;
            row.glAccount = lineItem.NU__GLAccount__r.Name;
            row.value = lineItem.NU__Amount__c;
            row.company = lineItem.NU__Entity__r.Name;

            if (lineItem.NU__Type__c == TRANSACTION_TYPE_AR)
            {
                row.title = 'Accounts Receivable';
                row.detail = TRANSACTION_TYPE_AR;
            }
            else if (lineItem.NU__Type__c == TRANSACTION_TYPE_INCOME)
            {
                row.title = lineItem.NU__Type__c;
                row.detail = lineItem.NU__ProductCode__c;
            }
            else if (lineItem.NU__Type__c == TRANSACTION_TYPE_PAYMENT)
            {
                row.title = lineItem.NU__Type__c;
                row.detail = lineItem.NU__PaymentLine__r.NU__Payment__r.NU__EntityPaymentMethod__r.NU__PaymentMethod__r.Name; 
            }
            
            if (lineItem.NU__Category__c == TRANSACTION_CATEGORY_CREDIT){
                row.value = lineItem.NU__Amount__c * -1;    
            }
            
            //if there is already a matching row, just combine them
            Boolean found = false;
            for (Row r : myrows)
            {
                if (
                    r.transactionDate == row.transactionDate &&
                    r.batchNumber == row.batchNumber &&
                    r.glAccount == row.glAccount &&
                    r.company == row.company &&
                    r.title == row.title &&
                    r.detail == row.detail)
                {
                    found = true;
                    r.value += row.value;
                    break;
                }
            }
            if (!found)
            {
                myrows.add(row);
            }
        }

        //remove empty rows, split value into debit/credit, add row numbers
        TotalDebit = 0;
        TotalCredit = 0;
        for (Integer i = 0; i < myrows.size(); i++)
        {
            Row r = myrows[i];
            if (r.value == 0)
            {
                myrows.remove(i);
                i--;
            }
            else
            {
                if (r.value > 0)
                {
                    r.debit = r.value;
                    r.credit = 0;
                }
                else
                {
                    r.debit = 0;
                    r.credit = -r.value;
                }
                TotalDebit += r.debit;
                TotalCredit += r.credit;
                r.rowNumber = i + 1;
            }
        }
        
        return myrows;
    }
    
    private String getExportFileContents(Set<Id> batchesToExportIds){
        List<Row> exportRows = getRowsFromBatches(batchesToExportIds);

        String body = getExportFileColumnLine();
        
        Row firstRow = exportRows[0];
        
        // A newline isn't needed here because the first spl row will add it.
        String transBeginLine =
        'TRNS' + TAB_DELIMITER +
        'GENERAL JOURNAL' + TAB_DELIMITER + 
        firstRow.transactionDateString + TAB_DELIMITER +
        NIMBLE_AMS + TAB_DELIMITER +
        firstRow.glAccount + TAB_DELIMITER +
        firstRow.value;
        
        body += transBeginLine;

        /*
        
        SPL{!TB}{!TB}
        GENERAL JOURNAL{!TB}
        <apex:outputText value="{0,date,MM/dd/yyyy}"><apex:param value="{!row.transactionDate}"/></apex:outputText>{!TB}
        {!row.glAccount}{!TB}
        Salesforce{!TB}
        {!row.title},{!row.company},{!row.detail}{!TB}
        <apex:outputText value="{0,number,0.00}"><apex:param value="{!row.value}"/></apex:outputText>{!CR}
        */
        Integer exportRowsCount = exportRows.size();

        for (Integer i = 1; i < exportRowsCount; ++i){
            Row r = exportRows[i];

            body += 
              NEWLINE +
              'SPL' + TAB_DELIMITER + TAB_DELIMITER +
              r.GeneralJournalString + TAB_DELIMITER +
              r.transactionDateString + TAB_DELIMITER +
              r.glAccount + TAB_DELIMITER +
              NIMBLE_AMS + TAB_DELIMITER +
              r.Memo + TAB_DELIMITER +
              r.value;
        }
        
        /* ENDTRNS{!TB}{!TB}{!TB}{!TB}{!TB}{!TB}{!TB}{!CRLF} */
        
        String EndTransLine = 
        NEWLINE +
        'ENDTRNS' + TAB_DELIMITER +
        TAB_DELIMITER +
        TAB_DELIMITER +
        TAB_DELIMITER +
        TAB_DELIMITER +
        TAB_DELIMITER +
        TAB_DELIMITER +
        NEWLINE;
        
        body += EndTransLine;
        
        ExportFileContents = body;

        return body;
    }
    
    
    
    private String getExportFileColumnLine(){
    	String columnHeaderLine = 
    	'!TRNS' + TAB_DELIMITER +
    	'TRNSTYPE' + TAB_DELIMITER +
    	'DATE' + TAB_DELIMITER +
    	'NAME' + TAB_DELIMITER +
    	'ACCNT' + TAB_DELIMITER +
    	'AMOUNT' + NEWLINE;
    	
    	String splHeaderLine =
    	EXPORT_FILE_SPL_COL + TAB_DELIMITER +
    	EXPORT_FILE_SPL_ID_COL + TAB_DELIMITER +
    	EXPORT_FILE_TRANS_TYPE_COL + TAB_DELIMITER +
    	EXPORT_FILE_DATE_COL + TAB_DELIMITER +
    	EXPORT_FILE_ACCOUNT_COL + TAB_DELIMITER +
    	EXPORT_FILE_NAME_COL + TAB_DELIMITER +
    	EXPORT_FILE_MEMO_COL + TAB_DELIMITER + 
    	EXPORT_FILE_AMOUNT_COL + NEWLINE;
    	
    	String endTransLine =
    	'!ENDTRNS' + TAB_DELIMITER +
    	TAB_DELIMITER +
    	TAB_DELIMITER +
    	TAB_DELIMITER +
    	TAB_DELIMITER + NEWLINE;
    	
        String hfColLine = columnHeaderLine + splHeaderLine +
                           endTransLine;

        return hfColLine;
    }
    
    private void insertExportDocument(String exportContents){
        Folder exportFolder = [select id from Folder where name = :EXPORT_FOLDER_NAME];

        String quickbooksContentType = 'application/vnd.ms-excel#' + ExportFileName;
        
        exportDocument = new Document(
            Body = Blob.valueOf(exportContents),
            ContentType = quickbooksContentType,
            Name = ExportFileName,
            FolderId = exportFolder.Id
        );

        insert exportDocument;
    }
    
    private List<NU__Batch__c> getBatchesByIds(Set<Id> ids) {
        return
         [select
                Name,
                NU__DisplayName__c,
                NU__AR__c,
                NU__ExpectedTotalCashValue__c,
                NU__DifferenceCashValue__c,
                NU__ExportedOn__c,
                NU__Entity__c,
                NU__Entity__r.Name,
                NU__Entity__r.NU__BatchExportConfiguration__c,
                NU__Entity__r.NU__BatchExportConfiguration__r.Name,
                NU__Entity__r.NU__BatchExportConfiguration__r.NU__ExportGenerator__c,
                NU__Health__c,
                NU__PendingOn__c,
                NU__PostedOn__c,
                NU__Revenue__c,
                NU__Source__c,
                NU__Status__c,
                NU__Title__c,
                NU__TotalCashValue__c,
                NU__TransactionDate__c,
                NU__CartCount__c,
                Id,
                RecordTypeId,
                RecordType.Name,
                (Select id
                   from NU__Carts__r),
                (Select id,
                        name
                   from NU__Transactions__r)
           from NU__Batch__c
          where Id in :ids];
    }
}