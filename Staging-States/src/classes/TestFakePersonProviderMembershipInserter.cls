/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class inserts a generic individual membership for a given account for testing
 */
public with sharing class TestFakePersonProviderMembershipInserter implements ITestMembershipInserter {
	public NU__Membership__c insertMembership(Id accountId){
		NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
		
		NU__MembershipType__c providerMT = DataFactoryMembershipTypeExt.createProviderMembershipType(anyEntity.Id);
		providerMT.NU__Category__c = NU.Constant.MEMBERSHIP_TYPE_CATEGORY_INDIVIDUAL;
		
		insert providerMT;
		
		NU__Membership__c currentCalendarYearMembership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(accountId, providerMT.Id);
		insert currentCalendarYearMembership;
		
		return currentCalendarYearMembership;
	}
}