public with sharing class EduMaterials_Controller extends EduReportsControllerBase {
    private string mtype{get;set;}
    public string getMType() {
        if (mtype == null || mtype == '') {
            mtype = ApexPages.CurrentPage().getParameters().get('mt');
            if (mtype == null) {
                mtype = '';
            }
        }
        return mtype;
    }
    public void setMType(string s) {
        mtype = s;
    }
    
    public List<Conference_Session__c> SumList { get;set; }
    
    public string getPageSubtitle() {
        if (getMType() == 'sgn') {
            return 'Session Signs';
        }
        else if (getMType() == 'mgap') {
            return 'Monitor Guidelines (AM/PEAK)';
        }
        else if (getMType() == 'mgpa') {
            return 'Monitor Guidelines (PA)';
        }
        else if (getMType().left(3) == 'cev') {
            return 'ATTENDEE CE VERIFICATION FORM';
        }
        else if (getMType() == 'mona') {
            return 'MONITOR ASSESSMENT FORM';
        }
        else {
            return '';
        }
    }
    
    public override string getPageTitle() {
        return RptRenderAs == '' && Conference != null ? Conference.Name + getPageSubtitle() : '';
    }
    
    public EduMaterials_Controller() {
        SetConPageSize = 255;
        SetQuery();
    }
    
    public override void SetConRefreshData() {
        if (NeedsData()) {
            SumList = setCon.getRecords();
        }
    }
    
    public void SetQuery() {
        SetConQuery = '';
        string mtypeType = '';
        
        if (getMType() == 'sgn') {
            SetConQuery = 'SELECT Session_Title_Full__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        else if (getMType().left(3) == 'cev') {
            SetConQuery = 'SELECT Session_Title_Full__c, Timeslot__r.Session_Time__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        else if (getMType() == 'xpta') {
            SetConQuery = 'SELECT Session_Number__c, Session_Number_Numeric__c, Timeslot__r.Timeslot_Code__c, Expected_Attendance__c, Actual_Attendance__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        else if (getMType() == 'mona') {
            SetConQuery = 'SELECT Session_Title_Full__c, Expected_Attendance__c, Timeslot__r.Session_Time__c, (SELECT Id, Speaker__r.LeadingAge_Id__c, Speaker__r.NU__FullName__c, Speaker__r.FirstName, Speaker__r.LastName, Role__c FROM Conference_Session_Speakers__r) FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        
        if (!NeedsData()) {
            SetConQuery = 'SELECT Session_Number__c FROM Conference_Session__c WHERE Conference__c = :EventId LIMIT 1'; //Temp query to prevent errors
        }
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
    
    public boolean NeedsData() {
        if (getMType() == 'sgn' || getMType().left(3) == 'cev' || getMType() == 'xpta' || getMType() == 'mona') {
            return true;
        }
        else {
            return false;
        }
    }
}