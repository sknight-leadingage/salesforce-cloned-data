/* 
* Test Code for Manage Account Status 
*/
@isTest(SeeAllData=true)
private class TestManageAccountWizard {
     static final integer WizardMax = 15; //Remember to change this if more steps are added
    static Account StatePartner = null;
    static Account ProviderAccount = null;
    static Account ProviderAccountQueried = null;
    
    static NU__MembershipType__c JointStateProviderMT = null;
    static NU__Membership__c JointStateProviderMembership = null;
    
    static Joint_Billing__c JB = null;
    static Joint_Billing__c JBQueried = null;
    static Date JoinDateToUse = null;
    
    private static void loadTestData(){
    	if (JoinDateToUse == null){
    		JoinDateToUse = Date.valueOf(String.valueOf(Date.Today().year()) + '-01-01');
    	}
    	
        StatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
        ProviderAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, StatePartner.Id, JoinDateToUse);
        ProviderAccount.Billing_Type__c = 'Millage Standard';
        ProviderAccount.LeadingAge_Member__c = 'Member';
        ProviderAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);
        
        JointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        JointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
        
        JB = DataFactoryJointBilling.insertJointBillingWithItemAndItemLine(statePartner.Id, providerAccount.Id, jointStateProviderMembership.NU__EndDate__c, providerAccountQueried.Dues_Price__c);
        JBQueried = getJointBillingById(JB.Id);
    }
    
    private static void updateProviderAccountProviderJoinOn(Date providerJoinOn){
    	ProviderAccount.Provider_Join_On__c = providerJoinOn;
    	update ProviderAccount;
    }
    
    private static Joint_Billing__c getJointBillingById(Id jointBillingId){
        return [select id,
                       name,
                       Amount__c,
                       Bill_Date__c,
                       Start_Date__c,
                       End_Date__c
                  from Joint_Billing__c
                 where id = :jointBillingId];
    }
    
    private static Joint_Billing_Item_Line__c getFirstAdjustmentJointBillingItemLine(Id jointBillingId, Id providerId){
        return
        [select id,
                name,
                Amount__c,
                Dues_price__c,
                Program_Service_Revenue__c,
                Revenue_Year_Submitted__c
           from Joint_Billing_Item_Line__c
          where Joint_Billing_Item__r.Joint_Billing__c = :jointBillingId
            and Joint_Billing_Item__r.Account__c = :providerId
            and Type__c = :Constant.JOINT_BILLING_ITEM_LINE_TYPE_ADJUSTMENT
          order by createddate
          limit 1];
    }
    
    private static void updateJointBilling(){
        Test.startTest();
        
        new JointBillingUpdater().execute(null);
        
        Test.stopTest();
    }
     
     static testmethod void CheckPicklistsAndBaseVars() {
        Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
	    NU__Affiliation__c affTestProvInd = new NU__Affiliation__c(NU__EndDate__c = system.today().addyears(1),NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
	    insert affTestProvInd;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',individualAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
        
		List<SelectOption> testOpts = mc.getaccStatuses();
		system.assertNotEquals(testOpts.size(), 0, 'Account Status picklist contains no values.');
		
		testOpts.clear();
		testOpts = mc.getaffEnableList();
		system.assertNotEquals(testOpts.size(), 0, 'Affiliations picklist contains no values.');
		
		system.assertEquals(mc.WizardMax != null && mc.WizardMax > 0, true, 'Wizard Max is zero or null.');
     }
     

     static testmethod void ChangeStatusActivetoInactiveTest(){
     
        Account a = new Account(FirstName ='Joe', LastName ='Test', PersonEmail= 'joetest@test.org', NU__Status__c ='Active', RecordTypeId='012d0000000gc0Y');
        insert a;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',a.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(a, null, 'No User Record Found.');

        //Opening Step of Wizard
        system.assertEquals(1, mc.WizardStep);

        //Choose Yes
        //mc.optYesNoSelected='1';
        mc.WizardNext();
        system.assertEquals(1, mc.WizardStep);
        
        //Confirming to change the status
        a.NU__Status__c = 'Inactive';
        mc.WizardNext();
        
        //At the end of Wizard step
        system.assertEquals(1, mc.WizardStep);
        List<Account> tempAccount = [SELECT NU__Status__c FROM Account WHERE Id = :a.Id];
        
        for(Account acc: tempAccount){
            system.assertEquals('Active', acc.NU__Status__c);
        }
        
        List<NU__Affiliation__c> AffAccount = [SELECT NU__Status__c FROM NU__Affiliation__c WHERE NU__Account__c = :a.Id];
        
        for(NU__Affiliation__c aff: AffAccount){
            system.assertEquals('Active', aff.NU__Status__c);
        }
        
   }     
   static testmethod void ChangeStatusInactivetoActiveTest(){
        Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
	    NU__Affiliation__c affTestProvInd = new NU__Affiliation__c(NU__EndDate__c = system.today().addyears(1),NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
	    insert affTestProvInd;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',individualAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(individualAccount, null, 'No User Record Found.');

        //Opening Step of Wizard
        system.assertEquals(1, mc.WizardStep);

        //Choose Yes
        mc.accStatus='Active';
        mc.WizardNext();
        system.assertEquals(2, mc.WizardStep);
        
        //Confirming to change the status
        individualAccount.NU__Status__c = 'Active';
        
		List<SelectOption> testOpts = mc.getaffEnableList();
		system.assertNotEquals(testOpts.size(), 0, 'Affiliations picklist contains no values.');
        
        mc.affEnable = testOpts[0].getValue();
        mc.WizardNext();
        
        //At the end of Wizard step
        system.assertEquals(3, mc.WizardStep);
        List<Account> tempAccount = [SELECT NU__Status__c FROM Account WHERE Id = :individualAccount.Id];
        
        for(Account acc: tempAccount){
            system.assertEquals('Active', acc.NU__Status__c);
        }
        
        List<NU__Affiliation__c> AffAccount = [SELECT NU__Status__c FROM NU__Affiliation__c WHERE NU__Account__c = :individualAccount.Id];
        
        for(NU__Affiliation__c aff: AffAccount){
            system.assertEquals('Active', aff.NU__Status__c);
        }
        
    }

    
    static testmethod void TestingJointBillingPartofTheWizardNewMember(){
     
        //NU__RecordTypeName__c='Provider', LeadingAge_Member_Company__c= 'Non-Member',
        RecordType providerRT = AccountRecordTypeUtil.getProviderRecordType();
        loadTestData();
        Account b = new Account(Name='TestProvier',  LeadingAge_Member__c = 'Non-member', NU__Status__c ='Active', RecordTypeId=providerRT.Id, State_Partner_ID__c = StatePartner.Id, Revenue_Year_Submitted__c = string.valueof(system.today().year()), Program_Service_Revenue__c = 10000000);
        insert b;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',b.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(b, null, 'No User Record Found.');

        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);
        mc.selectedBillingType='Millage Standard';
        mc.WizardNext();

        //Chosen Billing Type
        mc.selectedBillingType='Millage Standard';
        mc.selectedJoinReason = 'Other';
        mc.WizardNext();
        system.assertEquals(12, mc.WizardStep);
        
        mc.WizardNext();
        mc.WizardStep = 14;
        mc.Done = true;
        mc.IndStatusSave();
//-------------------------------------------------------------------------------------------------------        
        mc.WizardStep = 10;
        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);

        //Chosen Billing Type
        mc.selectedBillingType='Under Construction';
        mc.WizardNext();
        system.assertEquals(12, mc.WizardStep);
		 mc.WizardNext();
		 mc.WizardStep = 14;
         mc.Done = true;
		 mc.IndStatusSave();
//-------------------------------------------------------------------------------------------------------        
        mc.WizardStep = 10;
        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);

        //Chosen Billing Type
        mc.selectedBillingType='Fixed Rate: Public Housing Authorities';
        mc.WizardNext();
        system.assertEquals(12, mc.WizardStep); 
         mc.WizardNext();
         mc.WizardStep = 14;
         mc.Done = true;    
         mc.IndStatusSave();
//-------------------------------------------------------------------------------------------------------        
        mc.WizardStep = 10;
        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);

        //Chosen Billing Type
        mc.selectedBillingType='Complimentary';
        mc.WizardNext();
        system.assertEquals(12, mc.WizardStep);  
         mc.WizardNext();   
         mc.WizardStep = 14;
         mc.Done = true;     
         mc.IndStatusSave();
//-------------------------------------------------------------------------------------------------------        
        mc.WizardStep = 10;
        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);

        //Chosen Billing Type
        mc.selectedBillingType='Corporate Program';
        mc.WizardNext();
        system.assertEquals(12, mc.WizardStep);   
         mc.WizardNext();
         mc.WizardStep = 14;
         mc.Done = true;   
         mc.IndStatusSave();

            
        
                            
        
        
        
    }
    
    	 static testmethod void TestingJointBillingPartofTheWizardNewMemberState(){
     
        //NU__RecordTypeName__c='Provider', LeadingAge_Member_Company__c= 'Non-Member',
        RecordType providerRT = AccountRecordTypeUtil.getProviderRecordType();
        loadTestData();
        Account b = new Account(Name='TestProvier',  LeadingAge_Member__c = 'Non-member', NU__Status__c ='Active', RecordTypeId=providerRT.Id, State_Partner_ID__c = StatePartner.Id, Revenue_Year_Submitted__c = string.valueof(system.today().year()), Program_Service_Revenue__c = 10000000);
        insert b;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',b.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        
        system.assertNotEquals(b, null, 'No User Record Found.');

        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);
        mc.selectedBillingType='Negotiated Rate';
        mc.selectedJoinReason = 'Other';
        b.Dues_Price_Override__c = 100;
        mc.IndStatusSave();
        
         //-------------------------------------------------------------------------------------------------------        
        mc.WizardStep = 10;
        //Opening Step of Wizard
        system.assertEquals(10, mc.WizardStep);

        //Chosen Billing Type
        mc.selectedBillingType='Negotiated Rate';
        mc.WizardNext();
        system.assertEquals(12, mc.WizardStep); 
         mc.WizardNext();
         mc.WizardNext();
         mc.WizardStep = 14;
         mc.Done = true;
         mc.IndStatusSave();
         
        
    	 }

        static testmethod void TestingJointBillingPartofTheWizardExtingMember(){
     
        //NU__RecordTypeName__c='Provider', LeadingAge_Member_Company__c= 'Non-Member',
        RecordType providerRT = AccountRecordTypeUtil.getProviderRecordType();
        loadTestData();
        Account b = new Account(Name='TestProvier',  LeadingAge_Member__c = 'Member', NU__Status__c ='Active', RecordTypeId=providerRT.Id, State_Partner_ID__c = StatePartner.Id);
        insert b;
        NU__Membership__c MJointBilling = DataFactoryMembershipExt.createCurrentProviderMembership(ProviderAccount.Id);
        insert MJointBilling;

        b.NU__Membership__c = MJointBilling.Id;
        update b;
		
        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',ProviderAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(b, null, 'No User Record Found.');

        //Opening Step of Wizard
        //system.assertEquals(10, mc.WizardStep);
        
        mc.actionTaken = 'Change Billing Type';
        mc.WizardNext();
		mc.selectedBillingType='Millage Standard';
		mc.WizardNext();
		mc.WizardStep = 14;
        mc.Done = true;
		mc.IndStatusSave();
		
		mc.WizardStep = 11;
		mc.actionTaken = 'Cancel Joint Billing';
        mc.WizardNext();
		mc.selectedCancellation = 'Bankruptcy';
		mc.WizardNext();
		mc.IndStatusSave();
		
		
		mc.WizardStep = 11;
		mc.actionTaken = 'Override Amount';
		b.Dues_Price_Override__c = 100;
        mc.WizardNext();
		mc.WizardNext();
		mc.WizardStep = 14;
         mc.Done = true;
		mc.IndStatusSave();
		
		mc.WizardStep = 10;
		mc.WizardNext();
		mc.IndStatusSave();
		mc.WizardStep = 11;
		mc.WizardNext();
		mc.IndStatusSave();
        mc.WizardStep = 12;
        mc.WizardNext();
        mc.IndStatusSave();
        mc.WizardStep = 13;
        mc.WizardNext();
        mc.Adjustment = true;
        mc.PSRNeed = true;
        
        mc.WizardStep = 12;
        mc.WizardNext();
        
        mc.Adjustment = false;
        mc.PSRNeed = false;
        
        mc.WizardStep = 12;
        mc.WizardNext();
        
        
        
        //mc.IndStatusSave();
        mc.WizardStep = 13;
        mc.WizardNext();
        mc.IndStatusSave();
        mc.getListOfBillingType();
        mc.getListOfCancellationType();
        mc.getListOfJoinReasonType();
        mc.getItems();
        
        mc.WizardStep = 2;
        mc.WizardBack();
        
        mc.WizardStep = 10;
        mc.HoldWizardStep = true;
        mc.actionTaken = 'Change Billing Type';
        mc.WizardBack();
        
       
        mc.changedM = MJointBilling; 
        mc.account.LeadingAge_Member__c = 'Member';
        mc.actionTaken = 'Change Billing Type';
        mc.account.RecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID; 
        mc.getItems();
        mc.IndStatusSave();
        
        
        
    }


static testmethod void TestingJointBillingPartofTheWizardInStatusSaveFun(){
     
        //NU__RecordTypeName__c='Provider', LeadingAge_Member_Company__c= 'Non-Member',
        RecordType providerRT = AccountRecordTypeUtil.getProviderRecordType();
        loadTestData();
        Account b = new Account(Name='TestProvier',  LeadingAge_Member__c = 'Member', NU__Status__c ='Active', RecordTypeId=providerRT.Id, State_Partner_ID__c = StatePartner.Id);
        insert b;
        NU__Membership__c MJointBilling = DataFactoryMembershipExt.createCurrentProviderMembership(ProviderAccount.Id);
        insert MJointBilling;

        b.NU__Membership__c = MJointBilling.Id;
        update b;
		
        PageReference testPr = Page.ManageAccount;
        
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',ProviderAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(b, null, 'No User Record Found.');    
        
        mc.changedM = MJointBilling; 
        mc.account.LeadingAge_Member__c = 'Member';
        mc.actionTaken = 'Change Billing Type';
        mc.selectedBillingType = 'Negotiated Rate';
        
        mc.account.Dues_Price_Override__c = 100;
        mc.account.RecordTypeId = providerRT.Id; 
        mc.IndStatusSave();
        
        mc.getListOfBillingType();
        mc.account.RecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID; 
        mc.getListOfBillingType();
        

		}

}