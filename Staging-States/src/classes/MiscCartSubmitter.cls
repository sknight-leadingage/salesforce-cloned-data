/**
 * @author NimbleAMS
 * @date Updated: 2/27/13
 * @description Performs actions when Miscellaneous Order Items are submitted through the Nimble AMS Order Process.
 * Implements IOrderItemTypeCartSubmitter.
 */
 global with sharing class MiscCartSubmitter implements NU.IOrderItemTypeCartSubmitter {
	private Map<Id, NU__Miscellaneous__c> cartItemLineMiscellaneousMap;
	private Map<NU__Miscellaneous__c, NU__OrderItemLine__c> MiscellaneousToUpdateMap;

	/**
	 * @description Creates a Miscellaneous record for all Cart Item Lines belonging to the Cart Item and Order Item passed in.
	 * If a Miscellaneous already exists for the Cart Item Line, the existing Miscellaneous record is updated and a new record is NOT created.
	 * This method puts these Miscellaneous records into a map, using the Id of the Cart Item Line as the key, and is maintained throughout the cart submission process..
	 * The Order Item parameter is optional - it is not used in this implementation of the IOrderItemTypeCartSubmitter.
	 * @param NU__CartItem__c the Cart Item that is being converted into an Order Item
	 * @param NU__OrderItem__c the Order Item that the Cart Item is being converted to
	 * @return a new instance of NU.OperationResult()
	 */
	global NU.OperationResult beforeOrderItemSaved(NU__CartItem__c cartItem, NU__OrderItem__c orderItem){
		NU.OperationResult result = new NU.OperationResult();

		if (cartItemLineMiscellaneousMap == null){
			// Have to instantiate this here because this class is created at run time and
			// private member initialization isn't executed.
			cartItemLineMiscellaneousMap = new Map<Id, NU__Miscellaneous__c>();
		}

		if (cartItem != null && NU.CollectionUtil.listHasElements(cartItem.NU__CartItemLines__r)){
			for (NU__CartItemLine__c cartItemLine : cartItem.NU__CartItemLines__r) {
				NU__Miscellaneous__c miscellaneous = createMiscellaneousFromCartItemLine(cartItem, cartItemLine);
				cartItemLineMiscellaneousMap.put(cartItemLine.Id, miscellaneous);
			}
		}

    	return new NU.OperationResult();
	}

	/**
	 * @description Saves the Miscellaneous records that were created in the beforeOrderItemSaved() method, if any were created.
	 * The NU.OperationResult() that is returned contains information on the result of saving the Miscellaneous records, specifically if the operation was successful or not.
	 * @return an instance of NU.OperationResult()
	 */
	global NU.OperationResult afterOrderItemsSaved(){
		NU.OperationResult saveResult = new NU.OperationResult();

    	if (cartItemLineMiscellaneousMap != null && cartItemLineMiscellaneousMap.isEmpty() == false) {
    		upsert cartItemLineMiscellaneousMap.values();
    		
			//saveResult = MiscellaneousManager.Instance.saveMiscellaneous(cartItemLineMiscellaneousMap.values());
    	}

    	return saveResult;
	}

	/**
	 * @description Grabs the Miscellaneous record from the map created in beforeOrderItemSaved() and populates the Lookup field to the Miscellaneous record on the Order Item Line.
	 * Because the Miscellaneous records will need to be updated later after the Order Item Line is saved, the Order Item Line is put in a map, using the Miscellaneous record as the key,
	 * and is maintained throughout the cart submission process.
	 * @param NU__CartItemLine__c the Cart Item Line used to retrieve the Miscellaneous record
	 * @param NU__OrderItemLine__c the Order Item Line that has the Miscellaneous Lookup field
	 */
	global void beforeOrderItemLineSaved(NU__CartItemLine__c cartItemLine, NU__OrderItemLine__c orderItemLineToSave){
		if (miscellaneousToUpdateMap == null){
			miscellaneousToUpdateMap = new Map<NU__Miscellaneous__c, NU__OrderItemLine__c>();
		}
		
		if (cartItemLineMiscellaneousMap != null && cartItemLineMiscellaneousMap.containsKey(cartItemLine.Id)){
			NU__Miscellaneous__c miscellaneous = cartItemLineMiscellaneousMap.get(cartItemLine.Id);
			orderItemLineToSave.NU__Miscellaneous__c = miscellaneous.Id;
			miscellaneousToUpdateMap.put(miscellaneous, orderItemLineToSave);
		}
	}

	/**
	 * @description Uses the Miscellaneous Key and Order Item Line Value from the map created in beforeOrderItemLineSaved() and populates the Lookup field to the Order Item Line
	 * record on the Miscellaneous record. The NU.OperationResult() that is returned contains information on the result of saving the Miscellaneous records, specifically if the operation was successful or not.
	 * @return an instance of NU.OperationResult()
	 */
	global NU.OperationResult afterOrderItemLinesSaved(){
		NU.OperationResult saveResult = new NU.OperationResult();

		if (miscellaneousToUpdateMap != null && !miscellaneousToUpdateMap.isEmpty()) {
        	for (NU__Miscellaneous__c miscellaneous : miscellaneousToUpdateMap.keySet()) {
        		miscellaneous.NU__OrderItemLine__c = miscellaneousToUpdateMap.get(miscellaneous).Id;
        	}
        	
        	List<NU__Miscellaneous__c> miscellaneousToUpdate = new List<NU__Miscellaneous__c>(miscellaneousToUpdateMap.keySet());
        	
        	upsert miscellaneousToUpdate;
        	
        	//saveResult = MiscellaneousManager.Instance.saveMiscellaneous(miscellaneousToUpdate);
        }

		return saveResult;
	}
	
	/**
	 * @description Does not do anything at this time.
	 * @param NU.CartSubmissionResult the NU.CartSubmissionResult instance of the current cart submission
	 */
	global void populateCartSubmissionResult(NU.CartSubmissionResult result){
		
	}
	
	private NU__Miscellaneous__c createMiscellaneousFromCartItemLine(NU__CartItem__c productCI, NU__CartItemLine__c productCIL) {
    	Id miscellaneousId = null;
    	
    	if (productCIL.NU__OrderItemLine__c != null) {
    		miscellaneousId = productCIL.NU__OrderItemLine__r.NU__Miscellaneous__c;
    	}
    	
    	NU__Miscellaneous__c miscellaneous = new NU__Miscellaneous__c(
    		Id = miscellaneousId,
    		NU__Account__c = productCI.NU__Customer__c,
    		NU__Product__c = productCIL.NU__Product__c
    	);
    	
    	return miscellaneous;
    }
}