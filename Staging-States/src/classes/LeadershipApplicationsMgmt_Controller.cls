public with sharing class LeadershipApplicationsMgmt_Controller {
    public List<Academy_Applicant__c> LeadershipApplications {get; set;}
    private List<Academy_Applicant__c> LeadershipApplications_orig {get; set;}
    public static final String ACADEMY_YEAR_PARAM = 'acadyear';
    //public string ApplicationStatus {get; set;}
    public boolean displayConfirmMSG {get; set;}
    public boolean displayErrorMSG {get; set;}
    
    public LeadershipApplicationsMgmt_Controller(){
            LeadershipApplications = getLeadershipApplications();
            LeadershipApplications_orig = LeadershipApplications.deepClone();
            
            displayConfirmMSG = false;
    }
    
    public string AcademyYear
    {
        get
        {
            return ApexPages.CurrentPage().getParameters().get(ACADEMY_YEAR_PARAM);
        }
   
    }
    
    private List<Academy_Applicant__c> getLeadershipApplications(){
    
      if( AcademyYear != null )
        {
        List<Academy_Applicant__c> Applications = [SELECT Name,  Account__r.Name, Academy_Year__c, Submission_Status__c, Application_Status__c FROM Academy_Applicant__c WHERE Submission_Status__c = 'Complete' AND Academy_Year__c = :AcademyYear ];
        
         return Applications;
        }
      return null;
    }
    
    public void Save(){
      try
          {
          List<Academy_Applicant__c> UpdatedAcademyApplicants = new List <Academy_Applicant__c>();
          
          string submittedStatus;
          string originalStatus;
          displayErrorMSG = false;
          
          for(Integer i=0; i < LeadershipApplications.size(); i++)
              {
                  submittedStatus = LeadershipApplications[i].Application_Status__c;
                  originalStatus = LeadershipApplications_orig[i].Application_Status__c;
              
                  if(submittedStatus != originalStatus)
                  {
                      UpdatedAcademyApplicants.add(LeadershipApplications[i]);
                  }
                  else
                  {
                      displayErrorMSG = true;
                  }
              
              }
          update UpdatedAcademyApplicants;
          displayConfirmMSG = true;
          displayErrorMSG = false;
          
          }
    catch (DMLException e)
        {
            ApexPages.addMessages(e); 
        }
        finally
        {
        }
    
    }
    
    
}