/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMultisiteProviderMembershipSetter {
	static void assertValidation(Id multiSiteOrgAccountId, String expectedValidationMsg){
		String validationMsg = '';
    	
        try {
        	new MultisiteProviderMembershipSetter(multiSiteOrgAccountId);
        }
        
        catch (Exception ex){
        	validationMsg = ex.getMessage();
        }
        
        TestUtil.assertValidationMessage(validationMsg, expectedValidationMsg);
	}
	
	static void setMSOProviderMembership(Id multiSiteOrgAccountId){
		new MultisiteProviderMembershipSetter(multiSiteOrgAccountId).set();
	}

	static void assertMultiSiteProviderMemberCount(Id multiSiteOrdAccountId, Integer expectedMSOProviderMemberCount){
		Account mso = [select id, name, Multi_site_Provider_Member_Count__c from Account where id = :multiSiteOrdAccountId];
		
		system.assertEquals(expectedMSOProviderMemberCount, mso.Multi_site_Provider_Member_Count__c);
	}

    static testMethod void nullMultiSiteOrgAccountIdValidationTest() {
    	assertValidation(null, MultiSiteProviderMembershipSetter.NULL_MULTI_SITE_ORG_ACCOUNT_ID);
    }
    
    static testmethod void msoNotFoundValidationTest(){
    	Account mso = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	delete mso;
    	
    	assertValidation(mso.Id, MultisiteProviderMembershipSetter.MULTI_SITE_ORG_NOT_FOUND);
    }
    
    static testmethod void notAnMSOValidationTest(){
    	Account provider = DataFactoryAccountExt.insertProviderAccount();
    	
    	assertValidation(provider.Id, MultisiteProviderMembershipSetter.ACCOUNT_IS_NOT_AN_MSO);
    }
    
    static testmethod void zeroMultiSiteProviderMemberCountTest(){
    	Account mso = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	setMSOProviderMembership(mso.Id);
    	
    	assertMultiSiteProviderMemberCount(mso.Id, 0);
    }
    
    static testmethod void oneMultiSiteProviderMemberCountTest(){
    	Account mso = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
    	NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, mso.Id, true);
    	
    	DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id);
    	
    	
    	setMSOProviderMembership(mso.Id);
    	
    	assertMultiSiteProviderMemberCount(mso.Id, 1);
    }
}