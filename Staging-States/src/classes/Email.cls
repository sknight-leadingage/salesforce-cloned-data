public with sharing class Email {
	
	public Email() {
		SaveAsActivity = true;
	}
	
	public Id TemplateId { get; set; }
	public Boolean SaveAsActivity { get; set; }	
	public Id OrgWideEmailAddressId { get; set; }
	public Id TargetObjectId { get; set; }
	public List<String> ToAddresses { get; set; }
	public Id WhatId { get; set; }
	
	public Messaging.SingleEmailMessage getEmail() {
		Messaging.SingleEmailMessage result = new Messaging.SingleEmailMessage();
		result.TemplateId = TemplateId;
		result.SaveAsActivity = SaveAsActivity;
		result.OrgWideEmailAddressId = OrgWideEmailAddressId;
		result.TargetObjectId = TargetObjectId;
		result.ToAddresses = ToAddresses;
		result.WhatId = WhatId;
		return result;
	}
}