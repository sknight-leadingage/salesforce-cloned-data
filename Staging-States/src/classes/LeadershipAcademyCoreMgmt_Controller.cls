public with sharing class LeadershipAcademyCoreMgmt_Controller {
    public static final String ACADEMY_YEAR_PARAM = 'acadyear';
    public List<Academy_Applicant__c> AcademyCoreMgmt {get; set;}

    
    public LeadershipAcademyCoreMgmt_Controller(){
            AcademyCoreMgmt = getAcademyCoreMgmt();

               
            //displayConfirmMSG = false;
    }
    
    public string AcademyYear
    {
        get
        {
            return ApexPages.CurrentPage().getParameters().get(ACADEMY_YEAR_PARAM);
        }
   
    }
    
    private List<Academy_Applicant__c> getAcademyCoreMgmt(){
    
      if( AcademyYear != null )
        {
        List<Academy_Applicant__c> AcademyList = [SELECT Id, Name, Account__c, Account__r.Name, Academy_Year__c, Submission_Status__c, Application_Status__c FROM Academy_Applicant__c WHERE Submission_Status__c = 'Complete' AND Academy_Year__c = :AcademyYear ];
        
         return AcademyList;
        }
      return null;
    }
    

    

}