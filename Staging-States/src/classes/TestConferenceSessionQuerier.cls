/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceSessionQuerier {

    static testMethod void getConferenceSessionsByConferenceIdTest() {
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceId(session.Conference__c);
        
        system.assert(NU.CollectionUtil.listHasElements(sessions), 'No session found');
    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest1(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.SessionNumber = 'non existent session number';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest2(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.ProposalNumber = '999';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest3(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.Company = 'non existent company';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest4(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.FirstName = 'non existent first name';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest5(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.LastName = 'non existent last name';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest6(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.Title = 'non existent title';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest7(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.TrackName = 'non existent track name';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest8(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.EducationStaffFirstName = 'non existent first name';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest9(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.EducationStaffLastName = 'non existent last name';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest10(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.LeadingAgeID = '123456';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest11(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.NotesPhrase = 'non existent note';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest12(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.TimeslotCode = 'A';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    
    static testmethod void getConferenceSessionsByConferenceIdAndSearchCriteriaTest13(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        
        ConferenceSessionSearchCriteria searchCriteria = new ConferenceSessionSearchCriteria();
        searchCriteria.LearningObjectivesPhrase = 'non existent learning objectives';
        
        List<Conference_Session__c> sessions = ConferenceSessionQuerier.getConferenceSessionsByConferenceIdAndSearchCriteria(session.Conference__c, searchCriteria);

    }
    

}