public without sharing class AccountTrigger_ProgramRevenueHandler extends NU.TriggerHandlersBase {
	public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {
        List <Account> RevenueAccountsUpdated = new List<Account>();
        List <Account> oldRecords = (List <Account>)oldRecordMap.values();
        
        for (Account oldAccount: oldRecords) {
            Account newAccount = (Account)newRecordMap.get(oldAccount.Id);
            
            System.Debug('   RevenueAccountsUpdated for newAccount: Id = ' + newAccount.Id +
            				', Revenue_Year_Submitted__c = ' + newAccount.Revenue_Year_Submitted__c + 
            				', Program_Service_Revenue__c = ' + newAccount.Program_Service_Revenue__c +
            				', Multi_Site_Program_Service_Revenue__c = ' + newAccount.Multi_Site_Program_Service_Revenue__c);
            				
            System.Debug('   RevenueAccountsUpdated for oldAccount: Id = ' + oldAccount.Id +
            				', Revenue_Year_Submitted__c = ' + oldAccount.Revenue_Year_Submitted__c + 
            				', Program_Service_Revenue__c = ' + oldAccount.Program_Service_Revenue__c +
            				', Multi_Site_Program_Service_Revenue__c = ' + oldAccount.Multi_Site_Program_Service_Revenue__c);
         
            if ((newAccount.Revenue_Year_Submitted__c != null && newAccount.Program_Service_Revenue__c != null) &&
                (oldAccount.Revenue_Year_Submitted__c != newAccount.Revenue_Year_Submitted__c || oldAccount.Program_Service_Revenue__c != newAccount.Program_Service_Revenue__c)
               )
            {
                RevenueAccountsUpdated.Add(newAccount);
            }
            
            else if ((newAccount.Revenue_Year_Submitted__c != null && newAccount.Multi_Site_Program_Service_Revenue__c != null) &&
                (oldAccount.Revenue_Year_Submitted__c != newAccount.Revenue_Year_Submitted__c || oldAccount.Multi_Site_Program_Service_Revenue__c != newAccount.Multi_Site_Program_Service_Revenue__c)
               )
            {
                RevenueAccountsUpdated.Add(newAccount);
            }
        }
    
        System.Debug('   RevenueAccountsUpdated: ' + RevenueAccountsUpdated);
        
        if (RevenueAccountsUpdated.size() == 0 ) {
                return;
            }
        
        
        Map <Id, Account> RevAccountsUpdatedQuery = new Map<Id,Account>([
            SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID IN : RevenueAccountsUpdated
        
        ]);
        
        List <ProgramRevenueHistory__c> updatedHistory = new List<ProgramRevenueHistory__c>();
        
        System.Debug('   RevAccountsUpdatedQuery: ' + RevAccountsUpdatedQuery);
        
        decimal PSR = null;
        
        for (Account xItem: RevenueAccountsUpdated)
         {
            Account RevAccountQ = RevAccountsUpdatedQuery.get(xItem.Id);
            
            System.Debug('   RevAccountQ: ' + RevAccountQ);
            System.Debug('   xItem: ' + xItem);
            
            ProgramRevenueHistory__c RevHistory  = null;
            
            for (ProgramRevenueHistory__c xHistory : RevAccountQ.Program_Revenue_History__r){
                if (xHistory.Revenue_Year_Submitted__c == xItem.Revenue_Year_Submitted__c){
                    RevHistory = xHistory;
                    break;
                }
            }
            
            System.Debug('   RevHistory: ' + RevHistory);
            
            if (RevHistory != null) { 
            	
            	if((xItem.Program_Service_Revenue__c != null && RevHistory.Program_Service_Revenue__c != xItem.Program_Service_Revenue__c) || (xItem.Multi_Site_Program_Service_Revenue__c != null && RevHistory.Program_Service_Revenue__c != xItem.Multi_Site_Program_Service_Revenue__c) )
            	{
				            		if(xItem.Program_Service_Revenue__c != null) {
				    			                      PSR = xItem.Program_Service_Revenue__c;
							    		}
							        else {
							    			          PSR = xItem.Multi_Site_Program_Service_Revenue__c;
							    		}
							    		
				    RevHistory = new ProgramRevenueHistory__c(              
                    Revenue_Year_Submitted__c = xItem.Revenue_Year_Submitted__c,
                    Program_Service_Revenue__c = PSR,
                    Account__c = xItem.Id
                    ); 
            	 updatedHistory.add(revHistory);   
            	}
            
                
            }
            else {
            	if((xItem.Program_Service_Revenue__c != null ) || (xItem.Multi_Site_Program_Service_Revenue__c != null) )
		            	{
						     if(xItem.Program_Service_Revenue__c != null) {
						    			                      PSR = xItem.Program_Service_Revenue__c;
									    		}
							  else {
									    			          PSR = xItem.Multi_Site_Program_Service_Revenue__c;
									    		}
									    		
						    RevHistory = new ProgramRevenueHistory__c(              
		                    Revenue_Year_Submitted__c = xItem.Revenue_Year_Submitted__c,
		                    Program_Service_Revenue__c = PSR,
		                    Account__c = xItem.Id
		                    ); 
		            	updatedHistory.add(revHistory);
		            	}
		            	
            
                
            }
         }
         
         upsert updatedHistory;
	}

                
                
	public override void onAfterInsert(Map<Id, sObject> newRecordMap) {
        List <Account> RevenueAccounts = new List<Account>();
        List <Account> newRecords = (List <Account>)newRecordMap.values();
        
        //Add all updated providers from form to RevenueAccount list
        for (Account newAccount: newRecords){
            if (newAccount.Revenue_Year_Submitted__c != null && 
                    newAccount.Program_Service_Revenue__c != null)
            {
                RevenueAccounts.Add(newAccount);
            }
            else if (newAccount.Revenue_Year_Submitted__c != null && 
            newAccount.Multi_Site_Program_Service_Revenue__c != null)
            {
            	RevenueAccounts.Add(newAccount);
            }
        }
        
        //Iterate thru list and update revenue
        List <ProgramRevenueHistory__c> ProgRevHistory = new List<ProgramRevenueHistory__c>();
        decimal PSR = null;
        
        for (Account RevAccount: RevenueAccounts){
        	if(RevAccount.Program_Service_Revenue__c != null)
    		{
    			PSR = RevAccount.Program_Service_Revenue__c;
    		}
        	else 
    		{
    			PSR = RevAccount.Multi_Site_Program_Service_Revenue__c;
    		}
        	
            ProgramRevenueHistory__c progRevItem = new ProgramRevenueHistory__c (           
                Revenue_Year_Submitted__c = RevAccount.Revenue_Year_Submitted__c,
                Program_Service_Revenue__c = PSR,
                Account__c = RevAccount.Id
            );
            ProgRevHistory.Add(progRevItem);
        }
        
        insert ProgRevHistory;
	}
}