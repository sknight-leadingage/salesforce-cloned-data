/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestJointStateProviderOrderMgrController {
	private static final Decimal SERVICE_FEE_PERCENTAGE = Constant.PROVIDER_SERVICE_FEE_PERCENTAGE;
	
	static Account StatePartner = null;
    static Account ProviderAccount = null;
    static Account ProviderAccountQueried = null;
    
    static NU__MembershipType__c JointStateProviderMT = null;
    static NU__Membership__c JointStateProviderMembership = null;
    
    static Joint_Billing__c JB = null;
    static Joint_Billing__c JBQueried = null;
    
    private static Joint_Billing__c getJointBillingById(Id jointBillingId){
    	return [select id,
    	               name,
    	               Amount__c,
    	               Service_Fee__c,
    	               Net_Amount__c,
    	               Order__c,
    	               State_Partner__c
    	          from Joint_Billing__c
    	         where id = :jointBillingId];
    }
    
    private static NU__Order__c getOrderById(Id orderId){
    	return [select id,
    	               name,
    	               NU__GrandTotal__c
    	          from NU__Order__c
    	         where id = :orderId];
    }
	
	private static void loadTestData(){
		StatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
        ProviderAccount = DataFactoryAccountExt.insertProviderAccount(500000);
        ProviderAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);

        NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, statePartner.Id, true);
        
        NU__PriceClass__c defaultPriceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        JointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();        
        
        JointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
        
        JB = DataFactoryJointBilling.insertJointBillingWithItemAndItemLine(statePartner.Id, providerAccount.Id, jointStateProviderMembership.NU__EndDate__c, providerAccountQueried.Dues_Price__c);
        
        JBQueried = getJointBillingById(JB.Id);
	}

	static testmethod void openManualBatchSelectOptsTest(){
		loadTestData();
		
		NU__Batch__c openManualBatch = NU.DataFactoryBatch.insertManualBatch(JointStateProviderMT.NU__Entity__c);
		
		ApexPages.StandardController sc = new ApexPages.StandardController(JB);
		
		JointStateProviderOrderManagerController controller = new JointStateProviderOrderManagerController(sc);
		
		List<SelectOption> openManualBatchOpts = controller.OpenManualBatchSelectOpts;
		
		system.assert(openManualBatchOpts.size() > 0, 'No open manual batches found.');
	}
	
	static testmethod void manualBatchSelectedTest(){
		loadTestData();
		
		NU__Batch__c openManualBatch = NU.DataFactoryBatch.createManualBatch(JointStateProviderMT.NU__Entity__c);
		
		openManualBatch.NU__TransactionDate__c = Date.Today().addDays(-10);
		
		insert openManualBatch;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(JB);
		
		JointStateProviderOrderManagerController controller = new JointStateProviderOrderManagerController(sc);
		
		controller.dummyCart.NU__Batch__c = openManualBatch.Id;
		controller.batchSelected();
		
		system.assertEquals(openManualBatch.NU__TransactionDate__c, controller.dummyCart.NU__TransactionDate__c);
	}
	
	static testmethod void automaticBatchReselectedTest(){
		loadTestData();
		
		NU__Batch__c openManualBatch = NU.DataFactoryBatch.createManualBatch(JointStateProviderMT.NU__Entity__c);
		
		openManualBatch.NU__TransactionDate__c = Date.Today().addDays(-10);
		
		insert openManualBatch;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(JB);
		
		JointStateProviderOrderManagerController controller = new JointStateProviderOrderManagerController(sc);
		
		controller.dummyCart.NU__Batch__c = openManualBatch.Id;
		controller.batchSelected();
		
		system.assertEquals(openManualBatch.NU__TransactionDate__c, controller.dummyCart.NU__TransactionDate__c);
		
		controller.dummyCart.NU__Batch__c = null;
		controller.batchSelected();
		
		system.assertEquals(Date.Today(), controller.dummyCart.NU__TransactionDate__c);
	}


    static testMethod void createOrderTest() {
        loadTestData();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(JB);
		
		JointStateProviderOrderManagerController controller = new JointStateProviderOrderManagerController(sc);
		
		Apexpages.PageReference resultPage = controller.manageOrder();
		
		system.assert(resultPage != null, 'An error occurred');
		system.assert(controller.managedOrder != null);
		
		NU__Order__c createdOrder = getOrderById(controller.managedOrder.Id);
		
		system.assertEquals(JBQueried.Net_Amount__c, createdOrder.NU__GrandTotal__c);
    }
    
    static testmethod void updateOrderTest(){
    	loadTestData();
        
        // Create Order first.
        ApexPages.StandardController sc = new ApexPages.StandardController(JB);
		
		JointStateProviderOrderManagerController controller = new JointStateProviderOrderManagerController(sc);
		
		Apexpages.PageReference resultPage = controller.manageOrder();
		
		NU__Order__c createdOrder = getOrderById(controller.managedOrder.Id);
		
		system.assertEquals(JBQueried.Net_Amount__c, createdOrder.NU__GrandTotal__c);
		
		// Then update it.
		
		Joint_Billing_Item_Line__c createdJBIL = [select id, name, Joint_Billing_Item__c from Joint_Billing_Item_Line__c where Joint_Billing_Item__r.Joint_Billing__c = :JB.Id];
		
		Decimal adjustmentAmount = 200;
		Joint_Billing_Item_Line__c adjustmentJBIL = JointBillingUtil.createJointBillingItemLine(adjustmentAmount, Date.Today(), Constant.JOINT_BILLING_ITEM_LINE_TYPE_ADJUSTMENT);
		adjustmentJBIL.Joint_Billing_Item__c = createdJBIL.Joint_Billing_Item__c;
		
		insert adjustmentJBIL;
		
		Joint_Billing__c adjustedjbQueried = getJointBillingById(JB.Id);
		
		sc = new ApexPages.Standardcontroller(adjustedjbQueried);
		controller = new JointStateProviderOrderManagerController(sc);
		controller.manageOrder();
		
		NU__Order__c updatedOrder = getOrderById(controller.managedOrder.Id);
		
		Decimal expectedAdjustmentAmount = (providerAccountQueried.Dues_Price__c + adjustmentAmount) * (1 - SERVICE_FEE_PERCENTAGE);
		
		system.assertEquals(expectedAdjustmentAmount, updatedOrder.NU__GrandTotal__c);
    }

    static testmethod void msoBillJointlyTwoMembershipsNotCreatedTest(){
    	Account msoAcct = DataFactoryAccountExt.insertMultiSiteAccount(500000);
    	
 		NU__PriceClass__c defaultPriceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        JointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();        
        
        JointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(msoAcct.Id, jointStateProviderMT);
    	
    	Joint_Billing__c msoJB = DataFactoryJointBilling.createJointBilling(msoAcct.Id, JointStateProviderMembership.NU__EndDate__c);
    	
    	JointStateProviderBiller jspb = new JointStateProviderBiller(msoJB);
    	
    	List<Account> msoAccounts = 
    	[Select id,
		        Name,
		        Multi_Site_Program_Service_Revenue__c,
		        Multi_Site_Dues_Price__c,
		        Multi_Site_Corporate__c,
		        Program_Service_Revenue__c,
		        Revenue_Year_Submitted__c,
		        Provider_Join_On__c,
		        Dues_Price__c,
		        RecordTypeId
		   from Account
		  where id = :msoAcct.Id];
		  
 /*		jspb.execute(null, msoAccounts);
		
		
		
		JointStateProviderOrderManager jointOrderMgr = new JointStateProviderOrderManager(msoJB.Id, null, Date.Today());
		
		jointOrderMgr.createOrder(); */
    }
}