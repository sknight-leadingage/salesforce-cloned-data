/**
 * @author NimbleUser
 * @date Updated: 2/6/13
 * @description This class provides utility methods for dealing with scheduled jobs.
 */
public with sharing class ScheduleUtil {
	public static final String ALREADY_SCHEDULED_PARTIAL_MSG = 'is already scheduled for execution';
	
	/**
	 * @description Convenient method that will schedule all the custom scheduled jobs as necessary.
	 */
	public static void scheduleCustomJobs(){
		try{
			MultiSiteProviderMembershipSetterJob.scheduleAt2AM();
		}
		catch (System.AsyncException ex){
			if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
				throw ex;
			}
		}
		
		try{
			JointBillingUpdater.scheduleAt3AM();
		}
		catch (System.AsyncException ex){
			if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
				throw ex;
			}
		}
		
		try{
			CalculateMembershipBillingTotals.schedule();
		}
		catch (System.AsyncException ex){
			if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
				throw ex;
			}
		}
		
		try{
			CASTFieldSyncJob.scheduleAt12AM();
		}
		catch (System.AsyncException ex){
			if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
				throw ex;
			}
		}
		
		try{
            BatchUpdateAffiliationStateNames.scheduleAt5AM();
        }
        catch (System.AsyncException ex){
            if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
                throw ex;
            }
        }
	}
	
	public static void scheduleAutomatedTestingJobs(){
		try{
			AutomatedTestJobQueuer.createDaily4AMScheduledJob();
		}
		catch (System.AsyncException ex){
			if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
				throw ex;
			}
		}
		
		try{
			AutomatedTestingJob.createEvery15MinuteScheduledJobs();
		}
		catch (System.AsyncException ex){
			if (ex.getMessage().containsIgnoreCase(ALREADY_SCHEDULED_PARTIAL_MSG) == false){
				throw ex;
			}
		}
	}
}