public without sharing class TriggerManager {
    public static Map<String,integer> executionCount = new Map<String,integer>();
    public static Map<String,integer> executionStage = new Map<String,integer>();
    public static Map<String,Boolean> executionHasRun = new Map<String,Boolean>();
    
    public static boolean shouldRun(String methodName) {
    	integer runCount = 0;
    	integer runWhen = 1;
    	boolean hasRun = false;
    	boolean retVal = false;
    	
    	if (executionStage.containsKey(methodName)) {
    		runWhen = executionStage.get(methodName);
    	}
    	
    	if (executionCount.containsKey(methodName)) {
    		if (!executionHasRun.containsKey(methodName)) {
    			executionHasRun.put(methodName, false);
    		}
    		
    		hasRun = executionHasRun.get(methodName);
    		runCount = executionCount.get(methodName);
    		
    		if (runCount >= runWhen && hasRun == false) {
    			runCount = 0;
    		} 
    	}
    	runCount++;
    	
		if (runCount == runWhen) {
			retVal = true;
		}
    	
    	executionHasRun.put(methodName, retVal);
    	executionCount.put(methodName, runCount);
    	
    	return retVal;
    }
}