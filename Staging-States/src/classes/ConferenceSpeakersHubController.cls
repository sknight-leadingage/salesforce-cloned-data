public with sharing class ConferenceSpeakersHubController extends ConferenceManagementControllerBase {

    public ConferenceSpeakersHubController(){
        try {
            string param = ApexPages.CurrentPage().getParameters().get(ConferenceManagementControllerBase.SPEAKER_ID_QUERY_STRING_NAME);
            if (param != null) {
                LeadingAgeIDToSearch = param;
                searchForSpeakers();
            } else {
                initializeSpeakerSessionToAdd();
            }
        } catch (Exception ex) {
                initializeSpeakerSessionToAdd();
        }
    }

    public override List<SObject> getConferenceObjects(){
        if (hasSpeakerSearchCriteria){
            return AccountQuerier.getSpeakersForEventBySearchCriteria(FirstNameToSearch, LastNameToSearch, CompanyToSearch, ZipCodeToSearch, PhoneToSearch, LeadingAgeIDToSearch, EventId);
        }
        
        return AccountQuerier.getAllSpeakersByConferenceId(EventId);
    }
    
    public Account CurrentSpeaker {
        get {
            return (Account) CurrentConferenceObject;
        }
    }
    
    public List<Conference_Session_Speaker__c> CurrentSpeakerEventSessions {
        get {
            if (CurrentSpeaker == null){
                return new List<Conference_Session_Speaker__c>();
            }
            
            return [Select id,
                           name,
                           Role__c,
                           Session__c,
                           Session__r.Title__c,
                           Session__r.Session_Title_Full__c,
                           Session__r.Session_Number__c
                      from Conference_Session_Speaker__c
                     where Speaker__c = :CurrentSpeaker.Id
                       and Session__r.Conference__c = :EventId
                     order by Session__r.Session_Number__c];
        }
    }
    
    public Conference_Session_Speaker__c SessionSpeakerToAdd { get; set; }
    
    public List<Conference_Session__c> AvailableEventSessionsForCurrentSpeaker {
        get {
            if (CurrentSpeaker == null){
                return new List<Conference_Session__c>();
            }
            
            Set<Id> speakerEventSessionIds = NU.CollectionUtil.getLookupIds(CurrentSpeakerEventSessions, 'Session__c');
            
            return [Select id,
                           name,
                           Title__c,
                           Session_Title_Full__c
                      from Conference_Session__c
                     where Conference__c = :EventId
                       and id not in :speakerEventSessionIds
                     order by Session_Number_Numeric__c];
        }
    }
    
    public Boolean hasSpeakerSearchCriteria {
        get {
            return String.isNotBlank(FirstNameToSearch) ||
                   String.isNotBlank(LastNameToSearch) ||
                   String.isNotBlank(CompanyToSearch) ||
                   String.isNotBlank(ZipCodeToSearch) ||
                   String.isNotBlank(PhoneToSearch) ||
                   String.isNotBlank(LeadingAgeIDToSearch);
        }
    }
    
    public String FirstNameToSearch { get; set; }
    
    public String LastNameToSearch { get; set; }
    
    public String CompanyToSearch { get; set; }
    
    public String ZipCodeToSearch { get; set; }
    
    public String PhoneToSearch { get; set; }
    
    public String LeadingAgeIDToSearch {get;set;}
    
    public String SessionSpeakerIdToDelete { get; set; }
    
    public List<SelectOption> AvailableEventSessionsForCurrentSpeakerSelectOpts {
        get {
            List<SelectOption> opts = new List<SelectOption>();
            
            for (Conference_Session__c eventSession : AvailableEventSessionsForCurrentSpeaker){
                SelectOption so = new SelectOption(eventSession.Id, eventSession.Session_Title_Full__c);
                
                opts.add(so);
            }
            
            return opts;
        }
    }
    
    public void initializeSpeakerSessionToAdd(){
        SessionSpeakerToAdd = new Conference_Session_Speaker__c(
            Speaker__c = CurrentSpeaker != null && CurrentSpeaker.Id != null ? CurrentSpeaker.Id : null
        );
    }
    
    public override void onPageChange(){
        initializeSpeakerSessionToAdd();
    }
    
    public void addSessionSpeaker(){
        try{
            if (SessionSpeakerToAdd != null && SessionSpeakerToAdd.Role__c != null) {
                insert SessionSpeakerToAdd;
                Refresh();
                initializeSpeakerSessionToAdd();
            } else {
                throw new ApplicationException('You must choose a role for this speaker on the selected session.');
            }
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    
    public void searchForSpeakers(){
        refreshAtPage1();
        initializeSpeakerSessionToAdd();
    }
    
    public void clearSearch(){
        FirstNameToSearch = '';
        LastNameToSearch = '';
        CompanyToSearch = '';
        ZipCodeToSearch = '';
        PhoneToSearch = '';
        LeadingAgeIDToSearch = '';
        
        refreshAtPage1(); 
    }
    
    public void deleteSessionSpeaker(){
        try{
            Conference_Session_Speaker__c sessionSpeakerToDelete = new Conference_Session_Speaker__c(
                Id = SessionSpeakerIdToDelete
            );
            
            delete sessionSpeakerToDelete;
            
            Refresh();
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
}