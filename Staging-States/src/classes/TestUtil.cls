/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class TestUtil {

    public static Boolean pageMessagesHasMessage(String pageMsg){
		system.assert(String.isBlank(pageMsg) == false);

		List<ApexPages.Message> pageMessages = ApexPages.getMessages();
		
		if (NU.CollectionUtil.listHasElements(pageMessages)){
			for (ApexPages.Message pageMessage : pageMessages){
				if (pageMessage.getSummary() == pageMsg){
					return true;
				}
			}
		}

		return false;
	}

	public static void assertPageMessagesHasMessage(String expectedPageMessage){
		system.assert(String.isBlank(expectedPageMessage) == false);
		system.assert(pageMessagesHasMessage(expectedPageMessage) == true);
	}
	
	public static void assertInsertValidationRule(SObject objectToInsert, String expectedValidationRuleMessage){
		String actualExpectedExceptionMsg = '';
    	
    	try{
    		insert objectToInsert;
    	}
    	catch (DMLException ex){
    		actualExpectedExceptionMsg = ex.getMessage();
    	}
    	
    	assertValidationMessage(actualExpectedExceptionMsg, expectedValidationRuleMessage);
	}
	
	public static void assertUpdateValidationRule(SObject objectToUpdate, String expectedValidationRuleMessage){
		String actualExpectedExceptionMsg = '';
    	
    	try{
    		update objectToUpdate;
    	}
    	catch (DMLException ex){
    		actualExpectedExceptionMsg = ex.getMessage();
    	}
    	
    	assertValidationMessage(actualExpectedExceptionMsg, expectedValidationRuleMessage);
	}
	
	public static void assertValidationMessage(String actualValidationMessage, String expectedValidationMessage){
		system.assert(actualValidationMessage.containsIgnoreCase(expectedValidationMessage), 'The actual validation message "' + actualValidationMessage + '" does not contain "' + expectedValidationMessage + '".');
	}
}