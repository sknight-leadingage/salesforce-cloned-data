/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMembershipTrigger {

    static testMethod void statePartnerPortalUserCanCreateInStateProviderMembershipTest() {
        Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

        Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            NU__Membership__c newProviderMembership = new TestProviderMembershipInserter().insertMembership(statePartnerProvider.Id);
            
            system.assert(newProviderMembership.Id != null, 'The membership was not successfully inserted.');
        }
    }
    
    static testMethod void statePartnerPortalUserCanEditInStateProviderMembershipTest() {
        Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

        Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            NU__Membership__c newProviderMembership = new TestProviderMembershipInserter().insertMembership(statePartnerProvider.Id);
            
            system.assert(newProviderMembership.Id != null, 'The membership was not successfully inserted.');
            
            newProviderMembership.NU__StartDate__c = newProviderMembership.NU__StartDate__c.AddMonths(-3);
            
            update newProviderMembership;
        }
    }
    
    static testMethod void statePartnerPortalUserCanDeleteInStateProviderMembershipTest() {
        Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

        Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            NU__Membership__c newProviderMembership = new TestProviderMembershipInserter().insertMembership(statePartnerProvider.Id);
            
            system.assert(newProviderMembership.Id != null, 'The membership was not successfully inserted.');
            
            delete newProviderMembership;
        }
    }
    
    static testmethod void statePartnerPortalUserCannotInsertOutOfStateProviderMembershipTest(){
        Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

        Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();
        Account statePartner2Provider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartner2Provider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            String exceptionMsg = '';
            
            UserQuerier.clearCurrentUserCache();
            
            try{
                NU__Membership__c newProviderMembership = new TestProviderMembershipInserter().insertMembership(statePartner2Provider.Id);
            }
            catch (DMLException dmlEx){
                exceptionMsg = dmlEx.getMessage();
            }
            
            if (exceptionMsg != '') {
                TestUtil.assertValidationMessage(exceptionMsg, Constant.STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG);
            }
        }
    }

    static testmethod void statePartnerPortalUserCannotEditOutOfStateProviderMembershipTest(){
        Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

        Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();
        Account statePartner2Provider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartner2Provider.Id, statePartnerPortalUser.Id);
        
        NU__Membership__c newProviderMembership = new TestProviderMembershipInserter().insertMembership(statePartner2Provider.Id);
        
        NU__Membership__c newProviderMembershipQueried1 = [select id, name, NU__MembershipType__r.NU__Entity__c from NU__Membership__c where id = :newProviderMembership.Id];
        
        NU__Entity__Share entityShare = new NU__Entity__Share(
            ParentId = newProviderMembershipQueried1.NU__MembershipType__r.NU__Entity__c,
            UserOrGroupId = statePartnerPortalUser.Id,
            AccessLevel = 'Read'
        );
        
        insert entityShare;
        
        AccountShare statePartnerShare = DataFactoryAccountShare.insertAccountShare(statePartnerAccount.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            String exceptionMsg = '';
            
            UserQuerier.clearCurrentUserCache();
            
            try{
                newProviderMembership.NU__StartDate__c = newProviderMembership.NU__StartDate__c.addMonths(-3);
                update newProviderMembership;
            }
            catch (DMLException dmlEx){
                exceptionMsg = dmlEx.getMessage();
            }
            
            if (exceptionMsg != '') {
                TestUtil.assertValidationMessage(exceptionMsg, Constant.STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG);
            }
        }
    }

    static testmethod void statePartnerPortalUserCannotDeleteOutOfStateProviderMembershipTest(){
        Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

        Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();
        Account statePartner2Provider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartner2Provider.Id, statePartnerPortalUser.Id);
        
        NU__Membership__c newProviderMembership = new TestProviderMembershipInserter().insertMembership(statePartner2Provider.Id);
        
        System.runAs (statePartnerPortalUser){
            String exceptionMsg = '';
            
            try{
                delete newProviderMembership;
            }
            catch (DMLException dmlEx){
                exceptionMsg = dmlEx.getMessage();
            }
            
            if (exceptionMsg != '') {
                system.assert(String.isNotBlank(exceptionMsg), 'The provider membership was successfully deleted when it should not have been.');
            }
        }
    }

    static testmethod void providerLapsedOnResetFromNewProviderJoinTest(){
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
        
        NU__Membership__c providerMembership = new TestProviderMembershipInserter().insertMembership(providerAccount.Id);
        
        providerAccount.Provider_Lapsed_On__c = Date.Today();
        update providerAccount;
        
        Date newStartDate = providerMembership.NU__StartDate__c.addYears(1);
        Date newEndDate = providerMembership.NU__EndDate__c.addYears(1);
        
        system.assert(providerAccount.Provider_Lapsed_On__c < newStartDate, 'The provider lapsed on date is on or after the new start date.');
        
        NU__Membership__c newerMembership = NU.DataFactoryMembership.insertMembership(providerAccount.Id, providerMembership.NU__MembershipType__c, newStartDate, newEndDate);
        
        Account providerAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);
        
        system.assert(providerAccountQueried.Provider_Lapsed_On__c == null, 'The provider lapsed on was not set to null.');
    }
    
    static testmethod void providerLapsedOnResetFromRenewalTest(){
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
        
        NU__Membership__c providerMembership = new TestProviderMembershipInserter().insertMembership(providerAccount.Id);
        
        providerAccount.Provider_Lapsed_On__c = providerMembership.NU__EndDate__c.addMonths(1);
        update providerAccount;
        
        Date newStartDate = providerMembership.NU__StartDate__c.addYears(1);
        Date newEndDate = providerMembership.NU__EndDate__c.addYears(1);
        
        system.assert(providerAccount.Provider_Lapsed_On__c > newStartDate, 'The provider lapsed on date is on or before the new start date.');
        
        NU__Membership__c newerMembership = NU.DataFactoryMembership.insertMembership(providerAccount.Id, providerMembership.NU__MembershipType__c, newStartDate, newEndDate);
        
        Account providerAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);
        
        system.assert(providerAccountQueried.Provider_Lapsed_On__c == null, 'The provider lapsed on was not set to null.');
    }
}