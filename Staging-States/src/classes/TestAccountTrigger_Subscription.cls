@isTest
public with sharing class TestAccountTrigger_Subscription {
	/*static testmethod void testMembershipSusbscriptions() {
	}*/

    static testmethod void duplicateSubscriptionsNotInDeleteListTest(){
    	integer SubsCount = 7;
    	
        List<NU__Product__c> onlineSubProducts = DataFactoryProductExt.insertOnlineSubscriptionProducts(SubsCount);
        Set<Id> onlineSubProductIds = NU.CollectionUtil.getSobjectIds(onlineSubProducts);
        
        Account individual = DataFactoryAccountExt.insertIndividualAccount();
        List<Online_Subscription__c> currentSubs = DataFactoryOnlineSubscription.insertOnlineSubscriptions(individual.Id, onlineSubProductIds);
        
        Account individual2 = DataFactoryAccountExt.insertIndividualAccount();
        List<Online_Subscription__c> currentSubs2 = DataFactoryOnlineSubscription.insertOnlineSubscriptions(individual2.Id, onlineSubProductIds);
        
        system.assertNotEquals(0, currentSubs.size());
        system.assertNotEquals(0, currentSubs2.size());
        
        DataFactoryMembershipExt.insertCurrentProviderMembership(individual.Id);
        
        individual.PersonEmailBouncedDate = date.today();
        individual.PersonEmailBouncedReason = 'Testing';
        update individual;
        
        currentSubs[3].Status__c = Constant.SUBSCRIPTION_STATUS_HELD;
        currentSubs[6].Status__c = Constant.SUBSCRIPTION_STATUS_HELD;
        update currentSubs;
        
        Database.merge(individual, individual2);
        
        List<Online_Subscription__c> subsAfterMerge = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :individual.Id];
        system.assertEquals(SubsCount, subsAfterMerge.size());
        
        individual.PersonEmailBouncedDate = null;
        individual.PersonEmailBouncedReason = null;
        update individual;
        
        List<Online_Subscription__c> subsNoHeld = [SELECT Id, Status__c FROM Online_Subscription__c WHERE Account__c = :individual.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE];
        system.assertEquals(SubsCount, subsNoHeld.size());
        
        individual.PersonEmailBouncedDate = date.today();
        individual.PersonEmailBouncedReason = 'Testing 2';
        update individual; 
        
        subsNoHeld[4].Status__c = Constant.SUBSCRIPTION_STATUS_HELD;
        subsNoHeld[5].Status__c = Constant.SUBSCRIPTION_STATUS_HELD;
        update subsNoHeld;
        
        individual.PersonEmailBouncedDate = null;
        individual.PersonEmailBouncedReason = null;
        update individual;
        
        List<Online_Subscription__c> subsNoHeld2 = [SELECT Id, Status__c FROM Online_Subscription__c WHERE Account__c = :individual.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE];
        system.assertEquals(SubsCount, subsNoHeld2.size());
    }
}