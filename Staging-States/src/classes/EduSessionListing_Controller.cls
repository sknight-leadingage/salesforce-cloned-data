public with sharing class EduSessionListing_Controller extends EduReportsControllerBase {
    public List<Conference_Session__c> SessionData {get;set;}
    
    public EduSessionListing_Controller() {
        SessionData = [SELECT Session_Number__c, Title__c, PTF_Code__c, Timeslot__r.Timeslot_Code__c, Timeslot__r.Session_Date__c, Timeslot__r.Session_Time__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c];
    }
}