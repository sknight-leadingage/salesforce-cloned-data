/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /*
 Member Status Fields

We’ve added State_Partner_Member__c and LeadingAge_Member__c fields to the Account object: in the case of the State field, 
to add the ability for states to easily see if the account is a member, and for the LeadingAge field, to replace the formulas that currently do this job.  
Current code is implemented in the States Sandbox ⇒ AccountTriggerHandlers trigger, setStateAndLeadingMemberField method.

To test the current implementation, we need to set up the following test conditions:

Test Rules: General Setup
During the tests we’ll need to be able to create test data for: Entities (Which requires GLs), State Partner Logins, State Partner Accounts, Provider Accounts, 
Individual Accounts, Membership Types, Membership Products, Memberships, 

Rule #1
Test: Create a Provider account.
Assertion: Both fields should be Non-member.

Rule #2
Test: Create a Provider with a LeadingAge National membership.  
To do this in a test class, it’s probably necessary to create an Entity and ensure the Entity is named “LeadingAge” and also create an account record of type State Partner 
(E.g. insert a state partner), and change the name of the state partner record to “LeadingAge”, before creating the Provider and Membership/Membership Type entries.
Assertion: State Member field should be Non-member, LeadingAge Member field should be Member.

Rule #3
Test: Create a Provider with a LeadingAge National membership and then add a State Membership product to this.  
To create the State Membership, it’s probably necessary to set up an entity and name it as a random state, e.g. change its name to “LeadingAge Washington” 
and then set up the account that’s a State Partner record type, with the same state name, etc, before creating the membership/membership type.
Assertion: Both member fields should show Member.


Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
        
NU__Affiliation__c primaryAffiliation = NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);
Rule #4
Test: taking the Provider record from rule #3, add an Individual with a Primary Affiliation to this Provider.
Assertion: Both member fields should show Member, for the Individual & Provider accounts.

Rule #5
Test: taking the records from rule #4, cancel the National membership.
Assertion: Member fields should read National field = Non-member, State field = Member, for the Individual & Provider accounts.

Rule #6
Test: taking the records from rule #5, add another Individual with Primary Affiliation to the Provider.
Assertion: Individual should show member fields: National field = Non-member, State field = Member.

Rule #7
Test: taking the records from rule #6, reinstate (Or add a new) National membership.
Assertion: Both member fields should show Member again, for the Provider and both child accounts.

Rule #8
Test: taking the records from rule #7, cancel the State membership.
Assertion: Both member fields should show National field = Member and State field = Non-member, for the Provider and both child accounts.
 
 */
@isTest
private class TestMemberStatusFields {
	private static Account StatePartnerLeadingAge { get; set; }
	private static Account StatePartner { get; set; }
	
	private static NU__MembershipType__c providerMT { get; set; }
	private static NU__MembershipType__c providerMTState  { get; set; }
	
	private static NU__Entity__c entityLeadingAge { get; set; }
	private static NU__Entity__c entityState { get; set; }

	private static void dataSetup() {
        //Zeroth tests
        User CurrentUser = UserQuerier.getCurrentUserWithStateInformation();
        Boolean isRunningUserAStatePartner = CurrentUser.ContactId != null || UserQuerier.UserStatePartnerAccount != null;
        system.assertEquals(false, isRunningUserAStatePartner); //At this point in the test class, we should be running as non-state user
        
        //LeadingAge Membership
        providerMT = DatafactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        providerMT.Grants_National_Membership__c = true;
        providerMT.Grants_State_Membership__c = false;
        update providerMT;
        
        //State Membership
        providerMTState = DatafactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        providerMTState.Grants_National_Membership__c = false;
        providerMTState.Grants_State_Membership__c = true;
        update providerMTState;
        
        //LeadingAge Entity
        entityLeadingAge = setEntityDetailsAndAddGLFor(providerMT.NU__Entity__c, Constant.LEADINGAGE_ENTITY_NAME, 'NY'); //Because default test state is NY
        system.assertNotEquals(null, entityLeadingAge.Name);
        
        //State
        entityState = setEntityDetailsAndAddGLFor(providerMTState.NU__Entity__c, 'LeadingAge Washington', 'WA');
        system.assertNotEquals(null, entityState.Name);
                
        //LeadingAge
        StatePartnerLeadingAge = createStatePartnerForEntity(entityLeadingAge);
        insert StatePartnerLeadingAge;
        
        //State
        StatePartner = createStatePartnerForEntity(entityState);
        insert StatePartner;
	}
	
	private static Account createStatePartnerForEntity(NU__Entity__c Entity) {
		Account returnValue = DataFactoryAccountExt.createStatePartnerAccount();
		returnValue.Name = Entity.Name;
		returnValue.NU__PrimaryEntity__c = Entity.Id;
		returnValue.BillingState = Entity.NU__State__c;
		
		return returnValue;
	}
	
	private static NU__Entity__c setEntityDetailsAndAddGLFor(Id EntityId, String EntityName, String StateToSet) {
        NU__Entity__c returnValue = new NU__Entity__c(Id = EntityId);
        returnValue.Name = EntityName;
        returnValue.NU__Status__c = 'Active';
        returnValue.NU__State__c = StateToSet;
        update returnValue;
        
        NU__GLAccount__c glAccountTemp = NU.DataFactoryGLAccount.insertRevenueGLAccount(returnValue.Id);
        
		return returnValue;
	}
	
	private static Account createProviderTestAccount() {
        Account providerAccount = DataFactoryAccountExt.createProviderAccount(15000000);
		providerAccount.NU__PrimaryEntity__c = entityState.Id;
		providerAccount.State_Partner_ID__c = StatePartner.Id;
        providerAccount.State_Partner_Name__c = StatePartner.Name;
        insert providerAccount;
        return providerAccount;
	}
	
	private static Account createIndividualAndAffiliateTo(Account providerAccount) {
        Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
        NU__Affiliation__c primaryAffiliation = NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAccount.Id, true);
        return individualAcct;
	}
	
	private static void addNationalMembershipTo(Account providerAccount) {
        NU__Membership__c insertedMembership = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id, providerMT);
        insertedMembership.NU__StartDate__c = Date.parse('1/1/' + System.Today().year());
        insertedMembership.NU__EndDate__c = Date.parse('12/31/' + System.Today().year());
        insert insertedMembership;
	}
	
	private static void addStateMembershipTo(Account providerAccount) {
		NU__Membership__c insertedMembership2 = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id, providerMTState);
        insertedMembership2.NU__StartDate__c = Date.parse('1/1/' + System.Today().year());
        insertedMembership2.NU__EndDate__c = Date.parse('12/31/' + System.Today().year());
        insert insertedMembership2;
	}
	
	private static void CancelNationalMembershipTo(Account providerAccount) { 
        NU__Membership__c CancelNationalMembership = [SELECT Id, NU__Status__c, Last_Provider_Lapsed_On__c, NU__EndDateOverride__c, NU__StartDate__c, NU__EndDate__c, Grants_National_Membership__c FROM NU__Membership__c WHERE NU__Account__r.Id = :providerAccount.Id and Grants_National_Membership__c = true LIMIT 1];
        CancelNationalMembership.NU__EndDate__c = Date.parse('1/2/' + System.Today().year());
        CancelNationalMembership.NU__EndDateOverride__c = Date.parse('1/2/' + System.Today().year());
        CancelNationalMembership.Last_Provider_Lapsed_On__c = CancelNationalMembership.NU__EndDate__c;
        update CancelNationalMembership;
        
        providerAccount.LeadingAge_Member__c = 'blah';
        update providerAccount;
	}
	private static void CancelStateMembershipTo(Account providerAccount) {
		NU__Membership__c CancelStateMembershipTo = [SELECT Id, NU__Status__c, Last_Provider_Lapsed_On__c, NU__EndDateOverride__c, NU__StartDate__c, NU__EndDate__c, Grants_State_Membership__c FROM NU__Membership__c WHERE NU__Account__r.Id = :providerAccount.Id and Grants_State_Membership__c = true LIMIT 1];
		CancelStateMembershipTo.NU__EndDate__c = Date.parse('1/2/' + System.Today().year());
        CancelStateMembershipTo.NU__EndDateOverride__c = Date.parse('1/2/' + System.Today().year());
        CancelStateMembershipTo.Last_Provider_Lapsed_On__c = CancelStateMembershipTo.NU__EndDate__c;
        update CancelStateMembershipTo;
        
        providerAccount.State_Partner_Member__c = 'blah';
        update providerAccount;
	}

    static testMethod void r1CreateProvider() {
        /*
        Rule #1
        Test: Create a Provider account.
        Assertion: Both fields should be Non-member.
        */
        
		dataSetup();
        Test.startTest();
        Account providerAccount = createProviderTestAccount();

        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :providerAccount.Id];

        system.assertEquals(Constant.STATE_NONMEMBER, providerAccount.State_Partner_Member__c, 'State Member Field Should Not Be Null');        
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, providerAccount.LeadingAge_Member__c, 'LeadingAge Member Field Should Not Be Null');

		Test.stopTest();
    }
    
    static testMethod void r2CreateProviderNational() {
        /*
        Rule #2
Test: Create a Provider with a LeadingAge National membership.  
To do this in a test class, it’s probably necessary to create an Entity and ensure the Entity is named “LeadingAge” and also create an account record of type State Partner 
(E.g. insert a state partner), and change the name of the state partner record to “LeadingAge”, before creating the Provider and Membership/Membership Type entries.
Assertion: State Member field should be Non-member, LeadingAge Member field should be Member.
        */
        
		dataSetup();
        Test.startTest();
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :providerAccount.Id];        
        
        system.assertEquals(Constant.STATE_NONMEMBER, providerAccount.State_Partner_Member__c);
        system.assertEquals(Constant.LEADINGAGE_MEMBER, providerAccount.LeadingAge_Member__c);
        
        Test.stopTest();
    }
        
    static testMethod void r3CreateProviderNationalState() {
		/*Rule #3
Test: Create a Provider with a LeadingAge National membership and then add a State Membership product to this.  
To create the State Membership, it’s probably necessary to set up an entity and name it as a random state, e.g. change its name to “LeadingAge Washington” 
and then set up the account that’s a State Partner record type, with the same state name, etc, before creating the membership/membership type.
Assertion: Both member fields should show Member.     
		*/
		
		dataSetup();
        Test.startTest();
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        addStateMembershipTo(providerAccount);
		
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :providerAccount.Id];
		
        system.assertEquals(Constant.STATE_MEMBER, providerAccount.State_Partner_Member__c);
        system.assertEquals(Constant.LEADINGAGE_MEMBER, providerAccount.LeadingAge_Member__c);
        
        Test.stopTest();
    }
        
    static testMethod void r4AddPAffilToR3Provider() {
        /* 
	Rule #4
Test: taking the Provider record from rule #3, add an Individual with a Primary Affiliation to this Provider.
Assertion: Both member fields should show Member, for the Individual & Provider accounts.
        */
		dataSetup();
        Test.startTest();
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        addStateMembershipTo(providerAccount);
        
        Account individualAcct = createIndividualAndAffiliateTo(providerAccount);
       
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c, State_Partner_Name__c FROM Account WHERE Id = :providerAccount.Id];        
        individualAcct = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct.Id];
        
        system.assertNotEquals(null, individualAcct.NU__PrimaryAffiliation__c, 'Individual has no Primary Affiliation but should have one.');
        system.assertEquals(providerAccount.Id, individualAcct.NU__PrimaryAffiliation__c, 'Individual\'s expected Primary Affiliation is different from their actual Primary Affiliation.');
        
        //Provider
        system.assertEquals(Constant.STATE_MEMBER, providerAccount.State_Partner_Member__c, 'Provider isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, providerAccount.LeadingAge_Member__c, 'Provider isn\'t a LeadingAge member');
        
        //Individual
        system.assertEquals(Constant.STATE_MEMBER, individualAcct.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, individualAcct.LeadingAge_Member__c, 'Individual isn\'t a LeadingAge member');

        Test.stopTest();
    }
    
        static testMethod void r5CancelNationalMembershipR4Provider() {
        /* 
	Rule #5
Test: taking the records from rule #4, cancel the National membership.
Assertion: Member fields should read National field = Non-member, State field = Member, for the Individual & Provider accounts.

        */
		dataSetup();
        
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        addStateMembershipTo(providerAccount);
        
        Account individualAcct = createIndividualAndAffiliateTo(providerAccount);
        
        Test.startTest();

        CancelNationalMembershipTo(providerAccount);
        
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c, State_Partner_Name__c FROM Account WHERE Id = :providerAccount.Id];        
        individualAcct = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct.Id];
        
        //Provider
        system.assertEquals(Constant.STATE_MEMBER, providerAccount.State_Partner_Member__c, 'Provider isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, providerAccount.LeadingAge_Member__c, 'Provider is a LeadingAge member');
        
        //Individual
        system.assertEquals(Constant.STATE_MEMBER, individualAcct.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, individualAcct.LeadingAge_Member__c, 'Individual is a LeadingAge member');

        Test.stopTest();
    }
    
    
    static testMethod void r6CancelNationalMembershipR5ProviderAndAddingNewIndividual() {
        /* 
	Rule #6
Test: taking the records from rule #5, add another Individual with Primary Affiliation to the Provider.
Assertion: Individual should show member fields: National field = Non-member, State field = Member.

        */
		dataSetup();
        
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        addStateMembershipTo(providerAccount);
        
        Account individualAcct1 = createIndividualAndAffiliateTo(providerAccount);
        
        
        Test.startTest();

        CancelNationalMembershipTo(providerAccount);
        
        // Adding New Individual after canceling National Memeberhsip
        
        Account individualAcct2 = createIndividualAndAffiliateTo(providerAccount);
        
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c, State_Partner_Name__c FROM Account WHERE Id = :providerAccount.Id];        
        individualAcct1 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct1.Id];
        individualAcct2 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct2.Id];
        
        //Provider
        system.assertEquals(Constant.STATE_MEMBER, providerAccount.State_Partner_Member__c, 'Provider isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, providerAccount.LeadingAge_Member__c, 'Provider is a LeadingAge member');
        
        //Individual 1
        system.assertEquals(Constant.STATE_MEMBER, individualAcct1.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, individualAcct1.LeadingAge_Member__c, 'Individual is a LeadingAge member');
        
         //Individual 2
        system.assertEquals(Constant.STATE_MEMBER, individualAcct2.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, individualAcct2.LeadingAge_Member__c, 'Individual is a LeadingAge member');

        Test.stopTest();
    }
    
        static testMethod void r7CancelNationalMembershipR6ProviderThenGiveNewNationalMembership() {
        /* 
	Rule #7
Test: taking the records from rule #6, reinstate (Or add a new) National membership.
Assertion: Both member fields should show Member again, for the Provider and both child accounts.

        */
		dataSetup();
        
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        addStateMembershipTo(providerAccount);
        
        Account individualAcct1 = createIndividualAndAffiliateTo(providerAccount);
        Account individualAcct2 = createIndividualAndAffiliateTo(providerAccount);
        
        Test.startTest();

        CancelNationalMembershipTo(providerAccount);
        
        
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c, State_Partner_Name__c FROM Account WHERE Id = :providerAccount.Id];        
        individualAcct1 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct1.Id];
        individualAcct2 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct2.Id];
        
        //Provider
        system.assertEquals(Constant.STATE_MEMBER, providerAccount.State_Partner_Member__c, 'Provider isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, providerAccount.LeadingAge_Member__c, 'Provider is a LeadingAge member');
        
        //Individual 1
        system.assertEquals(Constant.STATE_MEMBER, individualAcct1.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, individualAcct1.LeadingAge_Member__c, 'Individual is a LeadingAge member');
        
         //Individual 2
        system.assertEquals(Constant.STATE_MEMBER, individualAcct2.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_NONMEMBER, individualAcct2.LeadingAge_Member__c, 'Individual is a LeadingAge member');
        
        
        //Adding New Natioanl Membership
        addNationalMembershipTo(providerAccount);
        
        
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c, State_Partner_Name__c FROM Account WHERE Id = :providerAccount.Id];        
        individualAcct1 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct1.Id];
        individualAcct2 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct2.Id];
        
        //Provider
        system.assertEquals(Constant.STATE_MEMBER, providerAccount.State_Partner_Member__c, 'Provider isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, providerAccount.LeadingAge_Member__c, 'Provider is a LeadingAge member');
        
        //Individual 1
        system.assertEquals(Constant.STATE_MEMBER, individualAcct1.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, individualAcct1.LeadingAge_Member__c, 'Individual is a LeadingAge member');
        
         //Individual 2
        system.assertEquals(Constant.STATE_MEMBER, individualAcct2.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, individualAcct2.LeadingAge_Member__c, 'Individual is a LeadingAge member');

        Test.stopTest();
    }
    
    static testMethod void r8CancelStateMembershipR7Provider() {
        /* 
	Rule #8
Test: taking the records from rule #7, cancel the State membership.
Assertion: Both member fields should show National field = Member and State field = Non-member, for the Provider and both child accounts.

        */
		dataSetup();
        
        Account providerAccount = createProviderTestAccount();

        addNationalMembershipTo(providerAccount);
        addStateMembershipTo(providerAccount);
        
        Account individualAcct1 = createIndividualAndAffiliateTo(providerAccount);
        Account individualAcct2 = createIndividualAndAffiliateTo(providerAccount);
        
        Test.startTest();

        CancelStateMembershipTo(providerAccount);
        
        
        providerAccount = [SELECT Id, Name, LeadingAge_Member__c, State_Partner_Member__c, State_Partner_Name__c FROM Account WHERE Id = :providerAccount.Id];        
        individualAcct1 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct1.Id];
        individualAcct2 = [SELECT Id, Name, NU__PrimaryEntity__c, State_Partner_ID__c, State_Partner_Name__c, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_Name__c, LeadingAge_Member__c, State_Partner_Member__c FROM Account WHERE Id = :individualAcct2.Id];
        
        //Provider
        system.assertEquals(Constant.STATE_NONMEMBER, providerAccount.State_Partner_Member__c, 'Provider isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, providerAccount.LeadingAge_Member__c, 'Provider is a LeadingAge member');
        
        //Individual 1
        system.assertEquals(Constant.STATE_NONMEMBER, individualAcct1.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, individualAcct1.LeadingAge_Member__c, 'Individual is a LeadingAge member');
        
         //Individual 2
        system.assertEquals(Constant.STATE_NONMEMBER, individualAcct2.State_Partner_Member__c, 'Individual isn\'t a state member');
        system.assertEquals(Constant.LEADINGAGE_MEMBER, individualAcct2.LeadingAge_Member__c, 'Individual is a LeadingAge member');
        
        


        Test.stopTest();
    }
}