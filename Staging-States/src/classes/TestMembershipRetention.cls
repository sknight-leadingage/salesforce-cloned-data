/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMembershipRetention {
	static NU__Membership__c membership;
	
	static Account individual;
	static NU__Entity__c entity;
	static NU__MembershipType__c membershipType;
	
	static Integer nextYear = Date.today().year()+1;
	
	private static void setupTest() {
		individual = NU.DataFactoryAccount.insertIndividualAccount();
		
		entity = NU.DataFactoryEntity.insertEntity();
		membershipType = NU.DataFactoryMembershipType.insertDefaultMembershipType(entity.Id);
	}
	
	static testMethod void BasicNewMembershipTest() {
		setupTest();
		
		Test.startTest();
		membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(individual.Id, membershipType.Id);
		insert membership;
		Test.stopTest();
		
		List<NU__Membership__c> memberships = queryForMembership(membership.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		NU__Membership__c testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_NEW);
	}
	
	static testMethod void BasicRenewingMembershipTest() {
		setupTest();
		membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(individual.Id, membershipType.Id);
		insert membership;
		
		Test.startTest();
		NU__Membership__c membership2 = new NU__Membership__c(NU__Account__c = individual.Id, NU__MembershipType__c = membershipType.Id,
			NU__StartDate__c = Date.newInstance(nextYear, 1, 1), NU__EndDate__c = Date.newInstance(nextYear, 12, 31));
		insert membership2;
		Test.stopTest();
		
		List<NU__Membership__c> memberships = queryForMembership(membership2.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		NU__Membership__c testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_RENEWING);
	}
	
	static testMethod void InsertAndRenewingMembershipsAtTheSameTimeTest() {
		setupTest();
		
		Test.startTest();
		membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(individual.Id, membershipType.Id);
		NU__Membership__c membership2 = new NU__Membership__c(NU__Account__c = individual.Id, NU__MembershipType__c = membershipType.Id,
			NU__StartDate__c = Date.newInstance(nextYear, 1, 1), NU__EndDate__c = Date.newInstance(nextYear, 12, 31));
		insert new List<NU__Membership__c>{membership,membership2};
		Test.stopTest();
		
		List<NU__Membership__c> memberships = queryForMembership(membership.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		NU__Membership__c testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_NEW);
		
		memberships = queryForMembership(membership2.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_RENEWING);
	}
	
	static testMethod void InsertMembershipWithStartDateBeforeExistingMembershipTest() {
		setupTest();
		membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(individual.Id, membershipType.Id);
		insert membership;
		
		List<NU__Membership__c> memberships = queryForMembership(membership.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		NU__Membership__c testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_NEW);
		
		Test.startTest();
		NU__Membership__c membership2 = new NU__Membership__c(NU__Account__c = individual.Id, NU__MembershipType__c = membershipType.Id,
			NU__StartDate__c = Date.newInstance(nextYear-2, 1, 1), NU__EndDate__c = Date.newInstance(nextYear-2, 12, 31));
		insert membership2;
		Test.stopTest();
		
		memberships = queryForMembership(membership2.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_NEW);
		
		memberships = queryForMembership(membership.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_RENEWING);
	}
	
	static testMethod void UpdateMembershipWithStartDateBeforeAnotherMembershipTest() {
		setupTest();
		membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(individual.Id, membershipType.Id);
		insert membership;
		
		List<NU__Membership__c> memberships = queryForMembership(membership.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		NU__Membership__c testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_NEW);
		
		Test.startTest();
		NU__Membership__c membership2 = new NU__Membership__c(NU__Account__c = individual.Id, NU__MembershipType__c = membershipType.Id,
			NU__StartDate__c = Date.newInstance(nextYear, 1, 1), NU__EndDate__c = Date.newInstance(nextYear, 12, 31));
		insert membership2;
		
		memberships = queryForMembership(membership2.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_RENEWING);
		
		membership2.NU__StartDate__c = Date.newInstance(nextYear-2, 1, 1);
		membership2.NU__EndDate__c = Date.newInstance(nextYear-2, 12, 31);
		update membership2;
		Test.stopTest();
		
		memberships = queryForMembership(membership2.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_NEW);
		
		memberships = queryForMembership(membership.Id);
		System.assert(memberships != null && memberships.size() == 1);
		
		testMembership = memberships.get(0);
		basicAsserts(testMembership, Constant.MEMBERSHIP_RETENTION_RENEWING);
	}
	
	private static void basicAsserts(NU__Membership__c testMembership, String retention) {
		//System.assert(testMembership.RetentionStatus__c != null, 'Did not expect retention status to be null.');
		//System.assert(testMembership.RetentionStatus__c == retention, 'Expected "' + retention + '", not "' + testMembership.RetentionStatus__c + '".');
	}
	
	private static List<NU__Membership__c> queryForMembership(Id membershipId) {
		return [SELECT Id, RetentionStatus__c FROM NU__Membership__c WHERE Id = :membershipId];
	}
}