public with sharing class ConferenceCategoriesController extends ConferenceSessionsControllerBase {
    
    public ConferenceCategoriesController(){
        populateSelectedContentCategories();
        populateSelectedCECategories();
    }
    
    public List<Conference_Content_Category__c> ContentCategories {
        get {
            return [select id,
                           name
                      from Conference_Content_Category__c
                     order by name];
        }
    }
    
    private List<SelectOption> AvailableContentCategorySelectOptsPriv = new List<SelectOption>();
    public List<SelectOption> AvailableContentCategorySelectOpts {
        get {
            if (AvailableContentCategorySelectOptsPriv.size() == 0){
                Set<Id> currentSessionContentCategoryIds = new Set<Id>();
                
                for (Conference_Content_Category_Session__c currentSessionContentCategory : CurrentSessionContentCategories){
                    currentSessionContentCategoryIds.add(currentSessionContentCategory.Conference_Content_Category__c);
                }
                
                system.debug('  CurrentSessionContentCategories ' + CurrentSessionContentCategories);
                system.debug('  CurrentSessionContentCategories ' + currentSessionContentCategoryIds);
                
                for (Conference_Content_Category__c contentCategory : ContentCategories){
                    if (currentSessionContentCategoryIds.contains(contentCategory.Id) == false){
                        SelectOption so = new SelectOption(contentCategory.Id, contentCategory.Name);
                        AvailableContentCategorySelectOptsPriv.add(so);
                    }
                }
            }
            
            return AvailableContentCategorySelectOptsPriv;
        }
        
        set {
            AvailableContentCategorySelectOptsPriv = value;
        }
    }

    public List<SelectOption> SelectedContentCategorySelectOpts { get; set; }
    
    public List<Conference_Content_Category_Session__c> CurrentSessionContentCategories {
        get {
            if (CurrentSession != null){
                return CurrentSession.Conference_Content_Category_Sessions__r;
            }
            
            return new List<Conference_Content_Category_Session__c>();
        }
    }
    
    public List<CE_Category__c> CECategories {
        get {
            return [select id,
                           name
                      from CE_Category__c
                     order by name];
        }
    }
    
    public List<CE_Conference_Session__c> CurrentSessionCECategories {
        get {
            if (CurrentSession != null){
                return CurrentSession.CE_Conference_Sessions__r;
            }
            
            return new List<CE_Conference_Session__c>();
        }
    }
    
    private List<SelectOption> AvailableCECategorySelectOptsPriv = new List<SelectOption>();
    public List<SelectOption> AvailableCECategorySelectOpts {
        get {
            if (AvailableCECategorySelectOptsPriv.size() == 0){
                Set<Id> currentSessionCECategoryIds = new Set<Id>();
                
                for (CE_Conference_Session__c currentSessionCECategory : CurrentSessionCECategories){
                    currentSessionCECategoryIds.add(currentSessionCECategory.CE_Category__c);
                }
                
                for (CE_Category__c ceCategory : CECategories){
                    if (currentSessionCECategoryIds.contains(ceCategory.Id) == false){
                        if (ceCategory.Name != 'Activity Workers' && ceCategory.Name != 'Fund Raising (CFRE)'){
                            SelectOption so = new SelectOption(ceCategory.Id, ceCategory.Name);
                            AvailableCECategorySelectOptsPriv.add(so);
                        }
                    }
                }
            }
            
            return AvailableCECategorySelectOptsPriv;
        }
        
        set {
            AvailableCECategorySelectOptsPriv = value;
        }
    }

    public List<SelectOption> SelectedCECategorySelectOpts { get; set; }
    
    public override void save(){
        saveSessionContentCategories();
        saveSessionCECategories();
        super.save(); //Call save in parent class
        
        saveHiddenCECategories();
  
        refresh();
    }

    
    public void saveHiddenCECategories(){
        List<CE_Conference_Session__c> sessionCECategoriesToSave = new List<CE_Conference_Session__c>();
        List<CE_Conference_Session__c> sessionCECategoriesToDelete = new List<CE_Conference_Session__c>();
        
        Id ActivityWorkersCECategoryId = 'a1Nd0000000BogkEAC';
        Id CFRECECategoryId = 'a1Nd0000000BohJEAS';
        
        if (CurrentSession.Activity_Workers_Certification__c != null){
            CE_Conference_Session__c sessionCECategory = new CE_Conference_Session__c(
                Conference_Session__c = CurrentSession.Id,
                CE_Category__c = ActivityWorkersCECategoryId
            );
            
            sessionCECategoriesToSave.add(sessionCECategory);
        }
        else {
            if (CurrentSessionCECategories.size() <= 0) {
                List<CE_Conference_Session__c> prevSelectedActivityWorkersItem = [SELECT Id FROM CE_Conference_Session__c WHERE Conference_Session__c = :CurrentSession.Id AND CE_Category__c = :ActivityWorkersCECategoryId];
                if (prevSelectedActivityWorkersItem.size() > 0) {
                    sessionCECategoriesToDelete.add(prevSelectedActivityWorkersItem[0]);
                }
            }
        }
        
        if (CurrentSession.CFRE_Certification__c != null){
            CE_Conference_Session__c sessionCECategory = new CE_Conference_Session__c(
                Conference_Session__c = CurrentSession.Id,
                CE_Category__c = CFRECECategoryId
            );
            
            sessionCECategoriesToSave.add(sessionCECategory);
        }
        else {
            if (CurrentSessionCECategories.size() <= 0) {
                List<CE_Conference_Session__c> prevSelectedCFREItem = [SELECT Id FROM CE_Conference_Session__c WHERE Conference_Session__c = :CurrentSession.Id AND CE_Category__c = :CFRECECategoryId];
                if (prevSelectedCFREItem.size() > 0) {
                    sessionCECategoriesToDelete.add(prevSelectedCFREItem[0]);
                }
            }
        }
        
        if (sessionCECategoriesToSave.size() > 0){
            upsert sessionCECategoriesToSave;
        }
        
        if (sessionCECategoriesToDelete.size() > 0){
            delete sessionCECategoriesToDelete;
        }
    }
    
    public void saveSessionContentCategories(){
        if (CurrentSessionContentCategories.size() > 0){
            delete CurrentSessionContentCategories;
        }
        
        List<Conference_Content_Category_Session__c> sessionContentCategoriesToSave = new List<Conference_Content_Category_Session__c>();
        
        for (SelectOption selectedContentCategorySelectOpt : SelectedContentCategorySelectOpts){
            Conference_Content_Category_Session__c sessionContentCategory = new Conference_Content_Category_Session__c(
                Conference_Session__c = CurrentSession.Id,
                Conference_Content_Category__c = selectedContentCategorySelectOpt.getValue()
            );
            
            sessionContentCategoriesToSave.add(sessionContentCategory);
        }
        
        if (sessionContentCategoriesToSave.size() > 0){
            insert sessionContentCategoriesToSave;
        }
    }
    
    public void saveSessionCECategories(){
        if (CurrentSessionCECategories.size() > 0){
            delete CurrentSessionCECategories;
        }
        
        List<CE_Conference_Session__c> sessionCECategoriesToSave = new List<CE_Conference_Session__c>();
        
        for (SelectOption selectedCECategorySelectOpt : SelectedCECategorySelectOpts){
            CE_Conference_Session__c sessionCECategory = new CE_Conference_Session__c(
                Conference_Session__c = CurrentSession.Id,
                CE_Category__c = selectedCECategorySelectOpt.getValue()
            );
            
            sessionCECategoriesToSave.add(sessionCECategory);
        }
        
        if (sessionCECategoriesToSave.size() > 0){
            insert sessionCECategoriesToSave;
        }
    }
    
    public override void onPageChange(){
        AvailableContentCategorySelectOpts.clear();
        AvailableCECategorySelectOpts.clear();
        
        populateSelectedContentCategories();
        populateSelectedCECategories();
    }
    
    public void populateSelectedContentCategories(){
        SelectedContentCategorySelectOpts = new List<SelectOption>();
        
        if (CurrentSessionContentCategories.size() > 0){

            for (Conference_Content_Category_Session__c currentSessionContentCategory : CurrentSessionContentCategories){
                SelectOption so = new SelectOption(currentSessionContentCategory.Conference_Content_Category__c,currentSessionContentCategory.Conference_Content_Category__r.Name );
                SelectedContentCategorySelectOpts.add(so);
            }
        }
    }
    
    public void populateSelectedCECategories(){
        SelectedCECategorySelectOpts = new List<SelectOption>();
        
        if (CurrentSessionCECategories.size() > 0){
            
            for (CE_Conference_Session__c sessionCECategory : CurrentSessionCECategories){
                if (sessionCECategory.CE_Category__r.Name != 'Activity Workers' && sessionCECategory.CE_Category__r.Name != 'Fund Raising (CFRE)'){
                    SelectOption so = new SelectOption(sessionCECategory.CE_Category__c, sessionCECategory.CE_Category__r.Name);
                    SelectedCECategorySelectOpts.add(so);
                }
            }
        }
    }
    
    //Move Certification section from Session page to here
    private List<SelectOption> getDomainsOfPracticeSelectOpts(List<SObject> domainsOfPractice) {
        List<SelectOption> opts = new List<SelectOption>();
        
        for (Sobject dop : domainsOfPractice){
            String fullName = String.valueOf(dop.get('Full_Name__c'));
            
            SelectOption opt = new SelectOption(dop.Id, fullName);
            opts.add(opt);
        }
        
        return opts;
    }
    
    
    public List<CA_RCFE_Domains_Of_Practice__c> CARCFEDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from CA_RCFE_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> CARCFEDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(CARCFEDomainsOfPractice);
        }
    }
    
    public List<Activity_Workers_Certification__c> ActivityWorkersCertification {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from Activity_Workers_Certification__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> ActivityWorkersCertificationSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(ActivityWorkersCertification);
        }
    }
    
    public List<CFRE_Certification__c> CFRECertification {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from CFRE_Certification__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> CFRECertificationSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(CFRECertification);
        }
    }
    
    public List<NAB_Domains_Of_Practice__c> NABDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from NAB_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> NABDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(NABDomainsOfPractice);
        }
    }
    
    public List<NASBA_Domains_Of_Practice__c> NASBADomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from NASBA_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }

    
    public List<SelectOption> NASBADomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(NASBADomainsOfPractice);
        }
    }
    
    public List<Kansas_Domains_Of_Practice__c> KansasDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from Kansas_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> KansasDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(KansasDomainsOfPractice);
        }
    }
    
    public List<Missouri_Domains_Of_Practice__c> MissouriDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from Missouri_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> MissouriDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(MissouriDomainsOfPractice);
        }
    }
    
    public List<Florida_Domains_Of_Practice__c> FloridaDomainsOfPractice {
        get{
            return [select id,
                           name
                      from Florida_Domains_Of_Practice__c
                      order by name];
        }
    }
    
    public List<SelectOption> FloridaDomainsOfPracticeSelectOpts {
        get{
            return PageUtil.getSelectOptionsFromSObjects(FloridaDomainsOfPractice);
        }
    }
    //end of certification section

    public integer RecordNum {get;set;}
    public void GoToRecord() {
        if (RecordNum != null && RecordNum > 0 && RecordNum <= ResultSize) {
            changeToPage(RecordNum);
            RecordNum = null;
        }
    }
    
    public override void search(){
        super.search();
        onPageChange();
    }
    
    public override void clearSearch(){
        super.clearSearch();
        onPageChange();
    }
}