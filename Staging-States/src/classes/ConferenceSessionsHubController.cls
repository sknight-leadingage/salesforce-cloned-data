public with sharing class ConferenceSessionsHubController extends ConferenceSessionsControllerBase {

    public List<Conference_Session_Speaker__c> CurrentSessionSpeakers {
        get{
            List<Conference_Session_Speaker__c> speakers = new List<Conference_Session_Speaker__c>();
            
            if (CurrentSession != null &&
                CurrentSession.Conference_Session_Speakers__r != null){
                speakers = CurrentSession.Conference_Session_Speakers__r;
            }
            
            return speakers;
        }
    }
    
    public List<CA_RCFE_Domains_Of_Practice__c> CARCFEDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from CA_RCFE_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> CARCFEDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(CARCFEDomainsOfPractice);
        }
    }
    
    public List<NAB_Domains_Of_Practice__c> NABDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from NAB_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> NABDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(NABDomainsOfPractice);
        }
    }
    
    public List<NASBA_Domains_Of_Practice__c> NASBADomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from NASBA_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public string selectedSpeakerId {get;set;}
    public PageReference selectItem() {
        PageReference pr = Page.ConferenceSpeakers;
        Map<String, String> queryParms = pr.getParameters();
        queryParms.put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, EventId);
        queryParms.put(ConferenceManagementControllerBase.SPEAKER_ID_QUERY_STRING_NAME, selectedSpeakerId);
        return pr;
    }
    
    public List<SelectOption> NASBADomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(NASBADomainsOfPractice);
        }
    }
    
    public List<Kansas_Domains_Of_Practice__c> KansasDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from Kansas_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> KansasDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(KansasDomainsOfPractice);
        }
    }
    
    public List<Missouri_Domains_Of_Practice__c> MissouriDomainsOfPractice {
        get{
            return [select id,
                           name,
                           Full_Name__c
                      from Missouri_Domains_Of_Practice__c
                      order by Full_Name__c];
        }
    }
    
    public List<SelectOption> MissouriDomainsOfPracticeSelectOpts {
        get{
            return getDomainsOfPracticeSelectOpts(MissouriDomainsOfPractice);
        }
    }
    
    public List<Florida_Domains_Of_Practice__c> FloridaDomainsOfPractice {
        get{
            return [select id,
                           name
                      from Florida_Domains_Of_Practice__c
                      order by name];
        }
    }
    
    public List<SelectOption> FloridaDomainsOfPracticeSelectOpts {
        get{
            return PageUtil.getSelectOptionsFromSObjects(FloridaDomainsOfPractice);
        }
    }
    
    public List<Conference_Timeslot__c> ConferenceTimeSlots {
        get {
            return [select id,
                           name,
                           Day__c,
                           Start__c,
                           End__c,
                           Session_Date__c,
                           Session_Time__c,
                           Short_Slot__c,
                           Timeslot__c,
                           Timeslot_Code__c
                      from Conference_Timeslot__c
                     where Conference__c = :EventId
                     order by Timeslot_Code__c, TimeSlot__c];
        }
    }
    
    public List<SelectOption> ConferenceTimeslotSelectOpts {
        get {
            List<SelectOption> opts = new List<SelectOption>();
            
            for (Conference_Timeslot__c ts : ConferenceTimeSlots){
                String timeslotLabel = ts.Timeslot_Code__c + ' - ' + ts.Timeslot__c;
                SelectOption opt = new SelectOption(ts.Id, timeslotLabel);
                opts.add(opt);
            }
            
            return opts;
        }
    }
    
    public String SelectedTimeSlotDisplay {
        get {
            String display = '';
            
            if (CurrentSession != null && CurrentSession.Timeslot__c != null){
                for (SelectOption timeslotOpt : ConferenceTimeslotSelectOpts){
                    if (timeslotOpt.getValue() == CurrentSession.Timeslot__c){
                        return timeslotOpt.getLabel();
                    }
                }
            }
            
            return display;
        }
    }
    
    public List<Division__c> Divisions {
        get {
            return [select id,
                           name
                      from Division__c
                      order by name];
        }
    }
    
    public List<SelectOption> DivisionSelectOpts {
        get {
            return PageUtil.getSelectOptionsFromSObjects(Divisions);
        }
    }
    
    public List<Conference_Session__History> SessionHistory {
        get {
            List<Conference_Session__History> history = new List<Conference_Session__History>();
            
            if (CurrentSession != null){
                history = 
                [select id,
                        Field,
                        OldValue,
                        NewValue,
                        CreatedById,
                        CreatedDate
                   from Conference_Session__History
                  where ParentId = :CurrentSession.Id
                  order by CreatedDate desc, Field];
            }
            
            return history;
        }
    }

    private List<SelectOption> getDomainsOfPracticeSelectOpts(List<SObject> domainsOfPractice) {
        List<SelectOption> opts = new List<SelectOption>();
        
        for (Sobject dop : domainsOfPractice){
            String fullName = String.valueOf(dop.get('Full_Name__c'));
            
            SelectOption opt = new SelectOption(dop.Id, fullName);
            opts.add(opt);
        }
        
        return opts;
    }
    
    public Id SessionSpeakerIdToDelete {get;set;}
    
    public void deleteSessionSpeaker(){
        try{
            Conference_Session_Speaker__c sessionSpeakerToDelete = new Conference_Session_Speaker__c(
                Id = SessionSpeakerIdToDelete
            );
            
            delete sessionSpeakerToDelete;
            
            Refresh();
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    
   public override void save(){
     try{
        upsert CurrentSessionSpeakers;
		CurrentSession.Session_AV_Modified__c = Datetime.now();
        super.save(); //Call save in parent class
     }
     catch(Exception ex){
       ApexPages.addMessages(ex);
     }
   }

    public integer RecordNum {get;set;}
    public void GoToRecord() {
        if (RecordNum != null && RecordNum > 0 && RecordNum <= ResultSize) {
            changeToPage(RecordNum);
            RecordNum = null;
        }
    }
    
    public override void search(){
        super.search();
        RecordNum = null;
    }
    
    public override void clearSearch(){
        super.clearSearch();
        RecordNum = null;
    }
}