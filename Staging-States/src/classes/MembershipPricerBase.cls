public abstract class MembershipPricerBase {
	protected Boolean isPrimaryMembershipProductOfMembership(NU__MembershipType__c membershipType, NU__Product__c membershipProduct, String containsMembershipName){
		if (membershipType == null ||
		    membershipType.Name.containsIgnoreCase(containsMembershipName) == false){
		    return false;
	    }
		
		NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(membershipType, membershipProduct);
		
		return mtpl != null && mtpl.NU__Purpose__c == Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY;
	}
	
	protected NU__MembershipTypeProductLink__c findMembershipTypeProductLink(NU__MembershipType__c membershipType, NU__Product__c membershipProduct){
		if (membershipType != null && membershipType.NU__MembershipTypeProductLinks__r != null) {
			for (NU__MembershipTypeProductLink__c mtpl : membershipType.NU__MembershipTypeProductLinks__r) {
				if (mtpl != null && mtpl.NU__Product__c != null && mtpl.NU__Product__c == membershipProduct.Id){
					return mtpl;
				}
			}
		}
		
		return null;
	}
	
	protected NU.ProductPricingInfo findMembershipProductPricingInfo(Id productId, NU.ProductPricingRequest productPricingRequest){
		for (NU.ProductPricingInfo ppi : productPricingRequest.productPricingInfos){
			if (ppi.ProductId == productId){
				return ppi;
			}
		}
		
		return null;
	}
}