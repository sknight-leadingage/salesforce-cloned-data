/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ARTrialBalanceControllerTest {

	//tests

    static testMethod void AvailableEntitiesSelectOptionsTest() {
        NU__Entity__c entity = NU.DataFactoryEntity.insertEntity();
        ARTrialBalanceController trialController = new ARTrialBalanceController();
        List<SelectOption> opts = trialController.EntityOptions;
        
        system.assert(NU.CollectionUtil.listHasElements(opts), 'No entities found for the Available Entities.');
        system.assert(opts[1].getLabel() == entity.Name, 'The entity option name is not the same as the entity name.');
    }
    
    static testMethod void OrderTypesSelectOptionsTest(){
    	NU__Entity__c entity = NU.DataFactoryEntity.insertEntity();
        ARTrialBalanceController trialController = new ARTrialBalanceController();
        List<SelectOption> opts = trialController.OrderTypeOptions;
        
        system.assert(NU.CollectionUtil.listHasElements(opts), 'No order types were found.');
    }
    
    static testMethod void noARTest(){
    	NU__Entity__c entity = NU.DataFactoryEntity.insertEntity();
    	insertARAgings(entity.id);
        ARTrialBalanceController trialController = new ARTrialBalanceController();
        
        test.startTest();
        trialController.Submit();
        test.stopTest();
        
        assertNoAR();
    }

    static testMethod void oneCustomerWithARTest(){
    	List<NU__Transaction__c> arTrans = insertARTransactions();
    	ARTrialBalanceController trialController = new ARTrialBalanceController();

        test.startTest();
        trialController.Submit();
        test.stopTest();
        trialController.populateBatchJob();

		assertHasAR(1);
		assertAging('Current');
		assertBalance();
    }

    static testMethod void priorARSnapshotsTest(){
    	List<NU__Transaction__c> arTrans = insertARTransactions();
    	List<ARSnapshot__c> arSnapshots = insertARSnapshot(arTrans);
    	ARTrialBalanceController trialController = new ARTrialBalanceController();

        test.startTest();
        trialController.Submit();
        test.stopTest();
        trialController.populateBatchJob();

		assertHasAR(1);
		assertAging('Current');
		assertBalance();
    }
    
    static testMethod void noARFromEarlierAsOfDateTest(){
    	List<NU__Transaction__c> arTrans = insertARTransactions();
    	ARTrialBalanceController trialController = new ARTrialBalanceController();
    	trialController.FormFields.AsOfDate.NU__EndDate__c = arTrans[0].NU__Date__c.addDays(-30);

        test.startTest();
        trialController.Submit();
        test.stopTest();
        
        assertNoAR();
    }

    static testMethod void noARFromLaterAsOfDateTest(){
    	List<NU__Transaction__c> arTrans = insertARTransactions();
    	ARTrialBalanceController trialController = new ARTrialBalanceController();
    	trialController.FormFields.AsOfDate.NU__EndDate__c = arTrans[0].NU__Date__c.addDays(+40);

        test.startTest();
        trialController.Submit();
        test.stopTest();
        
		assertHasAR(1);
		assertAging('Over 30 days');
		assertBalance();
    }
    
    static testMethod void noARFromDifferentEntityTest(){
    	NU__Entity__c otherEntity = NU.DataFactoryEntity.insertEntity();
    	insertARAgings(otherEntity.id);
    	
    	List<NU__Transaction__c> arTrans = insertARTransactions();
        ARTrialBalanceController trialController = new ARTrialBalanceController();
    	trialController.FormFields.EntityId = otherEntity.Id;

        test.startTest();
        trialController.Submit();
        test.stopTest();
        
        assertNoAR();
    }

    static testMethod void billToWithARTest(){
    	List<NU__Transaction__c> arTrans = insertARTransactions();
    	ARTrialBalanceController trialController = new ARTrialBalanceController();
    	trialController.FormFields.BillTo.NU__Account__c = arTrans[0].NU__BillTo__c;

        test.startTest();
        trialController.Submit();
        test.stopTest();

		assertHasAR(1);
		assertAging('Current');
		assertBalance();
    }
    
    static testMethod void OrderTypeARTest(){
    	List<NU__Transaction__c> arTrans = insertARTransactions();
        
        NU__OrderItem__c arOrderItem =
        [SELECT Id,
                Name,
                RecordType.Name
           FROM NU__OrderItem__c
          WHERE Id = :arTrans[0].NU__OrderItem__c];

    	ARTrialBalanceController trialController = new ARTrialBalanceController();
    	trialController.FormFields.OrderTypeName = arOrderItem.RecordType.Name;

        test.startTest();
        trialController.Submit();
        test.stopTest();

		assertHasAR(1);
		assertAging('Current');
		assertBalance();
    }

	//assertions
	static void assertNoAR(){
		List<ARSnapshot__c> arSnapshotList = [SELECT Id FROM ARSnapshot__c];
		system.assertEquals(0, arSnapshotList.size(), 'Expected no AR Snapshot rows, found ' + arSnapshotList.size());
	}

	static void assertHasAR(Integer expectedRows){
		List<ARSnapshot__c> arSnapshotList = [SELECT Id FROM ARSnapshot__c];
		system.assertEquals(expectedRows, arSnapshotList.size(), 'Expected ' + expectedRows + ' AR Snapshot rows, found ' + arSnapshotList.size());
	}

	static void assertAging(String bucketName){
		ARSnapshot__c arSnapshot = [SELECT Id, ARAging__r.Name FROM ARSnapshot__c LIMIT 1];
		system.assertEquals(bucketName, arSnapshot.ARAging__r.Name, 'Expected ' + bucketName + ' AR Snapshot bucket, found ' + arSnapshot.ARAging__r.Name);
	}

	static void assertBalance(){
		ARSnapshot__c arSnapshot = [SELECT Id, Balance__c FROM ARSnapshot__c LIMIT 1];
		NU__Order__c ord = [SELECT Id, NU__Balance__c FROM NU__Order__c LIMIT 1];
		system.assertEquals(ord.NU__Balance__c, arSnapshot.Balance__c, 'Expected ' + ord.NU__Balance__c + ' AR Snapshot bucket, found ' + arSnapshot.Balance__c);
	}

	//helper functions
    public static List<NU__Transaction__c> insertARTransactions(){
        NU__OrderItem__c merchandiseOI = NU.DataFactoryOrderItem.insertOrderItem();
        NU__Order__c merchandiseOrder = new NU__Order__c(id = merchandiseOI.NU__Order__c, NU__InvoiceDate__c = Date.today());
        upsert merchandiseOrder;
        insertARAgings(merchandiseOI.NU__Entity__c);

        NU__OrderItem__c merchandiseOIQueried = getOrderItemById(merchandiseOI.Id);
        
        List<NU__Product__c> merchandiseProds =
          NU.DataFactoryProduct.insertDefaultProducts(
            1,
            merchandiseOIQueried.NU__Order__r.NU__Entity__c);

        List<NU__OrderItemLine__c> oilsToInsert = new List<NU__OrderItemLine__c>();
        
        for (NU__Product__c merchandiseProd : merchandiseProds){
            NU__OrderItemLine__c oilToInsert = new NU__OrderItemLine__c(
               NU__Product2__c = merchandiseProd.Id,
               NU__Quantity__c = 1,
               NU__UnitPrice__c = merchandiseProd.NU__ListPrice__c,
               NU__OrderItem__c = merchandiseOI.Id
            );
            
            oilsToInsert.add(oilToInsert);
        }
        
        insert oilsToInsert;
        
        NU__Batch__c manualBatch = NU.DataFactoryBatch.insertManualBatch(merchandiseOIQueried.NU__Order__r.NU__Entity__c);
        
        List<NU__Transaction__c> ARTransToInsert = new List<NU__Transaction__c>();
        
        for (NU__OrderItemLine__c insertedOIL : oilsToInsert){
            NU__Transaction__c ARTran = new NU__Transaction__c(
               NU__Date__c = merchandiseOIQueried.NU__Order__r.NU__TransactionDate__c,
               NU__OrderItemLine__c = insertedOIL.Id,
               NU__OrderItem__c = insertedOIL.NU__OrderItem__c,
               NU__Order__c = merchandiseOIQueried.NU__Order__c,
               NU__BillTo__c = merchandiseOIQueried.NU__Order__r.NU__BillTo__c,
               NU__Amount__c = insertedOIL.NU__UnitPrice__c,
               NU__Entity2__c = merchandiseOIQueried.NU__Order__r.NU__Entity__c,
               NU__Customer__c = merchandiseOIQueried.NU__Customer__c,
               NU__Category__c = 'Debit',
               NU__GLAccount__c = merchandiseOIQueried.NU__ARGLAccount__c,
               NU__Batch__c = manualBatch.Id,
               NU__ReferenceGroup__c = 1,
               NU__ReferenceNumber__c = 1,
               NU__Type__c = 'AR'
            );
            
            ARTransToInsert.add(ARTran);
        }
        
        insert ARTransToInsert;
        
        return ARTransToInsert;
    }
    
    static NU__OrderItem__c getOrderItemById(Id orderItemId){
        return [SELECT Id,
                       name,
                       NU__Customer__c,
                       NU__Order__c,
                       NU__Order__r.NU__BillTo__c,
                       NU__Order__r.NU__Entity__c,
                       NU__Order__r.NU__TransactionDate__c,
                       NU__Order__r.NU__InvoiceDate__c,
                       NU__ARGLAccount__c
                  FROM NU__OrderItem__c
                 WHERE id = :orderItemId];
    }

	public static List<ARSnapshot__c> insertARSnapshot(List<NU__Transaction__c> transList) {
		List<ARSnapshot__c> arSnapshotList = new List<ARSnapshot__c>();
		for(NU__Transaction__c t : transList) {
			ARSnapshot__c snapshot = new ARSnapshot__c(
				ARAging__c = null,
				AsOfDate__c = t.NU__Date__c,
				Balance__c = 5.00,
				BillTo__c = t.NU__BillTo__c,
				DaysOutstanding__c = 10,
				Entity__c = t.NU__Entity2__c,
				Order__c = t.NU__Order__c);
			arSnapshotList.add(snapshot);		
		}
		insert arSnapshotList;
		
		return arSnapshotList;
	}

	private static void insertARAgings(Id entityId) {
    	NU__ARAging__c currentARAging = NU.DataFactoryARAging.insertARAging('Current', 30, entityId, 'Current');
    	NU__ARAging__c over30DaysARAging = NU.DataFactoryARAging.insertARAging('Over 30 days', 60, entityId, 'Over 30 Days');
    	NU__ARAging__c over120DaysARAging = NU.DataFactoryARAging.insertARAging('Over 120 days', 120, entityId, 'Over 120 Days');
	}
    
}