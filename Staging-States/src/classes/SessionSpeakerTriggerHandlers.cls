public with sharing class SessionSpeakerTriggerHandlers extends NU.TriggerHandlersBase {
    
    public override void onAfterInsert(Map<Id, sObject> newRecordMap){
        Set<Id> speakerIds = new Set<Id>();
        
        for (Conference_Session_Speaker__c newSessionSpeaker : (List<Conference_Session_Speaker__c>) newRecordMap.values()){
            speakerIds.add(newSessionSpeaker.Speaker__c);
        }
        
        Map<Id, Account> speakersMap = new Map<Id, Account>(
            [select id,
                    name,
                    LeadingAge_Id__c
               from Account
              where id in :speakerIds]
        );
            
        List<Conference_Session__c> sessionsToUpdate = new List<Conference_Session__c>();
        Map<Id,String> sessionMessages = new Map<Id,String>();
            
        for (Conference_Session_Speaker__c newSessionSpeaker : (List<Conference_Session_Speaker__c>) newRecordMap.values()){
            Account speaker = speakersMap.get(newSessionSpeaker.Speaker__c);
                
            String newSpeakerMsg = newSessionSpeaker.Role__c + ' ' + speaker.LeadingAge_Id__c + ' ' +
                                   speaker.Name + ' was added.';

            if (sessionMessages.containsKey(newSessionSpeaker.Session__c)) {
                String temp = sessionMessages.get(newSessionSpeaker.Session__c);
                temp = temp + ' ' + newSpeakerMsg;
                sessionMessages.put(newSessionSpeaker.Session__c, temp);
            }
            else {
                sessionMessages.put(newSessionSpeaker.Session__c, newSpeakerMsg);
            }
        }
        
        for (Id s : sessionMessages.keySet()) {
            String m = sessionMessages.get(s);
            if (m.length() > 255) {
                m = 'Multiple speakers were added in the same DML operation.';
            }
            
            Conference_Session__c sessionToUpdate = new Conference_Session__c(
                Id = s,
                Speaker_Change__c = m
            );
            sessionsToUpdate.add(sessionToUpdate);
        }
        
        update sessionsToUpdate;
    }
    
    public override void onBeforeDelete(Map<Id, sObject> oldRecordMap){
        Set<Id> speakerIds = new Set<Id>();
        
        for (Conference_Session_Speaker__c deletedSessionSpeaker : (List<Conference_Session_Speaker__c>) oldRecordMap.values()){
            speakerIds.add(deletedSessionSpeaker.Speaker__c);
        }
        
        Map<Id, Account> speakersMap = new Map<Id, Account>(
            [select id,
                    name,
                    LeadingAge_Id__c
               from Account
              where id in :speakerIds]
        );
            
        List<Conference_Session__c> sessionsToUpdate = new List<Conference_Session__c>();
            
        for (Conference_Session_Speaker__c deletedSessionSpeaker : (List<Conference_Session_Speaker__c>) oldRecordMap.values()){
            Account speaker = speakersMap.get(deletedSessionSpeaker.Speaker__c);
                
            String newSpeakerMsg = deletedSessionSpeaker.Role__c + ' ' + speaker.LeadingAge_Id__c + ' ' +
                                   speaker.Name + ' was deleted.';
                                   
            Conference_Session__c sessionToUpdate = new Conference_Session__c(
                Id = deletedSessionSpeaker.Session__c,
                Speaker_Change__c = newSpeakerMsg
            );
            
            sessionsToUpdate.add(sessionToUpdate);
        }
        
        update sessionsToUpdate;
    }
}