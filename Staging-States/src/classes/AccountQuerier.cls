/**
 * @author NimbleUser
 * @date Updated: 1/30/13
 * @description This class provides common Account queries that can be used throughout the org.
 */
public with sharing class AccountQuerier {
    
    /**
     * @description Queries an Account by its Id.
     * @param Id The Id of the account to query.
     * @return The Account queried by id.
     */
     
    public static Account getAccountById(Id accountId){
        if (accountId == null){
            return null;
        }
        
        List<Account> accounts = getAccountsByIds(new Set<Id>{ accountId });
        
        if (accounts.size() > 0){
            return accounts[0];
        }
        
        return null;
    }
    
    /**
     * @description Queries Accounts by their Ids.
     * @param Set<Id> The Ids of the accounts to query.
     * @return The Accounts queried by their Ids.
     */
    public static List<Account> getAccountsByIds(Set<Id> accountIds){
        if (accountIds == null || accountIds.size() == 0){
            return new List<Account>();
        }
        
        return [Select State_Partner_Id__c,
                       Revenue_Year_Submitted__c,
                       RecordTypeId,
                       Provider_Membership__c,
                       Provider_Membership__r.Last_Provider_Lapsed_On__c,
                       Provider_Member__c,
                       Provider_Member_Thru__c,
                       Provider_Lapsed__c,
                       Provider_Lapsed_On__c,
                       Provider_Join_On__c,
                       Program_Service_Revenue__c,
                       Dues_Price__c,
                       Name,
                       NU__Status__c,
                       NU__StatusMembership__c,
                       NU__StatusMembershipFlag__c,
                       NU__Membership__c,
                       NU__Membership__r.Last_Provider_Lapsed_On__c,
                       NU__MembershipType__c,
                       NU__Member__c,
                       NU__MemberThru__c,
                       NU__Lapsed__c,
                       NU__LapsedOn__c,
                       NU__JoinOn__c,
                       IAHSA_Join_On__c,
                       LastName,
                       FirstName,
                       Cast_Join_On__c
                       //State_Partner_Name__c
                  From Account
                 where id in :accountIds];
    }
    
    public static Map<Id, Account> getAccountsWithStatePartnerInformationByIds(Set<Id> accountIds){
        if (accountIds == null || accountIds.size() == 0){
            return new Map<Id, Account>();
        }
        
        return new Map<Id, Account>(
               [select id,
                       name,
                       Provider_Lapsed_On__c,
                       State_Partner_Id__c,
                       State_Partner_Id__r.Name,
                       //State_Partner_Name__c,
                       //Parent_State_Partner_Name__c,
                       //Grandparent_State_Partner_Name__c,
                       State_Partner_Permissions__c,
                       NU__PrimaryAffiliation__c,
                       NU__PrimaryAffiliation__r.State_Partner_Id__c,
                       NU__PrimaryAffiliation__r.State_Partner_Id__r.Name,
                       //NU__PrimaryAffiliation__r.State_Partner_Name__c,
                       //NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c,
                       //NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
                       NU__PrimaryAffiliation__r.State_Partner_Permissions__c,
                       NU__PrimaryAffiliation__r.RecordTypeId,
                       OwnerId,
                       NU__Membership__c,
                       NU__Membership__r.Last_Provider_Lapsed_On__c,
                       Provider_Membership__c,
                       Provider_Membership__r.Last_Provider_Lapsed_On__c
                  from Account
                 where id in :accountIds]);
    }
    
    public static Map<Id, Account> getAccountsWithStatePartnerInformationAndMembershipByIds(Set<Id> accountIds){
        if (accountIds == null || accountIds.size() == 0){
            return new Map<Id, Account>();
        }
        
        return new Map<Id, Account>(
               [select id,
                       name,
                       Provider_Lapsed_On__c,
                       State_Partner_Id__c,
                       //State_Partner_Name__c,
                       NU__PrimaryAffiliation__c,
                       NU__PrimaryAffiliation__r.State_Partner_Id__c,
                       NU__PrimaryAffiliation__r.RecordTypeId,
                       OwnerId,
                       NU__Membership__c,
                       NU__Membership__r.Last_Provider_Lapsed_On__c,
                       Provider_Membership__c,
                       Provider_Membership__r.Last_Provider_Lapsed_On__c
                  from Account
                 where id in :accountIds OR NU__PrimaryAffiliation__c in :accountIds]);
    }
    
    public static Account getMultiSiteOrgWithProviderMembers(Id multiSiteOrgAccountId){
    	if (multiSiteOrgAccountId == null) {
    		return null;
    	}
    	
        List<Account> accounts = 
        [select id,
                name,
                Multi_Site_Provider_Member_Count__c,
                Multi_Site_Provider_Non_Member_Count__c,
                Multi_Site_Corporate__c,
                Provider_Membership__c,
                RecordTypeId,
                RecordType.Name,
                (Select id,
                        name,
                        Multi_Site_Provider_Member_Count__c,
                        Multi_Site_Provider_Non_Member_Count__c,
                        Provider_Member__c,
                        Provider_Membership__c,
                        Provider_Membership__r.Id,
                        Provider_Membership__r.NU__Account__c,
                        Provider_Membership__r.NU__StartDate__c,
                        Provider_Membership__r.NU__EndDate__c,
                        RecordTypeId,
                        RecordType.Name
                   from NU__Accounts1__r
                  where RecordTypeId = :Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID
                    and Provider_Member__c = 'Yes'
                  order by Provider_Membership__r.NU__EndDate__c desc),
                (Select id,
                        name
                   from NU__Memberships__r)
           from Account
          where id = :multiSiteOrgAccountId];
          
        if (accounts.size() > 0){
            return accounts[0];
        }
        
        return null;
    }
    
    public static List<Account> getAllSpeakersByConferenceId(Id conferenceId){
        return [Select Academic_Institution__c,
                       Academic_Institution_Year__c,
                       BillingStreet,
                       BillingCity,
                       Speaker_Company_1__c,
                       Speaker_Company__c,
                       Speaker_Company_Name_Rollup__c,
                       BillingCountry,
                       NU__PersonEmail__c,
                       FirstName,
                       Formal_Name__c,
                       Speaker_Full_Address__c,
                       NU__FullName__c,
                       Highest_Degree_Level__c,
                       Id,
                       Knowledge_and_Professional_Experience__c,
                       LastName,
                       isLeadingAgeMember__c,
                       IsPersonAccount,
                       LeadingAge_Id__c,
                       Major_Discipline__c,
                       NU__MiddleName__c,
                       Name,
                       No_Degree__c,
                       Phone,
                       BillingPostalCode,
                       BillingState,
                       PersonTitle,
                       Speaker_Bio_Modified__c,
                       Speaker_Bio_Validated__c, 
                       ( Select id,
                               Session__r.Conference__c,
                               Session__r.Session_Number__c,
                               Session__r.Session_Title_Full__c,
                               Session_Title__c,
                               Overall_Average_Score__c,
                               Topic_Score__c,
                               Audience_Score__c,
                               Presentation_Score__c,
                               Info_Score__c 
                           from Session_Speakers__r
                           order by Session__r.Conference__r.Name, Session__r.Session_Number__c)
                  from Account
                  where IsPersonAccount = true
                    and id in (select Speaker__c
                                 from Conference_Session_Speaker__c
                                where Session__r.Conference__c = :conferenceId)
                  order by LastName, FirstName];
    }
    
    public static List<Account> getSpeakersBySearchCriteria(String firstName, String lastName, String speakerCompany, String zipCode, String phone, String laid){
        return getSpeakersForEventBySearchCriteria(firstName, lastName, speakerCompany, zipCode, phone, laid, '');
    }
    
    public static List<Account> getSpeakersForEventBySearchCriteria(String firstName, String lastName, String speakerCompany, String zipCode, String phone, String laid, String eventId){
        String speakerQuery = 
                'Select ' +
                       'Academic_Institution__c, ' +
                       'Academic_Institution_Year__c, ' +
                       'BillingStreet, ' +
                       'Speaker_Address__c, ' +
                       'BillingCity, ' +
                       'Speaker_Company_1__c, ' +
                       'Speaker_Company_2__c, ' +
                       'Speaker_Company_Sort__c, ' +
                       'Speaker_Company__c, ' + 
                       'Speaker_Company_Name_Rollup__c,' + 
                       'BillingCountry, ' +
                       'NU__Designation__c, ' +
                       'NU__PersonEmail__c, ' +
                       'NU__ExternalId__c, ' + 
                       'FirstName, ' + 
                       'Formal_Name__c, ' + 
                       'Speaker_Full_Address__c, ' + 
                       'NU__FullName__c, ' + 
                       'Graduate_Degree__c, ' + 
                       'Graduate_School__c, ' + 
                       'Graduate_Major__c, ' + 
                       'Graduate_No_Degree__c, ' + 
                       'Graduate_Year__c, ' + 
                       'Highest_Degree_Level__c, ' +
                       'Id, ' + 
                       'Knowledge_and_Professional_Experience__c, ' +
                       'Phone, ' + 
                       'LastName, ' +  
                       'isLeadingAgeMember__c, ' +
                       'IsPersonAccount, ' +  
                       'LeadingAge_Id__c, ' +
                       'Major_Discipline__c, ' +
                       'NU__MiddleName__c, ' + 
                       'Name, ' + 
                       'No_Degree__c, ' +
                       'OwnerId, ' + 
                       'BillingPostalCode, ' + 
                       'Professional_Background__c, ' + 
                       'Profession__c, ' + 
                       'BillingState, ' + 
                       'PersonTitle,  ' + 
                       'Speaker_Bio_Modified__c, ' +
                       'Speaker_Bio_Validated__c, ' +
                       'Undergraduate_Degree__c, ' + 
                       'Undergraduate_Major__c, ' + 
                       'Undergraduate_School__c, ' + 
                       'Undergradudate_No_Degree__c, ' + 
                       'Undergradudate_Year__c, ' +
                       '( Select id, ' +
                               'Session__r.Conference__c, ' +
                               'Session__r.Session_Number__c, ' +
                               'Session__r.Session_Title_Full__c, ' +
                               'Session_Title__c, ' +
                               'Overall_Average_Score__c, ' +
                               'Topic_Score__c, ' +
                               'Audience_Score__c, ' +
                               'Presentation_Score__c, ' +
                               'Info_Score__c ' +
                          ' from Session_Speakers__r ' +
                          ' order by Session__r.Conference__r.Name, Session__r.Session_Number__c ) ' +
                  'from Account ';
        //speaker__r.LeadingAge_ID__c
        String whereClause = ' IsPersonAccount = true ';
        
        if (String.isNotBlank(firstName)){
            whereClause += ' and FirstName LIKE \'' + String.escapeSingleQuotes(firstName) + '%\' ';
        }
        
        if (String.isNotBlank(lastName)){
            if (String.isNotBlank(whereClause)){
                whereClause += ' and ';
            }
            
            whereClause += 'LastName LIKE \'%' + String.escapeSingleQuotes(lastName) + '%\' ';
        }
        
        if (String.isNotBlank(speakerCompany)){
            if (String.isNotBlank(whereClause)){
                whereClause += ' and ';
            }
            
            whereClause += 'Speaker_Company_Name_Rollup__c LIKE \'%' + String.escapeSingleQuotes(speakerCompany) + '%\'';
        }
        
        if (String.isNotBlank(zipCode)){
            if (String.isNotBlank(whereClause)){
                whereClause += ' and ';
            }
            
            whereClause += 'BillingPostalCode = :zipCode';
        }
        
        if (String.isNotBlank(phone)){
            if (String.isNotBlank(whereClause)){
                whereClause += ' and ';
            }
            
            whereClause += 'Phone = :phone';
        }

        if (String.isNotBlank(laid)){
            if (String.isNotBlank(whereClause)){
                whereClause += ' and ';
            }
            
            whereClause += 'LeadingAge_ID__c = :laid';
        }
        
        if (String.isNotBlank(eventId)){
            if (String.isNotBlank(whereClause)){
                whereClause += ' and ';
            }
            
            whereClause += 'Id IN (SELECT Speaker__c FROM Conference_Session_Speaker__c WHERE Session__r.Conference__c = :eventId)';
        }
        
        if (String.isNotBlank(whereClause)){
            speakerQuery += ' where ' + whereClause;
        }
        
        String orderBy = ' order by LastName, FirstName';
        speakerQuery += orderBy;
        
        system.debug('    speakerQuery is ' + speakerQuery);
        
        return (List<Account>) Database.query(speakerQuery);
    }
    
    private static Map<string, Id> PostalStateToStatePartnerIdMap_priv = new Map<string, Id>();
    public static Map<string, Id> PostalStateToStatePartnerIdMap {
    	get
    	{
    		if (Test.isRunningTest()) {
    			if (PostalStateToStatePartnerIdMap_priv.size() == 0) {
	    			RecordType statePartnerAccountRT = AccountRecordTypeUtil.getStatePartnerRecordType();
	    			List<Account> partners = [SELECT Id, BillingState FROM Account WHERE RecordTypeId = :statePartnerAccountRT.Id];
	    			for (Account a : partners) {
	    				PostalStateToStatePartnerIdMap_priv.put(a.BillingState, a.Id);
	    			}
    			}
   				return PostalStateToStatePartnerIdMap_priv;
    		}
    		else {
	    		return new Map<string, Id>{  'LeadingAge' => '001d000000zu5kOAAQ',
		         'DC' => '001d000000zuCcgAAE',
		         'FL' => '001d000000zu6yCAAQ',
		         'GA' => '001d000000zu761AAA',
		         'LA' => '001d000000zu7CDAAY',
		         'AL' => '001d000000zu6QrAAI',
		         'AZ' => '001d000000zu75qAAA',
		         'CA' => '001d000000zu6cJAAQ',
		         'CO' => '001d000000zu6y7AAA',
		         'CT' => '001d000000zu6yBAAQ',
		         'IL' => '001d000000zu6cLAAQ',
		         'IN' => '001d000000zu6cMAAQ',
		         'IA' => '001d000000zu6cNAAQ',
		         'KS' => '001d000000zu6yJAAQ',
		         'KY' => '001d000000zu763AAA',
		         'ME' => '001d000000zu75PAAQ',
		         'NH' => '001d000000zu75PAAQ',
		         'MD' => '001d000000zuCceAAE',
		         'MA' => '001d000000zu6cQAAQ',
		         'MI' => '001d000000zu79OAAQ',
		         'MN' => '001d000000zu6cRAAQ',
		         'MO' => '001d000000zu6yVAAQ',
		         'NE' => '001d000000zu6ycAAA',
		         'NJ' => '001d000000zu6cbAAA',
		         'NY' => '001d000000zu6cgAAA',
		         'NC' => '001d000000zu766AAA',
		         'OH' => '001d000000zu6ykAAA',
		         'OK' => '001d000000zu6nsAAA',
		         'OR' => '001d000000zu7bEAAQ',
		         'PA' => '001d000000zu6ytAAA',
		         'RI' => '001d000000zu765AAA',
		         'SC' => '001d000000zu6W6AAI',
		         'TN' => '001d000000zu7CiAAI',
		         'TX' => '001d000000zu6z0AAA',
		         'VT' => '001d000000zuBQzAAM',
		         'VA' => '001d000000zu764AAA',
		         'WA' => '001d000000zu6zeAAA',
		         'WI' => '001d000000zu6d0AAA',
		         'WY' => '001d000000zu6TVAAY',
		         'MT' => '001d000000zu7suAAA',
		         'MS' => '001d000000zu7CDAAY', //Uses Gulf States instead
		         'SD' => '001d000000zu7K7AAI'
				};
    		}
    	}
    }
    
     public static Map<Id, string> StatePartnerIdToName {
     	get {
     		return new Map<Id, string>{ '001d000000zu5kOAAQ' => 'LeadingAge',
		     '001d000000zuCcgAAE' => 'LeadingAge DC' ,
		     '001d000000zu6yCAAQ' => 'LeadingAge Florida',
		     '001d000000zu761AAA' => 'LeadingAge Georgia',
		     '001d000000zu7CDAAY' => 'LeadingAge Gulf States',
		     '001d000000zu6QrAAI' => 'LeadingAge Alabama',
		     '001d000000zu75qAAA' => 'LeadingAge Arizona',
		     '001d000000zu6cJAAQ' => 'LeadingAge California',
		     '001d000000zu6y7AAA' => 'LeadingAge Colorado',
		     '001d000000zu6yBAAQ' => 'LeadingAge Connecticut',
		     '001d000000zu6cLAAQ' => 'LeadingAge Illinois',
		     '001d000000zu6cMAAQ' => 'LeadingAge Indiana',
		     '001d000000zu6cNAAQ' => 'LeadingAge Iowa',
		     '001d000000zu6yJAAQ' => 'LeadingAge Kansas',
		     '001d000000zu763AAA' => 'LeadingAge Kentucky',
		     '001d000000zu75PAAQ' => 'LeadingAge Maine & New Hampshire',
		     '001d000000zu75PAAQ' => 'LeadingAge Maine & New Hampshire',
		     '001d000000zuCceAAE' => 'LeadingAge Maryland',
		     '001d000000zu6cQAAQ' => 'LeadingAge Massachusetts',
		     '001d000000zu79OAAQ' => 'LeadingAge Michigan',
		     '001d000000zu6cRAAQ' => 'LeadingAge Minnesota',
		     '001d000000zu6yVAAQ' => 'LeadingAge Missouri',
		     '001d000000zu6ycAAA' => 'LeadingAge Nebraska',
		     '001d000000zu6cbAAA' => 'LeadingAge New Jersey',
		     '001d000000zu6cgAAA' => 'LeadingAge New York',
		     '001d000000zu766AAA' => 'LeadingAge North Carolina',
		     '001d000000zu6ykAAA' => 'LeadingAge Ohio',
		     '001d000000zu6nsAAA' => 'LeadingAge Oklahoma',
		     '001d000000zu7bEAAQ' => 'LeadingAge Oregon',
		     '001d000000zu6ytAAA' => 'LeadingAge PA',
		     '001d000000zu765AAA' => 'LeadingAge RI',
		     '001d000000zu6W6AAI' => 'LeadingAge South Carolina',
		     '001d000000zu7CiAAI' => 'LeadingAge Tennessee',
		     '001d000000zu6z0AAA' => 'LeadingAge Texas',
		     '001d000000zuBQzAAM' => 'LeadingAge Vermont',
		     '001d000000zu764AAA' => 'LeadingAge Virginia',
		     '001d000000zu6zeAAA' => 'LeadingAge Washington',
		     '001d000000zu6d0AAA' => 'LeadingAge Wisconsin',
		     '001d000000zu6TVAAY' => 'LeadingAge Wyoming',
		     '001d000000zu7suAAA' => 'MHA...An Association of Montana Health Care Providers',
		     '001d000000zu7CDAAY' => 'Mississippi Association of Homes and Services for the Aging', //Returns ID for Gulf States on purpose
		     '001d000000zu7K7AAI' => 'South Dakota Association of Healthcare Organizations'
			};
     	}
     }
}