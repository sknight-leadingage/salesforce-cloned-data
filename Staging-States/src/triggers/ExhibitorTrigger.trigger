trigger ExhibitorTrigger on Exhibitor__c (before insert, before update, before delete, after update, after insert, after delete, after undelete) {
        NU.TriggerHandlerManager.executeHandlers('ExhibitorTrigger', new ExhibitorTriggerHandlers());
}