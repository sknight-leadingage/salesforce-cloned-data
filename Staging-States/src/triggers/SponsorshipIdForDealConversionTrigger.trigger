trigger SponsorshipIdForDealConversionTrigger on Sponsorship__c (after insert) {
    List<Sponsorship__c> sponsorships = Trigger.new;

    for (Sponsorship__c sponsorship : sponsorships) {
        ConvertDealCartToOrder.getInstance().setSponsorship(sponsorship.Id);
    }
}