trigger ConferenceTimeslotTrigger on Conference_Timeslot__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	NU.TriggerHandlerManager.executeHandlers('ConferenceTimeslotTrigger', new ConferenceTimeslotTriggerHandlers());
}