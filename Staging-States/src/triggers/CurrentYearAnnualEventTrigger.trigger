trigger CurrentYearAnnualEventTrigger on NU__Event__c (before insert, before update, before delete, after update, after insert, after delete, after undelete) {
        NU.TriggerHandlerManager.executeHandlers('CurrentYearAnnualEventTrigger', new EventTriggerHandlers());
}