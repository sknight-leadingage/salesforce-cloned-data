trigger CustomMembershipTypeTrigger on NU__MembershipType__c (after update) {
	Set<Id> updatedMembershipTypes = new Set<Id>();
	
	// need to check to see if the grace period or name is changed
	for (NU__MembershipType__c membershipType : Trigger.new) {
		NU__MembershipType__c oldMembershipType = Trigger.oldMap.get(membershipType.Id);
		
		if (oldMembershipType != null && 
			(oldMembershipType.Name != membershipType.Name || oldMembershipType.NU__GracePeriod__c != membershipType.NU__GracePeriod__c)) {
				
			updatedMembershipTypes.add(membershipType.Id);
		}
	}
	
	if (updatedMembershipTypes.size() > 0) {
		Database.executeBatch(new MembershipTypeBatchUpdate(updatedMembershipTypes), MembershipTypeBatchUpdate.batchSize);
	}
}