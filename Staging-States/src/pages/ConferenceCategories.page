<apex:page controller="ConferenceCategoriesController">
<script>

function setFocusOnLoad() {}

</script>

    <style>
        .pbTitle {
            width: 100% !important;
        }
    
        .pbSubheader {
            color: black !important; 
        }
        .sameTextBoxWidth {
            width: 50%;
        }
    </style>

    <apex:pageMessages />
    
    <apex:pageMessage summary="{! Conference.Name } has no sessions."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria == false }" />
                      
    <apex:pageMessage summary="No sessions found."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria }" />
    
    <apex:form >
        <apex:pageBlock title="{! Conference.Name } Categories" rendered="{! ResultSize > 0 }">
        
            <div style="clear: both; height: 40px;">
                <apex:panelGrid columns="8" rendered="{! ResultSize > 0 }" style="float: left">
                    <apex:commandLink action="{!first}" rendered="{! ResultSize > 1 && PageNumber != 1 }">First</apex:commandlink>
                    <apex:commandLink action="{!previous}" rendered="{!hasPrevious}">Previous</apex:commandlink>
                    <apex:outputText value="Record {! PageNumber } of {! ResultSize }" />
                    <apex:commandLink action="{!next}" rendered="{!hasNext}">Next</apex:commandlink>
                    <apex:commandLink action="{!last}" rendered="{! ResultSize > 1 && PageNumber != ResultSize }">Last</apex:commandlink>
                    <apex:outputText value=" &nbsp;&nbsp;&nbsp; " escape="false"/>
                    <apex:inputText value="{!RecordNum}" label="Record #" style="width: 25px;" rendered="{!ResultSize > 1}"/>
                    <apex:commandButton action="{!GoToRecord}" rendered="{!ResultSize > 1}" value="GoTo Record"/>
                </apex:panelGrid>
            
           
                <div style="float: right;">
                    <c:ConferenceNavComponent eventId="{! EventId }" />
                </div>
            </div>
            
            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>
            </div>
        
            <apex:pageBlockSection columns="3" title="Session Info" collapsible="false">
                <apex:outputField value="{! CurrentSession.Conference_Track__c }" />
                
                <apex:outputField value="{! CurrentSession.Education_Staff__c }" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Proposal__c.Fields.Proposal_Number__c.Label }" />
                
                    <apex:outputText value="{! If (CurrentSession.Proposal__c != null, CurrentSession.Proposal__r.Proposal_Number__c, '') }" />
                
                </apex:pageBlockSectionItem>
                
                
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="1">
                <apex:outputField value="{! CurrentSession.Session_Title_Full__c }" />
                
                <apex:inputField value="{! CurrentSession.Comment__c }" styleClass="sameTextBoxWidth" />
                
                <apex:outputField value="{! CurrentSession.Learning_Objective_1__c }" />
                
                <apex:outputField value="{! CurrentSession.Learning_Objective_2__c }" />
                
                <apex:outputField value="{! CurrentSession.Learning_Objective_3__c }" />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="2">
                <apex:pageBlockSectionItem >
                    <c:MultiSelectComponent leftLabel="Available Content Categories"
                                            leftOpts="{!AvailableContentCategorySelectOpts}"
                                            rightLabel="Selected Content Categories"
                                            rightOpts="{!SelectedContentCategorySelectOpts}"
                                            size="14"
                                            width="150px"
                                            ShowUpDownButtons="false" />
                     
                
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <c:MultiSelectComponent leftLabel="Available CE Categories"
                                            leftOpts="{!AvailableCECategorySelectOpts}"
                                            rightLabel="Selected CE Categories"
                                            rightOpts="{!SelectedCECategorySelectOpts}"
                                            size="14"
                                            width="150px"
                                            ShowUpDownButtons="false" />
                
                </apex:pageBlockSectionItem>
            
            </apex:pageBlockSection>
            
            <!--Move Certification section from Session page-->
            <apex:pageBlockSection title="Certification"
                                   columns="2"
                                   rendered="{! ResultSize > 0 }">
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="NAB" />
                    
                    <apex:selectList id="NABSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.NAB__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! NABDomainsOfPracticeSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                 <apex:pageBlockSectionItem >
                    <apex:outputText value="CA-RCFE" />
                    
                    <apex:selectList id="CA-RCFESelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.CA_RCFE__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! CARCFEDomainsOfPracticeSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="NASBA" />
                    
                    <apex:selectList id="NASBASelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.NASBA__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! NASBADomainsOfPracticeSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Kansas" />
                    
                    <apex:selectList id="KansasSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.Kansas__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! KansasDomainsOfPracticeSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Missouri" />
                    
                    <apex:selectList id="MissouriSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.Missouri__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! MissouriDomainsOfPracticeSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Florida" />
                    
                    <apex:selectList id="FloridaSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.Florida__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! FloridaDomainsOfPracticeSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Activity Workers" />
                    
                    <apex:selectList id="ActivityWorkersSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.Activity_Workers_Certification__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! ActivityWorkersCertificationSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="CFRE" />
                    
                    <apex:selectList id="CFRESelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.CFRE_Certification__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! CFRECertificationSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:inputField value="{! CurrentSession.PTF_Code__c }" label="CE Hours" />

            </apex:pageBlockSection>
            <!-- End Certification section-->
            
            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>
            </div>
            
        </apex:pageBlock>
        
        <apex:pageBlock title="Search" rendered="{! ResultSize > 0 || HasSearchCriteria }">
            <apex:pageBlockSection columns="2">
                <apex:inputText value="{! SearchCriteria.SessionNumber }" label="{! $ObjectType.Conference_Session__c.Fields.Session_Number__c.Label }" />
                
                <apex:inputText value="{! SearchCriteria.ProposalNumber }" label="{! $ObjectType.Conference_Proposal__c.Fields.Proposal_Number__c.Label }" />
                
                <apex:inputText value="{! SearchCriteria.Title }" label="Session {! $ObjectType.Account.Fields.PersonTitle.Label }" />
                
                <apex:inputText value="{! SearchCriteria.TrackName }" label="{! $ObjectType.Conference_Track__c.Fields.Name.Label }" />
                
                <apex:inputText value="{! SearchCriteria.EducationStaffFirstName }" label="Education Staff First Name" />

                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                    <apex:commandButton value="Search" action="{! search }" />
                    <apex:commandButton value="Clear Search"
                                        action="{! clearSearch }"
                                        style="margin-left: 10px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>