<apex:component controller="NU.OrderPurchaseSubscription" allowDML="true">

    <apex:attribute name="c" type="NU.OrderController" description="OrderController" assignTo="{!Controller}"/>

    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd();NUBind()" id="ASBind"/>

    <apex:actionRegion >

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/stamp32.png)"/>
                <h1 class="pageType">{!c.Subheader}&nbsp;</h1>
                <h2 class="pageDescription">{!IF(IsEDIT, 'Edit', 'Add')} Subscription</h2>
            </div>
        </div>
    </div>

    <apex:pageMessages escape="{!c.EscapeMessages}" id="Msgs" />

    <apex:pageBlock mode="maindetail" Id="Main">

        <apex:pageBlockSection columns="1">
            <apex:inputField value="{!Controller.CurrentCartItem.NU__Customer__c}" rendered="{!!IsEdit}" id="Account">
                <apex:actionSupport event="onchange" action="{!OnAccountSelected}" rerender="Msgs,Inputs,ItemLines,Buttons" status="ASBind"/>
            </apex:inputField>
            <apex:outputField value="{!Controller.CurrentCartItem.NU__Customer__c}" rendered="{!IsEdit}"/>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && !IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__Entity__c}">
                    <apex:selectOptions value="{!EntityOptions}"/>
                    <apex:actionSupport event="onchange" onsubmit="if(!confirmEntitySwitch()) { this.value = '{!Controller.CurrentCartItem.NU__Entity__c}'; return false; }" action="{!OnEntitySelected}" rerender="Msgs,Main" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:outputField value="{!Controller.CurrentCartItem.NU__Entity__c}"/>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity__c != null && Controller.CurrentCartItem.NU__Customer__c != null}" id="PriceClass">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__PriceClass__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__PriceClass__c}">
                    <apex:selectOption itemvalue="" itemLabel="Please select a price class" rendered="{!Controller.CurrentCartItem.NU__PriceClass__c == null}"/>
                    <apex:selectOptions value="{!PriceClassOptions}"/>
                    <apex:actionSupport event="onchange" action="{!OnPriceClassSelected}" rerender="Msgs,ItemLines,Buttons" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Subscription Products" columns="1" rendered="{!Controller.Cart.NU__Entity__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Controller.CurrentCartItem.NU__PriceClass__c != null}">
            <apex:pageBlockTable value="{!ProductItemLines}" var="oli" id="ItemLines" >
                <apex:column >
                    <apex:facet name="header">Purchase</apex:facet>
                    <apex:inputField value="{!oli.NU__IsInCart__c}" styleClass="{!IF(oli.Id == null && oli.NU__OrderItemLine__c == null,'selectableRow','unselectableRow')}" rendered="{!!oli.Product__r.NU__TrackInventory__c || oli.Product__r.NU__InventoryOnHand__c > 0}"/>
                </apex:column>
                <apex:column value="{!oli.NU__Product__c}"/>
                <apex:column >
                    <apex:facet name="header">{!$ObjectType.NU__Membership__c.fields.NU__StartDate__c.label}</apex:facet>
                    <c:Required ><apex:inputField value="{!SubscriptionDates[oli.NU__Product__c].StartDate__c}" styleClass="hideDateLink" /></c:Required>
                </apex:column>
                <apex:column >
                    <apex:facet name="header">{!$ObjectType.NU__Membership__c.fields.NU__EndDate__c.label}</apex:facet>
                    <c:Required ><apex:inputField value="{!SubscriptionDates[oli.NU__Product__c].EndDate__c}" styleClass="hideDateLink" /></c:Required>
                </apex:column>
                <apex:column styleClass="number" headerClass="number">
                    <apex:facet name="header">Price</apex:facet>
                    <div style="display: inline-block"><c:Required ><apex:inputField value="{!oli.NU__UnitPrice__c}" required="false" styleClass="inputPrice" onfocus="this.maxLength = {! UnitPriceMaxLength };" /></c:Required></div>
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlockSection>

        <apex:pageBlockButtons location="both" id="Buttons">
            <apex:commandButton value="Save" styleClass="green" action="{!Save}" rerender="Msgs" rendered="{!Controller.Cart.NU__Entity__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Controller.CurrentCartItem.NU__PriceClass__c != null}" status="AS"/>
            <apex:commandButton value="Cancel" action="{!Cancel}" rerender="Msgs,Main" status="AS"/>
        </apex:pageBlockButtons>
    </apex:pageBlock>

    </apex:actionRegion>

</apex:component>