<apex:component id="nusidebar" allowDML="true">

    <apex:attribute name="c" type="NU.OrderController" description="OrderController"/>

    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd()" id="AS"/>

    <style type="text/css">
        .stage{!c.SidebarStage.SidebarStage} {
            background-color: white;
            border-left: 1px white solid;
            border-right: 1px white solid;
            border-top: 1px #eaeaea solid;
            border-bottom: 1px #eaeaea solid;
            margin: -1px !important;
        }
        .navigation .pbBody
        {
            margin: 9px 0px !important;
        }
        .sidebarList>li
        {
            padding-left: 12px;
            padding-right: 12px;
        }
        .nusidebar .properties .pbBottomButtons .pbTitle
        {
            padding: 0px !important;
            width: 0px !important;
        }
        .requiredInput .requiredBlock {
            background-color: #C00;
            position: absolute;
            left: -4px;
            width: 3px;
            top: 1px;
            bottom: 1px;
        }
        .requiredInput {
            position: relative;
            height: 100%;
        }
        .datePicker {
            z-index: 50102!important;
        }
        .blackBotBrdr { border-bottom-color: #999 !important; }
        .stageAddItems>.labelCol { cursor: pointer; }
        .properties .labelCol { text-align: left; }
        .properties .bPageBlock .detailList .data2Col { text-align: right; }
        .properties .buttons { padding: 10px; }
        .properties .buttons .links {
            padding-top: 10px;
        }
        .navigation table { width: 100%; }
        .navigation td.link { padding: 5px 10px 5px 14px; }
        .navigation td.status { padding: 5px 14px 5px 10px; text-align: right; }
        .navigation .status.warning { background-position: 67% 0 }
        .navigation .status.complete { background-position: 33% 0 }
        .navigation .status.err { background-position: 100% 0 }
        .navigation .status {
            float: right;
            margin-right: 2px;
            background-image: url({!URLFOR($Resource.Status)});
            width: 15px;
            height: 15px;
        }
        .invisible{display:none;}
    </style>

    <!-- <apex:outputPanel rendered="{! c.SidebarButton.SidebarButtonVisible == false && c.CurrentCartItem.OrderItem__c == null }">
        <style type="text/css">
            .nusidebar div.pbBottomButtons { display:none; }
        </style>
    </apex:outputPanel> -->

    <div class="nusidebar">

    <div class="navigation">
    <apex:pageBlock title="What Do You Want To Do?" id="progress">
        <ul class="sidebarList">
            <li class="stageOrderInfo">
                <apex:outputLink value="javascript:void(0)" styleClass="labelCol" onclick="showOrderDetailsDialog();">Order Info</apex:outputLink>

                <script type="text/javascript">
                    function showOrderDetailsDialog() {
                        buildBatchDropdown();
                        CustomDialog.show('OrderDetails');
                    }
                </script>

                <span class="status {!IF(ISBLANK(c.Cart.Id),'','complete')}"
                      title="{! IF(ISBLANK(c.Cart.Id), c.GreyedIconMsg, c.DoneIconMsg) }"/>
            </li>
            <li class="stageAddItems">
                <apex:outputLink value="javascript:void(0)" styleClass="labelCol">Add Items</apex:outputLink>
                <span id="addItemsButton">
                    <span id="addItemsArrow"/>
                    <span id="addItemsContainer">
                        <span id="addItemsMenu">
                            <apex:repeat value="{!c.ProductTypes}" var="pt">
                                <apex:outputLink value="{!pt.NU__OrderPageRelativeURL__c}{!c.params}" disabled="{!pt.NU__Status__c = 'Inactive'}">{!pt.Name}</apex:outputLink>
                            </apex:repeat>
                        </span>
                    </span>
                </span>
                <span class="status {!IF(c.CartItems.size > 0,'complete','')}"
                      title="{! IF(c.CartItems.size > 0, c.DoneIconMsg, c.GreyedIconMsg) }"/>
            </li>
            <li class="stageViewCart">
                <apex:outputLink value="/apex/Order{!c.params}" styleClass="labelCol">Verify Cart ({!c.CartItems.size})</apex:outputLink>
                <span class="status {!IF(c.CartItems.size > 0,'complete','')}"
                      title="{! IF(c.CartItems.size > 0, c.DoneIconMsg, c.GreyedIconMsg) }"/>
            </li>

            <li class="{!IF(c.cartHasTaxableOrShippingItems, 'stageTaxAndShipping', 'invisible')}">
            <!-- <li class="stageTaxAndShipping"> -->
                <apex:outputLink value="/apex/OrderTaxAndShipping{!c.params}" styleClass="labelCol">Tax &amp; Shipping</apex:outputLink>
                <span class="status {!IF(c.IsShippingComplete,'complete',IF($CurrentPage.Name == 'OrderPayment', 'err', ''))}"/>
            </li>
            <li class="stagePayment">
                <apex:outputLink value="/apex/OrderPayment{!c.params}" styleClass="labelCol">Payment</apex:outputLink>
                <span class="status {!IF(c.CartItems.size > 0, IF((c.Cart.Balance__c == 0 || ($CurrentPage.Name == 'OrderPayment')), 'complete', IF(c.Cart.Balance__c < 0, 'err', '')), '')}"
                    title="{!IF(c.CartItems.size > 0, IF((c.Cart.Balance__c == 0 || ($CurrentPage.Name == 'OrderPayment')), c.DoneIconMsg, IF(c.Cart.Balance__c < 0, c.PmtNegativeBalanceMsg, c.PmtWarningMsg)), c.GreyedIconMsg)}"/>
            </li>
        </ul>
    </apex:pageBlock>
    </div>

    <div class="properties">
    <apex:pageBlock title="Order Summary" id="OrderProperties">
        <apex:pageBlockSection columns="1">

            <apex:pageBlockSectionItem dataStyleClass="blackBotBrdr" labelStyleClass="blackBotBrdr" rendered="{!c.Entities.size != 1}">
                <apex:outputText value="Order Entity"/>
                <apex:outputLink value="/{!c.Cart.NU__Entity__c}">{!IF(!ISBLANK(c.Cart.Entity__r.NU__ShortName__c), c.Cart.Entity__r.NU__ShortName__c, c.Cart.Entity__r.Name)}</apex:outputLink>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem dataStyleClass="{! If(IsBlank(c.Cart.Order__c)=True , 'blackBotBrdr', '') }" labelStyleClass="{! If(IsBlank(c.Cart.Order__c)=True , 'blackBotBrdr', '') }">
                <apex:outputText value="Cart Number"/>
                <apex:outputLink value="/{!c.Cart.Id}">{!c.Cart.Name}</apex:outputLink>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem dataStyleClass="blackBotBrdr" labelStyleClass="blackBotBrdr" rendered="{! IsBlank(c.Cart.Order__c)=False }">
                <apex:outputText value="Order Number"/>
                <apex:outputLink value="/{!c.Order.Id}">{!c.Order.Name}</apex:outputLink>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem id="orderSBTotalSpan">
                <apex:outputLabel value="Subtotal"/>
                <apex:outputText value="{0,number,$#,##0.00}">
                    <apex:param value="{!c.Cart.NU__SubTotal__c}"/>
                </apex:outputText>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!c.Cart.NU__TotalDiscounts__c != 0}">
                <apex:outputLabel value="Discounts"/>
                <apex:outputText value="{0,number,($#,##0.00)}">
                    <apex:param value="{!c.Cart.NU__TotalDiscounts__c * -1}"/>
                </apex:outputText>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem dataStyleClass="blackBotBrdr" labelStyleClass="blackBotBrdr">
                <apex:outputText value="Tax & Shipping"/>
                <apex:outputField value="{!c.Cart.NU__TotalShippingAndTax__c}"/>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem >
                <apex:outputText value="Total"/>
                <apex:outputField value="{!c.Cart.NU__Total__c}"/>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem dataStyleClass="blackBotBrdr" labelStyleClass="blackBotBrdr">
                <apex:outputText value="Payment"/>
                <apex:outputField value="{!c.Cart.NU__TotalPayment__c}"/>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem >
                <apex:outputText value="Balance"/>
                <apex:outputField value="{!c.Cart.NU__Balance__c}"/>
            </apex:pageBlockSectionItem>

        </apex:pageBlockSection>

        <div class="buttons">
            <apex:actionRegion >
                <apex:commandButton value="{!c.Button.Text}" onclick="{!c.Button.OnClick}"  action="{!c.Button.Action}" styleClass="green largeButton" status="AS" />

                <div class="links">
                    <apex:commandLink value="Cancel {!IF(IsBlank(c.Cart.NU__Order__c), 'Cart', 'Changes')}"
                                        action="{!c.DeleteCart}"
                                        onclick="return confirmDelete()"
                                        rendered="{!c.Cart.Id != null}" />
                    <apex:outputText value=" | " rendered="{!(c.Cart.Id != null) && (!ISBLANK(c.Cart.NU__Order__c) && !c.AllCartItemsAreCancelled && c.CartItems.size > 1)}"/>
                       <apex:commandLink rendered="{!!ISBLANK(c.Cart.NU__Order__c) && !c.AllCartItemsAreCancelled && c.CartItems.size > 1}" value="Cancel All Items"
                                           action="{!c.CancelAllOrderItems}" />
                </div>
            </apex:actionRegion>
        </div>
    </apex:pageBlock>
    </div>

    <c:CustomDialog id="AddItems" title="Add Items">
        <apex:outputPanel id="AddItemsDialog">
            <apex:pageMessages escape="{!c.EscapeMessages}" />
            <apex:pageBlock mode="maindetail">
                <p>Which type of items do you want to add?</p>
                <c:Required >
                    <apex:selectRadio layout="pageDirection" value="{!c.AddItemsProductType}">
                        <apex:selectOptions value="{!c.ProductTypeOptions}"/>
                    </apex:selectRadio>
                </c:Required>
            </apex:pageBlock>
            <div class="buttons">
                <apex:commandButton value="Continue" action="{!c.OnAddItemsContinue}" styleClass="green" rerender="AddItemsDialog" status="AS"/>
                <apex:commandButton value="Cancel" onclick="CustomDialog.hide('AddItems');return false" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>

    <script type="text/javascript">
        var allBatches = {};
        <apex:repeat value="{!c.BatchMap}" var="b">
            allBatches["{!b}"] = ["{!JSENCODE(c.BatchMap[b].DisplayName__c)}", "<apex:outputText value="{0,date,M/d/yyyy}"><apex:param value="{!c.BatchMap[b].TransactionDate__c}"/></apex:outputText>"];
        </apex:repeat>

        //This cleans the date that comes from Salesforce which might come 1 day off because of the UTC
        var transactionDate = new Date('{!c.Cart.NU__TransactionDate__c}');
        cartDate = new Date(transactionDate.getUTCFullYear(), transactionDate.getUTCMonth(), transactionDate.getUTCDate());

        var batchesByEntity = {};
        <apex:repeat value="{!c.BatchesByEntity}" var="e">
            var batchesForEntity = [];
            if (!{!c.HasCreditCardPayment}) {
                <apex:repeat value="{!c.BatchesByEntity[e]}" var="b">
                    batchesForEntity.push("{!b.Id}");
                </apex:repeat>
            } else {
                <apex:repeat value="{!c.BatchesByEntity[e]}" var="b">
                    var batchDate = new Date('{!b.TransactionDate__c}');
                    batchDate = new Date(batchDate.getUTCFullYear(), batchDate.getUTCMonth(), batchDate.getUTCDate());
                    if (cartDate.valueOf() === batchDate.valueOf()) {
                        batchesForEntity.push("{!b.Id}");
                    }
                </apex:repeat>
            }
            batchesByEntity["{!e}"] = batchesForEntity;
        </apex:repeat>

        var orderTypesByEntity = {};
        <apex:repeat value="{!c.ProductTypesByEntity}" var="e">
            var configNameForOTByEntity = {};
            <apex:repeat value="{!c.ProductTypesByEntity[e]}" var="p">
                configNameForOTByEntity["{!p.Id}"] = "{!JSENCODE(p.Name)}";
            </apex:repeat>
            orderTypesByEntity["{!e}"] = configNameForOTByEntity;
        </apex:repeat>

        function createBatchField(batchField, backingField, entity, entityField) {
            $(batchField).change(function() {
                updateBatchBackingField(backingField, $(batchField).val());
            });
            if (entityField) {
                $(entityField).change(function() { updateBatchField(batchField, $(entityField).val()); });
            }
            updateBatchField(batchField, entity, $(backingField).val());
        }

        function updateBatchField(batchField, entity, value) {
            var batches = batchesByEntity[entity];
            $(batchField).empty();

            if ({!c.IsAutomaticBatchEnabled}) {
                $(batchField).append($("<option>").text("Automatic"));
            }
            if (batches) {
                for (var i = 0; i < batches.length; i++) {
                    $(batchField).append($("<option>").val(batches[i]).text(allBatches[batches[i]][0]));
                }
            }
            if (value) {
                $(batchField).find("option[value='" + value + "']").attr("selected", "selected");
            }
            $(batchField).change();
        }

        function updateBatchBackingField(backingField, value) {
            if (value != "Automatic" || {!c.IsAutomaticBatchEnabled}) {
                $(backingField).val(value == "Automatic" ? undefined : value);
            }
        }

        function createTransField(transField, transLabel, batchField) {
            $(batchField).change(function() { updateTransField(transField, transLabel, $(batchField).val()) });
            updateTransField(transField, transLabel, $(batchField).val());
        }

        function updateTransField(transField, transLabel, batch) {
            var automatic = batch == "Automatic";
            var hasCreditCardPayment = {!c.HasCreditCardPayment};
            $(transField).toggle(automatic && !hasCreditCardPayment);
            $(transLabel).toggle(hasCreditCardPayment || !automatic);
            if (!automatic) {
                $(transLabel).text(allBatches[batch][1]);
            }
            if (hasCreditCardPayment) {
                var transactionDate = new Date('{!c.Cart.NU__TransactionDate__c}');
                cartDate = new Date(transactionDate.getUTCFullYear(), transactionDate.getUTCMonth(), transactionDate.getUTCDate());
                var cartDateString = (cartDate.getMonth() + 1) + '/' + cartDate.getDate() + '/' + cartDate.getFullYear();
                $(transLabel).text(cartDateString);
            }
        }

        function createOrderTypeList(orderTypeList, backingField, entity, entityField) {
            if (entityField) {
                $(entityField).change(function() { updateOrderTypeList(orderTypeList, backingField, $(entityField).val()); });
            }
            updateOrderTypeList(orderTypeList, backingField, entity, $(backingField).val());
        }

        function updateOrderTypeList(orderTypeList, backingField, entity, value) {
            $(backingField).val(undefined);
            var orderTypes = orderTypesByEntity[entity];
            $(orderTypeList).empty();
            if (orderTypes) {
                for (var o in orderTypes) {
                    $(orderTypeList).append($("<tr>").append($("<td>").append($("<label>")
                        .text(orderTypes[o])
                        .prepend($("<input type='radio'>")
                            .attr("name", "nu-orderType")
                            .val(o)
                            .click(function (x) { $(backingField).val($(x.target).val()) })))));
                }
                $('#nu-orderTypeSection').show();
                $('#nu-orderTypeErrorNoOptions').hide();
            } else {
                $('#nu-orderTypeSection').hide();
                $('#nu-orderTypeErrorNoOptions').show();
            }
            if (value) {
                $(orderTypeList).find("input[value='" + value + "']").click();
            }
        }
    </script>

    <apex:actionStatus onStart="onAjaxStart()" onStop="buildOrderDetailUI();showOrderDetailsDialog();onAjaxEnd()" id="OD"/>
    <c:CustomDialog id="OrderDetails" title="Order Information">
        <apex:outputPanel layout="none" id="OrderPropertiesDialogPanel">
            <apex:pageMessage title="Info:" severity="info" strength="2" summary="Changes will not affect pricing of items already in the cart."/>
            <apex:pageMessage title="Info:" severity="info" strength="2" summary="{!c.CreditCardPaymentInCartMsg}" rendered="{!c.HasCreditCardPayment}"/>
            <apex:pageMessages escape="{!c.EscapeMessages}" />

            <apex:pageBlock mode="maindetail">
                <apex:pageBlockSection columns="1">

                    <apex:pageBlockSectionItem >
                        {!$ObjectType.NU__Cart__c.Fields.NU__BillTo__c.Label}
                        <apex:outputPanel layout="none">
                            <apex:outputField rendered="{!!ISBLANK(c.Cart.NU__Order__c)}" value="{!c.Cart.NU__BillTo__c}"/>
                            <c:Required ><apex:inputField rendered="{!ISBLANK(c.Cart.NU__Order__c)}" value="{!c.Cart.NU__BillTo__c}"/></c:Required>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!c.Entities.size != 1}">
                        {!IF(ISBLANK(c.Cart.NU__AdjustmentEntity__c),'Order','Adjustment')} Entity
                        <apex:outputLink value="/{!IF(ISBLANK(c.Cart.NU__AdjustmentEntity__c),c.Cart.NU__Entity__c,c.Cart.NU__AdjustmentEntity__c)}">
                            {!IF(ISBLANK(c.Cart.AdjustmentEntity__c),
                                IF(!ISBLANK(c.Cart.Entity__r.ShortName__c), c.Cart.Entity__r.ShortName__c, c.Cart.Entity__r.Name),
                                IF(!ISBLANK(c.Cart.AdjustmentEntity__r.ShortName__c), c.Cart.AdjustmentEntity__r.ShortName__c, c.Cart.AdjustmentEntity__r.Name)
                            )}
                        </apex:outputLink>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem >
                        {!$ObjectType.NU__Cart__c.Fields.NU__Batch__c.Label}
                        <c:Required >
                            <select size="1" id="nu-batchList2"></select>
                            <apex:inputHidden id="Batch" value="{!c.Cart.NU__Batch__c}"/>
                            <script type="text/javascript">
                                function getBatchField2() {
                                    return document.getElementById("{!$Component.Batch}");
                                }
                            </script>
                        </c:Required>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem >
                        {!$ObjectType.NU__Cart__c.Fields.NU__TransactionDate__c.Label}
                        <apex:outputPanel layout="none">
                            <c:Required id="TransDateField">
                                <apex:inputField value="{!c.Cart.NU__TransactionDate__c}" required="false"/>
                            </c:Required>
                            <apex:outputField id="TransDateLabel" value="{!c.Cart.NU__TransactionDate__c}"/>
                            <script type="text/javascript">
                                function getTransDateField2() {
                                    return document.getElementById("{!$Component.TransDateField}");
                                }
                                function getTransDateLabel2() {
                                    return document.getElementById("{!$Component.TransDateLabel}");
                                }
                            </script>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                       <apex:pageBlockSectionItem rendered="{! c.Cart.Order__c != null }">
                           <apex:outputText value="{! $ObjectType.Cart__c.Fields.PurchaseOrderNumber__c.Label }" />
                           <apex:inputField value="{!c.Cart.NU__PurchaseOrderNumber__c}" />
                       </apex:pageBlockSectionItem>
                       <apex:pageBlockSectionItem rendered="{!c.Cart.NU__Order__c != null && $Setup.NimbleAMSSettings__c.OverridableInvoiceNumber__c}">
                           <apex:outputText value="{! $ObjectType.Cart__c.Fields.InvoiceNumber__c.Label }" />
                           <apex:inputField value="{!c.Cart.NU__InvoiceNumber__c}" />
                       </apex:pageBlockSectionItem>
                       <apex:pageBlockSectionItem rendered="{! c.Cart.Order__c != null }">
                           <apex:outputText value="{! $ObjectType.Cart__c.Fields.InvoiceDescription__c.Label }" />
                           <apex:inputField value="{!c.Cart.NU__InvoiceDescription__c}" />
                       </apex:pageBlockSectionItem>
                       <apex:pageBlockSectionItem rendered="{! c.Cart.Order__c != null }">
                           <apex:outputText value="{! $ObjectType.Cart__c.Fields.InvoiceDate__c.Label }" />
                           <apex:inputField value="{!c.Cart.NU__InvoiceDate__c}" />
                       </apex:pageBlockSectionItem>
                       <apex:pageBlockSectionItem rendered="{! c.Cart.Order__c != null }">
                           <apex:outputText value="{! $ObjectType.Cart__c.Fields.InvoiceTerm__c.Label }" />
                           <apex:inputField value="{!c.Cart.NU__InvoiceTerm__c}" />
                       </apex:pageBlockSectionItem>
                       <apex:pageBlockSectionItem rendered="{! c.Cart.Order__c != null }">
                           <apex:outputText value="{! $ObjectType.Cart__c.Fields.InvoiceEmail__c.Label }" />
                           <apex:inputField value="{!c.Cart.NU__InvoiceEmail__c}" />
                       </apex:pageBlockSectionItem>

                </apex:pageBlockSection>
            </apex:pageBlock>
            <script type="text/javascript">
                function buildOrderDetailUI() {
                    createBatchField(document.getElementById("nu-batchList2"), getBatchField2(), "{!IF(ISBLANK(c.Cart.NU__AdjustmentEntity__c),c.Cart.NU__Entity__c,c.Cart.NU__AdjustmentEntity__c)}", undefined);
                    createTransField(getTransDateField2(), getTransDateLabel2(), document.getElementById("nu-batchList2"));
                }
                buildOrderDetailUI();
            </script>

            <div class="buttons">
                <apex:commandButton value="OK" styleClass="green" rerender="OrderPropertiesDialogPanel,OrderProperties" action="{!c.SaveProperties}" status="OD" />
                <input class="btn" type="button" value="Cancel" onclick="CustomDialog.hide('OrderDetails')" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>

    <apex:actionStatus onStart="onAjaxStart()" onStop="buildGettingStartedUI();CustomDialog.show('GettingStarted');onAjaxEnd()" id="GS"/>
    <c:CustomDialog id="GettingStarted" title="Let's Get Started" closeable="false">
        <apex:outputPanel layout="none" id="GettingStartedDialogPanel">
            <apex:pageMessages escape="{!c.EscapeMessages}" />

            <apex:pageMessage title="Error:" severity="Error" strength="2" summary="There are no active entities."
                rendered="{!c.Entities.size == 0}" />

            <apex:pageBlock mode="maindetail" id="GettingStartedForm" rendered="{!c.Entities.size != 0}">
                <apex:pageBlockSection columns="1">

                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.NU__Cart__c.Fields.NU__BillTo__c.Label}" />
                        <apex:outputPanel layout="none">
                            <c:Required ><apex:inputField value="{!c.Cart.NU__BillTo__c}" required="false" rendered="{!ISBLANK(c.Cart.NU__Order__c)}"/></c:Required>
                            <apex:outputField value="{!c.Cart.NU__BillTo__c}" rendered="{!!ISBLANK(c.Cart.NU__Order__c)}"/>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!c.Entities.size != 1}">
                        <apex:outputText value="{!IF(ISBLANK(c.Cart.NU__Order__c),'Order','Adjustment')} Entity" />
                        <apex:outputPanel layout="none">
                            <c:Required rendered="{!c.EntityOptions.size > 1}">
                                <apex:selectList value="{!c.Cart.NU__Entity__c}" rendered="{!ISBLANK(c.Cart.NU__Order__c)}" size="1" id="EntityList">
                                    <apex:selectOptions value="{!c.EntityOptions}"/>
                                </apex:selectList>
                                <apex:selectList value="{!c.Cart.NU__AdjustmentEntity__c}" rendered="{!!ISBLANK(c.Cart.NU__Order__c)}" size="1" id="AdjustmentEntityList">
                                    <apex:selectOptions value="{!c.EntityOptions}"/>
                                </apex:selectList>
                                <script type="text/javascript">
                                    function getEntityField() {
                                        return document.getElementById("{!IF(ISBLANK(c.Cart.NU__Order__c), $Component.EntityList, $Component.AdjustmentEntityList)}");
                                    }
                                </script>
                            </c:Required>
                            <apex:outputLink value="/{!c.Cart.NU__Entity__c}" rendered="{!!ISBLANK(c.Cart.NU__Order__c) && c.EntityOptions.size == 1}">{!IF(!ISBLANK(c.Order.Entity__r.NU__ShortName__c), c.Order.Entity__r.NU__ShortName__c, c.Order.Entity__r.Name)}</apex:outputLink>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.NU__Cart__c.Fields.NU__Batch__c.Label}" />
                        <c:Required >
                            <select size="1" id="nu-batchList"></select>
                            <apex:inputHidden id="Batch" value="{!c.Cart.NU__Batch__c}"/>
                            <script type="text/javascript">
                                function getBatchField() {
                                    return document.getElementById("{!$Component.Batch}");
                                }
                               function setFocusOnBatch(){
                                   document.getElementById("{!$Component.Batch}").focus();
                               }
                            </script>
                        </c:Required>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.NU__Cart__c.Fields.NU__TransactionDate__c.Label}" />
                        <apex:outputPanel layout="none">
                            <c:Required id="TransactionDateReq">
                                <apex:inputField id="TransactionDate" value="{!c.Cart.NU__TransactionDate__c}" required="false"/>
                            </c:Required>
                            <apex:outputField value="{!c.Cart.NU__TransactionDate__c}" id="TransactionDateLabel"/>
                            <script type="text/javascript">
                                function getTransDateField() {
                                    return document.getElementById("{!$Component.TransactionDateReq}");
                                }
                                function getTransDateLabel() {
                                    return document.getElementById("{!$Component.TransactionDateLabel}");
                                }
                            </script>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                </apex:pageBlockSection>
            </apex:pageBlock>

            <apex:outputPanel layout="none" rendered="{!c.ShowProductTypeList && c.Entities.size != 0}">
                <hr style="margin-bottom:20px" />
                <div id="nu-orderTypeErrorNoOptions" style="display:none">
                    <apex:pageMessage title="Error:" severity="Error" strength="2" summary="No order item types have been configured for this entity." />
                </div>
                <div id="nu-orderTypeSection">
                    <apex:pageBlock mode="maindetail">
                        <p>Which type of order do you want to start with?</p>
                        <c:Required >
                            <table id="nu-orderTypeList"></table>
                            <apex:inputHidden id="OrderType" value="{!c.GettingStartedProductType}"/>
                            <script type="text/javascript">
                                function getOrderTypeField() {
                                    return document.getElementById("{!$Component.OrderType}");
                                }
                            </script>
                        </c:Required>
                    </apex:pageBlock>
                </div>
            </apex:outputPanel>

            <div class="buttons">
                <apex:commandButton value="Continue" styleClass="green" action="{!c.GettingStartedContinue}" rerender="GettingStartedDialogPanel" status="GS" />
                <apex:commandButton value="Cancel" action="{!c.doCancel}" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>

    <c:CustomDialog id="InvoicePurchaseOrder" title="Confirm your PO / invoice information" closeable="true">
        <apex:outputPanel layout="none" id="InvoicePurchaseOrderPanel">
            <apex:pageMessages escape="{!c.EscapeMessages}" />

            <apex:pageBlock mode="maindetail">
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.NU__Cart__c.Fields.NU__Balance__c.Label}" />
                        <apex:outputField value="{!c.Cart.NU__Balance__c}" />
                    </apex:pageBlockSectionItem>
                    <apex:inputField value="{!c.Cart.NU__PurchaseOrderNumber__c}" />
                    <apex:inputField value="{!c.Cart.NU__InvoiceNumber__c}" rendered="{!$Setup.NimbleAMSSettings__c.OverridableInvoiceNumber__c}" />
                    <apex:inputField value="{!c.Cart.NU__InvoiceDescription__c}" />
                    <apex:inputField value="{!c.Cart.NU__InvoiceDate__c}" />
                    <apex:inputField value="{!c.Cart.NU__InvoiceTerm__c}" />
                    <apex:inputField value="{!c.Cart.NU__InvoiceEmail__c}" />
                </apex:pageBlockSection>
            </apex:pageBlock>

            <div class="buttons">
                <apex:commandButton value="Submit" styleClass="green" action="{!c.SaveAndSubmitCart}" rerender="InvoicePurchaseOrderPanel" status="AS" />
                <apex:commandButton value="Cancel" onclick="CustomDialog.hide('InvoicePurchaseOrder');return false" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>

    </div>

    <script type="text/javascript">
        $('html').click(function(){$("#addItemsButton").removeClass('active');});
        $("#addItemsButton,.stageAddItems>.labelCol").click(function(e){$("#addItemsButton").toggleClass('active');e.stopPropagation()});

        <apex:outputPanel rendered="{!ISBLANK(c.Cart.Id)}" layout="none">
            $(function(){CustomDialog.show('GettingStarted'); window.onload = setFocusOnBatch; buildGettingStartedUI(); });
        </apex:outputPanel>
        <apex:outputPanel rendered="{!c.ShowOrderInfo}" layout="none">
            $(function(){showOrderDetailsDialog(); });
        </apex:outputPanel>
    </script>
    <script type="text/javascript">
        function buildGettingStartedUI() {
            buildEntityDropdown();
            buildBatchDropdown();
        }

        function buildEntityDropdown() {
            var entityField = (typeof(getEntityField) != "undefined") ? getEntityField() : undefined;
            if (entityField && {!c.RememberOrderEntity}) {
                $(entityField).change(function() { nuLocalStorage.setItem("nu_new_order_entity", $(entityField).val()); });
                var lastEntity = nuLocalStorage.getItem("nu_new_order_entity");
                if (lastEntity && orderTypesByEntity[lastEntity]) {
                    $(entityField).val(lastEntity);
                }
            }
        }

        function buildBatchDropdown() {
            var entityField = (typeof(getEntityField) != "undefined") ? getEntityField() : undefined;
            var entity = entityField ? $(entityField).val() : "{!c.Cart.NU__Entity__c}";
            var batchField = getBatchField();
            // we need to watch 2 pick lists here one for the getting started dialog (nu-batchList)
            // and another for the edit dialog (nu-batchList2)
            var batchList = document.getElementById("nu-batchList");
            var batchList2 = document.getElementById("nu-batchList2");
            createBatchField(batchList, batchField, entity, entityField);
            if ({!c.RememberOrderBatch}) {
                // a change event for both picklists that sets the local storage item when either changes
                $(batchList).change(function() { nuLocalStorage.setItem("nu_new_order_batch", $(batchList).val()); });
                $(batchList2).change(function() { nuLocalStorage.setItem("nu_new_order_batch", $(batchList2).val()); });
                var lastBatch = nuLocalStorage.getItem("nu_new_order_batch");
                var batches = batchesByEntity[entity];
                if (lastBatch && batches && batches.indexOf(lastBatch) != -1 && !{!c.HasCreditCardPayment}) {
                    $(batchList).val(lastBatch).change();
                }
            }
            createTransField(getTransDateField(), getTransDateLabel(), document.getElementById("nu-batchList"));
            if (typeof(getOrderTypeField) != "undefined") {
                createOrderTypeList(document.getElementById("nu-orderTypeList"), getOrderTypeField(), entity, entityField);
            }
        }
    </script>

</apex:component>