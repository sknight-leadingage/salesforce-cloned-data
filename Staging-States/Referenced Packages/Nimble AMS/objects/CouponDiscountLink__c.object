<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Links a coupon rule to a product or entity order item. These links determine where the coupon code is able to be applied.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>CouponRule__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Coupon Rule</label>
        <referenceTo>CouponRule__c</referenceTo>
        <relationshipLabel>Coupon Discount Links</relationshipLabel>
        <relationshipName>CouponDiscountLinks</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>OrderItemConfiguration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Order Item Configuration</label>
        <referenceTo>OrderItemConfiguration__c</referenceTo>
        <relationshipLabel>Coupon Discount Links</relationshipLabel>
        <relationshipName>CouponDiscountLinks</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Coupon Discount Links</relationshipLabel>
        <relationshipName>CouponDiscountLinks</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Coupon Discount Link</label>
    <nameField>
        <displayFormat>Coupon Discount {0000000}</displayFormat>
        <label>Coupon Discount Link Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Coupon Discount Links</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>CouponRule__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CouponRule__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>CouponRule__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>BothProductAndOrderItemConfigNotAllowed</fullName>
        <active>true</active>
        <description>You cannot specify both a Product and an Order Item Configuration.</description>
        <errorConditionFormula>!ISBLANK(OrderItemConfiguration__c) &amp;&amp; !ISBLANK(Product__c)</errorConditionFormula>
        <errorMessage>You cannot specify both a Product and an Order Item Configuration.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CouponRuleEntityAndProdEntityMustMatch</fullName>
        <active>true</active>
        <description>The coupon rule&apos;s entity must match the product&apos;s entity.</description>
        <errorConditionFormula>!ISBLANK(Product__c) &amp;&amp; (CouponRule__r.CouponProduct__r.Entity__c &lt;&gt; Product__r.Entity__c)</errorConditionFormula>
        <errorMessage>The coupon rule&apos;s entity must match the product&apos;s entity.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ProductOrEntityOrderItemConfigRequired</fullName>
        <active>true</active>
        <description>Either a Product or an Order Item Configuration must be specified.</description>
        <errorConditionFormula>ISBLANK(Product__c) &amp;&amp; ISBLANK(OrderItemConfiguration__c)</errorConditionFormula>
        <errorMessage>Either a Product or an Order Item Configuration must be specified.</errorMessage>
    </validationRules>
</CustomObject>
