<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpEvents</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Picklist question options</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>DisplayOrder__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Display Order</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EventQuestion__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Event Question</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR 2</booleanFilter>
            <errorMessage>Event Question Options can only be attached to picklist questions.</errorMessage>
            <filterItems>
                <field>EventQuestion__c.Type__c</field>
                <operation>equals</operation>
                <value>Picklist</value>
            </filterItems>
            <filterItems>
                <field>EventQuestion__c.Type__c</field>
                <operation>equals</operation>
                <value>Picklist (Multi-Select)</value>
            </filterItems>
            <infoMessage>Event Question Options can only be attached to picklist questions.</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>EventQuestion__c</referenceTo>
        <relationshipLabel>Event Question Options</relationshipLabel>
        <relationshipName>EventQuestionOptions</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>OptionText__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Option Text</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <label>Event Question Option</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_Event_Question_Options</fullName>
        <columns>NAME</columns>
        <columns>EventQuestion__c</columns>
        <columns>OptionText__c</columns>
        <columns>DisplayOrder__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Event Question Options</label>
    </listViews>
    <nameField>
        <displayFormat>EventQuestionOption {0000000}</displayFormat>
        <label>Event Question Option Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Event Question Options</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>EventQuestion__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OptionText__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DisplayOrder__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>OptionText__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EventQuestion__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DisplayOrder__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>OptionText__c</searchFilterFields>
        <searchResultsAdditionalFields>OptionText__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EventQuestion__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DisplayOrder__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
