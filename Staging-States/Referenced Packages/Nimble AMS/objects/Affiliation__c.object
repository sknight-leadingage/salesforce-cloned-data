<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpAffiliations</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An Affiliation links individuals and organizations. It defines relationships between companies and employees, institutions and volunteers, and so forth.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fieldSets>
        <fullName>AffiliationPrintRosterColumns</fullName>
        <description>Affiliation Roster Page</description>
        <displayedFields>
            <field>Role__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>IsPrimary__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.NumberOfEmployees</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.JoinOn__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.StatusMembershipFlag__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.BillingCity</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.BillingState</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Affiliation Print Roster Columns</label>
    </fieldSets>
    <fieldSets>
        <fullName>AffiliationViewRosterColumns</fullName>
        <description>Affiliation Roster Page</description>
        <displayedFields>
            <field>Role__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>IsPrimary__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.NumberOfEmployees</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.JoinOn__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.StatusMembershipFlag__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.BillingCity</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Account__r.BillingState</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Affiliation View Roster Columns</label>
    </fieldSets>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Parent Affiliations</relationshipLabel>
        <relationshipName>Affiliations</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DoNotFlowdownAddress__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>When checked, no addresses are flowed down from the Primary Parent Account to the Child Account.</description>
        <externalId>false</externalId>
        <inlineHelpText>When checked, no addresses are flowed down from the Primary Parent Account to the Child Account.</inlineHelpText>
        <label>Do Not Flowdown Address</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EndDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IsCompanyManager__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If checked and Affiliation is marked as Primary, Account can manage the company using the self service site.</inlineHelpText>
        <label>Is Company Manager</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsPrimaryContact__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Denotes the child is the primary contact for the parent account.</description>
        <externalId>false</externalId>
        <inlineHelpText>Denotes the child is the primary contact for the parent account.</inlineHelpText>
        <label>Primary Contact</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsPrimary__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If checked, flowdown rules, company management can be activated. If checked, this affiliation also appears in the Affiliations tree.</inlineHelpText>
        <label>Primary Affiliation</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ParentAccount__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Parent Organization</description>
        <externalId>false</externalId>
        <inlineHelpText>Parent Organization</inlineHelpText>
        <label>Parent Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Child Affiliations</relationshipLabel>
        <relationshipName>Affiliates</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RemovalDate2__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Removal Date</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RemovalDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>zRemoval Date (DEPRECATED)</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>RemovalReason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reason why affiliation ended</inlineHelpText>
        <label>Removal Reason</label>
        <picklist>
            <picklistValues>
                <fullName>Deceased</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Incorrect Affiliation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No longer with the company</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Retired</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Role__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Role</label>
        <picklist>
            <picklistValues>
                <fullName>Advertising Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST CH Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST Federal Govt Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST General Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST Govt Affairs Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST PR Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST Primary Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST State Govt Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CFAR Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Company</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Employee</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Exhibitor Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Fundraising Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hospice</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IAHSA Primary Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Invoice Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LeadingAge Primary Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resident</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>State Primary Contact</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Trustee</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Search__c</fullName>
        <deprecated>false</deprecated>
        <description>Contains related object value(s) so that these records show up in a global search. This field should not be shown on page layouts.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contains related object value(s) so that these records show up in a global search. This field should not be shown on page layouts.</inlineHelpText>
        <label>Search</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>StartDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StatusFlag__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IMAGE(&apos;/resource/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusIcons/&apos; + IF(TEXT(Status__c) = &apos;Inactive&apos;, &apos;Inactive.png&apos;, &apos;Active.png&apos;), TEXT(Status__c))</formula>
        <label>Status Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Affiliation</label>
    <listViews>
        <fullName>NU_AllAffiliations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>ParentAccount__c</columns>
        <columns>IsPrimary__c</columns>
        <columns>Role__c</columns>
        <columns>IsCompanyManager__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Affiliations</label>
    </listViews>
    <listViews>
        <fullName>NU_AllPrimaryAffiliations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>ParentAccount__c</columns>
        <columns>IsPrimary__c</columns>
        <columns>Role__c</columns>
        <columns>IsCompanyManager__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>IsPrimary__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>All Primary Affiliations</label>
    </listViews>
    <listViews>
        <fullName>NU_LastWeeksAffiliations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>ParentAccount__c</columns>
        <columns>IsPrimary__c</columns>
        <columns>Role__c</columns>
        <columns>IsCompanyManager__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>LAST_WEEK</value>
        </filters>
        <label>Last Week&apos;s Affiliations</label>
    </listViews>
    <listViews>
        <fullName>NU_SelfServiceRosterRemovals</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>ParentAccount__c</columns>
        <columns>IsPrimary__c</columns>
        <columns>Role__c</columns>
        <columns>IsCompanyManager__c</columns>
        <columns>RemovalDate__c</columns>
        <columns>RemovalReason__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RemovalDate__c</field>
            <operation>notEqual</operation>
        </filters>
        <label>Self Service Roster Removals</label>
        <sharedTo>
            <role>National</role>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>NU_ThisWeeksAffiliations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>ParentAccount__c</columns>
        <columns>IsPrimary__c</columns>
        <columns>Role__c</columns>
        <columns>IsCompanyManager__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>THIS_WEEK</value>
        </filters>
        <label>This Week&apos;s Affiliations</label>
    </listViews>
    <listViews>
        <fullName>NU_TodaysAffiliations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>ParentAccount__c</columns>
        <columns>IsPrimary__c</columns>
        <columns>Role__c</columns>
        <columns>IsCompanyManager__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>Today&apos;s Affiliations</label>
    </listViews>
    <nameField>
        <displayFormat>Affiliation {0000000}</displayFormat>
        <label>Affiliation Id</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Affiliations</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ParentAccount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>IsPrimary__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Role__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>IsCompanyManager__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StatusFlag__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ParentAccount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>IsPrimary__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Role__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>IsCompanyManager__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StatusFlag__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ParentAccount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>IsPrimary__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Role__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>IsCompanyManager__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StatusFlag__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>ParentAccount__c</searchFilterFields>
        <searchFilterFields>IsPrimary__c</searchFilterFields>
        <searchFilterFields>Role__c</searchFilterFields>
        <searchFilterFields>IsCompanyManager__c</searchFilterFields>
        <searchFilterFields>RemovalReason__c</searchFilterFields>
        <searchFilterFields>StatusFlag__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ParentAccount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>IsPrimary__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Role__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>IsCompanyManager__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StatusFlag__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>Account_Required</fullName>
        <active>true</active>
        <description>The Individual field is required to establish a correct affiliation.</description>
        <errorConditionFormula>ISBLANK( Account__c )</errorConditionFormula>
        <errorMessage>The Account is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ParentAccount_Required</fullName>
        <active>true</active>
        <description>The Organization field is required to establish a correct affiliation.</description>
        <errorConditionFormula>ISBLANK(ParentAccount__c)</errorConditionFormula>
        <errorMessage>The Parent Account is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>StartDateBeforeEndDate</fullName>
        <active>true</active>
        <description>The Start Date must be before the End Date.</description>
        <errorConditionFormula>!ISBLANK(StartDate__c) &amp;&amp; !ISBLANK(EndDate__c) &amp;&amp; StartDate__c &gt; EndDate__c</errorConditionFormula>
        <errorMessage>The Start Date must be before the End Date.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>PrintRoster</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Print Roster</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>window.open(&quot;/apex/{!$Setup.Namespace__c.Prefix__c}AffiliationRoster?id={!Account.Id}&amp;print=1&quot;, &quot;_blank&quot;, &quot;height=600,location=no,resizable=yes,toolbar=no,status=no,menubar=no,scrollbars=1&quot;, false)</url>
    </webLinks>
    <webLinks>
        <fullName>ViewRoster</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>View Roster</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>window.open(&quot;/apex/{!$Setup.Namespace__c.Prefix__c}AffiliationRoster?id={!Account.Id}&quot;, &quot;_blank&quot;, &quot;height=600,location=no,resizable=yes,toolbar=no,status=no,menubar=no,scrollbars=1&quot;, false)</url>
    </webLinks>
</CustomObject>
