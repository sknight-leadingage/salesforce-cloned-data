<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BatchPostedOnPopulation</fullName>
        <field>PostedOn__c</field>
        <formula>Today()</formula>
        <name>Batch Posted On Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Batch Posted On Population</fullName>
        <actions>
            <name>BatchPostedOnPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Batch__c.Status__c</field>
            <operation>equals</operation>
            <value>Posted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
