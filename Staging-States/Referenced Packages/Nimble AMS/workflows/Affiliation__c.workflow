<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AffiliationSearchAccountPopulation</fullName>
        <field>Search__c</field>
        <formula>IF(ISBLANK( Account__r.Name )=False,
Account__r.Name,
Account__r.PersonContact__r.FirstName &amp; &apos; &apos; &amp;
Account__r.PersonContact__r.LastName )</formula>
        <name>Affiliation Search Account Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Affiliation Search Account Population</fullName>
        <actions>
            <name>AffiliationSearchAccountPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Affiliation__c.Search__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
