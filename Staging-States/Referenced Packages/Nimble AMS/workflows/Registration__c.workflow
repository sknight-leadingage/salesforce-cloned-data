<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>RegistrantEmailPopulation</fullName>
        <field>RegistrantEmail__c</field>
        <formula>Account__r.PersonEmail__c</formula>
        <name>Registrant Email Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RegistrationSearchAccountPopulation</fullName>
        <field>Search__c</field>
        <formula>IF(ISBLANK( Account__r.Name )=False,
        Account__r.Name,
           Account__r.PersonContact__r.FirstName &amp; &apos; &apos; &amp;
            Account__r.PersonContact__r.LastName )</formula>
        <name>Registration Search Account Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Registrant Email Population</fullName>
        <actions>
            <name>RegistrantEmailPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED</description>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Registration Search Account Population</fullName>
        <actions>
            <name>RegistrationSearchAccountPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED</description>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
