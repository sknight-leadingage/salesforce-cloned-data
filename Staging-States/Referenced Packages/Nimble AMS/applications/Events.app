<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Event__c</defaultLandingTab>
    <description>Manage all aspects of your events and registrations</description>
    <label>Events</label>
    <logo>NimbleAMS/LeadingAge_Logo.gif</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>Event__c</tab>
    <tab>Registration2__c</tab>
    <tab>Order__c</tab>
    <tab>Product__c</tab>
    <tab>EventBadge__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Idea</tab>
    <tab>Exhibitor__c</tab>
    <tab>Sponsorship__c</tab>
    <tab>Awards__c</tab>
</CustomApplication>
