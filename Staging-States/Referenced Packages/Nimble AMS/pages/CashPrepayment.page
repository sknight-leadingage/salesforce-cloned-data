<apex:page controller="NU.CashPrepaymentController" title="Cash Prepayment">
    <apex:pageMessage summary="Cash Prepayment is not enabled. Please contact Nimble AMS Support by submitting a support case through http://www.nimbleuser.com/support/, emailing us at support@nimbleuser.com, or calling us at (585) 586-4750 if you would like to setup this functionality."
                      severity="info"
                      strength="2"
                      rendered="{! IsPrepaymentAccepted == false }" />

    <apex:form rendered="{! IsPrepaymentAccepted }" id="CashPrepaymentForm">

        <apex:pageMessages escape="false" />

        <apex:pageBlock title="Cash Prepayment">
            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputText value="" />

                    <apex:selectRadio value="{! Mode }">
                        <apex:selectOption itemLabel="Prepay" itemValue="PREPAY" />
                        <apex:selectOption itemLabel="Refund" itemValue="REFUND" />

                        <apex:actionSupport event="onchange"
                                            action="{!ModeChanged}"
                                            rerender="CashPrepaymentForm" />
                    </apex:selectRadio>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{! IsPrepayMode }">
                     <apex:outputText value="Payment Method" />
                     <c:Required >
                     <apex:selectList value="{! PrepaymentRequest.Payment.EntityPaymentMethod__c }"
                                      multiselect="false"
                                      size="1">
                         <apex:selectOption itemLabel="Please select a Payment Method" itemValue="" />
                         <apex:selectOptions value="{!PaymentMethodSelectOpts}" />
                         <apex:actionSupport event="onchange"
                                             action="{!PaymentMethodChanged}"
                                             rerender="CashPrepaymentForm" />
                     </apex:selectList>

                     </c:Required>
                 </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{! PrepaymentRequest.Payment.EntityPaymentMethod__c != null || IsRefundMode }">
                    <apex:outputText value="{! $ObjectType.Payment__c.Fields.Payer__c.Label }" />

                    <c:Required >
                    <apex:inputField value="{! PrepaymentRequest.Payment.Payer__c }">
                        <apex:actionSupport event="onchange"
                                            action="{!PayerChanged}"
                                            rerender="CashPrepaymentForm" />
                    </apex:inputField>
                    </c:Required>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{! IsRefundMode && AvailableRefundPaymentsCount > 0 }">
                     <apex:outputText value="Available Payments For Refund" />

                     <c:Required >
                     <apex:selectList value="{! PrepaymentRequest.Payment.Id }" multiselect="false" size="1">
                         <apex:selectOptions value="{! AvailableCreditPaymentSelectOpts }" />
                     </apex:selectList>
                     </c:Required>
                 </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{! PrepaymentRequest.Payment.EntityPaymentMethod__c != null || (IsRefundMode && AvailableRefundPaymentsCount > 0) }">
                     <apex:outputText value="Batch" />

                     <c:Required >
                     <apex:selectList value="{! PrepaymentRequest.BatchId }" multiselect="false" size="1">
                         <apex:selectOption itemLabel="Automatic" itemValue="" />
                         <apex:selectOptions value="{!ManualBatchesSelectOpts}" />
                         <apex:actionSupport event="onchange"
                                             action="{!BatchChanged}"
                                             rerender="CashPrepaymentForm" />
                     </apex:selectList>
                     </c:Required>
                 </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{! (PrepaymentRequest.Payment.EntityPaymentMethod__c != null || (IsRefundMode && AvailableRefundPaymentsCount > 0)) && PrepaymentRequest.BatchId == null }">
                    <apex:outputText value="{! $ObjectType.Payment__c.Fields.PaymentDate__c.Label }" />

                    <c:Required >
                    <apex:inputField value="{! PrepaymentRequest.Payment.PaymentDate__c }" required="false" />
                    </c:Required>
                </apex:pageBlockSectionItem>

                <apex:outputField value="{! PrepaymentRequest.Payment.PaymentDate__c }"
                                  rendered="{! (PrepaymentRequest.Payment.EntityPaymentMethod__c != null || (IsRefundMode && AvailableRefundPaymentsCount > 0)) && PrepaymentRequest.BatchId != null }" />

                <apex:pageBlockSectionItem rendered="{! PrepaymentRequest.Payment.EntityPaymentMethod__c != null || (IsRefundMode && AvailableRefundPaymentsCount > 0) }">
                    <apex:outputText value="{!IF(IsPrepayMode,'Payment Amount','Refund Amount') }" />

                    <c:Required >
                    <apex:inputField value="{! PrepaymentRequest.Payment.PaymentAmount__c }" />
                    </c:Required>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem rendered="{! PrepaymentRequest.Payment.EntityPaymentMethod__c != null && SelectedEntityPaymentMethod.PaymentMethod__r.RecordType.Name == 'Check'}">
                    <apex:outputText value="{! $ObjectType.Payment__c.Fields.CheckNumber__c.Label }" />

                    <c:Required >
                    <apex:inputField value="{! PrepaymentRequest.Payment.CheckNumber__c }" />
                    </c:Required>
                </apex:pageBlockSectionItem>

                <apex:inputField value="{! PrepaymentRequest.Payment.Note__c }"
                                 rendered="{! PrepaymentRequest.Payment.EntityPaymentMethod__c != null }" />
            </apex:pageBlockSection>

            <apex:pageBlockButtons >
                <apex:commandButton value="Submit" action="{!SubmitPayment}" />
            </apex:pageBlockButtons>
        </apex:pageBlock>
    </apex:form>
</apex:page>