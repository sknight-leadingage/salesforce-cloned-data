<apex:page standardController="Account" extensions="NU.AffiliationRosterController" tabStyle="Account" showHeader="false" sidebar="false">
    <head>
        <title>Affiliation Roster</title>
    </head>
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" />
    <div id="print">
        <input type="button" class="btn" onclick="loadAll(print)" value="Print"/>
    </div>
    <div class="message errorM3" id="nu-error" style="display:none">
        <table border="0" cellpadding="0" cellspacing="0" class="messageTable" style="padding:0px;margin:0px;">
            <tr valign="top">
                <td><img alt="ERROR" class="msgIcon" src="/s.gif" title="ERROR"/></td>
                <td class="messageCell"><div class="messageText"><span style="color:#cc0000">
                    <h4>Error:</h4></span><span id="nu-errormsg"></span><br/></div>
                </td>
            </tr>
            <tr><td></td><td></td></tr>
        </table>
    </div>
    <div style="margin:10px">
        <apex:outputPanel rendered="{!!ISBLANK(Entity)}" layout="none">
            <apex:outputPanel rendered="{!!ISBLANK(Entity.NU__LogoURL__c)}" layout="none">
                <apex:image value="{!Entity.NU__LogoURL__c}" style="display:inline;vertical-align:middle;float:left;padding-right:30px" />
            </apex:outputPanel>
            <div style="float:left">
                <c:Address name="{!Entity.Name}"
                        street="{!Entity.NU__Street__c}"
                        city="{!Entity.NU__City__c}"
                        state="{!Entity.NU__State__c}"
                        postalcode="{!Entity.NU__PostalCode__c}"
                        country="{!Entity.NU__Country__c}" />
                <apex:outputPanel rendered="{!!ISBLANK(Entity.NU__Phone__c)}">
                    {!Entity.NU__Phone__c}<br/>
                </apex:outputPanel>
                <apex:outputPanel rendered="{!!ISBLANK(Entity.NU__Website__c)}">
                    {!Entity.NU__Website__c}<br/>
                </apex:outputPanel>
             </div>
        </apex:outputPanel>
        <div style="float:right;text-align:right">
            <div><h1 style="font-size:2em">{!Root.Name} Roster</h1></div>
            <apex:outputText value="{0,date,'as of 'MM'/'dd'/'yyyy}">
                <apex:param value="{!TODAY()}"/>
            </apex:outputText>
        </div>
        <div style="clear:both"></div>
    </div>
    <apex:pageBlock mode="maindetail">
        <apex:pageBlockTable value="{!EmptyAccountList}" var="a">
            <apex:column headerValue="{!$ObjectType.Account.fields.Name.label}"/>
            <apex:repeat value="{!Describe}" var="f">
                <apex:column headerValue="{!f.Label}"/>
            </apex:repeat>
            <apex:column headerClass="actionColumn" />
        </apex:pageBlockTable>
    </apex:pageBlock>
    <style type="text/css">
        #print {
            margin: 10px;
        }
        .nu-acctType {
            margin-right: 4px;
            display: inline-block;
            vertical-align: top;
            width: 15px;
            height: 15px;
        }
        .spacer {
            display: inline-block;
            white-space: nowrap;
        }
        .nu-toggle {
            margin-right: 4px;
            display: inline-block;
            vertical-align: top;
            width: 15px;
            height: 15px;
            -webkit-user-select: none;
        }
        .nu-individual .nu-acctType {
            background-image: url('{!URLFOR($Resource.NU__Icons, "user.png")}');
        }
        .nu-company .nu-acctType {
            background-image: url('{!URLFOR($Resource.NU__Icons, "building.png")}');
        }
        .nu-expand .nu-toggle {
            cursor: pointer;
            background-image: url('{!URLFOR($Resource.NU__Icons, "expand.png")}');
        }
        .nu-collapse .nu-toggle {
            cursor: pointer;
            background-image: url('{!URLFOR($Resource.NU__Icons, "collapse.png")}');
        }
        .nu-loading .nu-toggle {
            background-image: url('{!URLFOR($Resource.NU__Icons, "loading16.gif")}');
        }
        #nu-r{!Account.Id} td {
            background-color: #FF9;
        }
        #nu-r{!Account.Id}.highlight td {
            background-color: #BFB;
        }
        @media print {
            #print {
                display: none;
            }
            .actionColumn {
                display: none;
            }
            .nu-acctType {
                display: none;
            }
            .nu-toggle {
                display: none;
            }
            table {
                page-break-inside: auto;
            }
               tr {
                   page-break-inside: avoid;
                   page-break-after: auto;
               }
        }
    </style>
    <script type="text/javascript">
        var controller = {!Namespace}AffiliationRosterController;
        var isPrint = {!IsPrint};

        function rowEnter() {
            if (window.hiOn) { window.hiOn(this); }
        }

        function rowExit() {
            if (window.hiOff) { window.hiOff(this); }
        }

        function error(message) {
            $("#nu-errormsg").text(message);
            $("#nu-error").show();
        }

        TreeNode = (function() {

            var metadata = [
                <apex:repeat value="{!Describe}" var="f">
                    {"Name": "{!f.Name}", "Type": "{!f.Type}", "Scale": {!f.Scale}, "Html": {!f.Html}},
                </apex:repeat>
            ];

            function TreeNode(acc, aff, parent) {
                this._depth = (parent && parent._depth + 1) || 0;
                this._acc = acc;
                this._aff = aff;
                this._parent = parent;
                this._element = undefined;
                this._children = undefined;
            }

            var rgx = /(\d+)(\d{3})/;
            function addCommas(str) {
                var split = str.split(".");
                var s1 = split[0];
                var s2 = split.length > 1 ? "." + split[1] : "";
                while (rgx.test(s1)) {
                    s1 = s1.replace(rgx, "$1,$2");
                }
                return s1 + s2;
            }

            function fd(data, meta, cell) {
                if (data === undefined) {
                    return cell;
                }
                if (meta.Html) {
                    data = unescapeXML(data);
                }
                switch (meta.Type) {
                    case "BOOLEAN":
                        var chkstr = data ? "Checked" : "Unchecked";
                        return cell.append($("<img>", {
                                "src": "/img/checkbox_" + chkstr.toLowerCase() + ".gif",
                                "alt": chkstr,
                                "width": 21,
                                "height": 16,
                                "class": "checkImg",
                                "title": chkstr
                            }));
                    case "CURRENCY":
                        var temp = "$" + addCommas(Math.abs(data).toFixed(meta.Scale));
                        if (data < 0) {
                            temp = "(" + temp + ")";
                        }
                        return cell.text(temp);
                    case "DATE":
                        // apply offset based on the user's timezone or the date will be off by one day
                        var temp = new Date(data);
                        temp = new Date(temp.getTime() + 60000 * temp.getTimezoneOffset());
                        return cell.text(DateUtil.getDateStringFromUserLocale(temp));
                    case "DATETIME":
                        return cell.text(DateUtil.getDateTimeStringFromUserLocale(new Date(data)));
                    case "DOUBLE":
                        return cell.text(addCommas(data.toFixed(meta.Scale)));
                    case "EMAIL":
                        return cell.append($("<a>", {html: data, href: "mailto:" + data }));
                    case "INTEGER":
                        return cell.text(addCommas(data.toString()));
                    case "MULTIPICKLIST":
                        return cell.text(data.replace(/;/g, "; "));
                    case "PERCENT":
                        return cell.text(addCommas(data.toFixed(meta.Scale)) + "%");
                    case "REFERENCE":
                        return cell.append($("<a>", {html: data.Name, href: "/" + data.Id}));
                    case "TEXTAREA":
                        return cell.html(data.replace(/\r\n{0,1}/g, "<br/>"));
                    case "URL":
                        return cell.append($("<a>", {html: data, href:data}));
                }
                return cell.html(data);
            }

            function remove(self, link) {
                $(link).replaceWith($("<img>", {src: "{!URLFOR($Resource.Icons, 'loading16.gif')}", style: "vertical-align:middle;margin-top:-1px"}));
                controller.removeAffiliation(self._aff.Id, function(r, e) {
                        if (e.status) {
                            self._element.remove();
                            self._parent._children.remove(self);
                            if (self._parent._children.length === 0) {
                                self._parent._element.removeClass("nu-expand").removeClass("nu-collapse");
                            }
                        } else {
                            error(e.message);
                        }
                    });
            }

            function evaluate(val, prop) {
                var split = prop.split(".");
                for (var i = 0; i < split.length; i++) {
                    val = val[split[i]];
                    if (!val) { break; }
                }
                return val;
            }

            TreeNode.prototype.element = function() {
                if (this._element === undefined) {
                    var self = this;
                    var c1 = this._acc.IsPersonAccount ? "nu-individual" : "nu-company";
                    var c2 = (this._acc.{!Prefix}Affiliates__r && this._acc.{!Prefix}Affiliates__r.length) ? " nu-expand": "";
                    var name = this._acc.IsPersonAccount ? (this._acc.LastName + (this._acc.FirstName ? ', ' + this._acc.FirstName : '')) : this._acc.Name;
                    this._element = $("<tr>", {"class": "dataRow " + c1 + c2, "id": "nu-r" + this._acc.Id})
                        .append($("<td>", {"class": "dataCell"})
                            .append($("<span>").attr("class", "spacer").attr("style", "margin-left:" + this._depth * 16 + "px")
                                .append($("<span>").attr("class", "nu-toggle").click(function() { self.toggle(); }))
                                .append($("<span>", {"class": "nu-acctType"}))
                                .append($("<a>", {html: name, href: "/" + this._acc.Id, id: "nu-l" + this._acc.Id })
                                    .mouseover(function() { LookupHoverDetail.getHover('nu-l' + self._acc.Id, '/' + self._acc.Id + '/m?isAjaxRequest=1').show(); })
                                    .mouseout(function() { LookupHoverDetail.getHover('nu-l' + self._acc.Id).hide(); }))))
                        .mouseover(rowEnter)
                        .mouseout(rowExit);

                    <!--don't die from trailing comma in old IE-->
                    for (var i = 0; metadata[i]; i++) {
                        this._element.append(fd(evaluate(this._aff, metadata[i].Name), metadata[i], $("<td>").attr("class", "dataCell")));
                    }

                    if (this._parent) {
                        this._element
                            .append($("<td>").attr("class", "actionColumn dataCell")
                                .append($("<a>", {"class": "actionLink", "text": "Edit", "href": "/" + this._aff.Id + "/e"}))
                                .append("&nbsp;|&nbsp;")
                                .append($("<a>", {"class": "actionLink", "text": "Remove", "href": "javascript:void(0)"}).click(function () { remove(self, this); })));
                    } else {
                        this._element.append("<td>");
                    }
                }
                return this._element;
            };

            var collapseHelper = function collapseHelper(nodes) {
                for (var i = nodes.length - 1; i >= 0; i--) {
                    var node = nodes[i];
                    node._element.hide();
                    if (node._children) {
                        collapseHelper(node._children);
                    }
                }
            };

            var expandHelper = function expandHelper(nodes) {
                for (var i = nodes.length - 1; i >= 0; i--) {
                    var node = nodes[i];
                    node._element.show();
                    if (node._children && node._element.hasClass("nu-collapse")) {
                        expandHelper(node._children);
                    }
                }
            };

            TreeNode.prototype.toggle = function() {
                if (this._element.hasClass("nu-expand")) {
                    this.expand();
                } else if (this._element.hasClass("nu-collapse")) {
                    this.collapse();
                }
            };

            TreeNode.prototype.expand = function() {
                if (this._children === undefined) {
                    var self = this;
                    this._element.removeClass("nu-expand").addClass("nu-loading");
                    controller.loadChildren(this._acc.Id, isPrint, function(r, e) {
                        if (e.status) {
                            var temp = $();
                            self._children = [];
                            for (var i = 0, len = r.length; i < len; i++) {
                                var acc = r[i];
                                for (var j = 0, len2 = acc.{!Prefix}Affiliations__r.length; j < len2; j++) {
                                    var node = new TreeNode(acc, acc.{!Prefix}Affiliations__r[j], self);
                                    self._children.push(node);
                                    temp = temp.add(node.element());
                                }
                            }
                            self.element().after(temp);
                            self._element.removeClass("nu-loading").addClass("nu-collapse");
                        } else {
                            error(e.message);
                            self._element.removeClass('nu-loading').addClass('nu-expand');
                        }
                    });
                } else {
                    expandHelper(this._children);
                    this._element.removeClass("nu-expand").addClass("nu-collapse");
                }
            };

            TreeNode.prototype.collapse = function() {
                collapseHelper(this._children);
                this._element.removeClass("nu-collapse").addClass("nu-expand");
            };

            return TreeNode;
        })();

        function flatten(byParent, parentNode, out) {
            out.push(parentNode.element()[0]);
            var temp = byParent[parentNode._acc.Id];
            if (temp) {
                parentNode._children = [];
                parentNode._element.removeClass("nu-expand").addClass("nu-collapse")
                for (var i = 0, len = temp.length; i < len; i++) {
                    var node = new TreeNode(temp[i][0], temp[i][1], parentNode);
                    parentNode._children.push(node);
                    flatten(byParent, node, out);
                }
            }
        }

        <!--ignores first element (root)-->
        function mapByParent(accounts) {
            var result = {};
            for (var i = 1, len = accounts.length; i < len; i++) {
                var acc = accounts[i];
                for (var j = 0, len2 = acc.{!Prefix}Affiliations__r.length; j < len2; j++) {
                    var aff = acc.{!Prefix}Affiliations__r[j];
                    var parent = aff.{!Prefix}ParentAccount__c;
                    (result[parent] || (result[parent] = [])).push([acc, aff]);
                }
            }
            return result;
        }

        function load() {
            list.empty().append("<tr class='dataRow nu-loading'><td class='dataCell'><span class='nu-toggle'/>Loading</td></tr>");
            controller.loadAccounts(root, ids, isPrint, function (r, e) {
                if (e.status) {
                    var byParent = mapByParent(r);
                    var root = new TreeNode(r[0], {{!Prefix}Account__r: r[0]});
                    var elements = [];
                    flatten(byParent, root, elements);
                    list.empty().append(elements);
                    $('html, body').animate({scrollTop: $("#nu-r{!Account.Id}").offset().top - 200}, 500);
                } else {
                    error(e.message);
                }
            });
        }

        function loadAll(callback) {
            list.empty().append("<tr class='dataRow nu-loading'><td class='dataCell'><span class='nu-toggle'/>Loading</td></tr>");
            controller.loadAllAccounts(root, isPrint, function (r, e) {
                if (e.status) {
                    var byParent = mapByParent(r);
                    var root = new TreeNode(r[0], {{!Prefix}Account__r: r[0]});
                    var elements = [];
                    flatten(byParent, root, elements);
                    list.empty().append(elements);
                    if (callback) { callback(); }
                } else {
                    error(e.message);
                }
            });
        }

        var root = "{!Root.Id}";
        var ids = [
            <apex:repeat value="{!AccountIds}" var="a">
                "{!a}",
            </apex:repeat>
        ];
        if (ids[ids.length - 1] == undefined) { ids.pop(); }
        var list = $(".list tbody");
        if (isPrint) {
            loadAll(print);
        } else {
            load();
        }
    </script>
</apex:page>