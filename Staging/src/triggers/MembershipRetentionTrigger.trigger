trigger MembershipRetentionTrigger on NU__Membership__c (after insert, after update) {
    List<NU__Membership__c> membershipsToCalculate = new List<NU__Membership__c>();
    
    List<NU__Membership__c> memberships = [SELECT Id, NU__Account__c, NU__MembershipType__c, NU__StartDate__c, RetentionStatus__c 
        FROM NU__Membership__c
        WHERE Id IN :Trigger.new];
        
    if (Trigger.isUpdate) {
        // in case the trigger is firing because of an update of the retention status - we only care about updates where the start date is changing
        for (NU__Membership__c membership : memberships) {
            NU__Membership__c oldMembership = Trigger.oldMap.get(membership.Id);
            
            if (oldMembership.NU__StartDate__c != membership.NU__StartDate__c) {
                membershipsToCalculate.add(membership);
            }
        }
    }
    
    if (Trigger.isInsert) {
        membershipsToCalculate.addAll(memberships);
    }
    
    if (membershipsToCalculate != null && membershipsToCalculate.size() > 0) {
        MembershipRetention.Calculate(membershipsToCalculate);
    }
}