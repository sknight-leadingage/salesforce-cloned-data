trigger UpdateMembershipBillingLinesTrigger on NU__OrderItemLine__c (after insert, after update) {
	List<NU__OrderItemLine__c> idsToQuery = new List<NU__OrderItemLine__c>();
	if (Trigger.isInsert) {
		idsToQuery = Trigger.new;
	}
	else if (Trigger.isUpdate) {
		for (NU__OrderItemLine__c newOIL : Trigger.new) {
			NU__OrderItemLine__c oldOIL = Trigger.oldMap.get(newOIL.Id);
			
			// only run when the lookups, price, or status is changing
			if (oldOIL.NU__Membership__c != newOIL.NU__Membership__c ||
				oldOIL.NU__Donation__c != newOIL.NU__Donation__c ||
				oldOIL.NU__Status__c != newOIL.NU__Status__c ||
				oldOIL.NU__TotalPrice__c != newOIL.NU__TotalPrice__c) {
				
				idsToQuery.add(newOIL);
			}
		}
	}	
			
	// get all order item lines of memberships and donations attached to memberships, as well as all necessary info (dues pricing, membership type, year)
	List<NU__OrderItemLine__c> orderItemLines = [SELECT Id,
		NU__Membership__c,
		NU__Membership__r.NU__MembershipType__c,
		NU__Membership__r.NU__Account__c,
		NU__Membership__r.NU__StartDate__c,
		NU__Membership__r.NU__OrderItemLine__c,
		NU__MembershipTypeProductLink__c,
		NU__MembershipTypeProductLink__r.NU__Purpose__c,
		NU__Donation__c,
		NU__Donation__r.NU__Membership__c,
		NU__Donation__r.NU__Membership__r.NU__MembershipType__c,
		NU__Donation__r.NU__Membership__r.NU__Account__c,
		NU__Donation__r.NU__Membership__r.NU__StartDate__c,
		NU__Status__c,
		NU__TotalPrice__c
		FROM NU__OrderItemLine__c
		WHERE Id IN :idsToQuery
			AND 
				((NU__Membership__c != NULL AND NU__MembershipTypeProductLink__c != NULL 
					AND NU__MembershipTypeProductLink__r.NU__Purpose__c = :Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY)
				OR
				(NU__Donation__c != NULL
					AND NU__Donation__r.NU__Membership__c != NULL))];
	
	if (orderItemLines.size() > 0) {
		MembershipBilling.updateMembershipBillingLinesWithOILs(orderItemLines);
	}
}