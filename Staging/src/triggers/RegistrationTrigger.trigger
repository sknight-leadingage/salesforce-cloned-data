trigger RegistrationTrigger on NU__Registration2__c (after update) {
	NU.TriggerHandlerManager.executeHandlers('RegistrationTrigger', new RegistrationTriggerHandler());
}