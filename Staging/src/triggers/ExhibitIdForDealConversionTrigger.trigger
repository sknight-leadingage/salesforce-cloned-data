trigger ExhibitIdForDealConversionTrigger on Exhibitor__c (after insert) {
	List<Exhibitor__c> exhibits = Trigger.new;

    for (Exhibitor__c exhibit : exhibits) {
        ConvertDealCartToOrder.getInstance().setExhibit(exhibit.Id);
    }
}