// trigger for the Website Action object
// redirects to the Website Action Trigger handler
// used by the public Member Directory
// Developed by NimbleUser 2014 (NF), LA-132

trigger WebsiteActionTrigger on Website_Action__c (before insert, before update) {
    NU.TriggerHandlerManager.executeHandlers('WebsiteActionTrigger', new WebsiteActionTriggerHandler());
}