trigger CartItemLineTrigger on NU__CartItemLine__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    NU.TriggerHandlerManager.executeHandlers('CartItemLineTrigger', new CartItemLineTriggerHandlers());
}