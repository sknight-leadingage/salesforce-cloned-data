trigger CorporateAlliancePriorityMTrigger on NU__Membership__c (after insert, after update) {
	NU.TriggerHandlerManager.executeHandlers('CorporateAlliancePriorityM_Trigger', new CorporateAlliancePriorityM_Trigger());
}