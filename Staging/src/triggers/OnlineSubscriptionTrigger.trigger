trigger OnlineSubscriptionTrigger on Online_Subscription__c (before insert, before update) {
    NU.TriggerHandlerManager.executeHandlers('OnlineSubscriptionTrigger', new OnlineSubscriptionTriggerHandler());
}