trigger ConferenceProposalTrigger on Conference_Proposal__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	NU.TriggerHandlerManager.executeHandlers('ConferenceProposalTrigger', new ConferenceProposalTriggerHandlers());
}