public with sharing class CorporateAllianceMembershipPriority {

public static String getNotNull(String s){
		if (s == null){
				return '';
		}
		else {
				return s;
		}
}

public static void setCorporateAllianceMembershipPriority(Map<Id, Account> newAccounts){
		Set<Id> IdList = newAccounts.keySet();
    	
    	String AYearly = 'AM'+ string.valueof(System.Today().year()) +'%Annual%';
    	String SYearly = 'AM'+ string.valueof(System.Today().year()) +'%LeadingAge%Supporter%';
        List<Account> accounts = [SELECT Id, Name, Member_Product_Name__c, LeadingAge_ID__c, NU__RecordTypeName__c, LeadingAge_Member_Company__c, Provider_Membership__c,
                                  NU__Membership__c, NU__Membership__r.MembershipTypeName__c, NU__Membership__r.MembershipTypeProductName__c, 
                                  CAST_Membership__c,CAST_Member__c, CAST_Membership__r.MembershipTypeProductName__c, CAST_Member_Product_Name__c, 
                                  (SELECT Id, Account__c, ProductDesc__c, Status__c FROM Sponsorships__r WHERE ((ProductDesc__c LIKE :AYearly) OR (ProductDesc__c LIKE :SYearly )) AND (Status__c = 'Active') ORDER BY Sponsorship_Date__c DESC) 
                                  FROM Account 
                                  WHERE NU__RecordTypeName__c = :Constant.ACCOUNT_RECORD_TYPE_NAME_COMPANY
                                  AND ( Type = :Constant.ACCOUNT_RECORD_SUBTYPE_NAME_CORPORATE OR Type = :Constant.ACCOUNT_RECORD_SUBTYPE_NAME_UNIVERSITY)
                                  AND Id IN :IdList 
                                  ORDER BY LeadingAge_Member_Company__c, LeadingAge_ID__c];
        
        for (Account a : accounts){
        	Account d = new Account();
        	
        	if(a != Null && !getNotNull(a.LeadingAge_Member_Company__c).equalsIgnoreCase('Non-member'))
                {
                    if (a.Sponsorships__r != null && a.Sponsorships__r.size() > 0) {
                    	d = newAccounts.get(a.Id);
                        d.Membership_1__c = a.Sponsorships__r[0].ProductDesc__c;
                        d.Membership_2__c = a.CAST_Member_Product_Name__c;
                        if (getNotNull(d.Membership_1__c).containsIgnoreCase('Annual Meeting') && getNotNull(a.Member_Product_Name__c).equalsIgnoreCase('CAST Supporter') ) {
                        	d.Membership_1__c = a.CAST_Member_Product_Name__c;
                        	d.Membership_2__c = a.Sponsorships__r[0].ProductDesc__c;}
                        if (getNotNull(d.Membership_1__c).containsIgnoreCase('LeadingAge Supporter') && getNotNull(a.Member_Product_Name__c).equalsIgnoreCase('LeadingAge Supporter') ) {
                        	d.Membership_1__c = a.Member_Product_Name__c;
                        	d.Membership_2__c = a.Sponsorships__r[0].ProductDesc__c;} 
                        if (getNotNull(d.Membership_1__c).containsIgnoreCase('LeadingAge Supporter') && getNotNull(a.Member_Product_Name__c).containsIgnoreCase('CAST') )
                        {
                        	d.Membership_1__c = 'LeadingAge Supporter';
                           d.Membership_2__c = a.CAST_Member_Product_Name__c;
                        }
                        else { 
		                        if (getNotNull(d.Membership_1__c).containsIgnoreCase('LeadingAge Supporter') || getNotNull(d.Membership_2__c).containsIgnoreCase('LeadingAge Supporter'))    
		                        {
		                           d.Membership_1__c = 'LeadingAge Supporter';
		                           d.Membership_2__c = '';
		                        }     
                        }
                                	
                        
                 }
                 else if(a != Null && a.NU__Membership__c != null && (a.Member_Product_Name__c != null &&(getNotNull(a.Member_Product_Name__c).containsIgnoreCase('LeadingAge Supporter') || getNotNull(a.Member_Product_Name__c).containsIgnoreCase('Gold') || getNotNull(a.Member_Product_Name__c).containsIgnoreCase('Silver')
                   ||getNotNull(a.Member_Product_Name__c).equalsIgnoreCase('Business Associate') ||
                   getNotNull(a.Member_Product_Name__c).equalsIgnoreCase('CAST Business Associate') || getNotNull(a.Member_Product_Name__c).equalsIgnoreCase('CAST Supporter') )))
                  {
                    	d = newAccounts.get(a.Id);
                        d.Membership_1__c = a.Member_Product_Name__c;
                        d.Membership_2__c = '';
                        
                        if(a.CAST_Member__c == 'Yes')
                        {
                        	if (!getNotNull(d.Membership_1__c).containsIgnoreCase('CAST Business Associate') && !getNotNull(d.Membership_1__c).containsIgnoreCase('CAST Supporter') ) {
	                        	d.Membership_2__c = a.CAST_Membership__r.MembershipTypeProductName__c;
	                        }
	                        
	                        if(getNotNull(a.CAST_Membership__r.MembershipTypeProductName__c).equalsIgnoreCase('CAST Focus'))
	                        {
	                        	d.Membership_1__c = a.Member_Product_Name__c;
	                        	d.Membership_2__c = a.CAST_Membership__r.MembershipTypeProductName__c;
	                        }
	                        
	                        if((d.Membership_1__c == null || d.Membership_1__c == '') && a.CAST_Membership__r.MembershipTypeProductName__c != null)  
	                        {
	                        	d.Membership_1__c = a.CAST_Membership__r.MembershipTypeProductName__c;
	                        }
                        }
                        
                        
                  }

                }
                else{
                	if(a != Null)
                	{
                	d = newAccounts.get(a.Id);
                	d.Membership_1__c ='';
                	d.Membership_2__c ='';
                	}
                }
                
        }
    }
}