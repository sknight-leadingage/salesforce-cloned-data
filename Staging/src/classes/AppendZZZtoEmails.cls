/**
 * @author NimbleAMS
 * @description Appends '.zzz' to all emails. Used after a sandbox refresh to prevent from accidentally emailing members.
 */

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new AppendZZZtoEmails());

global class AppendZZZtoEmails implements Database.Batchable<sObject> {

	private String query;

	//add any custom email fields to this list, if it's on the account/contact record
    private List<string> fields = new List<string> {
        'PersonEmail',
        'NU__OtherEmail__c',
        'NU__PersonEmail__c',
        'NU__PrimaryContactEmail__c'
    };

    //batch apex jobs require three methods: start, execute, and finish; run in that order
                         
    global Database.QueryLocator start(Database.BatchableContext bc) {

        query = 'SELECT Id ';
        
        for (string field : fields) {
            query += ', ' + field;
        }
        
        query += ' FROM Account ';
        query += 'WHERE (' + fields[0] + ' != null';
        
        for (integer i = 1; i < fields.size(); i++) {
            query += ' OR ' + fields[i] + ' != null';
        }
        
        query += ') AND ((NOT ' + fields[0] + ' LIKE \'%.zzz\')';
        
        for (integer i = 1; i < fields.size(); i++) {
            query += ' OR (NOT ' + fields[i] + ' LIKE \'%.zzz\')';
        }
        
        query += ')';

		Organization org = [select Id, IsSandbox from Organization limit 1];
		
		//disallows this from being run in production
		if (!org.IsSandbox && !test.isRunningTest()) {
			return Database.getQueryLocator('SELECT Id FROM Account WHERE id = \'001000000000000\'');
		}
        
        return Database.getQueryLocator(query);
    }
                         
    global void execute(Database.BatchableContext bc, List<Account> accountsToUpdate) {
        for (Account account : accountsToUpdate) {
            for (string field : fields) {
                string value = (string)account.get(field);
                
                if (string.isNotBlank(value)) {
                    string newValue = value + '.zzz';
                    account.put(field, newValue);
                }
            }
        }
        
        update accountsToUpdate;
    }
    
    global void finish(Database.BatchableContext bc) {
        NU.BatchApexUtil.sendNotificationForFailures(bc);
    }
}