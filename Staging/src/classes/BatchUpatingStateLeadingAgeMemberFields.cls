global class BatchUpatingStateLeadingAgeMemberFields implements Database.Batchable<sobject>, Schedulable {
    public boolean bDateLimit = true;
    
    public static String getNotNull(String s){
		if (s == null){
				return '';
		}
		else {
				return s;
		}
	}
    global BatchUpatingStateLeadingAgeMemberFields() {
    	this.bDateLimit = true;
    }
    
    global BatchUpatingStateLeadingAgeMemberFields(boolean WithDateLimit) {
    	this.bDateLimit = WithDateLimit;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
	{
		String Query = 'SELECT Id, Name FROM Account';
		if (this.bDateLimit) {
			DateTime dt = System.Now().addHours(-2);
			Query = 'SELECT Id, Name FROM Account WHERE LastModifiedDate >= :dt' ;
		}
		return Database.getQueryLocator(Query);
	}
	
	global void execute(Database.BatchableContext BC,List<sObject> scope)
	{
		Map<Id, Account> newAccountsMap = new Map<Id, Account>();
		for (Account a : (List<Account>)scope){
			newAccountsMap.put(a.Id , a);  
		}
		setStateAndLeadingMemberField(newAccountsMap);
	}

    global void finish(Database.BatchableContext BC)
    {
    }
    
    private Account decideifUpdateNeeded(Account aToDecide, List<Account> parentAccounts) {
		Account returnAccount = null;
		
		String strLeadingAgeMemberStatus = Constant.LEADINGAGE_NONMEMBER;  //Assume Non-member to start off
		String strStateMemberStatus = Constant.STATE_NONMEMBER;            //Assume Non-member to start off
		
		//IF you are a state partner or Leadingage Activ then you are a member of your own state and leadingage. leadingage not state member 
		if (aToDecide.RecordTypeId == Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID && aToDecide.NU__Status__c == 'Active') {
		        if(aToDecide.name != 'LeadingAge'){
		        	strStateMemberStatus = Constant.STATE_MEMBER;
		        }
		        strLeadingAgeMemberStatus = Constant.LEADINGAGE_MEMBER;
		        
		        
		    }
		//Fix for Multi-Site Bug regarding memberships
		//If the account doesn't have any memberships directly attached then...
		    if (aToDecide.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID) {
		        if (aToDecide.Multi_Site_Provider_Member_Count__c >= 2) {
		            //If we're an MSO with 2+member children then we're a member
		            strLeadingAgeMemberStatus = Constant.LEADINGAGE_MEMBER;
		        }
		    }
		//If this account directly has membership records then try to figure out membership from that
		if (aToDecide.NU__Memberships__r != null && aToDecide.NU__Memberships__r.size() > 0) {
		    system.debug('MemberStatus :: Account: [' + aToDecide.Id + '] ' + aToDecide.Name + ', Pulling from direct memberships list');
		    for (NU__Membership__c m : aToDecide.NU__Memberships__r) {
		        system.debug('MemberStatus :: Account: [' + aToDecide.Name + '] Membership: Id=' + m.Id + ', ' + m.NU__Status__c + ', National=' + m.NU__MembershipType__r.Grants_National_Membership__c + ', State=' + m.NU__MembershipType__r.Grants_State_Membership__c);
		        if (m != null && m.NU__Status__c != null && m.NU__EntityName__c != null && m.NU__MembershipType__r != null
		                && m.NU__Status__c.equalsIgnoreCase(Constant.MEMBERSHIP_STATUS_CURRENT)) {
		                	
		            
				        if (getNotNull(m.MembershipTypeProductName__c).contains('CAST IAHSA Associate') || getNotNull(m.MembershipTypeProductName__c).contains('CAST Provider Associate') || getNotNull(m.MembershipTypeProductName__c).contains('CAST University Associate'))
				        {
				        	 //strLeadingAgeMemberStatus = Constant.LEADINGAGE_NONMEMBER;
				        }
				        else {
				    
						            if (m.NU__MembershipType__r.Grants_National_Membership__c == true) {
						                strLeadingAgeMemberStatus = Constant.LEADINGAGE_MEMBER;
						            }
						            if (m.NU__MembershipType__r.Grants_State_Membership__c == true) {
						                strStateMemberStatus = Constant.STATE_MEMBER;
						            }
				        }
		        }
		        

				
		    }
		}
		//else {
		    if (aToDecide.NU__PrimaryAffiliation__c != null) {
		        for (Account parentAcc : parentAccounts) {
		        	if (aToDecide.NU__PrimaryAffiliation__r.Id == parentAcc.Id && parentAcc.RecordTypeId == Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID){
					            	if (aToDecide.NU__Status__c == 'Active') {
									        if(parentAcc.name != 'LeadingAge'){
									        	strStateMemberStatus = Constant.STATE_MEMBER;
									        }
									        strLeadingAgeMemberStatus = Constant.LEADINGAGE_MEMBER;
									        
									        
									    }
					            }
					            
		            if (aToDecide.NU__PrimaryAffiliation__r.Id == parentAcc.Id && aToDecide.RecordTypeId != parentAcc.RecordTypeId) {
		            	if((parentAcc.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID && aToDecide.RecordTypeId == Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID) || parentAcc.RecordTypeId != Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID ){
				                
				                
				                if (parentAcc.LeadingAge_Member__c != null) {
				                        strLeadingAgeMemberStatus = parentAcc.LeadingAge_Member__c;
				                    }
				                if (parentAcc.State_Partner_Member__c != null) {
				                        strStateMemberStatus = parentAcc.State_Partner_Member__c;
				                    }
				                
				                if (parentAcc.NU__Memberships__r != null && parentAcc.NU__Memberships__r.size() > 0) {
				                    system.debug('MemberStatus :: Parent Account: [' + parentAcc.Id + '] ' + parentAcc.Name + ', Pulling from direct memberships list');
				                    
				                    
				                    for (NU__Membership__c m : parentAcc.NU__Memberships__r) {
				                        if (m != null && m.NU__Status__c != null && m.NU__EntityName__c != null && m.NU__MembershipType__r != null
				                                && m.NU__Status__c.equalsIgnoreCase(Constant.MEMBERSHIP_STATUS_CURRENT)) {
				                            
									        
									        if (getNotNull(m.MembershipTypeProductName__c).contains('CAST IAHSA Associate') || getNotNull(m.MembershipTypeProductName__c).contains('CAST Provider Associate') || getNotNull(m.MembershipTypeProductName__c).contains('CAST University Associate'))
									        {
									        	 //strLeadingAgeMemberStatus = Constant.LEADINGAGE_NONMEMBER;
									        }
									        else {
									    
											            if (m.NU__MembershipType__r.Grants_National_Membership__c == true) {
											                strLeadingAgeMemberStatus = Constant.LEADINGAGE_MEMBER;
											            }
											            if (m.NU__MembershipType__r.Grants_State_Membership__c == true) {
											                strStateMemberStatus = Constant.STATE_MEMBER;
											            }
									        }
				                    }
				                }
				                
				                
				                
							                
				                break;
		            	}
		            }
		            
		            
		        }
		        
		    }
		}
		
	
		
		if (aToDecide.State_Partner_Member__c != null && aToDecide.LeadingAge_Member__c != null) {
		    system.debug('MemberStatus :: Account: [' + aToDecide.Id + '] ' + aToDecide.Name + ', aToDecide.State_Partner_Member__c = ' + aToDecide.State_Partner_Member__c
		                    +  ', strStateMemberStatus = ' + strStateMemberStatus + ', aToDecide.LeadingAge_Member__c = ' + aToDecide.LeadingAge_Member__c + ', strLeadingAgeMemberStatus = ' + strLeadingAgeMemberStatus);
		}
		
		Boolean bStateMemberUpdate = false;
		Boolean bLeadingAgeMemberUpdate = false;
		
		//Test for State Member Field needing an update
		if (aToDecide.State_Partner_Member__c == null || (aToDecide.State_Partner_Member__c != null && aToDecide.State_Partner_Member__c != strStateMemberStatus)) {
		    bStateMemberUpdate = true;
		}
		//Test for LeadingAge Member field needing an update
		if (aToDecide.LeadingAge_Member__c == null || (aToDecide.LeadingAge_Member__c != null && aToDecide.LeadingAge_Member__c != strLeadingAgeMemberStatus)) {
		    bLeadingAgeMemberUpdate = true;
		}
		
		//Display debug message if required
		if (bStateMemberUpdate == true || bLeadingAgeMemberUpdate == true) {
		    system.debug('MemberStatus :: Account: [' + aToDecide.Id + '] ' + aToDecide.Name + ', Status needs updating, adding to list');
		}
		
		//Assign appropriate update record
		if (bStateMemberUpdate == true && bLeadingAgeMemberUpdate == true) {
		    returnAccount = new Account(Id = aToDecide.Id, State_Partner_Member__c = strStateMemberStatus, LeadingAge_Member__c = strLeadingAgeMemberStatus);
		}
		else if (bStateMemberUpdate == true && bLeadingAgeMemberUpdate == false) {
		    returnAccount = new Account(Id = aToDecide.Id, State_Partner_Member__c = strStateMemberStatus);
		}
		else if (bStateMemberUpdate == false && bLeadingAgeMemberUpdate == true) {
		    returnAccount = new Account(Id = aToDecide.Id, LeadingAge_Member__c = strLeadingAgeMemberStatus);
		}
		
		return returnAccount;
	}
            
    private void setStateAndLeadingMemberField(Map<Id, Account> newAccounts){
        Set<Id> IdList = newAccounts.keySet();
        Map<Id, Account> accountsUpdate = new Map<Id, Account>();
        List<Account> accounts = [select id,
                        name,
                        NU__Status__c,
                        NU__PrimaryAffiliation__c,
                        NU__PrimaryAffiliation__r.Id,
                        NU__PrimaryAffiliation__r.State_Partner_ID__c,
                        NU__PrimaryAffiliation__r.State_Partner_ID__r.Name,
                        //NU__PrimaryAffiliation__r.State_Partner_Name__c,
                        Multi_Site_Provider_Member_Count__c,
                        Multi_Site_Corporate__c,
                        //State_Partner_Name__c,
                        State_Partner_Member__c,
                        LeadingAge_Member__c,
                        LeadingAge_Member_Company__c,
                        LeadingAge_Member_Individual__c,
                        RecordTypeId, 
                        NU__PrimaryEntity__c,
                        NU__PrimaryEntity__r.Name,
                        State_Partner_ID__c,
                        State_Partner_ID__r.Name,
                        (Select id,
                                name,
                                NU__MembershipType__c,
                                NU__MembershipType__r.Grants_National_Membership__c,
                                NU__MembershipType__r.Grants_State_Membership__c,
                                MembershipTypeProductName__c,
                                NU__EntityName__c,
                                NU__Status__c,
                                NU__StartDate__c,
                                NU__EndDate__c
                           from NU__Memberships__r WHERE NU__Status__c = 'Current')
                   from Account
                  where id in :IdList];

        //Primary Affiliation preload
        Set<Id> paffIds = new Set<Id>();
        for (Account a : accounts){
            if ((a.NU__Memberships__r == null || (a.NU__Memberships__r != null && a.NU__Memberships__r.size() == 0)) && a.NU__PrimaryAffiliation__c != null) {
                paffIds.add(a.NU__PrimaryAffiliation__r.Id);
            }
        }
        
        List<Account> parentAccounts = new List<Account>();
        if (paffIds.size() > 0) {
            parentAccounts = [SELECT Id,
                        Name,
                        NU__Status__c,
                        NU__PrimaryAffiliation__c,
                        NU__PrimaryAffiliation__r.Id,
                        Multi_Site_Provider_Member_Count__c,
                        Multi_Site_Corporate__c,
                        //State_Partner_Name__c,
                        State_Partner_Member__c,
                        LeadingAge_Member__c,
                        LeadingAge_Member_Company__c,
                        LeadingAge_Member_Individual__c,
                        RecordTypeId, 
                        NU__PrimaryEntity__c,
                        NU__PrimaryEntity__r.Name,
                        State_Partner_ID__c,
                        State_Partner_ID__r.Name,
                        (SELECT id,
                                name,
                                NU__MembershipType__c,
                                NU__MembershipType__r.Grants_National_Membership__c,
                                NU__MembershipType__r.Grants_State_Membership__c,
                                MembershipTypeProductName__c,
                                NU__EntityName__c,
                                NU__Status__c,
                                NU__StartDate__c,
                                NU__EndDate__c
                           FROM NU__Memberships__r WHERE NU__Status__c = 'Current')
                  FROM Account
                  WHERE Id IN :paffIds];
        }
        
        List<Account> childPrimaryAccounts = [SELECT Id,
                        Name,
                        NU__Status__c,
                        NU__PrimaryAffiliation__c,
                        NU__PrimaryAffiliation__r.Id,
                        Multi_Site_Provider_Member_Count__c,
                        Multi_Site_Corporate__c,
                        //State_Partner_Name__c,
                        State_Partner_Member__c,
                        LeadingAge_Member__c,
                        LeadingAge_Member_Company__c,
                        LeadingAge_Member_Individual__c,
                        RecordTypeId, 
                        NU__PrimaryEntity__c,
                        NU__PrimaryEntity__r.Name,
                        State_Partner_ID__c,
                        State_Partner_ID__r.Name,
                        (SELECT id,
                                name,
                                NU__MembershipType__c,
                                NU__MembershipType__r.Grants_National_Membership__c,
                                NU__MembershipType__r.Grants_State_Membership__c,
                                MembershipTypeProductName__c,
                                NU__EntityName__c,
                                NU__Status__c,
                                NU__StartDate__c,
                                NU__EndDate__c
                           FROM NU__Memberships__r WHERE NU__Status__c = 'Current')
                  FROM Account
                  WHERE NU__PrimaryAffiliation__c IN :IdList];

        Account toUpdate = null;
        system.debug('MemberStatus :: Begin');
        
        //Update current-level accounts (Passing their parents for possible membership lookup purposes)
        for (Account a : accounts){
            toUpdate = decideifUpdateNeeded(a, parentAccounts);
            if (toUpdate != null) {
            	//Update cached accounts for later child flowdown.
            	if (toUpdate.State_Partner_Member__c != null) {
            		a.State_Partner_Member__c = toUpdate.State_Partner_Member__c; 
            	}
            	//Update cached accounts for later child flowdown.
            	if (toUpdate.LeadingAge_Member__c != null) {
            		a.LeadingAge_Member__c = toUpdate.LeadingAge_Member__c; 
            	}
            	//Set up actual DML update list
                if (accountsUpdate.containsKey(toUpdate.Id) == false) {
                    accountsUpdate.put(toUpdate.Id, toUpdate);
                }
            }
        }
        
        //Update children of current-level accounts
        for (Account childAccount : childPrimaryAccounts) {
            toUpdate = decideifUpdateNeeded(childAccount, accounts);
            if (toUpdate != null && accountsUpdate.containsKey(toUpdate.Id) == false) {
                accountsUpdate.put(toUpdate.Id, toUpdate);
            }
        }
        
        if (accountsUpdate.size() > 0) {
            system.debug('MemberStatus :: Updating list of changed accounts.');
            update accountsUpdate.values();
        }
        
		system.debug('MemberStatus :: End');
	}
            
    public static void schedule() {
        System.schedule('BatchUpMemberFieldsHourly', '0 0 * * * ?', new BatchUpatingStateLeadingAgeMemberFields());
    }
    
    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }
}