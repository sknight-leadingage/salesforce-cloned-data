public with sharing class EduProposalChangelog_Controller extends EduReportsControllerBase {
    static final string RTYPE_BY_STATUS = 'By Status';
    static final string RTYPE_BY_TRACK = 'By Track';

    public override string getPageTitle() {
        if (RTypeSel == null) {
            return '';
        } else {
            return '(' + RTypeSel + ')';
        }
    }
    public date startDate { get; set; }
    public date endDate { get; set; }
    
    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(RTYPE_BY_STATUS, RTYPE_BY_STATUS));
        options.add(new SelectOption(RTYPE_BY_TRACK, RTYPE_BY_TRACK));
        return options; 
    }
    
    public List<Conference_Proposal__History> HistoryInfo { get; set; }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == RTYPE_BY_TRACK) {
            WizardStep++;
        }
        WizardStep++;
        SetQuery();
    }
    
    public override void WizardBack(){
        if (RTypeSel == RTYPE_BY_TRACK) {
            WizardStep--;
        }
        WizardStep--;
    }
    // ******* END: Wizard Setup Code *********
    
    public EduProposalChangelog_Controller() {
        iWizardMax = 3;
        WizardStep = 1;
     }
    
    public void SetQuery() {
        string sOrder = '';
        if (RTypeSel == RTYPE_BY_STATUS){
            sOrder = 'Parent.Status__c, Parent.Proposal_Number__c ASC';
        }
        else if (RTypeSel == RTYPE_BY_TRACK){
            sOrder = 'Parent.Conference_Track__r.Name, Parent.Proposal_Number__c ASC, CreatedDate DESC';
        }
        string FieldNotFilter = 'Field != \'created\'';
        HistoryInfo = Database.Query('SELECT Parent.Status__c,Parent.Conference_Track__c,Parent.Conference_Track__r.Name,CreatedById,CreatedBy.Name,CreatedDate,Field,Id,IsDeleted,OldValue,NewValue,Parent.Id,Parent.name,Parent.Proposal_Full_Title__c FROM Conference_Proposal__History WHERE ' + FieldNotFilter + ' AND Parent.Conference__c = :EventId AND CreatedDate >= :startDate AND CreatedDate <= :endDate ORDER BY ' + sOrder);
    }
}