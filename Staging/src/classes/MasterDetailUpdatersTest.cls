@isTest
private class MasterDetailUpdatersTest {
    @isTest
    private static void subscriptionUpdaterTest() {
        MasterDetailSubscriptionUpdater.execute();
        MasterDetailSubscriptionUpdater instance = new MasterDetailSubscriptionUpdater();
        instance.start(null);
        try {
            instance.execute(null, new List<NU__Subscription__c> { new NU__Subscription__c() });
        } catch (Exception e) { }
        instance.finish(null);
    }
    
    @isTest
    private static void donationUpdaterTest() {
        MasterDetailDonationUpdater.execute();
        MasterDetailDonationUpdater instance = new MasterDetailDonationUpdater();
        instance.start(null);
        try {
            instance.execute(null, new List<NU__Donation__c> { new NU__Donation__c() });
        } catch (Exception e) { }
        instance.finish(null);
    }
    
    @isTest
    private static void deferredScheduleUpdater() {
        MasterDetailDeferredScheduleUpdater.execute();
        MasterDetailDeferredScheduleUpdater instance = new MasterDetailDeferredScheduleUpdater();
        instance.start(null);
        try {
            instance.execute(null, new List<NU__DeferredSchedule__c> { new NU__DeferredSchedule__c() });
        } catch (Exception e) { }
        instance.finish(null);
    }
}