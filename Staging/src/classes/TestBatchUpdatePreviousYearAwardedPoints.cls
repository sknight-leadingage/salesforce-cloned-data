/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
public class TestBatchUpdatePreviousYearAwardedPoints
{
    public static testMethod void testBatch() 
    {
      List <Account> accounts = new List<Account>();
       
       for(integer i = 0; i<75; i++)
       {
             Account a = new Account(Name = 'Test Company', Current_Year_Priority_Points__c = 2, Current_Year_Priority_Point_Adjustments__c = 1, Previous_Years_Priority_Points__c = 0);  
             accounts.add(a);
       }
      
       for(integer i = 0; i<25; i++)
       {
             Account a = new Account(Name = 'Test Company', Current_Year_Priority_Points__c = null, Current_Year_Priority_Point_Adjustments__c = null);  
             accounts.add(a);
       }
       
       insert accounts;
       
       
       Test.StartTest();
       BatchUpdatePreviousYearAwardedPoints batch = new BatchUpdatePreviousYearAwardedPoints();
       ID batchprocessid = Database.executeBatch(batch, 100);
       Test.StopTest();
    
       System.AssertEquals(75, database.countquery('SELECT COUNT() FROM Account WHERE Previous_Years_Priority_Points__c = 3'));  
       System.AssertEquals(25, database.countquery('SELECT COUNT() FROM Account WHERE Previous_Years_Priority_Points__c = null'));  
       
   }
}