public with sharing class EventQuerier {

    public static List<NU__Event__c> getConferences(){
        return [select id,
                       name,
                       NU__ShortName__c
                  from NU__Event__c
                 where NU__Type__c = :Constant.EVENT_TYPE_CONFERENCE
                 	   AND NU__Entity__r.Name = :Constant.LEADINGAGE_ENTITY_NAME
                 order by NU__StartDate__c desc];
    }
    
    public static NU__Event__c getConferenceById(Id conferenceId){
        if (conferenceId == null){
            return null;
        }
        
        List<NU__Event__c> conferences = 
        [select id,
                name,
                NU__ShortName__c
           from NU__Event__c
          where id = :conferenceId];
        
        if (conferences.size() > 0){
            return conferences[0];
        }
        
        return null;
    }
}