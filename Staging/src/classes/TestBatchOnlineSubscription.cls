/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * (SeeAllData=true)
 */
@isTest
private class TestBatchOnlineSubscription {
	public static final String DEFAULT_BUSINESS_ACCOUNT_NAME = 'Test Provider';
    public static final String DEFAULT_FIRST_NAME = 'FirstName';
    public static final String DEFAULT_LAST_NAME = 'LastName';
    public static final String DEFAULT_ACCOUNT_STATUS = Constant.ACCOUNT_STATUS_ACTIVE;  
    public static final String DEFAULT_BILLING_STREET = '656 High Road';
    public static final String DEFAULT_BILLING_CITY = 'LeadingVille';
    public static final String DEFAULT_BILLING_STATE = 'NY';
    public static final String DEFAULT_BILLING_POSTAL_CODE = '14555';
    
    private static Account TestAccount1;
    private static Account TestAccount2;
    private static NU__Membership__c IndividualsMT;
    private static List<NU__Product__c> onlineSubProducts;
	
	private static Integer increment = 0;
    private static String Increment() {
        return String.valueOf(++increment);
    }
    
    static void SetBaseData() {
        NU__Entity__c TestEntity = new NU__Entity__c(name = Constant.LEADINGAGE_ENTITY_NAME);
        insert TestEntity;
        
        // Test General Ledger Account
        NU__GLAccount__c myGLAccount = new NU__GLAccount__c(name='xxx-112-xxx', NU__Entity__c = TestEntity.Id );
        insert myGLAccount;
        
        //creating individual account
        Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
        DateTime dt = Date.today();
        onlineSubProducts = DataFactoryProductExt.insertOnlineSubscriptionProducts(25, TestEntity, myGLAccount);
        Set<Id> onlineSubProductIds = NU.CollectionUtil.getSobjectIds(onlineSubProducts);
        
        onlineSubProducts[3].Membership_default__c = true;
        onlineSubProducts[4].Name = Constant.ONLINE_SUBSCRITPION_PRODUCT_GENERAL_INFORMATION;
        onlineSubProducts[6].Membership_default__c = true;
        onlineSubProducts[7].Name = Constant.ONLINE_SUBSCRITPION_PRODUCT_EXHIBITS_AND_ADVERTISTING;
        onlineSubProducts[12].Name = Constant.ONLINE_SUBSCRITPION_PRODUCT_EDUCATION_AND_CONFERENCES;
        update onlineSubProducts;
        
        TestAccount1 = new Account(
		        RecordTypeId = AccountRecordTypeUtil.getIndividualRecordType().Id,
		        FirstName = DEFAULT_FIRST_NAME,
		        LastName = DEFAULT_LAST_NAME + Increment(),
		        PersonEmail = DEFAULT_LAST_NAME + Increment() + '@test' + pwd + '.com',
		        BillingStreet = DEFAULT_BILLING_STREET,
		        BillingCity = DEFAULT_BILLING_CITY,
		        BillingState = DEFAULT_BILLING_STATE,
		        BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
		        LeadingAge_Member__c = Constant.LEADINGAGE_MEMBER,
		        NU__Status__c = DEFAULT_ACCOUNT_STATUS );
       insert TestAccount1;

       IndividualsMT = DataFactoryMembershipExt.createCurrentAssociateMembership(TestAccount1.Id);
       insert IndividualsMT;
       
       TestAccount2 = new Account(
		        RecordTypeId = AccountRecordTypeUtil.getIndividualRecordType().Id,
		        FirstName = DEFAULT_FIRST_NAME,
		        LastName = DEFAULT_LAST_NAME + Increment(),
		        PersonEmail = DEFAULT_FIRST_NAME + Increment() + '@test' + pwd + '.com',
		        BillingStreet = DEFAULT_BILLING_STREET,
		        BillingCity = DEFAULT_BILLING_CITY,
		        BillingState = DEFAULT_BILLING_STATE,
		        BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
		        LeadingAge_Member__c = Constant.LEADINGAGE_NONMEMBER,
		        NU__Status__c = DEFAULT_ACCOUNT_STATUS );
       insert TestAccount2;
       
       BatchUpatingStateLeadingAgeMemberFields D = new BatchUpatingStateLeadingAgeMemberFields(true);
       Database.executeBatch(D);
       
       Account T = [SELECT Id, Name,  NU__Status__c, LeadingAge_Member_Individual__c FROM Account WHERE Id = :TestAccount1.id];
       System.assertEquals(T.NU__Status__c , Constant.ACCOUNT_STATUS_ACTIVE);
       System.assertEquals(T.LeadingAge_Member_Individual__c , Constant.LEADINGAGE_MEMBER);
       
       Account T2 = [SELECT Id, Name,  NU__Status__c, LeadingAge_Member_Individual__c FROM Account WHERE Id = :TestAccount2.id];
       System.assertEquals(T2.NU__Status__c , Constant.ACCOUNT_STATUS_ACTIVE);
       System.assertEquals(T2.LeadingAge_Member_Individual__c , Constant.LEADINGAGE_NONMEMBER);
    }
    
    static testMethod void testInitialMember() {
    	SetBaseData();
    	
    	Test.startTest();
		BatchOnlineSubscriptions c = new BatchOnlineSubscriptions();
		Database.executeBatch(c);
		Test.stopTest();
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id];
		
		system.assertNotEquals(0, subs.size(), 'Account 1 should have at least 1 member default subscription');
    }
    
    static testMethod void testInitialNonMember() {
    	SetBaseData();

    	Test.startTest();
		BatchOnlineSubscriptions c = new BatchOnlineSubscriptions();
		Database.executeBatch(c);
		Test.stopTest();
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount2.Id];
		
		system.assertEquals(0, subs.size(), 'Account 2 should have no member default subscriptions');
    }
    
    static testMethod void testNewMemberProductAdded() {
    	SetBaseData();
    	
    	onlineSubProducts[10].Membership_default__c = true;
    	update onlineSubProducts;
    	
    	Test.startTest();
		BatchOnlineSubscriptions c = new BatchOnlineSubscriptions();
		Database.executeBatch(c);
		Test.stopTest();
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id];
		
		system.assertEquals(3, subs.size(), 'Account 1 should have 3 member default subscriptions');
    }
    
    static testMethod void testBounceFields() {
        SetBaseData();
    	
    	onlineSubProducts[10].Membership_default__c = true;
    	update onlineSubProducts;
    	
		//BEGIN: Due to timing issues running multiple batches in the same testMethod, we fake add the sub for this specific test
		Online_Subscription__c subTest = new Online_Subscription__c(Product__c = onlineSubProducts[10].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today());
		insert subTest;
		//END
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id];
		
		system.assertEquals(1, subs.size(), 'Account 1 should have 1 member default subscription');
		
		Date dBouncedDate = Date.today();
		String sBouncedReason = 'Because I said So!';
		TestAccount1.PersonEmailBouncedDate = dBouncedDate;
		TestAccount1.PersonEmailBouncedReason = sBouncedReason;
		update TestAccount1;
		
		Account T = [SELECT Id, Name,  NU__Status__c, LeadingAge_Member_Individual__c, PersonEmailBouncedDate, PersonEmailBouncedReason FROM Account WHERE Id = :TestAccount1.id];
		System.assertEquals(T.NU__Status__c , Constant.ACCOUNT_STATUS_ACTIVE);
		System.assertEquals(T.LeadingAge_Member_Individual__c, Constant.LEADINGAGE_MEMBER);
		System.assertEquals(sBouncedReason, T.PersonEmailBouncedReason);
		System.assertEquals(dBouncedDate, T.PersonEmailBouncedDate);

    	Test.startTest();
		BatchOnlineSubscriptions c2 = new BatchOnlineSubscriptions();
		Database.executeBatch(c2);
		Test.stopTest();
		
		List<Online_Subscription__c> subs2 = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_HELD];
		
		system.assertNotEquals(0, subs2.size(), 'Account 1 should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_HELD + ' subscription');
    }
    
    static testMethod void testInactiveOnAccountOrSubChange() {
        SetBaseData();
    	
    	onlineSubProducts[10].Membership_default__c = true;
    	update onlineSubProducts;
    	
		//BEGIN: Due to timing issues running multiple batches in the same testMethod, we fake add the sub for this specific test
		Online_Subscription__c subTest = new Online_Subscription__c(Product__c = onlineSubProducts[10].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today());
		insert subTest;
		//END
		
		TestAccount1.NU__Status__c = Constant.ACCOUNT_STATUS_INACTIVE;
		update TestAccount1;
		
    	Test.startTest();
		BatchOnlineSubscriptions c2 = new BatchOnlineSubscriptions();
		Database.executeBatch(c2);
		Test.stopTest();
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_INACTIVE];
		
		system.assertNotEquals(0, subs.size(), 'Account 1 should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_INACTIVE + ' subscription');
    }
    
    static testMethod void testInactiveOnLossofMembership() {
        SetBaseData();
    	
    	onlineSubProducts[10].Membership_default__c = true;
    	update onlineSubProducts;
    	
		//BEGIN: Due to timing issues running multiple batches in the same testMethod, we fake add the sub for this specific test
		List<Online_Subscription__c> subTests = new List<Online_Subscription__c>();
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[3].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[6].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[10].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		insert subTests;
		//END
		
		Account T = [SELECT Id, NU__Status__c, LeadingAge_Member__c, LeadingAge_Member_Individual__c, (SELECT Id, Status__c, Product__c, Product__r.Membership_default__c FROM Online_Subscriptions__r WHERE Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE AND Product__r.Membership_default__c = true) FROM Account WHERE Id = :TestAccount1.Id];
		system.assertEquals(Constant.ACCOUNT_STATUS_ACTIVE, T.NU__Status__c, 'Account Status is wrong.');
		system.assertEquals(Constant.LEADINGAGE_MEMBER, T.LeadingAge_Member_Individual__c, 'Account has the wrong Member Field value');
		system.assertNotEquals(0, T.Online_Subscriptions__r.size(), 'While Still Member, Account should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_ACTIVE + ' member default sub.');
		
		TestAccount1.LeadingAge_Member__c = Constant.LEADINGAGE_NONMEMBER;
		update TestAccount1;
		
		
		
		
		Account T2 = [SELECT Id, NU__Status__c, LeadingAge_Member__c, LeadingAge_Member_Individual__c, (SELECT Id, Status__c, Product__c, Product__r.Membership_default__c FROM Online_Subscriptions__r WHERE Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE AND Product__r.Membership_default__c = true) FROM Account WHERE Id = :TestAccount1.Id];
		system.assertEquals(Constant.ACCOUNT_STATUS_ACTIVE, T2.NU__Status__c, 'Account Status is wrong.');
		system.assertEquals(Constant.LEADINGAGE_NONMEMBER, T2.LeadingAge_Member_Individual__c, 'Account has the wrong Member Field value');
		system.assertNotEquals(0, T2.Online_Subscriptions__r.size(), 'After Non-Member before Batch Job, Account should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_ACTIVE + ' member default sub.');
		
    	Test.startTest();
		BatchOnlineSubscriptions c2 = new BatchOnlineSubscriptions();
		Database.executeBatch(c2);
		Test.stopTest();
		
		/*
		Account T3 = [SELECT Id, NU__Status__c, LeadingAge_Member__c, LeadingAge_Member_Individual__c, (SELECT Id, Status__c, Product__c, Product__r.Membership_default__c FROM Online_Subscriptions__r WHERE Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE AND Product__r.Membership_default__c = true) FROM Account WHERE Id = :TestAccount1.Id];
		system.assertEquals(Constant.ACCOUNT_STATUS_ACTIVE, T3.NU__Status__c, 'Account Status is wrong.');
		system.assertEquals(Constant.LEADINGAGE_NONMEMBER, T3.LeadingAge_Member_Individual__c, 'Account has the wrong Member Field value');
		system.assertNotEquals(0, T3.Online_Subscriptions__r.size(), 'After Non-Member before Batch Job, Account should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_ACTIVE + ' member default sub.');
		
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_INACTIVE];
		
		system.assertNotEquals(0, subs.size(), 'Account 1 should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_INACTIVE + ' subscription');
		
		*/
    }
    
    static testMethod void testDefaultSubsWithProviderAffiliation() {
        SetBaseData();
        
        //BEGIN: Force Sub to products we're testing
		List<Online_Subscription__c> subTests = new List<Online_Subscription__c>();
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[4].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[7].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_INACTIVE, Start_Date__c = Date.today()));
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[12].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		insert subTests;
        //END
    	
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
        NU__Affiliation__c primaryAffil = NU.DataFactoryAffiliation.insertAffiliation(TestAccount1.Id, providerAccount.Id, true);
        
    	Test.startTest();
		BatchOnlineSubscriptions c2 = new BatchOnlineSubscriptions();
		Database.executeBatch(c2);
		Test.stopTest();
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE];
		
		system.assertNotEquals(0, subs.size(), 'Account 1 should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_ACTIVE + ' subscription');
    }
    
    static testMethod void testDefaultSubsWithCompanyAffiliation() {
        SetBaseData();
        
        //BEGIN: Force Sub to products we're testing
		List<Online_Subscription__c> subTests = new List<Online_Subscription__c>();
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[4].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[7].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_INACTIVE, Start_Date__c = Date.today()));
		subTests.add(new Online_Subscription__c(Product__c = onlineSubProducts[12].Id, Account__c = TestAccount1.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = Date.today()));
		insert subTests;
        //END
    	
        Account companyAccount = DataFactoryAccountExt.insertCompanyAccount();
        NU__Affiliation__c primaryAffil = NU.DataFactoryAffiliation.insertAffiliation(TestAccount1.Id, companyAccount.Id, true);
        
    	Test.startTest();
		BatchOnlineSubscriptions c2 = new BatchOnlineSubscriptions();
		Database.executeBatch(c2);
		Test.stopTest();
		
		List<Online_Subscription__c> subs = [SELECT Id FROM Online_Subscription__c WHERE Account__c = :TestAccount1.Id AND Status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE];
		
		system.assertNotEquals(0, subs.size(), 'Account 1 should have at least 1 ' + Constant.SUBSCRIPTION_STATUS_ACTIVE + ' subscription');
    }
}