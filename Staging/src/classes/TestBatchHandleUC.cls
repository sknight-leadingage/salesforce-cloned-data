@isTest
public with sharing class TestBatchHandleUC {
    static testMethod void StartUnitTest() {
    	
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount(10000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, providerAccount.Id, true);
        
        Account normalizeTest = new Account(Id = providerAccount.Id, Under_Construction__c = false);
        update normalizeTest;
        
        Account prov = [SELECT Id, Under_Construction__c FROM Account WHERE Id = :providerAccount.Id LIMIT 1];
        NU__Membership__c provM = [SELECT Id, Under_Construction__c FROM NU__Membership__c WHERE NU__Account__c = :providerAccount.Id LIMIT 1];
        
        System.assertEquals(false, prov.Under_Construction__c);
        System.assertEquals(false, provM.Under_Construction__c);
        
        normalizeTest.Under_Construction__c = true;
        update normalizeTest;
        
		Test.startTest();
        BatchHandleUnderConstruction c = new BatchHandleUnderConstruction();
        Database.executeBatch(c);
        Test.stopTest();
        
        prov = [SELECT Id, Under_Construction__c FROM Account WHERE Id = :providerAccount.Id LIMIT 1];
        provM = [SELECT Id, Under_Construction__c FROM NU__Membership__c WHERE NU__Account__c = :providerAccount.Id LIMIT 1];

        System.assertEquals(true, prov.Under_Construction__c);
        System.assertEquals(true, provM.Under_Construction__c);        
    }
}