/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class various test methods to test the AccountQuerier class logic.
 */
@isTest
private class TestAccountQuerier {

    static testMethod void getAccountByIdNullAccountIdTest() {
        Account nullAccount = AccountQuerier.getAccountById(null);
        system.assertEquals(null, nullAccount, 'The account is not null.');
    }
    
    static testMethod void getAccountByIdNoAccountTest() {
    	Account account = DataFactoryAccountExt.insertIndividualAccount();
    	
    	Id accountId = account.Id;
    	delete account;
    	
        Account noAccount = AccountQuerier.getAccountById(accountId);
        system.assertEquals(null, noAccount, 'The account was found.');
    }
    
    static testmethod void getAccountByIdTest(){
    	Account account = DataFactoryAccountExt.insertIndividualAccount();
    	
    	Account queriedAcct = AccountQuerier.getAccountById(account.Id);
    	system.assert(queriedAcct != null, 'The queried account was not found.');
    }
    
    static testmethod void getAccountsByIdsTest(){
    	Account account = DataFactoryAccountExt.insertIndividualAccount();
    	
    	List<Account> queriedAccts = AccountQuerier.getAccountsByIds(new Set<Id>{ account.Id });
    	system.assert(NU.CollectionUtil.listHasElements(queriedAccts), 'The queried account was not found.');
    }
}