public class OrderInvoicer {
	public static final String NO_ORDERS_TO_INVOICE = 'There are no orders to invoice.';

	public NU.OperationResult invoiceOrders(List<NU__Order__c> ordersToInvoice, Date invoiceDate){
		NU.OperationResult invoiceResult = new NU.OperationResult();
		
		if (NU.CollectionUtil.listHasElements(ordersToInvoice) == false){
			NU.OperationResult.addErrorMessage(invoiceResult, NO_ORDERS_TO_INVOICE);

			return invoiceResult;
		}

		// To Do possibly: Add validation to ensure a valid invoice Date despite it being optional.

		for (NU__Order__c orderToInvoice : ordersToInvoice){
			orderToInvoice.NU__InvoiceGenerated__c = true;

			if (invoiceDate != null){
				orderToInvoice.NU__InvoiceDate__c = invoiceDate;
			}
		}

		invoiceResult = saveOrders(ordersToInvoice);

		return invoiceResult;
	}

	private NU.OperationResult saveOrders(List<NU__Order__c> ordersToInvoice){
		NU.OperationResult saveResult = new NU.OperationResult();
		
		try{
			upsert ordersToInvoice;
		}
		catch(Exception ex){
			saveResult = NU.OperationResult.getErrorResult(ex);
		}
		
		return saveResult;
	}
}