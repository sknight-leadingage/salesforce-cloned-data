global class BatchUpdateProviderLapsedOn implements Database.Batchable<sobject>
{
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator('SELECT Id, Name, LeadingAge_ID__c, Provider_Lapsed_On__c, Cancellation_Reason__c FROM Account WHERE Provider_Lapsed_On__c != null AND Provider_Lapsed_On__c <= TODAY ORDER BY Provider_Lapsed_On__c');
   }
   global void execute(Database.BatchableContext BC,List<sObject> scope)
   {
		Map<Id,Sobject> lAcc = new Map<Id,Sobject>(scope);
		List<NU__Membership__c> lMShips = [SELECT Id, NU__Account__c, Membership_Date__c, NU__StartDate__c, NU__EndDate__c, NU__EndDateOverride__c, NU__Status__c, Cancellation_Reason__c, NU__OrderItemLine__c, NU__OrderItemLine__r.NU__DeferredSchedule__c FROM NU__Membership__c WHERE NU__Account__c IN :lAcc.keySet()];
		
		Map<Id, Account> lUpdateAcc = new Map<Id, Account>();
		List<NU__Membership__c> lUpdateMShips = new List<NU__Membership__c>();

		Account a = null;
		for (NU__Membership__c m : lMShips) {
	        a = (Account)lAcc.get(m.NU__Account__c);
	        if (a.Provider_Lapsed_On__c >= m.NU__StartDate__c && a.Provider_Lapsed_On__c <= m.NU__EndDate__c) {
	        	if (!lUpdateAcc.containsKey(a.Id)) {
	        		lUpdateAcc.put(a.Id, new Account(Id = a.Id, Provider_Lapsed_On__c = null, Cancellation_Reason__c = null));
	        	}
				
				lUpdateMShips.add(new NU__Membership__c(Id = m.Id, Last_Provider_Lapsed_On__c = a.Provider_Lapsed_On__c, Cancellation_Reason__c = a.Cancellation_Reason__c, NU__EndDateOverride__c = a.Provider_Lapsed_On__c));
	        }
		}

		for (Id af : lAcc.keySet()) {
            if (!lUpdateAcc.containsKey(af)) {
	            lUpdateAcc.put(af, new Account(Id = af, Provider_Lapsed_On__c = null));
            }
		}

		if (lUpdateAcc.size() > 0 || lUpdateMShips.size() > 0) {
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AccountTriggerHandlers');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTriggerHandlers');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AffiliationTriggerHandlers');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggerHandlers');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AffiliationTriggers');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggers');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTrigger');
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger_Subscriptions');
			if (lUpdateAcc.size() > 0) {
		        update lUpdateAcc.values();
			}
	
			if (lUpdateMShips.size() > 0) {
		        update lUpdateMShips;
			}
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger_Subscriptions');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTriggerHandlers');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AffiliationTriggers');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTriggers');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTrigger');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AffiliationTriggerHandlers');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTriggerHandlers');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AccountTriggerHandlers');
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger');
		}
   }

	global void finish(Database.BatchableContext BC)
	{
		//Ensure the job only runs once when temp scheduled... https://help.salesforce.com/apex/HTViewSolution?id=000002809&language=en_US
		AsyncApexJob a = [SELECT Id FROM AsyncApexJob WHERE Id = :BC.getJobId()];
		system.abortJob(a.id);
	}
}