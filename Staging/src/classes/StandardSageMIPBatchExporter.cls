/**
 * @author NimbleAMS
 * @date Updated: 3/4/2015
 * @description Exposes methods for the Sage MIP Batch Exporter. Implements IBatchExporter.
 */
global class StandardSageMIPBatchExporter implements NU.IBatchExporter {

	//constants
    public static final String EXPORT_FOLDER_NAME = 'GL Export';
    
    public static final String NO_BATCH_IDS_TO_EXPORT_ERR_MSG = 'No batch ids were given to export.';

    public static final String NEWLINE = '\r\n';
    public static final String TRANSACTION_CATEGORY_CREDIT = 'Credit';

	//global variables
    private String exportNumber { get; set; }
    private Document detailDocument { get; set; }
    
	//define all possible rows in the export file
    public class Row
    {
		public String rowType { get; set; }
		public String exportNumber { get; set; }
		public String batchNumber { get; set; }
		public String jvString { get; set; }
		public String sessionDescription { get; set; }
		public String headerDescription { get; set; }
		public String detailDescription { get; set; }
        public Date transactionDate { get; set; }
        public String transactionDateReadable { get; set; }
        public Date exportDate { get; set; }
        public Decimal amount { get; set; }
        public Decimal debit { get; set; }
        public Decimal credit { get; set; }
        public String glAccount { get; set; }
        public String eightString { get; set; }

		public String rowTypeFormatted { get { return padspace(rowType, 7) ; } }
		public String exportNumberFormatted { get { return padspace(exportNumber, 8) ; } }
		public String batchNumberFormatted { get { return padspace(batchNumber, 20) ; } }
		public String jvStringFormatted { get { return padspace(jvString, 3) ; } }
		public String sessionDescriptionFormatted { get { return padspace(sessionDescription, 32) ; } }
		public String headerDescriptionFormatted { get { return padspace(headerDescription, 51) ; } }
		public String detailDescriptionFormatted { get { return padspace(detailDescription, 60) ; } }
        public String transactionDateFormatted { get { return formatdate(transactionDate, 8) ; } }
        public String exportDateFormatted { get { return formatdate(exportDate, 8) ; } }
        public String debitFormatted { get { return padcurrency(debit, 14) ; } }
        public String creditFormatted { get { return padcurrency(credit, 14) ; } }
        public String glAccountFormatted { get { return formataccount(glAccount, 26) ; } }

    }

	//string formatting methods
	private static String padspace (String s, Integer length) {
		return s.rightPad(length);
	}

	private static String formatdate (Date d, Integer length) {
		return zeropad(string.valueof(d.month()), 2) + zeropad(string.valueof(d.day()), 2) + string.valueof(d.year());
	}

	private static String readabledate (Date d) {
		return zeropad(string.valueof(d.month()), 2) + '/' + zeropad(string.valueof(d.day()), 2) + '/' + string.valueof(d.year());
	}

	private static String padcurrency (Decimal d, Integer length) {
		return zeropad(string.valueof(d).replace('.', ''), length);
	}

	private static String zeropad (String s, Integer length) {
		String result = '';
		for (Integer i = 0; i < length - s.length(); i++) {
			result += '0';
		}
		return result + s; 
	}

	private static String formataccount (String s, Integer length) {
		return s.replace(' ', '');
	}
    
    /**
     * @description Returns the Component Output Panel of the Batch Export Preview View for the Set of Batch Ids passed in.
     * @param Set<Id> Set of Batch Id records
     * @return Component.Apex.OutputPanel
     */
    global Component.Apex.OutputPanel getPreviewComponent(Set<Id> batchesToExportIds){
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        Component.Apex.PageBlock detailPB = new Component.Apex.PageBlock();
        Component.Apex.PageblockTable previewTable = new Component.Apex.PageblockTable();

        List<Row> previewRows = getRowsFromBatches(batchesToExportIds);

        detailPB.title = 'Export File';
        previewTable.value = previewRows;
        previewTable.var = 'row';

        addPreviewColumn(previewTable, '{!row.batchNumber}', 'Batch #');
        addPreviewColumn(previewTable, '{!row.detailDescription}', 'Description');
        addPreviewColumn(previewTable, '{!row.transactionDateReadable}', 'Date');
        Component.Apex.Column debitColumn = addPreviewColumn(previewTable, '{!row.debit}', 'Debit');
        rightAlignColumn(debitColumn);
        Component.Apex.Column creditColumn = addPreviewColumn(previewTable, '{!row.credit}', 'Credit');
        rightAlignColumn(creditColumn);
        addPreviewColumn(previewTable, '{!row.glAccount}', 'GL Account');
        
        detailPB.childComponents.add(previewTable);
        op.ChildComponents.add(detailPB);
        return op;
    }

    /**
     * @description Performs the export on the specified Set of Batch Ids and returns a BatchExportResult.
     * If no Ids are passed in, the BatchExportResult returned will contain an error message.
     * @param Set<Id> Set of Batch Id records
     * @return a BatchExportResult
     */
    global NU.BatchExportResult export(Set<Id> batchesToExportIds){
        NU.BatchExportResult ber = new NU.BatchExportResult();
        
        if (NU.CollectionUtil.setHasElements(batchesToExportIds) == false){
            NU.OperationResult.addErrorMessage(ber.Result, NO_BATCH_IDS_TO_EXPORT_ERR_MSG);
            return ber;
        }
        
        String detailFileContents = getDetailFileContents(batchesToExportIds);
        insertExportDocuments(detailFileContents, batchesToExportIds);
        
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        Component.Apex.PageMessage pm = new Component.Apex.PageMessage();
        Component.Apex.PageBlock exportFilesPB = new Component.Apex.PageBlock();
        
        pm.Summary = 'The batch(es) were successfully exported.';
        pm.strength = 2;
        pm.severity = 'Info';
        op.ChildComponents.add(pm);
        exportFilesPB.title = 'Sage MIP Export File';
        
        Component.Apex.OutputLink detailOL = new Component.Apex.OutputLink();

        detailOL.value = '/' + detailDocument.Id;
        detailOL.target = '_blank';
        
        Component.Apex.OutputText detailOT = new Component.Apex.OutputText();
        detailOT.value = 'Export File';
        
        detailOL.ChildComponents.add(detailOT);
        
        op.ChildComponents.add(exportFilesPB);
        
        exportFilesPB.ChildComponents.add(detailOL);
        
        ber.SuccessView = op;
        ber.Result = new NU.OperationResult();
        
        return ber;
    }

    private void rightAlignColumn(Component.Apex.Column column){
        column.headerStyle = 'text-align: right;';
        column.style = column.headerStyle;
    }
    
    private Component.Apex.Column addPreviewColumn(Component.Apex.PageBlockTable previewTable, String columnExpression, String headerValue){
        Component.Apex.Column column = new Component.Apex.Column();
        column.expressions.value = columnExpression;
        
        if (NU.StringUtil.isNullOrEmpty(headerValue) == false){
            column.headerValue = headerValue;
        }

        previewTable.childComponents.add(column);
        
        return column;
    }
    
    private List<Row> getRowsFromBatches(Set<Id> batchIds){
        List<Row> rows = new List<Row>();
        
        List<NU__Transaction__c> trans =
        [select id,
                name,
                NU__Batch__c,
                NU__Batch__r.Name,
                NU__Date__c,
                NU__GLAccount__c,
                NU__GLAccount__r.Name,
                NU__Amount__c,
                NU__Entity__c,
                NU__Entity__r.Name,
                NU__Customer__r.Name,
                NU__OrderItemLine__r.NU__Product__r.RecordType.Name,
                NU__OrderItemLine__r.NU__Product__r.Name,
                NU__OrderItemLine__r.NU__Product__r.NU__ShortName__c,
                NU__OrderItemLine__r.NU__Product__r.NU__Event__r.NU__ShortName__c,
                NU__Type__c,
                NU__ProductCode__c,
                NU__PaymentLine__r.NU__Payment__r.NU__EntityPaymentMethod__r.NU__PaymentMethod__r.Name,
                NU__Category__c
           from NU__Transaction__c
          where NU__Batch__c in :batchIds
          order by NU__Date__c, NU__Batch__r.Name, NU__GLAccount__r.Name, name];
        
        rows = buildRows(trans);
          
        return rows;
    }

	//build the session row
	private Row buildSessionRow (NU__Transaction__c lineItem)
	{
		exportNumber = lineItem.NU__Batch__r.Name;
		
        Row row = new Row();
        row.rowType = 'HSESSN';
        row.exportNumber = exportNumber;
        row.jvString = 'JV';
        row.exportDate = Date.today();
        row.sessionDescription = 'BPGT Exp #' + row.exportNumber + ' on ' + readabledate(row.exportDate);
        row.detailDescription = 'Start of export file';
		return row;
	}

	//build a single header row
	private Row buildHeaderRow (NU__Transaction__c lineItem)
	{
        Row row = new Row();
        row.rowType = 'HDOC';
        row.exportNumber = exportNumber;
        row.batchNumber = lineItem.NU__Batch__r.Name;
        row.jvString = 'JV';
        row.headerDescription = 'Txns summarized for Data Entry Session ' + row.batchNumber;
        row.eightString = '!        8';
        row.transactionDate = lineItem.NU__Date__c;
        row.transactionDateReadable = readabledate(row.transactionDate);
        row.detailDescription = 'Start of batch';
		return row;
	}

	//build a single detail row
	private Row buildDetailRow (NU__Transaction__c lineItem)
	{
        Row row = new Row();
        row.rowType = 'DDOC';
        row.exportNumber = exportNumber;
        row.batchNumber = lineItem.NU__Batch__r.Name;
        row.jvString = 'JV';
        row.detailDescription = 'Transactions summarized by Data Entry Session and accounting';
        row.transactionDate = lineItem.NU__Date__c;
        row.transactionDateReadable = readabledate(row.transactionDate);
        row.amount = lineItem.NU__Amount__c;
        if (lineItem.NU__Category__c == TRANSACTION_CATEGORY_CREDIT){
            row.amount = lineItem.NU__Amount__c * -1;    
        }
        row.glAccount = lineItem.NU__GLAccount__r.Name;
		return row;
	}

	//build the rows for the export file
    public List<Row> buildRows(List<NU__Transaction__c> trans)
    {
        List<Row> myrows = new List<Row>();

        for (NU__Transaction__c lineItem : trans)
        {
			//if this is the first row in the export, build and add the session row first
			if (myrows.size() == 0)
			{
				Row sessionRow = BuildSessionRow(lineItem);
		        myrows.add(sessionRow);
			}

			Row Detailrow = BuildDetailRow(lineItem);
			
			//if this is the first row of a new batch/date, build and add the header row first
			Boolean foundHeader = false;
            for (Row r : myrows)
            {
                if (
                    r.transactionDate == detailRow.transactionDate &&
                    r.batchNumber == detailRow.batchNumber)
                {
                    foundHeader = true;
                    break;
                }
            }
            if (!foundHeader)
            {
				Row headerRow = BuildHeaderRow(lineItem);
                myrows.add(headerRow);
            }

            //if there is already a matching detail row, just combine them
            Boolean foundDetail = false;
            for (Row r : myrows)
            {
                if (
                    r.transactionDate == detailRow.transactionDate &&
                    r.batchNumber == detailRow.batchNumber &&
                    r.glAccount == detailRow.glAccount &&
                    r.detailDescription == detailRow.detailDescription)
                {
                    foundDetail = true;
                    r.amount += detailRow.amount;
                    break;
                }
            }

            //otherwise, add the detail row
            if (!foundDetail)
            {
                myrows.add(detailRow);
            }
        }

        //remove empty rows, split value into debit/credit
        for (Integer i = 0; i < myrows.size(); i++)
        {
            Row r = myrows[i];
            if (r.rowType == 'DDOC') {
	            if (r.amount == 0)
	            {
	                myrows.remove(i);
	                i--;
	            }
	            else
	            {
	                if (r.amount > 0)
	                {
	                    r.debit = r.amount;
	                    r.credit = 0;
	                }
	                else
	                {
	                    r.debit = 0;
	                    r.credit = -r.amount;
	                }
	            }
            }
        }
        
        return myrows;
    }

	//create the formatted output for export
    private String getDetailFileContents(Set<Id> batchesToExportIds){

        String dr_output = '';
        String cr_output = '';

        List<Row> exportRows = getRowsFromBatches(batchesToExportIds);
        String body = '';

        Integer scale = NU__Transaction__c.NU__Amount__c.getDescribe().getScale();

        for (Row r : exportRows){
            
			if (r.rowType == 'HSESSN')
			{
	            body += 
	              r.rowTypeFormatted +
	              r.exportNumberFormatted +
	              r.sessionDescriptionFormatted +
	              r.exportDateFormatted +
	              r.jvStringFormatted +
	              NEWLINE;
			}              

			if (r.rowType == 'HDOC')
			{
	            body += 
	              r.rowTypeFormatted +
	              r.exportNumberFormatted +
	              r.batchNumberFormatted +
	              r.jvStringFormatted +
	              r.transactionDateFormatted +
	              r.headerDescriptionFormatted +
	              r.eightString +
	              NEWLINE;
			}              

			if (r.rowType == 'DDOC')
			{
	            body += 
	              r.rowTypeFormatted +
	              r.exportNumberFormatted +
	              r.batchNumberFormatted +
	              r.jvStringFormatted +
	              r.detailDescriptionFormatted +
	              r.transactionDateFormatted +
	              r.debitFormatted +
	              r.creditFormatted +
	              r.glAccountFormatted +
	              NEWLINE;
			}              

        }

        return body;
    }
    
    private void insertExportDocuments(String detailContents, Set<Id> batchesToExportIds){
        Folder exportFolder = [select id from Folder where name = :EXPORT_FOLDER_NAME];

	    List<NU__Batch__c> SelectedBatches  = getSObjectsByIdsQuery(batchesToExportIds);

	    String FileNameBase = 'GLExport-' + SelectedBatches[0].NU__Entity__r.Name + '-' +  DateTime.now().format().replace(' ', '-');
	
	    String DetailFileName = FileNameBase + 'sf_export.txt';

        String txtContentType = 'text/plain';
    
        detailDocument = new Document(
            Body = Blob.valueOf(detailContents),
            ContentType = txtContentType,
            Name = DetailFileName,
            FolderId = exportFolder.Id
        );
        
        List<Document> exportDocs = new List<Document>{ detailDocument };

        insert exportDocs;
    }
    
    public List<SObject> getSObjectsByIdsQuery(Set<Id> ids) {
        return 
         [select
                Name,
                NU__Entity__r.Name,
                Id
           from NU__Batch__c
          where Id in :ids];
    }
}