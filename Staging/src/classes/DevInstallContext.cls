global class DevInstallContext implements InstallContext {
	global Id organizationId(){
		return null;
	}
	
	global Id installerId(){
		return null;
	}
	
	global Boolean isUpgrade(){
		return false;

	}
	
	global Boolean isPush(){
		return false;
	}
	
	global Version previousVersion(){
		return new Version(1, 2, 0);
	}
}