/**
 * @author NimbleUser
 * @date Updated: 2/18/13
 * @description This class tests the JointStateProviderBiller class.
 */
@isTest
public class TestJointStateProviderBiller {	
	static void assertJointBillingValidation(Joint_Billing__c jb, String expectedValidationMessage){
		Boolean exceptionThrown = false;
		String validationMessage = '';
		
		try{
			JointStateProviderBiller jspb = new JointStateProviderBiller(jb);
			
			Test.startTest();
			
			Database.executeBatch(jspb);
			
			Test.stopTest();
		}
		catch(ApplicationException ae){
			exceptionThrown = true;
			validationMessage = ae.getMessage();
		}
		catch(DMLException ex){
			exceptionThrown = true;
			validationMessage = ex.getMessage();
		}
		
		system.assertEquals(true, exceptionThrown, 'No validation exception thrown.');
		system.assertEquals(expectedValidationMessage, validationMessage);
	}
	
	static void assertJointStateProviderBillRun(Account stateProvider, List<Account> stateProviderAccounts, NU__MembershipType__c jointStateProviderMT, Joint_Billing__c jointBilling){
		system.assert(stateProvider != null, 'The state provider is null.');
		system.assert(jointStateProviderMT != null, 'The joint state provider membership type is null.');
		system.assert(jointBilling != null, 'The joint billing is null.');
		system.assert(NU.CollectionUtil.listHasElements(stateProviderAccounts), 'No state provider accounts given.');

		Account stateProviderQueried = [select id, name from account where id = :stateProvider.Id];
		
		NU__MembershipType__c jointStateProviderMTQueried = [select id, name from NU__MembershipType__c where id = :jointStateProviderMT.Id];
		
		List<Joint_Billing__c> stateProviderJointBillings = [select id, name, Start_Date__c, End_Date__c, Bill_Date__c from Joint_Billing__c where State_Partner__c = :stateProvider.Id];
		system.assertEquals(1, stateProviderJointBillings.size());
		
		Joint_Billing__c stateProviderJointBillingQueried = stateProviderJointBillings[0];

		Map<Id, Account> stateProviderAccountsMap = new Map<Id, Account>(
			[select id,
			        name,
			        Provider_Membership__c,
			        Program_Service_Revenue__c,
			        Revenue_Year_Submitted__c,
			        Dues_Price__c,
			        Multi_Site_Program_Service_Revenue__c,
			        Multi_Site_Dues_Price__c,
			        RecordTypeId
			   from Account
			  where id in :stateProviderAccounts]
		);
		
		List<Joint_Billing_Item__c> billingItems = [ 
			Select id,
			       name,
			       Account__c,
			       Amount__c,
			       (Select id,
			               name,
			               Amount__c,
			               Date__c,
			               Dues_Price__c,
			               Program_Service_Revenue__c,
			               Revenue_Year_Submitted__c
			          from Joint_Billing_Item_Lines__r)
			  from Joint_Billing_Item__c
			 where Joint_Billing__c = :stateProviderJointBillingQueried.Id
		];
		
		Map<Object, List<Joint_Billing_Item__c>> accountBillingItemsMap = NU.CollectionUtil.groupSObjectsByField(billingItems, 'Account__c');
		
		for (Account stateProviderAccount : stateProviderAccounts){
			Account stateProviderAccountQueried = stateProviderAccountsMap.get(stateProviderAccount.Id);
			
			system.assert(stateProviderAccountQueried != null);
			system.assert(stateProviderAccountQueried.Provider_Membership__c != null);
			
			List<Joint_Billing_Item__c> accountBillingItems = accountBillingItemsMap.get(stateProviderAccount.Id);

			system.assertEquals(1, NU.CollectionUtil.getlistCount(accountBillingItems));

			Joint_Billing_Item__c accountBillingItem = accountBillingItems[0];
			
			system.assert(accountBillingItem.Amount__c > 0);
			
			system.assertEquals(1, NU.CollectionUtil.getListCount(accountBillingItem.Joint_Billing_Item_Lines__r));
			
			Joint_Billing_Item_Line__c itemLine = accountBillingItem.Joint_Billing_Item_Lines__r[0];
			
			system.assert(itemLine.Amount__c > 0);
			system.assertEquals(jointBilling.Bill_Date__c, itemLine.Date__c);
			
			system.assertEquals(stateProviderAccountQueried.Revenue_Year_Submitted__c, itemLine.Revenue_Year_Submitted__c);
			
			if (stateProviderAccountQueried.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID){
				system.assertEquals(stateProviderAccountQueried.Program_Service_Revenue__c, itemLine.Program_Service_Revenue__c);
				system.assertEquals(stateProviderAccountQueried.Dues_Price__c, itemLine.Dues_Price__c);
			}
			else if (stateProviderAccountQueried.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
				system.assertEquals(stateProviderAccountQueried.Multi_Site_Program_Service_Revenue__c, itemLine.Program_Service_Revenue__c);
				system.assertEquals(stateProviderAccountQueried.Multi_Site_Dues_Price__c, itemLine.Dues_Price__c);
			}
		}
	}
	
	static void assertNonCorporateMSONotBilled(Id nonCorporateMSOId){
		List<Joint_Billing__c> jbs = [select id, name from Joint_Billing__c where State_Partner__c = :nonCorporateMSOId];
		
		system.assertEquals(0, jbs.size(), 'The non corporate multi-site org has been billed (Joint Billing created) when it should not have been.');
		
		assertProviderNotBilled(nonCorporateMSOId);
	}
	
	static void assertProviderNotBilled(Id accountId){
		List<Joint_Billing_Item__c> jbis = [select id, name from Joint_Billing_Item__c where Account__c = :accountId];
		
		system.assertEquals(0, jbis.size(), 'The non corporate multi-site org has been billed (Joint Billing Item created) when it should not have been.');
	}
	
	public static void assertCorporateProvidersHaveMemberships(List<Account> corporateProviders, Account corporateMSO){
		NU__Membership__c corporateMSOMembership =
		[select id,
		        name,
		        NU__StartDate__c,
		        NU__EndDate__c,
		        NU__MembershipType__c
		   from NU__Membership__c
		  where NU__Account__c = :corporateMSO.Id];
		
		List<Account> corporateProvidersQueried = 
		[select id,
		        name,
		        Provider_Membership__c,
		        Provider_Membership__r.Id,
		        Provider_Membership__r.NU__StartDate__c,
		        Provider_Membership__r.NU__EndDate__c,
		        Provider_Membership__r.NU__MembershipType__c,
		        Provider_Membership__r.NU__ExternalAmount__c,
		        NU__PrimaryAffiliation__c
		   from Account
		  where id in :corporateProviders];
		  
		for (Account corporateProvider : corporateProvidersQueried){
			system.assert(corporateProvider.Provider_Membership__c != null, 'The corporate provider\'s membership is null.');
			system.assert(corporateProvider.Provider_Membership__c != corporateMSOMembership.Id, 'The corporate provider\'s is linked to the mso membership when it should not be.');
			system.assertEquals(corporateMSOMembership.NU__StartDate__c, corporateProvider.Provider_Membership__r.NU__StartDate__c, 'The corporate provider\'s start date differs from the mso\'s membership start date.');
			system.assertEquals(corporateMSOMembership.NU__EndDate__c, corporateProvider.Provider_Membership__r.NU__EndDate__c, 'The corporate provider\'s end date differs from the mso\'s membership end date.');
			system.assertEquals(corporateMSOMembership.NU__MembershipType__c, corporateProvider.Provider_Membership__r.NU__MembershipType__c, 'The corporate provider\'s membership type differs from the mso\'s membership membership type.');
			system.assertEquals(0, corporateProvider.Provider_Membership__r.NU__ExternalAmount__c, 'The corporate provider\'s membership external amount is not zero.');
		}
	}
	
	public static Joint_Billing__c runJointBilling(Id statePartnerId, Date endDate){
		return runJointBilling(statePartnerId, endDate, null);
	}
	
	static Joint_Billing__c runJointBilling(Id statePartnerId, Date endDate, Date joinDate){	
		List<Joint_Billing__c> jbs = runJointBillings(new Set<Id>{ statePartnerId }, endDate, joinDate);
    	
    	return jbs[0];
	}
	
	static List<Joint_Billing__c> runJointBillings(Set<Id> statePartnerIds, Date endDate){
		return runJointBillings(statePartnerIds, endDate, null);
	}
	
	static List<Joint_Billing__c> runJointBillings(Set<Id> statePartnerIds, Date endDate, Date joinDate){
		List<Joint_Billing__c> jointBillings = new List<Joint_Billing__c>();
		
		Test.startTest();
		
		JointBillingUtil.ProviderJoinOnToUseForTesting = joinDate;
		
		for (Id statePartnerId : statePartnerIds){
			Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling(statePartnerId, endDate);
			jointBillings.add(jb);
			
	    	JointStateProviderBiller jspb = new JointStateProviderBiller(jb);
	    	
	    	Database.executeBatch(jspb);
		}
		
		
		Test.stopTest();
		
		return jointBillings;
	}
	
	static Joint_Billing__c findJointBilling(List<Joint_Billing__c> jointBillings, Id stateAssociationId){
		for (Joint_Billing__c jointBilling : jointBillings){
			if (jointBilling.State_Partner__c == stateAssociationId){
				return jointBilling;
			}
		}
		
		return null;
	}
	
	static Date calculatePreviousYearEndDate(){
		return Date.newInstance(Date.Today().year() - 1, 12, 31);
	}

    static testMethod void nullJointBillingValidationTest(){
        assertJointBillingValidation(null, JointStateProviderBiller.NULL_JOINT_BILLING_VAL_MSG);
    }
    
    static testMethod void JointBillingAlreadyRunValidationTest(){
    	Joint_Billing__c jb = DataFactoryJointBilling.insertJointBilling();
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_ALREADY_RUN_VAL_MSG);
    }
    
    static testmethod void JointBillingBillDateRequiredTest(){
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.Bill_Date__c = null;
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_BILL_DATE_REQUIRED_VAL_MSG);
    }
    
    static testmethod void JointBillingStartDateRequiredTest(){
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.Start_Date__c = null;
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_START_DATE_REQUIRED_VAL_MSG);
    }
    
    static testmethod void JointBillingEndDateRequiredTest(){
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.End_Date__c = null;
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_END_DATE_REQUIRED_VAL_MSG);
    }
    
    static testmethod void NoStatePartnerValMsgTest(){
    	NU__MembershipType__c providerMT = DataFactoryMembershipTypeExt.insertProviderMembershipType(); 
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.State_Partner__c = providerMT.Id;
    	
    	String noStatePartnerValMsg = JointStateProviderBiller.getNoStatePartnerValMsg(jb);
        assertJointBillingValidation(jb, noStatePartnerValMsg);
    }
    
    static testmethod void StatePartnerAccountChosenValidationTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
    	Account providerAccountQueried = JointStateProviderBiller.getStateProviderAccount(providerAccount.Id);
    	
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.State_Partner__c = providerAccount.Id;
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.getNotStateProviderValMsg(providerAccountQueried));
    }
    
    static testmethod void StartDateNotFirstOfMonthValidationTest(){    	
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.Start_Date__c = Date.newInstance(2012, 1, 2);
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_START_DAY_NOT_FIRST_OF_MONTH_VAL_MSG);
    }
    
    static testmethod void EndDateNotLastDayOfMonthValidationTest(){
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.End_Date__c = jb.End_Date__c.addDays(-5);
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_END_DATE_NOT_LAST_DAY_OF_MONTH_VAL_MSG);
    }
    
    static testmethod void NewStartDateBeforeCurrentEndDateValidationTest(){
    	Joint_Billing__c jb = DataFactoryJointBilling.createJointBilling();
    	
    	jb.End_Date__c = jb.Start_Date__c.addmonths(2).addDays(-1);
    	
        assertJointBillingValidation(jb, JointStateProviderBiller.JOINT_BILLING_START_DATE_NOT_AFTER_END_DATE_VAL_MSG);
    }

    static testmethod void singleRunTest(){
    	NU__MembershipType__c jointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
    	
    	Account statePartner = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account providerAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, statePartner.Id);
    	
    	NU__Membership__c jointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
    	
    	Joint_Billing__c jb = runJointBilling(statePartner.Id, jointStateProviderMembership.NU__EndDate__c);
    	
    	assertJointStateProviderBillRun(statePartner, new List<Account>{ providerAccount }, jointStateProviderMT, jb);
    }

    static testmethod void newProviderWithExistingProviderTest(){
    	NU__MembershipType__c jointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
    	
    	Account statePartner = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account existingProviderAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, statePartner.Id);
    	Account newProviderAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(1000000, statePartner.Id);
    	
    	NU__Membership__c jointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(existingProviderAccount.Id, jointStateProviderMT);
    	
    	Joint_Billing__c jb = runJointBilling(statePartner.Id, jointStateProviderMembership.NU__EndDate__c);
    	
    	assertJointStateProviderBillRun(statePartner, new List<Account>{ existingProviderAccount, newProviderAccount }, jointStateProviderMT, jb);
    }
    
    static testmethod void billJointlyMultiSiteOnlyInsertValidationTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(null);
    	providerAccount.Multi_Site_Bill_Jointly__c = true;    	
    	
    	TestUtil.assertInsertValidationRule(providerAccount, Constant.BILL_JOINTLY_MULTISITE_ONLY_VAL_RULE_MSG);
    }
    
    static testmethod void billJointlyMultiSiteOnlyUpdateValidationTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(null);
    	insert providerAccount;
    	
    	providerAccount.Multi_Site_Bill_Jointly__c = true;
    	
    	TestUtil.assertUpdateValidationRule(providerAccount, Constant.BILL_JOINTLY_MULTISITE_ONLY_VAL_RULE_MSG);
    }

    static testmethod void newMultiSiteOrgBilledJointlyTest(){
    	NU__MembershipType__c jointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
    	
    	Account multiSiteOrg = DataFactoryAccountExt.insertMultiSiteWithJointBillingAccount(500000);
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(multiSiteOrg.Multi_Site_Program_Service_Revenue__c);
    	
    	NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, multiSiteOrg.Id, true);
    	
    	Date endDate = calculatePreviousYearEndDate();
    	
    	Joint_Billing__c jb = runJointBilling(multiSiteOrg.Id, endDate);
    	
    	assertJointStateProviderBillRun(multiSiteOrg, new List<Account>{ multiSiteOrg }, jointStateProviderMT, jb);
    }
    
    // This test asserts that when a Non-Corporate MSO is billed jointly,
    // each provider belonging to the MSO has its corresponding State Association
    // billed and not the MSO for a new MSO. This is a result of the State Partner and MSO's
    // basecamp discussion at 
    // https://basecamp.com/1771424/projects/763396-leadingage-nimble/messages/8666616-state-partners-and
    static testmethod void newNonCorporateMultiSiteOrgNotBilledButProvidersAreBilledTest(){
    	NU__MembershipType__c jointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
    	
    	Account nonCorporateMSO = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	Account statePartner1 = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account statePartner2 = DataFactoryAccountExt.insertStatePartnerAccount();
    	
    	Account provider1 = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(10000000, statePartner1.Id);
    	Account provider2 = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(1000000, statePartner2.Id);
    	
    	NU.DataFactoryAffiliation.insertAffiliation(provider1.Id, nonCorporateMSO.Id, true);
    	NU.DataFactoryAffiliation.insertAffiliation(provider2.Id, nonCorporateMSO.Id, true);
    	
    	Date endDate = calculatePreviousYearEndDate();
    	Date joinDate = endDate.addDays(30);
    	
    	List<Joint_Billing__c> jbs = runJointBillings(new Set<Id>{ statePartner1.Id, statePartner2.Id }, endDate, joinDate);
    	
    	Joint_Billing__c provider1JB = findJointBilling(jbs, statePartner1.Id);
    	
    	assertJointStateProviderBillRun(statePartner1, new List<Account>{ provider1 }, jointStateProviderMT, provider1JB);
    	
    	Joint_Billing__c provider2JB = findJointBilling(jbs, statePartner2.Id);
    	
    	assertJointStateProviderBillRun(statePartner2, new List<Account>{ provider2 }, jointStateProviderMT, provider2JB);
    	
    	assertNonCorporateMSONotBilled(nonCorporateMSO.Id);
    }
    
    static testmethod void cancelledNonCorporateMSONotBilledAndProviderNotBilledTest(){
    	NU__MembershipType__c jointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
    	
    	Account statePartner = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account nonCorporateMSO = DataFactoryAccountExt.insertMultiSiteAccount();
    	Account providerAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, statePartner.Id);
    	
    	NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, nonCorporateMSO.Id, true);
    	
    	NU__Membership__c jointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
    	
    	providerAccount.NU__Status__c = Constant.ACCOUNT_STATUS_INACTIVE;
    	update providerAccount;
    	
    	Joint_Billing__c jb = runJointBilling(statePartner.Id, jointStateProviderMembership.NU__EndDate__c);
    	
    	assertNonCorporateMSONotBilled(nonCorporateMSO.Id);
    	
    	assertProviderNotBilled(providerAccount.Id);
    }

	static testmethod void newCorporateMSOWithCorporateProvidersBilledTest(){
		NU__MembershipType__c jointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
    	
    	Account statePartner = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account corporateMSO = DataFactoryAccountExt.insertCorporateMultiSiteAccount(100000, statePartner.Id);
    	
    	List<Account> corporateProviders = DataFactoryAccountExt.insertMultiSiteEnabledProviderAccounts(new List<Decimal>{ 500000, 500000 }, corporateMSO.Id);
    	
    	Date endDate = calculatePreviousYearEndDate();
    	
    	Joint_Billing__c jb = runJointBilling(statePartner.Id, endDate);
    	
    	assertJointStateProviderBillRun(statePartner, new List<Account>{ corporateMSO }, jointStateProviderMT, jb);
    	
    	assertCorporateProvidersHaveMemberships(corporateProviders, corporateMSO);
	}
}