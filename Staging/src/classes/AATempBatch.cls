global class AATempBatch implements Database.Batchable<SObject> {
	
	   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([select id, name,National_Provider_Id__c, Medicare_Id__c FROM account where National_Provider_Id__c != null]);
   }

   global void execute(Database.BatchableContext BC, List<SObject> batch){
      Set<Id> uq = new Set<Id>();
      for(SObject s : batch){
         Account so = (Account)s;
         uq.add(so.Id);
      }

      List<Account> sa = [SELECT id, National_Provider_Id__c, Medicare_Id__c FROM Account WHERE Id IN :uq];

      for(Account aq: sa) {
      	
      	if(aq.National_Provider_Id__c.length() < 10)
        	{
        	  aq.Medicare_Id__c = aq.National_Provider_Id__c;
        	   aq.National_Provider_Id__c = null ;
        	} 
      }

      update sa;
   }

   global void finish(Database.BatchableContext BC){

   }

}