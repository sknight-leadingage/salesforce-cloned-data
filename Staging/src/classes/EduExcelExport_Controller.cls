public with sharing class EduExcelExport_Controller extends EduReportsControllerBase {
    transient List<Conference_Session_Speaker__c> SpeakerListHolder { get;set; }
    transient List<Conference_Session__c> SessionListHolder { get;set; }
    transient List<CE_Category__c> CategoryListHolder { get;set; }
   
    public List<Conference_Session_Speaker__c> getSpeakerList() {
        if (SpeakerListHolder == null || SpeakerListHolder.size() == 0) {
            SetQuery();
        }
        return SpeakerListHolder;
    }

    public List<Conference_Session__c> getSessionList() {
        if (SessionListHolder == null || SessionListHolder.size() == 0) {
            SetQuery();
        }
        return SessionListHolder;
    }

    public List<CE_Category__c> getCategoryList() {
        return CategoryListHolder;
    }

    private string mtype{get;set;}
    public string getMType() {
        if (mtype == null || mtype == '') {
            mtype = ApexPages.CurrentPage().getParameters().get('mt');
            if (mtype == null) {
                mtype = '';
            }
        }
        return mtype;
    }
    public void setMType(string s) {
        mtype = s;
    }

    public override void SetPageType()
    {
        if (getMType() == 'mfl') {
            PageCType = 'contentType="application/vnd.ms-excel#EduEvalsMostFields.xls"';
        }
        else if (getMType() == 'ces') {
            PageCType = 'contentType="application/vnd.ms-excel#EduCEShowCare.xls"';
        }
        else if (getMType() == 'hub-av') {
            PageCType = 'contentType="application/vnd.ms-excel#EduAVRequestsAllFields.xls"';
        }
        else if (getMType() == 'hub-avc') {
            PageCType = 'contentType="application/vnd.ms-excel#EduAVRequestsComplete.xls"';
        }
        else if (getMType() == 'hub-sp') {
            PageCType = 'contentType="application/vnd.ms-excel#EduSpeakerProfileAllFields.xls"';
        }
        else if (getMType() == 'hub-spc') {
            PageCType = 'contentType="application/vnd.ms-excel#EduSpeakerProfileComplete.xls"';
        }
        else if (getMType() == 'hub-spce') {
            PageCType = 'contentType="application/vnd.ms-excel#EduSpeakerProfileCE.xls"';
        }
        else if (getMType() == 'hub-pmc') {
            PageCType = 'contentType="application/vnd.ms-excel#EduPresMaterialsComplete.xls"';
        }
    }
    
    public override string getPageTitle() {
        string sPT = '';
        if (RptRenderAs == '' && Conference != null) {
            if (getMType() == 'mfl') {
                sPT = Conference.Name + ': Evals - Most Fields';
            }
            else if (getMType() == 'ces') {
                sPT = Conference.Name + ': CE ShowCare';
            }
            else if (getMType() == 'hub-av') {
                sPT = Conference.Name + ': Audio Visual Requests: All Fields';
            }
            else if (getMType() == 'hub-avc') {
                sPT = Conference.Name + ': Audio Visual Requests: Complete/Incomplete';
            }
            else if (getMType() == 'hub-sp') {
                sPT = Conference.Name + ': Speaker Profile: All Fields';
            }
            else if (getMType() == 'hub-spc') {
                sPT = Conference.Name + ': Speaker Profile: Complete/Incomplete';
            }
            else if (getMType() == 'hub-spce') {
                sPT = Conference.Name + ': Speaker Profile CE Fields';
            }
            else if (getMType() == 'hub-pmc') {
                sPT = Conference.Name + ': Presentation Materials: Complete/Incomplete';
            }
        }
        return sPT;
    }
    
    public EduExcelExport_Controller() {
        PageCType = '';
/*
        if (Test.isRunningtest()) {
            SetConPageSize = 12;
        } else {
            NU__Configuration__c itmVal = [SELECT NU__Value__c FROM NU__Configuration__c WHERE Name = 'SPDataPaging' LIMIT 1];
            SetConPageSize = integer.valueof(itmVal.NU__Value__c);
        }
*/
        SetQuery();
    }
    
/*
    public override void SetConRefreshData() {
        if (getMType() == 'hub-sp' || getMType() == 'hub-spc') {
            SpeakerListHolder = setCon.GetRecords();
        }
        else {
            SessionListHolder = setCon.GetRecords();
        }
    }
*/
    
    public void SetQuery() {
        if (getMType() == 'hub-spce') {
            CategoryListHolder = [SELECT Name FROM CE_Category__c ORDER BY Name];
        }
    
        SetConQuery = '';
        if (getMType() == 'mfl') {
            SetConQuery = 'SELECT Proposal__r.Proposal_Number__c, Education_Staff__c, Education_Staff__r.Name, Expected_Attendance__c, Actual_Attendance__c, Session_Number_Numeric__c,Title__c,Timeslot__r.Timeslot_Code__c,Timeslot__r.Timeslot__c,Learning_Objective_1__c,Learning_Objective_2__c,Learning_Objective_3__c,CA_RCFE__r.Name,Florida__r.Name,Kansas__r.Name,Missouri__r.Name,NAB__r.Name,NASBA__r.Name,PTF_Code__c,PTF_Hours__c,Conference_Track__r.Name, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\'),(SELECT Conference_Content_Category__r.Name FROM Conference_Content_Category_Sessions__r),(SELECT CE_Category__r.Name FROM CE_Conference_Sessions__r) FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        }
        else if (getMType() == 'ces') {
            SetConQuery = 'SELECT Session_Number_Numeric__c,Timeslot__r.Timeslot_Code__c,Title__c,Timeslot__r.Timeslot__c,PTF_Code__c,PTF_Hours__c,Kansas__r.Name,NASBA__r.Name,Missouri__r.Name,Scantron_Link_ID__c,Conference__r.NU__ShortName__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        }
        else if (getMType() == 'hub-av') {
            SetConQuery = 'SELECT Education_Staff__r.Name, Session_Number_Numeric__c,Title__c,Timeslot__r.Timeslot_Code__c,Conference_Track__r.Name,Expected_Attendance__c, Number_of_Speakers__c, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\'), Aisle_Microphone__c, Flipchart_and_Markers__c, No_Additional_AV__c, Other_Setup__c, Other_Setup_Checkbox__c, Session_AV_Modified__c, Session_AV_Validated__c, Wheelchair_Access_and_Ramp__c, Wireless_Microphone__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        }
        else if (getMType() == 'hub-avc') {
            SetConQuery = 'SELECT Session_Number_Numeric__c, Timeslot__r.Timeslot_Code__c, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\'), Session_AV_Modified__c, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        }
        else if (getMType() == 'hub-sp') {
            SetConQuery = 'SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail,Speaker__r.Knowledge_and_Professional_Experience__c,Speaker__r.Academic_Institution__c,Speaker__r.Academic_Institution_Year__c,Speaker__r.Highest_Degree_Level__c,Speaker__r.Major_Discipline__c,Speaker__r.No_Degree__c,Speaker__r.Speaker_Bio_Modified__c,Speaker__r.Speaker_Bio_Validated__c,Session__r.Session_Number_Numeric__c,Session__r.Title__c,Session__r.Timeslot__r.Timeslot_Code__c,Session__r.Timeslot__r.Timeslot__c,Session__r.Conference_Track__r.Name,Session__r.Education_Staff__c,Session__r.Education_Staff__r.Name FROM Conference_Session_Speaker__c WHERE (Role__c = \'Speaker\' OR Role__c = \'Key Contact\') AND Session__r.Conference__c = :EventId ORDER BY Speaker__r.LastName, Speaker__r.FirstName, Session__r.Session_Number_Numeric__c';
        }
        else if (getMType() == 'hub-spc') {
            SetConQuery = 'SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail,Speaker__r.Knowledge_and_Professional_Experience__c,Speaker__r.Academic_Institution__c,Speaker__r.Academic_Institution_Year__c,Speaker__r.Highest_Degree_Level__c,Speaker__r.Major_Discipline__c,Speaker__r.No_Degree__c,Speaker__r.Speaker_Bio_Modified__c,Speaker__r.Speaker_Bio_Validated__c,Session__r.Session_Number_Numeric__c,Session__r.Title__c,Session__r.Timeslot__r.Timeslot_Code__c,Session__r.Timeslot__r.Timeslot__c,Session__r.Conference_Track__r.Name,Session__r.Education_Staff__c,Session__r.Education_Staff__r.Name FROM Conference_Session_Speaker__c WHERE (Role__c = \'Speaker\' OR Role__c = \'Key Contact\') AND Session__r.Conference__c = :EventId ORDER BY Speaker__r.LastName, Speaker__r.FirstName, Session__r.Session_Number_Numeric__c';
        }
        else if (getMType() == 'hub-spce') {
            SetConQuery = 'SELECT Session_Number_Numeric__c,Title__c,Timeslot__r.Timeslot_Code__c,Timeslot__r.Timeslot__c,Learning_Objective_1__c,Learning_Objective_2__c,Learning_Objective_3__c,CA_RCFE__r.Name,Florida__r.Name,Kansas__r.Name,Activity_Workers_Certification__r.Name,CFRE_Certification__r.Name,NAB__r.Name,NASBA__r.Name,PTF_Code__c,PTF_Hours__c,Conference_Track__r.Name, (SELECT Speaker__r.PersonTitle,Speaker__r.Salutation,Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Company_Name_Rollup__c,Speaker__r.BillingCity,Speaker__r.BillingState,Speaker__r.PersonEmail,Speaker__r.Knowledge_and_Professional_Experience__c FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\'),(SELECT CE_Category__r.Name FROM CE_Conference_Sessions__r) FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        }
        else if (getMType() == 'hub-pmc') {
            SetConQuery = 'SELECT Session_Number_Numeric__c,Title__c,Timeslot__r.Timeslot_Code__c, (SELECT Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.PersonEmail FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\'), Session_Uploads_Modified__c, Session_Uploads_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c';
        }
        
        if (SetConQuery == '') {
            SetConQuery = 'SELECT Session_Number__c FROM Conference_Session__c WHERE Conference__c = :EventId LIMIT 1'; //Dummy query to prevent errors
        }
        
        if (getMType() == 'hub-sp' || getMType() == 'hub-spc') {
            SpeakerListHolder = database.query(SetConQuery);
        }
        else {
            SessionListHolder = database.query(SetConQuery);
        }
/*
        setCon.SetPageSize(SetConPageSize);
        setCon.First();
        SetConRefreshData();
*/
    }
}