public with sharing class TestAssociateMembershipInserter implements ITestMembershipInserter {
	public NU__Membership__c insertMembership(Id accountId){
		return DataFactoryMembershipExt.insertCurrentAssociateMembership(accountId);
	}
}