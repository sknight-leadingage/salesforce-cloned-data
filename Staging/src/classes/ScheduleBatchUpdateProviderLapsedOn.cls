global class ScheduleBatchUpdateProviderLapsedOn implements Schedulable
{
	global void execute(SchedulableContext ctx) 
	{
		BatchUpdateProviderLapsedOn batchProLapsed = new BatchUpdateProviderLapsedOn();
		ID batchprocessid = Database.executeBatch(batchProLapsed,75);
	}   
}