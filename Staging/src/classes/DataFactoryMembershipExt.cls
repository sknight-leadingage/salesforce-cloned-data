/**
 * @author NimbleUser
 * @date Updated: 3/7/13
 * @description This class provides various functions for creating and inserting Membershipships in addition
 * to those found in NU.DataFactoryMembership. It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Membershipships without inserting them. The insert* functions create Membershipships
 * and insert them.
 */
public with sharing class DataFactoryMembershipExt {
	private static final Integer CURRENT_YEAR = Date.Today().year();
	private static final Integer FIRST_DAY_OF_THE_MONTH = 1;
	
	private class MembershipTerm {
		public Date StartDate;
		public Date EndDate;
		
		public MembershipTerm(NU__MembershipType__c membershipType){

			Integer annualStartMonth = Integer.valueOf(membershipType.NU__AnnualStartMonth__c);
			Integer term = Integer.valueOf(membershipType.NU__Term__c);
			
			StartDate = Date.NewInstance(CURRENT_YEAR, annualStartMonth, FIRST_DAY_OF_THE_MONTH);
			EndDate = startDate.addMonths(term).addDays(-1);
		}
	}
	
	public static NU__Membership__c createCurrentCalendarYearMembership(Id accountId, Id membershipTypeId){
		system.assert(accountId != null, 'The accountId is null.');
		system.assert(membershipTypeId != null, 'The membershipTypeId is null.');
		
		Date startDate = Date.NewInstance(CURRENT_YEAR, 1, 1);
		Date endDate = Date.NewInstance(CURRENT_YEAR, 12, 31);
		
		return NU.DataFactoryMembership.createMembership(accountId, membershipTypeId, startDate, endDate);
	}
	
	public static NU__Membership__c createMembership(Account jointBillingAccount, Joint_Billing__c jointBilling, NU__MembershipType__c jointStateProviderMembershipType){
		return createMembership(jointBillingAccount, jointBilling, jointStateProviderMembershipType, null);
	}
	
	public static NU__Membership__c createMembership(Account jointBillingAccount, Joint_Billing__c jointBilling, NU__MembershipType__c jointStateProviderMembershipType, Decimal externalAmount){
		Integer term = Integer.valueOf(jointStateProviderMembershipType.NU__Term__c);
		Date endDate = JointBilling.Start_Date__c.addMonths(term).addDays(-1);
		
		return new NU__Membership__c(
			NU__Account__c = jointBillingAccount.Id,
			NU__MembershipType__c = jointStateProviderMembershipType.Id,
			NU__StartDate__c = jointBilling.Start_Date__c,
			NU__EndDate__c = endDate,
			NU__ExternalAmount__c = externalAmount
		);
	}
	
	public static NU__Membership__c createMembership(Account jointBillingAccount, Joint_Billing__c jointBilling, NU__MembershipType__c jointStateProviderMembershipType, Decimal externalAmount, Date startDate, Date endDate){
		
		return new NU__Membership__c(
			NU__Account__c = jointBillingAccount.Id,
			NU__MembershipType__c = jointStateProviderMembershipType.Id,
			NU__StartDate__c = startDate,
			NU__EndDate__c = endDate,
			NU__ExternalAmount__c = externalAmount
		);
	}

	/**
	 * @description Creates a Provider Membership for the current year for the provided account.
	 * This inserts an entity and the provider membership type so it should only be used in test code.
	 * @return a current year Membership for the provided account that's not inserted. 
	 */
	public static NU__Membership__c createCurrentProviderMembership(Id accountId){
		NU__MembershipType__c providerMT = DataFactoryMembershipTypeExt.insertProviderMembershipType();
		
		return createCurrentProviderMembership(accountId, providerMT);
	}
	
	public static NU__Membership__c createCurrentProviderMembership(Id accountId, NU__MembershipType__c providerMembershipType){
		MembershipTerm memTerm = new MembershipTerm(providerMembershipType);
		
		NU__Membership__c providerMembership = NU.DataFactoryMembership.createMembership(accountId, providerMembershipType.Id, memTerm.StartDate, memTerm.EndDate);
		
		return providerMembership;
	}
	
	/**
	 * @description Inserts a Provider Membership for the current year for the provided account.
	 * This inserts an entity and the provider membership type so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account. 
	 */
	public static NU__Membership__c insertCurrentProviderMembership(Id accountId){
		NU__Membership__c currentProviderMembership = createCurrentProviderMembership(accountId);
		
		insert currentProviderMembership;
		return currentProviderMembership;
	}
	
	/**
	 * @description Inserts a Provider Membership for the current year for the provided account and provider membership type.
	 * This inserts an entity so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account and provider membership type. 
	 */
	public static NU__Membership__c insertCurrentProviderMembership(Id accountId, NU__MembershipType__c providerMembershipType){
		NU__Membership__c currentProviderMembership = createCurrentProviderMembership(accountId, providerMembershipType);
		
		insert currentProviderMembership;
		return currentProviderMembership;
	}
	
	/**
	 * @description Creates a CAST Membership for the current year for the provided account.
	 * This inserts an entity and the CAST membership type so it should only be used in test code.
	 * @return a current year Membership for the provided account that's not inserted. 
	 */
	public static NU__Membership__c createCurrentCASTMembership(Id accountId){
		NU__MembershipType__c castMT = DataFactoryMembershipTypeExt.insertCASTMembershipType();
		
		MembershipTerm memTerm = new MembershipTerm(castMT);
		
		NU__Membership__c castMembership = NU.DataFactoryMembership.createMembership(accountId, castMT.Id, memTerm.StartDate, memTerm.EndDate);
		
		return castMembership;
	}
	
	/**
	 * @description Inserts a CAST Membership for the current year for the provided account.
	 * This inserts an entity and the CAST membership type so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account. 
	 */
	public static NU__Membership__c insertCurrentCASTMembership(Id accountId){
		NU__Membership__c currentCastMembership = createCurrentCASTMembership(accountId);
		
		insert currentCastMembership;
		return currentCastMembership;
	}
	
	/**
	 * @description Creates a IAHSA Membership for the current year for the provided account.
	 * This inserts an entity and the IAHSA membership type so it should only be used in test code.
	 * @return a current year Membership for the provided account that's not inserted. 
	 */
	public static NU__Membership__c createCurrentIAHSAMembership(Id accountId){
		NU__MembershipType__c iahsaMT = DataFactoryMembershipTypeExt.insertIAHSAMembershipType();
		
		MembershipTerm memTerm = new MembershipTerm(iahsaMT);
		
		NU__Membership__c iahsaMembership = NU.DataFactoryMembership.createMembership(accountId, iahsaMT.Id, memTerm.StartDate, memTerm.EndDate);
		
		return iahsaMembership;
	}
	
	/**
	 * @description Inserts a IAHSA Membership for the current year for the provided account.
	 * This inserts an entity and the IAHSA membership type so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account. 
	 */
	public static NU__Membership__c insertCurrentIAHSAMembership(Id accountId){
		NU__Membership__c currentIAHSAMembership = createCurrentIAHSAMembership(accountId);
		
		insert currentIAHSAMembership;
		return currentIAHSAMembership;
	}
	
	/**
	 * @description Creates a IAHSA Membership for the current year for the provided account.
	 * This inserts an entity and the IAHSA membership type so it should only be used in test code.
	 * @return a current year Membership for the provided account that's not inserted. 
	 */
	public static NU__Membership__c createCurrentAssociateMembership(Id accountId){
		NU__MembershipType__c associateMT = DataFactoryMembershipTypeExt.insertAssociateMembershipType();
		
		MembershipTerm memTerm = new MembershipTerm(associateMT);
		
		NU__Membership__c associateMembership = NU.DataFactoryMembership.createMembership(accountId, associateMT.Id, memTerm.StartDate, memTerm.EndDate);
		
		return associateMembership;
	}
	
	/**
	 * @description Inserts a IAHSA Membership for the current year for the provided account.
	 * This inserts an entity and the IAHSA membership type so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account. 
	 */
	public static NU__Membership__c insertCurrentAssociateMembership(Id accountId){
		NU__Membership__c currentAssociateMembership = createCurrentAssociateMembership(accountId);
		
		insert currentAssociateMembership;
		return currentAssociateMembership;
	}
	
	/**
	 * @description Creates a Corporate Alliance Membership for the current year for the provided account.
	 * This inserts an entity and the Corporate Alliance membership type so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account. 
	 */
	public static NU__Membership__c createCurrentCorporateAllianceMembership(Id accountId){
		NU__MembershipType__c corporateAllianceMT = DataFactoryMembershipTypeExt.insertCorporateAllianceMembershipType();
		
		MembershipTerm memTerm = new MembershipTerm(corporateAllianceMT);
		
		system.assert(accountId != null, 'The accountId is null');
		system.assert(corporateAllianceMT.Id != null, 'The corporateAllianceMT.Id is null');
		system.assert(memTerm.StartDate != null, 'The memTerm.StartDate is null');
		system.assert(memTerm.EndDate != null, 'The memTerm.EndDate is null');
		
		NU__Membership__c corporateAllianceMembership = NU.DataFactoryMembership.createMembership(accountId, corporateAllianceMT.Id, memTerm.StartDate, memTerm.EndDate);
		
		return corporateAllianceMembership;
	}
	
	/**
	 * @description Inserts a Corporate Alliance Membership for the current year for the provided account.
	 * This inserts an entity and the Corporate Alliance membership type so it should only be used in test code.
	 * @return an inserted current year Membership for the provided account. 
	 */
	public static NU__Membership__c insertCurrentCorporateAllianceMembership(Id accountId){
		NU__Membership__c currentCorporateAllianceMembership = createCurrentCorporateAllianceMembership(accountId);
		
		insert currentCorporateAllianceMembership;
		return currentCorporateAllianceMembership;
	}
}