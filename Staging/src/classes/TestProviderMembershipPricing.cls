/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestProviderMembershipPricing {
	
	static NU__MembershipType__c getMembershipTypeById(Id membershipTypeId){
		return [select id,
		               name,
		               NU__Entity__c
		          from NU__MembershipType__c
		         where id = :membershipTypeId];
	}

    static testMethod void overridePriceTest() {
        Account providerAccount = DataFactoryAccountExt.createProviderAccount(800000);
        providerAccount.Dues_Price_Override__c = 2000;
        
        insert providerAccount;
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, providerAccount.Dues_Price_Override__c);
    }
    
    static testmethod void trialMembershipTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(800000);
        providerAccount.Trial_Membership__c = true;
        
        insert providerAccount;
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, 0);
    }
    
    static testmethod void underConstructionTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(800000);
        providerAccount.Under_Construction__c = true;
        
        insert providerAccount;
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, 350);
    }
    
    static testmethod void zeroProgramServiceRevenueTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(0);
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, 0);
    }
    
    static testmethod void threeHundredEightDuesPriceTest(){
    	Decimal desiredDuesPrice = 380;
    	Decimal threeHundredEightDuesProgramServiceRevenue = desiredDuesPrice / 0.0004;
    	
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(threeHundredEightDuesProgramServiceRevenue);
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, desiredDuesPrice);
    }
    
    static testmethod void fiveMillionProgramServiceRevenueTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(5000000);
        
        Decimal expectedProviderDuesPricing = (providerAccount.Program_Service_Revenue__c * 0.00035) + 50;
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, expectedProviderDuesPricing);
    }
    
    static testmethod void fifteenMillionProgramServiceRevenueTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
        
        Decimal expectedProviderDuesPricing = (providerAccount.Program_Service_Revenue__c * 0.00030) + 550;
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, expectedProviderDuesPricing);
    }
    
    static testmethod void minDuesPriceTest(){
    	Decimal minDuesProgramServiceRevenue = 200 / 0.0004;
    	
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(minDuesProgramServiceRevenue);
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, 350);
    }

    static testmethod void reinstatementAfterFiveYearsPriceTest(){
    	Decimal maxDuesPrice = ProviderMembershipPricer.BEFORE_2014_MAX_PROVIDER_DUES_PRICE;
    	Decimal maxProgramServiceRevenue = maxDuesPrice / 0.00030;
    	
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(maxProgramServiceRevenue);
    	
    	Integer numberOfYearsOld = -7;
    	
    	NU__MembershipType__c providerMT = DataFactoryMembershipTypeExt.insertProviderMembershipType();
    	
    	NU__Membership__c olderThan5YearsProviderMembership = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id, providerMT);
    	olderThan5YearsProviderMembership.NU__StartDate__c = Date.NewInstance(2006, 1, 1);
    	olderThan5YearsProviderMembership.NU__EndDate__c = Date.NewInstance(2006, 12, 31);
    	
    	insert olderThan5YearsProviderMembership;
    	
    	providerAccount.Provider_Lapsed_On__c = Date.NewInstance(2007, 1, 1);
    	update providerAccount;
    	
    	Decimal expectedProviderDuesPricing = maxDuesPrice * ProviderMembershipPricer.AFTER_FIVE_YEARS_REINSTATEMENT_DISCOUNT;
    	
    	Date startDate = Date.newInstance(2013, 1, 1);
    	Date endDate = Date.newInstance(2013, 12, 31);
    	
		TestMembershipPricingUtil.assertProviderPricingWithTerm(providerAccount, expectedProviderDuesPricing, providerMT, startDate, endDate, null);
    }

    static testmethod void reinstatementBeforeFiveYearsPriceTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(5000000);
    	
    	Integer numberOfYearsOld = -4;
    	
    	Decimal expectedProviderDuesPricing = (providerAccount.Program_Service_Revenue__c * 0.00035) + 50;
    	
    	NU__MembershipType__c providerMT = DataFactoryMembershipTypeExt.insertProviderMembershipType();
    	
    	NU__Membership__c olderThan5YearsProviderMembership = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id, providerMT);
    	olderThan5YearsProviderMembership.NU__StartDate__c = olderThan5YearsProviderMembership.NU__StartDate__c.addYears(numberOfYearsOld);
    	olderThan5YearsProviderMembership.NU__EndDate__c = olderThan5YearsProviderMembership.NU__EndDate__c.addYears(numberOfYearsOld);
    	
    	insert olderThan5YearsProviderMembership;
    	
    	providerAccount.Provider_Lapsed_On__c = Date.Today().addYears(numberOfYearsOld + 1);
    	update providerAccount;
    	
    	TestMembershipPricingUtil.assertProviderPricing(providerAccount, expectedProviderDuesPricing, providerMT);
    }

    static testmethod void noIncreaseTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(15000000);
    	providerAccount.Revenue_Year_Submitted__c = String.valueOf(Date.Today().Year() - 3);
    	
    	insert providerAccount;
        
        Decimal expectedProviderDuesPricing = (providerAccount.Program_Service_Revenue__c * 0.00030) + 550;
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, expectedProviderDuesPricing);
    }

    static testmethod void oneYearIncreaseTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(15000000);
    	Integer numberOfIncreaseYears = 1;
    	Integer currentYear = Date.Today().Year();
    	providerAccount.Revenue_Year_Submitted__c = String.valueOf(currentYear - (ProviderMembershipPricer.NUMBER_OF_YEARS_BEFORE_STARTING_DUES_INCREASING + numberOfIncreaseYears));
    	
    	insert providerAccount;
    	
    	NU__Membership__c lastYearProviderMembership = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id);
    	lastYearProviderMembership.NU__StartDate__c = Date.NewInstance(currentYear - 1, 1, 1);
    	lastYearProviderMembership.NU__EndDate__c = Date.NewInstance(currentYear - 1, 12, 31);
    	lastYearProviderMembership.NU__ExternalAmount__c = 1000;
    	
    	insert lastYearProviderMembership;
        
        Decimal expectedProviderDuesPricing = lastYearProviderMembership.NU__ExternalAmount__c * (1 + ProviderMembershipPricer.INCREASE_RATE);
        expectedProviderDuesPricing = expectedProviderDuesPricing.setScale(2);
        
        NU__MembershipType__c providerMT = getMembershipTypeById(lastYearProviderMembership.NU__MembershipType__c);
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, expectedProviderDuesPricing, providerMT);
    }
    
    static testmethod void twoIncreaseTest(){
    	Account providerAccount = DataFactoryAccountExt.createProviderAccount(15000000);
    	
    	Integer numberOfIncreaseYears = 2;
    	Integer currentYear = Date.Today().Year();
    	providerAccount.Revenue_Year_Submitted__c = String.valueOf(currentYear - (ProviderMembershipPricer.NUMBER_OF_YEARS_BEFORE_STARTING_DUES_INCREASING + numberOfIncreaseYears));
    	
    	insert providerAccount;
    	
    	NU__Membership__c lastYearProviderMembership = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id);
    	lastYearProviderMembership.NU__StartDate__c = Date.NewInstance(currentYear - 1, 1, 1);
    	lastYearProviderMembership.NU__EndDate__c = Date.NewInstance(currentYear - 1, 12, 31);
    	lastYearProviderMembership.NU__ExternalAmount__c = 1050;
    	
    	insert lastYearProviderMembership;
    	
    	NU__Membership__c twoYearsAgoProviderMembership = lastYearProviderMembership.clone(false,false,false,false);
    	twoYearsAgoProviderMembership.NU__StartDate__c = Date.NewInstance(currentYear - 2, 1, 1);
    	twoYearsAgoProviderMembership.NU__EndDate__c = Date.NewInstance(currentYear - 2, 12, 31);
    	twoYearsAgoProviderMembership.NU__ExternalAmount__c = 1000;
    	
    	insert twoYearsAgoProviderMembership;
    	
        
        Decimal expectedProviderDuesPricing = lastYearProviderMembership.NU__ExternalAmount__c * (1 + ProviderMembershipPricer.INCREASE_RATE);
        expectedProviderDuesPricing = expectedProviderDuesPricing.setScale(2);
        
        NU__MembershipType__c providerMT = getMembershipTypeById(lastYearProviderMembership.NU__MembershipType__c);
        
        TestMembershipPricingUtil.assertProviderPricing(providerAccount, expectedProviderDuesPricing, providerMT);
    }
    
    static testmethod void newMemberFirstHalfOfYearProratePriceTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
    	NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();
    	
    	Decimal expectedProviderDuesPricing = ((providerAccount.Program_Service_Revenue__c * 0.00030) + 550) * ProviderMembershipPricer.NEW_MEMBER_JOIN_DISCOUNT;
    	   	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = startDate.addDays(15);
    	
    	TestMembershipPricingUtil.assertProviderPricingWithTerm(providerAccount, expectedProviderDuesPricing, providerMT, startDate, endDate, joinDate);
    }
    
    static testmethod void newMemberSecondHalfOfYearProratePriceTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
    	NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();
    	
    	Decimal expectedProviderDuesPricing = ProviderMembershipPricer.NEW_MEMBER_JOIN_SECOND_HALF_OF_YEAR_PRICE;
    	   	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = startDate.addDays(200);
    	
    	TestMembershipPricingUtil.assertProviderPricingWithTerm(providerAccount, expectedProviderDuesPricing, providerMT, startDate, endDate, joinDate);
    }

    static testmethod void excludeServiceFeeTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
    	NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();

    	NU__Product__c membershipProduct = NU.DataFactoryProduct.createMembershipProduct('Provider Dues', 0.00, providerMT.NU__Entity__c);
    	insert membershipProduct;
        
        NU__MembershipTypeProductLink__c primaryMTPL = NU.DataFactoryMembershipTypeProductLink.createMembershipTypeProdLink(providerMT.Id, membershipProduct.Id);
    	primaryMTPL.Exclude_Provider_Service_Fee__c = true;
    	insert primaryMTPL;
    	
    	List<NU__MembershipTypeProductLink__c> mtpls = [select id, name, Exclude_Provider_Service_Fee__c from NU__MembershipTypeProductLink__c where NU__MembershipType__c = :providerMT.Id];
    	system.debug('   mtpls ' + mtpls);
    	
    	system.assert(mtpls.size() == 1, 'There are more than 1 primary mtpl');
    	
    	system.assertEquals(true, mtpls[0].Exclude_Provider_Service_Fee__c);
    	
    	Decimal expectedProviderDuesPricing = ((providerAccount.Program_Service_Revenue__c * 0.00030) + 550) * (1 - Constant.PROVIDER_SERVICE_FEE_PERCENTAGE);
    	
    	TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
    	       providerAccount.Id,
    	       membershipProduct,
    	       NU.Constant.PRICE_CLASS_DEFAULT,
    	       Date.Today(),
    	       providerMT.Id,
    	       expectedProviderDuesPricing,
    	       null,
    	       null);
    }
    
    static testmethod void serviceFeeTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
    	NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();

    	NU__Product__c duesMembershipProduct = NU.DataFactoryProduct.createMembershipProduct('Provider Dues', 0.00, providerMT.NU__Entity__c);
    	insert duesMembershipProduct;
        
        NU__MembershipTypeProductLink__c primaryMTPL = NU.DataFactoryMembershipTypeProductLink.createMembershipTypeProdLink(providerMT.Id, duesMembershipProduct.Id);
    	insert primaryMTPL;
    	
    	
    	NU__Product__c serviceFeeMembershipProduct = NU.DataFactoryProduct.createMembershipProduct('Provider Service Fee', 0.00, providerMT.NU__Entity__c);
    	insert serviceFeeMembershipProduct;
        
        NU__MembershipTypeProductLink__c serviceFeeMTPL = NU.DataFactoryMembershipTypeProductLink.createMembershipTypeProdLink(providerMT.Id, serviceFeeMembershipProduct.Id);
        serviceFeeMTPL.NU__Purpose__c = Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_OPTIONAL;
        serviceFeeMTPL.Provider_Service_Fee__c = true;
    	insert serviceFeeMTPL;
    	
    	   	
    	Decimal expectedProviderDuesPricing = ((providerAccount.Program_Service_Revenue__c * 0.00030) + 550) * (Constant.PROVIDER_SERVICE_FEE_PERCENTAGE);
    	
    	TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
    	       providerAccount.Id,
    	       serviceFeeMembershipProduct,
    	       NU.Constant.PRICE_CLASS_DEFAULT,
    	       Date.Today(),
    	       providerMT.Id,
    	       expectedProviderDuesPricing,
    	       null,
    	       null);
    }

    static testmethod void Year2014MaxDuesPriceTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(150000000);
    	NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();
    	
    	Decimal expectedProviderDuesPricing = ProviderMembershipPricer.AFTER_2013_MAX_PROVIDER_DUES_PRICE;
    	   	
    	Date startDate = Date.NewInstance(2014, 1, 1);
    	Date endDate = Date.NewInstance(2014, 12, 31);
    	Date joinDate = startDate.addDays(-200);
    	
    	TestMembershipPricingUtil.assertProviderPricingWithTerm(providerAccount, expectedProviderDuesPricing, providerMT, startDate, endDate, joinDate);
    }
    
    static testmethod void Year2013MaxDuesPriceTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount(150000000);
    	NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();
    	
    	Decimal expectedProviderDuesPricing = ProviderMembershipPricer.BEFORE_2014_MAX_PROVIDER_DUES_PRICE;
    	   	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = startDate.addDays(-200);
    	
    	TestMembershipPricingUtil.assertProviderPricingWithTerm(providerAccount, expectedProviderDuesPricing, providerMT, startDate, endDate, joinDate);
    }
}