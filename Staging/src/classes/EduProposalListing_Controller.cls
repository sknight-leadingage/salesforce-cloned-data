public with sharing class EduProposalListing_Controller extends EduReportsControllerBase {
    public override string getPageTitle() {
        if (RTypeSel == null) {
            return 'Proposal Listing';
        }
        else {
            return RTypeSel;
        }
    }
    
    public string PROPLIST_SNUMBER = 'Proposal Listing (By Proposal #)';
    public string PROPLIST_SSUBMITTER = 'Proposal Listing (By Submitter)';
    public string PROPLIST_SCOMPANY = 'Proposal Listing (By Company)';
    public string PROPLIST_STRACK = 'Proposal Listing (By Track)';
    public Map<string,integer> mTotals = new Map<string,integer>();

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(PROPLIST_SNUMBER, PROPLIST_SNUMBER));
        options.add(new SelectOption(PROPLIST_SSUBMITTER, PROPLIST_SSUBMITTER));
        options.add(new SelectOption(PROPLIST_SCOMPANY, PROPLIST_SCOMPANY));
        options.add(new SelectOption(PROPLIST_STRACK, PROPLIST_STRACK));
        return options; 
    }
    
    public List<Conference_Proposal__c> PropInfo { get; set; }
    public override void SetConRefreshData() {
        PropInfo = setCon.getRecords();
        if (RTypeSel == PROPLIST_STRACK) {
            GroupHeaderController.GroupTotals = mTotals;
        }
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == PROPLIST_SNUMBER) {
            WizardStep = 2;
        } else if (RTypeSel == PROPLIST_SSUBMITTER) {
            WizardStep = 3;
        } else if (RTypeSel == PROPLIST_SCOMPANY) {
            WizardStep = 4;
        } else if (RTypeSel == PROPLIST_STRACK) {
            WizardStep = 5;
        }
        SetQuery(1);
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    
    public EduProposalListing_Controller(){
        iWizardMax = 5;
        WizardStep = 1;
        SetConPageSize = 225;
    }
    
    public void SetQuery(integer reset) {
        string QueryBase = 'SELECT Proposal_Number__c, Title__c, Submitter__r.Account__r.Name, Submitter__r.Account__r.LastName, Submitter__r.Account__r.FirstName, Submitter__r.Account__r.Speaker_Company_Name_Rollup__c FROM Conference_Proposal__c WHERE Conference__c = :EventId ';
    
        if (RTypeSel == PROPLIST_SNUMBER) {
            QueryBase += ' ORDER BY Proposal_Number__c';
        } else if (RTypeSel == PROPLIST_SSUBMITTER) {
            QueryBase += ' ORDER BY Submitter__r.Account__r.LastName, Submitter__r.Account__r.FirstName';
        } else if (RTypeSel == PROPLIST_STRACK) {
            GroupHeaderController.GroupTotals = null;
            mTotals = null;
            mTotals = new Map<string,integer>();
            
            List<AggregateResult> Totals = [SELECT COUNT(Proposal_Number__c) sc, Conference_Track__r.Name afn FROM Conference_Proposal__c WHERE Conference__c = :EventId GROUP BY Conference_Track__r.Name ORDER BY Conference_Track__r.Name];
            for (AggregateResult ar : Totals ) {
                mTotals.put((string)ar.get('afn'), (integer)ar.get('sc'));
            }
            GroupHeaderController.GroupTotals = mTotals;
            
            QueryBase = 'SELECT Proposal_Number__c, Title__c, Submitter__r.Account__r.Name, Submitter__r.Account__r.LastName, Submitter__r.Account__r.FirstName, Submitter__r.Account__r.Speaker_Company_Name_Rollup__c, Conference_Track__r.Name FROM Conference_Proposal__c WHERE Conference__c = :EventId ORDER BY Conference_Track__r.Name';
        } else if (RTypeSel == PROPLIST_SCOMPANY) {
            QueryBase += ' ORDER BY Submitter__r.Account__r.Speaker_Company_Name_Rollup__c';
        }
        
        SetConQuery = QueryBase;
        if (reset == 1) {
            ResetCon();
        }
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}