@isTest
private class TestOnlineSubscriptionTrigger {
    
    static testmethod void testDuplicateTriggers() {
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        
        //Create new Test User      
        Account testPerson = new Account(FirstName = 'FN Temp Person', LastName = 'LN Temp Person' + sUqDate, PersonEmail = 'tempemail' + dtNow.format('yymmddhmmssS') + '@test.com');
        insert testPerson;
        
        // Test Entity
        NU__Entity__c TestEntity = new NU__Entity__c(name='Test Entity');
        insert TestEntity;
        
        // Test General Ledger Account
        NU__GLAccount__c myGLAccount = new NU__GLAccount__c(name='xxx-112-xxx', NU__Entity__c = TestEntity.Id );
        insert myGLAccount;
        
        //Online Subscription Record Type
        Id Prod_Record_Type_Id = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName().get('Online Subscription').getRecordTypeId();
        
        // Test Subscription Products               
        NU__Product__c product1 = 
                new NU__Product__c(name='Test Subscription 1', 
                                 RecordTypeId = Prod_Record_Type_Id,
                                 NU__Entity__c = TestEntity.Id,
                                 NU__RevenueGLAccount__c = myGLAccount.id,
                                 NU__DisplayOrder__c=1, 
                                 NU__ListPrice__c=0, 
                                 NU__QuantityMax__c=1);     
        NU__Product__c product2 = 
                new NU__Product__c(name='Test Subscription 2', 
                                 RecordTypeId = Prod_Record_Type_Id,
                                 NU__Entity__c = TestEntity.Id,
                                 NU__RevenueGLAccount__c = myGLAccount.id,
                                 NU__DisplayOrder__c=1, 
                                 NU__ListPrice__c=0, 
                                 NU__QuantityMax__c=1 );
        insert product1;
        insert product2;
                                 
        System.debug('Product 1 ID: ' + product1.Id );
        System.debug('Product 2 ID: ' + product2.Id );                       
        
        //insert original
        Online_Subscription__c mySub = new Online_Subscription__c(Account__c = testPerson.Id, Complimentary__c = true, Product__c = product1.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);
        insert mySub;
                    
                                
        // try to insert dupe, catch exception and test validation fires        
        try {
            Online_Subscription__c myNewSub = new Online_Subscription__c(Account__c = testPerson.Id, Complimentary__c = true, Product__c = product1.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);
            insert myNewSub;    
        }       
        catch (DmlException e){         
            system.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));            
        }               
        /**catch (Exception e){         
            system.assert(e.getMessage().contains('existing'));         
        }*/
                            
    
        //insert new subscription, then try to edit and assign an exisitng sub
        Online_Subscription__c myUpdatedSub = new Online_Subscription__c(Account__c = testPerson.Id, Complimentary__c = true, Product__c = product2.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);
        insert myUpdatedSub;
        // Check update condition, catch exception and test validation fires 
        try {
            myUpdatedSub.Product__c = product1.id;
            update myUpdatedSub;
        }   
        catch (DmlException e){         
            system.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));            
        }               
        
        myUpdatedSub = [SELECT Id, Product__c, Account__c, System_End_Date__c, Status__c FROM Online_Subscription__c WHERE Account__c = :testPerson.Id AND Product__c = :product2.id LIMIT 1];
        
        //Poke status protection code
        myUpdatedSub.System_End_Date__c = null;
        myUpdatedSub.Status__c = Constant.SUBSCRIPTION_STATUS_HELD; //Woodchuck... Constant.SUBSCRIPTION_STATUS_HELD;
        update myUpdatedSub;
        
        myUpdatedSub.System_End_Date__c = DateTime.now();
        myUpdatedSub.Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE;
        update myUpdatedSub;
    }
    
    static testmethod void testMergeRecordNoDuplicateSubscription(){
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        
        //Create new Test User #1     
        Account testPerson1 = new Account(FirstName = 'FN Temp Person 1', LastName = 'LN Temp Person 1x' + sUqDate, PersonEmail = 'tempemail1' + dtNow.format('yymmddhmmssS') + '@test.com');
        insert testPerson1;
        
        //Create new Test User #2     
        Account testPerson2 = new Account(FirstName = 'FN Temp Person 2', LastName = 'LN Temp Person 2x' + sUqDate, PersonEmail = 'tempemail2' + dtNow.format('yymmddhmmssS') + '@test.com');
        insert testPerson2;
        
        // Test Entity
        NU__Entity__c TestEntity = new NU__Entity__c(name='Test Entity');
        insert TestEntity;
        
        // Test General Ledger Account
        NU__GLAccount__c myGLAccount = new NU__GLAccount__c(name='xxx-112-xxx', NU__Entity__c = TestEntity.Id );
        insert myGLAccount;
        
        //Online Subscription Record Type
        Id Prod_Record_Type_Id = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName().get('Online Subscription').getRecordTypeId();
        
        // Test Subscription Products               
        NU__Product__c productA = 
                new NU__Product__c(name='TestSub Default A', 
                                 RecordTypeId = Prod_Record_Type_Id,
                                 NU__Entity__c = TestEntity.Id,
                                 NU__RevenueGLAccount__c = myGLAccount.id,
                                 NU__DisplayOrder__c=1, 
                                 NU__ListPrice__c=0, 
                                 NU__QuantityMax__c=1,
                                 Membership_Default__c = true);     
        NU__Product__c productB = 
                new NU__Product__c(name='TestSub Default B', 
                                 RecordTypeId = Prod_Record_Type_Id,
                                 NU__Entity__c = TestEntity.Id,
                                 NU__RevenueGLAccount__c = myGLAccount.id,
                                 NU__DisplayOrder__c=1, 
                                 NU__ListPrice__c=0, 
                                 NU__QuantityMax__c=1,
                                 Membership_Default__c = true);                      
        NU__Product__c productC = 
                new NU__Product__c(name='TestSub C', 
                                 RecordTypeId = Prod_Record_Type_Id,
                                 NU__Entity__c = TestEntity.Id,
                                 NU__RevenueGLAccount__c = myGLAccount.id,
                                 NU__DisplayOrder__c=1, 
                                 NU__ListPrice__c=0, 
                                 NU__QuantityMax__c=1);
        NU__Product__c productD = 
                new NU__Product__c(name='TestSub D', 
                                 RecordTypeId = Prod_Record_Type_Id,
                                 NU__Entity__c = TestEntity.Id,
                                 NU__RevenueGLAccount__c = myGLAccount.id,
                                 NU__DisplayOrder__c=1, 
                                 NU__ListPrice__c=0, 
                                 NU__QuantityMax__c=1);
                                 
        insert productA;
        insert productB;
        insert productC;
        insert productD;
                                 
        //insert subscription test person 1
        Online_Subscription__c Person1SubA = new Online_Subscription__c(Account__c = testPerson1.Id, Complimentary__c = true, Product__c = productA.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);
        Online_Subscription__c Person1SubB = new Online_Subscription__c(Account__c = testPerson1.Id, Complimentary__c = true, Product__c = productB.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);
        Online_Subscription__c Person1SubC = new Online_Subscription__c(Account__c = testPerson1.Id, Complimentary__c = true, Product__c = productC.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);

        
        insert Person1SubA;
        insert Person1SubB;
        insert Person1SubC;
        
        //insert subscription test person 2
        Online_Subscription__c Person2SubA = new Online_Subscription__c(Account__c = testPerson2.Id, Complimentary__c = true, Product__c = productA.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE);
        Online_Subscription__c Person2SubB = new Online_Subscription__c(Account__c = testPerson2.Id, Complimentary__c = true, Product__c = productB.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_INACTIVE);
        Online_Subscription__c Person2SubD = new Online_Subscription__c(Account__c = testPerson2.Id, Complimentary__c = true, Product__c = productD.id, Start_Date__c = dtNow, Status__c = Constant.SUBSCRIPTION_STATUS_HELD);
        
        insert Person2SubA;
        insert Person2SubB;
        insert Person2SubD;
                    
        try {
            merge testPerson1 testPerson2;
        }
         catch (DmlException e) {
        // Process exception here
        }
        

         ///Verify Subscriptions
          List<Online_Subscription__c> mySubs = [SELECT Id, Product__r.Name, Status__c FROM Online_Subscription__c WHERE Account__c = :testPerson1.Id];
  
          for(Online_Subscription__c s : mySubs)
          {    
            
          // Check Held subscription are now set to Active
            if (s.Product__c == productD.Id)
                {  
              system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
                }
    
          }    
    }
}