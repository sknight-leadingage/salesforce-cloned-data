/**
 * @author NimbleUser
 * @date Updated: 2/6/13
 * @description This class has various tests to ensure that the Program Service Revenue is aggregated to the MultiSite accounts.
 */
@isTest
public class TestMultiSitePSRAggregator {

	static void aggregate(){
		Test.startTest();
		
		new MultiSiteProgramServiceRevenueAggregator().execute(null);
		
		Test.stopTest();
	}
	
	public static void assertMultiSiteProgramServiceRevenue(Id multiSiteProviderId, Decimal expectedMultiSiteProgramServiceRevenue){
		Account multiSiteAccount = [Select id, name, Multi_Site_Program_Service_Revenue__c from Account where id = :multiSiteProviderId];
		
		system.assertEquals(expectedMultiSiteProgramServiceRevenue, multiSiteAccount.Multi_Site_Program_Service_Revenue__c);
	}

    static testMethod void aggregateTwoProvidersTest() {
        Account multiSite = DataFactoryAccountExt.insertMultiSiteAccount();
        
        List<Decimal> programServiceRevenues = new List<Decimal>{ 15000, 40000 };
        List<Account> multiSiteProviderAccounts = DataFactoryAccountExt.insertMultiSiteEnabledProviderAccounts(programServiceRevenues);
        Set<Id> multiSiteProviderAccountIds = NU.CollectionUtil.getSobjectIds(multiSiteProviderAccounts);
        
        NU.DataFactoryAffiliation.insertPrimaryAffiliations(multiSite.Id, multiSiteProviderAccountIds);
        
        aggregate();
        
        assertMultiSiteProgramServiceRevenue(multiSite.Id, 55000);
    }
    
    static testmethod void aggregateNoProvidersTest(){
    	Account multiSite = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	aggregate();
    	
    	assertMultiSiteProgramServiceRevenue(multiSite.Id, 0);
    }
    
    static testmethod void aggregateTwoProvidersWithOneUnderConstructionTest(){
    	Account multiSite = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	Account underConstructionProviderAccount = DataFactoryAccountExt.createMultiSiteEnabledUnderConstructionProviderAccount(30000);
    	insert underConstructionProviderAccount;
    	
    	Account providerAccount = DataFactoryAccountExt.insertMultiSiteEnabledProviderAccount(50000);
    	
    	NU.DataFactoryAffiliation.insertPrimaryAffiliations(multiSite.Id, new Set<Id>{ underConstructionProviderAccount.Id, providerAccount.Id });
    	
    	aggregate();
    	
    	assertMultiSiteProgramServiceRevenue(multiSite.Id, providerAccount.Program_Service_Revenue__c);
    }
}