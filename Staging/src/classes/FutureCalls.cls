global class FutureCalls
{
	@future
	public static void AbortJobAsync(Id jobId) {
		system.abortJob(jobId);
	}
}