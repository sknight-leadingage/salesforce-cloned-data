global class BatchOnlineSubscriptions implements Database.Batchable<sobject>, Schedulable {
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//staging new: String Query = 'SELECT Id,CreatedBy.FirstName, CreatedBy.LastName, Leadingage_ID__c, NU__PrimaryAffiliation__c, personemail, PersonEmailBouncedDate, PersonEmailBouncedReason,NU__PrimaryAffiliation__r.NU__Recordtypename__c, NU__PrimaryAffiliation__r.NU__Status__c,LeadingAge_Member_Individual__c, NU__Status__c FROM Account WHERE IsPersonAccount = true';
		String Query = 'SELECT Id FROM Account WHERE IsPersonAccount = true';
		return Database.getQueryLocator(Query);
	}
	
	global void execute(Database.BatchableContext BC,List<sObject> scope)
	{
   		Set<Id> accountsFromStartId = new Set<Id>();
   		for(sObject o : scope) {
   			accountsFromStartId.add(o.Id);
   		}
   		
   		List<Account> accounts = [SELECT Id,CreatedBy.FirstName,
   			CreatedBy.LastName, Leadingage_ID__c, NU__PrimaryAffiliation__c, personemail, PersonEmailBouncedDate,
   			PersonEmailBouncedReason,NU__PrimaryAffiliation__r.NU__Recordtypename__c, NU__PrimaryAffiliation__r.NU__Status__c,
   			LeadingAge_Member_Individual__c, NU__Status__c,
   				(SELECT Id, Status__c, Start_date__c, end_date__c, system_end_date__c, Complimentary__c, Product__r.Membership_restricted__c,
   				Product__r.Membership_default__c,Product__c, Product__r.NU__Entity__r.Name, Product__r.Name, Product__r.Online_Subscription_Type__c
   				FROM Online_Subscriptions__r WHERE Product__r.NU__Entity__r.Name = :Constant.LEADINGAGE_ENTITY_NAME)
   			FROM Account WHERE Id IN :accountsFromStartId];
   		
   		List<Account> NewMembers = new List<Account>();
        List<Account> ExpiredMembers = new List<Account>();
        List<Online_Subscription__c> ExpiredSubs = new List<Online_Subscription__c>();
        List<Online_Subscription__c> MemberSubs = new List<Online_Subscription__c>(); 
        //List<Online_Subscription__c> InsertMemberSubs = new List<Online_Subscription__c>();
   		//setup prep
   		// CreatedBy ='Phone 2Action'  CreatedBy.FirstName = 'Phone' CreatedBy.LastName = '2Action'
		Map<Id,NU__Product__c> subProducs = new Map<Id,NU__Product__c>([select id, name, Membership_restricted__c, Membership_default__c, NU__Entity__c, NU__Entity__r.Name,Online_Subscription_Type__c from NU__Product__c where NU__RecordTypeName__c = :Constant.ORDER_RECORD_TYPE_ONLINE_SUBSCRIPTION and NU__Entity__r.Name = :Constant.LEADINGAGE_ENTITY_NAME]);
   				 		 				 		
   		string sA = Constant.SUBSCRIPTION_STATUS_ACTIVE;
   	    DateTime dtNow = DateTime.Now();
   	    
   	    system.debug('*** Total accounts in batch step: ' + accounts.size());
   		system.debug('*** Total products in batch step: ' + subProducs.size());
   		
   		for (Account a : accounts) {
			List<Online_Subscription__c> mySubs = a.Online_Subscriptions__r;
   			
   			system.debug('*** Account: ' + a.PersonEmail + ', Status: ' + a.NU__Status__c);
   			
   			if(a.NU__Status__c == Constant.ACCOUNT_STATUS_INACTIVE)
   			{
	            if(mySubs != null && mySubs.size() > 0)  
	            {                             
	                for(Online_Subscription__c sub : mySubs){
	                	if (sub.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE) {
		                    Online_Subscription__c existingSub = new Online_Subscription__c(id = sub.id, status__c = Constant.SUBSCRIPTION_STATUS_INACTIVE, End_Date__c = sub.End_Date__c, System_End_Date__c = dtNow);
		                    if (existingSub.End_Date__c == null || existingSub.End_Date__c < dtNow) {
		                        existingSub.End_Date__c = dtNow;
		                    }
		                    ExpiredSubs.add(existingSub);
	                	}
	                }
	                
	            }
   			}
			else if(a.NU__Status__c == Constant.ACCOUNT_STATUS_ACTIVE){
				system.debug('*** Account: ' + a.PersonEmail + ', Member Status: ' + a.LeadingAge_Member_Individual__c);
				
   				 if(a.LeadingAge_Member_Individual__c == Constant.LEADINGAGE_NONMEMBER)
   				 {
   				 	if(mySubs != null && mySubs.size() > 0)
                    { 
	   				 	for(Online_Subscription__c sub : mySubs){
	   				 		if (sub.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE && (sub.Product__r.Membership_default__c == true || sub.Product__r.Membership_restricted__c == true)) {
	   				 			if (sub.Complimentary__c == false) {
			                        Online_Subscription__c existingSub = new Online_Subscription__c(id = sub.id, status__c = Constant.SUBSCRIPTION_STATUS_INACTIVE, End_Date__c = sub.End_Date__c, System_End_Date__c = dtNow);
			                        if (existingSub.End_Date__c == null || existingSub.End_Date__c < dtNow) {
			                            existingSub.End_Date__c = dtNow;
			                        }
			                        ExpiredSubs.add(existingSub);
	   				 			}
	   				 		}
	                    }
                    }
   				 }
   				 else if(a.LeadingAge_Member_Individual__c == Constant.LEADINGAGE_MEMBER)
   				 {
   				 	if(a.PersonEmailBouncedDate != null || a.PersonEmailBouncedReason != null)
   				 	{
   				 		if(mySubs != null && mySubs.size() > 0)  
	                    {                             
		                    for(Online_Subscription__c sub : mySubs){
		                    	if (sub.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE) {
			                        Online_Subscription__c existingSub = new Online_Subscription__c(id = sub.id, status__c = Constant.SUBSCRIPTION_STATUS_HELD, End_Date__c = sub.End_Date__c);
			                        if (existingSub.End_Date__c == null || existingSub.End_Date__c < dtNow) {
			                            existingSub.End_Date__c = dtNow;
			                            
			                            
			                        }
			                        ExpiredSubs.add(existingSub);
			                        
		                    	}
		                    }
	                    }
   				 	}
   				 	else if(a.PersonEmailBouncedDate == null)
   				 	{
   				 		//Configure Member Defaults
   				 		//Create temp list of default products
   				 		Map<Id,NU__Product__c> subMdefaults = new Map<Id,NU__Product__c>(); 
   				 		Map<Id,NU__Product__c> subProviderMSO = new Map<Id,NU__Product__c>();
   				 		Map<Id,NU__Product__c> subCompany = new Map<Id,NU__Product__c>();
   				 		if (subProducs != null && subProducs.size() > 0) {
   				 			for(NU__Product__c subProd : subProducs.values()){
   				 				if (subProd.Membership_default__c == true) {
   				 					subMdefaults.put(subProd.Id, subProd);
   				 				}
   				 				if (subProd.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_EDUCATION_AND_CONFERENCES || subProd.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_GENERAL_INFORMATION) {
   				 					subProviderMSO.put(subProd.Id, subProd);
   				 				}
   				 				if (subProd.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_EDUCATION_AND_CONFERENCES || subProd.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_EXHIBITS_AND_ADVERTISTING) {
   				 					subCompany.put(subProd.Id, subProd);
   				 				}
   				 			}
   				 		}
   				 		
   				 		system.debug('*** Total defaults in batch step: ' + subMdefaults.size());
   				 		
   				 		//Create temp list of default products for Primary Affiliation Account Record Type is: Provider OR MSO

				 
				    	//Create temp list of default products for Primary Affiliation Account Record Type is: Company
						 


						//Look for existing default products on account's subs
						if (mySubs != null && mySubs.size() > 0) {
	                    	for(Online_Subscription__c sub : mySubs){
	                    		if (sub.Product__r.Membership_default__c == true) {
	                    			//Account already has this sub product, so remove from temp list 
	                    			if (subMdefaults.containsKey(sub.Product__c)) {
	                    				subMdefaults.remove(sub.Product__c);
	                    			}
	                    			
	                    			//Also set it to Active if not already, note that we don't change U or H status here, only I -> A
	                    			if (sub.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE) {
	                    				MemberSubs.add(new Online_Subscription__c(Id = sub.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow, End_Date__c = null, System_End_Date__c = null));
	                    			}
	                    		}
	                    		
	                    		if(a.NU__PrimaryAffiliation__c != null && (a.NU__PrimaryAffiliation__r.NU__Recordtypename__c == Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER ||  a.NU__PrimaryAffiliation__r.NU__Recordtypename__c == Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE) && a.NU__PrimaryAffiliation__r.NU__Status__c == Constant.AFFILIATION_STATUS_ACTIVE)
	                    			{
	                    				if(sub.Product__r.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_EDUCATION_AND_CONFERENCES || sub.Product__r.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_GENERAL_INFORMATION)
	                    				{
	                    					if (subProviderMSO.containsKey(sub.Product__c)) {
			                    				subProviderMSO.remove(sub.Product__c);
			                    			}
			                    			//Also set it to Active if not already, note that we don't change U or H status here, only I -> A
			                    			if (sub.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE) {
			                    				MemberSubs.add(new Online_Subscription__c(Id = sub.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow, End_Date__c = null, System_End_Date__c = null));
			                    			}
	                    				}
	                    				
	                    				
	                    			}
	                    		if(a.NU__PrimaryAffiliation__c != null &&  a.NU__PrimaryAffiliation__r.NU__Recordtypename__c == Constant.ACCOUNT_RECORD_TYPE_NAME_COMPANY && a.NU__PrimaryAffiliation__r.NU__Status__c == Constant.AFFILIATION_STATUS_ACTIVE)
	                    			{
	                    				if(sub.Product__r.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_EDUCATION_AND_CONFERENCES || sub.Product__r.Name == Constant.ONLINE_SUBSCRITPION_PRODUCT_EXHIBITS_AND_ADVERTISTING)
	                    				{
	                    					if (subCompany.containsKey(sub.Product__c)) {
			                    				subCompany.remove(sub.Product__c);
			                    			}
			                    			//Also set it to Active if not already, note that we don't change U or H status here, only I -> A
			                    			if (sub.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE) {
			                    				MemberSubs.add(new Online_Subscription__c(Id = sub.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow, End_Date__c = null, System_End_Date__c = null));
			                    			}
	                    				}
	                    				
	                    				
	                    				
	                    				
	                    			}
	                    			
	                    	}
						}
                    	
                    	//If there's any Member Default products left in the temp list then the user doesn't have subs for those, so add them
                    	if (subMdefaults.size() > 0) {
	                    	for (NU__Product__c subProd : subMdefaults.values()) {
	                    		MemberSubs.add(new Online_Subscription__c(Account__c = a.Id, Product__c = subProd.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow));
	                    	}
                    	}
                    	
                    	/*if (subMdefaults.size() > 0) {
	                    	for (NU__Product__c subProd : subMdefaults.values()) {
	                    		InsertMemberSubs.add(new Online_Subscription__c(Account__c = a.Id, Product__c = subProd.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow));
	                    	}
                    	}*/
                    	
                    	if(a.NU__PrimaryAffiliation__c != null && (a.NU__PrimaryAffiliation__r.NU__Recordtypename__c == Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER ||  a.NU__PrimaryAffiliation__r.NU__Recordtypename__c == Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE) && a.NU__PrimaryAffiliation__r.NU__Status__c == Constant.AFFILIATION_STATUS_ACTIVE)
	                    			{
					                    if (subProviderMSO.size() > 0) {
					                    	for (NU__Product__c subProd : subProviderMSO.values()) {
					                    		MemberSubs.add(new Online_Subscription__c(Account__c = a.Id, Product__c = subProd.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow));
					                    	}
					                    }
	                    			}
	                    			
	                  if(a.NU__PrimaryAffiliation__c != null &&  a.NU__PrimaryAffiliation__r.NU__Recordtypename__c == Constant.ACCOUNT_RECORD_TYPE_NAME_COMPANY && a.NU__PrimaryAffiliation__r.NU__Status__c == Constant.AFFILIATION_STATUS_ACTIVE)
	                    			{  			
						                    if (subCompany.size() > 0) {
						                    	for (NU__Product__c subProd : subCompany.values()) {
						                    		MemberSubs.add(new Online_Subscription__c(Account__c = a.Id, Product__c = subProd.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = dtNow));
						                    	}
					                    	}
	                    			}
   				 	}
   				 }
   			}
   		}
   		
   		if (ExpiredSubs.size() > 0) { update ExpiredSubs; }
   		//if (InsertMemberSubs.size() > 0) { insert InsertMemberSubs; }
   		if (MemberSubs.size() > 0) { upsert MemberSubs; }
   		
   		system.debug('*** Total upserts in batch step: ' + MemberSubs.size());
	}
	
	global void finish(Database.BatchableContext BC)
	{
	}
	
	//BEGIN: Scahedule daily at 8pm
	public static void schedule() {
		System.schedule('Online Subscription Update', '0 0 20 * * ?', new BatchOnlineSubscriptions());
	}
	
    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }
    //END
}