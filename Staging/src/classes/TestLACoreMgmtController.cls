/*
* Test methods for the LeadershipApplicationsMgmt_Controller
*/
@isTest(SeeAllData=true)

private class TestLACoreMgmtController{

    static LeadershipAcademyCoreMgmt_Controller loadController(){
        PageReference pageRef = Page.LeadershipAcademyCoreMgmt;
        Test.setCurrentPage(pageRef);
        
        string SelectedAcademyYear = '2016';
        
        pageRef.getParameters().put(LeadershipAcademyControllerBase.ACADEMY_YEAR_PARAM, SelectedAcademyYear);
        
        LeadershipAcademyCoreMgmt_Controller AppController = new LeadershipAcademyCoreMgmt_Controller();
        
        return AppController;
     }
    
    static testmethod void DisplayResultTest(){
        LeadershipAcademyCoreMgmt_Controller AppController = loadController();
        system.assert(AppController.AcademyCoreMgmt != null);
    }
    
   
}