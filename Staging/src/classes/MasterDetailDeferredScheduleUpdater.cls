public without sharing class MasterDetailDeferredScheduleUpdater implements Database.Batchable<SObject> {
    public static void execute() {
        execute(200);
    }
    
    public static void execute(Integer batchSize) {
        Database.executeBatch(new MasterDetailDeferredScheduleUpdater(), 200);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT NU__DeferredRevenueMethod__c, NU__RecognizedRevenueGLAccount__c FROM NU__DeferredSchedule__c
            WHERE NU__DeferredRevenueMethod2__c = null OR NU__RecognizedRevenueGLAccount2__c = null]);
    }

    public void execute(Database.BatchableContext bc, List<NU__DeferredSchedule__c> schedules) {
        for (NU__DeferredSchedule__c schedule : schedules) {
            schedule.NU__DeferredRevenueMethod2__c = schedule.NU__DeferredRevenueMethod__c;
            schedule.NU__RecognizedRevenueGLAccount2__c = schedule.NU__RecognizedRevenueGLAccount__c;
        }
        update schedules;
    }

    public void finish(Database.BatchableContext bc) { }
}