public with sharing class EduPostIt_Controller extends EduReportsControllerBase {
    public List<Conference_Session__c> SumList { get;set; }
    
    public override string getPageTitle() {
        return RptRenderAs == '' && Conference != null ? Conference.Name + ': ' + string.valueof(Date.today()) : '';
    }
    
    public EduPostIt_Controller() {
        SetConPageSize = 255;
        SetQuery();
    }
    
    public override void SetConRefreshData() {
        SumList = setCon.getRecords();
    }
    
    public void SetQuery() {
        SetConQuery = 'SELECT Conference_Track__r.Name, Conference_Track__r.Track_Code__c, Proposal__r.Proposal_Number__c, Session_Length__c, Title__c, Submitter__r.Name, Submitter__r.LeadingAge_ID__c, Education_Staff__c, Education_Staff__r.Name FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Education_Staff__r.Name, Conference_Track__r.Track_Code__c, Proposal__r.Proposal_Number__c';

        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}