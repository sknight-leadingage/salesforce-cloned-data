/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceProposalQuerier {

    static testMethod void getConferenceProposalsByConferenceIdTest() {
        Conference_Proposal__c prop = DataFactoryConferenceProposal.insertConferenceProposal();
        
        List<Conference_Proposal__c> props = ConferenceProposalQuerier.getConferenceProposalsByConferenceId(prop.Conference__c);
    }
    
    static testmethod void getConferenceProposalsByConferenceIdAndSearchCriteriaTest(){
    	Conference_Proposal__c prop = DataFactoryConferenceProposal.insertConferenceProposal();
        
        ConferenceProposalSearchCriteria searchCriteria = new ConferenceProposalSearchCriteria();
        
        searchCriteria.Company = 'no company';
        searchCriteria.FirstName = 'no first name';
        searchCriteria.LastName = 'no last name';
        searchCriteria.LearningObjectivesPhrase = 'no learning objective';
        searchCriteria.NotesPhrase = 'no notes';
        searchCriteria.SessionNumber = 'no session number';
        searchCriteria.Title = 'no title';
        searchCriteria.TrackName = 'no track name';
        
        system.assert(searchCriteria.HasSearchCriteria, 'Has Search Criteria is false when it should be true');
        
        List<Conference_Proposal__c> props = ConferenceProposalQuerier.getConferenceProposalsByConferenceIdAndSearchCriteria(prop.Conference__c, searchCriteria);

		        
        searchCriteria.clearSearchCriteria();
        
        system.assert(searchCriteria.HasSearchCriteria == false, 'Has Search Criteria is true when it should be false.');
    }
}