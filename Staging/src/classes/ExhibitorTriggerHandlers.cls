//handles triggers related to NU__Exhibitor__c
//currently handles one scenario, where the Account record.Current_Year_Exhibitor__c flag is set or cleared when an related exhibitor record changes

//Developed by NimbleUser, 2013 (MG, NF)

public with sharing class ExhibitorTriggerHandlers extends NU.TriggerHandlersBase
{
    
    // public override void onBeforeInsert(List<sObject> newRecords){}    
   // public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {}    
   // public override void onBeforeDelete(Map<Id, sObject> oldRecordMap) { }
   
    public override void onAfterInsert(Map<Id, sObject> newRecordMap) 
    { 
        SetCurrentYearExhibitor((Map<Id, Exhibitor__c>)newRecordMap, false);
    }
    
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap)
    { 
        SetCurrentYearExhibitor((Map<Id, Exhibitor__c>)newRecordMap, false);
    }
    
    public override void onAfterDelete(Map<Id, sObject> oldRecordMap) 
    {
        SetCurrentYearExhibitor((Map<Id, Exhibitor__c>)oldRecordMap, true);
    }
    
    public override void onAfterUndelete(Map<Id, sObject> newRecordMap) 
    { 
        SetCurrentYearExhibitor((Map<Id, Exhibitor__c>)newRecordMap, false);
    } 

    //adjusts any Account record to set or clear the Current_Year_Exhibitor__c flag
    //depending on if the related exhibitor record for the current year is active
    //affects exhibitor point calculations
    private void SetCurrentYearExhibitor(Map<Id, Exhibitor__c> exhibitors, Boolean isAfterDelete)
    {
        //a list and map of affected accounts
        List<String> accountIds = new List<String>();
        Map<Id, Account> accounts = null;

		//a list and map of affected events
        List<String> eventIds = new List<String>();
        Map<Id, NU__Event__c> events = null;

		//a list of account requiring an update
        List<Account> accountsToUpdate = new List<Account>();
        
        //for all exhibitor records in the trigger, store the related account and event ids
        for(Exhibitor__c exh :exhibitors.values())
        {
            accountIds.add(exh.Account__c);
            eventIds.add(exh.Event__c);
        }

		//for the list of account and event ids, create maps
		//for account: the current_year_exhibitor flag
		//for events: the current_annual_event flag     
        accounts = new Map<Id, Account>([SELECT Id, Current_Year_Exhibitor__c FROM Account WHERE Id in :accountIds]);
        events = new Map<Id, NU__Event__c>([SELECT Id, Current_Year_Annual_Event__c FROM NU__Event__c where Id in :eventIds]);

		//for all exhibitor records in the trigger,         
        for(Exhibitor__c exh :exhibitors.values())
        {
            Account a = accounts.get(exh.Account__c);
            NU__Event__c e = events.get(exh.Event__c);
            
            if(a != null && e != null)
            {
                if(e.Current_Year_Annual_Event__c)
                {
					//update account if the exhibitor is deleted or cancelled, to clear the current year flag
                    if((isAfterDelete && a.Current_Year_Exhibitor__c) || (exh.Status__c == 'Cancelled' && a.Current_Year_Exhibitor__c))
                    {
                        a.Current_Year_Exhibitor__c = false;
                        accountsToUpdate.add(a);
                    }
                    //update account if the exhibitor is not cancelled, to set the current year flag
                    else if(exh.Status__c != 'Cancelled' && !a.Current_Year_Exhibitor__c)
                    {
                        a.Current_Year_Exhibitor__c = true;
                        accountsToUpdate.add(a);
                    }
                }
            }
        }

		//update all affected accounts
        if(accountsToUpdate.size() > 0)
        {
            update accountsToUpdate;
        }
    }
}