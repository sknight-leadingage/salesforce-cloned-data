/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceCategoriesController {

    static Conference_Session__c session = null;
    
    static ConferenceCategoriesController loadController(){
        PageReference pageRef = Page.ConferenceCategories;
        Test.setCurrentPage(pageRef);
        
        session = DataFactoryConferenceSession.insertConferenceSession();
        
        pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, session.Conference__c);
        
        ConferenceCategoriesController CategoriesController = new ConferenceCategoriesController();
        
        return CategoriesController;
    }
static testmethod void noActivityWorkersCertificationSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.ActivityWorkersCertificationSelectOpts .size() == 0);
    }
    
    static testmethod void noFloridaDomainsOfPracticeSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.FloridaDomainsOfPracticeSelectOpts .size() == 0);
    }
    
    static testmethod void noMissouriDomainsOfPracticeSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.MissouriDomainsOfPracticeSelectOpts .size() == 0);
    }
    
    static testmethod void noKansasDomainsOfPracticeSelectOptsTest(){
        ConferenceCategoriesController CategoriesController= loadController();
        system.assert(CategoriesController.KansasDomainsOfPracticeSelectOpts .size() == 0);
    }
    
    static testmethod void noNASBADomainsOfPracticeSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.NASBADomainsOfPracticeSelectOpts .size() == 0);
    }
    
    static testmethod void noNABDomainsOfPracticeSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.NABDomainsOfPracticeSelectOpts .size() == 0);
    }
    
    static testmethod void noCARCFEDomainsOfPracticeSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.CARCFEDomainsOfPracticeSelectOpts .size() == 0);
    }
    
    static testmethod void noCFRECertificationSelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.CFRECertificationSelectOpts .size() == 0);
    }
    
    static testmethod void noAvailableContentCategorySelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.AvailableContentCategorySelectOpts.size() == 0);
    
    }
    static testmethod void noSelectedContentCategorySelectOptsTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        system.assert(CategoriesController.SelectedContentCategorySelectOpts.size() == 0);
    
    }
    
    static testmethod void searchTitleTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        CategoriesController.SearchCriteria.Title = session.Title__c;
        
        CategoriesController.search();
        system.assert(CategoriesController.CurrentSession != null);
    }
    
    static testmethod void clearSearchTest(){
        ConferenceCategoriesController CategoriesController = loadController();
        CategoriesController.SearchCriteria.Title = session.Title__c;
        
        system.assert(CategoriesController.HasSearchCriteria == true);
        
        CategoriesController.clearSearch();
        
        system.assert(CategoriesController.HasSearchCriteria == false);
    }
    
    static testmethod void SaveConferenceCategoriesControllerTest(){
        session = DataFactoryConferenceSession.insertConferenceSession();
        
        //Content Categories
        List<Conference_Content_Category__c> catsDummy = new List<Conference_Content_Category__c>();
        catsDummy.add(new Conference_Content_Category__c(Name = 'Nursing'));
        catsDummy.add(new Conference_Content_Category__c(Name = 'Development'));
        catsDummy.add(new Conference_Content_Category__c(Name = 'Branding'));
        insert catsDummy;
        
        List<Conference_Content_Category_Session__c> catSession = new List<Conference_Content_Category_Session__c>();
        for(Conference_Content_Category__c c : catsDummy) {
            if (c.Name != 'Branding') {
                catSession.add(new Conference_Content_Category_Session__c(Conference_Session__c = session.Id, Conference_Content_Category__c = c.Id));
            }
        }
        insert catSession;
    
    
        PageReference pageRef = Page.ConferenceCategories;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, session.Conference__c);
        ConferenceCategoriesController CategoriesController = new ConferenceCategoriesController();
        
        system.assertEquals(session.Conference__c, CategoriesController.EventId);
        system.assertNotEquals(null, CategoriesController.CurrentSession);
        
        system.assertNotEquals(null, CategoriesController.CurrentSessionContentCategories);
        system.assertNotEquals(0, CategoriesController.CurrentSessionContentCategories.size());

        system.assertNotEquals(null, CategoriesController.ContentCategories);
        system.assertNotEquals(0, CategoriesController.ContentCategories.size());

        system.assertNotEquals(null, CategoriesController.AvailableContentCategorySelectOpts);
        system.assertNotEquals(0, CategoriesController.AvailableContentCategorySelectOpts.size());
        
        system.assertNotEquals(null, CategoriesController.SelectedContentCategorySelectOpts);
        system.assertNotEquals(0, CategoriesController.SelectedContentCategorySelectOpts.size());
        
        
        //CE Categories
        List<CE_Category__c> CEcatsDummy = new List<CE_Category__c>();
        CEcatsDummy.add(new CE_Category__c(Name = 'Activity Workers'));
        CEcatsDummy.add(new CE_Category__c(Name = 'Fund Raising (CFRE)'));
        CEcatsDummy.add(new CE_Category__c(Name = 'Human Resources'));
        CEcatsDummy.add(new CE_Category__c(Name = 'CPAs'));
        CEcatsDummy.add(new CE_Category__c(Name = 'Social Workers'));
        insert CEcatsDummy;
        
        List<CE_Conference_Session__c> CEcatSession = new List<CE_Conference_Session__c>();
        //Select just the 'CPAs' category
        for(CE_Category__c ce : CEcatsDummy) {
            if (ce.Name == 'CPAs') {
                CEcatSession.add(new CE_Conference_Session__c(Conference_Session__c = session.Id, CE_Category__c = ce.Id));
            }
        }
        insert CEcatSession;
        
        system.assertNotEquals(null, CEcatSession.size());
        system.assertNotEquals(0, CEcatSession.size());
        
        pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, session.Conference__c);
        Test.setCurrentPage(pageRef);
        CategoriesController = new ConferenceCategoriesController();
        
        system.assertNotEquals(null, CategoriesController.Conference);
        system.assertNotEquals(null, session.Conference__c);
        system.assertEquals(session.Conference__c, CategoriesController.Conference.Id);
                
        system.assertNotEquals(null, CategoriesController.AvailableCECategorySelectOpts);
        system.assertNotEquals(0, CategoriesController.AvailableCECategorySelectOpts.size());
        
        CategoriesController.save();
        
        system.assertNotEquals(null, CategoriesController.SelectedCECategorySelectOpts);
        system.assertNotEquals(0, CategoriesController.SelectedCECategorySelectOpts.size());
    
/*
    
        ConferenceCategoriesController CategoriesController = loadController();
        
        List<SelectOption> test_options = new List<SelectOption>();
                test_options.add(new SelectOption('Nursing', 'Nursing'));
                test_options.add(new SelectOption('Development', 'Development'));
        
        //Content Categories
        CategoriesController.SelectedContentCategorySelectOpts = test_options ;
        
        List<Conference_Content_Category_Session__c> CurrentSessionContentCategories = [SELECT Conference_Content_Category__c from Conference_Content_Category_Session__c];
        
        for (Conference_Content_Category_Session__c CC: CurrentSessionContentCategories){
            System.assertEquals('Nursing', CC.Conference_Content_Category__c); 
            System.assertEquals('Development', CC.Conference_Content_Category__c); 
        }
    
        //CE Categories
        CategoriesController.SelectedCECategorySelectOpts = test_options ;
        
        List<CE_Category__c> CurrentCECategories = [SELECT Name from CE_Category__c];
        
        for (CE_Category__c CE: CurrentCECategories){
            System.assertEquals('Nursing', CE.Name); 
            System.assertEquals('Development', CE.Name); 
        }
    
*/  
    } 
    
    
}