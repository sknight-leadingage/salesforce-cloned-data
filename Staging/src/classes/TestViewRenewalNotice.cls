@isTest
private class TestViewRenewalNotice {
	static final String PRODUCT_NAME = 'Membership Product';
	static final Decimal PRODUCT1_LIST_PRICE = 500.0;
	static final Decimal PRODUCT2_LIST_PRICE = 750.0;
	static final Decimal OPTIONAL_PRODUCT_LIST_PRICE = 250.0;
	
    static NU__Entity__c entity = null;
    static NU__Entity__c entity2 = null;
    
    static NU__Product__c product = null;
    static NU__Product__c product2 = null;
    static NU__Product__c optionalProduct = null;
    static NU__MembershipType__c membershipType = null;
    static NU__MembershipType__c membershipType2 = null;
    static NU__MembershipTypeProductLink__c mtpl = null;
    static NU__MembershipTypeProductLink__c mtpl2 = null;
    
    static Account account = null;
    static Account account2 = null;
    static Account primaryContact = null;
    static Account primaryContact2 = null;
    static NU__Affiliation__c affiliation = null;
    static NU__Affiliation__c affiliation2 = null;
    
    static NU__Membership__c membership = null;
    static NU__Membership__c membership2 = null;
    
    static ViewRenewalNoticeController controller = null;
    static Map<String,Schema.RecordTypeInfo> orderItemRTs = null;
    
    private static void setupTest() {
        entity = NU.DataFactoryEntity.insertEntity();
        entity2 = NU.DataFactoryEntity.insertEntity();
        
        product = NU.DataFactoryProduct.createMembershipProduct(PRODUCT_NAME, PRODUCT1_LIST_PRICE, entity.Id);
        product2 = NU.DataFactoryProduct.createMembershipProduct(PRODUCT_NAME, PRODUCT2_LIST_PRICE, entity2.Id);
        optionalProduct = NU.DataFactoryProduct.createMembershipProduct(PRODUCT_NAME, OPTIONAL_PRODUCT_LIST_PRICE, entity.Id);
        insert new List<NU__Product__c>{product, product2, optionalProduct};
        
        membershipType = DataFactoryMembershipTypeExt.insertProviderMembershipType(entity.Id);
        membershipType2 = DataFactoryMembershipTypeExt.insertProviderMembershipType(entity2.Id);
        mtpl = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(membershipType.Id, product.Id);
        mtpl2 = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(membershipType2.Id, product2.Id);
        
        NU__MembershipTypeProductLink__c mtpl3 = NU.DataFactoryMembershipTypeProductLink.createMembershipTypeProdLink(membershipType.Id, optionalProduct.Id);
        mtpl3.NU__Purpose__c = Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_OPTIONAL;
        insert mtpl3;
        
        account = DataFactoryAccountExt.insertProviderAccount();
        account2 = DataFactoryAccountExt.insertProviderAccount();
        primaryContact = NU.DataFactoryAccount.insertIndividualAccount();
        primaryContact2 = NU.DataFactoryAccount.insertIndividualAccount();
        
        controller = new ViewRenewalNoticeController();
        orderItemRTs = Schema.SObjectType.NU__OrderItem__c.getRecordTypeInfosByName();
    }
    
    static testMethod void NothingPassedIn_Test() {
        setupTest();
        basicAsserts(0);
    }
    
    static testMethod void AccountsPassedIn_NoSelection_Test() {
        setupTest();
        controller.Accounts = new List<Account>{account, account2};
        
        basicAsserts(0); // no membership relationship defined, so accounts are removed
    }
    
    static testMethod void AccountsPassedIn_WithSelection_Test() {
        setupTest();
        RenewalNoticeUtil.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Accounts = new List<Account>{account, account2};
        
        basicAsserts(0); // no memberships found
    }
    
    static testMethod void AccountPassedIn_WithMembership_Test() {
        setupTest();
        insertMemberships();
        
        Test.startTest();        
        RenewalNoticeUtil.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.accountId = account.Id;
        Test.stopTest();
        
        basicAsserts(1);
        renewalWrapperAsserts(ViewRenewalNoticeController.RenewalWrappers, true, null);
    }
    
    static testMethod void AccountsPassedIn_WithMemberships_Test() {
        setupTest();
        insertMemberships();
        
        Test.startTest();        
        RenewalNoticeUtil.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Accounts = new List<Account>{account, account2};
        Test.stopTest();
        
        basicAsserts(2);
        renewalWrapperAsserts(ViewRenewalNoticeController.RenewalWrappers, true, null);
    }
    
    static testMethod void AccountsPassedIn_WithMemberships_InvoiceOverride_Test() {
        setupTest();
        insertMemberships();
        
        Test.startTest();        
        Date dateOverride = Date.today().addDays(-10);
        RenewalNoticeUtil.invoiceDateOverride = dateOverride;
        RenewalNoticeUtil.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Accounts = new List<Account>{account, account2};
        Test.stopTest();
        
        basicAsserts(2);
        renewalWrapperAsserts(ViewRenewalNoticeController.RenewalWrappers, true, dateOverride);
    }
    
    static testMethod void AccountsPassedIn_WithMemberships_PrimaryContacts_Test() {
        setupTest();
        insertMemberships();
        
        affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        affiliation2 = NU.DataFactoryAffiliation.createAffiliation(primaryContact2.Id, account2.Id, true);
        affiliation2.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation2;
        
        // create an order for one of the memberships so that thecode for determining the primary product from the order item line can be tested
        NU__OrderItem__c orderItem = NU.DataFactoryOrderItem.insertOrderItem(entity.Id, account.Id, orderItemRTs.get(Constant.ORDER_RECORD_TYPE_MEMBERSHIP).RecordTypeId);
        NU__OrderItemLine__c orderItemLine = new NU__OrderItemLine__c(NU__Membership__c = membership.Id, NU__MembershipTypeProductLink__c = mtpl.Id, NU__OrderItem__c = orderItem.Id, 
        	NU__Product__c = product.Id, NU__Quantity__c = 1, NU__UnitPrice__c = PRODUCT1_LIST_PRICE);
        insert orderItemLine;
        membership.NU__OrderItemLine__c = orderItemLine.Id;
        update membership;
        
        Test.startTest();        
        RenewalNoticeUtil.affiliationRole = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        RenewalNoticeUtil.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Accounts = new List<Account>{account, account2};
        Test.stopTest();
        
        basicAsserts(2);
        renewalWrapperAsserts(ViewRenewalNoticeController.RenewalWrappers, false, null);
    }
    
    static testMethod void AccountsPassedIn_WithMemberships_PrimaryContacts_BlankAddresses_Test() {
        setupTest();
        insertMemberships();
        setBlankAddresses();
        
        affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        affiliation2 = NU.DataFactoryAffiliation.createAffiliation(primaryContact2.Id, account2.Id, true);
        affiliation2.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation2;
        
        Test.startTest();
        RenewalNoticeUtil.affiliationRole = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        RenewalNoticeUtil.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Accounts = new List<Account>{account, account2};
        Test.stopTest();
        
        basicAsserts(2);
        renewalWrapperAsserts(ViewRenewalNoticeController.RenewalWrappers, true, null);
    }
    
    private static void basicAsserts(Integer accountSize) {
        System.assert(controller.Accounts.size() == accountSize);
        if (accountSize > 0) {
        	System.assert(ViewRenewalNoticeController.RenewalWrappers.size() == controller.Accounts.size());
        }
    }
    
    private static void renewalWrapperAsserts(List<ViewRenewalNoticeController.RenewalWrapper> renewalWrappers, Boolean useOrgAddresses, Date invoiceDate) {
    	for (ViewRenewalNoticeController.RenewalWrapper rw : renewalWrappers) {
    		if (rw.account.Id == account.Id) {
    			System.assert(rw.entity.Id == entity.Id);
    			System.assert(rw.optionalProducts.size() == 1 && rw.optionalProducts.get(0).Id == optionalProduct.Id);
    			System.assert(rw.balance == PRODUCT1_LIST_PRICE + OPTIONAL_PRODUCT_LIST_PRICE);
    			
    			if (affiliation != null) {
    				System.assert(rw.primaryContact.Id == primaryContact.Id);
    			}
    			
    			System.assert(rw.startDate == membership.NU__StartDate__c.addYears(1));
    			System.assert(rw.endDate == membership.NU__EndDate__c.addYears(1));
    			System.assert(rw.membershipType.Id == membershipType.Id);
    		}
    		else {
    			System.assert(rw.entity.Id == entity2.Id);
    			System.assert(rw.optionalProducts.size() == 0);
    			System.assert(rw.balance == PRODUCT2_LIST_PRICE);
    			
    			if (affiliation2 != null) {
    				System.assert(rw.primaryContact.Id == primaryContact2.Id);
    			}
    			
    			System.assert(rw.startDate == membership2.NU__StartDate__c.addYears(1));
    			System.assert(rw.endDate == membership2.NU__EndDate__c.addYears(1));
    			System.assert(rw.membershipType.Id == membershipType2.Id);
    		}
    		
    		System.assert(rw.useOrgAddresses == useOrgAddresses);
    		System.assert(rw.primaryProducts.size() == 1);
    		System.assert(rw.requiredProducts.size() == 0);
    		
    		if (invoiceDate != null) {
    			System.assert(rw.invoiceDate == invoiceDate);
    		}
    	}
    }
    
    private static void insertMemberships() {
        membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(account.Id, membershipType.Id);
        membership2 = DataFactoryMembershipExt.createCurrentCalendarYearMembership(account2.Id, membershipType2.Id);
        insert new List<NU__Membership__c>{membership,membership2};
    }
    
    private static void setBlankAddresses() {
    	setBlankAddress(primaryContact);
    	setBlankAddress(primaryContact2);
    	setBlankAddress(account);
    	setBlankAddress(account2);

    	update new List<Account>{primaryContact, primaryContact2, account, account2};
    }
    
    private static void setBlankAddress(Account acct){
    	acct.BillingStreet = null;
    	acct.BillingCity = null;
    	acct.BillingState = null;
    	acct.BillingPostalCode = null;
    	acct.BillingCountry = null;
    }
}