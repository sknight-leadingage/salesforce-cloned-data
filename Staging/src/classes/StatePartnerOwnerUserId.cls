public class StatePartnerOwnerUserId {
	public static final Id GeneralStatePartnerUserId { get; set; }
	public static final Id LeadingAgeIntegrationUserId { get; set; }
	
	static {
		State_Partner_Owners__c owners = State_Partner_Owners__c.getInstance();
		
		if (Test.isRunningTest() &&
		    owners.Id == null){

			owners.General_State_Partner_User_Id__c = UserQuerier.GeneralStatePartnerUser.Id;
			owners.LeadingAge_Integration_User_Id__c = UserQuerier.LeadingAgeIntegrationUser.Id;
		}
		
		GeneralStatePartnerUserId = owners.General_State_Partner_User_Id__c;
		LeadingAgeIntegrationUserId = owners.LeadingAge_Integration_User_Id__c;
	}
}