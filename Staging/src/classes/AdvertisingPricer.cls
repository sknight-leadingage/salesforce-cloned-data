public with sharing class AdvertisingPricer implements IProductPricer {
	public static final Decimal CORPORATE_ALLIANCE_DISCOUNT_PERCENTAGE = 0.15;
	
	public static Decimal applyCorporateAllianceDiscount(Decimal regularPrice){
		return regularPrice * (1 - CORPORATE_ALLIANCE_DISCOUNT_PERCENTAGE);
	}
	
	public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
		if (productToPrice.RecordType.Name == Constant.ORDER_RECORD_TYPE_ADVERTISING &&
		    customer.NU__Member__c == 'Yes' &&
		    customer.NU__MembershipType__c == Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_NAME){
		    
		    DefaultProductPricer defaultPricer = new DefaultProductPricer();
		    Decimal regularPrice = defaultPricer.calculateProductPrice(productToPrice, customer, specialPrice, productPricingRequest, extraParams);
		    
		    Decimal discountedPrice = applyCorporateAllianceDiscount(regularPrice);
		    
		    return discountedPrice;
	    }

		return null;
	}
}