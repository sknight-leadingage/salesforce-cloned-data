/* This batch job updates all the affiliations' Parent State Partner Name and
   Grandparent State Partner Name field daily so that they remain updated after
   the affiliation records are created.
 */

global class BatchUpdateAffiliationStateNames implements Database.Batchable<SObject>, Schedulable {
	
   global static void scheduleAt5AM() {
      System.schedule('Batch Update Affiliation State Names', '0 0 5 * * ?', new BatchUpdateAffiliationStateNames());
   }
	
   global void execute(SchedulableContext context) {
      Database.executeBatch(new BatchUpdateAffiliationStateNames());
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(
          [SELECT Id,
                  NU__ParentAccount__r.State_Partner_Id__r.Name,
                  NU__ParentAccount__r.NU__PrimaryAffiliation__r.State_Partner_Id__r.Name
             FROM NU__Affiliation__c
            WHERE Update_State_Names_Needed__c = true]);
   }

   global void execute(Database.BatchableContext BC, List<NU__Affiliation__c> affiliations){
   	  NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTrigger');
      NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggers');
   	
      for(NU__Affiliation__c affil : affiliations){
          affil.Parent_State_Partner_Name__c = affil.NU__ParentAccount__r.State_Partner_Id__r.Name;
          affil.Grandparent_State_Partner_Name__c = affil.NU__ParentAccount__r.NU__PrimaryAffiliation__r.State_Partner_Id__r.Name;
      }
      
      update affiliations;
   }

   global void finish(Database.BatchableContext BC){

   }
}