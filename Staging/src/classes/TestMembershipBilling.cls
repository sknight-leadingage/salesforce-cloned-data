/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMembershipBilling {
	
	// basic data
	static NU__Entity__c entity = null;
	static NU__MembershipType__c membershipType = null;
	static NU__MembershipTypeProductLink__c mtpl = null;
	static NU__Membership__c membership = null;
	static NU__Donation__c donation = null;
	static NU__Product__c product = null;
	static NU__Product__c donationProduct = null;

	// for membership billing records
	static MembershipBilling__c memberBilling = null;
	static String year = String.valueOf(Date.today().year());
	
	// basic account data
	static Account individual = null;
	static Decimal billAmount = 1000.0;
	
	static NU__OrderItem__c orderItem = null;
	static NU__OrderItemLine__c orderItemLine = null;
	static NU__OrderItem__c donationOrderItem = null;
	static NU__OrderItemLine__c donationOrderItemLine = null;
	
	static Map<String, Schema.RecordTypeInfo> orderItemRecordTypes = Schema.SObjectType.NU__OrderItem__c.getRecordTypeInfosByName();
	
	private static void setupTest() {
		entity = NU.DataFactoryEntity.insertEntity();
		membershipType = NU.DataFactoryMembershipType.insertDefaultMembershipType(entity.Id);
		
		memberBilling = new MembershipBilling__c(Year__c = year, MembershipType__c = membershipType.Id); 
		insert memberBilling;
		
		individual = NU.DataFactoryAccount.insertIndividualAccount();
	}
	
    static testMethod void InsertMembershipBillingRecordTest() {
        setupTest();
        
        List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);
    }
    
    static testMethod void InsertMembershipBillingRecordThatAlreadyExistsTest() {
        setupTest();
        
        Test.startTest();
        MembershipBilling__c memberBilling2 = new MembershipBilling__c(Year__c = year, MembershipType__c = membershipType.Id);
        try {
        	insert memberBilling2;
        }
        catch (System.DmlException e) {
        	System.assert(ApexPages.hasMessages() == true);
        	System.assert(ApexPages.getMessages().get(0).getDetail() == MembershipBilling.DUPLICATE_MEMBERSHIP_BILLING_FOUND);
        }
        Test.stopTest();
        
        List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);
    }
    
    static testMethod void InsertMultipleMembershipBillingRecordsTest() {
        setupTest();
        
        Test.startTest();
        MembershipBilling__c memberBilling2 = new MembershipBilling__c(Year__c = String.valueOf(Date.today().year()+1), MembershipType__c = membershipType.Id);
        MembershipBilling__c memberBilling3 = new MembershipBilling__c(Year__c = String.valueOf(Date.today().year()+2), MembershipType__c = membershipType.Id);
        insert new List<MembershipBilling__c>{memberBilling2,memberBilling3};
        Test.stopTest();
        
        List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 3);
    }
    
    static testMethod void BasicBillMembershipTest() {
    	setupTest();
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 0);
    	
    	Test.startTest();
    	Map<Id,List<MembershipBillingInfo>> mbis = new Map<Id,List<MembershipBillingInfo>>();
    	MembershipBillingInfo mbi = new MembershipBillingInfo(individual.Id, billAmount, year, membershipType.Id, null, null, null);
    	mbis.put(individual.Id,new List<MembershipBillingInfo>{mbi});
    	
    	MembershipBilling.memberBillingInfos = mbis;
    	MembershipBilling.Bill();
    	Test.stopTest();
    	
    	List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingAsserts(mb, 0.0, 0.0, 0.0, 0, 0, 0, 0.0);
        billingLineAsserts(mbl, individual.Id, billAmount, Date.today(), 0.0, null, 0.0, mb.Id, null);
    }
    
    static testMethod void BillMembershipAndUpdateBillAmountTest() {
    	setupTest();
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 0);
    	
    	Test.startTest();
    	Map<Id,List<MembershipBillingInfo>> mbis = new Map<Id,List<MembershipBillingInfo>>();
    	MembershipBillingInfo mbi = new MembershipBillingInfo(individual.Id, billAmount, year, membershipType.Id, null, null, null);
    	mbis.put(individual.Id,new List<MembershipBillingInfo>{mbi});
    	
    	MembershipBilling.memberBillingInfos = mbis;
    	MembershipBilling.Bill();
    	
    	// update pricing and bill them again
    	mbi.membershipPrice = 1500.0;
    	MembershipBilling.memberBillingInfos = mbis;
    	MembershipBilling.Bill();
    	Test.stopTest();
    	
    	memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingLineAsserts(mbl, individual.Id, 1500.0, Date.today(), 0.0, null, 0.0, memberBilling.Id, null);
    }
    
    static testMethod void BillMembershipWithNoExistingMembershipBillingTest() {
    	setupTest();
    	
    	year = String.valueOf(Date.today().year()+1);
    	List<MembershipBilling__c> memberBillings = [SELECT Id FROM MembershipBilling__c WHERE Year__c = :year AND MembershipType__c = :membershipType.Id];
        System.assert(memberBillings != null && memberBillings.size() == 0);
    	
    	Test.startTest();
    	Map<Id,List<MembershipBillingInfo>> mbis = new Map<Id,List<MembershipBillingInfo>>();
    	MembershipBillingInfo mbi = new MembershipBillingInfo(individual.Id, billAmount, year, membershipType.Id, null, null, null);
    	mbis.put(individual.Id,new List<MembershipBillingInfo>{mbi});
    	
    	MembershipBilling.memberBillingInfos = mbis;
    	MembershipBilling.Bill();
    	Test.stopTest();
    	
    	memberBillings = [SELECT Id FROM MembershipBilling__c WHERE Year__c = :year AND MembershipType__c = :membershipType.Id];
    	System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(mb.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingLineAsserts(mbl, individual.Id, billAmount, Date.today(), 0.0, null, 0.0, mb.Id, null);
    }
    
    static testMethod void BasicBillMembershipAndInsertOrderItemLineTest() {
    	setupTest();
    	
    	Test.startTest();
    	Map<Id,List<MembershipBillingInfo>> mbis = new Map<Id,List<MembershipBillingInfo>>();
    	MembershipBillingInfo mbi = new MembershipBillingInfo(individual.Id, billAmount, year, membershipType.Id, null, null, null);
    	mbis.put(individual.Id,new List<MembershipBillingInfo>{mbi});
    	
    	MembershipBilling.memberBillingInfos = mbis;
    	MembershipBilling.Bill();
    	
    	insertMembershipOrder();
    	Test.stopTest();
    	
    	List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        //billingAsserts(mb, 0.0, 0.0, 0.0, 0, 0, 1, 0.0);
        //billingLineAsserts(mbl, individual.Id, billAmount, Date.today(), 0.0, null, 0.0, mb.Id, orderItemLine.Id);
    }
    
    static testMethod void BillMembershipAndInsertOrderItemLinesTest() {
    	setupTest();
    	
    	Test.startTest();
    	Map<Id,List<MembershipBillingInfo>> mbis = new Map<Id,List<MembershipBillingInfo>>();
    	MembershipBillingInfo mbi = new MembershipBillingInfo(individual.Id, billAmount, year, membershipType.Id, null, null, null);
    	mbis.put(individual.Id,new List<MembershipBillingInfo>{mbi});
    	
    	MembershipBilling.memberBillingInfos = mbis;
    	MembershipBilling.Bill();
    	
    	insertMembershipOrder();
    	insertDonationOrderItemLine();
    	Test.stopTest();
    	
    	List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingLineAsserts(mbl, individual.Id, billAmount, Date.today(), 0.0, donationOrderItemLine.Id, 0.0, mb.Id, orderItemLine.Id);
    }
    
    static testMethod void InsertOrderItemLinesWithNoBillingTest() {
    	setupTest();
    	
    	Test.startTest();
    	insertMembershipOrder();
    	insertDonationOrderItemLine();
    	Test.stopTest();
    	
    	List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingLineAsserts(mbl, individual.Id, orderItemLine.NU__UnitPrice__c, Date.today(), 0.0, donationOrderItemLine.Id, 0.0, mb.Id, orderItemLine.Id);
    }
    
    static testMethod void CancelOrderItemLinesTest() {
    	setupTest();
    	
    	Test.startTest();
    	insertMembershipOrder();
    	insertDonationOrderItemLine();
    	Test.stopTest();
    	
    	orderItemLine.NU__Status__c = Constant.ORDER_ITEM_LINE_STATUS_CANCELLED;
    	donationOrderItemLine.NU__Status__c = Constant.ORDER_ITEM_LINE_STATUS_CANCELLED;
    	update new List<NU__OrderItemLine__c>{orderItemLine,donationOrderItemLine};
    	
    	List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingLineAsserts(mbl, individual.Id, orderItemLine.NU__UnitPrice__c, Date.today(), 0.0, null, 0.0, mb.Id, null);
    }
    
    static testMethod void DoNotCalculateTotalsTest() {
    	setupTest();
    	
    	Test.startTest();
    	memberBilling.DoNotCalculateTotals__c = true;
    	update memberBilling;
    	
    	insertMembershipOrder();
    	Test.stopTest();
    	
    	List<MembershipBilling__c> memberBillings = mbQuery();
        System.assert(memberBillings != null && memberBillings.size() == 1);        
        MembershipBilling__c mb = memberBillings.get(0);
    	
    	List<MembershipBillingLine__c> memberBillingLines = mblQuery(memberBilling.Id);
        System.assert(memberBillingLines != null && memberBillingLines.size() == 1);        
        MembershipBillingLine__c mbl = memberBillingLines.get(0);
        
        billingAsserts(mb, 0.0, 0.0, 0.0, 0, 0, 0, 0.0);
        billingLineAsserts(mbl, individual.Id, orderItemLine.NU__UnitPrice__c, Date.today(), 0.0, null, 0.0, mb.Id, orderItemLine.Id);
    }
    
    private static void insertMembershipOrder() {
    	product = NU.DataFactoryProduct.insertDefaultMembershipProduct(entity.Id);
    	mtpl = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(membershipType.Id, product.Id);
    	membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(individual.Id, membershipType.Id);
		insert membership;
    	    	
    	orderItem = NU.DataFactoryOrderItem.insertOrderItem(entity.Id, individual.Id, orderItemRecordTypes.get('Membership').RecordTypeId);
    	orderItemLine = new NU__OrderItemLine__c(NU__OrderItem__c = orderItem.Id, NU__Membership__c = membership.Id, NU__MembershipTypeProductLink__c = mtpl.Id, 
    		NU__Product__c = product.Id, NU__Quantity__c = 1, NU__UnitPrice__c = product.NU__ListPrice__c, NU__Status__c = Constant.ORDER_ITEM_LINE_STATUS_ACTIVE);
    	insert orderItemLine;
    }
    
    private static void insertDonationOrderItemLine() {
    	donationProduct = NU.DataFactoryProduct.insertDonationProduct(entity.Id);
    	donation = new NU__Donation__c(NU__Account__c = individual.Id, NU__Product__c = donationProduct.Id, NU__Membership__c = membership.Id);
    	insert donation;
    	
    	donationOrderItem = NU.DataFactoryOrderItem.insertOrderItem(entity.Id, individual.Id, orderItemRecordTypes.get('Donation').RecordTypeId);
    	donationOrderItemLine = new NU__OrderItemLine__c(NU__OrderItem__c = orderItem.Id, NU__Membership__c = membership.Id, NU__Donation__c = donation.Id, 
    		NU__Product__c = donationProduct.Id, NU__Quantity__c = 1, NU__UnitPrice__c = donationProduct.NU__ListPrice__c, NU__Status__c = Constant.ORDER_ITEM_LINE_STATUS_ACTIVE);
    	insert donationOrderItemLine;
    }
    
    private static void billingAsserts(MembershipBilling__c mb, Decimal amountBilled, Decimal donationAmountReceived, Decimal duesAmountReceived, Integer membershipsBilled, Integer membershipsRenewed, Integer totalMemberships, Decimal totalAmountReceived) {
    	System.assert(mb.AmountBilled__c == amountBilled, 'Expected ' + amountBilled + ', not ' + mb.AmountBilled__c);
    	System.assert(mb.DonationAmountReceived__c == donationAmountReceived, 'Expected ' + donationAmountReceived + ', not ' + mb.DonationAmountReceived__c);
    	System.assert(mb.DuesAmountReceived__c == duesAmountReceived, 'Expected ' + duesAmountReceived + ', not ' + mb.DuesAmountReceived__c);
    	System.assert(mb.MembershipsBilled__c == membershipsBilled, 'Expected ' + membershipsBilled + ', not ' + mb.MembershipsBilled__c);
    	System.assert(mb.MembershipsRenewed__c == membershipsRenewed, 'Expected ' + membershipsRenewed + ', not ' + mb.MembershipsRenewed__c);
    	System.assert(mb.TotalMemberships__c == totalMemberships, 'Expected ' + totalMemberships + ', not ' + mb.TotalMemberships__c);
    	System.assert(mb.TotalAmountReceived__c == totalAmountReceived, 'Expected ' + totalAmountReceived + ', not ' + mb.TotalAmountReceived__c);
    }
    
    private static void billingLineAsserts(MembershipBillingLine__c mbl, Id accountId, Decimal amountBilled, Date billDate, Decimal donationAmountReceived, Id donationOIL, Decimal duesAmountReceived, Id memberBillingId, Id membershipOIL) {
    	System.assert(mbl.Account__c == accountId);
    	System.assert(mbl.AmountBilled__c == amountBilled, 'Expected ' + amountBilled + ', not ' + mbl.AmountBilled__c);
    	System.assert(mbl.BillDate__c == billDate, 'Expected ' + billDate + ', not ' + mbl.BillDate__c);
    	System.assert(mbl.DonationAmountReceived__c == donationAmountReceived, 'Expected ' + donationAmountReceived + ', not ' + mbl.DonationAmountReceived__c);
    	System.assert(mbl.DonationOrderItemLine__c == donationOIL, 'Expected ' + donationOIL + ', not ' + mbl.DonationOrderItemLine__c);
    	System.assert(mbl.DuesAmountReceived__c == duesAmountReceived, 'Expected ' + duesAmountReceived + ', not ' + mbl.DuesAmountReceived__c);
    	System.assert(mbl.MembershipBilling__c == memberBillingId, 'Expected ' + memberBillingId + ', not ' + mbl.MembershipBilling__c);
    	System.assert(mbl.MembershipOrderItemLine__c == membershipOIL, 'Expected ' + membershipOIL + ', not ' + mbl.MembershipOrderItemLine__c);
    }
    
    private static List<MembershipBilling__c> mbQuery() {
    	return [SELECT Id, AmountBilled__c, DonationAmountReceived__c, DuesAmountReceived__c, MembershipsBilled__c, MembershipsRenewed__c, TotalMemberships__c, TotalAmountReceived__c FROM MembershipBilling__c];
    }
    
    private static List<MembershipBillingLine__c> mblQuery(Id membershipBillingId) {
    	return [SELECT Id, Account__c, AmountBilled__c, BillDate__c, DonationAmountReceived__c, DonationOrderItemLine__c, DuesAmountReceived__c, MembershipBilling__c, MembershipOrderItemLine__c FROM MembershipBillingLine__c WHERE MembershipBilling__c = :membershipBillingId];
    }
}