public class ConferenceNavComponentController {
    public Id SelectedEventId { get; set; }
    
    public String ConferenceProposalsURL {
        get { return viewConferenceProposals().getUrl(); }
    }
    
    public String ConferenceSessionsURL {
        get { system.debug (viewConferenceSessions().getUrl());
        	return viewConferenceSessions().getUrl(); }
    }
    
    public String ConferenceSessionsHubURL {
        get { system.debug (viewConferenceSessionsHub().getUrl());
        	return viewConferenceSessionsHub().getUrl(); }
    }

    public String ConferenceSpeakersURL {
        get { return viewConferenceSpeakers().getUrl(); }
    }
    
    public String ConferenceSpeakersHubURL {
        get { return viewConferenceSpeakersHub().getUrl(); }
    }
    
    public String ConferenceCategoriesURL {
        get { return viewConferenceCategories().getUrl(); }
    }

    public String ConferenceReportsURL {
        get { return viewConferenceReports().getUrl(); }
    }
    
    public PageReference viewConferenceProposals(){
        return ConferenceNavUtil.viewConferenceProposals(SelectedEventId);
    }
    
    public PageReference viewConferenceSessions(){
        return ConferenceNavUtil.viewConferenceSessions(SelectedEventId);
    }
    
    public PageReference viewConferenceSessionsHub(){
        return ConferenceNavUtil.viewConferenceSessionsHub(SelectedEventId);
    }
    
    public PageReference viewConferenceSpeakers(){
        return ConferenceNavUtil.viewConferenceSpeakers(SelectedEventId);
    }
    
    public PageReference viewConferenceSpeakersHub(){
        return ConferenceNavUtil.viewConferenceSpeakersHub(SelectedEventId);
    }
    
    public PageReference viewConferenceReports(){
        return ConferenceNavUtil.viewConferenceReports(SelectedEventId);
    }
    
    public PageReference viewConferenceCategories(){
        return ConferenceNavUtil.viewConferenceCategories(SelectedEventId);
    }
}