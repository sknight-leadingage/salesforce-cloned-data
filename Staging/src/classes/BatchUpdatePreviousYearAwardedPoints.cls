//annually bulk update all Priority Points fields on all account records, according to LeadingAge exhibitor points rules.
//adds the current year's points value to the previous years' points value, then clears all current year fields for the upcoming year 

//run as a batch apex job because it affects more than 100 accounts. 

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new BatchUpdatePreviousYearAwardedPoints());

//To schedule in an anonymous window in Eclipse or Developer Console:
//BatchUpdatePreviousYearAwardedPoints.schedule();
//but it is preferable to only run this on demand, since it only happens once a year

//To unschedule, delete the job from the list of scheduled jobs.

//Developed by NimbleUser, 2013 (MG, NF)

global class BatchUpdatePreviousYearAwardedPoints implements Database.Batchable<sObject>, Schedulable
{

    //batch apex jobs require three methods: start, execute, and finish; run in that order
    //schedulable batch apex jobs require one method: execute

    //batch apex methods:

    global final String Query = 'SELECT ID, Current_Year_Exhibitor__c, Current_Year_Priority_Points__c, Current_Year_Priority_Point_Adjustments__c, Previous_Years_Priority_Points__c FROM Account WHERE IsPersonAccount = false';
   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
       List<Account> accounts = new List<Account>();
       
       for(sobject s : scope)
       {
            Account a = (Account)s;

            //preset account values to zero, when null
            if(a.Current_Year_Priority_Points__c == null)
            {
                a.Current_Year_Priority_Points__c = 0;
            }
            
            if(a.Current_Year_Priority_Point_Adjustments__c == null)
            {
                a.Current_Year_Priority_Point_Adjustments__c = 0;
            }
            
            if(a.Previous_Years_Priority_Points__c == null)
            {
                a.Previous_Years_Priority_Points__c = 0;
            }
             
            //add the current year's points value to the previous years' points value, then clear all current year fields for the upcoming year 
            if(a.Current_Year_Priority_Points__c != 0 || a.Current_Year_Priority_Point_Adjustments__c != 0 || a.Current_Year_Exhibitor__c)
            {
                a.Current_Year_Exhibitor__c = false;
                a.Previous_Years_Priority_Points__c = a.Previous_Years_Priority_Points__c + a.Current_Year_Priority_Points__c + a.Current_Year_Priority_Point_Adjustments__c;
                a.Current_Year_Priority_Points__c = 0;
                a.Current_Year_Priority_Point_Adjustments__c = 0;
                accounts.add(a);
            }          
       }

        //update accounts
       if(accounts.size() > 0)
       {
            update accounts;
       }
   }

   global void finish(Database.BatchableContext BC)
   {
        //do nothing upon finish. could send an email upon success or failure, if desired.
   }

    //scheduled batch apex methods:

    //to schedule daily at 3am (disabled because this is an annual job)
    public static void schedule() {
        //System.schedule('Exhibitor Priority Points Annual Batch Update', '0 0 3 * * ?', new BatchUpdatePreviousYearAwardedPoints());
    }

    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }

}