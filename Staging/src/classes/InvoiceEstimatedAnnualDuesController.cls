// visualforce page controller for InvoiceEstimatedAnnualDues (Estimated Annual Dues tab) in the
// State Partner Portal and also available to LeadingAge staff in Salesforce.
// The setup code is very similar to other State Partner Portal pages and could be refactored into
// a generic set of controls for all similar pages -NF


public class InvoiceEstimatedAnnualDuesController {
    
    // Generating Excl file
    
    public string PageCType { get; set; } 

    public void SetPageType()
       {
           PageCType = 'application/vnd.ms-excel#Estimated_Annual_Dues.xls';  
       }
    //public List <Account> ProviderAccounts { get; set;} //the rows in this report
    //public List <Joint_Billing_Item__c> ProviderAccountsJBI { get; set;} //the rows in this report
    public string SelectedState {get; set; } //the state to be included in the report
    public static Decimal SelectedStateBillingQuarter {get; set;}

    private static final Integer THISYEAR = Date.today().year(); //the year is always the current year

    public static Integer ThisYearCalculated {
        get {
            Integer y = Date.today().year();
            Integer m = Date.today().month();
            if (SelectedStateBillingQuarter != null) {
                Decimal SelectedStateBillingQuarterStartingMonth = ((SelectedStateBillingQuarter - 1) * 3) + 1;
                
                if (SelectedStateBillingQuarterStartingMonth > m)
                   y = y - 1;
                return y;
            }
            return 1;
        }
    }
    
    public string SelectedYearString {get{return string.valueOf(ThisYearCalculated);}} //string representation of SelectedYear

    private User currentUser {get; set;}
    private User p_pageUser = null;

    //stores a combination of account details, dues for this year, and last year; for a single account
    public class providerRecord1 {
        public Decimal estimatedDues {get; set;}
        public Account accountDetail {get; set;}
        public Account SelectedYearMembership {get; set;}
        //public Joint_Billing_Item__c SelectedPreviousYearMembershipSummary {get; set;}
        public Decimal SelectedPreviousYearMembershipSummary {get; set;}
        public Joint_Billing_Item_Line__c SelectedPreviousYearMembership {get; set;}
    }

    //constructor
    public InvoiceEstimatedAnnualDuesController() {
        //get user information    
        Id userId = UserInfo.getUserId();
        system.debug('Current User Id = ' + userId);
        currentUser = [Select id, name, Contact.Account.State_Partner_Id__c, CompanyName, Contact.Account.State_Partner_Id__r.Dues_Billing_Quarter__c, Profile.Name, UserRole.Name from User where id = :userId];

        system.debug('Current User Company Name = ' + currentUser.CompanyName);
        system.debug('Current User isUserLeadingAgeStaff = ' + String.ValueOf(isUserLeadingAgeStaff));
        system.debug('Current User State Partner Id = ' + String.ValueOf(currentUser.Contact.Account.State_Partner_Id__c));

        Decimal billingQtr = 0.0;
        //get form information
        if (!isUserLeadingAgeStaff){
            if (currentUser.Contact.Account.State_Partner_Id__c == null) {
                //currentUser = Database.query('Select id, name, Contact.Account.State_Partner_Id__c, Contact.Account.State_Partner_Id__r.Dues_Billing_Quarter__c, Profile.Name, UserRoleId, UserRole.Name from User where UserRole.Name like \'%' + currentUser.UserRole.Name + '%\' AND Contact.Account.State_Partner_Id__c != null LIMIT 1');
                List<Account> aStatePartnerList = Database.query('SELECT Id, Dues_Billing_Quarter__c FROM Account WHERE ((Name LIKE \'%' + currentUser.UserRole.Name + '%\' OR Name = \'' + currentUser.CompanyName + '\') AND Id != \'001d000000zu5uk\') AND RecordTypeId = \'' + Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID + '\' LIMIT 1');
                if (aStatePartnerList.size() > 0) {
                    SelectedState = String.ValueOf(aStatePartnerList[0].Id);
                    
                    system.debug('Current User SelectedState = ' + SelectedState);
                    billingQtr = aStatePartnerList[0].Dues_Billing_Quarter__c;
                }
            }
            else {
                SelectedState = String.ValueOf(currentUser.Contact.Account.State_Partner_Id__c);
                billingQtr = currentUser.Contact.Account.State_Partner_Id__r.Dues_Billing_Quarter__c;
            }
        }
        else {
            if (SelectedState != null) {
                billingQtr = [SELECT Dues_Billing_Quarter__c FROM Account WHERE Id = :SelectedState].Dues_Billing_Quarter__c;
            }
        }
        //below works for states, but not when logged in as LeadingAge Staff
        //SelectedStateBillingQuarter = currentUser.Contact.Account.State_Partner_Id__r.Dues_Billing_Quarter__c;
        if (SelectedState != null) {
            SelectedStateBillingQuarter = billingQtr; 
        }

        //run report
        //no action required; visualforce page calls estimatedDuesData
    }

    //outputs a (drop-down) list of State Partners, visible to LeadingAge Staff only 
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Account> lData = [
            SELECT ID,
                Name,
                BillingState
            FROM Account
            WHERE RecordType.DeveloperName = 'State_Partner'
            ORDER BY Name];
        for (Account a: lData) {
            options.add(new SelectOption(a.ID, a.Name));
        }
        return options;
    }

    //re-runs the report, if the state is changed in the drop-down list visible to LeadingAge staff only
    public void State_onChange() {
        Run();
    }

    //re-runs the report, if the run button is clicked
    public void Run() {
        if (SelectedState != null){
            List<Account> accSpBillQtr = [SELECT Id, Dues_Billing_Quarter__c FROM Account WHERE Id = :SelectedState LIMIT 1];
            if (accSpBillQtr.size() > 0 && accSpBillQtr[0].Dues_Billing_Quarter__c != null) {
                SelectedStateBillingQuarter = accSpBillQtr[0].Dues_Billing_Quarter__c;
            }
            else {
                SelectedStateBillingQuarter = 1;
            }
        }

        estimatedDuesData_Priv = null;
        CustomPricingManager.emptyAccountAndAccountMembershipCaches();
    }

    //retireve information about the user
    private User pageUser
    {
        get
        {
            if (p_pageUser == null)
            {
                p_pageUser = [SELECT ID, CompanyName, Contact.Account.ID FROM User WHERE ID = :UserInfo.getUserID()];
            }
            return p_pageUser;
        }
    }

    //determine if the user has access to all state partner reports, or just a single state partner report
    //When logged in as a State Partner Portal user, use the user --> Contact --> Account.State_Partner_Id
    //to determine the corresponding "State Partner" account.

    //SK TODO 20130308: Need to find out how to do this by id... or if this is even correct

    public boolean isUserLeadingAgeStaff
    {
        get
        {
            return (pageUser.CompanyName != null && pageUser.CompanyName == 'LeadingAge');
        }
    }
    
    private transient List <providerRecord1> estimatedDuesData_Priv = null;
     
    //retrieve all account records for a selected state, estimated dues, plus joint billing data for last year
    public List <providerRecord1> estimatedDuesData {
        get {
            if (estimatedDuesData_Priv == null || estimatedDuesData_Priv.isEmpty()) {
                List<Account> thisYearsDues = getProviderAccountsEstimated();
            
                Map<Id, Joint_Billing_Item__c> prevYearsDues = getJBIMap(getProviderAccountsForYear('all', ThisYearCalculated-1));
                estimatedDuesData_Priv = getProviderRecordList(thisYearsDues, prevYearsDues);
            }
            
            return estimatedDuesData_Priv;
        }
    }

    //totals for all accounts
    public Decimal estimatedDuesDataGrossAnnualDues {
        get {
            Decimal grossAD = 0;
            
            for (providerRecord1 a : estimatedDuesData){
                grossAD += a.estimatedDues;
            }
            
            return grossAD;
        }
    }

    public Decimal estimatedDuesDataServiceFee {
        get {
            return estimatedDuesDataGrossAnnualDues * -0.15;
        }
    }
    
    public Decimal estimatedDuesDataNetLeadingAgeDues {
        get {
            return estimatedDuesDataGrossAnnualDues + estimatedDuesDataServiceFee;
        }
    }


    //build a list of provider accounts from a combination of account, and dues for this year and last year
    private List <providerRecord1> getProviderRecordList (List<Account> thisYearsDues, Map<Id, Joint_Billing_Item__c> prevYearsDues) {
            List <providerRecord1> prList = new List <providerRecord1>();


            //cycle through accounts and build ProviderRecords for each (representing the rows of the report)
            for (Account a : thisYearsDues) {
                providerRecord1 pr = new providerRecord1();

                //attach the account data to the providerRecord
                pr.accountDetail = a;
                
                //attach last year's dues to the providerRecord
                if (prevYearsDues.containsKey(a.Id)) {
                    Joint_Billing_Item__c jbi = prevYearsDues.get(a.Id);
                    if (jbi != null)
                        pr.SelectedPreviousYearMembershipSummary = jbi.Amount__c;
                    if (!jbi.Joint_Billing_Item_Lines__r.isEmpty())
                        pr.SelectedPreviousYearMembership = jbi.Joint_Billing_Item_Lines__r [0];
                }
                
                if (a.Estimated_Dues_Pro_Forma__c == null){
                    pr.estimatedDues = 0;
                }
                else{
                    pr.estimatedDues = a.Estimated_Dues_Pro_Forma__c;                   
                }
                
                
                prList.add(pr);
            }
            
            return prList;
    }

    //create map from a list, with account as the key
    private Map<Id, Joint_Billing_Item__c> getJBIMap(List<Joint_Billing_item__c> jbiList) {
        Map<Id, Joint_Billing_Item__c> jbiMap = new Map<Id, Joint_Billing_Item__c>();
 
        for (Joint_Billing_Item__c a : jbiList){
            jbiMap.put(a.Account__r.Id, a);
        }

        return jbiMap;
    }

    //retrieve all account records for a selected state, plus joint billing data for this year and last year
    private List<Account> getProviderAccountsEstimated(){
        if (selectedState == null || selectedState == '-1')
        {
            return new List<Account>(); 
        }
        else
        {
            System.Debug('#####' + Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER + Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE + selectedState);
            return [SELECT
                id,
                LeadingAge_ID__c,
                Name,
                NU__RecordTypeName__c,
                Provider_Join_On__c,
                Provider_Lapsed_On__c,
                Multi_Site_Corporate__c,
                NU__PrimaryAffiliation__r.RecordTypeId,
                NU__PrimaryAffiliation__r.RecordType.Name,
                NU__PrimaryAffiliation__r.NU__RecordTypeName__c,
                NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c,
                NU__PrimaryAffiliation__r.Multi_Site_Corporate__c,
                Primary_Affiliation_Account_Name__c,
                Revenue_Year_Submitted__c, 
                Program_Service_Revenue__c,
                Dues_Price__c,
                Dues_Price_Override__c,
                Multi_Site_Dues_Price__c,
                Multi_Site_Program_Service_Revenue__c,
                Multi_Site_Enabled__c,
                Provider_Member__c,
                LeadingAge_Member_Company__c,
                OwnerId,
                Owner.Name,
                Estimated_Dues_Pro_Forma__c
             FROM Account
             WHERE ((RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER and (NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c = false)) // OR NU__PrimaryAffiliation__c = null))
             OR (RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE and Multi_Site_Corporate__c = true))
             AND State_Partner_ID__c = :selectedState
             AND Provider_Member__c = 'Yes'
             ORDER BY name ASC];             
        }
    }

    //retrieve accounts for a specified year (last year)
    private List<Joint_Billing_Item__c> getProviderAccountsForYear(string action, integer yr) {
        if (SelectedState == null || SelectedState == '-1')
        {
            return new List<Joint_Billing_Item__c>(); 
        }
        else
        {
            integer prevCalYearForBillingYear = yr;
            if (SelectedStateBillingQuarter != null && SelectedStateBillingQuarter > 1) {
                //For states that bill after January, the previous billing year will be the current calendar year
              prevCalYearForBillingYear = prevCalYearForBillingYear + 1;
            }
        
            String querySelect = 'SELECT ' +
                'Account__r.id, ' +
                'Account__r.LeadingAge_ID__c, ' +
                'Account__r.Name, ' +
                'Account__r.NU__RecordTypeName__c, ' +
                'Account__r.Provider_Join_On__c, ' +
                'Account__r.Provider_Lapsed_On__c, ' +
                'Account__r.Multi_Site_Corporate__c, ' +
                'Account__r.NU__PrimaryAffiliation__r.RecordTypeId, ' +
                'Account__r.NU__PrimaryAffiliation__r.RecordType.Name, ' +
                'Account__r.NU__PrimaryAffiliation__r.NU__RecordTypeName__c, ' +
                'Account__r.NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c, ' +
                'Account__r.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c, ' +
                'Start_Date__c, ' +
                'End_Date__c, ' +
                'Amount__c, ' +
                '(Select Id, ' +
                '   Revenue_Year_Submitted__c, ' +
                '   Program_Service_Revenue__c, ' +
                '   Dues_Price__c, ' +
                '   Amount__c, ' +
                '   Category__c ' +
                '    FROM Joint_Billing_Item_Lines__r ' +
                '    WHERE (CALENDAR_YEAR(Joint_Billing_Item__r.End_Date__c) = :prevCalYearForBillingYear) ' +
                '    ORDER BY Joint_Billing_Item__r.End_Date__c desc ' +
                ') ' +
                'FROM Joint_Billing_Item__c ';

            String queryWhere = 'WHERE Joint_Billing__r.State_Partner__c = :SelectedState ';
            queryWHERE += '';
            //queryWHERE += 'AND CALENDAR_YEAR(End_Date__c) = :ThisYearCalculated ';
            queryWHERE += 'AND CALENDAR_YEAR(End_Date__c) = :prevCalYearForBillingYear ';

            String queryOrder = 'ORDER BY Account__r.Name ASC';
            
            String queryFull = querySelect + queryWhere + queryOrder;
            System.Debug('#########' + SelectedState + ' ' + prevCalYearForBillingYear);
            List<Joint_Billing_Item__c> jbiList = (List<Joint_Billing_Item__c>) Database.query(queryFull);
            
            return jbiList;
        }
    }
    
    private Date calculateNextStartDateFromBillingQuarter(Decimal billingQuarter){
        Date todaysDate = Date.Today();
        
        Integer nextYear = ThisYearCalculated + 1;
        
        Date nextJanuary = Date.NewInstance(nextYear, 1, 1);
        Date nextStartDate = nextJanuary;
        
        if (billingQuarter == 2){
            nextStartDate = Date.newInstance(nextYear, 4, 1);
        }
        else if (billingQuarter == 3){
            nextStartDate = Date.newInstance(nextYear, 7, 1);
        }
        else if (billingQuarter == 4){
            nextStartDate = Date.newInstance(nextYear, 10, 1);
        }
        
        return nextStartDate;
    }
    
    private Date calculateNextEndDate(Date startDate, Integer termInMonths){
        return startDate.addMonths(termInMonths).addDays(-1);
    }
}