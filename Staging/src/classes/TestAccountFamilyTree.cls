/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest


private class TestAccountFamilyTree {
	
		public static string getNotNull(String s){
		if (s == null){
				return '';
		}
		else {
				return s;
		}
    }

    static testMethod void AccountFamilyTreeUnitTest() {
        // TO DO: implement unit test
        

        Account StateAccount = DataFactoryAccountExt.insertStatePartnerAccount();
        
        Account MSCompany = DataFactoryAccountExt.insertMultiSiteAccount();
        MSCompany.State_Partner_ID__c = StateAccount.Id;
        update MSCompany;
        
        Account Ind = DataFactoryAccountExt.insertPersonAccount(AccountRecordTypeUtil.getIndividualRecordType().Id);
        Ind.State_Partner_ID__c = StateAccount.Id;
        update Ind;
                
        Account Provider = DataFactoryAccountExt.insertProviderAccount();
        Provider.State_Partner_ID__c = StateAccount.Id;
        update Provider;        

        List<Account> accounts = [SELECT Id, Name, LeadingAge_ID__c, NU__RecordTypeName__c, State_Partner_Name__c, Parent_State_Partner_Name__c,
		                          Grandparent_State_Partner_Name__c, NU__PrimaryAffiliation__c, State_Partner_ID__r.Name,NU__PrimaryAffiliation__r.State_Partner_ID__r.Name
                                  ,NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name
                                  FROM Account 
                                  
                                  ORDER BY LeadingAge_ID__c]; 
                                  
        
        

         for (Account a : accounts){
	         	
	        if( (a != Null) && (getNotNull(a.NU__RecordTypeName__c).equalsIgnoreCase('Individual') || getNotNull(a.NU__RecordTypeName__c).equalsIgnoreCase('Individual Associate')))
	        {
	        	System.assertEquals(null,a.State_Partner_Name__c);
	        }
	        
	        if( (a != Null) && (getNotNull(a.NU__RecordTypeName__c).equalsIgnoreCase('Provider') || getNotNull(a.NU__RecordTypeName__c).equalsIgnoreCase('Company') ||
	                       getNotNull(a.NU__RecordTypeName__c).equalsIgnoreCase('Corporate/Alliance Sponsor')))
	        {
	        	System.assertEquals(null,a.State_Partner_Name__c);
	        }
	        
	        if( (a != Null) && getNotNull(a.NU__RecordTypeName__c).equalsIgnoreCase('Multi-site Organization'))
	        {
	        		System.assertEquals(null,a.State_Partner_Name__c);
	        }
         }
    }
}