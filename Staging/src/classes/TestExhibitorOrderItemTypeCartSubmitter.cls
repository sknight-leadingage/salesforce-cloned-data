/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
//tests 1-3 are standard
//tests 4-6 are custom for LeadingAge exhibitor priority points  
 
@isTest
private class TestExhibitorOrderItemTypeCartSubmitter {
    static NU__Entity__c entity = null;
    static NU__GLAccount__c gl = null;
    static NU__OrderItemConfiguration__c config = null;
    static NU__EntityOrderItem__c entityOrderItem = null;
    static Account billTo = null;
    static Account contact = null;
    static NU__PriceClass__c priceClass = null;
    
    static NU__Product__c exhibitorProduct = null;
    static NU__Deal__c exhibitorDeal = null;
    static DealItem__c exhibitorDealItem = null;
    
    static NU__Event__c evt = null;
    
    static NU__Cart__c cart = null;
    static NU__CartItem__c cartItem = null;
    static NU__CartItemLine__c cartItemLine = null;
    
    static Map<String, Schema.RecordTypeInfo> productRecordTypes = null;
    static Map<String, Schema.RecordTypeInfo> dealRecordTypes = null;
    
    private static void setupTest() {
        if (entity != null) return;
        
        entity = NU.DataFactoryEntity.insertEntity();
        gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);
        
        billTo = DataFactoryAccountExt.insertProviderAccount();
        contact = DataFactoryAccountExt.insertIndividualAccount();
        
        priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        
        
        
        // setup specific for exhibitor cart submitter        
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo exhibitorDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_EXHIBITOR);
        
        config = DataFactoryOrderItemConfigurationExt.insertExhibitorOrderItemConfigurationWithEntityOrderItem(entity.Id, gl.Id);
                
        exhibitorProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_EXHIBITOR, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_EXHIBITOR);
        
        exhibitorDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_EXHIBITOR, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, exhibitorDealRTI.getRecordTypeId());
        
        exhibitorDealItem = DataFactoryDealItem.insertDealItem(exhibitorDeal.Id, exhibitorProduct.Id, 500.00, 1);
        
        evt = NU.DataFactoryEvent.createEvent('Test Event', entity.Id);        
        evt.Current_Year_Annual_Event__c = true;
        upsert evt;
        
        exhibitorProduct.Event__c = evt.Id;
        update exhibitorProduct;
        
        cart = new NU__Cart__c(
            NU__BillTo__c = billTo.Id,
            NU__TransactionDate__c = Date.today(),
            NU__Entity__c = entity.Id,
            Deal__c = exhibitorDeal.Id
        );
        insert cart;
        
        Map<String, Schema.RecordTypeInfo> cartItemRecordTypes = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
        cartItem = new NU__CartItem__c(
            NU__Cart__c = cart.Id,
            NU__Customer__c = billTo.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = cartItemRecordTypes.get(Constant.ORDER_RECORD_TYPE_EXHIBITOR).getRecordTypeId()
        );
        insert cartItem;
        
        cartItemLine = new NU__CartItemLine__c(
            NU__CartItem__c = cartItem.Id,
            NU__IsInCart__c = true,
            NU__UnitPrice__c = exhibitorDealItem.Price__c,
            NU__Product__c = exhibitorDealItem.Product__c,
            NU__Quantity__c = exhibitorDealItem.Quantity__c                            
        );

        insert cartItemLine;
    } 
    
    // test methods
    static testMethod void testExhibitorTypeCartSubmitter() {
        setupTest();
        
        // Test #1: Instantiate exhibitorOrderItemTypeCartSubmitter, etc.
        ExhibitorOrderItemTypeCartSubmitter cartSubmitter = new ExhibitorOrderItemTypeCartSubmitter();
        System.assert(cartSubmitter != null, 'exhibitor Order Item Type Cart Submitter not loaded');
        
        // need to get more information from cart items
        List<NU__CartItem__c> cartItems = [SELECT Id,
            NU__Customer__c,
            (SELECT Id
                FROM NU__CartItemLines__r)
            FROM NU__CartItem__c
            WHERE Id = :cartItem.Id];
        
        NU__Order__c testOrder = new NU__Order__c(
            NU__BillTo__c = billTo.Id,
            NU__TransactionDate__c = cart.NU__TransactionDate__c,
            NU__Entity__c = entity.Id//,
            //Deal__c = exhibitorDeal.Id - this is added by a trigger
        );
        insert testOrder;
        
        Map<String, Schema.RecordTypeInfo> orderItemRecordTypes = Schema.SObjectType.NU__OrderItem__c.getRecordTypeInfosByName();
        NU__OrderItem__c orderItem = new NU__OrderItem__c(
            NU__Order__c = testOrder.Id,
            NU__Customer__c = billTo.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = orderItemRecordTypes.get(Constant.ORDER_RECORD_TYPE_EXHIBITOR).getRecordTypeId()
        );
        cartSubmitter.beforeOrderItemSaved(cartItems[0], orderItem);
        System.debug(orderItem);
        insert orderItem;
        cartSubmitter.afterOrderItemsSaved();
        
        NU__OrderItemLine__c orderItemLine = new NU__OrderItemLine__c(
            NU__OrderItem__c = orderItem.Id,
            NU__UnitPrice__c = cartItemLine.NU__UnitPrice__c,
            NU__Product__c = cartItemLine.NU__Product__c,
            NU__Quantity__c = cartItemLine.NU__Quantity__c                            
        );
        cartSubmitter.beforeOrderItemLineSaved(cartItemLine, orderItemLine);
        insert orderItemLine;
        cartSubmitter.afterOrderItemLinesSaved();
        
        // Test #2: Was an exhibitor history object created?
        List<Exhibitor__c> exhibitors = [SELECT Id, Event__r.Name, Event__r.Current_Year_Annual_Event__c, Account__r.Current_Year_Exhibitor__c, Account__c 
            FROM Exhibitor__c
            WHERE Order_Item_Line__c = :orderItemLine.Id];
        
        System.assert(exhibitors.size() > 0, 'No exhibitor history object was created.');
               
        // Test #3: Delete Cart & verify that the Order has a Deal ID and Deal has an Order ID
        delete cartItemLine;
        delete cartItem;
        delete cart;
        
        NU__Deal__c testDeal = [SELECT Id,
            Order__c
            FROM NU__Deal__c
            WHERE Id = :exhibitorDeal.Id];
            
        NU__Order__c testOrder2 = [SELECT Id,
            Deal__c
            FROM NU__Order__c
            WHERE Id = :testOrder.Id];
            
        System.assert(testDeal.Order__c == testOrder.Id, 'Deal was not updated with the order Id.');
        System.assert(testOrder2.Deal__c == testDeal.Id, 'Order was not updated with the deal Id.');
        
        //Test #4 accounts are flagged as current year exhibitor
        List<String> accountIds = new List<String>();
        
        for(Exhibitor__c exh :exhibitors)
        {
            accountIds.add(exh.Account__c);
            System.assert(exh.Account__r.Current_Year_Exhibitor__c);
        }
        
        //Test #5 accounts are no longer flagged as current year exhibitor
        delete exhibitors;
        List<Account> accounts = [SELECT Id, Current_Year_Exhibitor__c FROM Account where Id in :accountIds];
        
        for(Account a :accounts)
        {
            System.assert(!a.Current_Year_Exhibitor__c);
        }
        
        //Test #6 accounts are flagged as current year exhibitor
        undelete exhibitors;
        accounts = [SELECT Id, Current_Year_Exhibitor__c FROM Account where Id in :accountIds];
        
        for(Account a :accounts)
        {
            System.assert(a.Current_Year_Exhibitor__c);
        }
    }

}