global class BatchUpdateStatePartnerNames implements Database.Batchable<sObject>, Schedulable
{
	public final String Query = 'SELECT Id, RecordTypeId, Name, BillingState, ShippingState, ' +
	' State_Partner_ID__c, ' +
	' State_Partner_ID__r.Name, ' +
	' State_Partner_ID__r.BillingState,' +
	//' State_Partner_Name__c, ' +
	//' Parent_State_Partner_Name__c, ' +
	//' Grandparent_State_Partner_Name__c, ' +
	' State_Partner_Permissions__c,  ' +
	//' NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, ' +
	//' NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, ' +
	' NU__PrimaryAffiliationRecord__r.State_Partner_Permissions__c, ' +
	//' NU__PrimaryAffiliationRecord__r.BillingState, ' +
	' NU__PrimaryAffiliation__c, ' +
	' NU__PrimaryAffiliation__r.Name, ' +
	' NU__PrimaryAffiliation__r.State_Partner_ID__c, ' +
	' NU__PrimaryAffiliation__r.State_Partner_ID__r.BillingState, ' +
	' NU__PrimaryAffiliation__r.BillingState, ' +
	' NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, ' +
	//' NU__PrimaryAffiliation__r.State_Partner_Name__c, ' +
	//' NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, ' +
	//' NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c, ' +
	' NU__PrimaryAffiliation__r.State_Partner_Permissions__c,  ' +
	//' NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, ' +
	//' NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, ' +
	' NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.State_Partner_Permissions__c, ' +
	//' NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.BillingState, ' +
	' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__c, ' +
	' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, ' +
	' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.BillingState, ' +
	' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.BillingState, ' +
	//' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, ' +
	//' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c,  ' +
	//' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c, ' +
	' NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Permissions__c,  ' +
	'(Select id,' +
            'name,' +
            'NU__MembershipType__c,' +
            'NU__MembershipType__r.Grants_National_Membership__c,' +
            'NU__MembershipType__r.Grants_State_Membership__c, '  +
            'MembershipTypeProductName__c,' +
            'NU__EntityName__c,' +
            'NU__Status__c,' +
            'NU__StartDate__c,' +
            'NU__EndDate__c ' +
            'from NU__Memberships__r WHERE NU__MembershipType__r.Grants_State_Membership__c = true AND NU__Status__c = \'Current\')' + 
	' FROM Account ' +
	' WHERE RecordTypeId != \'' + Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID + '\'';
	
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(Query);
    }

    private static String normalizeSplit(String strPermissions) {
    	strPermissions = strPermissions.replace(',',';');
    	strPermissions = strPermissions.replaceAll('(;\\s+)', ';');
    	strPermissions = strPermissions.trim();
    	return strPermissions;
    }

	/*private static String normalizeSplit(String strPermissions) {
    	return strPermissions.replace(',',';');
    }*/
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    	List<Account> ascope = (List<Account>)scope;
    	List<sObject> toUpdate1 = new List<sObject>();
    	List<sObject> toUpdate2 = new List<sObject>();
    	    	
    	Boolean bChanged = false;
    	String strState = '';
    	
    	String initPermissions = '';
    	Set<String> lazySquash = new Set<String>();
    	List<String> lazySquashList = new List<String>();
    	String strForSquash = ''; 
    	
    	Map<String, Id> StatePartnerIdMAP = new Map<String, Id>();
    	
    	for(Account SPI : [Select Id, Name From Account where RecordTypeId = :Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID])
    	{
    		StatePartnerIdMAP.put(SPI.Name, SPI.Id);
    	}
    	
    	
    	for(Account au : ascope) {
    		bChanged = false;
    		
    		
    		if(au.State_Partner_Permissions__c != null){
    			
    			initPermissions = au.State_Partner_Permissions__c;
    		}
    		
    		
    		// Checking State partner of if Membership is State
    		if(au.NU__Memberships__r.size() == 1){
    			
    			au.State_Partner_ID__c = StatePartnerIdMAP.get(au.NU__Memberships__r[0].NU__EntityName__c);
    		}
    		
    		
    		
			if (au.State_Partner_ID__c == null) {
				//Attempt to set State Partner ID according to default rules
				if (au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.State_Partner_ID__c != null) {
					
					
						bChanged = true;
					
						au.State_Partner_ID__c = au.NU__PrimaryAffiliation__r.State_Partner_ID__c;
					
					
				}
				else {
    				strState = 'LeadingAge';
    				if (au.BillingState != null && AccountQuerier.PostalStateToStatePartnerIdMap.containsKey(au.BillingState)) {
    					strState = au.BillingState;
    				}
    				else if (au.ShippingState != null && AccountQuerier.PostalStateToStatePartnerIdMap.containsKey(au.ShippingState)) {
    					strState = au.ShippingState;
    				}
    				bChanged = true;
    				au.State_Partner_ID__c = AccountQuerier.PostalStateToStatePartnerIdMap.get(strState);
				}
			}    	
			
			if(au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.Name == 'LeadingAge'){
			
				au.State_Partner_ID__c = au.NU__PrimaryAffiliation__c;
			}
			
			lazySquash.clear();
    		lazySquashList.clear();		
			
    		if (au.State_Partner_ID__c != null &&  au.State_Partner_ID__r.name != 'LeadingAge' && au.State_Partner_ID__r.BillingState != null) {
				bChanged = true;
				
				lazySquash.add((au.State_Partner_ID__r.BillingState));
    		}
 
     		
    		
    		
    		
    		if (au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.BillingState != null) {
				bChanged = true;
				lazySquash.add((au.NU__PrimaryAffiliation__r.BillingState));
    		}
    		
    		if(au.BillingState != null){
    			bChanged = true;
    			lazySquash.add((au.BillingState));
    			
    			if(au.BillingState.containsIgnoreCase('LA') || au.BillingState.containsIgnoreCase('MS'))
    			{
    				lazySquash.add('LA');
    				lazySquash.add('MS');
    			}
    			
    			if(au.BillingState.containsIgnoreCase('ME') || au.BillingState.containsIgnoreCase('NH'))
    			{
    				lazySquash.add('ME');
    				lazySquash.add('NH');
    			}
    			
    			if(au.BillingState.containsIgnoreCase('TN'))
    			{
    				lazySquash.add('TX');
    				
    			}
    			
    		}
    		
    		if (au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.BillingState != null) {		
				bChanged = true;
				lazySquash.add(au.NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.BillingState);
    		}
    		if (au.State_Partner_Permissions__c != null) {
    			bChanged = true;
    			lazySquashList = normalizeSplit(au.State_Partner_Permissions__c).split(';');
    			lazySquash.addAll(lazySquashList); 
    		}
    		
    		if (au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.State_Partner_Permissions__c != null) {
    			bChanged = true;
    			lazySquashList = normalizeSplit(au.NU__PrimaryAffiliation__r.State_Partner_Permissions__c).split(';');
    			lazySquash.addAll(lazySquashList); 
    		}
    		
    		if (au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__c != null 
    				&& au.NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Permissions__c != null) {
    		    bChanged = true;
    			lazySquashList = normalizeSplit(au.NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Permissions__c).split(';');
    			lazySquash.addAll(lazySquashList); 
    		}
    		
    		if (lazySquash.size() > 0) {
				strForSquash = '';
				for(String S : lazySquash)
				{
					if (S != null) {
						strForSquash += S;
						strForSquash += '; ';
					}
				}
				strForSquash = strForSquash.removeEnd('; ');
				
				bChanged = true;
				au.State_Partner_Permissions__c = strForSquash;
				if (au.NU__PrimaryAffiliationRecord__c != null) {
					au.NU__PrimaryAffiliationRecord__r.State_Partner_Permissions__c = strForSquash;
				}
    		}
    		
    		if(!initPermissions.equals(strForSquash)){
    			
    			bChanged = true;
    		}
    		/*
    		if (au.NU__PrimaryAffiliationRecord__c != null && au.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c != au.Parent_State_Partner_Name__c){
				bChanged = true;
    			au.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c = au.Parent_State_Partner_Name__c;
    		}
    		
    		if (au.NU__PrimaryAffiliationRecord__c != null && au.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c != au.Grandparent_State_Partner_Name__c){
				bChanged = true;
    			au.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c = au.Grandparent_State_Partner_Name__c;
    		}
    		*/
    		
    		if(au.NU__PrimaryAffiliation__c != null && au.NU__PrimaryAffiliation__r.Name == 'LeadingAge'){
			
				au.State_Partner_Permissions__c = null;
			}
    		    		
    		if (bChanged) {
    			toUpdate1.add(new Account(Id = au.Id,
    				State_Partner_ID__c = au.State_Partner_ID__c,
					//State_Partner_Name__c = au.State_Partner_Name__c,
					//Parent_State_Partner_Name__c = au.Parent_State_Partner_Name__c,
					//Grandparent_State_Partner_Name__c = au.Grandparent_State_Partner_Name__c,
					State_Partner_Permissions__c = au.State_Partner_Permissions__c
    			));
    			if(au.NU__PrimaryAffiliationRecord__r != null)
    			{
    		      toUpdate2.add(au.NU__PrimaryAffiliationRecord__r);
    			}

    		}
    	}
    	
		NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AccountTriggerHandlers');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTriggerHandlers');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AffiliationTriggerHandlers');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggerHandlers');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AffiliationTriggers');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggers');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTrigger');
		NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger_Subscriptions');
    	if (toUpdate1.size() > 0) {
    		update toUpdate1;
    	}
    	if (toUpdate2.size() > 0) {
    		update toUpdate2;
    	}
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger_Subscriptions');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTriggerHandlers');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AffiliationTriggers');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTriggers');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTrigger');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AffiliationTriggerHandlers');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTriggerHandlers');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AccountTriggerHandlers');
		NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger');
	}

	global void finish(Database.BatchableContext BC)
	{
	}

	public static void schedule() {
		System.schedule('UpdateStatePartnerNames', '0 0 3 * * ?', new BatchUpdateStatePartnerNames());
	}

    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }
}