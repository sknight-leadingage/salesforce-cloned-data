/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCASTFieldSyncJob {
	
	static Account getAccountById(Id accountId){
		return [select id,
		               name,
		               NU__LapsedOn__c,
		               CAST_Lapsed_On__c,
		               CAST_Membership__r.Lapsed_On__c,
		               Provider_Lapsed_On__c,
		               IAHSA_Lapsed_On__c
		          from Account
		         where id = :accountId];
	}

	static void syncCastFields(){
		Test.startTest();
		
		Database.executeBatch(new CASTFieldSyncJob());
		
		Test.stopTest();
	}

    static testMethod void selectTest() {
        Test.startTest();
        
        new CASTFieldSyncJob().execute(null);
        
        Test.stopTest();
    }
    
    static testmethod void castLapsedOnSetFromSilverPremierSupporterTest(){
    	Account company = DataFactoryAccountExt.insertCompanyAccount();
    	company.CAST_Silver_Premier_Supporter__c = true;
    	update company;
    	
    	NU__Membership__c membership = DataFactoryMembershipExt.insertCurrentCorporateAllianceMembership(company.Id);
    	
    	syncCastFields();
    	
    	Account companyQueried = getAccountById(company.Id);
    	
    	system.assertEquals(companyQueried.NU__LapsedOn__c, companyQueried.CAST_Lapsed_On__c);
    }
    
    static testmethod void castLapsedOnSetFromCASTMembershipTest(){
    	Account company = DataFactoryAccountExt.insertCompanyAccount();
    	
    	NU__Membership__c membership = DataFactoryMembershipExt.insertCurrentCASTMembership(company.Id);
    	
    	syncCastFields();
    	
    	Account companyQueried = getAccountById(company.Id);
    	
    	system.assertEquals(companyQueried.CAST_Membership__r.Lapsed_On__c, companyQueried.CAST_Lapsed_On__c);
    }
    
    static testmethod void castLapsedOnSetFromProviderLapsedOnTest(){
    	Account company = DataFactoryAccountExt.insertCompanyAccount();
    	
    	NU__Membership__c membership = DataFactoryMembershipExt.insertCurrentProviderMembership(company.Id);
    	
    	company.CAST_LeadingAge_Provider__c = true;
    	company.Provider_Lapsed_On__c = Date.Today().addDays(30);
    	
    	update company;
    	
    	syncCastFields();
    	
    	Account companyQueried = getAccountById(company.Id);
    	
    	system.assertEquals(companyQueried.Provider_Lapsed_On__c, companyQueried.CAST_Lapsed_On__c);
    }
    
    static testmethod void castLapsedOnSetFromIAHSALapsedOnTest(){
    	Account company = DataFactoryAccountExt.insertCompanyAccount();
    	
    	NU__Membership__c membership = DataFactoryMembershipExt.insertCurrentIAHSAMembership(company.Id);
    	
    	company.CAST_IAHSA_Provider__c = true;
    	
    	update company;
    	
    	syncCastFields();
    	
    	Account companyQueried = getAccountById(company.Id);
    	
    	system.assertEquals(companyQueried.IAHSA_Lapsed_On__c, companyQueried.CAST_Lapsed_On__c);
    }
}