public with sharing class EduOnsiteEstimatedAttendance_Controller extends EduReportsControllerBase {
    public override string getPageTitle() {
        if (RTypeSel == null) {
            return 'Estimated Attendance';
        }
        else {
            return RTypeSel;
        }
    }
    
    public string EST_ATT_STIMESLOT = 'Estimated Attendance (By Timeslot)';
    public string EST_ATT_SNUMBER = 'Estimated Attendance (By Session #)';
    public string EST_ATT_SACTUAL = 'Estimated Attendance (By Actual Attendance)';

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(EST_ATT_STIMESLOT, EST_ATT_STIMESLOT));
        options.add(new SelectOption(EST_ATT_SNUMBER, EST_ATT_SNUMBER));
        options.add(new SelectOption(EST_ATT_SACTUAL, EST_ATT_SACTUAL));
        return options; 
    }
    
    public List<Conference_Session__c> SumList { get; set; }
    public override void SetConRefreshData() {
        SumList = setCon.getRecords();
    }
    
    //public List<Conference_Session__c> SessionInfo { get; set; }
    //public override void SetConRefreshData() {
      //  SessionInfo = setCon.getRecords();
    //}

    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == EST_ATT_SNUMBER) {
            WizardStep = 2;
        } else if (RTypeSel == EST_ATT_STIMESLOT) {
            WizardStep = 3;
        } else if (RTypeSel == EST_ATT_SACTUAL) {
            WizardStep = 4;
        } 
        SetQuery();
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    
    public EduOnsiteEstimatedAttendance_Controller(){
        iWizardMax = 4;
        WizardStep = 1;
        SetConPageSize = 225;
    }
    
    public void SetQuery() {
        string QueryBase = 'SELECT Session_Number__c, Session_Number_Numeric__c, Timeslot__r.Timeslot_Code__c, Expected_Attendance__c, Actual_Attendance__c FROM Conference_Session__c WHERE Conference__c = :EventId';
    
        if (RTypeSel == EST_ATT_SNUMBER) {
            QueryBase += ' ORDER BY Session_Number_Numeric__c';
        } else if (RTypeSel == EST_ATT_STIMESLOT) {
            QueryBase += ' ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        } else if (RTypeSel == EST_ATT_SACTUAL) {
            QueryBase += ' ORDER BY Actual_Attendance__c DESC';
        } 
        
        SetConQuery = QueryBase;
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}