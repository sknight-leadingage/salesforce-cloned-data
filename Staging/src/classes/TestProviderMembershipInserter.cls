/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class inserts default provider memberships for a given account for testing
 */
public with sharing class TestProviderMembershipInserter implements ITestMembershipInserter {
	public NU__Membership__c insertMembership(Id accountId){
		return DataFactoryMembershipExt.insertCurrentProviderMembership(accountId);
	}
}