public with sharing class RenewalNoticeController {

	private List<Account> tempAccounts = new List<Account>();
	
	private List<Account> privAccounts = new List<Account>();
    public List<Account> Accounts {
    	get {
    		return privAccounts;
    	}
    }
    
    private Map<Id,Account> privAccountsToEmail = new Map<Id,Account>();
    public List<Account> AccountsToEmailList {
    	get {
    		return privAccountsToEmail.values();
    	}
    }
    
    public String RenewalDialogScript { get; set; }
    
    public NU__Order__c DummyOrder { 
    	get {
    		if (DummyOrder == null) {
    			DummyOrder = new NU__Order__c();
    		}
    		return DummyOrder;
    	}
    	set {
    		DummyOrder = value;
    	}
    }
    
    /*public Date invoiceDateOverride { 
    	get {
    		return RenewalNoticeUtil.invoiceDateOverride;
    	}
    	set {
    		RenewalNoticeUtil.invoiceDateOverride = value;
    	}
    }*/
    
    public String membershipRelationship {
    	get {
    		return RenewalNoticeUtil.membershipRelationship;
    	} 
    	set {
    		RenewalNoticeUtil.membershipRelationship = value;
    	}
    }
    
    public String affiliationRole {
    	get {
    		return RenewalNoticeUtil.affiliationRole;
    	}
    	set {
    		RenewalNoticeUtil.affiliationRole = value;
    	}
    }
    
    public Boolean showAllProducts {
    	get {
    		return RenewalNoticeUtil.showAllProducts;
    	}
    	set {
    		RenewalNoticeUtil.showAllProducts = value;
    	}
    }
    
    public List<SelectOption> MembershipOptions {
    	get {
    		return RenewalNoticeUtil.Instance.MembershipOptions;
    	}
    }
    
    public List<SelectOption> RoleOptions {
    	get {
    		return RenewalNoticeUtil.Instance.RoleOptions;
    	}
    }
    
    public Boolean CanRender { get; set; }
    public Boolean CanEmail { get; set; }
    public Boolean RenderPDF { get; set; }
    
    public Id BatchEmailJobId { get; set; }
    
    public void Bill() {
    	updateSettings();
    	
    	MembershipBilling.memberBillingInfos = ViewRenewalNoticeController.getMembershipBillingInfos();
    	String result = MembershipBilling.Bill();
    	
    	ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Info, result);
        ApexPages.addMessage(pageMsg);
    }

    public RenewalNoticeController(ApexPages.StandardSetController controller) {
    	CanRender = false;
    	CanEmail = false;
    	
    	Integer selectionSize = controller.getSelected().size();
    	
    	if (selectionSize == 0 && ApexPages.currentPage().getParameters().get('id') == null) {
        	ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error, 'You did not select any accounts. Please go back and make a selection before continuing.');
	        ApexPages.addMessage(pageMsg);
        }

        if (selectionSize > 0) {
            tempAccounts = controller.getSelected();
        } else {
            try {
                tempAccounts = new List<Account> { new Account(Id = ApexPages.currentPage().getParameters().get('id')) };
            } catch (Exception e) { }
        }
    }
    
    public void Generate() {
    	updateSettings();
    	
    	// is a valid membership relationship selected?
    	if (membershipRelationship == null || membershipRelationship == '') {
    		ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please select a valid membership option.');
	        ApexPages.addMessage(pageMsg);
	        
	        CanRender = false;
    	}
    	else {
	    	// loop through accounts and determine if they are valid selections - ie, only accounts with existing memberships can get renewal notices, and grace periods have to be analyzed
	        privAccounts = validateAccounts(tempAccounts);
	        if (privAccounts.size() != tempAccounts.size()) { // accounts were removed
	            ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Warning, 'Some of the accounts you selected are not displayed below because they do not have an existing membership.');
	            ApexPages.addMessage(pageMsg);
	        }
	        
	        // need to loop through all accounts and see if e-mails were found
        	// only those accounts with valid e-mails will have their renewals sent out
	        privAccountsToEmail = validatePrimaryContacts(privAccounts);
	        
	        CanRender = (Accounts.size() > 0);
	        CanEmail = (privAccountsToEmail.size() > 0);
    	}
    }
    
    public String getRender() {
		if (ApexPages.currentPage().getParameters().get('pdf') != null) {
			RenderPDF = Boolean.valueOf(ApexPages.currentPage().getParameters().get('pdf'));
			return 'pdf';
		}
		return null;
	}
	
	public PageReference PDF(){
		updateSettings();
		
		List<Task> tasksToAdd = new List<Task>();
        for (Account account : Accounts) {
            Task task = new Task();
            task.WhatId = account.Id;
            task.Description = 'Generated Membership Renewal Notice PDF';
            task.Status = 'Completed';
            task.ActivityDate = date.today();
            task.Subject = 'Generate Membership Renewal Notice PDF';
            tasksToAdd.add(task);
        }
        
        if (tasksToAdd.size() > 0) {
            insert tasksToAdd;  
        }
		
		PageReference ref = ApexPages.currentPage();
		ref.getParameters().put('pdf','true');
		return ref;
	}
    
    public void Email() {
    	updateSettings();    
        
        if (privAccountsToEmail == null) {
        	privAccountsToEmail = validatePrimaryContacts(privAccounts);
        }
        
        // get the e-mail template
        List<EmailTemplate> renewalEmailTemplates = [SELECT Id FROM EmailTemplate WHERE developerName = 'MembershipRenewalTemplate'];

        if (renewalEmailTemplates == null || renewalEmailTemplates.size() == 0) {
            ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The membership renewal template was not found.\n');
            ApexPages.addMessage(pageMsg);
            return;
        }
        EmailTemplate renewalEmailTemplate = renewalEmailTemplates.get(0);

		List<Email> renewalEmails = new List<Email>();
        
        // get recipient who should always receive e-mails, if set
        List<NU__Configuration__c> renewalRecipientEmailList = [SELECT NU__Value__c FROM NU__Configuration__c WHERE Name='RenewalRecipientEmail'];
        String renewalRecipientEmail = (renewalRecipientEmailList.size() > 0 && String.isNotBlank(renewalRecipientEmailList.get(0).NU__Value__c) ? renewalRecipientEmailList.get(0).NU__Value__c : null);
        
        // get Id of the org-wide e-mail address to use, if set
        List<NU__Configuration__c> renewalEmailFromOrgWideIdList = [SELECT NU__Value__c FROM NU__Configuration__c WHERE Name='RenewalEmailFromOrgWideId'];
        Id renewalEmailFromOrgWideId = (renewalEmailFromOrgWideIdList.size() > 0 && String.isNotBlank(renewalEmailFromOrgWideIdList.get(0).NU__Value__c) ? (Id)renewalEmailFromOrgWideIdList.get(0).NU__Value__c : null);
        
        // create the e-mails to send
        for (Id primaryContactId : privAccountsToEmail.keySet()) {
        	Id accountId = privAccountsToEmail.get(primaryContactId).Id;
        	
            Email renewalEmail = new Email();
            renewalEmail.TemplateId = renewalEmailTemplate.Id;
            renewalEmail.WhatId = accountId;
            
            renewalEmail.TargetObjectId = primaryContactId;
            
            if (renewalRecipientEmail != null) {
                renewalEmail.ToAddresses = new List<String>{renewalRecipientEmail};
            }
                        
            if (renewalEmailFromOrgWideId != null)
            {
                renewalEmail.OrgWideEmailAddressId = renewalEmailFromOrgWideId;
            }

            renewalEmails.add(renewalEmail);
        }
		
		BatchEmailJobId = BatchEmailSender.send(renewalEmails, membershipRelationship, affiliationRole, RenewalNoticeUtil.invoiceDateOverride, showAllProducts);
    }

    public void Print() {
    	updateSettings();
    	RenewalDialogScript = 'print();';
        
        List<Task> tasksToAdd = new List<Task>();
        for (Account account : Accounts) {
            Task task = new Task();
            task.WhatId = account.Id;
            task.Description = 'Printed Membership Renewal Notice';
            task.Status = 'Completed';
            task.ActivityDate = date.today();
            task.Subject = 'Print Membership Renewal Notice';
            tasksToAdd.add(task);
        }
        
        if (tasksToAdd.size() > 0) {
            insert tasksToAdd;  
        }
    }
    
    private void updateSettings() {
    	RenewalDialogScript = null; 
    	
    	if (DummyOrder.NU__InvoiceDate__c != null) {
    		RenewalNoticeUtil.invoiceDateOverride = DummyOrder.NU__InvoiceDate__c;
    	}
    }
    
    private Map<Id,Account> validatePrimaryContacts(List<Account> accountsToValidate) {
    	Map<Id,Account> privAccountsToEmail = new Map<Id,Account>();

		Boolean affiliationRoleMessageDisplayed = false;
		for (Account account : accountsToValidate) {       
            if (!account.IsPersonAccount) {            	
            	// business accounts should have a primary contact
            	if (account.NU__Affiliates__r == null || account.NU__Affiliates__r.size() == 0) {
            		// was a role even selected?
            		if (!affiliationRoleMessageDisplayed && affiliationRole == null) {
            			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Because Business Accounts are selected, you must specify a primary contact before you can e-mail any renewal notices.'));
            			affiliationRoleMessageDisplayed = true;
            			continue;
            		}
            		else if (affiliationRole != null) {
            			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'There is no ' + affiliationRole + ' associated with the account: <a href="/' + account.Id + '">' + account.Name + '</a>.'));
            			continue;
            		}
            		continue;
            	}
            	
            	// more than one primary contact found?
            	else if (account.NU__Affiliates__r.size() > 1) {
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'There is more than one ' + affiliationRole + ' associated with the account: <a href="/' + account.Id + '">' + account.Name + '</a>, so the first one found is being used.'));
            	}
            	
            	Account primaryContact = new Account(Id = account.NU__Affiliates__r[0].NU__Account__c, 
            		Name = account.NU__Affiliates__r[0].NU__Account__r.Name,
            		PersonEmail = account.NU__Affiliates__r[0].NU__Account__r.PersonEmail);
            			
            	if (primaryContact.PersonEmail == null) {
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The ' + affiliationRole + ' associated with the account: <a href="/' + account.Id + '">' + account.Name + '</a> (<a href="/' + primaryContact.Id + '">' + primaryContact.Name + '</a>) does not have an e-mail address listed.'));
            		continue;
            	}
            		
				privAccountsToEmail.put(account.NU__Affiliates__r[0].NU__Account__r.PersonContactId,new Account(Id = account.Id, Name = account.Name));
            }
            else {
            	if (account.PersonEmail == null) {
            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The person account, <a href="/' + account.Id + '">' + account.Name + '</a>, does not have an e-mail address listed.'));
            		continue;
            	}
            	
            	privAccountsToEmail.put(account.PersonContactId,new Account(Id = account.Id, Name = account.Name));
            }
        }

        return privAccountsToEmail;
    }
    
    private List<Account> validateAccounts(List<Account> accountsToValidate) {
        Map<Id,Account> mapAccountsToValidate = new Map<Id,Account>();
        mapAccountsToValidate.putAll(accountsToValidate);
        
        RenewalNoticeUtil.validatedAccounts = null;
        RenewalNoticeUtil.accountIds = mapAccountsToValidate.keySet();
        
        return RenewalNoticeUtil.Instance.getValidatedAccounts();
    }
    
    @RemoteAction
	public static BatchEmailSender.JobStatus getJobStatus(Id jobId) {
		return BatchEmailSender.getJobStatus(jobId);
	} 
}