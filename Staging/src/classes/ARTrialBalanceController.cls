//bulk calculate the outstanding AR for each selected entity/account/order and store as records in the a/r snapshot object
// - a VisualForce page controller to provide a user interface
// - a batchable task to bulk calcualte the ar and store it as a/r snapshot records
// - an action poller to indicate the progress of the batch task
//run as a batch apex job because it affects more than 200 accounts.
//Developed by NimbleUser 2015 (NF, LF)

//To run once interactively, visit:
// [The base URL] + /apex/ARTrialBalance;

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new ARTrialBalanceController());

public with sharing class ARTrialBalanceController implements Database.Batchable<sObject> {

    ///PART 1: VisualForce form submission

    //constructor for visualforce page
    public ARTrialBalanceController () {
        FormFields = new FormFieldsRecord();
        BatchJobId = null;
    }

    //form submission record
    public FormFieldsRecord FormFields { get; set; }
    public class FormFieldsRecord {

        //report options
        public String Name { get; set; }
        public NU__Affiliation__c AsOfDate { get; set; } //use 'Affiliation' so that a Lookup dialog appears, requesting a Date (NU__EndDate__c)
        public String EntityId { get; set; }
        public String OrderTypeName { get; set; }
        public NU__Affiliation__c BillTo { get; set; } //use 'Affiliation' so that a Lookup dialog appears, requesting an Account (NU__Account__c)

        //constructor
        public FormFieldsRecord(){
            Name = 'AR Trial Balance';
            AsOfDate = new NU__Affiliation__c(NU__EndDate__c = Date.today());
            EntityId = 'All';
            OrderTypeName = 'All';
            BillTo = new NU__Affiliation__c();
        }
    }

    //picklists for form
    public List<SelectOption> EntityOptions {
        get {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All', 'All'));
            for (NU__Entity__c entity : EntitiesList){
                SelectOption option = new SelectOption(entity.Id, entity.Name);
                options.add(option);
            }
            return options;
        }
    }

    public List<SelectOption> OrderTypeOptions {
        get {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('All', 'All'));
            for (NU__OrderItemConfiguration__c orderType : OrderTypesList){
                SelectOption option = new SelectOption(orderType.Name, orderType.Name);
                options.add(option);
            }
            return options;
        }
    }

    private List<NU__Entity__c> EntitiesListPrivate = null;
    public List<NU__Entity__c> EntitiesList {
        get {
            if (EntitiesListPrivate == null) {
                EntitiesListPrivate =
                [SELECT Id,
                        Name,
                        (SELECT Id,
                                Name,
                                NU__Days2__c
                           FROM NU__ARAgings__r
                           ORDER BY NU__Days2__c)
                   FROM NU__Entity__c
                  WHERE NU__Status__c = 'Active'
                  ORDER BY Name];
            }
            return EntitiesListPrivate;
        }
    }

    private List<NU__OrderItemConfiguration__c> OrderTypesListPrivate = null;
    public List<NU__OrderItemConfiguration__c> OrderTypesList {
        get {
            if (OrderTypesListPrivate == null) {
                OrderTypesListPrivate =
                [SELECT Id,
                        Name
                   FROM NU__OrderItemConfiguration__c
                 ORDER BY Name];
            }
            return OrderTypesListPrivate;
        }
    }

    public void Submit () {
        String msg = 'The AR trail balance is being calculated. You will receive an email when the task is finished. ' +
            'You may leave this window open to monitor the progress of the task. The task will continue to run if you close this window.';
        ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Info, msg);
        ApexPages.addMessage(pageMsg);
        BatchJobId = Database.executeBatch(new ARTrialBalanceController(FormFields));
    }


    ///PART 2: Batchable task execution

    public Id BatchJobId { get; set; }

    //constructor for batchable task
    public ARTrialBalanceController (FormFieldsRecord FormFieldsFromForm) {
        FormFields = FormFieldsFromForm;
    }

    //batch apex jobs require three methods: start, execute, and finish; run in that order
    //schedulable batch apex jobs require one method: execute

    public Database.QueryLocator start (Database.BatchableContext BC) {
        String query = 'SELECT id, Name FROM Account';
        query += ' WHERE NU__Status__c = \'Active\'';

        if (FormFields.BillTo.NU__Account__c != null) {
            query += ' AND id = \'' + FormFields.BillTo.NU__Account__c + '\'';
        }

        System.debug ('Query:' + query);
        return Database.getQueryLocator(query);
    }

    public void execute (Database.BatchableContext BC, List<Account> batchOfAccounts) {

        //delete all prior AR Snapshot records for these accounts for this As Of Date/Entity
        DeleteARSnapshots(batchOfAccounts);

        //retrieve and store transactions (filtering by entity, order type)
        List<NU__Transaction__c> ARTransactions = getARTransactions(batchOfAccounts);

        //group and store transactions by account (bill to) and order
        Map<Object, Map<Object, List<NU__Transaction__c>>> transactionsByAccountThenOrderMap = groupARTransactions(ARTransactions);
        //Note: it may not be necessary to group by account as above, where a simple grouping by order could be sufficient

        //ignore orders that are paid off; store the rest as AR Snapshot records
        List<ARSnapshot__c> arSnapshotList = buildSnapshotList(transactionsByAccountThenOrderMap);

        //save results
        if (arSnapshotList.size() > 0) {
            insert arSnapshotList;
        }

    }

    public void finish (Database.BatchableContext BC) {
        AsyncApexJob job = [SELECT Id, CreatedBy.Email, NumberOfErrors FROM AsyncApexJob WHERE ID =: BC.getJobId()];
        if (job.NumberOfErrors > 0) {
            NU.BatchApexUtil.sendNotificationForFailures(BC);
        } else {
            sendNotificationForSuccesses(BC, job, buildSuccessEmail());
        }
    }

    //delete all prior AR Snapshot records for these accounts for this As Of Date/Entity
    private void DeleteARSnapshots(List<Account> batchOfAccounts) {
        List<ARSnapshot__c> snapshotList = null;

        Date asOfDate = null;
        if (FormFields.AsOfDate.NU__EndDate__c != null) {
            asOfDate = FormFields.AsOfDate.NU__EndDate__c;
        }

        String entityId = null;
        if (FormFields.EntityId != null && FormFields.EntityId != 'All') {
            entityId = FormFields.EntityId;
            snapshotList = [SELECT Id FROM ARSnapshot__c
                WHERE Entity__c = :entityId
                AND AsOfDate__c = :asOfDate
                AND BillTo__c = :batchOfAccounts];
        } else {
            snapshotList = [SELECT Id FROM ARSnapshot__c
                WHERE AsOfDate__c = :asOfDate
                AND BillTo__c = :batchOfAccounts];
        }

        delete snapshotList;
    }

    //retrieve and store transactions (filtering by entity, order type)
    private List<NU__Transaction__c> getARTransactions(List<Account> batchOfAccounts){
        String AR_TYPE = 'AR';
        Date asOfDate = Date.today();
        if (FormFields.AsOfDate.NU__EndDate__c != null) {
            asOfDate = FormFields.AsOfDate.NU__EndDate__c;
        }

        String query = 'SELECT Id, ' +
                          'Name, ' +
                          'NU__Amount__c, ' +
                          'NU__BillTo__c, ' +
                          'NU__Category__c, ' +
                          'NU__Customer__c, ' +
                          'NU__Entity2__c, ' +
                          'NU__GLAccount__c, ' +
                          'NU__Order__c, ' +
                          'NU__Order__r.NU__InvoiceDate__c, ' +
                          'NU__OrderItem__r.RecordType.Name ' +
                     'FROM NU__Transaction__c ' +
                    'WHERE NU__Date__c <= :asOfDate ' +
                    '  AND NU__Type__c = :AR_TYPE';

        if (FormFields.EntityId != null && FormFields.EntityId != 'All') {
            Id EntityId = FormFields.EntityId;
            query += ' AND NU__Entity2__c = :EntityId';
        }

        if (FormFields.OrderTypeName != null && FormFields.OrderTypeName != 'All') {
            String orderType = FormFields.OrderTypeName;
            query += ' AND NU__OrderItem__r.RecordType.Name = :orderType';
        }

        query += ' AND NU__BillTo__c = :batchOfAccounts';

        query += ' ORDER BY NU__BillTo__r.Name, NU__Entity2__c, NU__Order__r.CreatedDate';

        return Database.query(query);
    }

    //group and store transactions by account (bill to) and order
    private Map<Object, Map<Object, List<NU__Transaction__c>>> groupARTransactions(List<NU__Transaction__c> ARTransactions){
        Map<Object, List<NU__Transaction__c>> transactionsByAccountMap = NU.CollectionUtil.groupSObjectsByField(ARTransactions, 'NU__BillTo__c');
        Map<Object, Map<Object, List<NU__Transaction__c>>> transactionsByAccountThenOrderMap = new Map<Object, Map<Object, List<NU__Transaction__c>>>();

        Set<Object> AccountIdSet = transactionsByAccountMap.keySet();
        for (Object accountId : AccountIdSet) {
            List<NU__Transaction__c> transactionsThisAccountList = transactionsByAccountMap.get(accountId);
            Map<Object, List<NU__Transaction__c>> transactionsByOrderMap = NU.CollectionUtil.groupSObjectsByField(transactionsThisAccountList, 'NU__Order__c');
            if (transactionsByOrderMap != null && transactionsByOrderMap.size() > 0) {
                transactionsByAccountThenOrderMap.put(accountId, transactionsByOrderMap);
            }
        }

        return transactionsByAccountThenOrderMap;
    }

    //ignore orders that are paid off; store the rest as AR Snapshot records
    private List<ARSnapshot__c> buildSnapshotList(Map<Object, Map<Object, List<NU__Transaction__c>>> transactionsByAccountThenOrderMap) {
        List<ARSnapshot__c> arSnapshotList = new List<ARSnapshot__c>();
        Set<Object> AccountIdSet = transactionsByAccountThenOrderMap.keySet();
        for (Object accountId : AccountIdSet) {
            Map<Object, List<NU__Transaction__c>> transactionsByOrderMap = transactionsByAccountThenOrderMap.get(accountId);
            Set<Object> OrderIdSet = transactionsByOrderMap.keySet();
            for (Object orderId : OrderIdSet) {
                List<NU__Transaction__c> transactionsList = transactionsByOrderMap.get(orderId);
                buildSnapshotListFromOrder(transactionsList, arSnapshotList);
            }
        }
        return arSnapshotList;
    }

    //intermediate data rows for the export
    public class Row
    {
        public NU__Transaction__c firstTransaction { get; set; }
        public Decimal outstandingBalance { get; set; }
        public String glAccount { get; set; }
        public String orderType { get; set; }
    }

    //for each order, group transactions by ar account and order item type, then create ar snapshot records
    private void buildSnapshotListFromOrder(List<NU__Transaction__c> transactionsList, List<ARSnapshot__c> arSnapshotList) {
        List<Row> rowList = new List<Row>();

        for (NU__Transaction__c t : transactionsList) {

            //if there is already a matching detail row, just combine them
            Boolean foundDetail = false;
            for (Row r : rowList)
            {
                if (
                    r.glAccount == t.NU__GLAccount__c &&
                    r.orderType == t.NU__OrderItem__r.RecordType.Name)
                {
                    foundDetail = true;
                    if (t.NU__Category__c == 'Debit'){
                        r.outstandingBalance += t.NU__Amount__c;
                    }
                    else{
                        r.outstandingBalance -= t.NU__Amount__c;
                    }
                    break;
                }
            }

            //otherwise, add the detail row
            if (!foundDetail)
            {
                Decimal outstandingBalance = 0;
                if (t.NU__Category__c == 'Debit'){
                    outstandingBalance += t.NU__Amount__c;
                }
                else{
                    outstandingBalance -= t.NU__Amount__c;
                }
                if (outstandingBalance != 0.00) {
                    Row n = new Row();
                    n.firstTransaction = t;
                    n.outstandingBalance = outstandingBalance;
                    n.glAccount = t.NU__GLAccount__c;
                    n.orderType = t.NU__OrderItem__r.RecordType.Name;
                    rowList.add(n);
                }
            }
        }

        if (rowList.size() > 0) {
            for (Row r : rowList) {
                if(r.outstandingBalance != 0.00) {
                    List<NU__Transaction__c> tList = new List<NU__Transaction__c>();
                    tList.add(r.firstTransaction);
                    arSnapshotList.add(createARSnapshotRecord(tList, r.outstandingBalance));
                }
            }
        }
    }

    //save order details as a snapshot
    private ARSnapshot__c createARSnapshotRecord (List<NU__Transaction__c> transactionsList, Decimal orderOutstandingBalance) {
        Date invoiceDate = transactionsList[0].NU__Order__r.NU__InvoiceDate__c;
        Date asOfDate = Date.today();
        if (FormFields.AsOfDate.NU__EndDate__c != null) {
            asOfDate = FormFields.AsOfDate.NU__EndDate__c;
        }
        Integer daysOutstanding = 999;
        if (invoiceDate != null) {
            daysOutstanding = invoiceDate.daysBetween(asOfDate);
        }
        NU__ARAging__c agingBucket = getAgingBucket(transactionsList[0].NU__Entity2__c, daysOutstanding);

        ARSnapshot__c snapshot = new ARSnapshot__c(
            ARAging__c = agingBucket.id,
            AsOfDate__c = asOfDate,
            Balance__c = orderOutstandingBalance,
            BillTo__c = transactionsList[0].NU__BillTo__c,
            DaysOutstanding__c = daysOutstanding,
            Entity__c = transactionsList[0].NU__Entity2__c,
            GLAccount__c = transactionsList[0].NU__GLAccount__c,
            Order__c = transactionsList[0].NU__Order__c,
            OrderType__c = transactionsList[0].NU__OrderItem__r.RecordType.Name,
            SnapshotName__c = FormFields.Name);

        return snapshot;
    }

    private NU__ARAging__c getAgingBucket (Id entityId, Integer daysOutstanding) {
        Map<Id, NU__Entity__c> entities = EntitiesMap;
        NU__Entity__c entityWithAging = entities.get(entityId);
        List<NU__ARAging__c> agings = entityWithAging.NU__ARAgings__r;
        for (NU__ARAging__c aging : agings) {
            if (daysOutstanding <= aging.NU__Days2__c){
                return aging;
            }
        }
        return null;
    }

    private Map<Id, NU__Entity__c> EntitiesMapPrivate = null;
    private Map<Id, NU__Entity__c> EntitiesMap {
        get {
            if (EntitiesMapPrivate == null) {
                EntitiesMapPrivate =
                new Map<Id, NU__Entity__c>(EntitiesList);
            }
            return EntitiesMapPrivate;
        }
    }

    //from product (BatchApexUtil)
    private static void sendNotificationForSuccesses(Database.BatchableContext context, AsyncApexJob job, Messaging.SingleEmailMessage message) {
        if (!Test.isRunningTest()) {
            sendSuccessEmail(job, message);
        }
    }

    //from product (BatchApexUtil)
    private static void sendSuccessEmail(AsyncApexJob job, Messaging.SingleEmailMessage message) {
        message.setToAddresses(new String[]{ job.CreatedBy.Email });
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
    }

    private Messaging.SingleEmailMessage buildSuccessEmail() {
        Messaging.SingleEmailMessage successEmail = new Messaging.SingleEmailMessage();

        String body = '<p>Success! Your AR Snapshot has been created. Records are stored in the "AR Snapshots" object.' +
                ' You can produce custom Salesforce Listviews by navigating to the "AR Snapshots" tab,' +
                ' and you can produce custom Salesforce Reports by navigating to the "Reports" tab and creating a ' +
                ' custom report with the Report Type of "AR Snapshots".</p>';

        successEmail.setHtmlBody(body);
        successEmail.setSubject('Your AR Snapshot has been created');
        return successEmail;
    }


    ///PART 3: Status Bar for Results section
    //based on: http://salesforce.stackexchange.com/questions/17569/best-practices-for-monitoring-scheduled-apex-and-batch-apex

    public BatchJobRecord batchJob {
        get {
            return populateBatchJob();
        }
    }

    public BatchJobRecord populateBatchJob(){

        BatchJobRecord b = new BatchJobRecord();

        AsyncApexJob a = [SELECT TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, ExtendedStatus, Id, CreatedDate, CreatedById, CreatedBy.Email, CompletedDate, ApexClassId, ApexClass.Name
            FROM AsyncApexJob
            WHERE Id =: BatchJobId
            AND JobType='BatchApex'
            ORDER BY CreatedDate desc
            LIMIT 1];
        Double itemsProcessed = a.JobItemsProcessed;
        Double totalItems = a.TotalJobItems;

        b.job = a;

        if(totalItems == 0) {
            b.percentComplete = 0;
        }
        else {
            b.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
        }

        map<string,string> bgColorMap=new map<string,string>();
        bgColorMap.put('Queued','#f8f8f8');
        bgColorMap.put('Processing','#f8f8f8');
        bgColorMap.put('Aborted','#551A8B');
        bgColorMap.put('Completed','#f8f8f8');
        bgColorMap.put('Failed','#9E0508');
        bgColorMap.put('Preparing','#f8f8f8');

        map<string,string> fgColorMap=new map<string,string>();
        fgColorMap.put('Queued','#F7B64B');
        fgColorMap.put('Processing','#F7B64B');
        fgColorMap.put('Aborted','#B23AEE');
        fgColorMap.put('Completed','#20F472');
        fgColorMap.put('Failed','#FFB6C1');
        fgColorMap.put('Preparing','#F7B64B');

        b.bgStatusColor=bgColorMap.get(a.Status);
        b.fgStatusColor=fgColorMap.get(a.Status);

        return b;
    }

    public Class BatchJobRecord {
        public AsyncApexJob job { get; set; }
        public Integer percentComplete { get; set; }
        public string bgStatusColor { get;set; }
        public string fgStatusColor { get;set; }

        //constructor
        public BatchJobRecord(){
            this.job = null;
            this.percentComplete = 0;
            bgStatusColor = null;
            fgStatusColor = null;
        }
    }

}