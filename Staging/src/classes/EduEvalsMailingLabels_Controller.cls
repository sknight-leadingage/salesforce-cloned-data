public with sharing class EduEvalsMailingLabels_Controller extends EduReportsControllerBase {
    public List<Conference_Speaker__c> SpeakerList { get;set; }
    
    public integer getRecordCount()
    {
        return setCon.getResultSize() > setCon.getPageSize() ? setCon.getPageSize() : setCon.getResultSize();
    }

    public integer getRowsLeftSide()
    {
        return getRecordCount() <= 0 ? 0 : (integer)math.roundToLong(getRecordCount() / 2);
    }
    
    public override string getPageTitle() {
        return RptRenderAs == '' && Conference != null ? Conference.Name + ': Content Categories by Session' : '';
    }
    
    public EduEvalsMailingLabels_Controller() {
        SetConPageSize = 550;
        SetQuery();
    }
    
    public override void SetConRefreshData() {
        SpeakerList = setCon.getRecords();
    }
    
    public void SetQuery() {
        SetConQuery = 'SELECT Account__r.NU__FullName__c, Account__r.PersonTitle, Account__r.Speaker_Company_Name_Rollup__c, Account__r.ShippingStreet, Account__r.ShippingCity, Account__r.ShippingState, Account__r.ShippingPostalCode FROM Conference_Speaker__c WHERE Account__c IN (SELECT Speaker__c FROM Conference_Session_Speaker__c WHERE Session__r.Conference__c = :EventId AND (Role__c = \'Speaker\' OR Role__c = \'Key Contact\')) ORDER BY Account__r.LastName, Account__r.FirstName';
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}