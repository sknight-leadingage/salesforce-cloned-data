global with sharing class PopulateRetentionStatusForDataConversion implements Database.Batchable<sObject> {
    
    global List<NU__Membership__c> start(Database.BatchableContext context) {
    	return [SELECT Id, NU__Account__c, NU__MembershipType__c, NU__StartDate__c, RetentionStatus__c FROM NU__Membership__c WHERE RetentionStatus__c = null];
    }
    
    global void execute(Database.BatchableContext context, List<NU__Membership__c> memberships) {
    	if (memberships.size() > 0) {
        	MembershipRetention.Calculate(memberships);
    	}
    }
    
    global void finish(Database.BatchableContext context) { }
}