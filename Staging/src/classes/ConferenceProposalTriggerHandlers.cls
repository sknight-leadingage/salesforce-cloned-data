public with sharing class ConferenceProposalTriggerHandlers extends NU.TriggerHandlersBase {
	
	public override void onBeforeInsert(List<Sobject> newRecords){
		List<Conference_Proposal__c> newProps = (List<Conference_Proposal__c>) newRecords;
		
		Set<Id> conferenceIds = NU.CollectionUtil.getLookupIds(newProps, 'Conference__c');
		
		Map<Id, NU__Event__c> conferencesWithLastProposal = getConferencesWithLastProposal(conferenceIds);
		
		Map<Id, Decimal> conferenceProposalNumbers = getConferenceProposalNumbers(conferencesWithLastProposal);
		
		for (Conference_Proposal__c newProp : newProps){
			if (newProp.Conference__c != null &&
			    newProp.Proposal_Number__c == null){
			
				newProp.Proposal_Number__c = conferenceProposalNumbers.get(newProp.Conference__c);
				
				Decimal nextProposalNumber = newProp.Proposal_Number__c + 1;
				
				conferenceProposalNumbers.put(newProp.Conference__c, nextProposalNumber);
			}
		}
	}
	
	public override void onAfterInsert(Map<Id, sObject> newRecordMap){
		List<Conference_Proposal__c> newProposals = (List<Conference_Proposal__c>) newRecordMap.values();
		
		List<Conference_Proposal__c> newProposalsWithSubmitter = new List<Conference_Proposal__c>();
		
		for (Conference_Proposal__c newProposal : newProposals){
			if (newProposal.Submitter__c != null){
				newProposalsWithSubmitter.add(newProposal);
			}
		}
		
		if (newProposalsWithSubmitter.size() > 0){
			List<Conference_Proposal_Speaker__c> proposalSpeakers = new List<Conference_Proposal_Speaker__c>();
			
			for (Conference_Proposal__c newProposalWithSpeaker : newProposalsWithSubmitter){
				Conference_Proposal_Speaker__c proposalSpeaker = new Conference_Proposal_Speaker__c(
					Speaker__c = newProposalWithSpeaker.Submitter__c,
					Proposal__c = newProposalWithSpeaker.Id
				);
				
				proposalSpeakers.add(proposalSpeaker);
			}
			
			insert proposalSpeakers;
		}
	}
	
	private Map<Id, NU__Event__c> getConferencesWithLastProposal(Set<Id> conferenceIds){
		List<NU__Event__c> conferences = 
		[select id,
		        name,
		        (Select id,
		                name,
		                Proposal_Number__c
		           from Conference_Proposals__r
		           order by createddate desc
		           limit 1)
		   from NU__Event__c
		  where id in :conferenceIds];
		  
		return new Map<Id, NU__Event__c>( conferences );
	}
	
	private Map<Id, Decimal> getConferenceProposalNumbers(Map<Id, NU__Event__c> conferencesWithLastProposal){
		Map<Id, Decimal> conferenceProposalNumbers = new Map<Id, Decimal>();
		
		for (Id conferenceId : conferencesWithLastProposal.keySet()){
			NU__Event__c conference = conferencesWithLastProposal.get(conferenceId);
			
			Decimal nextProposalNumber = Constant.PROPOSAL_NUMBER_STARTING_NUMBER;
			
			if (conference.Conference_Proposals__r.size() == 1 &&
			    conference.Conference_Proposals__r[0].Proposal_Number__c != null){

				nextProposalNumber = conference.Conference_Proposals__r[0].Proposal_Number__c + 1;
			}
			
			conferenceProposalNumbers.put(conferenceId, nextProposalNumber);
		}
		
		return conferenceProposalNumbers;
	}
}