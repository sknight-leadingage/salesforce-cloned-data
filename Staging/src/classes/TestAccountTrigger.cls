/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAccountTrigger {

	static List<Account> insertChildPrimaryAffiliationIndividuals(Id parentId, Integer numberOfChildrenToInsert){
		List<Account> childIndividuals = NU.DataFactoryAccount.insertIndividualAccounts(numberOfChildrenToInsert);
    	
    	Set<Id> childIndividualIds = NU.CollectionUtil.getSobjectIds(childIndividuals);
    	
    	List<NU__Affiliation__c> primaryAffils = NU.DataFactoryAffiliation.insertPrimaryAffiliations(parentId, childIndividualIds);
    	
    	return childIndividuals;
	}
	
	static void updateAccountStatePartnerId(Account accountToUpdate, Id newStatePartnerId){
		accountToUpdate.State_Partner_ID__c = newStatePartnerId;
		update accountToUpdate;
	}
	
	static void updateAccountStatePartnerIdToNull(Account accountToUpdate){
		accountToUpdate.State_Partner_Id__c = null;
		update accountToUpdate;
	}

	static List<Account> getAccounts(Set<Id> accountIds){
		return [select id,
		               name,
		               OwnerId,
		               Owner.Name
		          from Account
		         where id in :accountIds];
	}
	
	static Account getAccount(Id accountId){
		List<Account> accounts = getAccounts(new Set<Id>{ accountId });
		
		if (accounts.size() > 0){
			return accounts[0];
		}
		
		return null;
	}

	static void assertAccountOwnerIsGeneralStatePartnerUser(Id accountId){
		assertAccountsOwnerIsGeneralStatePartnerUser(new Set<Id>{ accountId });
	}
	
	static void assertAccountsOwnerIsGeneralStatePartnerUser(Set<Id> accountIds){		
		system.assert(StatePartnerOwnerUserId.GeneralStatePartnerUserId != null, 'The General State Partner Id is not set.');
		
		assertAccountsOwnerIsUser(accountIds, StatePartnerOwnerUserId.GeneralStatePartnerUserId);
	}
	
	static void assertAccountOwnerIsLeadingAgeIntegrationUser(Id accountId){
		assertAccountsOwnerIsLeadingAgeIntegrationUser(new Set<Id>{ accountId });
	}
	
	static void assertAccountsOwnerIsLeadingAgeIntegrationUser(Set<Id> accountIds){
		system.assert(StatePartnerOwnerUserId.LeadingAgeIntegrationUserId != null, 'The LeadingAge Integeration User Id is not set.');
		
		assertAccountsOwnerIsUser(accountIds, StatePartnerOwnerUserId.LeadingAgeIntegrationUserId);
	}

	static void assertAccountOwnerIsUser(Id accountId, User expectedOwner){
		assertAccountsOwnerIsUser(new Set<Id>{ accountId }, expectedOwner);
	}
	
	static void assertAccountsOwnerIsUser(Set<Id> accountIds, User expectedOwner){
		assertAccountsOwnerIsUser(accountIds, expectedOwner.Id);
	}
	
	static void assertAccountsOwnerIsUser(Set<Id> accountIds, Id expectedOwnerId){
		List<Account> accounts = getAccounts(accountIds);
		
		for (Account account : accounts){
			system.assertEquals(expectedOwnerId, account.OwnerId, 'The owner, ' + account.Owner.Name + ' is not the expected one, ' + expectedOwnerId);
		}
	}
	
	static void assertAccountOwnerIsNOTLeadingAgeIntegrationUserOrGeneralStatePartnerUser(Id accountId){
		Account account = getAccount(accountId);
		
		system.assert (account != null, 'The account was not found.');
		
		system.assert(account.OwnerId != StatePartnerOwnerUserId.GeneralStatePartnerUserId, 'The account is assigned to the general state partner user when it should not be.');
		system.assert(account.OwnerId != StatePartnerOwnerUserId.LeadingAgeIntegrationUserId, 'The account is assigned to the LeadingAge Integration user when it should not be.');
	}

	@isTest(SeeAllData=false)
	static void statePartnerUserEditingStateProviderAllowedTest(){
		Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

    	Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();      	
        	statePartnerProvider.Program_Service_Revenue__c = 10000;
        	
        	update statePartnerProvider;
        }
	}
	
	@isTest(SeeAllData=false)
	static void statePartnerUserEditingProviderFromDifferentStateButMSOInStateTest(){
		Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

		Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account statePartner2Provider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        
        Account inStateMSO = DataFactoryAccountExt.insertMultiSiteAccount(null, statePartnerAccount.Id);
        
        NU__Affiliation__c providerMSOAffil = NU.DataFactoryAffiliation.insertAffiliation(statePartner2Provider.Id, inStateMSO.Id, true);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare statePartner2ProviderShare = DataFactoryAccountShare.insertAccountShare(statePartner2Provider.Id, statePartnerPortalUser.Id);
        AccountShare inStateMSOShare = DataFactoryAccountShare.insertAccountShare(inStateMSO.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();
        	statePartner2Provider.Program_Service_Revenue__c = 10000;
        	
        	update statePartner2Provider;
        }
	}

	@isTest(SeeAllData=false)
    static void statePartnerUserEditingOutOfStateProviderAccountValidationTest() {
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

    	Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account statePartner2Provider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartner2Provider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();      	
        	statePartner2Provider.Program_Service_Revenue__c = 10000;
        	
        	TestUtil.assertUpdateValidationRule(statePartner2Provider, Constant.STATE_PARTNER_EDIT_VAL_MSG);
        }
    }

    @isTest(SeeAllData=false)
    static void statePartnerUserEditingProviderMembershipInStateTest() {
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

    	Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();      	
        	statePartnerProvider.Provider_Lapsed_On__c = Date.Today();
        	
        	update statePartnerProvider;
        }
    }
    
    @isTest(SeeAllData=false)
    static void statePartnerUserCancellingProviderMembershipInStateAttachedToInStateMSOTest() {
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

		Account statePartnerMSO = DataFactoryAccountExt.insertMultiSiteAccount(null, statePartnerAccount.Id);

    	Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        NU.DataFactoryAffiliation.insertAffiliation(statePartnerProvider.Id, statePartnerMSO.Id, true);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare providerAccountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        AccountShare msoAccountShare = DataFactoryAccountShare.insertAccountShare(statePartnerMSO.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();      	
        	statePartnerProvider.Provider_Lapsed_On__c = Date.Today();
        	
        	update statePartnerProvider;
        }
    }
    
    @isTest(SeeAllData=false)
    static void statePartnerUserCancellingProviderMembershipOutOfStateAttachedToInStateMSOValidationTest() {
    	Account statePartnerAccount1 = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();

		Account statePartnerMSO = DataFactoryAccountExt.insertMultiSiteAccount(null, statePartnerAccount1.Id);

    	Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        NU.DataFactoryAffiliation.insertAffiliation(statePartnerProvider.Id, statePartnerMSO.Id, true);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount1.Id);

        AccountShare providerAccountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        AccountShare msoAccountShare = DataFactoryAccountShare.insertAccountShare(statePartnerMSO.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();     	
        	statePartnerProvider.Provider_Lapsed_On__c = Date.Today();
        	
        	TestUtil.assertUpdateValidationRule(statePartnerProvider, Constant.STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG);
        }
    }


	@isTest(SeeAllData=false)
    static void statePartnerUserEditingOutOfStateButInStateMSOProviderMembershipValidationTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

		Account statePartnerAccount2 = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account statePartner2Provider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount2.Id);
        
        Account inStateMSO = DataFactoryAccountExt.insertMultiSiteAccount(null, statePartnerAccount.Id);
        
        NU__Affiliation__c providerMSOAffil = NU.DataFactoryAffiliation.insertAffiliation(statePartner2Provider.Id, inStateMSO.Id, true);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare statePartner2ProviderShare = DataFactoryAccountShare.insertAccountShare(statePartner2Provider.Id, statePartnerPortalUser.Id);
        AccountShare inStateMSOShare = DataFactoryAccountShare.insertAccountShare(inStateMSO.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
        	UserQuerier.clearCurrentUserCache();
        	      	
        	statePartner2Provider.Provider_Lapsed_On__c = Date.Today();
        	
        	TestUtil.assertUpdateValidationRule(statePartner2Provider, Constant.STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG);
        }
    }

    @isTest(SeeAllData=false)
    static void statePartnerUserEditingIndividualInProviderStateTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();

    	Account statePartnerProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
        
        Account statePartnerProviderEmployee = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(statePartnerProviderEmployee.Id, statePartnerProvider.Id, true);
        
        User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAccount.Id);

        AccountShare accountShare = DataFactoryAccountShare.insertAccountShare(statePartnerProvider.Id, statePartnerPortalUser.Id);
        AccountShare employeeShare = DataFactoryAccountShare.insertAccountShare(statePartnerProviderEmployee.Id, statePartnerPortalUser.Id);
        
        System.runAs (statePartnerPortalUser){
            UserQuerier.clearCurrentUserCache();     	
        	statePartnerProviderEmployee.LastName = 'Different Last Name';
        	
        	update statePartnerProviderEmployee;
        }
    }

    @isTest(SeeAllData=false)
    static void individualAffiliatesHimselfWithProviderSoAccountOwnerBecomesGeneralStatePartnerUserTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account providerAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
    	
    	Account statePartnerProviderEmployee = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(statePartnerProviderEmployee.Id, providerAccount.Id, true);
    	
    	assertAccountOwnerIsGeneralStatePartnerUser(statePartnerProviderEmployee.Id);
    }

	@isTest(SeeAllData=false)
    static void existingCompanyAccountBecomesAffiliatedWithStatePartnerSoAccountOwnerBecomesGeneralStatePartnerTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
    	
    	assertAccountOwnerIsNOTLeadingAgeIntegrationUserOrGeneralStatePartnerUser(providerAccount.Id);
    	
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	
    	updateAccountStatePartnerId(providerAccount, statePartnerAccount.Id);
    	
    	assertAccountOwnerIsGeneralStatePartnerUser(providerAccount.Id);
    }
    
    @isTest(SeeAllData=false)
    static void existingCompanyWithImmediateChildrenBecomesAffiliatedWithStatePartnerSoAllTheirAccountOwnersBecomeGeneralStateParnterUserTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
    	
    	List<Account> providerIndividuals = NU.DataFactoryAccount.insertIndividualAccounts(10);
    	
    	Set<Id> providerIndividualIds = NU.CollectionUtil.getSobjectIds(providerIndividuals);
    	
    	List<NU__Affiliation__c> primaryAffils = NU.DataFactoryAffiliation.insertPrimaryAffiliations(providerAccount.Id, providerIndividualIds);
    	
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	
    	updateAccountStatePartnerId(providerAccount, statePartnerAccount.Id);
    	
    	Set<Id> providerWithChildrenIds = new Set<Id> { providerAccount.Id };
    	providerWithChildrenIds.addAll(providerIndividualIds);
    	
    	assertAccountsOwnerIsGeneralStatePartnerUser(providerWithChildrenIds);
    }
    
    @isTest(SeeAllData=false)
    static void existingProviderWithImmediateChildrenAndGrandChildrenBecomesAffiliatedWithStatePartnerSoAllTheirAccountOwnersBecomeGeneralStateParnterUserTest(){
    	Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
    	
    	Account subProviderAccount = DataFactoryAccountExt.insertProviderAccount();
    	NU.DataFactoryAffiliation.insertAffiliation(subProviderAccount.Id, providerAccount.Id, true);

    	List<Account> providerIndividuals = insertChildPrimaryAffiliationIndividuals(providerAccount.Id, 10);
    	
    	List<Account> subProviderIndividuals = insertChildPrimaryAffiliationIndividuals(subProviderAccount.Id, 10);
    	
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	
    	updateAccountStatePartnerId(providerAccount, statePartnerAccount.Id);
    	
    	Set<Id> accountIds = new Set<Id> { providerAccount.Id };
    	accountIds.addAll(NU.CollectionUtil.getSobjectIds(providerIndividuals));
    	accountIds.addAll(NU.CollectionUtil.getSobjectIds(subProviderIndividuals));
    	
    	assertAccountsOwnerIsGeneralStatePartnerUser(accountIds);
    }

	@isTest(SeeAllData=false)
    static void createStatePartnerPortalUserThenCreateStatePartnerThenLinkTogetherTest(){
    	User statePartnerPortalUser = DataFactoryUser.insertStatePartnerPortalUser(null);
    	
    	User statePartnerPortalUserQueried = [select id, name, Contact.AccountId from User where id = :statePartnerPortalUser.Id];
    	
    	Account statePartnerPortalAccount = getAccount(statePartnerPortalUserQueried.Contact.AccountId);
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	
    	statePartnerPortalAccount.State_Partner_Id__c = statePartnerAccount.Id;
    	update statePartnerPortalAccount;
    }


	@isTest(SeeAllData=false)
    static void createNewAccountWithPrimaryAffiliationToAStatePartnerSoOwnerSetGeneralStatePartnerUserTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account parentProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
    	
    	Account childProvider = DataFactoryAccountExt.insertProviderAccount(null, parentProvider.Id);
    	
    	assertAccountOwnerIsGeneralStatePartnerUser(childProvider.Id);
    }

    @isTest(SeeAllData=false)
    static void newGrandChildAccountWithPrimaryAffiliationWhoseOWnerIsGeneralStatePartnerSoOwnerSetToGeneralStatePartnerUserTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account parentProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
    	
    	Account childProvider = DataFactoryAccountExt.insertProviderAccount(null, parentProvider.Id);
    	
    	assertAccountOwnerIsGeneralStatePartnerUser(childProvider.Id);
    	
    	Account grandChildProvider = DataFactoryAccountExt.insertProviderAccount(null, childProvider.Id);
    	assertAccountOwnerIsGeneralStatePartnerUser(grandChildProvider.Id);	
    }

	@isTest(SeeAllData=false)  
    static void newGrandChildAccountWithPrimaryAffiliationFromNewAffiliationRecordWhoseOWnerIsGeneralStatePartnerSoOwnerSetToGeneralStatePartnerUserTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
    	Account parentProvider = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(null, statePartnerAccount.Id);
    	
    	Account childProvider = DataFactoryAccountExt.insertProviderAccount(null, parentProvider.Id);
    	
    	assertAccountOwnerIsGeneralStatePartnerUser(childProvider.Id);
    	
    	Account grandChildProvider = DataFactoryAccountExt.insertProviderAccount(null);
    	
    	NU.DataFactoryAffiliation.insertAffiliation(grandChildProvider.Id, childProvider.Id, true);
    	
    	assertAccountOwnerIsGeneralStatePartnerUser(grandChildProvider.Id);
    }

    static testmethod void newCorporateProviderAffiliatedWithCorporateMSOSoGetsMembershipTest(){
    	Account corporateMSO = DataFactoryAccountExt.insertCorporateMultiSiteAccount(null, null);
    	NU__Membership__c corporateMSOMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(corporateMSO.Id);
    	
    	Account corporateProvider = DataFactoryAccountExt.insertMultiSiteEnabledProviderAccount(null);
    	NU.DataFactoryAffiliation.insertAffiliation(corporateProvider.Id, corporateMSO.Id, true);
    	
    	TestJointStateProviderBiller.assertCorporateProvidersHaveMemberships(new List<Account>{ corporateProvider }, corporateMSO);
    }
  
    static testmethod void newCorporateMSOProviderBecomesCorporateProviderSoGetsMembershipTest(){
    	Account corporateMSO = DataFactoryAccountExt.insertCorporateMultiSiteAccount(null, null);
    	NU__Membership__c corporateMSOMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(corporateMSO.Id);
    	
    	Account nonCorporateProvider = DataFactoryAccountExt.insertProviderAccount();
    	NU.DataFactoryAffiliation.insertAffiliation(nonCorporateProvider.Id, corporateMSO.Id, true);

		Account nonCorporateProviderQueried = [select id, name, Provider_Membership__c from Account where id = :nonCorporateProvider.Id];
		system.assert(nonCorporateProviderQueried.Provider_Membership__c == null, 'The non corporate provider account has a provider membership when it should not.');
		nonCorporateProvider.Multi_Site_Enabled__c = true;
		update nonCorporateProvider;
    	
    	TestJointStateProviderBiller.assertCorporateProvidersHaveMemberships(new List<Account>{ nonCorporateProvider }, corporateMSO);
    }
    
    static testmethod void newCorporateMSOProviderOnInsertBecomesCorporateProviderSoGetsMembershipTest(){
    	Account corporateMSO = DataFactoryAccountExt.insertCorporateMultiSiteAccount(null, null);
    	NU__Membership__c corporateMSOMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(corporateMSO.Id);
    	
    	Account corporateProvider = DataFactoryAccountExt.createProviderAccount(null);
    	corporateProvider.Multi_Site_Enabled__c = true;
    	corporateProvider.NU__PrimaryAffiliation__c = corporateMSO.Id;
    	insert corporateProvider;
    	
    	TestJointStateProviderBiller.assertCorporateProvidersHaveMemberships(new List<Account>{ corporateProvider }, corporateMSO);
    }

	@isTest(SeeAllData=false)
    static void newProviderStatePartnerNotSetFromNonStatePartnerCreationTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();        
        
        Account newStateProvider = DataFactoryAccountExt.insertProviderAccount();
        
        Account newStateProviderQueried = AccountQuerier.getAccountById(newStateProvider.Id);
        system.assertEquals(null, newStateProviderQueried.State_Partner_Id__c, 'The provider created by a non state partner user was assigned to a state.');
    }

    @isTest(SeeAllData=false)
	static void newStatelessMSOShouldBeOwnedByGeneralUser(){
		Account newStatelessMSO = DataFactoryAccountExt.createMultiSiteAccount(null);
		newStateLessMSO.State_Partner_ID__c = null;
		insert newStatelessMSO;
		
		Account newStatelessMSOQueried = getAccount(newStatelessMSO.Id);
		system.assertEquals(StatePartnerOwnerUserId.GeneralStatePartnerUserId, newStatelessMSOQueried.OwnerId, 'The new stateless MSO is not owned by the general user.');
	}
	
	@isTest(SeeAllData=false)
	static void newdefaultStatePartnerIDFromEntity(){
		Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        
        Account Statepartner = DataFactoryAccountExt.createStatePartnerAccount();
        
        Statepartner.Name = 'LeadingAge Testing';
        insert Statepartner;
		NU__Entity__c TestEntity = new NU__Entity__c(name='LeadingAge Testing');
        insert TestEntity;
        
        //NU__Entity__c Tempentity = [Select id from NU__Entity__c where Name = 'LeadingAge Testing' limit 1 ];
		Account testPerson = new Account(FirstName = 'FN Temp Person', LastName = 'LN Temp Person' + sUqDate, PersonEmail = 'tempemail' + dtNow.format('yymmddhmmssS') + '@test.com');
        insert testPerson;
		
		 testPerson.NU__PrimaryEntity__c = TestEntity.Id;
		 update testPerson;
		
		testPerson = [Select Id, State_Partner_Id__c from Account where Id = :testPerson.Id];
		if (testPerson.State_Partner_Id__c == Statepartner.Id) {
			system.assertEquals(testPerson.State_Partner_Id__c, Statepartner.Id, 'The user did not get his State Partner Id from his PrimaryEntity ');
		}
	}

}