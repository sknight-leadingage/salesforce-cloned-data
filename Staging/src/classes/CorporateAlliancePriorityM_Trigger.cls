public with sharing class CorporateAlliancePriorityM_Trigger extends NU.TriggerHandlersBase {
	private void UpdateAccountMembershipPriorityFields(Map<Id, sObject> newRecordMap){
		List<NU__Membership__c> lMembers = (List<NU__Membership__c>)newRecordMap.values();
		List<Account> mAccounts = new List<Account>();
		Map<Id, Account> m2Accounts = new Map<Id, Account>();
		
		Account a;
		for(NU__Membership__c s : lMembers){
			a = new Account(Id = s.NU__Account__c);
			
			  mAccounts.add(a);		
		}
		
		mAccounts = [select Id,RecordTypeId,NU__RecordTypeName__c, Type from Account where Id IN :mAccounts];
		
		for(Account s : mAccounts){
			
			
			 if (s.RecordTypeId == Constant.ACCOUNT_CORPORATE_ALLIANCE_SPONSOR_RECORD_TYPE_ID){
			 	m2Accounts.put(s.Id, s);
			 	
			 }
			  		
		}
		
		if (!m2Accounts.IsEmpty()){
			CorporateAllianceMembershipPriority.setCorporateAllianceMembershipPriority(m2Accounts);
			List<Account> lAccountUpdate = m2Accounts.values();
			update lAccountUpdate;
		}
	}
	
	
	public override void onAfterInsert(Map<Id, sObject> newRecordMap){
		UpdateAccountMembershipPriorityFields(newRecordMap);
	}
	
	public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
		UpdateAccountMembershipPriorityFields(newRecordMap);
	}
}

//if (a.RecordTypeId == Constant.ACCOUNT_CORPORATE_ALLIANCE_SPONSOR_RECORD_TYPE_ID){