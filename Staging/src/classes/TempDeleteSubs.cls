global class TempDeleteSubs implements Database.Batchable<sobject>
{
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator('SELECT Id FROM Online_Subscription__c WHERE LastModifiedDate < LAST_N_DAYS:30');
   }
   global void execute(Database.BatchableContext BC,List<sObject> scope)
   {
		List<Online_Subscription__c> lSubs = (List<Online_Subscription__c>)scope;    	
   	
		if (lSubs.size() > 0) {
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger_Subscriptions');
			delete lSubs;
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger_Subscriptions');
		}
   }

   global void finish(Database.BatchableContext BC)
   {
   }
}