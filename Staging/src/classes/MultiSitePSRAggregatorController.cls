public with sharing class MultiSitePSRAggregatorController {
	public static final String SUCCESS_MSG = 'The multi-site program service revenue has been updated.';
	private Id AccountRecordId = null;
	
	private Boolean showFormPriv = true;
	public Boolean ShowForm{
		get{
			return showFormPriv;
		}
		set{
			showFormPriv = value;
		}
	}
	
	private void addSuccessMessage(){
		ApexPages.Message successMessage = new ApexPages.Message(ApexPages.Severity.INFO, SUCCESS_MSG);
		ApexPages.addMessage(successMessage);
	}
	
	public MultiSitePSRAggregatorController(ApexPages.StandardController controller) {
		AccountRecordId = controller.getId();
	}
	
	public void Calculate(){
		MultiSiteProgramServiceRevenueAggregator agg = new MultiSiteProgramServiceRevenueAggregator();
		agg.aggregate(AccountRecordId);
		
		ShowForm = false;
		
		addSuccessMessage();
	}
}