public with sharing class SpecialAgreementPricer extends MembershipPricerBase implements IProductPricer {


    
     public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
     
		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(membershipType, productToPrice);
		
		//poduct type should be membership
		if (productPricingRequest.MembershipTypeId == null){
			system.debug('   SpecialAgreementPricer:: Membership Type Id is null');
			return null;
		}
		

		
		if(customer.Billing_Type__c == null) {
			system.debug('   SpecialAgreementPricer:: Billing Type not set');
			return null;
		}
		
		if(customer.Billing_Type__c.containsIgnoreCase('Special Agreement:'))
			{
			   if(customer.Billing_Type__c.containsIgnoreCase('VOA'))
		         {
		           
		           return 600;
		         }
	         	else if(customer.Billing_Type__c.containsIgnoreCase('POAH'))
			         	{
			         		return 350;
			         	}
			         	else if(customer.Billing_Type__c.containsIgnoreCase('NCR'))
				         	{
				         		return 350;
				         	}
				         	else if(customer.Billing_Type__c.containsIgnoreCase('TCB'))
				         	     {
				         	     	return 350;
				         	     }
				         	     else 
				         	     {
				         	     	return null;
				         	     }
			         	
			}
	      else 
	         {
	         	return null;
	         }
     }
}