public with sharing class LeadershipAcademyAlumniReport_Controller {
    public static final String ACADEMY_YEAR_PARAM = 'acadyear';
    public string SelectedEvent {get; set;} 
    public List<Academy_Status__c> AcademyAlumniList{get;set;}
    public List<ClassSummary> ClassSummaryInfo { get; set; }
    public Boolean ShowAllYears { get; set; }
    
    public string AcademyYear
    {
        get
        {
            return ApexPages.CurrentPage().getParameters().get(ACADEMY_YEAR_PARAM);
        }
   
    }
    
    //a drop down list of Events
    public List<SelectOption> getEventList(){
        List<SelectOption> options = new List<SelectOption>();
        
        List<NU__Event__c> EventListData = [SELECT Id, Name FROM NU__Event__c WHERE CALENDAR_YEAR(NU__StartDate__c) > 2010 AND NU__Entity__r.Name = 'LeadingAge' AND NU__Type__c = 'Conference' ORDER BY NU__StartDate__c DESC];
        for (NU__Event__c a: EventListData) {
            options.add(new SelectOption(a.Id, a.Name));
        }
        return options;

       
    }

     public void Event_onChange(){
          AcademyAlumniList = getAcademyAlumniList();
                  
    }
    

    public LeadershipAcademyAlumniReport_Controller(){
            AcademyAlumniList = getAcademyAlumniList();

            if (ShowAllYears == null) {
                ShowAllYears = false;
            }

            if (AcademyYear != null) {            
                AggregateResult[] results = [SELECT Academy_Class_Status__c, COUNT(Id) sc FROM Academy_Status__c WHERE Academy_Year__c = :AcademyYear GROUP BY Academy_Class_Status__c];
                if (results.size() > 0) {
                    ClassSummaryInfo = new List<ClassSummary>();
                    for (AggregateResult ar : results) {
                        ClassSummaryInfo.add(new ClassSummary(ar));
                    }
                }
            }
    }

    private List<Academy_Status__c> getAcademyAlumniList(){
        
        if(AcademyYear != null && selectedEvent != null)
            {
                List<Academy_Status__c> AcademyAlumniList = new List<Academy_Status__c>();
                
                if (ShowAllYears == false) {
                    AcademyAlumniList =[SELECT Account__r.name, Account__r.PersonEmail, Academy_Class_Status__c, Academy_Year__c 
                        FROM Academy_Status__c WHERE Academy_Year__c = :AcademyYear AND Academy_Class_Status__c = 'Alumni' 
                        AND Account__c IN (SELECT NU__Account__c FROM NU__Registration2__c WHERE NU__Event__c = :SelectedEvent)];
                }
                else {
                    AcademyAlumniList =[SELECT Account__r.name, Account__r.PersonEmail, Academy_Class_Status__c, Academy_Year__c 
                        FROM Academy_Status__c WHERE Academy_Class_Status__c = 'Alumni' 
                        AND Account__c IN (SELECT NU__Account__c FROM NU__Registration2__c WHERE NU__Event__c = :SelectedEvent)
                        ORDER BY Academy_Year__c DESC];
                }
            
                return AcademyAlumniList;
            }
    
    
    return null;
    }


    public class ClassSummary {
        public Integer StatusCount { get; private set; }
        public String Status { get; private set; }

        public ClassSummary(AggregateResult ar) {
            StatusCount = (Integer) ar.get('sc');
            Status = (String) ar.get('Academy_Class_Status__c');
            
            if (Status == null) {
                Status = '(No Status Set)';
            }
        }
    }

}