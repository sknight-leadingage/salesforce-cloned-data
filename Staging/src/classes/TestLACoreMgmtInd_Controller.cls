/*
* Test methods for the LeadershipAcademyCoreMgmtInd_Controller
*/
@isTest(SeeAllData=true)

private class TestLACoreMgmtInd_Controller{

    static LeadershipAcademyCoreMgmtInd_Controller loadController(){
        PageReference pageRef = Page.LeadershipAcademyCoreMgmtInd;
        Test.setCurrentPage(pageRef);
        
        string SelectedAcademyYear = '2016';
        
        pageRef.getParameters().put(LeadershipAcademyControllerBase.ACADEMY_YEAR_PARAM, SelectedAcademyYear);
        pageRef.getParameters().put('acc', 'a1pd00000028oUKAAY'); //one of 2016 Applicant accountId
        
        LeadershipAcademyCoreMgmtInd_Controller AppController = new LeadershipAcademyCoreMgmtInd_Controller();
        
        return AppController;
     }
    
    static testmethod void DisplayResultTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        system.assert(AppController.AcademyCoreMgmtInd != null);
        
        //Display Application Status Drop-Down
        system.assert(AppController.getAppStatuses().size() > 0, 'The application status should have multiple values. Size is: ' + AppController.getAppStatuses().size());
     }
     
     static testmethod void SaveApplicationStatus(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        
        //update Application Status
        AppController.AcademyCoreMgmtInd.Application_Status__c = 'Accepted';
        AppController.ProcessSave();
     }
     
    static testmethod void AddToClass(){     
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        AppController.AddToClass();
        system.assertEquals('Applicant', AppController.AcademyCoreMgmtInd.Academy_Status__r.Academy_Class_Status__c );
        system.assertEquals('2016', AppController.AcademyCoreMgmtInd.Academy_Status__r.Academy_Year__c );
        
        
    }
    static testmethod void hasNextTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        //system.assert(AppController.hasNext == false);
    }
    
    static testmethod void hasPreviousTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        system.assert(AppController.hasPrevious == false);
    }
    
    static testmethod void ResultSizeTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        system.assert(AppController.ResultSize > 0);
    }
    
   static testmethod void firstTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        AppController.first();
    }
    
    static testmethod void lastTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        AppController.last();
    }
    
    static testmethod void previousTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        AppController.previous();
    }
    
    static testmethod void nextTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        AppController.next();
    }
    
    static testmethod void cancelTest(){
        LeadershipAcademyCoreMgmtInd_Controller AppController = loadController();
        AppController.cancel();
    }
}