public with sharing class JointStateProviderBillingController {
	public Joint_Billing__c JointBilling { get; set; }
	
	public Boolean ShowForm { get; set; }
	
	private List<Account> statePartnerAccountsPriv = null;
	public List<Account> StatePartnerAccounts{
		get{
			if (statePartnerAccountsPriv == null){
				statePartnerAccountsPriv = 
				[select id,
				        name
				   from Account
				  where RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER
				     or (RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE and
				         Multi_Site_Bill_Jointly__c = true)
				  order by name];
			}
			
			return statePartnerAccountsPriv;
		}
	}
	
	public List<SelectOption> StatePartnerAccountSelectOpts{
		get{
			List<SelectOption> opts = new List<SelectOption>();
			
			for (Account statePartnerAccount : StatePartnerAccounts){
				SelectOption opt = new SelectOption(statePartnerAccount.Id, statePartnerAccount.Name);
				opts.add(opt);
			}
			
			return opts;
		}
	}
	
	public JointStateProviderBillingController(){
		JointBilling = new Joint_Billing__c(
			Bill_Date__c = Date.Today()			
		);
		
		ShowForm = true;
	}
	
	
	public void runBilling(){
		JointStateProviderBiller jspb = null;
		
		try{
			jspb = new JointStateProviderBiller(JointBilling);
			
			String apexSyncJobId = Database.executeBatch(jspb, 50);
		
			ShowForm = false;
		}
		catch (Exception ex){
			ApexPages.addMessages(ex);
		}
	}
}