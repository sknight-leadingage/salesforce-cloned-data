public with sharing class DataFactoryAccountShare {
	public static AccountShare insertAccountShare(Id accountIdToShare, Id userIdToShareTo){
		AccountShare accountShare = new AccountShare(
        	AccountAccessLevel = 'Edit',
        	OpportunityAccessLevel = 'Edit',
        	CaseAccessLevel = 'Edit',
        	AccountId = accountIdToShare,
        	UserOrGroupId = userIdToShareTo
        );
        
        insert accountShare;
        
        return accountShare;
	}
}