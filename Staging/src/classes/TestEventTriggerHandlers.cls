/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
public class TestEventTriggerHandlers
{
    public static testMethod void ValidateTrigger()
    {

		//create two events, the first is a 'current' event
        NU__Event__c evt = NU.DataFactoryEvent.insertEvent();
        NU__Event__c evt2 = NU.DataFactoryEvent.insertEvent();
        evt.Current_Year_Annual_Event__c = true;
        update evt;
        System.assert(evt.Current_Year_Annual_Event__c);

		//try to set the second event to true to trigger the expected error message        
        try
        {
            evt2.Current_Year_Annual_Event__c = true;
            update evt2;
            System.assert(false);
        }
        catch(Exception e)
        {
            System.assert(e.getMessage().contains('Another event is already marked as the current year event'));
        }

		//delete the first event and set the second event to true        
        delete evt;
        evt2.Current_Year_Annual_Event__c = true;
        update evt2;

		//try to undelete the first event to trigger the expected error message        
        try
        {
            undelete evt;
            System.assert(false);
        }
        catch(DmlException e)
        {
            System.debug(e.getMessage());
            System.assert(e.getMessage().contains('Another event is already marked as the current year event'));
        }
    }
}