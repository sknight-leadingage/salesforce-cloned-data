public without sharing class MasterDetailDonationUpdater implements Database.Batchable<SObject> {
    public static void execute() {
        execute(200);
    }
    
    public static void execute(Integer batchSize) {
        Database.executeBatch(new MasterDetailDonationUpdater(), 200);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT NU__Product__c FROM NU__Donation__c WHERE NU__Product2__c = null]);
    }

    public void execute(Database.BatchableContext bc, List<NU__Donation__c> donations) {
        for (NU__Donation__c donation : donations) {
            donation.NU__Product2__c = donation.NU__Product__c;
        }
        update donations;
    }

    public void finish(Database.BatchableContext bc) { }
}