/*
* Test Code for  CreateCompany
*/
@isTest(SeeAllData=true)
private class TestCreateCompanyWholeWizardFlow {
    static final integer WizardMax = 8; //Remember to change this if more steps are added
    
    static testmethod void ProviderFlowNotAttachedToParentTest(){
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        system.assertEquals('Company', ccc.GetMSOorMCDescription);
                
        //Opening Step of Wizard
        ccc.companyTypes = Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID;
        ccc.WizardNext();
        
        //Choosing to Add a Provider skips us to Step 4, so let's check that
        system.assertEquals(4, ccc.WizardStep); //4
        
        system.assertEquals('Provider', ccc.GetMSOorMCDescription);
        
        //Say no to 'Attach to Parent MSO/MC'
        ccc.optChooseMSO = '2';
        ccc.WizardNext();
        
        //We should be at Step 6
        system.assertEquals(6, ccc.WizardStep); //6
        
        //Set some dummy values
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        ccc.sNewProviderName = 'TM Provider' + sUqDate;
        ccc.newProvider.BillingStreet = '1 Test Pl';
        ccc.newProvider.BillingCity = 'Test Town';
        ccc.newProvider.BillingState = 'CA';
        ccc.newProvider.BillingPostalCode = '90210';
        ccc.sFirstName = 'TM First Name' + sUqDate;
        ccc.sLastName = 'TM Last Name' + sUqDate;        
        ccc.newProvider.Phone = '1231231234';
        ccc.sPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';              
                        
        //We move to step 7 after entering data
        ccc.WizardNext();
        system.assertEquals(ccc.WizardStep, 7);
        
        //TODO: SK 20130408 I guess some of the Demographics fields will be required later?  Update code below if so
        ccc.newProvider.Program_Service_Revenue__c = 12346;
        ccc.newProvider.Revenue_Year_Submitted__c = string.valueof(datetime.now().year());
        ccc.newProvider.Provider_Join_On__c = datetime.now().date();        
        
        ccc.WizardNext();
        system.assertEquals(ccc.WizardStep, WizardMax);
        
        //Now that we've made it to the end, let's test the Back button!
        ccc.WizardBack();
        system.assertEquals(ccc.WizardStep, 7);
        
        ccc.WizardBack();
        system.assertEquals(ccc.WizardStep, 6);
        
        ccc.WizardBack();
        system.assertEquals(4, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void ProviderFlowAttachedToParentTest(){
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        //Opening Step of Wizard
        ccc.companyTypes = Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID;
        ccc.WizardNext();
        
        //Choosing to Add a Provider skips us to Step 4, so let's check that
        system.assertEquals(4, ccc.WizardStep); //4
        
        //Say yes to 'Attach to Parent MSO/MC'
        ccc.optChooseMSO = '1';
        ccc.WizardNext();
        
        //We should be at Step 5
        system.assertEquals(5, ccc.WizardStep); //5
        
        //Set up values used here and later on
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        ccc.selectedMSO_Name = 'TM MSC or MC Test' + sUqDate;

        List<Account> testMSOs = new List<Account>();
        testMSOs.add(new Account(Name = ccc.selectedMSO_Name));
        insert testMSOs;

        //Pretend to exercise the Search
        ccc.sFindMSOSearchText = ccc.selectedMSO_Name;
        ccc.performSearch();
        ccc.selectRow(); //Doesn't really do anything from the point of view of this test
        List<Account> tempList = ccc.getdsMSOList();
        tempList = [SELECT Id FROM Account WHERE Name = :ccc.selectedMSO_Name];
        ccc.selectedMSO_Id = tempList[0].Id;
        
        //We've done whatever with searching, now move to next step
        ccc.WizardNext();
        
        //We should be at Step 6
        system.assertEquals(6, ccc.WizardStep); //6
        
        //Kind of a pointless assert as we just set it above, but if we do the search properly, this would be useful
        system.assertNotEquals(null, ccc.selectedMSO_Id);
        
        //Set some dummy values
        ccc.sNewProviderName = 'TM Provider Attached' + sUqDate;
        ccc.newProvider.BillingStreet = '1 Test Pl';
        ccc.newProvider.BillingCity = 'Test Town';
        ccc.newProvider.BillingState = 'CA';
        ccc.newProvider.BillingPostalCode = '90210';
        ccc.sFirstName = 'TM First Name' + sUqDate;
        ccc.sLastName = 'TM Last Name' + sUqDate;
        ccc.sPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
        ccc.newContact.Phone = '1231231234';
        
        //We move to step 7 after entering data
        ccc.WizardNext();
        system.assertEquals(ccc.WizardStep, 7);
        
        //TODO: SK 20130408 I guess some of the Demographics fields will be required later?  Add code below if so
        ccc.newProvider.Program_Service_Revenue__c = 12346;
        ccc.newProvider.Revenue_Year_Submitted__c = string.valueof(datetime.now().year());
        ccc.newProvider.Provider_Join_On__c = datetime.now().date();   
        
        //Now that Demographics are set up save the provider
        ccc.WizardNext(); //GC - Provider Save moved to within  WizardNext()
        system.assertEquals(ccc.WizardStep, WizardMax);
    }
    
    static testmethod void MSOMCFlowCreateMSOWithoutAttachedProvider(){
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        //Opening Step of Wizard
        ccc.companyTypes = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID;
        ccc.WizardNext();
        
        //Choosing to create an MSO or MC takes us to Step 2... the difference is that ccc.newMSOMC.Type is null for MSO
        system.assertEquals(ccc.WizardStep, 2);
        system.assertEquals(null, ccc.newMSOMC.Type);
        system.assertEquals(Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE, ccc.GetMSOorMCDescription);
        
        //Set some dummy values
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        ccc.sNewMSOMCName = 'TM Provider' + sUqDate;
        ccc.newMSOMC.BillingStreet = '1 Test Pl';
        ccc.newMSOMC.BillingCity = 'Test Town';
        ccc.newMSOMC.BillingState = 'CA';
        ccc.newMSOMC.BillingPostalCode = '90210';
        ccc.sMFirstName = 'TM First Name' + sUqDate;
        ccc.sMLastName = 'TM Last Name' + sUqDate;
        ccc.sMPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
        ccc.newMContact.Phone = '1231231234';
        
        //We've set our values, moving to the next step should save them...
        ccc.WizardNext();
        List<Account> tempMSO = [SELECT Id FROM Account WHERE Name = :ccc.sNewMSOMCName];
        system.assertNotEquals(null, tempMSO.size());
        system.assertNotEquals(0, tempMSO.size());

        //...meaning that we're now at Step 3 of the Wizard
        system.assertEquals(3, ccc.WizardStep);
        
        //Now we decide to choose 'No' to the question: Do you want to attach a provider site
        ccc.optYesNoSelected = '2';
        ccc.WizardNext();
        
        //We should be at the end of the Wizard now!
        system.assertEquals(WizardMax, ccc.WizardStep);
    }
    
    static testmethod void MSOMCFlowCreateMSOWithAttachedProvider(){
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        //Opening Step of Wizard
        ccc.companyTypes = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID;
        ccc.WizardNext();
        
        //Choosing to create an MSO or MC takes us to Step 2... the difference is that ccc.newMSOMC.Type is null for MSO
        system.assertEquals(ccc.WizardStep, 2);
        system.assertEquals(null, ccc.newMSOMC.Type);
        system.assertEquals(Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE, ccc.GetMSOorMCDescription);
        
        //Set some dummy values
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        ccc.sNewMSOMCName = 'TM Provider' + sUqDate;
        ccc.newMSOMC.BillingStreet = '1 Test Pl';
        ccc.newMSOMC.BillingCity = 'Test Town';
        ccc.newMSOMC.BillingState = 'CA';
        ccc.newMSOMC.BillingPostalCode = '90210';
        ccc.sMFirstName = 'TM First Name' + sUqDate;
        ccc.sMLastName = 'TM Last Name' + sUqDate;
        ccc.sMPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
        ccc.newMContact.Phone = '1231231234';
        
        //We've set our values, moving to the next step should save them...
        ccc.WizardNext();
        List<Account> tempMSO = [SELECT Id FROM Account WHERE Name = :ccc.sNewMSOMCName];
        system.assertNotEquals(null, tempMSO.size());
        system.assertNotEquals(0, tempMSO.size());

        //...meaning that we're now at Step 3 of the Wizard
        system.assertEquals(3, ccc.WizardStep);
        
        //Now we decide to choose 'Yes' to the question: Do you want to attach a provider site
        ccc.optYesNoSelected = '1';
        ccc.WizardNext();
        
        system.assertEquals(6, ccc.WizardStep);
        system.assertNotEquals(null, ccc.selectedMSO_Id);
        
        //We're now at step 6, where the Wizard continues with the Create Provider flow, which we've already tested
        //  So assert that we're displaying the 'Provider' title and then we're done with this branch
        system.assertEquals(Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER, ccc.GetMSOorMCDescription);
    }
    
    static testmethod void MSOMCFlowCreateMCWithoutAttachedProvider(){
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        //Opening Step of Wizard
        ccc.companyTypes = Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY;
        ccc.WizardNext();
        
        //Choosing to create an MSO or MC takes us to Step 2... the difference is that ccc.newMSOMC.Type = Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY for MC
        system.assertEquals(ccc.WizardStep, 2);
        system.assertEquals(Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY, ccc.newMSOMC.Type);
        system.assertEquals(Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY, ccc.GetMSOorMCDescription);
        
        //Set some dummy values
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
        ccc.sNewMSOMCName = 'TM MC' + sUqDate;
        ccc.newMSOMC.BillingStreet = '1 Test Pl';
        ccc.newMSOMC.BillingCity = 'Test Town';
        ccc.newMSOMC.BillingState = 'CA';
        ccc.newMSOMC.BillingPostalCode = '90210';
        ccc.sMFirstName = 'TM First Name' + sUqDate;
        ccc.sMLastName = 'TM Last Name' + sUqDate;
        ccc.sMPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
        ccc.newMContact.Phone = '1231231234';
        
        //We've set our values, moving to the next step should save them...
        ccc.WizardNext();
        List<Account> tempMC = [SELECT Id FROM Account WHERE Name = :ccc.sNewMSOMCName];
        system.assertNotEquals(null, tempMC.size());
        system.assertNotEquals(0, tempMC.size());

        //...meaning that we're now at Step 3 of the Wizard
        system.assertEquals(3, ccc.WizardStep);
        
        //Now we decide to choose 'No' to the question: Do you want to attach a provider site
        ccc.optYesNoSelected = '2';
        ccc.WizardNext();
        
        //We should be at the end of the Wizard now!
        system.assertEquals(WizardMax, ccc.WizardStep);
        
        //Note: MSO and MC use the same Wizard flow, so we don't need to test MC with adding a provider 
    }
    
    static testmethod void SundryTests(){
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        ccc.sFindMSOSearchText = null;
        List<Account> tempList = ccc.getdsMSOList();
        system.assertNotEquals(null, tempList);
        system.assertEquals(0, tempList.size());
        
        system.assertEquals(WizardMax, ccc.WizardMax);
    }
    
    static testmethod void ProviderSaveWithInvalidData(){
		PageReference testPr = Page.CreateCompany;
		Test.setCurrentPage(testPr);
		
		CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
                           
     	
     	////TEST PROVIDER CONTACTS
     	ccc.WizardStep = 6; //jump to provider input
     	
     	//Set some dummy values
        Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
                 
        ccc.sNewProviderName = 'TM Provider Attached' + sUqDate;
        ccc.newProvider.BillingStreet = '1 Test Pl';
        ccc.newProvider.BillingCity = 'Test Town';
        ccc.newProvider.BillingState = 'CA';
        ccc.newProvider.BillingPostalCode = '90210';
        ccc.sFirstName = 'TM First Name' + sUqDate;
        ccc.sLastName = 'TM Last Name' + sUqDate;
        ccc.sPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
        ccc.newContact.Phone = '1231231234';
                             
     	ccc.sPersonEmailAddress = 'invalid';  //try to proceed with invalid email
     	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 6);

		Account dupEmail = new Account(firstname='Duplicate', lastname='Email', PersonEmail='duplicate@email.com');
		insert dupEmail;
		     	    
     	ccc.sPersonEmailAddress = dupEmail.PersonEmail; //'gchowdhry@leadingage.org';  //try to proceed with existing email
     	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 6);
     	
     	ccc.sPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
     	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 7); //proceed with valid, unique email
     	 
	   	 //TEST PROVIDER PROGRAM REVENUE
	   	 
	   	//Make Sure Year Submitted is a number
	   	ccc.newProvider.Revenue_Year_Submitted__c = 'abcdc';
	   	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 7); 
     	
     	//Invalid Revenue Year, 
     	ccc.newProvider.Revenue_Year_Submitted__c =  string.valueOf(datetime.now().year() + 10);
	   	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 7); 
     	
     	//Invalid Revenue Year, 
     	ccc.newProvider.Revenue_Year_Submitted__c =  string.valueOf(datetime.now().year() - 10);
	   	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 7); 
     	
     	//Valid Revenue Year
     	ccc.newProvider.Revenue_Year_Submitted__c =  string.valueOf(datetime.now().year());
	   	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, WizardMax); 
     
     
     }
     
     static testmethod void MSOSaveWithInvalidData(){
		PageReference testPr = Page.CreateCompany;
		Test.setCurrentPage(testPr);
		
		CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');        
     
     	//TEST MSO-MC CONTACTS
     	ccc.WizardStep = 2; 
     	
     	Datetime dtNow = Datetime.now();
        String sUqDate = dtNow.format(' (yy mm dd h mm ss S)');
     	
     	ccc.sNewMSOMCName = 'TM MC' + sUqDate;
        ccc.newMSOMC.BillingStreet = '1 Test Pl';
        ccc.newMSOMC.BillingCity = 'Test Town';
        ccc.newMSOMC.BillingState = 'CA';
        ccc.newMSOMC.BillingPostalCode = '90210';
        ccc.sMFirstName = 'TM First Name' + sUqDate;
        ccc.sMLastName = 'TM Last Name' + sUqDate;
                
        ccc.sMPersonEmailAddress = 'invalid';  //try to proceed with invalid email
     	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 2);

		Account dupEmail = new Account(firstname='Duplicate', lastname='Email', PersonEmail='duplicate@email.com');
		insert dupEmail;		     	         
     	ccc.sMPersonEmailAddress = dupEmail.PersonEmail; //'gchowdhry@leadingage.org';  //try to proceed with existing email 
     	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 2);
     	
     	ccc.sMPersonEmailAddress = 'TMEmail' + dtNow.format('yymmddhmmssS') + '@test.com';
     	ccc.WizardNext(); //
     	system.assertEquals(ccc.WizardStep, 3); //proceed with valid, unique email
     
     
     }
     
     
       static testmethod void InvalidTestNoMSOSelected(){
       	
       	//test validation that requires an mso/mc selection if choosing to attach provider to company
        PageReference testPr = Page.CreateCompany;
        Test.setCurrentPage(testPr);
        
        CreateCompany_Controller ccc = new CreateCompany_Controller();
        system.assert(ccc.getcompanyTypesItems().size() > 0, 'No data in Company Types!');
        
        //Opening Step of Wizard
        ccc.companyTypes = Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID;
        ccc.WizardNext();
        
        //Choosing to Add a Provider skips us to Step 4, so let's check that
        system.assertEquals(4, ccc.WizardStep); //4
        
        //Say yes to 'Attach to Parent MSO/MC'
        ccc.optChooseMSO = '1';
        ccc.WizardNext();
        
        //We should be at Step 5
        system.assertEquals(5, ccc.WizardStep); //5
        
        ccc.WizardNext(); // validation kicks in and prevents you from moving to step 6 because an MSO was not seleced
        system.assertEquals(5, ccc.WizardStep); //5
        
        
       }
   
}