public with sharing class ConferenceProposalQuerier {

    public static Conference_Proposal__c getConferenceProposalById(Id conferenceProposalId){
        if (conferenceProposalId == null){
            return null;
        }
        
        List<Conference_Proposal__c> conferenceProposals = getConferenceProposalsByIds(new Set<Id>{ conferenceProposalId });
        
        if (conferenceProposals.size() > 0){
            return conferenceProposals[0];
        }
        
        return null;
    }

    public static List<Conference_Proposal__c> getConferenceProposalsByIds(Set<Id> conferenceProposalIds){
        if (NU.CollectionUtil.setHasElements(conferenceProposalIds) == false){
            return new List<Conference_Proposal__c>();
        }
        
        return [Select id,
                       name,
                       Abstract__c,
                       Completed__c,
                       Conference__c,
                       Conference_Track__c,
                       Counter__c,
                       Date__c,
                       Division__c,
                       Expected_Attendance__c,
                       International__c,
                       Learning_Objective_1__c,
                       Learning_Objective_2__c,
                       Learning_Objective_3__c,
                       Notes__c,
                       Proposal_Number__c,
                       Session__c,
                       Session__r.Name,
                       Session__r.Session_Number__c,
                       Session_Length__c,
                       Sponsorship_Level__c,
                       Submitter__c,
                       Submitter__r.Account__c,
                       Submitter__r.Account__r.NU__FullName__c,
                       Submitter__r.Account__r.PersonTitle,
                       Submitter__r.Account__r.Speaker_Company_1__c,
                       Submitter__r.Company_Name_Rollup__c,
                       Submitter__r.Account__r.Speaker_Full_Address__c,
                       Submitter__r.Account__r.Phone,
                       Submitter__r.Account__r.NU__PersonEmail__c,
                       Submitter__r.Account__r.LeadingAge_ID__c,
                       Status__c,
                       Title__c,
                       (Select id,
                               name,
                               Proposal__c,
                               Speaker__c,
                               Speaker__r.Id,
                               Speaker__r.Account__c
                          from Conference_Proposal_Speakers__r)
                  from Conference_Proposal__c
                 where id in :conferenceProposalIds];
    }
    
    public static List<Conference_Proposal__c> getConferenceProposalsByConferenceId(Id conferenceId){
        return [Select id,
                       name,
                       Abstract__c,
                       Completed__c,
                       Conference__c,
                       Conference_Track__c,
                       Counter__c,
                       Date__c,
                       Division__c,
                       Expected_Attendance__c,
                       International__c,
                       Learning_Objective_1__c,
                       Learning_Objective_2__c,
                       Learning_Objective_3__c,
                       Notes__c,
                       Proposal_Number__c,
                       Session__c,
                       Session__r.Name,
                       Session__r.Session_Number__c,
                       Session__r.Session_Title_Full__c,
                       Session__r.Conference_Track__r.Name,
                       Session__r.Conference_Track__r.Track_Code__c,
                       Session__r.Title__c,
                       Session_Length__c,
                       Sponsorship_Level__c,
                       Submitter__c,
                       Submitter__r.Company_1__c,
                       Submitter__r.Email__c,
                       Submitter__r.Full_Name__c,
                       Submitter__r.Full_Address__c,
                       Submitter__r.Phone__c,
                       Submitter__r.Title__c,
                       Submitter__r.Account__c,
                       Submitter__r.Account__r.NU__FullName__c,
                       Submitter__r.Account__r.PersonTitle,
                       Submitter__r.Account__r.Speaker_Company_1__c,
                       Submitter__r.Company_Name_Rollup__c,
                       Submitter__r.Account__r.Speaker_Full_Address__c,
                       Submitter__r.Account__r.Phone,
                       Submitter__r.Account__r.NU__PersonEmail__c,
                       Submitter__r.Account__r.LeadingAge_ID__c,
                       Status__c,
                       Title__c,
                       (Select id,
                               name,
                               Proposal__c,
                               Speaker_Full_Name__c,
                               Speaker__c,
                               Speaker__r.Title__c,
                               Speaker__r.Company_1__c,
                               Speaker__r.City__c,
                               Speaker__r.State__c,
                               Speaker__r.Country__c,
                               Speaker__r.Account__c,
                               Speaker__r.Account__r.LeadingAge_ID__c
                          from Conference_Proposal_Speakers__r),
                       (Select Field,
                               OldValue,
                               NewValue,
                               CreatedDate,
                               CreatedById
                          from Histories)
                  from Conference_Proposal__c
                 where Conference__c = :conferenceId
                 order by Proposal_Number__c];
    }
    
    public static List<Conference_Proposal__c> getConferenceProposalsByConferenceIdAndSearchCriteria(Id conferenceId, ConferenceProposalSearchCriteria searchCriteria){
        String query = 
               'Select id, '+
                       'name, '+
                       'Abstract__c, '+
                       'Completed__c, '+
                       'Conference__c, '+
                       'Conference_Track__c, '+
                       'Conference_Track__r.Name, '+
                       'Counter__c, '+
                       'Date__c, '+
                       'Division__c, '+
                       'Expected_Attendance__c, '+
                       'International__c, '+
                       'Learning_Objective_1__c, '+
                       'Learning_Objective_2__c, '+
                       'Learning_Objective_3__c, '+
                       'Notes__c, '+
                       'Proposal_Number__c, '+
                       'Session__c, '+
                       'Session__r.Name, ' +
                       'Session__r.Session_Number__c, ' +
                       'Session__r.Session_Title_Full__c, ' +
                       'Session__r.Conference_Track__r.Name, ' +
                       'Session__r.Conference_Track__r.Track_Code__c, ' +
                       'Session__r.Title__c, ' +
                       'Session__r.Status__c, ' +
                       'Session_Length__c, '+
                       'Sponsorship_Level__c, '+
                       'Submitter__c, '+
                       'Submitter__r.Company_1__c, '+
                       'Submitter__r.Email__c, '+
                       'Submitter__r.First_Name__c, '+
                       'Submitter__r.Full_Name__c, '+
                       'Submitter__r.Full_Address__c, '+
                       'Submitter__r.Last_Name__c, '+
                       'Submitter__r.Phone__c, '+
                       'Submitter__r.Title__c, '+
                       'Submitter__r.Account__c, '+
                       'Submitter__r.Account__r.NU__FullName__c, '+
                       'Submitter__r.Account__r.PersonTitle, '+
                       'Submitter__r.Account__r.Speaker_Company_1__c, '+
                       'Submitter__r.Company_Name_Rollup__c, ' +
                       'Submitter__r.Account__r.Speaker_Full_Address__c, '+
                       'Submitter__r.Account__r.Phone, '+
                       'Submitter__r.Account__r.NU__PersonEmail__c, '+
                       'Submitter__r.Account__r.LeadingAge_ID__c, '+
                       'Status__c, '+
                       'Title__c, '+
                       '(Select id, '+
                               'name, '+
                               'Proposal__c, '+
                               'Speaker_Full_Name__c, ' +
                               'Speaker__c, '+
                               'Speaker__r.Title__c, '+
                               'Speaker__r.Company_1__c, '+
                               'Speaker__r.City__c, '+
                               'Speaker__r.State__c, '+
                               'Speaker__r.Country__c, '+
                               'Speaker__r.Account__c, '+
                               'Speaker__r.Account__r.LeadingAge_ID__c '+
                          'from Conference_Proposal_Speakers__r), '+
                       '(Select Field, '+
                               'OldValue, '+
                               'NewValue, '+
                               'CreatedDate, '+
                               'CreatedById '+
                          'from Histories) '+
                  'from Conference_Proposal__c '+
                 'where Conference__c = :conferenceId ';
                 
        if (String.isNotBlank(SearchCriteria.Company)){
            //String company = SearchCriteria.Company;
            query += ' and Submitter__r.Company_Name_Rollup__c LIKE \'%' + String.escapeSingleQuotes(SearchCriteria.Company) +'%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.FirstName)){
            String firstName = SearchCriteria.FirstName;
            query += ' and Submitter__r.First_Name__c LIKE \'' + String.escapeSingleQuotes(firstName) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.LastName)){
            String lastName = SearchCriteria.LastName;
            query += ' and Submitter__r.Last_Name__c LIKE \'%' + String.escapeSingleQuotes(lastName) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.Title)){
            //String title = SearchCriteria.Title;
            query += ' and Title__c LIKE \'%' + String.escapeSingleQuotes(SearchCriteria.Title) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.TrackName)){
            //String trackName = SearchCriteria.TrackName;
            query += ' and Conference_Track__r.Name LIKE \'%'+ String.escapeSingleQuotes(SearchCriteria.TrackName) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.LeadingAgeId)){
            String laId = SearchCriteria.LeadingAgeId;
            query += ' and Submitter__r.Account__r.LeadingAge_Id__c = :laId ';
        }
        
        if (String.isNotBlank(SearchCriteria.SessionNumber)){
            String sid = SearchCriteria.SessionNumber;
            query += ' and Session__r.Session_Number__c = :sid ';
        }
        
        if (String.isNotBlank(SearchCriteria.ProposalNumber)){
            Decimal pid = Decimal.valueOf(SearchCriteria.ProposalNumber);
            query += ' and Proposal_Number__c = :pid ';
        }

        if (String.isNotBlank(SearchCriteria.Status)){
            String stid = SearchCriteria.Status;
            query += ' and Status__c = :stid ';
        }
                
        query += ' order by Submitter__r.First_Name__c, createdDate';
                 
        List<Conference_Proposal__c> proposals = Database.query(query);
        
        if (String.isNotBlank(SearchCriteria.NotesPhrase)){
            Integer proposalsCount = proposals.size();
            
            for (Integer index = proposalsCount - 1; index >= 0; --index){
                Conference_Proposal__c proposal = proposals[index];
                
                if (String.isBlank(proposal.Notes__c) ||
                    proposal.Notes__c.containsIgnoreCase(SearchCriteria.NotesPhrase) == false){
                    proposals.remove(index);
                }
            }
        }
        
        if (String.isNotBlank(SearchCriteria.LearningObjectivesPhrase)){
            Integer proposalsCount = proposals.size();
            
            for (Integer index = proposalsCount - 1; index >= 0; --index){
                Conference_Proposal__c proposal = proposals[index];
                
                if ((String.isBlank(proposal.Learning_Objective_1__c) ||
                    proposal.Learning_Objective_1__c.containsIgnoreCase(SearchCriteria.LearningObjectivesPhrase) == false) &&
                    
                    (String.isBlank(proposal.Learning_Objective_2__c) ||
                    proposal.Learning_Objective_2__c.containsIgnoreCase(SearchCriteria.LearningObjectivesPhrase) == false) &&
                    
                    (String.isBlank(proposal.Learning_Objective_3__c) ||
                    proposal.Learning_Objective_3__c.containsIgnoreCase(SearchCriteria.LearningObjectivesPhrase) == false)){
                        
                    proposals.remove(index);
                }
            }
        }
        
        return proposals;
    }
}