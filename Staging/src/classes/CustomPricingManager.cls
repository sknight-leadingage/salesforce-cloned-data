global without sharing class CustomPricingManager implements NU.IPricingManager {
    private static final String YES_STR = 'yes';
    
    private static List<IProductPricer> productPricers = 
        // Create the pricers in the order in which they should
        // be invoked.
        new List<IProductPricer>{
            new MultiSiteProviderMembershipPricing(),
            new ProviderMembershipPricer(),
            new IAHSAMembershipPricer(),
            new EventSessionPricer(),
            new AdvertisingPricer(),
            new BedCountPricer(),
            
            // The Default Product Pricer should always be last
            new DefaultProductPricer()
        };
        
    private static List<Account> accountQuery(List<Account> accounts) {
        // Don't waste a SOQL query if the list is null.
        if (accounts == null){
            return null;
        }
        
        return [select id,
                NU__Member__c,
                NU__MembershipType__c,
                IAHSA_Member__c,
                IAHSA_Membership__c,
                CAST_Member__c,
                Provider_Member__c,
                Provider_Lapsed_On__c,
                Provider_Member_Thru__c,
                Provider_Membership__c,
                Multi_Site_Dues_Price__c,
                Dues_Price__c,
                Dues_Price_Override__c,
                RecordType.Name,
                RecordTypeId,
                Revenue_Year_Submitted__c,
                Program_Service_Revenue__c,
                Under_Construction__c,
                For_Profit__c,										//Bedcount for the account and for-profit or not
                Number_Of_Assisted_Living_Units__c,					
                Number_of_Independent_Living_Units__c,
                Independent_Living_Beds__c,
                ICF_Beds__c,
                Number_of_Skilled_Nursing_Facility_Beds__c,
                Number_of_Intermedicate_Care_Beds__c,
                Number_Of_Nursing_Beds__c,
                Number_of_Personal_Care_Beds__c,
                NU__PrimaryEntity__r.Id,            				//Entity id, name and bedcount prices
                NU__PrimaryEntity__r.Name,
                NU__PrimaryEntity__r.FP_Assisted_Living_Beds__c,
                NU__PrimaryEntity__r.FP_Independent_Living_Units__c,
                NU__PrimaryEntity__r.FP_Intermedicate_Care_Beds__c,
                NU__PrimaryEntity__r.FP_Nursing_Home_Beds__c,
                NU__PrimaryEntity__r.FP_Personal_Care_Beds__c,
                NU__PrimaryEntity__r.FP_Skilled_Nursing_Facility_Units__c,
                NU__PrimaryEntity__r.FP_ICF_IID_Beds__c,
                NU__PrimaryEntity__r.NP_Assisted_Living_Beds__c,
                NU__PrimaryEntity__r.NP_Independent_Living_Units__c,
                NU__PrimaryEntity__r.NP_Intermedicate_Care_Beds__c,
                NU__PrimaryEntity__r.NP_Nursing_Home_Beds__c,
                NU__PrimaryEntity__r.NP_Personal_Care_Beds__c,
                NU__PrimaryEntity__r.NP_Skilled_Nursing_Facility_Units__c,
                NU__PrimaryEntity__r.NP_ICF_IID_Beds__c				
				
           from Account
          where id IN :accounts];
    }
        
    private static Map<Id,Account> mappedAccounts = null;
    private Account getAccountById(Id accountId){
//      system.debug ('  getAccountById::accountId' + accountId);
//       system.debug ('  getAccountById::mappedAccounts' + mappedAccounts);
//       system.debug ('  getAccountById::mappedAccountsGet' + mappedAccounts.get(accountId));
        // already queried the accounts in bulk?
        if (mappedAccounts != null && mappedAccounts.get(accountId) != null) {
            return mappedAccounts.get(accountId);
        }
        
        List<Account> accounts = accountQuery(new List<Account>{new Account(Id = accountId)});
        
        if (accounts.size() > 0){
            return accounts[0];
        }
        
        return null;
    }
    
    private static List<NU__Membership__c> membershipQuery(List<Account> accounts) {
        system.debug('    membershipQuery::accounts ' + accounts);
        
        // Don't waste a SOQL query if the list is null.
        if (accounts == null){
            return null;
        }
        
        List<NU__Membership__c> memberships = 
        
               [select id,
                       Name,
                       NU__Account__c,
                       NU__Amount__c,
                       NU__ExternalAmount__c,
                       NU__StartDate__c,
                       NU__EndDate__c,
                       Membership_Date__c,
                       NU__MembershipType__c,
                       NU__MembershipType__r.Name,
                       NU__MembershipProductName__c,
                       NU__Status__c,
                       Under_Construction__c
                  from NU__Membership__c
                 where NU__Account__c IN :accounts];
                   //and NU__MembershipType__c = :membershipTypeId];
        
        system.debug('    membershipQuery::memberships ' + memberships);
        
        return memberships;
    }
    
    private static Map<Object,List<SObject>> mappedAccountMemberships = null;
    private List<NU__Membership__c> getAccountMemberships(Id accountId, Id membershipTypeId){
        if (accountId == null ||
            membershipTypeId == null){
            return new List<NU__Membership__c>();
        }
        
        Map<Object,List<SObject>> memberships = new Map<Object,List<SObject>>();
        
        system.debug('    getAccountMemberships::accountId ' + accountId);
        system.debug('    getAccountMemberships::membershipTypeId ' + membershipTypeId);
        system.debug('    getAccountMemberships::memberships ' + memberships);
        system.debug('    getAccountMemberships::mappedAccountMemberships ' + mappedAccountMemberships);
        
        // already queried the account memberships in bulk?
        if (mappedAccountMemberships != null && mappedAccountMemberships.get(accountId) != null) {
            memberships = NU.CollectionUtil.groupSObjectsByField(mappedAccountMemberships.get(accountId),'NU__MembershipType__c');
        }
        else {
            memberships = NU.CollectionUtil.groupSObjectsByField(membershipQuery(new List<Account>{new Account(Id = accountId)}),'NU__MembershipType__c');
        }
        return memberships.get(membershipTypeId);
    }
    
    private NU__Event__c getEventById(Id eventId){
        // Don't waste a SOQL query if the event Id is null.
        if (eventId == null){
            return null;
        }
        
        List<NU__Event__c> events =
        [select id,
                name,
                NU__EarlyRegistrationCutOffDate__c,
                NU__RegularRegistrationCutOffDate__c
           from NU__Event__c
          where id = :eventId];
        
        if (events.size() > 0){
            return events[0];
        }
        
        return null;
    }
    
    private static List<NU__MembershipType__c> membershipTypeQuery(Set<Id> membershipTypeIds) {
        // Don't waste a SOQL query if the set is null.
        if (membershipTypeIds == null){
            return null;
        }
        
        return [select id,
                name,
                (select id,
                        Name,
                        NU__Product__c,
                        Exclude_Provider_Service_Fee__c,
                        Provider_Service_Fee__c,
                        State_Dues__c,
                        National_Dues__c,
                        NU__Purpose__c
                   from NU__MembershipTypeProductLinks__r
                  where NU__Purpose__c = :Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY
                  or NU__Purpose__c = :Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_OPTIONAL
                     or Provider_Service_Fee__c = true)
           from NU__MembershipType__c
          where id IN :membershipTypeIds];
    }
    
    private static Map<Id,NU__MembershipType__c> mappedMembershipTypes = null;
    private NU__MembershipType__c getMembershipType(Id membershipTypeId){
        // already queried the membership types in bulk?
        if (mappedMembershipTypes != null && mappedMembershipTypes.get(membershipTypeId) != null) {
            return mappedMembershipTypes.get(membershipTypeId);
        }
        
        List<NU__MembershipType__c> membershipTypes = membershipTypeQuery(new Set<Id>{membershipTypeId});
        
        if (membershipTypes.size() > 0){
            return membershipTypes[0];
        }
        
        return null;
    }
    
    private static List<NU__Product__c> productQuery(Set<Id> productIds) {
        // Don't waste a SOQL query if the membership Type Id is null.
        if (productIds == null){
            return null;
        }
        
        return [select id,
                     Name,
                     RecordType.Name,
                     NU__Entity__c,
                     NU__Entity__r.Name,
                     NU__Event__c,
                     NU__Event__r.Name,
                     NU__Event__r.NU__ShortName__c,
                     NU__EventSessionEndDate__c,
                     NU__EventSessionGroup__c,
                     NU__EventSessionGroup__r.Name,
                     NU__EventSessionSpecialVenueInstructions__c,
                     NU__EventSessionStartDate__c,
                     NU__IsEventBadge__c,
                     NU__IsFee__c,
                     NU__IsShippable__c,
                     NU__IsTaxable__c,
                     NU__ListPrice__c,
                     NU__ShortName__c,
                     NU__QuantityMax__c,
                     NU__Status__c,
                     (select NU__PriceClasses__c,
                             NU__DefaultPrice__c,
                             NU__EarlyPrice__c,
                             NU__LatePrice__c,
                             Name
                        from NU__SpecialPrices__r
                       //where NU__PriceClasses__c includes ( :productPricingRequest.PriceClassName )
                     )
                from NU__Product__c
                where id in :productIds];
    }
    
    private static Map<Id,NU__Product__c> mappedProducts = null;
    private List<NU__Product__c> getProductsFromPricingRequest(NU.ProductPricingRequest productPricingRequest){
        List<NU__Product__c> products = new List<NU__Product__c>();
        Set<Id> productIds = new Set<Id>();

        for (NU.ProductPricingInfo ppi : productPricingRequest.productPricingInfos){
            // already queried the product in bulk?
            if (mappedProducts != null && mappedProducts.get(ppi.ProductId) != null) {
                products.add(mappedProducts.get(ppi.ProductId));
                continue;
            }
            productIds.add(ppi.ProductId);
        }
        
        if (productIds.size() > 0) {
            products.addAll(productQuery(productIds));
        }
        return products;
    }
    
    private NU__SpecialPrice__c findSpecialPrice(List<NU__SpecialPrice__c> specialPrices, String priceClassName){
        
        if (String.isBlank(priceClassName) ||
            NU.CollectionUtil.listHasElements(specialPrices) == false){
            return null;
        }

        for (NU__SpecialPrice__c price : specialPrices) {
            List<string> priceClasses = price.NU__PriceClasses__c.split(NU.Constant.PICKLIST_DELIMITER);

            for (string priceClass : priceClasses) {
                if (priceClass == priceClassName){
                    return price;
                }
            }
        }

        return null;
    }
    
    private Boolean isMember(Account acct){
        return acct != null &&
               (acct.NU__Member__c == YES_STR ||
                acct.IAHSA_Member__c == YES_STR ||
                acct.CAST_Member__c == YES_STR ||

                acct.Provider_Member__c == YES_STR);
    }
    
    private Boolean isEmptyProductPricingRequest(NU.ProductPricingRequest productPricingRequest){
        return productPricingRequest == null ||
               NU.CollectionUtil.listHasElements(productPricingRequest.productPricingInfos) == false;
    }
    
    public static void PopulateAccounts(List<Account> accounts) {
        if (mappedAccounts == null && NU.CollectionUtil.ListHasElements(accounts)) {
            mappedAccounts = new Map<Id,Account>(accountQuery(accounts));
        }
    }
    
    public static void PopulateMembershipTypes(Set<Id> membershipTypeIds) {
        if (mappedMembershipTypes == null && NU.CollectionUtil.SetHasElements(membershipTypeIds)) {
            mappedMembershipTypes = new Map<Id,NU__MembershipType__c>(membershipTypeQuery(membershipTypeIds));
        }
    }
    
    public static void PopulateAccountMemberships(List<Account> accounts) {
        if (mappedAccountMemberships == null && NU.CollectionUtil.ListHasElements(accounts)) {
            mappedAccountMemberships = NU.CollectionUtil.groupSObjectsByField(membershipQuery(accounts),'NU__Account__c');
        }
    }
    
    public static void PopulateProducts(Set<Id> productIds) {
        if (mappedProducts == null && NU.CollectionUtil.SetHasElements(productIds)) {
            mappedProducts = new Map<Id,NU__Product__c>(productQuery(productIds));
        }
    }
    
    public static void emptyAccountAndAccountMembershipCaches(){
        mappedAccountMemberships = null;
        mappedAccounts = null;
    }
    
    global String GetPriceClass(NU.PriceClassRequest priceClassRequest) {
        if(priceClassRequest != null &&
           priceClassRequest.AccountId != null){

            Account acct = getAccountById(priceClassRequest.AccountId);

            if (isMember(acct)){
                return NU.Constant.PRICE_CLASS_MEMBER;
            }
        }

        return NU.Constant.PRICE_CLASS_DEFAULT;
    }
    
    global Map<Id, Decimal> GetProductPrices(NU.ProductPricingRequest productPricingRequest){
    	return GetProductPrices(productPricingRequest, false);
    }
    
    global Map<Id, Decimal> GetProductPrices(NU.ProductPricingRequest productPricingRequest, boolean IsFutureCalc){
                        
        Map<Id, Decimal> productPrices = new Map<Id, Decimal>();
        
        if (isEmptyProductPricingRequest(productPricingRequest)){
            return productPrices;
        }

        List<NU__Product__c> prods = getProductsFromPricingRequest(productPricingRequest);
        NU__Event__c event = getEventById(productPricingRequest.eventId);
        Account customer = getAccountById(productPricingRequest.AccountId);
        NU__MembershipType__c membershipType= getMembershipType(productPricingRequest.MembershipTypeId);
        List<NU__Membership__c> accountMemberships = getAccountMemberships(productPricingRequest.AccountId, productPricingRequest.MembershipTypeId);
        
        NU__MembershipType__c jointProviderMT = MembershipTypeQuerier.getJointStateProviderMembershipType();
        
        List<NU__Membership__c> accountProviderMemberships = null;
        
        if (jointProviderMT != null){
            accountProviderMemberships = getAccountMemberships(productPricingRequest.AccountId, jointProviderMT.Id);
        }
        
        Boolean isEventPrice = event != null && productPricingRequest.TransactionDate != null;
   
        Map<String, object> extraPricingParams = new Map<String, Object>{ 
            'MembershipType' => MembershipType,
            'Event' => event,
            'AccountMemberships' => accountMemberships,
            'AccountProviderMemberships' => accountProviderMemberships,
            'FutureCalculationRequested' => IsFutureCalc
        };

        for (NU__Product__c prod : prods) {
                 
            NU__SpecialPrice__c specialPrice = 
                findSpecialPrice(prod.NU__SpecialPrices__r, productPricingRequest.PriceClassName);

            for (IProductPricer productPricer : productPricers){
                Decimal price = productPricer.calculateProductPrice(prod, customer, specialPrice, productPricingRequest, extraPricingParams);
                
                if (price != null){
                    productPrices.put(prod.Id, price);
                    break;
                }
            }
        }

        return productPrices;
    }
}