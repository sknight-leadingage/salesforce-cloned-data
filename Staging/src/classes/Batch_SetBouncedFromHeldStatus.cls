global class Batch_SetBouncedFromHeldStatus implements Database.Batchable<SObject> {
   global final string bMessage;

   global Batch_SetBouncedFromHeldStatus() {
        List<NU__Configuration__c> c = [SELECT Id, NU__Value__c FROM NU__Configuration__c WHERE Name = 'LyrisBounceMessage' LIMIT 1];
        if (c.size() > 0) {
            bMessage = c[0].NU__Value__c;
        }
        else {
            bMessage = 'Please verify that this email address is valid.';
        }
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT Account__r.Id FROM Online_Subscription__c WHERE Status__c = 'Held' AND Account__r.PersonEmailBouncedDate = null AND Account__r.PersonEmailBouncedReason = null ORDER BY Account__r.Id]);
   }

   global void execute(Database.BatchableContext BC, List<SObject> batch){
      Set<Id> uq = new Set<Id>();
      for(SObject s : batch){
         Online_Subscription__c so = (Online_Subscription__c)s;
         uq.add(so.Account__r.Id);
      }

      List<Account> sa = [SELECT Id, PersonEmailBouncedDate, PersonEmailBouncedReason FROM Account WHERE Id IN :uq];

      for(Account aq: sa) {
         aq.PersonEmailBouncedDate = datetime.now();
         aq.PersonEmailBouncedReason = bMessage;
      }

      update sa;
   }

   global void finish(Database.BatchableContext BC){

   }

}