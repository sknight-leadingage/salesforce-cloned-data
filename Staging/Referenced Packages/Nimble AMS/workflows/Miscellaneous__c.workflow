<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MiscellaneousCustomerEmailPopulation</fullName>
        <field>CustomerEmail__c</field>
        <formula>Account2__r.PersonEmail__c</formula>
        <name>Miscellaneous Customer Email Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MiscellaneousSearchAccountPopulation</fullName>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <field>Search__c</field>
        <formula>IF(ISBLANK( Account2__r.Name )=False, 
Account2__r.Name, 
Account2__r.PersonContact__r.FirstName &amp; &apos; &apos; &amp; 
Account2__r.PersonContact__r.LastName )</formula>
        <name>Miscellaneous Search Account Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Miscellaneous Customer Email Population</fullName>
        <actions>
            <name>MiscellaneousCustomerEmailPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!ISBLANK(Account2__r.PersonContact__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Miscellaneous Search Account Population</fullName>
        <actions>
            <name>MiscellaneousSearchAccountPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Miscellaneous__c.Search__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
