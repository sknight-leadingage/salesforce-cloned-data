<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MembershipCustomerEmailPopulation</fullName>
        <field>CustomerEmail__c</field>
        <formula>Account__r.PersonEmail__c</formula>
        <name>Membership Customer Email Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MembershipSearchAccountPopulation</fullName>
        <field>Search__c</field>
        <formula>IF(ISBLANK( Account__r.Name )=False,
        Account__r.Name,
           Account__r.PersonContact__r.FirstName &amp; &apos; &apos; &amp;
            Account__r.PersonContact__r.LastName )</formula>
        <name>Membership Search Account Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Membership Customer Email Population</fullName>
        <actions>
            <name>MembershipCustomerEmailPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!ISBLANK(Account__r.PersonContact__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Membership Search Account Population</fullName>
        <actions>
            <name>MembershipSearchAccountPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Membership__c.Search__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
