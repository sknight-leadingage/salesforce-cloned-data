<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CouponCustomerEmailPopulation</fullName>
        <field>CustomerEmail__c</field>
        <formula>Account__r.PersonEmail__c</formula>
        <name>Coupon Customer Email Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CouponSearchAccountPopulation</fullName>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <field>Search__c</field>
        <formula>IF(ISBLANK( Account__r.Name )=False, 
Account__r.Name, 
Account__r.PersonContact__r.FirstName &amp; &apos; &apos; &amp; 
Account__r.PersonContact__r.LastName )</formula>
        <name>Coupon Search Account Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Coupon Customer Email Population</fullName>
        <actions>
            <name>CouponCustomerEmailPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!ISBLANK(Account__r.PersonContact__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Coupon Search Account Population</fullName>
        <actions>
            <name>CouponSearchAccountPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Coupon__c.Search__c</field>
            <operation>equals</operation>
            <value>null</value>
        </criteriaItems>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
