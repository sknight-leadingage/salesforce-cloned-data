<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OrderSearchAccountPopulation</fullName>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <field>Search__c</field>
        <formula>IF(ISBLANK(BillTo__r.Name )=False, 
BillTo__r.Name, 
BillTo__r.PersonContact__r.FirstName &amp; &apos; &apos; &amp; 
BillTo__r.PersonContact__r.LastName )</formula>
        <name>Order Search Account Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetDefaultInvoiceNumber</fullName>
        <field>InvoiceNumber__c</field>
        <formula>RIGHT(Name, LEN(Name) - FIND(&apos; &apos;, Name))</formula>
        <name>Set Default Invoice Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order Search Account Population</fullName>
        <actions>
            <name>OrderSearchAccountPopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.Search__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Populates the Search__c field with the account name so that the record is available in the Global Search</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Default Invoice Number</fullName>
        <actions>
            <name>SetDefaultInvoiceNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.InvoiceNumber__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
