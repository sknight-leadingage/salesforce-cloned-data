<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CartItemLineIsShippablePopulation</fullName>
        <field>IsShippable__c</field>
        <literalValue>1</literalValue>
        <name>Cart Item Line Is Shippable Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CartItemLineIsTaxablePopulation</fullName>
        <field>IsTaxable__c</field>
        <literalValue>1</literalValue>
        <name>Cart Item Line Is Taxable Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsCouponCartItemLinePopulation</fullName>
        <field>IsCouponCartItemLine__c</field>
        <literalValue>1</literalValue>
        <name>Is Coupon Cart Item Line Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cart Item Line Is Coupon Cart Item Line Population</fullName>
        <actions>
            <name>IsCouponCartItemLinePopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cart Item Line Is Shippable Population</fullName>
        <actions>
            <name>CartItemLineIsShippablePopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cart Item Line Is Taxable Population</fullName>
        <actions>
            <name>CartItemLineIsTaxablePopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
