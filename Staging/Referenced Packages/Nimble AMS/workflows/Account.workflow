<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PasswordChanged</fullName>
        <description>Password Changed</description>
        <protected>false</protected>
        <recipients>
            <field>PersonEmail__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NimbleAMSEmailTemplates/NimbleAMSPasswordChanged</template>
    </alerts>
    <rules>
        <fullName>Password Changed</fullName>
        <actions>
            <name>PasswordChanged</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>DEPRECATED</description>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
