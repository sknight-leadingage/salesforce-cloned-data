<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Committee__c</defaultLandingTab>
    <description>Manage your committees and committee members</description>
    <label>Committees</label>
    <logo>NimbleAMS/LeadingAge_Logo.gif</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>Committee__c</tab>
    <tab>CommitteeMembership__c</tab>
    <tab>CommitteePosition__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Idea</tab>
    <tab>Awards__c</tab>
</CustomApplication>
