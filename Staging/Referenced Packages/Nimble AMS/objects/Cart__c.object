<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>CartRedirector</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>CartRedirector</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpOrders</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Order Container object for staff transactions.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>AdjustmentEntity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Temporary field used for order adjustments. Can be different than the Entity if Entity Crossover is enabled.</description>
        <externalId>false</externalId>
        <inlineHelpText>Temporary field used for order adjustments. Can be different than the Entity if Entity Crossover is enabled.</inlineHelpText>
        <label>Adjustment Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Adjustment Carts</relationshipLabel>
        <relationshipName>AdjustmentCarts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Balance__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount still owed or to refund for this cart.</description>
        <externalId>false</externalId>
        <formula>Total__c -  TotalPayment__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The amount still owed or to refund for this cart.</inlineHelpText>
        <label>Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Batch__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The batch object this cart is associated with.</description>
        <externalId>false</externalId>
        <label>Batch</label>
        <referenceTo>Batch__c</referenceTo>
        <relationshipLabel>Carts</relationshipLabel>
        <relationshipName>Carts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>BillTo__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The account who will be billed for this purchase.</description>
        <externalId>false</externalId>
        <inlineHelpText>The account who will be billed for this purchase.</inlineHelpText>
        <label>Bill To</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Carts</relationshipLabel>
        <relationshipName>Carts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Data__c</fullName>
        <deprecated>false</deprecated>
        <description>Placeholder for serialized order data.</description>
        <externalId>false</externalId>
        <label>Data</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Entity2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The entity supplying the products and services.</description>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Carts</relationshipLabel>
        <relationshipName>Carts2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Carts</relationshipLabel>
        <relationshipName>Carts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalId__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Identifier__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>Denotes the Id of the Account this cart is the primary cart for.</description>
        <externalId>false</externalId>
        <inlineHelpText>Denotes the Id of the Account this cart is the primary cart for.</inlineHelpText>
        <label>Identifier</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>InvoiceDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The creation date of the invoice (if any) associated with this cart.</description>
        <externalId>false</externalId>
        <label>Invoice Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>InvoiceDescription__c</fullName>
        <deprecated>false</deprecated>
        <description>A description of what this cart&apos;s invoice contains (if an invoice is needed).</description>
        <externalId>false</externalId>
        <label>Invoice Description</label>
        <length>2000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>InvoiceEmail__c</fullName>
        <deprecated>false</deprecated>
        <description>The email address to which the invoice should be sent in addition to the &quot;Bill To&apos;s&quot; email address.</description>
        <externalId>false</externalId>
        <inlineHelpText>The email address to which the invoice should be sent in addition to the &quot;Bill To&apos;s&quot; email address.</inlineHelpText>
        <label>Invoice Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InvoiceNumber__c</fullName>
        <deprecated>false</deprecated>
        <description>The identifying number of the invoice associated with this cart.</description>
        <externalId>false</externalId>
        <label>Invoice Number</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InvoiceTerm__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of days added to the Invoice Date to calculate the Invoice Due Date.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of days added to the Invoice Date to calculate the Invoice Due Date.</inlineHelpText>
        <label>Invoice Term</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>A lookup of the order (if any) associated with this cart.</description>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>Order__c</referenceTo>
        <relationshipLabel>Cart</relationshipLabel>
        <relationshipName>Cart</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PurchaseOrderNumber__c</fullName>
        <deprecated>false</deprecated>
        <description>The purchase order number associated with this cart.</description>
        <externalId>false</externalId>
        <label>Purchase Order Number</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Purpose__c</fullName>
        <deprecated>false</deprecated>
        <description>The purpose of this cart. Determined by the record type of the first cart item.</description>
        <externalId>false</externalId>
        <inlineHelpText>The purpose of this cart.</inlineHelpText>
        <label>Purpose</label>
        <picklist>
            <picklistValues>
                <fullName>Donation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Membership</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Merchandise</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Miscellaneous</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Registration</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Subscription</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ShippingCartItemCount__c</fullName>
        <deprecated>false</deprecated>
        <description>Number of Cart Items that have shipping</description>
        <externalId>false</externalId>
        <label>Shipping Cart Item Count</label>
        <summarizedField>CartItem__c.ShippingItemLineCount__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>SubTotal__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of the cart orders&apos; sub totals excluding shipping and tax.</description>
        <externalId>false</externalId>
        <inlineHelpText>The sum of the cart orders&apos; sub totals excluding shipping and tax.</inlineHelpText>
        <label>Sub Total</label>
        <summarizedField>CartItem__c.SubTotal__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TaxableCartItemCount__c</fullName>
        <deprecated>false</deprecated>
        <description>Number of Taxable Cart Items</description>
        <externalId>false</externalId>
        <label>Taxable Cart Item Count</label>
        <summarizedField>CartItem__c.TaxableItemLineCount__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalDiscounts__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of all discounts for cart items associated with this cart.</description>
        <externalId>false</externalId>
        <label>Total Discounts</label>
        <summarizedField>CartItem__c.Discounts__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalPayment__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of the cart items&apos; refunds and payments.</description>
        <externalId>false</externalId>
        <inlineHelpText>The sum of the cart items&apos; refunds and payments.</inlineHelpText>
        <label>Total Payment</label>
        <summarizedField>CartItem__c.TotalPayment__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalShippingAndTax__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of all shipping and all taxes due for the contents of this cart.</description>
        <externalId>false</externalId>
        <formula>TotalShipping__c + TotalTax__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Shipping And Tax</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalShipping__c</fullName>
        <deprecated>false</deprecated>
        <description>The total value of shipping for all cart items associated with this cart.</description>
        <externalId>false</externalId>
        <label>Total Shipping</label>
        <summarizedField>CartItem__c.TotalShipping__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTax__c</fullName>
        <deprecated>false</deprecated>
        <description>A roll-up of taxes applied to cart items associated with this cart.</description>
        <externalId>false</externalId>
        <label>Total Tax</label>
        <summarizedField>CartItem__c.TotalTax__c</summarizedField>
        <summaryForeignKey>CartItem__c.Cart__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Total__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of all items, discounts, shipping values, and taxes associated with these carts.</description>
        <externalId>false</externalId>
        <formula>SubTotal__c + TotalDiscounts__c + TotalShipping__c + TotalTax__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TransactionDate__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <deprecated>false</deprecated>
        <description>The date this cart was created.</description>
        <externalId>false</externalId>
        <label>Transaction Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Cart</label>
    <listViews>
        <fullName>NU_AllCarts</fullName>
        <columns>NAME</columns>
        <columns>BillTo__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>Total__c</columns>
        <columns>TotalPayment__c</columns>
        <columns>Balance__c</columns>
        <columns>Batch__c</columns>
        <columns>Order__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All Carts</label>
    </listViews>
    <listViews>
        <fullName>NU_LastWeeksCarts</fullName>
        <columns>NAME</columns>
        <columns>BillTo__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>Total__c</columns>
        <columns>TotalPayment__c</columns>
        <columns>Balance__c</columns>
        <columns>Batch__c</columns>
        <columns>Order__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TransactionDate__c</field>
            <operation>equals</operation>
            <value>LAST_WEEK</value>
        </filters>
        <label>Last Week&apos;s Carts</label>
    </listViews>
    <listViews>
        <fullName>NU_ThisWeeksCarts</fullName>
        <columns>NAME</columns>
        <columns>BillTo__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>Total__c</columns>
        <columns>TotalPayment__c</columns>
        <columns>Balance__c</columns>
        <columns>Batch__c</columns>
        <columns>Order__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TransactionDate__c</field>
            <operation>equals</operation>
            <value>THIS_WEEK</value>
        </filters>
        <label>This Week&apos;s Carts</label>
    </listViews>
    <listViews>
        <fullName>NU_TodaysCarts</fullName>
        <columns>NAME</columns>
        <columns>BillTo__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>Total__c</columns>
        <columns>TotalPayment__c</columns>
        <columns>Balance__c</columns>
        <columns>Batch__c</columns>
        <columns>Order__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TransactionDate__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>Today&apos;s Carts</label>
    </listViews>
    <nameField>
        <displayFormat>Cart {0000000}</displayFormat>
        <label>Cart Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Cart</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>BillTo__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TransactionDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalPayment__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Balance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Batch__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Order__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>BillTo__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TransactionDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Total__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalPayment__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Balance__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Batch__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Order__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BillTo__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TransactionDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Total__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TotalPayment__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Balance__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Order__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Batch__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>BillTo__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TransactionDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Total__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TotalPayment__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Balance__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Batch__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Order__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>BillToRequired</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(BillTo__c)</errorConditionFormula>
        <errorDisplayField>BillTo__c</errorDisplayField>
        <errorMessage>The Bill To is required.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>New</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>New (DEPRECATED)</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/{!$Setup.Namespace__c.Prefix__c}Order</url>
    </webLinks>
</CustomObject>
