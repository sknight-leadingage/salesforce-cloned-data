<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Specifies the schedule used to recognize deferred revenue.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>DeferredAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The amount to be deferred.</inlineHelpText>
        <label>Deferred Amount</label>
        <precision>11</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DeferredBalance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>DeferredAmount__c -  RecognizedAmount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The amount of deferred revenue left to be recognized.</inlineHelpText>
        <label>Deferred Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DeferredRevenueGLAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>HYPERLINK(&apos;/&apos; +  DeferredRevenueMethod__r.DeferredRevenueGLAccount__c  , DeferredRevenueMethod__r.DeferredRevenueGLAccount__r.Name)</formula>
        <inlineHelpText>The GL Account used to defer the revenue.</inlineHelpText>
        <label>Deferred Revenue GL Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DeferredRevenueMethod__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The method used to recognize this deferred revenue.</inlineHelpText>
        <label>Deferred Revenue Method</label>
        <referenceTo>DeferredRevenueMethod__c</referenceTo>
        <relationshipLabel>Deferred Schedules</relationshipLabel>
        <relationshipName>DeferredSchedules</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>DeferredTerm__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The original count of how many times the deferred revenue has to be recognized.</inlineHelpText>
        <label>Deferred Term</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>NextRecognitionDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date of the next deferred revenue recognition.</inlineHelpText>
        <label>Next Recognition Date</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RecognitionAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Round( Floor(DeferredAmount__c / DeferredTerm__c * 100) / 100, 2 )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This assumes a straight-line deferred revenue recognition method and is calculated as Deferred Balance divided by Remaining Recognition Count.</inlineHelpText>
        <label>Recognition Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RecognitionHealth__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( OR( (RecognizedAmount__c != Round( (DeferredTerm__c-RemainingRecognitionCount__c)* RecognitionAmount__c, 2) &amp;&amp; RemainingRecognitionCount__c &gt; 0),
        (RecognizedAmount__c != DeferredAmount__c &amp;&amp; RemainingRecognitionCount__c = 0)
      ),

    IMAGE(&apos;/resource/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusIcons/ERROR.png&apos;,&apos;Status&apos;),
    IMAGE(&apos;/resource/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusIcons/OK.png&apos;,&apos;Status&apos;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Monitors the health of the Deferred Schedule and denotes if there are problems. A schedule can become unhealthy when adjustments are made to any of its items. Recognition corrections are automatically applied nightly. 

Green = Healthy
Red = Unhealthy</inlineHelpText>
        <label>Recognition Health</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RecognizedAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The amount of deferred revenue recognized so far.</inlineHelpText>
        <label>Recognized Amount</label>
        <precision>11</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>RecognizedRevenueGLAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The GL Account to which deferred revenue is recognized.</inlineHelpText>
        <label>Recognized Revenue GL Account</label>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Deferred Schedules</relationshipLabel>
        <relationshipName>DeferredSchedules</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>RemainingRecognitionCount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The number of deferred recognitions remaining.</inlineHelpText>
        <label>Remaining Recognition Count</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>StartDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The date on which deferred revenue will start being recognized.</inlineHelpText>
        <label>Start Date</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StatusFlag__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(!ISBLANK(Status__c), IMAGE(&apos;/resource/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusIcons/&apos; + Status__c + &apos;.png&apos;, &apos;Status&apos;), &quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Blue = Future - Start Date is in the future
Green = Current - Revenue is being recognized
Gray = Past - All revenue has been recognized</inlineHelpText>
        <label>Status Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(StartDate__c &gt; TODAY(), &apos;Future&apos;,
    IF(StartDate__c &lt;= TODAY(), IF(RemainingRecognitionCount__c &gt; 0 &amp;&amp; DeferredBalance__c &gt; 0, &apos;Current&apos;, &apos;Past&apos;), &apos;&apos;)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Deferred Schedule</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>DeferredAmount__c</columns>
        <columns>DeferredBalance__c</columns>
        <columns>DeferredRevenueGLAccount__c</columns>
        <columns>DeferredRevenueMethod__c</columns>
        <columns>StartDate__c</columns>
        <columns>NextRecognitionDate__c</columns>
        <columns>DeferredTerm__c</columns>
        <columns>RecognitionAmount__c</columns>
        <columns>RecognizedAmount__c</columns>
        <columns>RecognizedRevenueGLAccount__c</columns>
        <columns>RemainingRecognitionCount__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_Deferred_Schedules</fullName>
        <columns>NAME</columns>
        <columns>DeferredRevenueMethod__c</columns>
        <columns>DeferredTerm__c</columns>
        <columns>RemainingRecognitionCount__c</columns>
        <columns>StartDate__c</columns>
        <columns>NextRecognitionDate__c</columns>
        <columns>RecognitionAmount__c</columns>
        <columns>DeferredAmount__c</columns>
        <columns>RecognizedAmount__c</columns>
        <columns>DeferredBalance__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Deferred Schedules</label>
    </listViews>
    <listViews>
        <fullName>All_Deferred_Schedules_With_Balance</fullName>
        <columns>NAME</columns>
        <columns>DeferredRevenueMethod__c</columns>
        <columns>DeferredTerm__c</columns>
        <columns>RemainingRecognitionCount__c</columns>
        <columns>StartDate__c</columns>
        <columns>NextRecognitionDate__c</columns>
        <columns>RecognitionAmount__c</columns>
        <columns>DeferredAmount__c</columns>
        <columns>RecognizedAmount__c</columns>
        <columns>DeferredBalance__c</columns>
        <columns>RecognitionHealth__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>DeferredBalance__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </filters>
        <label>All Deferred Schedules - With Balance</label>
    </listViews>
    <listViews>
        <fullName>All_Deferred_Schedules_Zero_Balance</fullName>
        <columns>NAME</columns>
        <columns>DeferredRevenueMethod__c</columns>
        <columns>DeferredTerm__c</columns>
        <columns>RemainingRecognitionCount__c</columns>
        <columns>StartDate__c</columns>
        <columns>RecognitionAmount__c</columns>
        <columns>DeferredAmount__c</columns>
        <columns>RecognizedAmount__c</columns>
        <columns>DeferredBalance__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>DeferredBalance__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>All Deferred Schedules - Zero Balance</label>
    </listViews>
    <nameField>
        <displayFormat>Deferred Schedule {0000000}</displayFormat>
        <label>Deferred Schedule Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Deferred Schedules</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>DeferredRevenueMethod__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DeferredTerm__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RemainingRecognitionCount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StartDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RecognitionAmount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DeferredAmount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RecognizedAmount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DeferredBalance__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>DeferredRevenueMethod__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DeferredTerm__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RemainingRecognitionCount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StartDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RecognitionAmount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DeferredAmount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RecognizedAmount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DeferredBalance__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DeferredRevenueMethod__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DeferredTerm__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RemainingRecognitionCount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StartDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RecognitionAmount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DeferredAmount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RecognizedAmount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DeferredBalance__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>DeferredRevenueMethod__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DeferredTerm__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RemainingRecognitionCount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StartDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RecognitionAmount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DeferredAmount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RecognizedAmount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DeferredBalance__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>DeferredTermCannotBeLessThanOne</fullName>
        <active>true</active>
        <description>The Deferred Term cannot be less than 1.</description>
        <errorConditionFormula>DeferredTerm__c &lt; 1</errorConditionFormula>
        <errorDisplayField>DeferredTerm__c</errorDisplayField>
        <errorMessage>The Deferred Term cannot be less than 1.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RecognitionCountCannotBeNegative</fullName>
        <active>true</active>
        <description>The Remaining Recognition Count cannot be negative.</description>
        <errorConditionFormula>RemainingRecognitionCount__c &lt; 0</errorConditionFormula>
        <errorDisplayField>RemainingRecognitionCount__c</errorDisplayField>
        <errorMessage>The Remaining Recognition Count cannot be negative.</errorMessage>
    </validationRules>
</CustomObject>
