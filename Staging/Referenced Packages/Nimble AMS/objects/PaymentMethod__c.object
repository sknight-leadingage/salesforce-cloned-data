<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Payment Method records are used to maintain a system-wide list of available payment methods as they relate to Orders.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IsDeletable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>When set to true, payments can be deleted after submitting.</description>
        <externalId>false</externalId>
        <inlineHelpText>Controls whether or not an already submitted payment is deletable when the batch is still open.</inlineHelpText>
        <label>Is Deletable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsPayment__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Signifies whether or not payment is accepted and payment lines created.</description>
        <externalId>false</externalId>
        <inlineHelpText>Signifies whether or not payment is accepted and payment lines created.</inlineHelpText>
        <label>Is Payment</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Payment Method</label>
    <listViews>
        <fullName>All_Payment_Methods</fullName>
        <columns>NAME</columns>
        <columns>IsPayment__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Payment Methods</label>
    </listViews>
    <listViews>
        <fullName>All_Receipt</fullName>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All Receipt</label>
    </listViews>
    <listViews>
        <fullName>All_Refund</fullName>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All Refund</label>
    </listViews>
    <nameField>
        <label>Payment Method Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Payment Methods</pluralLabel>
    <recordTypeTrackHistory>true</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Cash</fullName>
        <active>true</active>
        <label>Cash</label>
    </recordTypes>
    <recordTypes>
        <fullName>Check</fullName>
        <active>true</active>
        <label>Check</label>
    </recordTypes>
    <recordTypes>
        <fullName>Credit</fullName>
        <active>true</active>
        <label>Credit</label>
    </recordTypes>
    <recordTypes>
        <fullName>CreditCard</fullName>
        <active>true</active>
        <label>Credit Card</label>
    </recordTypes>
    <recordTypes>
        <fullName>Transfer</fullName>
        <active>true</active>
        <label>Transfer</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <customTabListAdditionalFields>IsPayment__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>LAST_UPDATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>RECORDTYPE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>IsPayment__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UPDATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_UPDATE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RECORDTYPE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>IsPayment__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UPDATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>LAST_UPDATE</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchResultsAdditionalFields>RECORDTYPE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>IsPayment__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>UPDATEDBY_USER</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>LAST_UPDATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>IsDeletableSelectedForCreditCard</fullName>
        <active>true</active>
        <description>Is Deletable can&apos;t be checked when the Record Type is Credit Card</description>
        <errorConditionFormula>AND ( RecordType.Name = &apos;Credit Card&apos;,  IsDeletable__c = TRUE)</errorConditionFormula>
        <errorMessage>Credit Card payments can&apos;t be deletable</errorMessage>
    </validationRules>
</CustomObject>
