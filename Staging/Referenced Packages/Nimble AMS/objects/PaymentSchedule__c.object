<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Defines the date interval in which recurring payments can be made.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>ExternalId__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IntervalAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>If Interval Unit is Months, value must be between 1 and 12.
If Interval Unit is Days, value must be between 7 and 365.</description>
        <externalId>false</externalId>
        <inlineHelpText>If Interval Unit is Months, value must be between 1 and 12.
If Interval Unit is Days, value must be between 7 and 365.</inlineHelpText>
        <label>Interval Amount</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IntervalUnit__c</fullName>
        <deprecated>false</deprecated>
        <description>The time unit for the interval. For example, &quot;Months&quot; or &quot;Days&quot;.</description>
        <externalId>false</externalId>
        <inlineHelpText>The time unit for the interval. For example, &quot;Months&quot; or &quot;Days&quot;.</inlineHelpText>
        <label>Interval Unit</label>
        <picklist>
            <picklistValues>
                <fullName>Months</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Days</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Payment Schedule</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>IntervalAmount__c</columns>
        <columns>IntervalUnit__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Payment Schedules</label>
    </listViews>
    <listViews>
        <fullName>NewPaymentSchedulesLastWeek</fullName>
        <columns>NAME</columns>
        <columns>IntervalAmount__c</columns>
        <columns>IntervalUnit__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>LAST_WEEK</value>
        </filters>
        <label>New Payment Schedules Last Week</label>
    </listViews>
    <listViews>
        <fullName>NewPaymentSchedulesThisWeek</fullName>
        <columns>NAME</columns>
        <columns>IntervalAmount__c</columns>
        <columns>IntervalUnit__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>THIS_WEEK</value>
        </filters>
        <label>New Payment Schedules This Week</label>
    </listViews>
    <listViews>
        <fullName>NewPaymentSchedulesToday</fullName>
        <columns>NAME</columns>
        <columns>IntervalAmount__c</columns>
        <columns>IntervalUnit__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATEDBY_USER</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>New Payment Schedules Today</label>
    </listViews>
    <nameField>
        <label>Payment Schedule Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Payment Schedules</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>IntervalAmount__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>IntervalAmount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>IntervalUnit__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>IntervalAmount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>IntervalUnit__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>IntervalAmount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>IntervalUnit__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>IntervalAmountMustBeBetween1And12</fullName>
        <active>true</active>
        <description>The Interval Amount must be between 1 and 12 if the Interval Unit is Months.</description>
        <errorConditionFormula>ISPICKVAL(IntervalUnit__c,&apos;Months&apos;) &amp;&amp; (IntervalAmount__c &lt; 1 || IntervalAmount__c &gt; 12)</errorConditionFormula>
        <errorDisplayField>IntervalAmount__c</errorDisplayField>
        <errorMessage>The Interval Amount must be between 1 and 12 if the Interval Unit is Months.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>IntervalAmountMustBeBetween7And365</fullName>
        <active>true</active>
        <description>The Interval Amount must be between 7 and 365 if the Interval Unit is Days.</description>
        <errorConditionFormula>ISPICKVAL(IntervalUnit__c,&apos;Days&apos;) &amp;&amp; (IntervalAmount__c &lt; 7 || IntervalAmount__c &gt; 365)</errorConditionFormula>
        <errorDisplayField>IntervalAmount__c</errorDisplayField>
        <errorMessage>The Interval Amount must be between 7 and 365 if the Interval Unit is Days.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>IntervalUnitRequired</fullName>
        <active>true</active>
        <errorConditionFormula>IsBlank( Text( IntervalUnit__c ))</errorConditionFormula>
        <errorDisplayField>IntervalUnit__c</errorDisplayField>
        <errorMessage>The interval unit is required.</errorMessage>
    </validationRules>
</CustomObject>
