<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpOrders</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>AvailableCreditBalance__c</fullName>
        <deprecated>false</deprecated>
        <description>The credit balance remaining from this credit payment that can be applied to purchases.</description>
        <externalId>false</externalId>
        <formula>IF (IsCredit__c,  PaymentAmount__c - TotalPaymentApplied__c,
    0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The credit balance remaining from this credit payment that can be applied to purchases.</inlineHelpText>
        <label>Available Credit Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CheckNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Check Number</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardCity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card City</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardCountry__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Country</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardExpirationMonth__c</fullName>
        <deprecated>false</deprecated>
        <description>Include leading zero where applicable (ex. &quot;03&quot;)</description>
        <externalId>false</externalId>
        <inlineHelpText>Include leading zero where applicable (ex. &quot;03&quot;)</inlineHelpText>
        <label>Credit Card Expiration Month</label>
        <picklist>
            <picklistValues>
                <fullName>01</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>02</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>03</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>04</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>05</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>06</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>07</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>08</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>09</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>11</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>12</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>CreditCardExpirationYear__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Expiration Year</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardIsVoid__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>When checked, the credit card was submitted for settlement, but then was voided before settlement finished, so no funds were transferred.</description>
        <externalId>false</externalId>
        <inlineHelpText>When checked, the credit card was submitted for settlement, but then was voided before settlement finished, so no funds were transferred.</inlineHelpText>
        <label>Credit Card Is Void</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>CreditCardName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Name on Credit Card</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardPostalCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Postal Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardRefundedPayment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The credit card payment to which this credit card refund payment is applied.</description>
        <externalId>false</externalId>
        <inlineHelpText>The credit card payment to which this credit card refund payment is applied.</inlineHelpText>
        <label>Credit Card Refunded Payment</label>
        <referenceTo>Payment__c</referenceTo>
        <relationshipLabel>Credit Card Refund Payments</relationshipLabel>
        <relationshipName>CreditCardRefundPayments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CreditCardSecurityCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Security Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardState__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card State</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardStreet2__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Street 2</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardStreet3__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Street 3</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CreditCardStreet__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Street</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>CreditPayableGLAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The payable GL Account used for this credit payment.</description>
        <externalId>false</externalId>
        <inlineHelpText>The payable GL Account used for this credit payment.</inlineHelpText>
        <label>Credit Payable GL Account</label>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Credit Payments</relationshipLabel>
        <relationshipName>CreditPayments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EntityCreditCardIssuer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Issuer</label>
        <referenceTo>EntityCreditCardIssuer__c</referenceTo>
        <relationshipLabel>Payments</relationshipLabel>
        <relationshipName>Payments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EntityPaymentMethod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity Payment Method</label>
        <referenceTo>EntityPaymentMethod__c</referenceTo>
        <relationshipLabel>Payments</relationshipLabel>
        <relationshipName>Payments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <description>The entity that received the payment, derived from the entity payment method</description>
        <externalId>false</externalId>
        <formula>IF(!IsBlank(EntityPaymentMethod__c),HYPERLINK(&apos;/&apos; &amp; EntityPaymentMethod__r.Entity__c, EntityPaymentMethod__r.Entity__r.Name, &apos;_self&apos;),&apos;&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The entity that received the payment, derived from the entity payment method</inlineHelpText>
        <label>Entity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IsCredit__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>When checked, a credit payment was made. These are created from cash prepayments and credit refunds.</description>
        <externalId>false</externalId>
        <inlineHelpText>When checked, a credit payment was made. These are created from cash prepayments and credit refunds.</inlineHelpText>
        <label>Is Credit</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Note__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Note</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Payer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Account that made the payment from Self Service.</description>
        <externalId>false</externalId>
        <inlineHelpText>Account that made the payment from Self Service.</inlineHelpText>
        <label>Payer</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Payments</relationshipLabel>
        <relationshipName>Payments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PaymentAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>PaymentDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Date</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>PaymentProcessorAuthorizationId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Authorization Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorAvsCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Avs Code</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorCardHolderVerifCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Card Holder Verif Code</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Code</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorRawResponse__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Raw Response</label>
        <length>4000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>15</visibleLines>
    </fields>
    <fields>
        <fullName>PaymentProcessorReasonCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Reason Code</label>
        <length>4</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorReasonMessage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Reason Message</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorSercurityVerifCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Security Verif Code</label>
        <length>1</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorSplitTenderId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Split Tender Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PaymentProcessorTransactionId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Processor Transaction Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PurchaseOrderNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purchase Order Number</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RecurringPaymentMessages__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Recurring Payment Messages</label>
        <length>4000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>RecurringPaymentResultCode__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Recurring Payment Result Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RecurringPayment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Gives more detail about the recurring subscription that created the payment, if Source = Authorize.Net ARB</description>
        <externalId>false</externalId>
        <inlineHelpText>Gives more detail about the recurring subscription that created the payment, if Source = Authorize.Net ARB</inlineHelpText>
        <label>Recurring Payment</label>
        <referenceTo>RecurringPayment__c</referenceTo>
        <relationshipLabel>Payments</relationshipLabel>
        <relationshipName>Payments</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Source</label>
        <picklist>
            <picklistValues>
                <fullName>Salesforce</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Self Service</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>TotalPaymentApplied__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of the amount applied from all the payment lines.</description>
        <externalId>false</externalId>
        <inlineHelpText>The sum of the amount applied from all the payment lines.</inlineHelpText>
        <label>Total Payment Applied</label>
        <summarizedField>PaymentLine__c.PaymentAmount__c</summarizedField>
        <summaryForeignKey>PaymentLine__c.Payment__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <label>Payment</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>PaymentAmount__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_Payments</fullName>
        <columns>NAME</columns>
        <columns>PaymentAmount__c</columns>
        <columns>PaymentDate__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Payments</label>
    </listViews>
    <listViews>
        <fullName>CreditCards</fullName>
        <columns>NAME</columns>
        <columns>CreditCardNumber__c</columns>
        <columns>CreditCardName__c</columns>
        <columns>PaymentAmount__c</columns>
        <columns>PaymentDate__c</columns>
        <columns>PaymentProcessorTransactionId__c</columns>
        <columns>CreditCardIsVoid__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CreditCardNumber__c</field>
            <operation>notEqual</operation>
        </filters>
        <label>Credit Cards</label>
    </listViews>
    <nameField>
        <displayFormat>Payment {0000000}</displayFormat>
        <label>Payment Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Payments</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>AvailableCreditBalanceCannotBeNegative</fullName>
        <active>true</active>
        <errorConditionFormula>IsCredit__c &amp;&amp; AvailableCreditBalance__c &lt; 0</errorConditionFormula>
        <errorMessage>The available credit balance cannot be negative.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>PaymentMethodRequired</fullName>
        <active>true</active>
        <description>The payment method is required.</description>
        <errorConditionFormula>IsBlank(  EntityPaymentMethod__c  )</errorConditionFormula>
        <errorMessage>The payment method is required.</errorMessage>
    </validationRules>
</CustomObject>
