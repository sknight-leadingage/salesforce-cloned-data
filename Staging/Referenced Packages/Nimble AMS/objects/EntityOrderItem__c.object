<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Defines which order item types are available for a given entity and what Accounts Receivable General Ledger to use.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ARGLAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Accounts Receivable General Ledger Account to use for this order item type for this entity.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Accounts Receivable General Ledger Account to use for this order item type for this entity.</inlineHelpText>
        <label>AR GL Account</label>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Entity Order Items</relationshipLabel>
        <relationshipName>EntityOrderItems</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <description>The entity that this order item type is configured for.</description>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Entity Order Items</relationshipLabel>
        <relationshipName>EntityOrderItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>OrderItemConfiguration__c</fullName>
        <deprecated>false</deprecated>
        <description>The order item configuration record that is available to the entity.</description>
        <externalId>false</externalId>
        <label>Order Item Configuration</label>
        <referenceTo>OrderItemConfiguration__c</referenceTo>
        <relationshipLabel>Entity Order Items</relationshipLabel>
        <relationshipName>EntityOrderItems</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ShippingGLAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The GL Account to use for shipping.</description>
        <externalId>false</externalId>
        <inlineHelpText>The GL Account to use for shipping.</inlineHelpText>
        <label>Shipping GL Account</label>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Entity Order Items (Shipping GL Account)</relationshipLabel>
        <relationshipName>ShippingEntityOrderItem</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Entity Order Item</label>
    <nameField>
        <displayFormat>Entity Order Item {0000000}</displayFormat>
        <label>Entity Order Item Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Entity Order Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>ARAndShippingGLsMustDiffer</fullName>
        <active>true</active>
        <description>The AR GL Account and Shipping GL Account must not be identical.</description>
        <errorConditionFormula>AND(IsBlank( ARGLAccount__c )=False,
          IsBlank( ShippingGLAccount__c )=False,
          ARGLAccount__c = ShippingGLAccount__c)</errorConditionFormula>
        <errorMessage>The AR GL Account and Shipping GL Account must not be identical.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ARGLAccountEntityMustMatch</fullName>
        <active>true</active>
        <description>The AR GL Account&apos;s entity must match this entity.</description>
        <errorConditionFormula>AND( IsBlank( Entity__c ) = False,
           IsBlank(  ARGLAccount__c ) = False,
           Entity__c &lt;&gt;  ARGLAccount__r.Entity__c )</errorConditionFormula>
        <errorDisplayField>ARGLAccount__c</errorDisplayField>
        <errorMessage>The AR GL Account&apos;s entity must match this entity.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ARGLAccountRequired</fullName>
        <active>true</active>
        <description>The AR GL Account is required.</description>
        <errorConditionFormula>IsBlank(  ARGLAccount__c  )</errorConditionFormula>
        <errorDisplayField>ARGLAccount__c</errorDisplayField>
        <errorMessage>The AR GL Account is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ShippingGLAccountEntityMustMatch</fullName>
        <active>true</active>
        <description>The Shipping GL Account&apos;s entity must match this entity.</description>
        <errorConditionFormula>AND( IsBlank( Entity__c ) = False,
           IsBlank(  ShippingGLAccount__c ) = False,
           Entity__c &lt;&gt;  ShippingGLAccount__r.Entity__c )</errorConditionFormula>
        <errorDisplayField>ShippingGLAccount__c</errorDisplayField>
        <errorMessage>The Shipping GL Account&apos;s entity must match this entity.</errorMessage>
    </validationRules>
</CustomObject>
