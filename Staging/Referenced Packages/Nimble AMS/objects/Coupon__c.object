<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Represents a discount that was used by an Account when placing an Order. Ties an Account to a Coupon Code.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fieldSets>
        <fullName>CouponColumns</fullName>
        <description>Coupons List</description>
        <displayedFields>
            <field>Account__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Product__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>CouponCode__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>CouponCodeValue__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Amount__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Coupon Columns</label>
    </fieldSets>
    <fields>
        <fullName>Account__c</fullName>
        <deprecated>false</deprecated>
        <description>The Bill To of the order for which the coupon was redeemed.</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Coupons</relationshipLabel>
        <relationshipName>Coupons</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount saved by using the coupon. This is either the coupon order item line&apos;s Total Price, or the External Amount if imported from an outside source.</description>
        <externalId>false</externalId>
        <formula>CouponOrderItemLine__r.TotalPrice__c + ExternalAmount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The amount saved by using the coupon</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CouponCodeValue__c</fullName>
        <deprecated>false</deprecated>
        <description>The text value of the coupon code that the user entered to redeem the coupon. This is a formula that references the Coupon Code&apos;s Code field.</description>
        <externalId>false</externalId>
        <formula>CouponCode__r.Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Coupon Code Value</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CouponCode__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The coupon code record that contains information about the code value that was redeemed. An account can only redeem a coupon code for one order.</description>
        <externalId>false</externalId>
        <label>Coupon Code</label>
        <referenceTo>CouponCode__c</referenceTo>
        <relationshipLabel>Coupons</relationshipLabel>
        <relationshipName>Coupons</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CouponOrderItemLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to the Order Item Line of the Coupon product.</description>
        <externalId>false</externalId>
        <label>Coupon Order Item Line</label>
        <referenceTo>OrderItemLine__c</referenceTo>
        <relationshipLabel>Coupons</relationshipLabel>
        <relationshipName>Coupons</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CustomerEmail__c</fullName>
        <deprecated>false</deprecated>
        <description>The customer&apos;s email at the time the order was placed. Populated via workflow.</description>
        <externalId>false</externalId>
        <label>Customer Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EntityName__c</fullName>
        <deprecated>false</deprecated>
        <description>The coupon product&apos;s entity as a text field. Can be used to filter for entity-specific coupons in search, list views, and reports.</description>
        <externalId>false</externalId>
        <label>Entity Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Used in importing legacy data. Dollar Amount is added to Amount field.</description>
        <externalId>false</externalId>
        <inlineHelpText>Used in importing legacy data. Dollar Amount is added to Amount field.</inlineHelpText>
        <label>External Amount</label>
        <precision>11</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>OrderItemLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Lookup to the Order Item Line to which the coupon applies, if the coupon was product-specific.</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to the Order Item Line to which the Coupon applies, if the Coupon was product-specific.</inlineHelpText>
        <label>Order Item Line</label>
        <referenceTo>OrderItemLine__c</referenceTo>
        <relationshipLabel>Applied Coupons</relationshipLabel>
        <relationshipName>AppliedCoupons</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PrimaryAffiliation__c</fullName>
        <deprecated>false</deprecated>
        <description>Customer&apos;s engagement account when the coupon record was created (the customer&apos;s primary affiliation).</description>
        <externalId>false</externalId>
        <formula>CouponOrderItemLine__r.OrderItem__r.CustomerPrimaryAffiliation__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Customer&apos;s engagement account when the coupon record was created (customer&apos;s primary affiliation)</inlineHelpText>
        <label>Engagement Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The coupon product that was redeemed.</description>
        <externalId>false</externalId>
        <label>Product</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Product__c.RecordTypeName__c</field>
                <operation>equals</operation>
                <value>Coupon</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Coupons</relationshipLabel>
        <relationshipName>Coupons</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Search__c</fullName>
        <deprecated>false</deprecated>
        <description>Contains related object value(s) so that these records show up in a global search. This field should not be shown on page layouts.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contains related object value(s) so that these records show up in a global search. This field should not be shown on page layouts.</inlineHelpText>
        <label>Search</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>StatusFlag__c</fullName>
        <deprecated>false</deprecated>
        <description>Visual representation of the Status, which is based on the status of the Coupon Order Item Line.</description>
        <externalId>false</externalId>
        <formula>IF(!ISBLANK(Status__c), IMAGE(&apos;/resource/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusIcons/&apos; + Status__c + &apos;.png&apos;, &apos;Status&apos;), IMAGE(&apos;/resource/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusIcons/Inactive.png&apos;, &apos;Status&apos;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Based on the status of the Coupon Order Item Line</inlineHelpText>
        <label>Status Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <description>The order Status of this coupon record. This is a formula field that reference&apos;s the status of the Coupon Order Item Line.</description>
        <externalId>false</externalId>
        <formula>TEXT(CouponOrderItemLine__r.Status__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Coupon</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>CouponCode__c</columns>
        <columns>Amount__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Coupons</label>
    </listViews>
    <nameField>
        <displayFormat>Coupon {0000000}</displayFormat>
        <label>Coupon Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Coupons</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CouponCode__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CouponCodeValue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Amount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StatusFlag__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CouponCode__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CouponCodeValue__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Amount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StatusFlag__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>Account__c</lookupFilterFields>
        <lookupFilterFields>CouponCode__c</lookupFilterFields>
        <lookupFilterFields>CouponCodeValue__c</lookupFilterFields>
        <lookupFilterFields>Product__c</lookupFilterFields>
        <lookupFilterFields>Status__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CouponCode__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CouponCodeValue__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Amount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StatusFlag__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>CouponCode__c</searchFilterFields>
        <searchFilterFields>CouponCodeValue__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
        <searchFilterFields>Amount__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CouponCode__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CouponCodeValue__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Product__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Amount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StatusFlag__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
