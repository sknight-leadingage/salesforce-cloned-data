<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>BankAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The bank account that should be debited or credited for this payment.</description>
        <externalId>false</externalId>
        <inlineHelpText>The bank account that should be debited or credited for this payment.</inlineHelpText>
        <label>Bank Account</label>
        <referenceTo>BankAccount__c</referenceTo>
        <relationshipLabel>Entity Payment Methods</relationshipLabel>
        <relationshipName>EntityPaymentMethods</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Entity Payment Methods</relationshipLabel>
        <relationshipName>EntityPaymentMethods</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>PaymentMethod__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment Method</label>
        <referenceTo>PaymentMethod__c</referenceTo>
        <relationshipLabel>Entity Payment Methods</relationshipLabel>
        <relationshipName>EntityPaymentMethods</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Entity Payment Method</label>
    <nameField>
        <displayFormat>EntityPaymentMethod {0000000}</displayFormat>
        <label>Entity Payment Method Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Entity Payment Methods</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>BankAccountEntityMustMatch</fullName>
        <active>true</active>
        <description>The Bank Account&apos;s Entity must match the Entity selected.</description>
        <errorConditionFormula>AND( IsBlank( Entity__c )=False,
Entity__c &lt;&gt;    BankAccount__r.Entity__c)</errorConditionFormula>
        <errorDisplayField>BankAccount__c</errorDisplayField>
        <errorMessage>The Bank Account&apos;s Entity must match the Entity selected.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>BankAccountRequiredForPayment</fullName>
        <active>true</active>
        <description>A bank account is required.</description>
        <errorConditionFormula>AND( IsBlank(PaymentMethod__c) = False,
           PaymentMethod__r.IsPayment__c = True,
           IsBlank( BankAccount__c ))</errorConditionFormula>
        <errorMessage>A bank account is required.</errorMessage>
    </validationRules>
</CustomObject>
