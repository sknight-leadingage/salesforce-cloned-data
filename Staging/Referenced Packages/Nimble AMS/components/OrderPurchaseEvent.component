<apex:component controller="NU.OrderPurchaseEvent" allowDML="true">

    <apex:attribute name="c" type="NU.OrderController" description="OrderController" assignTo="{!Controller}"/>

    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd();NUBind()" id="ASBind"/>
    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd();" id="AS"/>

    <apex:actionRegion >

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/custom51_100/presenter32.png)"/>
                <h1 class="pageType">{!c.Subheader}&nbsp;</h1>
                <h2 class="pageDescription">{!IF(IsEDIT, 'Edit', 'Add')} Registration</h2>
            </div>
        </div>
    </div>

    <apex:pageMessages escape="{!c.EscapeMessages}" id="Msgs" />

    <apex:pageBlock mode="maindetail" Id="Main">

        <apex:pageBlockSection columns="1">
            <apex:inputField value="{!DummyEventReg.NU__Account__c}" rendered="{!!IsEdit}">
                <apex:actionSupport event="onchange" action="{!OnAccountSelected}" rerender="Msgs,Main" status="ASBind"/>
            </apex:inputField>
            <apex:outputField value="{!DummyEventReg.NU__Account__c}" rendered="{!IsEdit}"/>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && !IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__Entity__c}">
                    <apex:selectOptions value="{!EntityOptions}"/>
                    <apex:actionSupport event="onchange" onsubmit="if(!confirmEntitySwitch()) { this.value = '{!Controller.CurrentCartItem.NU__Entity__c}'; return false; }" action="{!OnEntitySelected}" rerender="Msgs,Main" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:outputField value="{!Controller.CurrentCartItem.NU__Entity__c}"/>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && !IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__Registration2__c.Fields.NU__Event2__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!DummyEventReg.NU__Event2__c}">
                    <apex:selectOption itemvalue="" itemLabel="Please select an event" rendered="{!DummyEventReg.NU__Event2__c == null}"/>
                    <apex:selectOptions value="{!EventOptions}"/>
                    <apex:actionSupport event="onchange" action="{!OnEventSelected}" rerender="Msgs,Main,EventBadgeDialogPanel" status="ASBind"/>
                </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:outputField value="{!DummyEventReg.NU__Event2__c}" rendered="{!IsEdit}" />

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && DummyEventReg.NU__Account__c != null && DummyEventReg.NU__Event2__c != null && !AlreadyRegistered}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__PriceClass__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__PriceClass__c}">
                    <apex:selectOption itemvalue="" itemLabel="Please select a price class" rendered="{!Controller.CurrentCartItem.NU__PriceClass__c == null}"/>
                    <apex:selectOptions value="{!PriceClassOptions}"/>
                    <apex:actionSupport event="onchange" action="{!OnPriceClassSelected}" rerender="Msgs,Main" status="ASBind"/>
                </apex:selectList>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Sessions" columns="1" rendered="{!DummyEventReg.NU__Event2__c != null && DummyEventReg.NU__Account__c != null && !AlreadyRegistered}">
            <apex:pageBlockSectionItem >
                <apex:pageBlockTable value="{!SessionOrderItemLines}" var="oli">
                    <apex:column styleClass="purCol">
                        <apex:facet name="header">Purchase</apex:facet>
                        <apex:inputField value="{!oli.NU__IsInCart__c}" styleClass="{!IF(oli.Id == null && oli.NU__OrderItemLine__c == null,'selectableRow','unselectableRow')}" rendered="{! oli.Product2__r.TrackInventory__c == false || oli.Product2__r.InventoryOnHand__c > 0 || oli.Id != null }"/>
                    </apex:column>
                    <apex:column value="{!oli.NU__Product2__c}"/>
                    <apex:column headerClass="number" styleclass="number">
                        <apex:facet name="header">Inventory</apex:facet>
                        <apex:outputField value="{!oli.Product2__r.NU__InventoryOnHand__c}" styleClass="nbr" rendered="{!oli.Product2__r.NU__TrackInventory__c == true}" />
                        <apex:outputText rendered="{! oli.Product2__r.TrackInventory__c == false}" />
                    </apex:column>

                    <apex:column styleClass="number" headerClass="number">
                        <apex:facet name="header">
                            <apex:outputText value="{! $ObjectType.OrderItemLine__c.Fields.UnitPrice__c.Label }" />
                           </apex:facet>
                        <div style="display: inline-block;">
                            <c:Required rendered="{! oli.Product2__r.TrackInventory__c == false || oli.Product2__r.InventoryOnHand__c > 0 || oli.Id != null }">
                             <apex:inputField value="{!oli.NU__UnitPrice__c}"
                                              styleClass="inputPrice"
                                              required="false"
                                              onkeyup="calcPrice(this)"
                                              onblur="calcPrice(this)"
                                              onfocus="this.maxLength = {! UnitPriceMaxLength };" />
                            </c:Required>
                            <apex:outputField value="{!oli.NU__UnitPrice__c}" rendered="{! oli.Product2__r.TrackInventory__c && oli.Product2__r.InventoryOnHand__c == 0 && oli.Id == null }" />
                        </div>

                    </apex:column>

                    <apex:column styleClass="number" headerClass="number">
                        <apex:facet name="header">
                            <apex:outputText value="{! $ObjectType.OrderItemLine__c.Fields.Quantity__c.Label }" />
                           </apex:facet>
                        <div style="display: inline-block">
                            <c:Required rendered="{! oli.Product2__r.TrackInventory__c == false || oli.Product2__r.InventoryOnHand__c > 0 || oli.Id != null }">
                                <apex:inputField value="{!oli.NU__Quantity__c}"
                                                 styleClass="inputQuantity"
                                                 required="false"
                                                 onkeyup="calcPrice(this)"
                                                 onblur="calcPrice(this)"
                                                 onfocus="this.maxLength = {! QuantityMaxLength };" />
                            </c:Required>
                            <apex:outputField value="{!oli.NU__Quantity__c}" rendered="{! oli.Product2__r.TrackInventory__c && oli.Product2__r.InventoryOnHand__c == 0 && oli.Id == null }" />
                        </div>
                    </apex:column>

                    <apex:column styleClass="number priceCOL" headerClass="number priceCOL">
                        <apex:facet name="header">Price</apex:facet>
                        <span class="outputPrice">
                            <apex:outputText value="{0,number,$0.00}">
                                <apex:param value="{!oli.NU__UnitPrice__c * oli.NU__Quantity__c}"/>
                            </apex:outputText>
                        </span>
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Badges" columns="1" rendered="{!Controller.Cart.NU__Entity2__c != null && DummyEventReg.NU__Account__c != null && DummyEventReg.NU__Event2__c != null && !AlreadyRegistered}">
            <apex:outputPanel >
                <apex:pageBlockTable value="{!EventBadgeValues}" var="wrapper">
                    <apex:column >
                        <apex:commandLink value="Edit" action="{!EditEventBadge}" status="AS" rerender="Msgs,EventBadgeDialogPanel">
                            <apex:param name="index" value="{!wrapper.Index}" assignTo="{!BadgeIndex}" />
                        </apex:commandLink>
                        <apex:commandLink value="Del" action="{!DeleteEventBadge}" styleClass="actionButtonLeftSpace" rendered="{!wrapper.Index != 0}" rerender="Msgs,Main" status="ASBind">
                            <apex:param name="index" value="{!wrapper.Index}" assignTo="{!BadgeIndex}" />
                        </apex:commandLink>
                    </apex:column>
                    <apex:column value="{!wrapper.EventBadge.NU__BadgeType__c}"/>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.NU__EventBadge__c.fields.NU__Name__c.label}</apex:facet>
                        {!wrapper.EventBadge.NU__FirstName__c} {!wrapper.EventBadge.NU__LastName__c}
                    </apex:column>
                    <apex:column value="{!wrapper.EventBadge.NU__BadgeClass__c}" />
                    <apex:column headerValue="{!$ObjectType.NU__OrderItemLine__c.Fields.NU__UnitPrice__c.Label}" headerClass="nbr" styleClass="nbr">
                        <apex:outputField value="{!wrapper.EventBadgeLine.UnitPrice__c }" rendered="{!wrapper.Index != 0}" />
                    </apex:column>
                </apex:pageBlockTable>
                <apex:commandButton value="Add Badge"
                                    style="margin-top:10px"
                                    action="{!AddEventBadge}"
                                    rendered="{! EventBadgeTemplateLinesCount > 0 }"
                                    rerender="EventBadgeDialogPanel"
                                    status="AS"/>
            </apex:outputPanel>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Questions" columns="1" rendered="{!DummyEventReg.NU__Event2__c != null && DummyEventReg.NU__Account__c != null && !AlreadyRegistered}">
            <apex:outputText value="There are no event questions defined for this event" rendered="{!AllEventQuestionsCount == 0}" />

            <apex:repeat var="question" value="{!EventOnlyQuestions}">
                    <c:EventQAComponent evtQAWrapper="{!question}" />
            </apex:repeat>

            <apex:repeat var="question" value="{!EventSessionOnlyQuestions}">
                    <c:EventQAComponent evtQAWrapper="{!question}" />
            </apex:repeat>
        </apex:pageBlockSection>

        <apex:pageBlockButtons location="both">
            <apex:commandButton value="Save" styleClass="green" action="{!Save}" rendered="{!Controller.Cart.NU__Entity2__c != null && DummyEventReg.NU__Account__c != null && DummyEventReg.NU__Event2__c != null && Controller.CurrentCartItem.NU__PriceClass__c != null && !AlreadyRegistered}" rerender="Msgs,Main" status="ASBind"/>
            <apex:commandButton value="Cancel" action="{!Cancel}" rerender="Msgs,Main" status="ASBind"/>
        </apex:pageBlockButtons>

    </apex:pageBlock>

    </apex:actionRegion>

    <c:CustomDialog id="EventBadge" title="Event Badge" closeable="false">

        <apex:outputPanel id="EventBadgeDialogPanel">

            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!BadgeDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:outputPanel layout="none"
                rendered="{! Controller.Cart.Entity2__c != null &&
                          DummyEventReg.Account__c != null &&
                          DummyEventReg.Event2__c != null &&
                          !AlreadyRegistered &&
                          NOT(ISBLANK(CurrentBadgeWrapper.EventBadge)) &&
                          (CurrentBadgeWrapper.EventBadgeLine.Product2__c != null || CurrentBadgeWrapper.Index == 0 || EventBadgeTypeOptionsCount > 0) }">

            <apex:pageMessages escape="{!c.EscapeMessages}" />

            <apex:pageBlock mode="maindetail">
                <apex:pageBlockSection columns="2">
                    <apex:pageBlockSectionItem rendered="{!CurrentBadgeWrapper.Index != 0}">
                        <apex:outputLabel value="Badge Product"/>
                        <c:Required >
                            <apex:selectList size="1" value="{!CurrentBadgeWrapper.EventBadgeLine.NU__Product2__c}">
                                <apex:selectOption itemLabel="Please select an event badge" itemValue="" />
                                <apex:selectOptions value="{!EventBadgeTypeOptions}" />
                                <apex:actionSupport event="onchange" action="{!OnEventBadgeTypeSelected}" rerender="Msgs,EventBadgeDialogPanel" status="AS" />
                            </apex:selectList>
                        </c:Required>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem rendered="{!CurrentBadgeWrapper.Index != 0}">
                        <apex:outputLabel value="Price"/>
                        <apex:outputPanel layout="none">
                            <c:Required rendered="{!NOT(ISBLANK(CurrentBadgeWrapper.EventBadgeLine.NU__Product2__c))}">
                                <apex:inputField value="{!CurrentBadgeWrapper.EventBadgeLine.NU__UnitPrice__c}" required="false"  onfocus="this.maxLength = {! UnitPriceMaxLength };" />
                            </c:Required>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="{!$ObjectType.NU__EventBadge__c.Fields.NU__BadgeClass__c.Label}"/>
                        <apex:outputPanel layout="none">
                            <c:Required ><apex:inputField value="{!CurrentBadgeWrapper.EventBadge.NU__BadgeClass__c}" /></c:Required>
                            <apex:inputField value="{!CurrentBadgeWrapper.EventBadge.NU__BadgeType__c}" style="display:none" />
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
            </apex:pageBlock>

            <hr style="margin-bottom:20px" />

            <apex:pageBlock mode="maindetail">
                <apex:pageBlockSection columns="2">
                    <apex:pageBlockSectionItem >
                        {!$ObjectType.NU__EventBadge__c.Fields.NU__FirstName__c.Label}
                        <apex:inputField value="{!CurrentBadgeWrapper.EventBadge.NU__FirstName__c}" required="false"/>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        {!$ObjectType.NU__EventBadge__c.Fields.NU__LastName__c.Label}
                        <c:Required ><apex:inputField value="{!CurrentBadgeWrapper.EventBadge.NU__LastName__c}" required="false"/></c:Required>
                    </apex:pageBlockSectionItem>
                    <apex:repeat value="{!$ObjectType.NU__EventBadge__c.FieldSets.NU__EventBadgeAddEdit}" var="f">
                        <apex:inputField value="{!CurrentBadgeWrapper.EventBadge[f]}"/>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlock>

            <div class="buttons">
                <apex:commandButton value="Save Badge" action="{!saveEventBadge}" styleClass="green" rerender="Main,EventBadgeDialogPanel" status="ASBind" />
                <apex:commandButton value="Cancel" action="{!cancelEditEventBadge}" rerender="EventBadgeDialogPanel" status="AS" />
            </div>
        </apex:outputPanel>
        </apex:outputPanel>
    </c:CustomDialog>

</apex:component>