<apex:component controller="NU.OrderPaymentController" allowDML="true">
    <apex:attribute name="c"
                    type="NU.OrderController"
                    description="OrderController"
                    assignTo="{!Controller}"/>

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/cash32.png)"/>
                <h1 class="pageType">{!c.Subheader}&nbsp;</h1>
                <h2 class="pageDescription">Add Payment</h2>
            </div>
        </div>
    </div>

    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd()" id="AS"/>
    <apex:pageMessages escape="{!c.EscapeMessages}" id="Msgs" />

    <apex:pageMessage summary="The entity, {!SelectedEntityForPayment.Name}, does not have any payment methods defined."
        rendered="{!entityPaymentMethodCount == 0}"
        severity="warning"
        strength="2"/>

    <apex:pageMessage summary="{!MUST_SUBMIT_CART_TO_APPLY_CHANGES_INFO_MSG}"
        rendered="{! Controller.Cart.Order__c != null }"
        severity="info"
        strength="1"/>

    <apex:outputPanel id="RecurringAndNonrecurringCartItemsSelectedMsgPanel">
        <apex:pageMessage id="RecurringAndNonrecurringCartItemsSelectedMsg" summary="{!CANNOT_PAY_RECURRING_AND_NONRECURRING_SIMULTANEOUSLY_MSG}"
            rendered="{!AreRecurringAndNonrecurringCartItemsSelected}"
            severity="warning"
            strength="2"/>
    </apex:outputPanel>

    <apex:outputPanel id="TransferMsgPanel">
        <apex:pageMessage id="TransferPaymentMsg" summary="This cart has cart items with refunds needed, and active cart items with balances. Please remember to refund/pay balances accordingly, or transfer the payments to the active cart item(s)."
            rendered="{!Controller.CanTransferPayments}"
            severity="warning"
            strength="2"/>
    </apex:outputPanel>

    <apex:outputPanel id="AutomaticTransferPanel" rendered="{!!DidAutomaticTransferCheckOccur && CanAutomaticallyTransferPayments}">
        <apex:actionFunction name="processAutomaticTransfer" action="{!processAutomaticTransfer}" status="AS" rerender="TransferMsgPanel, cartItems, payments"/>
        <script type="text/javascript">
            $(document).ready(function(){
                processAutomaticTransfer();
            });
        </script>
    </apex:outputPanel>

    <apex:pageBlock Title="Cart Items" id="cartItems">
        <apex:outputText value="No Cart Items" rendered="{!CartItemWrapperCount == 0}" />
        <apex:pageBlockTable value="{!CartItemWrappers}" var="ciw" rendered="{!CartItemWrapperCount > 0}">
            <apex:column style="text-align:center;" rendered="{! entityPaymentMethodCount > 0 }">
                <apex:inputCheckbox value="{!ciw.Selected}">
                    <apex:actionSupport event="onchange"
                        status="AS" rerender="RecurringAndNonrecurringCartItemsSelectedMsgPanel" />
                </apex:inputCheckbox>
            </apex:column>
            <apex:column value="{!ciw.CartItem.NU__Customer__c}" />
            <apex:column >
                <apex:facet name="header">Order Type</apex:facet>
                <apex:outputText value="{!ciw.CartItem.RecordType.Name}" />
                <apex:outputText value=" (Recurring)" rendered="{!ciw.Recurring}" />
            </apex:column>
            <apex:column headerValue="Entity" rendered="{!c.isCartMultiEntity == true}">
                <apex:outputField value="{!ciw.CartItem.Entity__r.NU__ShortName__c}" />
            </apex:column>
            <apex:column headerValue="{!$ObjectType.NU__CartItem__c.Fields.NU__Total__c.Label}" styleClass="number" headerClass="number" footerClass="number totalFooter">
                <apex:outputField value="{!ciw.CartItem.NU__Total__c}" />
                <apex:outputPanel rendered="{!c.IsEdit}">
                    <apex:facet name="footer">
                        <apex:outputField value="{!c.Cart.NU__Total__c}" />
                    </apex:facet>
                </apex:outputPanel>
            </apex:column>
            <apex:column headerValue="{!$ObjectType.NU__CartItem__c.Fields.NU__TotalPayment__c.Label}" styleClass="number" headerClass="number" footerClass="number totalFooter">
                <apex:outputField value="{!ciw.CartItem.NU__TotalPayment__c}" />
                <apex:outputPanel rendered="{!c.IsEdit}">
                    <apex:facet name="footer">
                        <apex:outputField value="{!c.Cart.NU__TotalPayment__c}" rendered="{!c.IsEdit}" />
                    </apex:facet>
                </apex:outputPanel>
            </apex:column>
            <apex:column headerValue="{!$ObjectType.NU__CartItem__c.Fields.NU__Balance__c.Label}" styleClass="number" headerClass="number" footerClass="number totalFooter">
                <apex:outputField value="{!ciw.CartItem.NU__Balance__c}" />
                <apex:outputPanel rendered="{!c.IsEdit}">
                    <apex:facet name="footer">
                        <apex:outputField value="{!c.Cart.NU__Balance__c}" rendered="{!c.IsEdit}" />
                    </apex:facet>
                </apex:outputPanel>
            </apex:column>
            <apex:column headerValue="Status" rendered="{! c.Cart.Order__c != null }">
                <apex:outputText value="{! If (ciw.CartItem.CartItemLineCount__c == 0, 'Cancelled', 'Active') }" />
            </apex:column>
        </apex:pageBlockTable>

        <apex:pageBlockButtons location="bottom" rendered="{! entityPaymentMethodCount > 0 && (! IsBlank(Controller.Cart.Balance__c) && CartItemWrapperCount != 0)}">
            <apex:commandButton value="{! IF (Controller.Cart.Balance__c > 0, 'Pay', IF(Controller.Cart.Balance__c < 0, 'Refund', 'Pay / Refund')) }"
                                styleClass="green {! IF(Controller.Cart.Balance__c != 0, 'OrderPmtPayBtn', 'OrderPmtPayRefundBtn')}"
                                rerender="PaymentDialogPanel"
                                action="{!AddPayment}" />
            <apex:commandButton value="Transfer Payments"
                                styleClass="green OrderPmtTransferBtn"
                                rerender="TransferPaymentsDialogPanel"
                                rendered="{!Controller.CanTransferPayments}"
                                action="{!loadTransferPaymentsDialog}" />
        </apex:pageBlockButtons>
    </apex:pageBlock>

    <apex:pageMessage summary="{!CANNOT_MODIFY_PAYMENT_UNLESS_BATCH_OPEN_MSG}"
        rendered="{!ArePaymentsInNonOpenBatches}"
        severity="info"
        strength="1"/>

    <apex:pageBlock Title="Payments" id="payments">
        <apex:outputText value="No Payments" rendered="{!CartPaymentsCount == 0}" />
        <apex:pageBlockTable value="{!CartPayments}" var="cp" rendered="{!CartPaymentsCount > 0}">
            <apex:column styleClass="orderPmtPaymentsActionColumn">
                <apex:commandLink value="Edit" action="{!editPayment}" rerender="PaymentDialogPanel" rendered="{!cp.IsEditable}">
                    <apex:param name="pmtToEdit" value="{!cp.Record.Id}" assignTo="{!SelectedPaymentId}" />
                </apex:commandLink>
                {!" "}
                <apex:commandLink value="Del"
                          action="{!deleteCartPayment}"
                          onclick="return confirmDelete()"
                          styleClass="actionButtonLeftSpace"
                          rendered="{!cp.IsDeletable}">
                    <apex:param name="pmtToDelete" value="{!cp.Record.Id}" assignTo="{!SelectedPaymentId}" />
                </apex:commandLink>
                <apex:commandLink value="Void"
                          action="{!loadVoidDialog}"
                          rerender="VoidCCDialogPanel"
                          rendered="{!cp.IsVoidable}">
                    <apex:param name="pmtToDelete" value="{!cp.Record.Id}" assignTo="{!SelectedPaymentId}" />
                </apex:commandLink>
            </apex:column>
            <apex:column headerValue="{!$ObjectType.NU__CartPayment__c.Fields.NU__Payment__c.Label}" value="{!cp.Record.NU__Payment__c}" rendered="{!Controller.Cart.NU__Order__c != null}" />
            <apex:column headerValue="{!$ObjectType.NU__PaymentMethod__c.Fields.Name.Label}">
                <apex:outputField value="{!cp.Record.EntityPaymentMethod__r.PaymentMethod__r.Name}" />
            </apex:column>
            <apex:column headerValue="{!$ObjectType.NU__CartPayment__c.Fields.NU__PaymentDate__c.Label}">
                <apex:outputField value="{!Controller.Cart.NU__TransactionDate__c}" rendered="{!IsNull(cp.Record.Payment__r)}" />
                <apex:outputField value="{!cp.Record.Payment__r.NU__PaymentDate__c}" rendered="{!IsNull(cp.Record.Payment__r) == false}" />
            </apex:column>
            <apex:column headerValue="CC / Check #">
                <apex:outputText value="{!cp.Record.NU__CheckNumber__c}" rendered="{!cp.Record.EntityPaymentMethod__r.PaymentMethod__r.RecordType.Name == 'Check'}" />
                <apex:outputText value="{!cp.Record.NU__CreditCardNumber__c}" rendered="{!cp.Record.EntityPaymentMethod__r.PaymentMethod__r.RecordType.Name == 'Credit Card'}" />
            </apex:column>

            <apex:column headerValue="{!$ObjectType.NU__CartPayment__c.Fields.NU__CreditCardIsVoid__c.Label}">
                <apex:outputField value="{!cp.Record.NU__CreditCardIsVoid__c}" />
            </apex:column>

            <apex:column headerValue="Entity" rendered="{!CartHasDifferentPaymentEntities}">
                <apex:outputField value="{!cp.Record.EntityPaymentMethod__r.Entity__r.NU__ShortName__c}" />
            </apex:column>

            <apex:column headerValue="{!$ObjectType.NU__CartPayment__c.Fields.NU__Note__c.Label}" styleClass="orderPmtPaymentNoteCol">
                <apex:outputField value="{!cp.Record.NU__Note__c}" />
            </apex:column>
            <apex:column value="{!cp.Record.NU__PaymentAmount__c}" styleClass="number" headerClass="number" />
        </apex:pageBlockTable>
    </apex:pageBlock>

    <apex:pageBlock Title="Recurring Payments" id="recurringPayments" rendered="{!RecurringPaymentsCount > 0}">
        <apex:pageBlockTable value="{!RecurringPayments}" var="rp">
            <apex:column styleClass="orderPmtPaymentsActionColumn">
                <apex:commandLink value="Cancel"
                          action="{!cancelRecurringPayment}"
                          onclick="return confirmDelete()"
                          rendered="{!rp.NU__Status__c == 'Active'}">
                    <apex:param name="recurringPmtToDelete" value="{!rp.Id}" assignTo="{!SelectedRecurringPaymentId}" />
                </apex:commandLink>
            </apex:column>
            <apex:column value="{!rp.Name}" />
            <apex:column value="{!rp.NU__Account__c}" />
            <apex:column value="{!rp.PaymentScheduleLink__r.NU__PaymentSchedule__c}" headerValue="{!$ObjectType.NU__PaymentSchedule__c.Label}" />
            <apex:column value="{!rp.NU__CreditCardNumber__c}" />
            <apex:column value="{!rp.NU__Status__c}" />
            <apex:column value="{!rp.NU__Amount__c}" styleClass="number" headerClass="number" />
        </apex:pageBlockTable>
    </apex:pageBlock>

    <c:CustomDialog id="Payment" title="Payment Details" style="overflow: auto; max-height: 600px; width: 750px;" closeable="false">
        <apex:outputPanel id="PaymentDialogPanel">
            <apex:pageMessages id="PaymentMsgs" />

            <apex:pageMessage summary="{!CREDIT_CARD_PAYMENT_FOR_DIFFERENT_DATE_MSG}"
                rendered="{!HasCreditCardPaymentForDifferentDate && !(Payment != null && Payment.NU__EntityPaymentMethod__c != null)}"
                severity="info" strength="2" />

            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!PaymentDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:pageBlock mode="maindetail" id="pmtForm">
                <apex:pageBlockSection columns="1" id="orderPmtFormInputs">
                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c == null && ShowEntityForPaymentOptions}">
                        Payment Entity
                        <apex:selectList value="{!SelectedEntityForPaymentId}" multiselect="false" size="1">
                            <apex:selectOptions value="{!EntityForPaymentSelectOptions}" />
                            <apex:actionSupport event="onchange" action="{!entityForPaymentChanged}"
                                status="AS" rerender="PaymentDialogPanel" />
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c != null && ShowEntityForPaymentOptions}">
                        Payment Entity
                        <apex:outputField value="{! SelectedEntityForPayment.ShortName__c }"></apex:outputField>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c == null }">
                        {!PaymentMode} Method

                        <apex:selectList value="{!SelectedPaymentMethodString}" multiselect="false" size="1">
                            <apex:selectOption itemLabel="Please select a Payment Method" itemValue="" />
                            <apex:selectOptions value="{!availablePaymentMethods}" />
                            <apex:actionSupport event="onchange" action="{!paymentMethodChanged}"
                                focus="{!IF (SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card', 'ccNum',
                                    IF (SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Check' && Payment.CreditPayment__c == '', 'checkNum', 'orderPmtNote'))}"
                                status="AS" rerender="PaymentDialogPanel" />
                        </apex:selectList>

                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c != null }">
                        Payment Method
                        <apex:outputText value="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name}" />
                    </apex:pageBLockSectionItem>

                    <!-- Installment Payment Section -->
                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c == null && SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && ShowInstallmentPaymentsSection}">
                        <apex:repeat id="RptInstallmentPayments" value="{!PaymentSchedules}" var="ps">
                            <br />
                            {!ps.CartItem.Customer__r.Name}'s Recurring Membership Installment Plan (optional):
                            <apex:selectRadio Id="RadioInstallmentPayments" value="{!ps.SelectedPaymentScheduleLinkId}">
                                <apex:selectOptions value="{!ps.SelectOptions}" />
                                <apex:actionSupport event="onchange" action="{!updateInstallmentAmounts}" rerender="PaymentDialogPanel" status="AS" />
                            </apex:selectRadio>
                            <apex:outputPanel rendered="{!ps.SelectedPaymentScheduleLinkId != null}">
                                (<apex:outputText value="{0,number,$#,##0.00}">
                                    <apex:param value="{!ps.RecurringPaymentCalculation.InstallmentAmount}"/>
                                </apex:outputText> Per Installment)<br />
                            </apex:outputPanel>
                            <br />
                        </apex:repeat>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.EntityPaymentMethod__c != '' && Controller.Cart.Balance__c == 0 && ShowCCPaymentType }">
                        <apex:outputText value="Payment Type" />

                        <apex:selectRadio value="{!CreditCardPaymentType}">
                            <apex:selectOption itemValue="Pay" itemLabel="Pay"/>
                            <apex:selectOption itemValue="Refund" itemLabel="Refund"/>
                            <apex:actionSupport event="onchange" action="{!creditCardPaymentTypeChanged}" rerender="PaymentDialogPanel" />
                        </apex:selectRadio>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && (Controller.Cart.NU__Balance__c < 0 || CreditCardPaymentType == 'Refund')}">
                        <apex:outputText value="Credit Card" />

                        <apex:outputPanel >
                            <c:Required >
                                <apex:selectList value="{!Payment.NU__CreditCardRefundedPayment__c}" multiselect="false" size="1">
                                    <apex:selectOption itemLabel="Please select a credit card to refund" itemValue="" />
                                      <apex:selectOptions value="{!RefundableCreditCardPaymentSelectOptions}" />
                                      <apex:actionSupport event="onchange" action="{!creditCardToRefundChanged}" rerender="PaymentDialogPanel" />
                                  </apex:selectList>
                              </c:Required>
                          </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && (Controller.Cart.NU__Balance__c < 0 || CreditCardPaymentType == 'Refund')}">
                        <apex:outputText value="Manual" />

                        <apex:inputCheckbox value="{!manualCCRefund}">
                            <apex:actionSupport event="onchange" action="{!manualCCRefundChanged}" rerender="PaymentDialogPanel" />
                        </apex:inputCheckbox>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c == null && (Payment.EntityPaymentMethod__c != null && SelectedPaymentMethod.PaymentMethod__r.IsPayment__c == true) }">
                        <apex:outputText value="{!IF(SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && ShowInstallmentPaymentsSection,
                            'Initial ','')}{!PaymentMode} Amount" />

                        <apex:outputPanel id="PaymentAmount">
                            <c:Required rendered="{! SelectedRefundCC == '' || DateValue(SelectedRefundCC.CreatedDate) != Today() || manualCCRefund == true }">
                                <apex:inputField value="{!Payment.NU__PaymentAmount__c}">
                                    <apex:actionSupport event="onblur" action="{!paymentAmountChanged}" rerender="PaymentMsgs,PaymentAmount,ApplyPayment" status="AS"/>
                                </apex:inputField>
                            </c:Required>

                            <apex:outputField value="{!Payment.NU__PaymentAmount__c}" rendered="{! SelectedRefundCC != '' && DateValue(SelectedRefundCC.CreatedDate) == Today() && manualCCRefund != true }" />
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem rendered="{! Payment.Payment__c != null }" >
                        Payment Amount
                        <apex:outputField value="{!Payment.NU__PaymentAmount__c}" />
                    </apex:pageBlockSectionItem>

                    <!-- Payment label and box to display check number -->
                    <apex:pageBlockSectionItem rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Check' && Payment.NU__CreditPayment__c == '' && Payment.NU__EntityPaymentMethod__c != ''}">
                        <apex:outputText value="{! $ObjectType.Payment__c.Fields.CheckNumber__c.Label }" />
                        <c:Required >
                            <apex:inputField id="checkNum" value="{!Payment.NU__CheckNumber__c}" />
                        </c:Required>
                    </apex:pageBlockSectionItem>

                    <apex:inputField value="{!Payment.NU__PurchaseOrderNumber__c}"
                                     rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'PO / Invoice' && Payment.NU__EntityPaymentMethod__c != ''}" />

                    <apex:pageBlockSectionItem rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && (Controller.Cart.NU__Balance__c > 0 || CreditCardPaymentType == 'Pay' )}">
                        <apex:outputPanel />
                        <apex:outputPanel >
                            <div id="ccImageDiv">
                                <apex:repeat value="{!CreditCardIssuers}" var="cci">
                                    <img src="{!cci.CreditCardIssuer__r.ImageStaticResourceName__c}"
                                             title="{!cci.CreditCardIssuer__r.CardNumberErrorMessage__c}"
                                             id="{!cci.CreditCardIssuer__r.Name}"
                                             class="{! IF(cci.CreditCardIssuer__r.Name = selectedCreditCardIssuerName, 'selectedPmtImg', 'unselectedPmtImg')}" />
                                </apex:repeat>
                            </div>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem HelpText="{!$ObjectType.NU__Payment__c.Fields.NU__CreditCardNumber__c.inlineHelpText}" rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && (Controller.Cart.NU__Balance__c > 0 || manualCCRefund == true || CreditCardPaymentType == 'Pay' )}">
                        <apex:outputText value="Number" />
                        <apex:outputPanel >
                            <c:Required >
                                <apex:inputField id="ccNum" value="{!Payment.NU__CreditCardNumber__c}" onkeyup="updateCreditCardIssuerDisplay(this);" />
                            </c:Required>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.EntityPaymentMethod__c != '' && (Controller.Cart.Balance__c > 0 || manualCCRefund == true || CreditCardPaymentType == 'Pay') }">
                        <apex:outputLabel value="Expires" for="ccexpmonth"  />
                        <apex:outputPanel >
                            <apex:selectList value="{!Payment.NU__CreditCardExpirationMonth__c}"
                                                 size="1" id="ccexpmonth"
                                                 styleClass="ccexpmonth">
                                <apex:selectOption itemValue="01" itemLabel="01" />
                                <apex:selectOption itemValue="02" itemLabel="02" />
                                <apex:selectOption itemValue="03" itemLabel="03" />
                                <apex:selectOption itemValue="04" itemLabel="04" />
                                <apex:selectOption itemValue="05" itemLabel="05" />
                                <apex:selectOption itemValue="06" itemLabel="06" />
                                <apex:selectOption itemValue="07" itemLabel="07" />
                                <apex:selectOption itemValue="08" itemLabel="08" />
                                <apex:selectOption itemValue="09" itemLabel="09" />
                                <apex:selectOption itemValue="10" itemLabel="10" />
                                <apex:selectOption itemValue="11" itemLabel="11" />
                                <apex:selectOption itemValue="12" itemLabel="12" />
                            </apex:selectList>

                            <apex:selectList value="{!Payment.NU__CreditCardExpirationYear__c}"
                                             size="1"
                                             id="ccexpyear"
                                             styleClass="ccexpyear">
                                <apex:selectOptions value="{!ExpirationYearOptions}" />
                            </apex:selectList>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem id="CSC" rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && (Controller.Cart.NU__Balance__c > 0 || CreditCardPaymentType == 'Pay')}">
                        <apex:outputPanel >
                            <span id="csc" style="position:relative">CSC
                                <span src="/s.gif" alt="" class="customHelpOrb" title="" onmouseover="$('#csc .customHelpText').show()" onmouseout="$('#csc .customHelpText').hide()" />
                                <span class="customHelpText cscHelp">
                                    <h3>Where to find your credit card security/authorization code:</h3>
                                    <p><img src="{!URLFOR($Resource.CCVN, 'visa_mc_ccvn.jpeg')}"/></p>
                                    <p><img src="{!URLFOR($Resource.CCVN, 'amex_ccvn.jpeg')}"/></p>
                                </span>
                            </span>
                        </apex:outputPanel>

                        <apex:outputPanel >
                            <c:Required rendered="{!SelectedPaymentMethod.Entity__r.NU__RequireCreditCardSecurityCode__c == true}">
                                <apex:inputField id="cscTB"
                                       label="CSC"
                                       value="{!Payment.NU__CreditCardSecurityCode__c}"
                                       styleClass="creditSecurityCode" />
                            </c:Required>

                            <apex:inputField id="cscTB"
                                       label="CSC"
                                       value="{!Payment.NU__CreditCardSecurityCode__c}"
                                       rendered="{!SelectedPaymentMethod.Entity__r.NU__RequireCreditCardSecurityCode__c == false}"
                                       styleClass="creditSecurityCode" />
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:inputField label="Cardholder Name" value="{!Payment.NU__CreditCardName__c}" rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && (Controller.Cart.NU__Balance__c > 0 || CreditCardPaymentType == 'Pay')}" />
                    <!-- Payment Note section -->
                    <apex:pageBlockSectionItem rendered="{!Payment.NU__EntityPaymentMethod__c != ''}">
                        <apex:outputPanel >
                            <apex:outputText value="{!$ObjectType.NU__CartPayment__c.Fields.NU__Note__c.Label}" />
                        </apex:outputPanel>
                        <apex:outputPanel >
                            <span id="orderPmtNoteSpan">
                            <apex:inputField value="{!Payment.NU__Note__c}"
                                             id="orderPmtNote"
                                             style="height: 100px; width: 150px;" />
                             </span>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <apex:pageBlockSection id="orderPmtCCAddressCol" columns="1" rendered="{!SelectedPaymentMethod.PaymentMethod__r.RecordType.Name == 'Credit Card' && Payment.NU__EntityPaymentMethod__c != '' && SelectedPaymentMethod.Entity__r.NU__RequireCreditCardAddress__c && CreditCardPaymentType == 'Pay'}">
                    <apex:pageBlockSectionItem />
                    <apex:pageBlockSectionItem />
                    <apex:pageBlockSectionItem />
                    <apex:inputField label="Street" value="{!Payment.NU__CreditCardStreet__c}" />
                    <apex:inputField label="City" value="{!Payment.NU__CreditCardCity__c}" />
                    <apex:inputField label="State" value="{!Payment.NU__CreditCardState__c}" />
                    <apex:inputField label="Postal Code" value="{!Payment.NU__CreditCardPostalCode__c}" />
                    <apex:inputField label="Country" value="{!Payment.NU__CreditCardCountry__c}" />
                </apex:pageBlockSection>
            </apex:pageBlock>

            <apex:outputPanel id="ApplyPayment">
                <apex:pageBlock mode="maindetail" title="Apply Payment" rendered="{!ShowPaymentSplit}">
                    <apex:pageBlockTable value="{!SplitPayments}" var="p">
                        <apex:column value="{!p.CartItem__r.NU__Customer__c}"/>
                        <apex:column value="{!p.CartItem__r.RecordType.Name}">
                            <apex:facet name="header">Order Type</apex:facet>
                        </apex:column>
                        <apex:column value="{!p.CartItem__r.NU__Balance__c}" styleClass="number" headerClass="number" footerClass="number totalFooter">
                            <apex:facet name="footer">
                                <apex:outputText value="{0,number,$0.00}" styleClass="inputPrice">
                                    <apex:param value="{!SelectedCartItemWrapperBalance}"/>
                                </apex:outputText>
                            </apex:facet>
                        </apex:column>
                        <apex:column styleClass="number" headerClass="number" footerClass="number totalFooter">
                            <apex:facet name="header">Payment</apex:facet>
                            <div style="display: inline-block">
                                <c:Required ><apex:inputField value="{!p.NU__PaymentAmount__c}" styleClass="inputPrice paymentAmt" required="false" /></c:Required>
                            </div>
                            <apex:facet name="footer">
                                <span class="paymentSum"/>
                            </apex:facet>
                        </apex:column>
                    </apex:pageBlockTable>
                </apex:pageBlock>
            </apex:outputPanel>

            <div class="buttons" style="clear: both;">
                   <apex:commandButton value="Save" action="{!savePayment}" styleClass="green" rerender="PaymentDialogPanel, cartItems, payments, recurringPayments" status="AS" />
                <apex:commandButton value="Cancel" action="{!cancelPayment}" />
            </div>

            <script type="text/javascript">
                $(".paymentAmt").keyup(CalculatePaymentTotal).blur(CalculatePaymentTotal);
                function CalculatePaymentTotal() {
                    var total = 0;
                    $(".paymentAmt[value!='']").each(function() {
                        total += parseFloat($(this).val());
                    });
                    $(".paymentSum").text(isNaN(total) ? '' : '$' + total.toFixed(2));
                }

                var creditCardIssuers = new Array();

                <apex:repeat value="{!CreditCardIssuers}" var="cci">
                    creditCardIssuers.push(
                        { name: "{!cci.CreditCardIssuer__r.Name}",
                          regex: "{!cci.CreditCardIssuer__r.NU__CardNumberIdentificationRegEx__c}",
                          imageURL: "{!cci.CreditCardIssuer__r.NU__ImageStaticResourceName__c}" }
                    );
                </apex:repeat>

                function getCreditCardImg(imgSrc){
                    var ccImageDiv = document.getElementById('ccImageDiv');
                    var ccImages = ccImageDiv.childNodes;

                    for (var i=0; i < ccImages.length; ++i){
                        var ccImg = ccImages[i];

                        if (ccImg.src && ccImg.src.indexOf(imgSrc) > -1){
                            return ccImg;
                        }
                    }

                    return null;
                }

                function updateCreditCardIssuerDisplay(creditCardNumberTextBox){
                    if (!creditCardNumberTextBox) { return; }
                    var cciLength = creditCardIssuers.length;

                    for (var cciIndex = 0; cciIndex < cciLength; ++cciIndex){
                        var creditCardIssuer = creditCardIssuers[cciIndex];
                        var creditCardRegEx = new RegExp(creditCardIssuer.regex, "i");
                        var creditCardImg = getCreditCardImg(creditCardIssuer.imageURL);

                        if (creditCardRegEx.test(creditCardNumberTextBox.value)){
                            creditCardImg.className = 'selectedPmtImg';
                        }
                        else{
                            creditCardImg.className = 'unselectedPmtImg';
                        }
                    }
                }

                var cscTextBox = document.getElementById("{!$Component.pmtForm.orderPmtFormInputs.cscTB}");

                if (cscTextBox) { cscTextBox.maxLength = 4; }

                var orderPmtNoteTB = document.getElementById("{!$Component.pmtForm.orderPmtFormInputs.orderPmtNote}");

                if (orderPmtNoteTB) { orderPmtNoteTb.className = 'orderPmtNoteTB';}

                if ({!showCCAddress}){
                    var orderPmtFormInputsDiv = document.getElementById("{!$Component.pmtForm.orderPmtFormInputs}");

                    if (orderPmtFormInputsDiv) { orderPmtFormInputsDiv.className = 'orderPmtFormInputs'; }

                    var orderPmtCCAddressColDiv = document.getElementById("{!$Component.pmtForm.orderPmtCCAddressCol}");

                    if (orderPmtCCAddressColDiv) { orderPmtCCAddressColDiv.className = 'orderPmtCCAddressCol'; }
                }

            </script>​

        </apex:outputPanel>
    </c:CustomDialog>

    <c:CustomDialog id="VoidCC" title="Void Credit Card Payment" closeable="false">
        <apex:outputPanel id="VoidCCDialogPanel">
            <apex:pageMessages />
            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!VoidCCDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:pageBlock mode="maindetail">
                <apex:pageBlockSection columns="1">
                    <apex:inputField value="{! Payment.Note__c }" />
                </apex:pageBlockSection>
            </apex:pageBlock>

            <div class="buttons" style="clear: both;">
                <apex:commandButton value="Void" action="{!voidCreditCardCartPayment}" styleClass="green" rerender="VoidCCDialogPanel, cartItems, payments" status="AS" />
                <apex:commandButton value="Cancel" action="{!cancelVoid}" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>

    <c:CustomDialog id="TransferPayments" title="Transfer Payments" closeable="false">
        <apex:outputPanel id="TransferPaymentsDialogPanel">
            <apex:pageMessages />
            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!TransferPaymentsDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:pageBlock mode="maindetail">
                <apex:pageBlockSection columns="1">
                    <apex:outputText value="Are you sure that you want to transfer payments?" rendered="{!isValidSelectionForTransferPayment}" />
                </apex:pageBlockSection>
            </apex:pageBlock>

            <div class="buttons" style="clear: both;">
                   <apex:commandButton value="Transfer" action="{!processTransfer}" styleClass="green" rendered="{!isValidSelectionForTransferPayment}" rerender="TransferPaymentsDialogPanel, cartItems, payments" status="AS" />
                <apex:commandButton value="Cancel" action="{!cancelTransfer}" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>
</apex:component>