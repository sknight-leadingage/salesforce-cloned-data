<apex:component controller="NU.OrderPurchaseStore" allowDML="true">

    <apex:attribute name="c" type="NU.OrderController" description="OrderController" assignTo="{!Controller}"/>

    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd();NUBind()" id="ASBind"/>
    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd()" id="AS"/>

    <apex:actionRegion >

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/box32.png)"/>
                <h1 class="pageType">{!c.Subheader}&nbsp;</h1>
                <h2 class="pageDescription">{!IF(IsEDIT, 'Edit', 'Add')} Merchandise</h2>
            </div>
        </div>
    </div>

    <apex:pageMessages escape="{!c.EscapeMessages}" id="Msgs" />

    <apex:pageBlock mode="maindetail" id="Inputs">

        <apex:pageBlockSection columns="1">
            <apex:inputField value="{!Controller.CurrentCartItem.NU__Customer__c}" rendered="{!!IsEdit}" id="Account">
                <apex:actionSupport event="onchange" action="{!OnAccountSelected}" rerender="Msgs,Inputs,ItemLines,Buttons" status="ASBind"/>
            </apex:inputField>
            <apex:outputField value="{!Controller.CurrentCartItem.NU__Customer__c}" rendered="{!IsEdit}"/>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && !IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__Entity__c}">
                    <apex:selectOptions value="{!EntityOptions}"/>
                    <apex:actionSupport event="onchange" onsubmit="if(!confirmEntitySwitch()) { this.value = '{!Controller.CurrentCartItem.NU__Entity__c}'; return false; }" action="{!OnEntitySelected}" rerender="Msgs,Inputs" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:outputField value="{!Controller.CurrentCartItem.NU__Entity__c}"/>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null}" id="PriceClass">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__PriceClass__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__PriceClass__c}">
                    <apex:selectOption itemvalue="" itemLabel="Please select a price class" rendered="{!Controller.CurrentCartItem.NU__PriceClass__c == null}"/>
                    <apex:selectOptions value="{!PriceClassOptions}"/>
                    <apex:actionSupport event="onchange" action="{!OnPriceClassSelected}" rerender="Msgs,ItemLines,Buttons" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Merchandise" columns="1" rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Controller.CurrentCartItem.NU__PriceClass__c != null}">
            <apex:pageBlockTable value="{!ProductItemLines}" var="cil" id="ItemLines" >
                <apex:column >
                    <apex:facet name="header">Purchase</apex:facet>
                    <apex:inputField value="{!cil.NU__IsInCart__c}" styleClass="{!IF(cil.Id == null && cil.NU__OrderItemLine__c == null,'selectableRow','unselectableRow')}"
                            rendered="{! cil.Product2__r.TrackInventory__c == false || cil.Product2__r.InventoryOnHand__c > 0 || cil.Id != null || AllowMerchandiseBackordering }"/>
                </apex:column>
                <apex:column value="{!cil.NU__Product2__c}"/>
                <apex:column headerClass="number" styleClass="number">
                    <apex:facet name="header">Inventory</apex:facet>
                    <apex:outputField value="{!cil.Product2__r.NU__InventoryOnHand__c}" rendered="{!cil.Product2__r.NU__TrackInventory__c}" />
                    <apex:outputText rendered="{!!cil.Product2__r.NU__TrackInventory__c}" />
                </apex:column>
                <apex:column styleClass="number" headerClass="number">
                    <apex:facet name="header"><apex:outputText value="{! $ObjectType.OrderItemLine__c.Fields.UnitPrice__c.Label }" /></apex:facet>
                    <div style="display: inline-block">
                        <c:Required rendered="{! cil.Product2__r.TrackInventory__c == false || cil.Product2__r.InventoryOnHand__c > 0 || cil.Id != null || AllowMerchandiseBackordering }">
                            <apex:inputField value="{!cil.NU__UnitPrice__c}"
                                             styleClass="inputPrice"
                                             required="false"
                                             onkeyup="calcPrice(this)"
                                             onblur="calcPrice(this)"
                                             onfocus="this.maxLength = {! UnitPriceMaxLength };" />
                        </c:Required>
                        <apex:outputField value="{!cil.NU__UnitPrice__c}"
                                rendered="{! cil.Product2__r.TrackInventory__c && cil.Product2__r.InventoryOnHand__c == 0 && cil.Id == null && !AllowMerchandiseBackordering }" />
                    </div>
                </apex:column>
                <apex:column styleClass="number" headerClass="number">
                    <apex:facet name="header"><apex:outputText value="{! $ObjectType.OrderItemLine__c.Fields.Quantity__c.Label }" /></apex:facet>
                    <div style="display: inline-block">
                        <c:Required rendered="{! cil.Product2__r.TrackInventory__c == false || cil.Product2__r.InventoryOnHand__c > 0 || cil.Id != null || AllowMerchandiseBackordering }">
                            <apex:inputField value="{!cil.NU__Quantity__c}"
                                             styleClass="inputQuantity"
                                             required="false"
                                             onkeyup="calcPrice(this)"
                                             onblur="calcPrice(this)"
                                             onfocus="this.maxLength = {! QuantityMaxLength };" />
                         </c:Required>
                         <apex:outputField value="{!cil.NU__Quantity__c}"
                                  rendered="{! cil.Product2__r.TrackInventory__c && cil.Product2__r.InventoryOnHand__c == 0 && cil.Id == null && !AllowMerchandiseBackordering }" />
                     </div>
                </apex:column>
                <apex:column styleClass="number priceCOL" headerClass="number priceCOL">
                    <apex:facet name="header">Price</apex:facet>
                    <span class="outputPrice">
                        <apex:outputText value="{0,number,$#,##0.00}">
                            <apex:param value="{!cil.NU__UnitPrice__c * cil.NU__Quantity__c}"/>
                        </apex:outputText>
                    </span>
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlockSection>

        <apex:pageBlockButtons location="both" id="Buttons">
            <apex:commandButton value="Save" styleClass="green" action="{!Save}" rerender="Msgs" rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Controller.CurrentCartItem.NU__PriceClass__c != null}" status="AS"/>
            <apex:commandButton value="Cancel" action="{!Cancel}" rerender="Msgs,Main" status="AS"/>
        </apex:pageBlockButtons>
    </apex:pageBlock>

    </apex:actionRegion>

</apex:component>