<apex:page controller="ConferenceProposalsController">
<script>

function setFocusOnLoad() {}

function copyToClipboard (text) {
  window.prompt ("To copy to clipboard press Ctrl+C, Enter:", text);
}

function domClip(ctrl) {
    var skillsSelect = document.getElementById(ctrl);
    if (skillsSelect != null && skillsSelect.type != null && (skillsSelect.type == 'select-one' || skillsSelect.type == 'select-multiple')) {
        var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
        copyToClipboard(selectedText);
    }
}

</script>

    <style>
        .pbTitle {
            width: 100% !important;
        }
        
        .pbSubheader {
            color: black !important; 
        }
        
        .sameTextBoxWidth {
            width: 95%;
        }
        
        .sameTextBoxHeightSmall {
            height: 32px;
        }
        
        .sameTextBoxHeight {
            
            height: 100px;
        }
    </style>

    <apex:pageMessages />
    
    <apex:pageMessage summary="{! Conference.Name } has no proposals."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria == false }" />
    
    <apex:pageMessage summary="No proposals found."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria }" />                  
    
    
    <apex:form id="ConfernceProposalsForm">
        <apex:pageBlock title="{! Conference.Name } Proposals : {! CurrentProposal.Name }">
            <div style="clear: both; height: 40px;">
                <apex:panelGrid columns="8" rendered="{! ResultSize > 0 }" style="float: left">
                    <apex:commandLink action="{!first}" rendered="{! ResultSize > 1 && PageNumber != 1 }">First</apex:commandlink>
                    <apex:commandLink action="{!previous}" rendered="{!hasPrevious}">Previous</apex:commandlink>
                    <apex:outputText value="Record {! PageNumber } of {! ResultSize }" />
                    <apex:commandLink action="{!next}" rendered="{!hasNext}">Next</apex:commandlink>
                    <apex:commandLink action="{!last}" rendered="{! ResultSize > 1 && PageNumber != ResultSize }">Last</apex:commandlink>
                    <apex:outputText value=" &nbsp;&nbsp;&nbsp; " escape="false"/>
                    <apex:inputText value="{!RecordNum}" label="Record #" style="width: 25px;" rendered="{!ResultSize > 1}"/>
                    <apex:commandButton action="{!GoToRecord}" rendered="{!ResultSize > 1}" value="GoTo Record"/>
                </apex:panelGrid>
            
           
                <div style="float: right;">
                    <c:ConferenceNavComponent eventId="{! EventId }" />
                </div>
            </div>
            
            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>
            </div>
            
            <apex:pageBlockSection title="Submitter Information" rendered="{! ResultSize > 0 }">

                <apex:outputField value="{! CurrentProposal.Submitter__r.Account__r.NU__FullName__c}" />
                <apex:outputField value="{! CurrentProposal.Submitter__r.Account__r.PersonTitle}" />
                <apex:outputField value="{! CurrentProposal.Submitter__r.Company_Name_Rollup__c}" />
                <apex:outputField value="{! CurrentProposal.Submitter__r.Account__r.Speaker_Full_Address__c}" />
                <apex:outputField value="{! CurrentProposal.Submitter__r.Account__r.Phone}" />
                <apex:outputField value="{! CurrentProposal.Submitter__r.Account__r.NU__PersonEmail__c}" />
                <apex:outputField value="{! CurrentProposal.Submitter__r.Account__r.LeadingAge_ID__c}" />

            </apex:pageBlockSection>

            <apex:pageBlockSection columns="1"
                                   id="pbs"
                                   title="Proposal Information"
                                   rendered="{! ResultSize > 0 }">
            
                <apex:outputField value="{! CurrentProposal.Proposal_Number__c }" />
                <apex:outputText value="{! IF (CurrentProposal.Session__c == null, 'No', 'Yes' ) }" label="Promoted to Session" />
                <apex:inputField value="{! CurrentProposal.Title__c }" styleClass="sameTextBoxWidth" />
                <apex:outputText value="{! CurrentProposal.Session__r.Session_Title_Full__c }" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Track" />
                    <apex:outputPanel >
                        <apex:selectList id="ConferenceTracksSelectList"
                                     multiselect="false"
                                     size="1"
                                     value="{! CurrentProposal.Conference_Track__c }">
                            <apex:selectOption itemLabel="Please select a track" itemValue="" />
                            <apex:selectOptions value="{! AvailableTracksSelectOpts }"/>
                        </apex:selectList>
                        &nbsp; <apex:commandButton value="Copy" id="jscopy1" onclick="domClip('{!$Component.ConferenceTracksSelectList}')"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:inputField value="{! CurrentProposal.Session_Length__c }" />
                <apex:inputField value="{! CurrentProposal.Sponsorship_Level__c }" />
                <apex:inputField value="{! CurrentProposal.Status__c }" />
                <apex:inputField value="{! CurrentProposal.Notes__c }" style="width: 350px" />
            </apex:pageBlockSection>

<!--
            <apex:pageBlockSection columns="1"
                                   id="pbs"
                                   title="Proposal Information"
                                   rendered="{! ResultSize > 0 }">
            
                <apex:inputField value="{! CurrentProposal.Title__c }"
                                 styleClass="sameTextBoxWidth" />
                
                <apex:outputText value="{! CurrentProposal.Session__r.Session_Title_Full__c }" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Track" />
                    <apex:selectList id="ConferenceTracksSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentProposal.Conference_Track__c }">
                        <apex:selectOption itemLabel="Please select a track" itemValue="" />
                        <apex:selectOptions value="{! AvailableTracksSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
                
                <apex:outputField value="{! CurrentProposal.Proposal_Number__c }" />
                
                <apex:inputField value="{! CurrentProposal.Session_Length__c }" />
                
                <apex:outputText value="{! IF (CurrentProposal.Session__c == null, 'No', 'Yes' ) }" label="Promoted to Session" />
                
                <apex:inputField value="{! CurrentProposal.Sponsorship_Level__c }" />
                
                <apex:pageBlockSectionItem />
                
                <apex:inputField value="{! CurrentProposal.Status__c }" />
                
                <apex:pageBlockSectionItem />
                
                <apex:inputField value="{! CurrentProposal.Notes__c }" style="width: 350px" />
                
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>
-->

            <apex:pageBlockSection columns="1" title="Speakers" rendered="{! ResultSize > 0 }" id="ShowSpeakers">
                <apex:pageBlockTable value="{! CurrentProposal.Conference_Proposal_Speakers__r }"
                                     var="propSpeaker">

                    <apex:column headerValue="Speaker Full Name">
                      <apex:outputLink value="/apex/EduLinkSpeaker?eventId={! EventId }&ps={! propSpeaker.Id }&jmp={! PageNumber }&title={! URLENCODE(SearchCriteria.Title) }&fn={! URLENCODE( SearchCriteria.FirstName  ) }&sn={! URLENCODE( SearchCriteria.LastName  ) }&cmpy={! URLENCODE( SearchCriteria.Company  ) }&trck={! URLENCODE( SearchCriteria.TrackName  ) }&lrno={! URLENCODE( SearchCriteria.LearningObjectivesPhrase  ) }&notes={! URLENCODE( SearchCriteria.NotesPhrase  ) }&sta={! URLENCODE( SearchCriteria.Status  ) }">{! IF(ISBLANK(propSpeaker.Speaker_Full_Name__c), '<name missing>', propSpeaker.Speaker_Full_Name__c) }</apex:outputLink>
                    </apex:column>

                    <apex:column headerValue="Speaker Full Name">
                      <apex:outputtext style="cursor: pointer;" value="{! propSpeaker.Speaker_Full_Name__c }" />
                      <apex:actionSupport action="{!selectItem}" event="onclick" rerender="ShowSpeakers">
                          <apex:param name="selAccId" value="{!propSpeaker.Id}" assignTo="{!selectedSpeakerId}" />
                      </apex:actionSupport>
                    </apex:column>

                    <apex:column headerValue="Affiliation">

                        <apex:outputText value="{! propSpeaker.Speaker__r.Title__c }" />,&nbsp;
                        <apex:outputText value="{! propSpeaker.Speaker__r.Company_1__c }" />,&nbsp;
                        <apex:outputText value="{! propSpeaker.Speaker__r.City__c }" />,&nbsp;
                        <apex:outputText value="{! propSpeaker.Speaker__r.State__c }" />,&nbsp;
                        <apex:outputText value="{! propSpeaker.Speaker__r.Country__c }" />
                        
                    </apex:column>
                    
                    <apex:column headerValue="Speaker Account Lookup Set">
                        <apex:outputText value="{! IF(propSpeaker.Speaker__r.Account__c == NULL, 'No', 'Yes (' + propSpeaker.Speaker__r.Account__r.LeadingAge_ID__c + ')') }" />
                    </apex:column>
                </apex:pageBlockTable>              
            </apex:pageBlockSection>
            
            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/> &nbsp; 
            <apex:commandButton action="{! ConvertToSession }"
                                value="Convert To Session"
                                style="margin-left: 10px;"
                                rendered="{! CurrentProposal.Session__c == null && ResultSize > 0 }" />  &nbsp; 
            <apex:commandButton action="{! deleteCurrentConferenceObject }"
                                    value="Delete"
                                    rendered="{! $ObjectType.Conference_Proposal__c.Deletable  && ResultSize > 0 }"
                                    onclick="return confirm('Are you sure?');"
                                    style="margin-left: 10px;" />
            </div>
        </apex:pageBlock>
        
        <apex:pageBlock title="Track Changes"
                        rendered="{! ResultSize > 0 }">
            <apex:outputText value="No track changes have been made." rendered="{! TrackChangesCount == 0 }" />
        
            <apex:pageBlockTable value="{! TrackChanges }"
                                 var="TrackChange"
                                 rendered="{! TrackChangesCount > 0 }">
                <apex:column value="{! TrackChange.OldValue }" />
                <apex:column value="{! TrackChange.NewValue }" />
                <apex:column value="{! TrackChange.CreatedDate }" />
                <apex:column value="{! TrackChange.CreatedById }" />
            </apex:pageBlockTable>
        </apex:pageBlock>
            
        <apex:pageBlock title="Status Changes"
                        rendered="{! ResultSize > 0 }">
            <apex:outputText value="No status changes have been made." rendered="{! StatusChangesCount == 0 }" />
        
            <apex:pageBlockTable value="{! StatusChanges }"
                                 var="StatusChange"
                                 rendered="{! StatusChangesCount > 0 }">
                <apex:column value="{! StatusChange.OldValue }" />
                <apex:column value="{! StatusChange.NewValue }" />
                <apex:column value="{! StatusChange.CreatedDate }" />
                <apex:column value="{! StatusChange.CreatedById }" />
            </apex:pageBlockTable>
        </apex:pageBlock>
        
        <apex:pageblock title="Search" rendered="{! ResultSize > 0 }">
            <apex:pageBlockSection title="Search" columns="2">       
                <apex:inputText value="{! SearchCriteria.ProposalNumber }" label="Proposal Number" />
                <apex:inputText value="{! SearchCriteria.Title }" label="Proposal Title" />
                
                <apex:inputText value="{! SearchCriteria.FirstName }" label="{! $ObjectType.Conference_Speaker__c.Fields.First_Name__c.Label }" />
                <apex:inputText value="{! SearchCriteria.LastName }" label="{! $ObjectType.Conference_Speaker__c.Fields.Last_Name__c.Label }" />
                
                <apex:inputText value="{! SearchCriteria.Company }" label="Company" />
                <apex:inputText value="{! SearchCriteria.TrackName }" label="{! $ObjectType.Conference_Track__c.Fields.Name.Label }" />

                <apex:inputText value="{! SearchCriteria.LearningObjectivesPhrase }" label="Learning Objectives Phrase" />
                <apex:inputText value="{! SearchCriteria.NotesPhrase }" label="Notes Phrase" />

                <apex:selectList value="{!SearchCriteria.Status}" multiselect="false" size="1" label="Status">
                    <apex:selectOption itemValue="" itemLabel="(All)" />
                    <apex:selectOption itemValue="Accepted" itemLabel="Accepted" />
                    <apex:selectOption itemValue="Maybe" itemLabel="Maybe" />
                    <apex:selectOption itemValue="Rejected" itemLabel="Rejected" />
                </apex:selectList>
                <apex:inputText value="{! SearchCriteria.LeadingAgeId }" label="Speaker's {! $ObjectType.Account.Fields.LeadingAge_Id__c.Label }" />

                <apex:pageBlockSectionItem />

                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:commandButton value="Search" action="{! search }" />
                        <apex:commandButton value="Clear Search"
                                            action="{! clearSearch }"
                                            style="margin-left: 10px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageblock>
    </apex:form>
    
    
</apex:page>