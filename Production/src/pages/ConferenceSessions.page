<apex:page controller="ConferenceSessionsController">
<script>

function setFocusOnLoad() {}

function copyToClipboard (text) {
  window.prompt ("To copy to clipboard press Ctrl+C, Enter:", text);
}

function domClip(ctrl) {
    var skillsSelect = document.getElementById(ctrl);
    if (skillsSelect != null && skillsSelect.type != null && (skillsSelect.type == 'select-one' || skillsSelect.type == 'select-multiple')) {
        var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
        copyToClipboard(selectedText);
    }
}

</script>

    <style>
        .pbTitle {
            width: 100% !important;
        }
        
        .pbSubheader {
            color: black !important; 
        }
        
        .sameTextBoxWidth {
            width: 95%;
        }
        
        .sameTextBoxHeight {
            
            height: 100px;
        }
    </style>

    <apex:pageMessages />

    <apex:pageMessage summary="{! Conference.Name } has no sessions."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria == false }" />
    
    <apex:pageMessage summary="No sessions found."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria }" />
                      
    <apex:form id="ConferenceSessionsForm">
        <apex:pageBlock title="{! Conference.Name } Session : {! CurrentSession.Name }">
        
            <div style="clear: both; height: 40px;">
                <apex:panelGrid columns="8" rendered="{! ResultSize > 0 }" style="float: left">
                    <apex:commandLink action="{!first}" rendered="{! ResultSize > 1 && PageNumber != 1 }">First</apex:commandlink>
                    <apex:commandLink action="{!previous}" rendered="{!hasPrevious}">Previous</apex:commandlink>
                    <apex:outputText value="Record {! PageNumber } of {! ResultSize }" />
                    <apex:commandLink action="{!next}" rendered="{!hasNext}">Next</apex:commandlink>
                    <apex:commandLink action="{!last}" rendered="{! ResultSize > 1 && PageNumber != ResultSize }">Last</apex:commandlink>
                    <apex:outputText value=" &nbsp;&nbsp;&nbsp; " escape="false"/>
                    <apex:inputText value="{!RecordNum}" label="Record #" style="width: 25px;" rendered="{!ResultSize > 1}"/>
                    <apex:commandButton action="{!GoToRecord}" rendered="{!ResultSize > 1}" value="GoTo Record"/>
                </apex:panelGrid>
            
           
                <div style="float: right;">
                    <c:ConferenceNavComponent eventId="{! EventId }" />
                </div>
            </div>

            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; 
                <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; 
                <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>
            </div>
            
            <apex:pageBlockSection columns="2" title="Scheduling" rendered="{! ResultSize > 0 }">

                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Timeslot__c.Label }" />
                    <apex:outputPanel >
                        <apex:selectList id="TimeslotSelectList"
                                     multiselect="false"
                                     size="1"
                                     value="{! CurrentSession.Timeslot__c }"
                                     rendered="{! CurrentSession.Locked__c == false }">
                            <apex:selectOption itemLabel="Please select a {! $ObjectType.Conference_Session__c.Fields.Timeslot__c.Label }" itemValue="" />
                            <apex:selectOptions value="{! ConferenceTimeslotSelectOpts }"/>
                        </apex:selectList>
                        <apex:outputText value="{! SelectedTimeSlotDisplay }" rendered="{! CurrentSession.Locked__c == true}" />
                        &nbsp; <apex:commandButton value="Copy" id="jscopy1" onclick="domClip('{!$Component.TimeslotSelectList}')" rendered="{! CurrentSession.Locked__c == false }"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Session_Number__c.Label }" />
                    
                    <apex:outputPanel >
                        <apex:inputField value="{! CurrentSession.Session_Number__c }"
                                         rendered="{! CurrentSession.Locked__c == false }" />
                        <apex:outputField value="{! CurrentSession.Session_Number__c }"
                                          rendered="{! CurrentSession.Locked__c == true }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <apex:outputField value="{! CurrentSession.Session_Length__c }" />

                <apex:pageBlockSectionItem >
                    <apex:outputText value="Track" />
                    <apex:outputPanel >
                        <apex:selectList id="ConferenceTracksSelectList"
                                     multiselect="false"
                                     size="1"
                                     value="{! CurrentSession.Conference_Track__c }"
                                     rendered="{! CurrentSession.Locked__c == false }">
                            <apex:selectOption itemLabel="Please select a track" itemValue="" />
                            <apex:selectOptions value="{! AvailableTracksSelectOpts }"/>
                        </apex:selectList>
                        &nbsp; <apex:commandButton value="Copy" id="jscopy2" onclick="domClip('{!$Component.ConferenceTracksSelectList}')" rendered="{! CurrentSession.Locked__c == false }"/>
                        <apex:outputField value="{! CurrentSession.Conference_Track__c }" rendered="{! CurrentSession.Locked__c == true }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>

            <apex:pageBlockSection columns="2" rendered="{! ResultSize > 0 }" title="Main Info">
            
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Title__c.Label }" />
                
                    <apex:outputPanel >
                        <apex:inputField value="{! CurrentSession.Title__c }"
                                         styleClass="sameTextBoxWidth"
                                         rendered="{! CurrentSession.Locked__c == false }" />
                                         
                        <apex:outputField value="{! CurrentSession.Title__c }"
                                          rendered="{! CurrentSession.Locked__c == true }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                    <apex:outputField value="{! CurrentSessionProposal.Proposal_Number__c }" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Learning_Objective_1__c.Label }" />
                    
                    <apex:outputPanel >
                        <apex:inputField value="{! CurrentSession.Learning_Objective_1__c }"
                                         styleclass="sameTextBoxWidth sameTextBoxHeight"
                                         rendered="{! CurrentSession.Locked__c == false }" />
                        <apex:outputField value="{! CurrentSession.Learning_Objective_1__c }"
                                         rendered="{! CurrentSession.Locked__c == true }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                    <apex:inputField value="{! CurrentSession.Sponsorship_Level__c }" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Learning_Objective_2__c.Label }" />
                    
                    <apex:outputPanel >
                        <apex:inputField value="{! CurrentSession.Learning_Objective_2__c }"
                                         styleclass="sameTextBoxWidth sameTextBoxHeight"
                                         rendered="{! CurrentSession.Locked__c == false }" />
                        <apex:outputField value="{! CurrentSession.Learning_Objective_2__c }"
                                         rendered="{! CurrentSession.Locked__c == true }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                    <apex:outputText value="" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Learning_Objective_3__c.Label }" />
                    
                    <apex:outputPanel >
                        <apex:inputField value="{! CurrentSession.Learning_Objective_3__c }"
                                         styleclass="sameTextBoxWidth sameTextBoxHeight"
                                         rendered="{! CurrentSession.Locked__c == false }" />
                        <apex:outputField value="{! CurrentSession.Learning_Objective_3__c }"
                                         rendered="{! CurrentSession.Locked__c == true }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                    <apex:outputText value="" />
                
                <apex:inputField value="{! CurrentSession.Notes__c }"
                                 styleClass="sameTextBoxWidth" />
                
            </apex:pageBlockSection>
           
            <apex:pageBlockSection title="Submitter" columns="2" rendered="{! ResultSize > 0 }">
                <apex:outputField value="{!CurrentSession.Submitter__r.FirstName}" />
                <apex:outputField value="{!CurrentSession.Submitter__r.Lastname}" />
                <apex:outputField value="{!CurrentSession.Submitter__r.Speaker_Company_Name_Rollup__c}" />
                <apex:outputField value="{!CurrentSession.Submitter__r.PersonTitle}" />
                <apex:outputField value="{!CurrentSession.Submitter__r.LeadingAge_ID__c}" />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Speakers" columns="1" rendered="{! ResultSize > 0 }" id="ShowSpeakers">
                <apex:outputText value="There are no speakers" rendered="{! CurrentSessionSpeakers.size == 0 }" />
                
                <apex:pageBlockTable value="{! CurrentSessionSpeakers }"
                                     var="SessionSpeaker"
                                     rendered="{! CurrentSessionSpeakers.size > 0 }"
                                     width="100%">
                                     
                    <apex:column headerValue="Remove" rendered="{!$ObjectType.Conference_Session_Speaker__c.Deletable}">
                        <apex:commandLink value="Remove Speaker" action="{! deleteSessionSpeaker }" onclick="return confirm('Are you sure you want to remove this speaker from selected session?');">
                            <apex:param name="q" value="{!SessionSpeaker.Id}" assignTo="{! SessionSpeakerIdToDelete}" />
                        </apex:commandLink>
                    </apex:column>
                                     
                    <apex:column headerValue="Speaker Full Name">
                      <apex:outputtext style="cursor: pointer;" value="{! SessionSpeaker.Speaker__r.NU__FullName__c }" />
                      <apex:actionSupport action="{!selectItem}" event="onclick" rerender="ShowSpeakers">
                          <apex:param name="selAccId" value="{!SessionSpeaker.Speaker__r.LeadingAge_ID__c}" assignTo="{!selectedSpeakerId}" />
                      </apex:actionSupport>
                    </apex:column>

                    <apex:column headerValue="Order">
                        <apex:inputField value="{! SessionSpeaker.Order__c }" style="width: 30px;" />
                    </apex:column>

                    <apex:column headerValue="Affiliation">
                        <apex:outputText value="{! SessionSpeaker.Speaker__r.PersonTitle }" />,&nbsp;
                        <apex:outputText value="{! SessionSpeaker.Speaker__r.Speaker_Company_Name_Rollup__c }" />,&nbsp;
                        <apex:outputText value="{! SessionSpeaker.Speaker__r.BillingCity }" />,&nbsp;
                        <apex:outputText value="{! SessionSpeaker.Speaker__r.BIllingState }" />,&nbsp;
                        <apex:outputText value="{! SessionSpeaker.Speaker__r.BillingCountry }" />
                        
                    </apex:column>
                    
                    <apex:column value="{! SessionSpeaker.Speaker__r.Leadingage_Id__c }" />
                    
                    <apex:column value="{! SessionSpeaker.Role__c }" />

                    <!--
                    <apex:column headerValue="Role">
                        <apex:inputField value="{! SessionSpeaker.Role__c }" />
                    </apex:column>
                    -->
                    
                    <!--
                    <apex:column value="{! SessionSpeaker.Overall_Average_Score__c }" />
                    <apex:column value="{! SessionSpeaker.Topic_Score__c }" />
                    <apex:column value="{! SessionSpeaker.Audience_Score__c }" />
                    <apex:column value="{! SessionSpeaker.Presentation_Score__c }" />
                    <apex:column value="{! SessionSpeaker.Info_Score__c }" />
                    -->
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            
            
            <apex:pageBlockSection columns="2" title="Internal" rendered="{! ResultSize > 0 }">
            
                <apex:inputField value="{! CurrentSession.Education_Staff__c }" />
                
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Division__c.Label }" />
                    
                    <apex:selectList id="DivisionSelectList"
                                 multiselect="false"
                                 size="1"
                                 value="{! CurrentSession.Division__c }">
                        <apex:selectOption itemLabel="None" itemValue="" />
                        <apex:selectOptions value="{! DivisionSelectOpts }"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
            
            </apex:pageBlockSection>
            
                        
            <apex:pageBlockSection title="Attendance" columns="2" rendered="{! ResultSize > 0 }">
                <apex:inputField value="{! CurrentSession.Expected_Attendance__c }"
                                 rendered="{! CurrentSession.Locked__c == false }" />
                <apex:outputField value="{! CurrentSession.Expected_Attendance__c }"
                                 rendered="{! CurrentSession.Locked__c == true }" />
                                 
                <apex:inputField value="{! CurrentSession.Actual_Attendance__c }"
                                 rendered="{! CurrentSession.Locked__c == false }" />
                <apex:outputField value="{! CurrentSession.Actual_Attendance__c }"
                                 rendered="{! CurrentSession.Locked__c == true }" />
            </apex:pageBlockSection>
            
            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>

                <apex:commandButton action="{! deleteCurrentConferenceObject }"
                                    value="Delete"
                                    rendered="{! $ObjectType.Conference_Session__c.Deletable && ResultSize > 0 }"
                                    onclick="return confirm('Are you sure?');"
                                    style="margin-left: 10px;" />
                                    
                <apex:commandButton action="{! lock }"
                                    value="Lock"
                                    rendered="{! CurrentSession.Locked__c == false && $ObjectType.Conference_Session__c.Fields.Locked__c.Updateable && ResultSize > 0 }"
                                    style="margin-left: 10px;" />
                <apex:commandButton action="{! unlock }"
                                    value="Unlock"
                                    rendered="{! CurrentSession.Locked__c == true && $ObjectType.Conference_Session__c.Fields.Locked__c.Updateable && ResultSize > 0 }"
                                    style="margin-left: 10px;" />
            </div>
        </apex:pageBlock>
        
        <apex:pageBlock title="Session ChangeLog">
            <apex:pageBlockSection title="Session ChangeLog"
                                   columns="1"
                                   rendered="{! ResultSize > 0 }">
                
                <apex:outputText value="No session changes have been made." rendered="{! SessionHistory.size == 0 }" />
        
                <apex:pageBlockTable value="{! SessionHistory }"
                                     var="sessionChange"
                                     rendered="{! SessionHistory.size > 0 }">
                    <apex:column value="{! sessionChange.Field }" />
                    <apex:column headerValue="{! $ObjectType.Conference_Session__History.Fields.OldValue.Label}">
                        <apex:outputField value="{! sessionChange.OldValue}" rendered="{! sessionChange.Field != 'Speaker_Change__c' && LEFT(sessionChange.Field, 18) != 'Learning_Objective' }" />
                    </apex:column>
                    <apex:column headerValue="{! $ObjectType.Conference_Session__History.Fields.NewValue.Label}">
                        <apex:outputField value="{! sessionChange.NewValue}" rendered="{! LEFT(sessionChange.Field, 18) != 'Learning_Objective' }" />
                        <apex:outputText value="was changed" rendered="{! LEFT(sessionChange.Field, 18) == 'Learning_Objective' }" />
                    </apex:column>
                    <apex:column value="{! sessionChange.CreatedDate }" />
                    <apex:column value="{! sessionChange.CreatedById }" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>

        <apex:pageBlock title="Search">
            <apex:pageBlockSection title="Search" columns="2">
                <apex:inputText value="{! SearchCriteria.SessionNumber }" label="{! $ObjectType.Conference_Session__c.Fields.Session_Number__c.Label }" />
                <apex:inputText value="{! SearchCriteria.Title }" label="Session {! $ObjectType.Account.Fields.PersonTitle.Label }" />
                
                <apex:inputText value="{! SearchCriteria.FirstName }" label="{! $ObjectType.Account.Fields.FirstName.Label }" />
                <apex:inputText value="{! SearchCriteria.LastName }" label="{! $ObjectType.Account.Fields.LastName.Label }" />
                
                <apex:inputText value="{! SearchCriteria.Company }" label="Company" />
                
                <apex:inputText value="{! SearchCriteria.TrackName }" label="{! $ObjectType.Conference_Track__c.Fields.Name.Label }" />
                
                <apex:inputText value="{! SearchCriteria.EducationStaffFirstName }" label="Education Staff First Name" />
                
                <apex:inputText value="{! SearchCriteria.EducationStaffLastName }" label="Education Staff Last Name" />
                
                <apex:inputText value="{! SearchCriteria.LearningObjectivesPhrase }" label="Learning Objectives Phrase" />
                
                <apex:inputText value="{! SearchCriteria.NotesPhrase }" label="Notes Phrase" />
                
                <apex:inputText value="{! SearchCriteria.ProposalNumber }" label="Proposal Number" />

                <apex:inputText value="{! SearchCriteria.LeadingAgeID }" label="Speaker's LeadingAge ID" />
                
                <apex:inputText value="{! SearchCriteria.TimeslotCode }" label="Timeslot Code" /> 
                
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                    <apex:commandButton value="Search" action="{! search }" />
                    <apex:commandButton value="Clear Search"
                                        action="{! clearSearch }"
                                        style="margin-left: 10px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
    </apex:form>
</apex:page>