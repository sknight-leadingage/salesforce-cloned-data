global class BatchHandleUnderConstruction implements Schedulable, Database.Batchable<sObject>  {
    global static void scheduleAt1AM() {
        System.schedule('Handle Under Construction', '0 0 1 * * ?', new BatchHandleUnderConstruction());
    }

   global Database.QueryLocator start(Database.BatchableContext BC)
   {
		return Database.getQueryLocator('SELECT Id, Provider_Membership__c, Provider_Membership__r.Id FROM Account WHERE Under_Construction__c = true AND Provider_Membership__c != null AND Provider_Membership__r.Under_Construction__c = false');
   }
   
    global void execute(SchedulableContext context) {
        // This exectue method is for the Schedulable call
        Database.executeBatch(new BatchHandleUnderConstruction());
    }
   
   global void execute(Database.BatchableContext BC,List<sObject> scope)
   {
   		// This execute method is for the Batabase.Batchable call
		List<Account> lAccs = (List<Account>)scope;
		Map<Id,NU__Membership__c> lUpdates = new Map<Id, NU__Membership__c>();      
		
		for(Account p : lAccs){
			if (!lUpdates.containsKey(p.Provider_Membership__r.Id)){
				lUpdates.put(p.Provider_Membership__r.Id, new NU__Membership__c(Id = p.Provider_Membership__r.Id, Under_Construction__c = true));
			}
			
		}
		
		if (!lUpdates.isEmpty()){
			update lUpdates.values();
		}
   }
   
   global void finish(Database.BatchableContext BC)
   {
   }
}