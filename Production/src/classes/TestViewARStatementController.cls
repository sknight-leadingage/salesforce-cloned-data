/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class TestViewARStatementController {
	
	static NU__Entity__c entity = null;
	
	static Account account = null;
	static Account account2 = null;
	
	static ViewARStatementController controller = null;
	
	static NU__Order__c anOrder = null;
	static NU__OrderItem__c orderItem = null;
	static NU__OrderItemLine__c orderItemLine = null;
	static NU__Product__c product = null;

	private static void setupTest() {
		entity = NU.DataFactoryEntity.insertEntity();		
		account = NU.DataFactoryAccount.insertOrganizationAccount();	
	}

    static testMethod void BasicTest() {
        setupTest();
        
        controller = new ViewARStatementController();        
        controller.Accounts = new List<Account>();
        
        basicAsserts(0, 0, Date.today());
    }
    
    static testMethod void AccountWithNoBalanceTest() {
        setupTest();
        
        controller = new ViewARStatementController();        
        controller.Accounts = new List<Account>{account};
        
        basicAsserts(1, 0, Date.today());
    }
    
    static testMethod void AccountWithBalanceTest() {
        setupTest();
        insertOrderWithBalance(entity.Id, account.Id, 100.0);
        
        controller = new ViewARStatementController(); 
        ARStatementUtil.entityId = entity.Id;       
        controller.Accounts = new List<Account>{account};
        
        basicAsserts(1, 1, Date.today());
        basicAccountWrapperAsserts(100.0);
    }
    
    static testMethod void AccountWithBalance_InvoiceDateOverride_Test() {
        setupTest();
        insertOrderWithBalance(entity.Id, account.Id, 100.0);
        
        controller = new ViewARStatementController(); 
        ARStatementUtil.entityId = entity.Id;  
        ARStatementUtil.invoiceDateOverride = Date.today().addDays(-1);     
        controller.Accounts = new List<Account>{account};
        
        basicAsserts(1, 1, Date.today().addDays(-1));
        basicAccountWrapperAsserts(100.0);
    }
    
    static testMethod void AccountWithBalance_MinimumBalanceOverride_Test() {
        setupTest();
        insertOrderWithBalance(entity.Id, account.Id, 100.0);
        
        controller = new ViewARStatementController(); 
        ARStatementUtil.entityId = entity.Id;  
        ARStatementUtil.minimumBalanceOverride = 101.0;   
        controller.Accounts = new List<Account>{account};
        
        basicAsserts(1, 0, Date.today());
    }
    
    static testMethod void AccountsWithBalanceTest() {
        setupTest();
        account2 = NU.DataFactoryAccount.insertOrganizationAccount();
        insertOrderWithBalance(entity.Id, account.Id, 100.0);
        insertOrderWithBalance(entity.Id, account2.Id, 100.0);
        
        controller = new ViewARStatementController(); 
        ARStatementUtil.entityId = entity.Id;       
        controller.Accounts = new List<Account>{account, account2};
        
        basicAsserts(2, 2, Date.today());
    }
    
    private static void basicAsserts(Integer accountsSize, Integer accountWrappersSize, Date invoiceDateOverride) {
    	// assert the size of the valid accounts passed in
    	System.assertEquals(accountsSize, controller.Accounts.size());
    	
    	// assert the size of the account wrappers
    	System.assertEquals(accountWrappersSize, controller.AccountWrappers.size());
    	
    	// assert the invoice date specified
    	System.assertEquals(invoiceDateOverride, controller.InvoiceDate);
    }
    
    private static void basicAccountWrapperAsserts(Decimal balance) {
    	// assert the Entity is set
    	System.assertEquals(entity.Id, controller.Entity.Id);
    	
    	// assert the account is set
    	System.assertEquals(account.Id, controller.AccountWrappers.get(0).account.Id);
    	
    	// assert that the balance is correct
    	System.assertEquals(balance, controller.AccountWrappers.get(0).balance);
    }
    
    public static void insertOrderWithBalance(Id entityId, Id accountId, Decimal price) {
    	product = NU.DataFactoryProduct.createDefaultProduct('Test Product', price, entityId);
    	insert product;
    	
    	orderItem = NU.DataFactoryOrderItem.insertOrderItem(entityId, accountId);
    	orderItemLine = new NU__OrderItemLine__c(NU__OrderItem__c = orderItem.Id, NU__Product__c = product.Id, NU__Quantity__c = 1, NU__UnitPrice__c = price);
    	insert orderItemLine;
    }
}