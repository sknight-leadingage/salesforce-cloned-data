public class MembershipTypeQuerier {
	public static NU__MembershipType__c getJointStateProviderMembershipType(){
		List<NU__MembershipType__c> mts =
		[select id,
		        name,
		        NU__Term__c,
		        NU__Entity__c,
		        NU__Entity__r.Id,
		        NU__Entity__r.NU__InvoiceTerm__c,
		        (Select id,
		                name,
		                NU__Product__c
		           from NU__MembershipTypeProductLinks__r
		          where NU__Purpose__c = :Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY)
		   from NU__MembershipType__c
		  where name = :Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME];
		
		if (mts.size() > 0){
			return mts[0];
		}
		
		return null;
	}
	
	public static Map<Id, NU__MembershipType__c> getAllMembershipTypesAndTheirActiveProductLinks(){
		return new Map<Id, NU__MembershipType__c>(
    		[select id,
    		        name,
    		        NU__Entity__c,
    		        (Select id,
    		                name,
    		                NU__Product__c
    		           from NU__MembershipTypeProductLinks__r
    		          where NU__Status__c = 'Active')
    		   from NU__MembershipType__c]);
	}
	
	public static List<NU__MembershipType__c> getProviderMembershipTypes(){
		String providerLikeStr = '%' + Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER + '%';
		
		return [select id,
		               name,
		               NU__Entity__c,
		               NU__Entity__r.Name
		          from NU__MembershipType__c
		         where Name like :providerLikeStr ];
	}
}