/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the provider membership flowdown logic.
 */
@isTest
private class TestProviderMembershipFlowdown {

	static void assertLapsedOnFlowdown(Date expectedLapsedOn, Set<Id> flowedDownLapsedOnAcctIds){
		system.assert(NU.CollectionUtil.setHasElements(flowedDownLapsedOnAcctIds), 'The flowed down lasped on account ids is null or empty');
		
		List<Account> flowedDownLapsedOnAccounts = [select id, Provider_Lapsed_On__c from Account where id in :flowedDownLapsedOnAcctIds];
		
		for (Account flowedDownLapsedOnAccount : flowedDownLapsedOnAccounts){
			system.assertEquals(expectedLapsedOn, flowedDownLapsedOnAccount.Provider_Lapsed_On__c);
		}
	}
	
	static void updateProviderLapsedOn(Account account, Date newProviderLapsedOn){
		account.Provider_Lapsed_On__c = Date.Today();
    	update account;
	}


    static testmethod void providerMembershipFlowDownOneLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownOneLevelTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

	static testmethod void membershipFlowdownOneLevelFromCompanyOverrideTest(){
		TestMembershipFlowdownUtil.membershipFlowdownOneLevelFromCompanyOverrideTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
	}
	
	static testmethod void noMembershipFlowdownFromCompanyToCompanyTest(){
		TestMembershipFlowdownUtil.noMembershipFlowdownFromCompanyToCompanyTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
	}

	static testmethod void membershipFlowdownOneLevelTestFromFlowdownCompanyMembershipBeingCheckedTest(){
		TestMembershipFlowdownUtil.membershipFlowdownOneLevelTestFromFlowdownCompanyMembershipBeingCheckedTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
	}
	
	static testmethod void membershipFlowdownTwoLevelTestFromFlowdownCompanyMembershipBeingCheckedTest(){
		
    	TestMembershipFlowdownUtil.membershipFlowdownTwoLevelTestFromFlowdownCompanyMembershipBeingCheckedTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
	}
	
	static testmethod void noLongerFlowdownCompanyMembershipOneLevelTest(){
		
		TestMembershipFlowdownUtil.noLongerFlowdownCompanyMembershipOneLevelTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
	}
	
	static testmethod void noLongerFlowdownCompanyMembershipTwoLevelsTest(){
		
		TestMembershipFlowdownUtil.noLongerFlowdownCompanyMembershipTwoLevelsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
	}

    static testmethod void membershipFlowDownTwoLevelsTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownTwoLevelsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipFlowDownOneLevelMultipleAcctsTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownOneLevelMultipleAcctsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipFlowDownTwoLevelsMultipleAcctsTest(){
    	
		TestMembershipFlowdownUtil.membershipFlowDownTwoLevelsMultipleAcctsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNullFlowDownOneLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipNullFlowDownOneLevelTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNullFlowDownTwoLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipNullFlowDownTwoLevelTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownFromPersonToPersonTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownFromPersonToPersonTest(
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownFromPersonToBusinessTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownFromPersonToBusinessTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownToNonPrimaryAffiliationsTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownToNonPrimaryAffiliationsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownWithIndividualMembershipTypeTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownToNonPrimaryAffiliationsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestFakePersonProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }
    
    static testmethod void newPrimaryAffiliationTest() {
    	
    	TestMembershipFlowdownUtil.newPrimaryAffiliationTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newPrimaryAffiliationFlowdownTwoLevelsTest(){
    	
    	TestMembershipFlowdownUtil.newPrimaryAffiliationFlowdownTwoLevelsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newIndivToIndivPrimaryAffilWithNoFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.newIndivToIndivPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newIndivToOrgPrimaryAffilWithNoFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.newIndivToOrgPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deletePrimaryAffilErasingMembershipFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.deletePrimaryAffilErasingMembershipFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deletePrimaryAffilErasingTwoMembershipFlowdownLevelsTest(){
    	
    	TestMembershipFlowdownUtil.deletePrimaryAffilErasingTwoMembershipFlowdownLevelsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deleteNonPrimaryAffilWithNoErasingMembershipFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.deleteNonPrimaryAffilWithNoErasingMembershipFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void changeAffiliationToPrimaryWithoutExistingFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.changeAffiliationToPrimaryWithoutExistingFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

	static testmethod void changeAffiliationToNonPrimaryWithExistingFlowdownTest(){
		
		TestMembershipFlowdownUtil.changeAffiliationToNonPrimaryWithExistingFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void lapsedOnFlowedDownOneLevelTest(){
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);

    	NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(providerAcct.Id);
    	
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(insertedMembership.Id, new Set<Id> { individualAcct.Id }, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	
    	updateProviderLapsedOn(providerAcct, Date.Today());
    	
    	assertLapsedOnFlowdown(providerAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id });
    }
    
    static testmethod void lapsedOnFlowedDownTwoLevelTest(){
    	Account multiSiteAcct = DataFactoryAccountExt.insertMultiSiteAccount();
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU__Affiliation__c affil = NU.DataFactoryAffiliation.insertAffiliation(providerAcct.Id, multiSiteAcct.Id, true);
    	TestMembershipFlowdownUtil.flowdownCompanyMembership(affil);
    	
    	NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);

    	NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(multiSiteAcct.Id);
    	
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(insertedMembership.Id, new Set<Id> { individualAcct.Id, providerAcct.Id }, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	
    	updateProviderLapsedOn(multiSiteAcct, Date.Today());
    	
    	assertLapsedOnFlowdown(multiSiteAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id, providerAcct.Id });
    }
    
    static testmethod void lapsedOnRevokedFlowedDownOneLevelTest(){
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);

    	NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(providerAcct.Id);
    	
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(insertedMembership.Id, new Set<Id> { individualAcct.Id }, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	
    	updateProviderLapsedOn(providerAcct, Date.Today());
    	
    	assertLapsedOnFlowdown(providerAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id });
    	
    	updateProviderLapsedOn(providerAcct, null);
    	
    	assertLapsedOnFlowdown(providerAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id });
    }
    
    static testmethod void lapsedOnRevokedFlowedDownTwoLevelTest(){
    	Account multiSiteAcct = DataFactoryAccountExt.insertMultiSiteAccount();
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU__Affiliation__c providerAffil = NU.DataFactoryAffiliation.insertAffiliation(providerAcct.Id, multiSiteAcct.Id, true);
    	TestMembershipFlowdownUtil.flowdownCompanyMembership(providerAffil);    	
    	
    	
    	NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);

    	NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(multiSiteAcct.Id);
    	
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(insertedMembership.Id, new Set<Id> { individualAcct.Id, providerAcct.Id }, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	
    	updateProviderLapsedOn(multiSiteAcct, Date.Today());
    	
    	assertLapsedOnFlowdown(multiSiteAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id, providerAcct.Id });
    	
    	updateProviderLapsedOn(multiSiteAcct, null);
    	
    	assertLapsedOnFlowdown(multiSiteAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id, providerAcct.Id });
    }

    static testmethod void lapsedOnRevokedOnNoLongerPrimaryAffiliationTest(){
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU__Affiliation__c primaryAffiliation = NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);

    	NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(providerAcct.Id);
    	
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(insertedMembership.Id, new Set<Id> { individualAcct.Id }, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	
    	updateProviderLapsedOn(providerAcct, Date.Today());
    	
    	assertLapsedOnFlowdown(providerAcct.Provider_Lapsed_On__c, new Set<Id>{ individualAcct.Id });
    	
    	primaryAffiliation.NU__IsPrimary__c = false;
    	update primaryAffiliation;
    	
    	assertLapsedOnFlowdown(null, new Set<Id>{ individualAcct.Id });
    }
    
    static testmethod void inactiveAffiliationCantBePrimaryTest(){
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU__Affiliation__c primaryAffiliation = NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);
    	
    	primaryAffiliation.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
    	update primaryAffiliation;
    	
    	primaryAffiliation.NU__IsPrimary__c = true;
    	
    	TestUtil.assertUpdateValidationRule(primaryAffiliation, 'An account\'s primary affiliation cannot be inactive');    	
    }
    
    static testmethod void providerMemberYesOnFlowdownTest(){
    	Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
    	Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
    	NU__Affiliation__c primaryAffiliation = NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);

		NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(providerAcct.Id);
    	
    	Account individualAcctQueried = AccountQuerier.getAccountById(individualAcct.Id);
    	
    	system.assertEquals('Yes', individualAcctQueried.Provider_Member__c);
    }


	static testmethod void joinOnNewPrimaryAffiliationTest(){
		
		TestMembershipFlowdownUtil.joinOnNewPrimaryAffiliationTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnnewPrimaryAffiliationFlowdownTwoLevelsTest(){
		
		TestMembershipFlowdownUtil.joinOnnewPrimaryAffiliationFlowdownTwoLevelsTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnNewIndivToIndivPrimaryAffilWithNoFlowdownTest(){
		
		TestMembershipFlowdownUtil.joinOnNewIndivToIndivPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnNewIndivToOrgPrimaryAffilWithNoFlowdownTest(){
		
		TestMembershipFlowdownUtil.joinOnNewPrimaryAffiliationTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void deletePrimaryAffilErasingJoinOnFlowdownTest(){
		
		TestMembershipFlowdownUtil.deletePrimaryAffilErasingJoinOnFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void changeAffiliationToPrimaryWithoutExistingJoinOnFlowdownTest(){
		
		TestMembershipFlowdownUtil.changeAffiliationToPrimaryWithoutExistingJoinOnFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnChangeAffiliationToNonPrimaryWithExistingFlowdownTest(){
		
		TestMembershipFlowdownUtil.joinOnChangeAffiliationToNonPrimaryWithExistingFlowdownTest(
    	    Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestProviderMembershipInserter(),
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    /*
    @isTest(SeeAllData=true)
    static void lcsAffiliationTooManyScriptStatementsTest(){
    	Account lcs = [select id, IAHSA_Membership__c, NU__Membership__c from Account where id = '001J000000WUZKs'];
    	Account lifecareSvcs = AccountQuerier.getAccountById('001J000000WUYrO');
    	
    	Test.startTest();
    	
    	NU.DataFactoryAffiliation.insertAffiliation(lifecareSvcs.Id, lcs.Id, true);
    	
    	Test.stopTest();
    	
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(lcs.IAHSA_Membership__c, new Set<Id> { lifecareSvcs.Id }, Constant.IAHSA_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	TestMembershipFlowdownUtil.assertMembershipFlowDown(lcs.NU__Membership__c, new Set<Id> { lifecareSvcs.Id }, Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }
    */
}