/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBatchUtStateLeadingAgeMember {


    public static final String DEFAULT_BUSINESS_ACCOUNT_NAME = 'Test Provider';
    public static final String DEFAULT_FIRST_NAME = 'FirstName';
    public static final String DEFAULT_LAST_NAME = 'LastName';
    public static final String DEFAULT_ACCOUNT_STATUS = Constant.ACCOUNT_STATUS_ACTIVE;
    
    public static final String DEFAULT_BILLING_STREET = '656 High Road';
    public static final String DEFAULT_BILLING_CITY = 'LeadingVille';
    public static final String DEFAULT_BILLING_STATE = 'NY';
    public static final String DEFAULT_BILLING_POSTAL_CODE = '14555';
    
    private static Integer increment = 0;
    private static String Increment() {
        return String.valueOf(++increment);
    }
    
    
    static testmethod void PerformTestMember() {
       Test.startTest();
       
       List<Account> newAccounts = new List<Account>(); 
       Account a = DataFactoryAccountExt.createMultiSiteAccount(10000.00);
       Date dt = date.today();
        newAccounts.add(a);
        
       Account aT = DataFactoryAccountExt.createProviderAccount(11000.00);
	   aT.NU__PrimaryAffiliation__c = a.Id ;
       newAccounts.add(aT);
       
       
       
       Account aT2 = DataFactoryAccountExt.createProviderAccount(11000.00);
       aT2.NU__PrimaryAffiliation__c = a.Id ;
       newAccounts.add(aT2);
       
       
       
       Account aT3 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT3);
       
       //creating individual account
       Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
        
       Account P = new Account(
            RecordTypeId = '012d0000000gc0YAAQ',
            FirstName = DEFAULT_FIRST_NAME,
            LastName = DEFAULT_LAST_NAME + Increment(),
            PersonEmail = DEFAULT_LAST_NAME + Increment() + '@test' + pwd + '.com',
            BillingStreet = DEFAULT_BILLING_STREET,
            BillingCity = DEFAULT_BILLING_CITY,
            BillingState = DEFAULT_BILLING_STATE,
            BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
            NU__Status__c = DEFAULT_ACCOUNT_STATUS );
       
       P.NU__PrimaryAffiliation__c = aT.Id;
        newAccounts.add(P);
        
        insert newAccounts;
        
        
       NU__Affiliation__c AffP = new NU__Affiliation__c(NU__ParentAccount__c = aT.Id, NU__Account__c = P.Id, NU__isPrimary__c = true, NU__DoNotFlowdownAddress__c = true,NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);
       insert AffP; 
       NU__Affiliation__c Aff1 = new NU__Affiliation__c(NU__ParentAccount__c = a.Id, NU__Account__c = aT.Id, NU__isPrimary__c = true, NU__DoNotFlowdownAddress__c = true,NU__Role__c = Constant.AFFILIATION_ROLE_COMPANY);
       insert Aff1;
       NU__Affiliation__c Aff2 = new NU__Affiliation__c(NU__ParentAccount__c = a.Id, NU__Account__c = aT2.Id, NU__isPrimary__c = true, NU__DoNotFlowdownAddress__c = true,NU__Role__c = Constant.AFFILIATION_ROLE_COMPANY);
       insert Aff2;
       NU__Affiliation__c Aff3 = new NU__Affiliation__c(NU__ParentAccount__c = a.Id, NU__Account__c = aT3.Id, NU__isPrimary__c = true, NU__DoNotFlowdownAddress__c = true,NU__Role__c = Constant.AFFILIATION_ROLE_COMPANY);
       insert Aff3;
       
       NU__MembershipType__c ProviderMT = DataFactoryMembershipTypeExt.insertProviderMembershipType();
       ProviderMT.Grants_National_Membership__c = true;
       ProviderMT.Grants_State_Membership__c = true;
       update ProviderMT;
       NU__Membership__c m = NU.DataFactoryMembership.createMembership(aT.Id, ProviderMT.Id, date.newInstance(dt.year(), 1, 1), date.newInstance(dt.year(), 12, 31));
       insert m;
       
       
       NU__Membership__c m2 = NU.DataFactoryMembership.createMembership(aT2.Id, ProviderMT.Id, date.newInstance(dt.year(), 1, 1), date.newInstance(dt.year(), 12, 31));
       insert m2;
       
       NU__Membership__c m3 = NU.DataFactoryMembership.createMembership(aT3.Id, ProviderMT.Id, date.newInstance(dt.year(), 1, 1), date.newInstance(dt.year(), 12, 31));
       insert m3;
        
        Account ab = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id];
        Account ab2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id];
        
       System.assertEquals(ab.State_Partner_Member__c, null);
       System.assertEquals(ab.LeadingAge_Member__c, null);
       
       //child affiliation
       System.assertEquals(ab2.State_Partner_Member__c, null);
       System.assertEquals(ab2.LeadingAge_Member__c, null);
       
       
       BatchUpatingStateLeadingAgeMemberFields c = new BatchUpatingStateLeadingAgeMemberFields();
       Database.executeBatch(c);
       Test.stopTest();

        Account ac = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id]; 
       System.assertEquals(ac.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac.LeadingAge_Member__c, 'Non-member');
       
       Account ac2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Member');
       
       Account ac3 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT3.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Member');
              
    }
    
    static testmethod void PerformTestMember2() {
       Test.startTest();
       
       List<Account> newAccounts = new List<Account>(); 
       Account a = DataFactoryAccountExt.createMultiSiteAccount(10000.00);
       Date dt = date.today();
        newAccounts.add(a);
        
       Account aT = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT);
       
       
       Account aT2 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT2);
       
       Account aT3 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT3);
       
       //creating individual account
       Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
        
       Account P = new Account(
            RecordTypeId = '012d0000000gc0YAAQ',
            FirstName = DEFAULT_FIRST_NAME,
            LastName = DEFAULT_LAST_NAME + Increment(),
            PersonEmail = DEFAULT_LAST_NAME + Increment() + '@test' + pwd + '.com',
            BillingStreet = DEFAULT_BILLING_STREET,
            BillingCity = DEFAULT_BILLING_CITY,
            BillingState = DEFAULT_BILLING_STATE,
            BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
            NU__Status__c = DEFAULT_ACCOUNT_STATUS );
       
       P.NU__PrimaryAffiliation__c = aT.Id;
        newAccounts.add(P);
        
        insert newAccounts;
        
       NU__MembershipType__c ProviderMT = DataFactoryMembershipTypeExt.insertProviderMembershipType();
       ProviderMT.Grants_National_Membership__c = true;
       ProviderMT.Grants_State_Membership__c = false;
       update ProviderMT;
       NU__Membership__c m = NU.DataFactoryMembership.createMembership(aT.Id, ProviderMT.Id, date.newInstance(dt.year(), 1, 1), date.newInstance(dt.year(), 12, 31));
       insert m;
       
       
       NU__Membership__c m2 = NU.DataFactoryMembership.createMembership(aT2.Id, ProviderMT.Id, date.newInstance(dt.year(), 1, 1), date.newInstance(dt.year(), 12, 31));
       insert m2;
       
       NU__Membership__c m3 = NU.DataFactoryMembership.createMembership(a.Id, ProviderMT.Id, date.newInstance(dt.year(), 1, 1), date.newInstance(dt.year(), 12, 31));
       insert m3;
        
        Account ab = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id];
        Account ab2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id];
        
       System.assertEquals(ab.State_Partner_Member__c, null);
       System.assertEquals(ab.LeadingAge_Member__c, null);
       
       //child affiliation
       System.assertEquals(ab2.State_Partner_Member__c, null);
       System.assertEquals(ab2.LeadingAge_Member__c, null);
       
       
       BatchUpatingStateLeadingAgeMemberFields c = new BatchUpatingStateLeadingAgeMemberFields();
       Database.executeBatch(c);
       Test.stopTest();

        Account ac = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id]; 
       System.assertEquals(ac.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac.LeadingAge_Member__c, 'Member');
       
       Account ac2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Member');
       
       Account ac3 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT3.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Member');
              
    }
    
    static testmethod void PerformTestNonmember() {
       Test.startTest();
       
       List<Account> newAccounts = new List<Account>(); 
       Account a = DataFactoryAccountExt.createMultiSiteAccount(10000.00);
       Date dt = date.today();
        newAccounts.add(a);
        
       Account aT = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT);
       
       
       
       Account aT2 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT2);
       
       Account aT3 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       newAccounts.add(aT3);
       
       //creating individual account
       Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
        
       Account P = new Account(
            RecordTypeId = '012d0000000gc0YAAQ',
            FirstName = DEFAULT_FIRST_NAME,
            LastName = DEFAULT_LAST_NAME + Increment(),
            PersonEmail = DEFAULT_LAST_NAME + Increment() + '@test' + pwd + '.com',
            BillingStreet = DEFAULT_BILLING_STREET,
            BillingCity = DEFAULT_BILLING_CITY,
            BillingState = DEFAULT_BILLING_STATE,
            BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
            NU__Status__c = DEFAULT_ACCOUNT_STATUS );
       
       P.NU__PrimaryAffiliation__c = aT.Id;
        newAccounts.add(P);
        
        insert newAccounts;
        
        
        
        Account ab = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id];
        Account ab2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id];
        
       System.assertEquals(ab.State_Partner_Member__c, null);
       System.assertEquals(ab.LeadingAge_Member__c, null);
       
       //child affiliation
       System.assertEquals(ab2.State_Partner_Member__c, null);
       System.assertEquals(ab2.LeadingAge_Member__c, null);
       
       
       BatchUpatingStateLeadingAgeMemberFields c = new BatchUpatingStateLeadingAgeMemberFields();
       Database.executeBatch(c);
       Test.stopTest();

        Account ac = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id]; 
       System.assertEquals(ac.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac.LeadingAge_Member__c, 'Non-member');
       
       Account ac2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Non-member');
       
       Account ac3 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT3.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Non-member');
       

    }
    
    static testmethod void PerformTestRandom() {
       Test.startTest();
       
       List<Account> newAccounts = new List<Account>(); 
       Account a = DataFactoryAccountExt.createMultiSiteAccount(10000.00);
       a.State_Partner_Member__c = 'Non-member';
       a.LeadingAge_Member__c = 'Non-member';
       Date dt = date.today();
        newAccounts.add(a);
        
       Account aT = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       aT.State_Partner_Member__c = 'Non-member';
       aT.LeadingAge_Member__c = 'Non-member';
       
       newAccounts.add(aT);
       
       
       Account aT2 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       aT2.State_Partner_Member__c = 'Member';
       aT2.LeadingAge_Member__c = 'Member';
       newAccounts.add(aT2);
       
       Account aT3 = DataFactoryAccountExt.createProviderAccount(11000.00);
       aT3.State_Partner_Member__c = 'Member';
       aT3.LeadingAge_Member__c = 'Member';
       newAccounts.add(aT3);
       

        
        insert newAccounts;
        

        
        Account ab = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id];
        Account ab2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id];
        
       System.assertEquals(ab.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ab.LeadingAge_Member__c, 'Non-member');
       
       //child affiliation
       System.assertEquals(ab2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ab2.LeadingAge_Member__c, 'Non-member');
       
       
       BatchUpatingStateLeadingAgeMemberFields c = new BatchUpatingStateLeadingAgeMemberFields();
       Database.executeBatch(c);
       Test.stopTest();

        Account ac = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id]; 
       System.assertEquals(ac.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac.LeadingAge_Member__c, 'Non-member');
       
       Account ac2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Non-member');
       
       Account ac3 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT3.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Non-member');
       

    }
    
    static testmethod void PerformTestRandomWithoutDateFilter() {
       Test.startTest();
       
       List<Account> newAccounts = new List<Account>(); 
       Account a = DataFactoryAccountExt.createMultiSiteAccount(10000.00);
       a.State_Partner_Member__c = 'Non-member';
       a.LeadingAge_Member__c = 'Non-member';
       Date dt = date.today();
        newAccounts.add(a);
        
       Account aT = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       aT.State_Partner_Member__c = 'Non-member';
       aT.LeadingAge_Member__c = 'Non-member';
       
       newAccounts.add(aT);
       
       
       Account aT2 = DataFactoryAccountExt.createProviderAccount(11000.00, a.Id);
       aT2.State_Partner_Member__c = 'Member';
       aT2.LeadingAge_Member__c = 'Member';
       newAccounts.add(aT2);
       
       Account aT3 = DataFactoryAccountExt.createProviderAccount(11000.00);
       aT3.State_Partner_Member__c = 'Member';
       aT3.LeadingAge_Member__c = 'Member';
       newAccounts.add(aT3);
       

        
        insert newAccounts;
        

        
        Account ab = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id];
        Account ab2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id];
        
       System.assertEquals(ab.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ab.LeadingAge_Member__c, 'Non-member');
       
       //child affiliation
       System.assertEquals(ab2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ab2.LeadingAge_Member__c, 'Non-member');
       
       
       BatchUpatingStateLeadingAgeMemberFields c = new BatchUpatingStateLeadingAgeMemberFields(false);
       Database.executeBatch(c);
       Test.stopTest();

        Account ac = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :a.Id]; 
       System.assertEquals(ac.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac.LeadingAge_Member__c, 'Non-member');
       
       Account ac2 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Non-member');
       
       Account ac3 = [SELECT Id, State_Partner_Member__c, LeadingAge_Member__c FROM Account WHERE Id = :aT3.Id]; 
       System.assertEquals(ac2.State_Partner_Member__c, 'Non-member');
       System.assertEquals(ac2.LeadingAge_Member__c, 'Non-member');
       

    }
}