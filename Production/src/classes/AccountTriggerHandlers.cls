public class AccountTriggerHandlers extends NU.TriggerHandlersBase {
    Id multiSiteOrgRecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID;
    
    static List<Account> insertedIndividualsInContext = new List<Account>();
    static Set<Id> statesCanEditAccountIds = new Set<Id>();
    static boolean isExecuting = false;

    private User currentUserPriv = null;
    private User CurrentUser {
        get {
            if (currentUserPriv == null){
                currentUserPriv = UserQuerier.getCurrentUserWithStateInformation();
            }
            
            return currentUserPriv;
        }
    }
    
    private Boolean isStatePartnerUser(){
        return CurrentUser.ContactId != null ||
               UserQuerier.UserStatePartnerAccount != null;
    }

    private Id userStatePartnerIdPriv = null;
    private Boolean userStatePartnerIdCalculated = false;
    private Id UserStatePartnerId {
        get {
            if (userStatePartnerIdCalculated == false &&
                isStatePartnerUser() == true){

                userStatePartnerIdPriv = 
                UserQuerier.UserStatePartnerAccount != null ?
                    UserQuerier.UserStatePartnerAccount.Id :
                    CurrentUser.Contact.Account.State_Partner_Id__c;
                
                
                userStatePartnerIdCalculated = true;
            }
            
            return userStatePartnerIdPriv;
        }
    }
    
    private Id StatePartnerId(string Findname){		
	    			
	    	if(Findname != null || Findname == '')		
	    	{		
			    	List<Account> SPI = new List<Account>();		
			    			
			    	SPI = [Select Id from Account where Name = :Findname limit 1];		
			    			
			    	if(SPI.size() > 0)		
			    	{		
			    		return SPI[0].Id;		
			    	}		
	    	}		
	    	 		
	    	 return null;		
	    }
    private void populateInsertedIndividualInContext(List<Account> newAccounts){
        for (Account newAcct : newAccounts){            
            if (newAcct.IsPersonAccount == true){
                insertedIndividualsInContext.add(newAcct);
            }
        }
    }
    
    private Boolean isNewlyInsertedIndividual(Account individual){
        for (Account insertedIndividual : insertedIndividualsInContext){
            if (individual.IsPersonAccount == true &&
                insertedIndividual.FirstName == individual.FirstName &&
                insertedIndividual.LastName == individual.LastName){
                return true;
            }
        }
        return false;
    }

    public void validateMultiSiteBillJointly(List<Account> accounts){
        for (Account acct : accounts){
            if (acct.RecordTypeId != multiSiteOrgRecordTypeId &&
                acct.Multi_Site_Bill_Jointly__c == true){
                acct.addError(Constant.BILL_JOINTLY_MULTISITE_ONLY_VAL_RULE_MSG);
            }
        }
    }
    
    public void defaultStatePartnerForStatePartnerCreatedAccounts(List<Account> newAccounts){
        if (isStatePartnerUser() == false){
            return;
        }
        
        for (Account acct : newAccounts){
            if (acct.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID ||
                acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID ||
                acct.IsPersonAccount == true){
                
                acct.State_Partner_Id__c = UserStatePartnerId;
            }
        }
    }

    public void setOwnerToGeneralStatePartnerUserOnBeforeInsert(List<Account> newRecords){
        Set<Id> primaryAffilIds = new Set<Id>();
        List<Account> accountsWithPrimaryAffils = new List<Account>();
        
        for (Account acct : newRecords){

            //Reset as GeneralStatePartnerUser if multi-site org or state partner id is populated 
            if ((acct.State_Partner_Id__c != null ||
                 acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID
                )
                 &&
                acct.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID){

                //exclude State Partners using Nimble AMS (eg. Washington and Kentucky)
                if (!UserQuerier.UserStatePartnerAccountUsesNimbleAMS) {
                    acct.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                }
            }

            //invstigate further if primary affiliation is populated, and user State Partner is not using Nimble AMS (eg. Washington and Kentucky)
            if (acct.NU__PrimaryAffiliation__c != null && !UserQuerier.UserStatePartnerAccountUsesNimbleAMS){
                accountsWithPrimaryAffils.add(acct);
                primaryAffilIds.add(acct.NU__PrimaryAffiliation__c);
            }
        }
        
        if (accountsWithPrimaryAffils.size() > 0){
            Map<Id, Account> primaryAffilAccounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(primaryAffilIds);

            for (Account accountWithPrimaryAffil : accountsWithPrimaryAffils){
                Account primaryAffilAccount = primaryAffilAccounts.get(accountWithPrimaryAffil.NU__PrimaryAffiliation__c);
                
                if ((primaryAffilAccount.State_Partner_Id__c != null &&
                     accountWithPrimaryAffil.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID) ||
                     
                     // If the parent's owner is the general state partner then one of its "parents"
                     // are associated with a State.
                     primaryAffilAccount.OwnerId == StatePartnerOwnerUserId.GeneralStatePartnerUserId){
                     accountWithPrimaryAffil.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                }
            }
        }
    }
    
    public void updateOwnershipOnBeforeUpdate(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
        /*
           Set the account's owner ids based on their primary affiliation. If
           an account becomes primary affiliatied with an account linked to a
           state partner, set its owner to the "General State Partner" user.
        */
        
        List<Account> accountsWithUpdatedPrimaryAffils = new List<Account>();
        
        List<Account> accountsNowOwnedByGeneralStatePartnerUser = new List<Account>();
        
        for (Account oldAcct : oldAccounts.values()){
            Account newAcct = newAccounts.get(oldAcct.Id);
            
            if (oldAcct.NU__PrimaryAffiliation__c != newAcct.NU__PrimaryAffiliation__c){
                
                if (newAcct.NU__PrimaryAffiliation__c != null){
                    accountsWithUpdatedPrimaryAffils.add(newAcct);
                }
            }
            
            // account has become assigned to a state partner
            // so set its owner to the general state partner user.
            if (oldAcct.State_Partner_Id__c == null
                && newAcct.State_Partner_Id__c != null
                && newAcct.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID
                && !UserQuerier.UserStatePartnerAccountUsesNimbleAMS){
                newAcct.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                accountsNowOwnedByGeneralStatePartnerUser.add(newAcct);
            }
            
            if (oldAcct.OwnerId != StatePartnerOwnerUserId.GeneralStatePartnerUserId &&
                newAcct.OwnerId == StatePartnerOwnerUserId.GeneralStatePartnerUserId){
                accountsNowOwnedByGeneralStatePartnerUser.add(newAcct);
            }
        }
        
        if (accountsWithUpdatedPrimaryAffils.size() > 0){
            Set<Id> allChangedPrimaryAffilIds = NU.CollectionUtil.getLookupIds(accountsWithUpdatedPrimaryAffils, 'NU__PrimaryAffiliation__c');
            Map<Id, Account> primaryAffilAccounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(allChangedPrimaryAffilIds);
            
            if (accountsWithUpdatedPrimaryAffils.size() > 0){
                
                
                for (Account accountWithUpdatedPrimaryAffil : accountsWithUpdatedPrimaryAffils){
                    Account queriedPrimaryAffilAccount = primaryAffilAccounts.get(accountWithUpdatedPrimaryAffil.NU__PrimaryAffiliation__c);
                    
                    if ((queriedPrimaryAffilAccount.State_Partner_Id__c != null
                        || queriedPrimaryAffilAccount.OwnerId == StatePartnerOwnerUserId.GeneralStatePartnerUserId)
                        && !UserQuerier.UserStatePartnerAccountUsesNimbleAMS){
                        accountWithUpdatedPrimaryAffil.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                    }
                }
            }
        }
        
        List<Account> accountsToUpdate = new List<Account>();
        
        if (accountsNowOwnedByGeneralStatePartnerUser.size() > 0){
            // Update child primary affiliations' owner ids to the
            // general state partner user.
            
            List<Account> childAccounts = 
            [select id
               from Account
              where NU__PrimaryAffiliation__c in :accountsNowOwnedByGeneralStatePartnerUser
                and OwnerId != :StatePartnerOwnerUserId.GeneralStatePartnerUserId];
                
            for (Account childAccount : childAccounts){
                if (!UserQuerier.UserStatePartnerAccountUsesNimbleAMS) {
                    childAccount.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                    accountsToUpdate.add(childAccount);
                }
            }
        }
        
        if (accountsToUpdate.size() > 0){
            update accountsToUpdate;
        }
    }
    
    private List<Account> getLatestAccountInfos(List<Account> accounts){
        return [select id,
                        name,
                        //State_Partner_Name__c,
                        NU__PrimaryAffiliation__c,
                        NU__PrimaryAffiliation__r.Id,
                        NU__PrimaryAffiliation__r.Provider_Membership__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__StartDate__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__EndDate__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__MembershipType__c,
                        NU__PrimaryAffiliation__r.RecordTypeId,
                        NU__PrimaryAffiliation__r.Multi_Site_Corporate__c,
                        Provider_Membership__c,
                        Provider_Membership__r.NU__MembershipType__c,
                        Provider_Membership__r.NU__StartDate__c,
                        Provider_Membership__r.NU__EndDate__c,
                        RecordTypeId,
                        (Select id,
                                name,
                                NU__MembershipType__c,
                                NU__StartDate__c,
                                NU__EndDate__c
                           from NU__Memberships__r)
                   from Account
                  where id in :accounts];
    }
    
    private Boolean isCorporateMSOWithProviderMembership(Account account){
        return account.NU__PrimaryAffiliation__r.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
               account.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == true &&
               account.NU__PrimaryAffiliation__r.Provider_Membership__c != null;
    }
    
    private Boolean needsCorporateProviderMembershipCreated(Account account){
        NU__Membership__c parentMSOMembership = account.NU__PrimaryAffiliation__r.Provider_Membership__r;
        
        for (NU__Membership__c accountMembership : account.NU__Memberships__r){
            if (accountMembership.NU__MembershipType__c == parentMSOMembership.NU__MembershipType__c &&
                accountMembership.NU__StartDate__c == parentMSOMembership.NU__StartDate__c &&
                accountMembership.NU__EndDate__c == parentMSOMembership.NU__EndDate__c){
                
                return false;
            }
        }
        
        return true;
    }
    
    private void createCorporateProviderMemberships(List<Account> newCorporateProvidersToCreateMembershipsFor){
        if (newCorporateProvidersToCreateMembershipsFor.size() > 0){
            List<NU__Membership__c> newCorporateProviderMemberships = new List<NU__Membership__c>();
            
            for (Account corporateProvider : newCorporateProvidersToCreateMembershipsFor){
                NU__Membership__c corporateProviderMembership = new NU__Membership__c(
                    NU__Account__c = corporateProvider.Id,
                    NU__StartDate__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__StartDate__c,
                    NU__EndDate__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__EndDate__c,
                    NU__MembershipType__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__MembershipType__c,
                    NU__ExternalAmount__c = 0
                );
                
                newCorporateProviderMemberships.add(corporateProviderMembership);
            }
            
            insert newCorporateProviderMemberships;
        }
    }
    
    private void createProviderMembershipsForNewCorporateProvidersOnAfterInsert(List<Account> newAccounts){
        List<Account> potentialNewCorporateProviders = new List<Account>();
        
        for (Account newAccount : newAccounts){
            if (newAccount.NU__PrimaryAffiliation__c != null &&
                newAccount.Multi_Site_Enabled__c == true){
                potentialNewCorporateProviders.add(newAccount);
            }
        }
        
        if (potentialNewCorporateProviders.size() > 0){
            List<Account> latestAccountInfos = getLatestAccountInfos(potentialNewCorporateProviders);
            List<Account> newCorporateProvidersToCreateMembershipsFor = new List<Account>();
            
            for (Account latestAccountInfo : latestAccountInfos){
                if (isCorporateMSOWithProviderMembership(latestAccountInfo)){
                    newCorporateProvidersToCreateMembershipsFor.add(latestAccountInfo);
                }
            }
            
            createCorporateProviderMemberships(newCorporateProvidersToCreateMembershipsFor);
        }
    }
    
    private void createProviderMembershipsForNewCorporateProviders(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
        Boolean bNeedsDeeperInspection = false;
        
        for (Account oldAccount : oldAccounts.values()){
            Account newAccount = newAccounts.get(oldAccount.Id);
            
            if ((oldAccount.NU__PrimaryAffiliation__c != newAccount.NU__PrimaryAffiliation__c &&
                  newAccount.NU__PrimaryAffiliation__c != null) 
                  
                  ||
                
                 (oldAccount.Multi_Site_Enabled__c == false &&
                  newAccount.Multi_Site_Enabled__c == true)
                
                )
                {
                    
                bNeedsDeeperInspection = true;
                break;
            }
        }
        
        
        if (bNeedsDeeperInspection) {
            Map<Id, Account> latestAccountInfos = new Map<Id, Account>( getLatestAccountInfos(newAccounts.values()) );
            List<Account> newCorporateProvidersToCreateMembershipsFor = new List<Account>();
            
            for (Account oldAccount : oldAccounts.values()){
                Account newAccount = newAccounts.get(oldAccount.Id);
                Account latestAccountInfo = latestAccountInfos.get(oldAccount.Id);
                
                if (((oldAccount.NU__PrimaryAffiliation__c != newAccount.NU__PrimaryAffiliation__c &&
                      newAccount.NU__PrimaryAffiliation__c != null) 
                      
                      ||
                    
                     (oldAccount.Multi_Site_Enabled__c == false &&
                      newAccount.Multi_Site_Enabled__c == true)
                    
                    )
                    
                     &&
                    newAccount.Multi_Site_Enabled__c == true &&
                    isCorporateMSOWithProviderMembership(latestAccountInfo) &&
                    needsCorporateProviderMembershipCreated(latestAccountInfo)){
                        
                    newCorporateProvidersToCreateMembershipsFor.add(latestAccountInfo);
                }
            }
            
            createCorporateProviderMemberships(newCorporateProvidersToCreateMembershipsFor);
        }
    }
    
    private void setStatePartnerFieldsForStateUsers(List<Account> newAccounts){
        Account stateUserStatePartnerAccount = UserQuerier.UserStatePartnerAccount;
        
        if (stateUserStatePartnerAccount == null){
            return;
        }
        
        for (Account newAccount : newAccounts){
            newAccount.State_Partner_Id__c = stateUserStatePartnerAccount.Id;
            //newAccount.State_Partner_Name__c = stateUserStatePartnerAccount.Name;
        }
    }
    
    public override void onBeforeInsert(List<Sobject> newRecords){
        List<Account> newAccounts = (List<Account>) newRecords;
        
        populateInsertedIndividualInContext(newAccounts);
        
        validateMultiSiteBillJointly(newAccounts);
        
        defaultStatePartnerForStatePartnerCreatedAccounts(newAccounts);

        setOwnerToGeneralStatePartnerUserOnBeforeInsert(newAccounts);
        
        setStatePartnerFieldsForStateUsers(newAccounts);
    }
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        List<Account> newAccounts = (List<Account>) newRecordMap.values();
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldRecordMap;
        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newRecordMap;
        
        validateMultiSiteBillJointly(newAccounts);
        updateOwnershipOnBeforeUpdate(oldAccountsMap, newAccountsMap);
    }
    
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldRecordMap;
        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newRecordMap;
        List<Account> newAccounts = (List<Account>) newRecordMap.values();		
        		
        createProviderMembershipsForNewCorporateProviders(oldAccountsMap, newAccountsMap);
    }
    
    public override void onAfterInsert(Map<Id, sObject> newRecordMap){
        List<Account> newAccounts = (List<Account>) newRecordMap.values();
        
        createProviderMembershipsForNewCorporateProvidersOnAfterInsert(newAccounts);
    }
}