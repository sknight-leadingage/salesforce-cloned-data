/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDealTrigger {

    static testMethod void dealEventFromDifferentEntityThanDealItemsTest() {
        NU__Entity__c entity1 = NU.DataFactoryEntity.insertEntity();
        NU__Entity__c entity2 = NU.DataFactoryEntity.insertEntity();
        
        NU__GLAccount__c gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity1.Id);
        
        Account anyAcct = NU.DataFactoryAccount.insertIndividualAccount();
        
        Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        Map<String, Schema.RecordTypeInfo> dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo sponsorshipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        
        NU__Product__c sponsorshipProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, entity1.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        
        NU__Deal__c sponsorshipDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, anyAcct.Id, anyAcct.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, sponsorshipDealRTI.getRecordTypeId());
        
        DealItem__c sponsorshipDealItem = DataFactoryDealItem.insertDealItem(sponsorshipDeal.Id, sponsorshipProduct.Id, 500.00, 1);
        
		NU__Event__c differentEntityEvent = NU.DataFactoryEvent.insertEvent(entity2.Id);
		
		sponsorshipDeal.Event__c = differentEntityEvent.Id;
		
		TestUtil.assertUpdateValidationRule(sponsorshipDeal, Constant.DEAL_EVENT_ENTITY_DIFFERENT_FROM_DEAL_ITEMS_ENTITY_VAL_MSG);
    }
}