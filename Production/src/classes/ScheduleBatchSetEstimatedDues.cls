global class ScheduleBatchSetEstimatedDues implements Schedulable
{
	global void execute(SchedulableContext ctx) 
	{
		BatchSetEstimatedDues batchSchEst = new BatchSetEstimatedDues();
		ID batchprocessid = Database.executeBatch(batchSchEst, 20);
	}   
}