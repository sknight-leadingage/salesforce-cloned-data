public without sharing class AccountTriggerSubscriptionsHandler extends NU.TriggerHandlersBase
{
    // leave in trigger
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {
    	Set<Id> currentSubsForAccIds = newRecordMap.keySet();
    	List<Online_Subscription__c> subsCurrentHeldButBounceCleared = [SELECT Id, Account__c, Product__c, Product__r.Id, Start_Date__c, Status__c, End_Date__c, System_End_Date__c FROM Online_Subscription__c WHERE Account__r.IsPersonAccount = true AND Account__c = :currentSubsForAccIds AND Account__r.PersonEmailBouncedDate = null AND Status__c = :Constant.SUBSCRIPTION_STATUS_HELD];
    	
    	if (subsCurrentHeldButBounceCleared.size() > 0) {
	    	List<Online_Subscription__c> subsToUpdate = new List<Online_Subscription__c>();
	    	
			for(Online_Subscription__c subCurrent : subsCurrentHeldButBounceCleared) {
				subsToUpdate.add(new Online_Subscription__c(Id = subCurrent.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, End_Date__c = null, System_End_Date__c = null));
			}
			
			if (subsToUpdate.size() > 0) {
				update subsToUpdate;
			}
    	}
    }
    
    public override void onAfterDelete(Map<Id, sObject> oldRecordMap) {
        //See: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_triggers_merge_statements.htm
        List<Account> oldTriggerAccounts = oldRecordMap.values();
        Set<Id> winningIds = new Set<Id>();
        for(Account winningId : oldTriggerAccounts) {
            if (winningId.MasterRecordId != null) {
                winningIds.add(winningId.MasterRecordId);
            }
        }

        if (winningIds.size() > 0) {
            List<Online_Subscription__c> toDelete = new List<Online_Subscription__c>();
            List<Online_Subscription__c> toUpdate = new List<Online_Subscription__c>();
            List<Online_Subscription__c> subsCurrent = [SELECT Id, Account__c, Product__c, Product__r.Id, Start_Date__c, Status__c, End_Date__c, System_End_Date__c FROM Online_Subscription__c WHERE Account__c = :winningIds ORDER BY Product__c, Start_Date__c DESC];
 
            integer subI = 0;
            Id lastProdId = null;
            for(Online_Subscription__c subCurrent : subsCurrent) {
                if (lastProdId != null && lastProdId == subCurrent.Product__r.Id) {
                    subI++;
                }
                else {
                    subI = 0;
                    if (subCurrent.Status__c == Constant.SUBSCRIPTION_STATUS_HELD) {
                        toUpdate.add(subCurrent); //Defer this for later so that dupes are deleted before we try to update (Validation reasons)
                    }
                }
                
                if (subI > 0) {
                    toDelete.add(subCurrent);
                }

                lastProdId = subCurrent.Product__r.Id;
            }

            //Delete before update (Validation reasons)
            if (toDelete.size() > 0) {
                delete toDelete;
            }

            if (toUpdate.size() > 0) {
                for(Online_Subscription__c subUpdate : toUpdate) {
                    subUpdate.Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE;
                    subUpdate.End_Date__c = null;
                    subUpdate.System_End_Date__c = null;
                }
                update toUpdate;
            }
        }
    }
    
}