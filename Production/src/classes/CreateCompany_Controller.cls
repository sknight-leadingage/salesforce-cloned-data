public with sharing class CreateCompany_Controller {
    // ******* BEGIN: Choose Type to Create *********
    public string companyTypes {get;set;}
    public string strDebug  {get;set;}
    
    public List<SelectOption> getcompanyTypesItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID, Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE));
        options.add(new SelectOption(Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY, Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY));
        options.add(new SelectOption(Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID, Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER));
        return options; 
    }
    // ******* END: Choose Type to Create *********
    
     
    public string optYesNoSelected {get;set;}

    // ******* BEGIN: Wizard Setup Code *********
    private integer iWizardMax = 8;
    public integer WizardStep {get;set;}
    public integer WizardMax
    {
            get{
                    return iWizardMax;
            }
    }
    
    public void WizardBack(){
        if (WizardStep == 4){
            //We're at "Create a Provider"
            if (optYesNoSelected == null){
                //We came straight here from the first Wizard Step so we go back there now
                WizardStep = 2; //This goes back to step 1, because we always subtract 1 and the end of the method
            }
            else {
                //We're about to go back to the end of the 'Create MSO/MC' Step
                optYesNoSelected = null; //Reset our selection (This helps make the page header correct)
            }
        }       
            
        if (WizardStep == 6){
                if (optChooseMSO == '2'){
                        //Going in reverse direction, if we choose 'no' to are we attached to an MSO, then we skip a step
                        WizardStep--;                   
                }
        }
        
        WizardStep--;
        
        if (companyTypes != null && WizardStep == 1){
            companyTypes = null;
            optYesNoSelected = null; //Reset this option list here too, since we're basically starting over at this point
        }
    }
    
    public void WizardNext(){
        if(WizardStep == 1){
            if (companyTypes != null)
            {
                newMSOMC.Type = null;
                if (companyTypes == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID){
                      WizardStep = 3; //Goes to step 4 because method always increments at the end
                }
                else if (companyTypes == Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY){
                         newMSOMC.Type = Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY;
                }
            }
        }
        
        if (WizardStep == 2){
            //Save new MSOMC
            if (sNewMSOMCName != '' && sNewMSOMCName != null){
                if (isValid(2)){
                    MSOMCSave();                   
                }
            }
        }
        
        if (WizardStep == 3){
            if (optYesNoSelected != null)
            {
                //This is the question after you create an MSO/MC and we ask 'Do you want to attach a Provider'
                if (optYesNoSelected == '2'){
                    //if 'No' then jump to end
                    //WizardStep = iWizardMax -1; //This goes to the last step.  We take one off so that we can add one at the end of the method.
                    //GC updated - Jump to end, then exit.  If we proceed we will hit provider save below
                    WizardStep = iWizardMax;
                    return;
                }
                else {
                        //if 'Yes' then we need to transition past the 'Are you attached to an MSO/MC' step, because at this point in the Wizard, we just created one
                        if (selectedMSO_Id != null){
                                //WizardStep = 5; //This will jump us to Step 6 once the step gets incremented at the end of the method
                                WizardStep = 6;
                                return;
                        }
                }
            }
        }
           
        if(WizardStep == 4){
            if (optChooseMSO == '2'){
                    //Going in forward direction, if we choose 'no' to are we attached to an MSO, then we skip a step
                    // WizardStep++;
                    WizardStep = 6;
                    return;
            }
        }
            
        if(WizardStep == 5) // Run Validation to ensure MSO is selected
        {   
            if (isValid(5)) {   
                //Run Validation.  If is invalid, the step will be decremented in function              
            }
        }       
            
                 
            
       if(WizardStep == 6) // Do Save
        {   
            if (isValid(6)) {   
                //Run Validation.  If is invalid, the step will be decremented in function              
            }
        }       
            
            
        if(WizardStep == 7) // Do Save
        {   
            if (isValid(7)) {   
                ProviderSave();
            }
        }       
                            

        WizardStep++;
    }
    // ******* END: Wizard Setup Code *********
    
    public Account newMSOMC {get;set;}
    public string sNewMSOMCName { get; set; }  //create seperate variable to display custom lavel "MSO/MC Name" rather than generic "Account Name"
    
    public Account newMContact {get;set;}
    public string sMFirstName { get; set; }
    public string sMLastName { get; set; }
    public string sMPersonEmailAddress { get; set; }
    
    public Account newProvider {get;set;}
    public string sNewProviderName { get; set; }  //create seperate variable to display custom lavel "Provider Name" rather than generic "Account Name"
    
    public Account newContact {get;set;}
    public string sFirstName { get; set; }
    public string sLastName { get; set; }
    public string sPersonEmailAddress { get; set; }
    
    public string optChooseMSO {get;set;}
    public string sFindMSOSearchText {get;set;}
    public Id selectedMSO_Id {get; set;}
    public string selectedMSO_Name {get; set;}
    public boolean displayConfirmMSG {get;set;}
    
    public CreateCompany_Controller(){
        WizardStep = 1;
        newMSOMC = new Account(RecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID);
        newMContact = new Account(RecordTypeId = Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID);
        newProvider = new Account(RecordTypeId = Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID);
        newContact = new Account(RecordTypeId = Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID);
    }

    public void selectRow() {        
    }
    
    public void performSearch(){
    }
    
    public List<Account> getdsMSOList(){
           if (sFindMSOSearchText != null)
           {
                    string sq = '%' + sFindMSOSearchText + '%';
            return [SELECT id,
                    name,
                    BillingCity, 
                    BillingState,
                    BillingStreet,
                    BillingPostalCode, 
                    LeadingAge_Member_Company__c,
                    Type
             FROM Account
             WHERE RecordType.DeveloperName = 'Multi_site_Organization' AND name LIKE :sq ORDER BY name ASC];
            }
            
             return new List<Account>(); 
    }
    
    public string GetMSOorMCDescription {
        get
        {
            string retVal = 'Company';
            if (companyTypes != null){
                  if ((optYesNoSelected == null || (optYesNoSelected != null && optYesNoSelected == '2')) && companyTypes == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
                    retVal = Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE;  
                  }
                  else if ((optYesNoSelected == null || (optYesNoSelected != null && optYesNoSelected == '2')) && companyTypes == Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY){
                    retVal = Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY;
                  }
                  else
                  {
                    retVal = Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER;
                  }
            }
            return retVal;
        }
    }

    public void MSOMCSave()
    {      
        
         try
        { 
            //Assign custom input values to the sObject (This seemed necessary due to read-only/default labelling problems)
            newMSOMC.Name = sNewMSOMCName;
            newMContact.FirstName = sMFirstName;
            newMContact.LastName = sMLastName;
            newMContact.PersonEmail = sMPersonEmailAddress;
            
            //Set New MSO/MC Shipping equal to Billing Address
            newMSOMC.ShippingStreet = newMSOMC.BillingStreet;
            newMSOMC.ShippingCity = newMSOMC.BillingCity;
            newMSOMC.ShippingState = newMSOMC.BillingState;
            newMSOMC.ShippingPostalCode = newMSOMC.BillingPostalCode;
            
            //Set Contact Addresses equal to organization adddress
            newMContact.BillingStreet = newMSOMC.BillingStreet;
            newMContact.BillingCity = newMSOMC.BillingCity;
            newMContact.BillingState = newMSOMC.BillingState;
            newMContact.BillingPostalCode = newMSOMC.BillingPostalCode;
            
            newMContact.ShippingStreet = newMSOMC.BillingStreet;
            newMContact.ShippingCity = newMSOMC.BillingCity;
            newMContact.ShippingState = newMSOMC.BillingState;
            newMContact.ShippingPostalCode = newMSOMC.BillingPostalCode;                            
            
            //Note: We're managing the Type further up in the Wizard
            /*
            if (companyTypes == Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY){
                We're adding an MC so let's set that here
                newMSOMC.Type = Constant.ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY;
            }
            */
            
            List<Account> insertAccounts = new List<Account>();
            insertAccounts.add(newMSOMC);
            insertAccounts.add(newMContact);
            Database.SaveResult[] dbsrInfo = Database.Insert(insertAccounts);
            if (dbsrInfo.size() == 2){
                    if (dbsrInfo[0].isSuccess()){
                            selectedMSO_Id = dbsrInfo[0].getId();
                            selectedMSO_Name = sNewMSOMCName;
                    }
            }
        }      
            
        catch (DMLException e)
        {                   
            ApexPages.addMessages(e);   
        }
        finally{} 
        
    }
    
    public void ProviderSave()
    {
        
     try
        {                                                   
                                
                newProvider.Name = sNewProviderName; //set name from custom input field
                
                //Set Provider Shipping/Mailing Addresses equal to Billing adddress
                newProvider.ShippingStreet = newProvider.BillingStreet;
                newProvider.ShippingCity = newProvider.BillingCity;
                newProvider.ShippingState = newProvider.BillingState;
                newProvider.ShippingPostalCode = newProvider.BillingPostalCode;
                                                                
                //SK 20130321: Can't believe this is necessary, but Google says it's the best way (The SObject First/Last come up as read-only!)
                newContact.FirstName = sFirstName;
                newContact.LastName = sLastName;
                newContact.PersonEmail = sPersonEmailAddress;
                
                //Set Contact Addresses equal to organization adddress
                newContact.BillingStreet = newProvider.BillingStreet;
                newContact.BillingCity = newProvider.BillingCity;
                newContact.BillingState = newProvider.BillingState;
                newContact.BillingPostalCode = newProvider.BillingPostalCode;
                
                newContact.ShippingStreet = newProvider.BillingStreet;
                newContact.ShippingCity = newProvider.BillingCity;
                newContact.ShippingState = newProvider.BillingState;
                newContact.ShippingPostalCode = newProvider.BillingPostalCode;                         
                
                
                //Set State Assocation - Removed and performed in trigger instead.
                //User pageUser = [SELECT ID, CompanyName, Contact.Account.State_Partner_Id__c FROM User WHERE ID = :UserInfo.getUserID()]; 
                //newProvider.State_Partner_ID__c  = pageUser.Contact.Account.State_Partner_Id__c;                              
                                        
                insert newProvider;
                insert newContact;                                          
                
                //Create Membership for Provider Company
                Date startDate = datetime.now().date();
                startDate = date.newInstance(startDate.year(), startDate.month(), 1);
                
                Date endDate = datetime.now().date();
                endDate = endDate.addMonths(12);
                endDate = date.newInstance(endDate.year(), enddate.month(), date.daysInMonth(endDate.year(), enddate.month()));
                
                NU__MembershipType__c providerMT = MembershipTypeQuerier.getJointStateProviderMembershipType();
        
                if (providerMT != null)
                {
                    NU__Membership__c ProviderMembership = new NU__Membership__c(
                        NU__Account__c = newProvider.Id,
                        NU__MembershipType__c = providerMT.Id,
                        NU__StartDate__c = startDate,
                        NU__EndDate__c = endDate
                    );
                    insert ProviderMembership;
                }
        
                List <NU__Affiliation__c> AffList = new List <NU__Affiliation__c>();
        
                NU__Affiliation__c Provider_Contact_Aff = new NU__Affiliation__c(NU__ParentAccount__c = newProvider.Id, NU__Account__c = newContact.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT, NU__StartDate__c = system.today());   
                //insert aff;
                AffList.add(Provider_Contact_Aff);
                
                // Create Affilliation between Provider and MSO
                if (optChooseMSO == '1' || optYesNoSelected == '1'){
                                NU__Affiliation__c ProviderSite_MSO_Aff = new NU__Affiliation__c(NU__ParentAccount__c = selectedMSO_Id, NU__Account__c = newProvider.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_COMPANY);
                                AffList.add(ProviderSite_MSO_Aff);                      
                        }
                
                                        
                insert AffList;        
                                
                //WizardStep++; //GC removed, increment performed in WizardNext()       
        }
        
     catch (DMLException e)
        {                   
            ApexPages.addMessages(e); 
            WizardStep --; // do not advance to final page with errors.  
        }
    finally{}
     
    }
    
    public boolean isValid(integer currentStep)
    {               
            integer returnToStep = currentStep -1 ;
                                                        
            ///// VALIDATE STEP 2 - MSO Prime Contact Email ///////////////////         
            if (currentStep ==2){
                                                                                                                
                Pattern MyPattern = Pattern.compile('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}');                             
                Matcher MyMatcher = MyPattern.matcher(sMPersonEmailAddress);
                
                if (!MyMatcher.matches()) {
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid email address. The email address below is not valid.');
                    ApexPages.addMessage(myMsg);
                    return false;
                }       
                                                            
                List<Account> CheckEmail = [SELECT ID FROM Account WHERE PersonEmail = :sMPersonEmailAddress];          
                if (CheckEmail.size() > 0) {                
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a unique email address. The email address below is already in use.');
                    ApexPages.addMessage(myMsg);
                    return false;               
                }                       
            }                           
            ///// End VALIDATE STEP 2 - MSO Prime Contact Email ///////////////////                                                 
                            
                            
            ///// VALIDATE STEP 5 - MSO Prime Contact Email ///////////////////         
            if (currentStep == 5){
                if (selectedMSO_Name == null || selectedMSO_Name == '') {
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select an MSO.');
                    ApexPages.addMessage(myMsg);
                    return false;               
                }           
            }               
                            
                            
            ///// VALIDATE STEP 6 - Provider Prime Contact Email ///////////////////            
            if (currentStep ==6){
                                                                                                                
                Pattern MyPattern = Pattern.compile('[a-zA-Z0-9._-]+@[a-zA-Z]+.[a-zA-Z]{2,4}');                             
                Matcher MyMatcher = MyPattern.matcher(sPersonEmailAddress);
                
                if (!MyMatcher.matches()) {
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid email address. The email address below is not valid.');
                    ApexPages.addMessage(myMsg);
                    return false;
                }       
                                                            
                List<Account> CheckEmail = [SELECT ID FROM Account WHERE PersonEmail = :sPersonEmailAddress];           
                if (CheckEmail.size() > 0) {                
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a unique email address. The email address below is already in use.');
                    ApexPages.addMessage(myMsg);
                    return false;               
                }                       
            }                           
            ///// End VALIDATE STEP 6 - Provider Prime Contact Email ///////////////////                
                                                
                        
            ///// VALIDATE STEP 7 - Provider Demographics ///////////////////           
            if (currentStep ==7){
                // Validate program revenue year is a number value
                Pattern isnumbers = Pattern.Compile('^[0-9]+$');
                Matcher numberMatch = isnumbers.matcher(newProvider.Revenue_Year_Submitted__c);
                
                if(!numberMatch.Matches())
                {
                  // return to error step, display error message, and return false
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid program revenue year.');
                    ApexPages.addMessage(myMsg);
                    return false;
                }
                                            
                                            
                // Validate program revenue year within 3 years         
                integer year;
                year = integer.valueof(newProvider.Revenue_Year_Submitted__c.trim());
                
                if( year < datetime.now().year() - 3 || year > datetime.now().year()) 
                {
                    // return to error step, display error message, and return false
                    WizardStep = returnToStep;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a current program revenue year. The program revenue must be within the last 3 years.');
                    ApexPages.addMessage(myMsg);
                    return false;
                }
            }
            ///// END VALIDATE STEP 7 - Provider Demographics ///////////////////
                                                
            return true;                                                                            
        
    }
    
    
}