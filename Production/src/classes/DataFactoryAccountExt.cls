/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class provides various functions for creating and inserting Accounts in addition
 * to those found in NU.DataFactoryAccount. It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Accounts without inserting them. The insert* functions create Accounts
 * and insert them.
 */
public with sharing class DataFactoryAccountExt {
    public static final String DEFAULT_BUSINESS_ACCOUNT_NAME = 'Test Provider';
    public static final String DEFAULT_FIRST_NAME = 'FirstName';
    public static final String DEFAULT_LAST_NAME = 'LastName';
    public static final String DEFAULT_ACCOUNT_STATUS = Constant.ACCOUNT_STATUS_ACTIVE;
    
    public static final String DEFAULT_BILLING_STREET = '656 High Road';
    public static final String DEFAULT_BILLING_CITY = 'LeadingVille';
    public static final String DEFAULT_BILLING_STATE = 'NY';
    public static final String DEFAULT_BILLING_POSTAL_CODE = '14555';
    
    private static Integer increment = 0;
    
    private static String Increment() {
        return String.valueOf(++increment);
    }

    public static Account createProviderAccount(Decimal programServiceRevenue){
        Account providerAccount = createBusinessAccount(Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID);
        providerAccount.Program_Service_Revenue__c = programServiceRevenue;
        
        return providerAccount;
    }
    
    public static Account createProviderAccount(Decimal programServiceRevenue, Id primaryAffiliationId){
        Account providerAccount = createProviderAccount(programServiceRevenue);
        providerAccount.NU__PrimaryAffiliation__c = primaryAffiliationId;
        
        return providerAccount;
    }
    
    public static Account createMultiSiteEnabledUnderConstructionProviderAccount(Decimal programServiceRevenue){
        Account providerAccount = createProviderAccount(programServiceRevenue);
        providerAccount.Under_Construction__c = true;
        
        return providerAccount;
    }
    
    public static Account createProviderLinkedToStatePartnerAccount(Decimal programServiceRevenue, Id statePartnerId){
        Account providerAccount = createProviderAccount(programServiceRevenue);
        providerAccount.State_Partner_ID__c = statePartnerId;
        
        return providerAccount;
    }
    
    public static Account createLTQAAccount(Decimal annualOperatingBudget){
        return null;
    }
    
    public static Account createMultiSiteAccount(Decimal multiSiteProgramServiceRevenue){
        Account multiSiteAccount = createBusinessAccount(Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID);
        multiSiteAccount.Multi_Site_Program_Service_Revenue__c = multiSiteProgramServiceRevenue;
        
        return multiSiteAccount;
    }
    
    public static Account createCorporateMultiSiteAccount(Decimal multiSiteProgramServiceRevenue){
        Account corporateMSO = createMultiSiteAccount(multiSiteProgramServiceRevenue);
        corporateMSO.Multi_Site_Corporate__c = true;
        
        return corporateMSO;
    }
    
    public static Account createMultiSiteWithJointBillingAccount(Decimal multiSiteProgramServiceRevenue){
        Account multiSiteAccount = createMultiSiteAccount(multiSiteProgramServiceRevenue);
        multiSiteAccount.Multi_Site_Bill_Jointly__c = true;
        
        return multiSiteAccount;
    }

    public static Account createBusinessAccount(Id businessAccountRecordTypeId){
        return new Account(
            RecordTypeId = businessAccountRecordTypeId,
            Name = DEFAULT_BUSINESS_ACCOUNT_NAME + Increment(),
            BillingStreet = DEFAULT_BILLING_STREET,
            BillingCity = DEFAULT_BILLING_CITY,
            BillingState = DEFAULT_BILLING_STATE,
            BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
            NU__Status__c = DEFAULT_ACCOUNT_STATUS
        );
    }
    
    public static List<Account> createBusinessAccounts(Id businessAccountRecordTypeId, Integer numToCreate){
        system.assert(numToCreate != null);
        system.assert(numToCreate > 0);
        system.assert(businessAccountRecordTypeId != null);

        list<Account> acctsToCreate = new List<Account>();
        
        for (Integer createIndex = 0; createIndex < numToCreate; ++createIndex){
            Account businessAccount = createBusinessAccount(businessAccountRecordTypeId);
            
            acctsToCreate.add(businessAccount);
        }
        
        return acctsToCreate;
    }
    
    public static Account createPersonAccount(Id personAccountRecordTypeId){
        Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
    
        return new Account(
            RecordTypeId = personAccountRecordTypeId,
            FirstName = DEFAULT_FIRST_NAME,
            LastName = DEFAULT_LAST_NAME + Increment(),
            PersonEmail = DEFAULT_LAST_NAME + Increment() + '@test' + pwd + '.com',
            BillingStreet = DEFAULT_BILLING_STREET,
            BillingCity = DEFAULT_BILLING_CITY,
            BillingState = DEFAULT_BILLING_STATE,
            BillingPostalCode = DEFAULT_BILLING_POSTAL_CODE,
            NU__Status__c = DEFAULT_ACCOUNT_STATUS
        );
    }
    
    public static List<Account> createPersonAccounts(Id personAccountRecordTypeId, Integer numToCreate){
        system.assert(numToCreate != null);
        system.assert(numToCreate > 0);
        system.assert(personAccountRecordTypeId != null);

        list<Account> acctsToCreate = new List<Account>();
        
        for (Integer createIndex = 0; createIndex < numToCreate; ++createIndex){
            Account personAccount = createPersonAccount(personAccountRecordTypeId);
            
            acctsToCreate.add(personAccount);
        }
        
        return acctsToCreate;
    }
    
    public static Account insertBusinessAccount(Id businessAccountRecordTypeId){
        return insertBusinessAccounts(businessAccountRecordTypeId, 1)[0];
    }
    
    public static List<Account> insertBusinessAccounts(Id businessAccountRecordTypeId, Integer numToCreate){
        List<Account> businessAccountsToInsert = createBusinessAccounts(businessAccountRecordTypeId, numToCreate);
        
        insert businessAccountsToInsert;
        return businessAccountsToInsert;
    }
    
    public static Account insertPersonAccount(Id personAccountRecordTypeId){
        return insertPersonAccounts(personAccountRecordTypeId, 1)[0];
    }
    
    public static List<Account> insertPersonAccounts(Id personAccountRecordTypeId, Integer numToCreate){
        List<Account> personAccountsToInsert = createPersonAccounts(personAccountRecordTypeId, numToCreate);
        
        insert personAccountsToInsert;
        return personAccountsToInsert;
    }
    
    public static Account createStatePartnerAccount(){
        RecordType statePartnerAccountRT = AccountRecordTypeUtil.getStatePartnerRecordType();
        
        return createBusinessAccount(statePartnerAccountRT.Id);
    }
    
    public static Account insertStatePartnerAccount(){
        RecordType statePartnerAccountRT = AccountRecordTypeUtil.getStatePartnerRecordType();
        
        return insertBusinessAccount(statePartnerAccountRT.Id);
    }
    
    public static Account insertStatePartnerPortalAccount(Id statePartnerAccountId){
        RecordType statePartnerPortalRT = AccountRecordTypeUtil.getStatePartnerPortalRecordType();
        
        Account statePartnerPortalAccount = createBusinessAccount(statePartnerPortalRT.Id);
        statePartnerPortalAccount.State_Partner_Id__c = statePartnerAccountId;
        //statePartnerPortalAccount.OwnerId = ownerUserId;
        
        insert statePartnerPortalAccount;
        
        return statePartnerPortalAccount;
    }
    
    public static Account insertStatePartnerPortalAccount(){
        RecordType statePartnerPortalRT = AccountRecordTypeUtil.getStatePartnerPortalRecordType();
        
        Account statePartnerPortalAccount = createBusinessAccount(statePartnerPortalRT.Id);
        
        insert statePartnerPortalAccount;
        return statePartnerPortalAccount;
    }
    
    public static Account insertMultiSiteAccount(){
        RecordType multiSiteAccountRT = AccountRecordTypeUtil.getMultiSiteRecordType();
        
        return insertBusinessAccount(multiSiteAccountRT.Id);
    }
    
    public static Account insertProviderAccount(){
        RecordType providerRT = AccountRecordTypeUtil.getProviderRecordType();
        
        return insertBusinessAccount(providerRT.Id);
    }
    
    public static Account insertCompanyAccount(){
        RecordType companyRT = AccountRecordTypeUtil.getCompanyRecordType();
        
        return insertBusinessAccount(companyRT.Id);
    }
    
    public static Account insertIndividualAccount(){
        RecordType individualRT = AccountRecordTypeUtil.getIndividualRecordType();
        
        return insertPersonAccount(individualRT.Id);
    }
    
    public static Account insertIndividualAssociateAccount(){
        RecordType individualAssociateRT = AccountRecordTypeUtil.getIndividualAssociateRecordType();
        
        return insertPersonAccount(individualAssociateRT.Id);
    }
    
    public static Account insertProviderAccount(Decimal programServiceRevenue){
        Account providerAccount = createProviderAccount(programServiceRevenue);
        insert providerAccount;
        
        return providerAccount;
    }
    
    public static Account insertProviderAccount(Decimal programServiceRevenue, Id primaryAffiliationId){
        Account providerAccount = createProviderAccount(programServiceRevenue, primaryAffiliationId);
        insert providerAccount;
        
        return providerAccount;
    }
    
    public static Account insertProviderLinkedToStatePartnerAccount(Decimal programServiceRevenue, Id statePartnerId){
        Account providerAccount = createProviderLinkedToStatePartnerAccount(programServiceRevenue, statePartnerId);
        
        insert providerAccount;
        
        return providerAccount;
    }
    
    public static Account insertProviderLinkedToStatePartnerAccount(Decimal programServiceRevenue, Id statePartnerId, Date providerJoinDate){
        Account providerAccount = createProviderLinkedToStatePartnerAccount(programServiceRevenue, statePartnerId);
        providerAccount.Provider_Join_On__c = providerJoinDate;
        
        insert providerAccount;
        
        return providerAccount;
    }
    
    public static Account insertLTQAAccount(Decimal annualOperatingBudget){
        return null;
    }
    
    public static Account insertMultiSiteAccount(Decimal multiSiteProgramServiceRevenue){
        Account multiSiteAccount = createMultiSiteAccount(multiSiteProgramServiceRevenue);
        insert multiSiteAccount;
        
        return multiSiteAccount;
    }
    
    public static Account insertMultiSiteAccount(Decimal multiSiteProgramServiceRevenue, Id statePartnerId){
        Account multiSiteAccount = createMultiSiteAccount(multiSiteProgramServiceRevenue);
        multiSiteAccount.State_Partner_Id__c = statePartnerId;
        
        insert multiSiteAccount;
        
        return multiSiteAccount;
    }
    
    public static Account insertMultiSiteEnabledProviderAccount(Decimal programServiceRevenue){
        return insertMultiSiteEnabledProviderAccounts(new List<Decimal>{ programServiceRevenue })[0];
    }
    
    public static Account insertMultiSiteWithJointBillingAccount(Decimal multiSiteProgramServiceRevenue){
        Account multiSiteAccount = createMultiSiteWithJointBillingAccount(multiSiteProgramServiceRevenue);
        insert multiSiteAccount;
        
        return multiSiteAccount;
    }
    
    public static List<Account> insertMultiSiteEnabledProviderAccounts(List<Decimal> programServiceRevenues){
        return insertMultiSiteEnabledProviderAccounts(programServiceRevenues, null);
    }
    
    public static List<Account> insertMultiSiteEnabledProviderAccounts(List<Decimal> programServiceRevenues, Id msoId){
        List<Account> multiSiteEnabledProviderAccounts = new List<Account>();
        
        for (Decimal programServiceRevenue : programServiceRevenues){
            Account multiSiteEnabledProviderAccount = createProviderAccount(programServiceRevenue);
            multiSiteEnabledProviderAccount.Multi_Site_Enabled__c = true;
            multiSiteEnabledProviderAccount.NU__PrimaryAffiliation__c = msoId;
            
            multiSiteEnabledProviderAccounts.add(multiSiteEnabledProviderAccount);
        }
        
        insert multiSiteEnabledProviderAccounts;
        
        return multiSiteEnabledProviderAccounts;
    }
    
    public static Account insertCorporateMultiSiteAccount(Decimal msoProgramServiceRevenue, Id statePartnerId){
        Account corporateMSO = createCorporateMultiSiteAccount(msoProgramServiceRevenue);
        corporateMSO.State_Partner_Id__c = statePartnerId;
        
        insert corporateMSO;
        return corporateMSO;
    }
}