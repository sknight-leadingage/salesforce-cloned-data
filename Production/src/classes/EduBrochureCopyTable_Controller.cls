public with sharing class EduBrochureCopyTable_Controller extends EduReportsControllerBase {
    transient List<Conference_Session__c> DataList { get;set; }
    public List<Conference_Session__c> getDataList() {
        if (DataList == null || DataList.size() == 0) {
            SetQuery();
        }
        return DataList;
    }
    
    public integer DataListSize {
        get {
            if (DataList == null) {
                return 0;
            } else {

                return DataList.size();
            }
        }
    }
    
    
    public override void SetPageType()
    {
        PageCType = 'contentType="application/vnd.ms-excel#EduBrochureCopyTable.xls"';
    }
    
    public override string getPageTitle() {
        return RptRenderAs == '' && Conference != null ? Conference.Name + ': Brochure Copy Table' : '';
    }
    
    public EduBrochureCopyTable_Controller() {
        PageCType = '';
     //   SetConPageSize = 70;
        SetQuery();
    }
 /*   
    public override void SetConRefreshData() {
        DataList = setCon.getRecords();
    }
    
  */  
    
    public void SetQuery() {
        SetConQuery = '';
      if (SetConQuery == '') {
        SetConQuery = 'SELECT Session_Number__c, Title__c, Timeslot__r.Timeslot_Code__c, Timeslot__r.Session_Date__c, Timeslot__r.Session_Time__c, Learning_Objective_1__c, Learning_Objective_2__c, Learning_Objective_3__c, NASBA__r.Name, PTF_Code__c, (SELECT Speaker__r.Name, Speaker__r.Speaker_Company_Name_Rollup__c, Speaker__r.PersonTitle, Speaker__r.ShippingCity, Speaker__r.ShippingState FROM Conference_Session_Speakers__r)  FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
		   	DataList = database.query(SetConQuery);
/*		setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
*/
    }
}