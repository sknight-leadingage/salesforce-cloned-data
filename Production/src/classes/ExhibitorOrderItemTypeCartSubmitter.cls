global with sharing class ExhibitorOrderItemTypeCartSubmitter implements NU.IOrderItemTypeCartSubmitter{
	Map<Id, Exhibitor__c> cartItemLineExhibitorMap;
    Map<Exhibitor__c, NU__OrderItemLine__c> exhibitorsToUpdateMap;
    
    global NU.OperationResult beforeOrderItemSaved(NU__CartItem__c cartItem, NU__OrderItem__c orderItem) {  
        if (cartItemLineExhibitorMap == null) {
            cartItemLineExhibitorMap = new Map<Id, Exhibitor__c>();
        }
        
        List<Id> ids = new List<Id>();
        for (NU__CartItemLine__c cartItemLine : cartItem.NU__CartItemLines__r) {
            ids.add(cartItemLine.Id);
        }
        List<NU__CartItemLine__c> exhibitorCartItemLines = [SELECT Id,
            NU__OrderItemLine__r.Exhibitor__c,
            NU__Product__c,
            NU__Product__r.Event__c,
            NU__Product__r.NU__Description__c,
            NU__Data__c,
            Booked_By__c,
            Booked_Date__c,
            Booth_Number__c
            FROM NU__CartItemLine__c
            WHERE Id IN :ids];


        for (NU__CartItemLine__c cartItemLine : exhibitorCartItemLines) {
            Exhibitor__c exhibitor = createExhibitorFromCartItemLine(cartItem, cartItemLine);
            cartItemLineExhibitorMap.put(cartItemLine.Id, exhibitor);
        }

        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global NU.OperationResult afterOrderItemsSaved() {

        if (cartItemLineExhibitorMap != null && cartItemLineExhibitorMap.isEmpty() == false) {
            System.debug(cartItemLineExhibitorMap.values());
            upsert cartItemLineExhibitorMap.values();
        }

        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global void beforeOrderItemLineSaved(NU__CartItemLine__c cartItemLine, NU__OrderItemLine__c orderItemLineToSave) {
        if (exhibitorsToUpdateMap == null) {
            exhibitorsToUpdateMap = new Map<Exhibitor__c, NU__OrderItemLine__c>();
        }
        
        if (cartItemLineExhibitorMap != null && cartItemLineExhibitorMap.containsKey(cartItemLine.Id)){
            Exhibitor__c exhibitor = cartItemLineExhibitorMap.get(cartItemLine.Id);
            orderItemLineToSave.Exhibitor__c = exhibitor.Id;
            exhibitorsToUpdateMap.put(exhibitor, orderItemLineToSave);
        }
    }

    global NU.OperationResult afterOrderItemLinesSaved() {

        if (exhibitorsToUpdateMap != null && !exhibitorsToUpdateMap.isEmpty()) {
            for (Exhibitor__c exhibitor : exhibitorsToUpdateMap.keySet()) {
                exhibitor.Order_Item_Line__c = exhibitorsToUpdateMap.get(exhibitor).Id;
            }
            
            List<Exhibitor__c> exhibitorsToUpdate = new List<Exhibitor__c>(exhibitorsToUpdateMap.keySet());
            upsert exhibitorsToUpdate;
        }
        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global void populateCartSubmissionResult(NU.CartSubmissionResult result) {
        
    }
    
    private Exhibitor__c createExhibitorFromCartItemLine(NU__CartItem__c exhibitorCI, NU__CartItemLine__c exhibitorCIL) {
        Id exhibitorId = null;
        
        if (exhibitorCIL.NU__OrderItemLine__c != null) {
            exhibitorId = exhibitorCIL.NU__OrderItemLine__r.Exhibitor__c;
        }
        
        Exhibitor__c exhibitor = new Exhibitor__c(
                Id = exhibitorId,
                Account__c = exhibitorCI.NU__Customer__c,
                Product__c = exhibitorCIL.NU__Product__c,
                Booked_By__c = exhibitorCIL.Booked_By__c,
                Booked_Date__c = exhibitorCIL.Booked_Date__c,
                Booth_Number__c = exhibitorCIL.Booth_Number__c
            );
        
        if (exhibitor.Id == null){
        	exhibitor.Description__c = exhibitorCIL.NU__Product__r.NU__Description__c;
        	exhibitor.Event__c = exhibitorCIL.NU__Product__r.Event__c;
        }
        
        return exhibitor;
    }
}