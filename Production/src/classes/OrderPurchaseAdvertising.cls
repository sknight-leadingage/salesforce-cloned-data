public with sharing class OrderPurchaseAdvertising extends OrderPurchaseBase {
	public override String getRecordType(){
    	return Constant.ORDER_RECORD_TYPE_ADVERTISING;
    }
}