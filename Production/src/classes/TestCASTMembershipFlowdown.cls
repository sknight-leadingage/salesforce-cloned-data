/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the CAST membership flowdown logic.
 */
@isTest
private class TestCASTMembershipFlowdown {

    static testmethod void providerMembershipFlowDownOneLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownOneLevelTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipFlowDownOneLevelMultipleAcctsTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownOneLevelMultipleAcctsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNullFlowDownOneLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipNullFlowDownOneLevelTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownFromPersonToPersonTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownFromPersonToPersonTest(
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownFromPersonToBusinessTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownFromPersonToBusinessTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownToNonPrimaryAffiliationsTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownToNonPrimaryAffiliationsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownWithIndividualMembershipTypeTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownToNonPrimaryAffiliationsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestFakePersonProviderMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }
    
    static testmethod void newPrimaryAffiliationTest() {
    	
    	TestMembershipFlowdownUtil.newPrimaryAffiliationTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newIndivToIndivPrimaryAffilWithNoFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.newIndivToIndivPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newIndivToOrgPrimaryAffilWithNoFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.newIndivToOrgPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deletePrimaryAffilErasingMembershipFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.deletePrimaryAffilErasingMembershipFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deleteNonPrimaryAffilWithNoErasingMembershipFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.deleteNonPrimaryAffilWithNoErasingMembershipFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void changeAffiliationToPrimaryWithoutExistingFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.changeAffiliationToPrimaryWithoutExistingFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

	static testmethod void changeAffiliationToNonPrimaryWithExistingFlowdownTest(){
		
		TestMembershipFlowdownUtil.changeAffiliationToNonPrimaryWithExistingFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }
    
    static testmethod void membershipToIndividualFlowDownButNoCompanyFlowdownTest(){
    	TestMembershipFlowdownUtil.membershipToIndividualFlowDownButNoCompanyFlowdownTest(
    		Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void membershipToIndividualFlowdownButNoCompanyFlowdownTwoLevelsTest(){
    	TestMembershipFlowdownUtil.membershipToIndividualFlowdownButNoCompanyFlowdownTwoLevelsTest(
    		Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestCASTMembershipInserter(),
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }

	/*@isTest(SeeAllData=true)
	static void insertNCRCastMembershipTest(){
		Account nationalChurchResidencesAccount = [select id, name from Account where name = 'National Church Residences' and LeadingAge_ID__c = '133748'];
		
		NU__MembershipType__c castMT = [select id, name from NU__MembershipType__c where name = :Constant.CAST_MEMBERSHIP_TYPE_NAME];
		
		
		NU__Membership__c currentCASTMembership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(nationalChurchResidencesAccount.Id, castMT.Id);
		insert currentCASTMembership;
	}*/
}