/**
 * @author NimbleUser
 * @date Updated: 2/6/13
 * @description This class has various test methods to test the MultiSite Program Service Revenue
 * Aggregator Controller.
 */
@isTest
private class TestMultiSitePSRAggregatorController {

    static testMethod void aggregationSuccessfulTest() {
    	Account multiSite = DataFactoryAccountExt.insertMultiSiteAccount();
        
        List<Decimal> programServiceRevenues = new List<Decimal>{ 15000, 40000 };
        List<Account> multiSiteProviderAccounts = DataFactoryAccountExt.insertMultiSiteEnabledProviderAccounts(programServiceRevenues);
        Set<Id> multiSiteProviderAccountIds = NU.CollectionUtil.getSobjectIds(multiSiteProviderAccounts);
        
        NU.DataFactoryAffiliation.insertPrimaryAffiliations(multiSite.Id, multiSiteProviderAccountIds);
    	
    	
        MultiSitePSRAggregatorController aggController = new MultiSitePSRAggregatorController(new ApexPages.StandardController(multiSite));
        aggController.Calculate();
        
        TestMultiSitePSRAggregator.assertMultiSiteProgramServiceRevenue(multiSite.Id, 55000);
    }
}