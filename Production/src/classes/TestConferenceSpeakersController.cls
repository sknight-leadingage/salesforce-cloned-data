/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceSpeakersController {

	static Conference_Session__c session = null;
	
	static ConferenceSpeakersController loadController(){
		PageReference pageRef = Page.ConferenceSpeakers;
    	Test.setCurrentPage(pageRef);
    	
    	session = DataFactoryConferenceSession.insertConferenceSession();
    	
    	pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, session.Conference__c);
    	
    	ConferenceSpeakersController speakersController = new ConferenceSpeakersController();
    	
    	return speakersController;
	}

    static testMethod void CurrentSpeakerTest() {
        ConferenceSpeakersController speakersController = loadController();
        system.assert(speakersController.CurrentSpeaker != null);
    }
    
    static testMethod void CurrentSpeakerEventSessionsTest() {
        ConferenceSpeakersController speakersController = loadController();
        system.assert(speakersController.CurrentSpeakerEventSessions.size() > 0);
    }
    
    static testMethod void noAvailableEventSessionsForCurrentSpeakerSelectOptsTest() {
        ConferenceSpeakersController speakersController = loadController();
        system.assert(speakersController.AvailableEventSessionsForCurrentSpeakerSelectOpts.size() == 0);
    }
    
    static testmethod void noSpeakersFoundTest(){
    	ConferenceSpeakersController speakersController = loadController();
    	speakersController.firstNameToSearch = 'non existent first name';
    	speakersController.searchForSpeakers();
    	
    	system.assert(speakersController.CurrentSpeaker == null);
    }
    
    static testmethod void clearSearchTest(){
    	ConferenceSpeakersController speakersController = loadController();
    	speakersController.firstNameToSearch = 'non existent first name';
    	
    	system.assert(speakersController.hasSpeakerSearchCriteria);
    	
    	speakersController.clearSearch();
    	
    	system.assert(speakersController.hasSpeakerSearchCriteria == false);
    }
    
    static testmethod void deleteSessionSpeakerTest(){
    	ConferenceSpeakersController speakersController = loadController();
    	
		Conference_Session_Speaker__c sessionSpeaker = speakersController.CurrentSpeakerEventSessions[0];
    	
    	speakersController.SessionSpeakerIdToDelete = sessionSpeaker.Id;
    	
    	speakersController.deleteSessionSpeaker();
    }
    
    static testmethod void addSessionSpeakerTest(){
    	ConferenceSpeakersController speakersController = loadController();
    	speakersController.addSessionSpeaker();
    }
}