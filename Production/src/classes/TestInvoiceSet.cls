/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestInvoiceSet {
	private static NU__Order__c testOrder = null;
	
	private static NU__Order__c getOrderById(Id orderId){
		return
		[select id,
		        name,
		        NU__InvoiceDate__c,
		        NU__InvoiceGenerated__c,
		        Invoice_Attention_To__c
		   from NU__Order__c
		  where id = :orderId];
	}
	
	private static InvoiceSet createInvoiceSet(){
		testOrder = NU.DataFactoryOrder.insertOrder();
		
		String orderId = testOrder.Id;
		String query = 'select Id from NU__Order__c where Id = :OrderId';
		ApexPages.StandardSetController orderInvoiceSSC = new ApexPages.StandardSetController(Database.getQueryLocator(query));
		
		InvoiceSet invSet = new InvoiceSet(orderInvoiceSSC);
		
		return invSet;
	} 

    static testMethod void constructorTest() {
		InvoiceSet invSet = createInvoiceSet();
		
		system.assertEquals(false, invSet.ShowEmailButton);
    }
    
    static testmethod void printTest(){
    	InvoiceSet invSet = createInvoiceSet();
		invSet.Orders = new List<NU__Order__c>{ testOrder };
		
		invSet.dummyOrder.NU__InvoiceDate__c = Date.Today();
		invSet.Print();
		
		NU__Order__c queriedOrder = getOrderById(testOrder.Id);
		
		system.assertEquals(invSet.dummyOrder.NU__InvoiceDate__c, queriedOrder.NU__InvoiceDate__c);
		system.assertEquals(true, queriedOrder.NU__InvoiceGenerated__c);
    }
    
    static testmethod void updateAttentionToTest(){
    	InvoiceSet invSet = createInvoiceSet();
		invSet.Orders = new List<NU__Order__c>{ testOrder };
		
		invSet.dummyOrder.Invoice_Attention_To__c = 'luke freelander';
		
		invSet.updateAttentionTo();
		
		NU__Order__c queriedOrder = getOrderById(testOrder.Id);
		system.assertEquals(invSet.dummyOrder.Invoice_Attention_To__c, queriedOrder.Invoice_Attention_To__c);
    }
}