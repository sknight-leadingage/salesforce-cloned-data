/*
* Test methods for the LeadershipApplicationsMgmt_Controller
*/
@isTest(SeeAllData=true)

private class TestLeadershipApplicationsMgmtController{

    static LeadershipApplicationsMgmt_Controller loadController(){
        PageReference pageRef = Page.LeadershipApplicationsMgmt;
        Test.setCurrentPage(pageRef);
        
        string SelectedAcademyYear = '2016';
        
        pageRef.getParameters().put(LeadershipAcademyControllerBase.ACADEMY_YEAR_PARAM, SelectedAcademyYear);
        
        LeadershipApplicationsMgmt_Controller AppController = new LeadershipApplicationsMgmt_Controller();
        
        return AppController;
     }
    
   
}