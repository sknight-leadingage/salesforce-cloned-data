/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestStandardQuickBooksBatchExporter {
	
	static List<NU__Transaction__c> insertTransactions(){
		NU__Batch__c insertManualPostedBatch = NU.DataFactorybatch.insertManualPostedBatch();
		Account billTo = NU.DataFactoryAccount.insertIndividualAccount();
		
		List<NU__Transaction__c> transactionsToInsert = new List<NU__Transaction__c>();
		
		NU__GLAccount__c revenueGL = NU.DataFactoryGLAccount.insertRevenueGLAccount(insertManualPostedBatch.NU__Entity__c);
		NU__GLAccount__c arGL = NU.DataFactoryGLAccount.insertARGLAccount(insertManualPostedBatch.NU__Entity__c);
		
		NU__Transaction__c revenueTrans1 = new NU__Transaction__c(
            NU__Amount__c = 50,
            NU__Batch__c = insertManualPostedBatch.Id,
            NU__BillTo__c = billTo.Id,
            NU__Category__c = 'Credit',
            NU__Customer__c = billTo.Id,
            NU__Date__c = Date.Today(),
            NU__Entity__c = insertManualPostedBatch.NU__Entity__c,
            NU__GLAccount__c = revenueGL.Id,
            NU__OrderType__c = 'New',
            NU__ProductCode__c = 'Revenue',
            NU__ReferenceGroup__c = 1,
            NU__ReferenceNumber__c = 1,
            NU__Source__c = 'Salesforce',
            NU__Type__c = 'Income'
        );
        
        transactionsToInsert.add(revenueTrans1);
        
        NU__Transaction__c revenueTrans2 = new NU__Transaction__c(
            NU__Amount__c = 150,
            NU__Batch__c = insertManualPostedBatch.Id,
            NU__BillTo__c = billTo.Id,
            NU__Category__c = 'Credit',
            NU__Customer__c = billTo.Id,
            NU__Date__c = Date.Today(),
            NU__Entity__c = insertManualPostedBatch.NU__Entity__c,
            NU__GLAccount__c = revenueGL.Id,
            NU__OrderType__c = 'New',
            NU__ProductCode__c = 'Revenue',
            NU__ReferenceGroup__c = 1,
            NU__ReferenceNumber__c = 1,
            NU__Source__c = 'Salesforce',
            NU__Type__c = 'Income'
        );
        
        transactionsToInsert.add(revenueTrans2);
        
        NU__Transaction__c arTrans1 = new NU__Transaction__c(
            NU__Amount__c = revenueTrans1.NU__Amount__c + revenueTrans2.NU__Amount__c,
            NU__Batch__c = insertManualPostedBatch.Id,
            NU__BillTo__c = billTo.Id,
            NU__Category__c = 'Debit',
            NU__Customer__c = billTo.Id,
            NU__Date__c = Date.Today(),
            NU__Entity__c = insertManualPostedBatch.NU__Entity__c,
            NU__GLAccount__c = arGL.Id,
            NU__OrderType__c = 'New',
            NU__ProductCode__c = 'AR',
            NU__ReferenceGroup__c = 1,
            NU__ReferenceNumber__c = 1,
            NU__Source__c = 'Salesforce',
            NU__Type__c = 'AR'
        );
        
        transactionsToInsert.add(arTrans1);
        
        NU__Transaction__c paymentTrans1 = new NU__Transaction__c(
            NU__Amount__c = arTrans1.NU__Amount__c,
            NU__Batch__c = insertManualPostedBatch.Id,
            NU__BillTo__c = billTo.Id,
            NU__Category__c = 'Credit',
            NU__Customer__c = billTo.Id,
            NU__Date__c = Date.Today(),
            NU__Entity__c = insertManualPostedBatch.NU__Entity__c,
            NU__GLAccount__c = revenueGL.Id,
            NU__OrderType__c = 'New',
            NU__ProductCode__c = 'Revenue',
            NU__ReferenceGroup__c = 1,
            NU__ReferenceNumber__c = 1,
            NU__Source__c = 'Salesforce',
            NU__Type__c = 'Income'
        );
        
        transactionsToInsert.add(paymentTrans1);
        
        NU__Transaction__c zeroDollarRevenue1 = new NU__Transaction__c(
            NU__Amount__c = 0,
            NU__Batch__c = insertManualPostedBatch.Id,
            NU__BillTo__c = billTo.Id,
            NU__Category__c = 'Debit',
            NU__Customer__c = billTo.Id,
            NU__Date__c = Date.Today(),
            NU__Entity__c = insertManualPostedBatch.NU__Entity__c,
            NU__GLAccount__c = arGL.Id,
            NU__OrderType__c = 'New',
            NU__ProductCode__c = 'NoRevenue',
            NU__ReferenceGroup__c = 1,
            NU__ReferenceNumber__c = 1,
            NU__Source__c = 'Salesforce',
            NU__Type__c = 'Income'
        );
        
        transactionsToInsert.add(zeroDollarRevenue1);
		
		
		insert transactionsToInsert;
		
		return transactionsToInsert;
	}
	
	static set<Id> getBatchIds(List<NU__Transaction__c> trans){
		Set<id> ids = new Set<id>();
		
		for (NU__Transaction__c tran : trans){
			ids.add(tran.NU__Batch__c);
		}
		
		return ids;
	}

    static testMethod void getPreviewComponentTest() {
    	List<NU__Transaction__c> trans = insertTransactions();
    	Set<Id> batchIdsToPreview = getBatchIds(trans);
    	
        StandardQuickBooksBatchExporter exporter = new StandardQuickBooksBatchExporter();
        
        Component.Apex.OutputPanel previewPanel = exporter.getPreviewComponent(batchIdsToPreview);
    }
    
    static testMethod void exportTest() {
    	List<NU__Transaction__c> trans = insertTransactions();
    	Set<Id> batchIdsToExport = getBatchIds(trans);
    	
        StandardQuickBooksBatchExporter exporter = new StandardQuickBooksBatchExporter();
        NU.BatchExportResult exportResult = exporter.export(batchIdsToExport);
        
        system.assert(NU.OperationResult.isSuccessful(exportResult.Result));
        system.assert(exportResult.SuccessView != null);
    }
    
    static testMethod void NoBatchesTest(){
    	Set<Id> noBatchIds = new Set<Id>();
    	
    	StandardQuickBooksBatchExporter exporter = new StandardQuickBooksBatchExporter();
        NU.BatchExportResult exportResult = exporter.export(noBatchIds);
        
        system.assert(NU.OperationResult.isSuccessful(exportResult.Result) == false);
    }
    
    static testMethod void contantsTest(){
    	List<String> contants = new List<String>{
	    	StandardQuickBooksBatchExporter.EXPORT_FOLDER_NAME,
	    
		    StandardQuickBooksBatchExporter.EXPORT_FILE_SPL_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_SPL_ID_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_TRANS_TYPE_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_DATE_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_ACCOUNT_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_NAME_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_MEMO_COL,
		    StandardQuickBooksBatchExporter.EXPORT_FILE_AMOUNT_COL,
		    
		    StandardQuickBooksBatchExporter.EXPORT_FILE_GL_ACCOUNT_COL,
		    StandardQuickBooksBatchExporter.DETAIL_FILE_AMOUNT_COL,
		    StandardQuickBooksBatchExporter.DETAIL_FILE_DESCRIPTION_COL,
		    StandardQuickBooksBatchExporter.DETAIL_FILE_TRANS_DATE_COL,
		    
		    StandardQuickBooksBatchExporter.TRANSACTION_TYPE_AR,
		    StandardQuickBooksBatchExporter.TRANSACTION_TYPE_PAYMENT,
		    StandardQuickBooksBatchExporter.TRANSACTION_TYPE_INCOME,
		    
		    StandardQuickBooksBatchExporter.TRANSACTION_CATEGORY_DEBIT,
		    StandardQuickBooksBatchExporter.TRANSACTION_CATEGORY_CREDIT,
		    
		    StandardQuickBooksBatchExporter.NEWLINE,
		    StandardQuickBooksBatchExporter.TAB_DELIMITER,
		
		    StandardQuickBooksBatchExporter.TRANS,
		    StandardQuickBooksBatchExporter.SPL,
		    StandardQuickBooksBatchExporter.GENERAL_JOURNAL,
		    StandardQuickBooksBatchExporter.NIMBLE_AMS,
		    
		    StandardQuickBooksBatchExporter.NO_BATCH_IDS_TO_EXPORT_ERR_MSG
    	};
    }
}