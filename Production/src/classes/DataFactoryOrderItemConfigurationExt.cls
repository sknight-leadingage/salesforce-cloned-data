public with sharing class DataFactoryOrderItemConfigurationExt {
	
	public static NU__OrderItemConfiguration__c insertSponsorshipOrderItemConfigurationWithEntityOrderItem(Id entityId, Id arGLAccountId){
		NU__OrderItemConfiguration__c sponsorshipOIC = new NU__OrderItemConfiguration__c(
		    Name = 'Sponsorship',
            NU__OrderPageRelativeURL__c = '/apex/C__OrderSponsorshipProducts',
            NU__CartSubmitterClass__c = 'SponsorshipOrderItemTypeCartSubmitter',
            NU__ImageRelativeURL__c = '/img/icon/cash24.png',
            NU__Status__c = 'Active');

        insert sponsorshipOIC;
        
        NU__EntityOrderItem__c entityOrderItem = new NU__EntityOrderItem__c(
            NU__ARGLAccount__c = arGLAccountId,
            NU__Entity__c = entityId,
            NU__OrderItemConfiguration__c = sponsorshipOIC.Id);
            
        insert entityOrderItem;
        
        return sponsorshipOIC;
	}

	public static NU__OrderItemConfiguration__c insertAdvertisingOrderItemConfigurationWithEntityOrderItem(Id entityId, Id arGLAccountId){
		NU__OrderItemConfiguration__c advertisingOIC = new NU__OrderItemConfiguration__c(
		    Name = 'Advertising',
            NU__OrderPageRelativeURL__c = '/apex/C__OrderAdvertisingProducts',
            NU__CartSubmitterClass__c = 'AdvertisingOrderItemTypeCartSubmitter',
            NU__ImageRelativeURL__c = '/img/icon/cash24.png',
            NU__Status__c = 'Active');

        insert advertisingOIC;
        
        NU__EntityOrderItem__c entityOrderItem = new NU__EntityOrderItem__c(
            NU__ARGLAccount__c = arGLAccountId,
            NU__Entity__c = entityId,
            NU__OrderItemConfiguration__c = advertisingOIC.Id);
            
        insert entityOrderItem;
        
        return advertisingOIC;
	}
	
	public static NU__OrderItemConfiguration__c insertExhibitorOrderItemConfigurationWithEntityOrderItem(Id entityId, Id arGLAccountId){
		NU__OrderItemConfiguration__c exhibitorOIC = new NU__OrderItemConfiguration__c(
		    Name = 'Exhibitor',
            NU__OrderPageRelativeURL__c = '/apex/C__OrderExhibitorProducts',
            NU__CartSubmitterClass__c = 'ExhibitorOrderItemTypeCartSubmitter',
            NU__ImageRelativeURL__c = '/img/icon/cash24.png',
            NU__Status__c = 'Active');

        insert exhibitorOIC;
        
        NU__EntityOrderItem__c entityOrderItem = new NU__EntityOrderItem__c(
            NU__ARGLAccount__c = arGLAccountId,
            NU__Entity__c = entityId,
            NU__OrderItemConfiguration__c = exhibitorOIC.Id);
            
        insert entityOrderItem;
        
        return exhibitorOIC;
	}
}