public with sharing class JointBillingItemLineTriggerHandlers  extends NU.TriggerHandlersBase {
	
	public override void onAfterInsert(Map<Id, sObject> newRecordMap) {
		updateJointBillingItemsMembershipAmounts(newRecordMap);
	}
	
	public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {
		List<Joint_Billing_Item_Line__c> oldJBILs = (List<Joint_Billing_Item_Line__c>) oldRecordMap.values();
		
		Set<Id> jointBillingItemIdsToUpdate = new Set<Id>();
		
		for (Joint_Billing_Item_Line__c oldJBIL : oldJBILs){
			Joint_Billing_Item_Line__c newJBIL = (Joint_Billing_Item_Line__c) newRecordMap.get(oldJBIL.Id);
			
			if (oldJBIL.Amount__c != newJBIL.Amount__c){
				jointBillingItemIdsToUpdate.add(newJBIL.Joint_Billing_Item__c);
			}
		}
		
		if (jointBillingItemIdsToUpdate.size() > 0){
			updateJointBillingItemsMembershipAmounts(jointBillingItemIdsToUpdate);
		}
	}
	
	public override void onAfterDelete(Map<Id, sObject> oldRecordMap) {
		updateJointBillingItemsMembershipAmounts(oldRecordMap);
    }
    
    private void updateJointBillingItemsMembershipAmounts(Map<Id, sObject> recordMap){
    	List<Joint_Billing_Item_Line__c> JBILs = (List<Joint_Billing_Item_Line__c>) recordMap.values();
		
		Set<Id> jointBillingItemIdsToUpdate = new Set<Id>();
		
		for (Joint_Billing_Item_Line__c jbil : JBILs){
			jointBillingItemIdsToUpdate.add(jbil.Joint_Billing_Item__c);
		}
		
		updateJointBillingItemsMembershipAmounts(jointBillingItemIdsToUpdate);
    }
    
    private void updateJointBillingItemsMembershipAmounts(Set<Id> jointBillingItemIdsToUpdate){
    	List<Joint_Billing_Item__c> jointBillingItems = getJointBillingItemsForMembershipAmountUpdating(jointBillingItemIdsToUpdate);
    	Map<Id, Decimal> jointBillingDuesAmountsMap = getJointBillingDuesAmountsMap(jointBillingItemIdsToUpdate);
    	
    	Map<Id, NU__Membership__c> accountMembershipsMap = getAccountMembershipsMap(jointBillingItems);
    	
    	List<NU__Membership__c> membershipsToUpdate = new List<NU__Membership__c>();
    	
    	for (Joint_Billing_Item__c jbi : jointBillingItems){
    		Decimal jointBillingDuesAmount = jointBillingDuesAmountsMap.get(jbi.Id);
    		
    		NU__Membership__c accountMembership = accountMembershipsMap.get(jbi.Account__c);
    		
    		if (accountMembership != null &&
    		    accountMembership.NU__OrderItemLine__c == null &&
    		    jointBillingDuesAmount != accountMembership.NU__Amount__c){
    		    
    		    accountMembership.NU__ExternalAmount__c = jointBillingDuesAmount;
    		    membershipsToUpdate.add(accountMembership);
		    }
    	}
    	
    	if (membershipsToUpdate.size() > 0){
    		update membershipsToUpdate;
    	}
    }
    
    private List<Joint_Billing_Item__c> getJointBillingItemsForMembershipAmountUpdating(Set<Id> jointBillingItemIdsToUpdate){
    	return [select id,
    	               name,
    	               Account__c,
    	               Amount__c,
    	               Joint_Billing__r.Start_Date__c
    	          from Joint_Billing_Item__c
    	         where id in :jointBillingItemIdsToUpdate];
    }
    
    private Map<Id, Decimal> getJointBillingDuesAmountsMap(Set<Id> jointBillingItemIdsToUpdate){
    	Map<Id, Decimal> jointBillingDuesAmountsMap = new Map<Id, Decimal>();
    	
    	List<AggregateResult> results =
    	[select Joint_Billing_Item__c,
    	        SUM(Amount__c) TotalDuesAmount
    	   from Joint_Billing_Item_Line__c
    	  where Joint_Billing_Item__c in :jointBillingItemIdsToUpdate
    	  group by Joint_Billing_Item__c];
    	  
    	for (AggregateResult jointBillingAmountAR : results){
    		jointBillingDuesAmountsMap.put((Id) jointBillingAmountAR.get('Joint_Billing_Item__c'), (Decimal) jointBillingAmountAR.get('TotalDuesAmount'));
    	}
    	
    	return jointBillingDuesAmountsMap;
    }
    
    private Map<Id, NU__Membership__c> getAccountMembershipsMap(List<Joint_Billing_Item__c> jointBillingItems){
    	Map<Id, NU__Membership__c> accountMembershipsMap = new Map<Id, NU__Membership__c>();
    	
    	Set<Date> startDates = new Set<Date>();
    	
    	for (Joint_Billing_Item__c jbi : jointBillingItems){
    		startDates.add(jointBillingItems[0].Joint_Billing__r.Start_Date__c);
    	}
    	
    	Set<Id> accountIds = NU.CollectionUtil.getLookupIds(jointBillingItems, 'Account__c');
    	
    	List<NU__Membership__c> memberships = 
    	[select id,
    	        name,
    	        NU__ExternalAmount__c,
    	        NU__Amount__c,
    	        NU__Account__c,
    	        NU__OrderItemLine__c
    	   from NU__Membership__c
    	  where NU__StartDate__c in :startDates
    	    and NU__Account__c in :accountIds
    	    and NU__MembershipType__r.Name = :Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME];
    	
    	for (NU__Membership__c membership : memberships){
    		accountMembershipsMap.put(membership.NU__Account__c, membership);
    	}
    	
    	return accountMembershipsMap;
    }
}