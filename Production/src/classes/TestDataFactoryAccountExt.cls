/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the DataFactoryAccountExt class logic.
 */
@isTest
private class TestDataFactoryAccountExt {

	private static void assertAccountInsert(Account insertedAcct, Id expectedAccountRecordTypeId){
		system.assert(insertedAcct != null, 'The inserted account is null');
		system.assert(expectedAccountRecordTypeId != null, 'The expected account record type id is null');
		system.assert(insertedAcct.Id != null, 'The account was instantiated, but not inserted.');
		system.assertEquals(expectedAccountRecordTypeId, insertedAcct.RecordTypeId, 'The account is not the expected record type.');
	}

    static testMethod void insertProviderAccountTest() {
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount();
        assertAccountInsert(providerAccount, AccountRecordTypeUtil.getProviderRecordType().Id );
    }
    
    static testmethod void insertIndividualAccountTest(){
    	Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        assertAccountInsert(individualAccount, AccountRecordTypeUtil.getIndividualRecordType().Id );
    }
    
    static testmethod void insertIndividualAssociateAccountTest(){
    	Account individualAssociateAccount = DataFactoryAccountExt.insertIndividualAssociateAccount();
        assertAccountInsert(individualAssociateAccount, AccountRecordTypeUtil.getIndividualAssociateRecordType().Id );
    }
    
    static testmethod void insertStateProviderAccountTest(){
    	Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
        assertAccountInsert(statePartnerAccount, AccountRecordTypeUtil.getStatePartnerRecordType().Id );
    }
}