@isTest(SeeAllData=false)
public with sharing class TestRegistrationTrigger {

    static testmethod void CancelledRegistrationTest() {
        
        // Test Entity
        NU__Entity__c TestEntity = new NU__Entity__c(name='Test Entity');
        insert TestEntity;
    
    
        //Create Event
        NU__Event__c eve = new NU__Event__c();
        eve.Name = 'Test Event';
        eve.NU__ShortName__c = 'TEST';
        eve.NU__StartDate__c = DateTime.now();
        eve.NU__EndDate__c = DateTime.now().addDays(5);
        eve.NU__City__c = 'Washington';
        eve.NU__StateProvince__c = 'DC';
        eve.NU__Entity__c = TestEntity.Id;
        insert eve;
        
        //Create Test Users
       
        Account person1 = DataFactoryAccountExt.insertIndividualAccount();
        Account person2 = DataFactoryAccountExt.insertIndividualAccount();
        
        List<Account> persons = [select id, name, PersonContactId from account where id = :person1.id or id = :person2.Id];
        
        system.debug('    cancelRegistrationTest.person ' + persons); 
                             
        // Create Group
        Registration_Group__c myGroup = new Registration_Group__c();
        myGroup.Group_Owner__c = person1.Id;
        myGroup.Event__c = eve.Id;
        insert myGroup;
        
        //Create Registration Objects                   
        NU__Registration2__c reg_one = new NU__Registration2__c();        
        reg_one.NU__Account__c = person1.Id;
        reg_one.NU__Event__c = eve.Id;
        reg_one.Registration_Group__c = myGroup.Id;
        reg_one.NU__Status__c = 'Active';
        insert reg_one;
        
        NU__Registration2__c reg_two = new NU__Registration2__c();        
        reg_two.NU__Account__c = person2.Id;
        reg_two.NU__Event__c = eve.Id;
        reg_two.Registration_Group__c = myGroup.Id;
        reg_two.NU__Status__c = 'Active';
        insert reg_two;
        
        //Add Group Members
        Registration_Group_Member__c rGM1 = new Registration_Group_Member__c();
        rGM1.Account__c = person1.Id;
        rGM1.Registration_Group__c = myGroup.Id;
        rGM1.Group_Status__c = 'Active';
        insert rGM1;
        
        Registration_Group_Member__c rGM2 = new Registration_Group_Member__c();
        rGM2.Account__c = person2.Id;
        rGM2.Registration_Group__c = myGroup.Id;
        rGM2.Group_Status__c = 'Active';
        insert rGM2;               
                
        //Cancel Registration
        reg_two.NU__Status__c = 'Cancelled';
        update reg_two;

        //ReFresh value from SalesForce
        rGM2 = [select Id, Group_Status__c from Registration_Group_Member__c where Id = :rGM2.Id];                
                
        //Check the registration group status was also set to cancelled                 
        system.assertEquals('Cancelled', rGM2.Group_Status__c );
    
    }
}