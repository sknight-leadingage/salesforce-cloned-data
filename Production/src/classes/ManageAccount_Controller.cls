public with sharing class ManageAccount_Controller { 

    private final Account account;
    
    public Id AccountId{
        get{
            return ApexPages.CurrentPage().getParameters().get('pv0');
        }
    }
    
    public string actionTaken {get;set;}
    public String getActionTaken() {
        return actionTaken;
    }
    
    public String accStatus { get; set; }
    public List<SelectOption> getaccStatuses() {
        List<SelectOption> options = new List<SelectOption>();

		Schema.DescribeFieldResult fieldResult = Schema.Account.fields.NU__Status__c.getDescribe();
		if (fieldResult != null) {
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
			{
				if (!f.getLabel().equalsIgnoreCase(account.NU__Status__c)) {
					options.add(new SelectOption(f.getLabel(), f.getLabel()));
				}
			}
		}
        return options;
    }

    public String affEnable { get; set; }
    public List<SelectOption> getaffEnableList() {
    	List<SelectOption> options = new List<SelectOption>();
    	
    	if (account != null) {
	    	Id accId = account.Id;
	    	List<NU__Affiliation__c> affs = [SELECT Id, Parent_Name__c FROM NU__Affiliation__c WHERE NU__Account__c = :accId];
	    	
	    	for(NU__Affiliation__c aff : affs) {
	    		options.add(new SelectOption(aff.Id, aff.Parent_Name__c));
	    	}
    	}
    	return options;
    }

    public ManageAccount_Controller() {
        WizardStep = 1;
        
        account = [SELECT   Id, Name, NU__RecordTypeName__c,
                            RecordTypeId, State_Partner_ID__c,
                            NU__Status__c,
                            Dues_Price_Override_Reason__c,LeadingAge_Member_Company__c,
                            NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.Name
                            FROM Account WHERE Id = :AccountId];
    }

    public Account getAccount() {
        return account;
    }
    
     // ******* BEGIN: Wizard Setup Code *********
    private integer iWizardMax = 3;
    public integer WizardStep {get;set;}
    public integer WizardLastStep {get;set;}
    public boolean Done {get;set;}
    public integer WizardMax
    {
            get{
                    return iWizardMax;
            }
    }
      
    public void WizardBack(){
        if(WizardStep == 2 || WizardStep == 3){
            WizardStep = 1;
            return;
        
        }
    }
    
    public void WizardNext(){
        WizardLastStep = WizardStep;
        if(WizardStep == 1){
            if(accStatus != null)
            {
				WizardStep = 2;
				return;
            }
        }
        else if(WizardStep == 2) {
                IndStatusSave();
        }
        
		WizardStep++;  
    }
    //******* END: Wizard Setup Code *********
    
    Public void IndStatusSave()
    {
		try
		{
	        if  (account.NU__RecordTypeName__c.containsIgnoreCase('Individual'))    
            {
						Account a = [SELECT Id, NU__Status__c FROM Account WHERE Id = :AccountId];
						a.NU__Status__c = accStatus;
						update a;

                        List<NU__Affiliation__c> AffiliationAccounts = [SELECT Id, NU__Account__c, NU__Status__c FROM NU__Affiliation__c WHERE NU__Account__c = :AccountId];

                        for(NU__Affiliation__c af : AffiliationAccounts) {
                            if (a.NU__Status__c == Constant.ACCOUNT_STATUS_INACTIVE){
                                af.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
                            }
                
                        }
                  		update AffiliationAccounts;

                        if(affEnable!= null && affEnable!= 'none'){
                        	Id affId = AffEnable;
                        	NU__Affiliation__c AffAccount = [SELECT NU__Status__c, NU__IsPrimary__c FROM NU__Affiliation__c WHERE Id = :affId];
                        
                        	affAccount.NU__Status__c = Constant.AFFILIATION_STATUS_ACTIVE;
                        	affAccount.NU__IsPrimary__c = true;

							update affAccount;	
                        	
                        
                        }
   
                        Done = true;
                }
	    }
		catch (DMLException e)
		{                   
            ApexPages.addMessages(e); 
            WizardStep = WizardLastStep; // do not advance to final page with errors.  
        }
    	finally{}
    }
}