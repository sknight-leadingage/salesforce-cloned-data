public with sharing class EduSpeakerConflict_Controller extends EduReportsControllerBase {
    public List<Conference_Session_Speaker__c> SessionData {get;set;}
    
    public integer SessionDataSize { get; set; }
    
    public EduSpeakerConflict_Controller() {
        SetQuery();
    }
    
    public void SetQuery() {
        SessionDataSize = 0;
        GroupHeaderController.GroupTotals = new Map<string,integer>();
        List<AggregateResult> Totals = [SELECT COUNT(Speaker__c) sc, Speaker__r.FirstName fn, Speaker__r.LastName ln FROM Conference_Session_Speaker__c WHERE (Role__c = 'Key Contact' OR Role__c = 'Speaker')  AND Session__r.Conference__c = :EventId GROUP BY Speaker__r.FirstName, Speaker__r.LastName];
        for (AggregateResult ar : Totals ) {
            SessionDataSize++;
            GroupHeaderController.GroupTotals.put((string)ar.get('ln') + (string)ar.get('fn'), (integer)ar.get('sc'));
        }
    
        SessionData = [SELECT Session__r.Session_Number__c, Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Title__c,Speaker__r.FirstName,Speaker__r.LastName, Speaker_Full_Name__c FROM Conference_Session_Speaker__c WHERE (Role__c = 'Key Contact' OR Role__c = 'Speaker') AND Session__r.Conference__c = :EventId ORDER BY Speaker__r.LastName, Speaker__r.FirstName, Session__r.Session_Number_Numeric__c];
    }
}