public with sharing class MultiSiteProviderMembershipPricing extends MembershipPricerBase implements IProductPricer {
	public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
		if (customer != null &&
		    customer.RecordTypeId != Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
			return null;
		}
		
		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		
		if (isPrimaryMembershipProductOfMembership(membershipType, productToPrice, Constant.PROVIDER_MEMBERSHIP_TYPE_NAME) == false){
		    return null;
		}
		
		Decimal providerDuesPrice = customer.Multi_Site_Dues_Price__c;
		
		//Should probably add this 'if' to MembershipPricerBase as a method at some point?
        if (customer != null && customer.Dues_Price_Override__c != null && customer.Dues_Price_Override__c > 0){
        	//If we have a dues override amount...
        	if (extraParams.containsKey('FutureCalculationRequested')){
	        	boolean IsFutureCalc = (boolean)extraParams.get('FutureCalculationRequested');
	        	if (IsFutureCalc == false) {
	        		//...and it's NOT a future calculation, then apply the override!
	        		providerDuesPrice = customer.Dues_Price_Override__c;
	        	}
        	}
        	else {
        		//...and it's not clear if it's a future calc or not, then just apply the override anyway.
        		providerDuesPrice = customer.Dues_Price_Override__c;
        	}
        }
		
		return providerDuesPrice;
	}
}