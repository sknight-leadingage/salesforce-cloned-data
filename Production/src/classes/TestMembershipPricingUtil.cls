/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test helper methods to assist the Membership Pricing Tests.
 */
public class TestMembershipPricingUtil {
	public static NU.ProductPricingRequest createProductPricingRequest(List<NU__Product__c> products, String priceClassName){
		NU.ProductPricingRequest ppr = new NU.ProductPricingRequest();

		List<NU.ProductPricingInfo> ppis = new List<NU.ProductPricingInfo>();
		
		if (NU.CollectionUtil.listHasElements(products)){
			for (NU__Product__c product : products){
				NU.ProductPricingInfo ppi = new NU.ProductPricingInfo();
				
				ppi.ProductId = product.Id;
				ppi.Quantity = 1;
				
				ppis.add(ppi);
			}

			ppr.ProductPricingInfos = ppis;
		}

		ppr.PriceClassName = priceClassName;

		return ppr;
	}
	
	public static Map<Id, Decimal> getProductPrice(
	       Id accountId,
	       NU__Product__c prod,
	       String priceClass,
	       DateTime transactionDate,
	       Id membershipTypeId){

		return getProductPrice(accountId, prod, priceClass, transactionDate, membershipTypeId, null, null);
	}
	
	public static Map<Id, Decimal> getProductPrice(
	       Id accountId,
	       NU__Product__c prod,
	       String priceClass,
	       DateTime transactionDate,
	       Id membershipTypeId,
	       Date startDate,
	       Date endDate){

		return getProductPrice(accountId, prod, priceClass, transactionDate, membershipTypeId, startDate, endDate, null);
	}
	
	public static Map<Id, Decimal> getProductPrice(
	       Id accountId,
	       NU__Product__c prod,
	       String priceClass,
	       DateTime transactionDate,
	       Id membershipTypeId,
	       Date startDate,
	       Date endDate,
	       Date joinDate){

		CustomPricingManager cpm = new CustomPricingManager();
		
		NU.ProductPricingRequest ppr = createProductPricingRequest(new List<NU__Product__c>{ prod }, priceClass);
		
		if (startDate != null || endDate != null || joinDate != null){
			NU.ProductPricingInfo ppi = ppr.ProductPricingInfos[0];
			
			ppi.StartDate = startDate;
			ppi.EndDate = endDate;
			ppi.JoinDate = joinDate;
		}
		
		ppr.TransactionDate = transactionDate;
		ppr.MembershipTypeId = membershipTypeId;
		ppr.AccountId = accountId;
		
		return cpm.GetProductPrices(ppr);
	}
	
	public static void assertExpectedMembershipPricingCalculated(
	       Id accountId,
	       NU__Product__c prod,
	       String priceClass,
	       DateTime transactionDate,
	       Id membershipTypeId,
	       Decimal expectedPrice){

		assertExpectedMembershipPricingCalculated(accountId, prod, priceClass, transactionDate, membershipTypeId, expectedPrice, null, null);
	}
	
	public static void assertExpectedMembershipPricingCalculated(
		   Id accountId,
	       NU__Product__c prod,
	       String priceClass,
	       DateTime transactionDate,
	       Id membershipTypeId,
	       Decimal expectedPrice,
	       Date startDate,
	       Date endDate
	){
		assertExpectedMembershipPricingCalculated(accountId, prod, priceClass, transactionDate, membershipTypeId, expectedPrice, startDate, endDate, null);
	}
	
	public static void assertExpectedMembershipPricingCalculated(
		   Id accountId,
	       NU__Product__c prod,
	       String priceClass,
	       DateTime transactionDate,
	       Id membershipTypeId,
	       Decimal expectedPrice,
	       Date startDate,
	       Date endDate,
	       Date joinDate
	){
		Map<Id, Decimal> prodPrices = TestMembershipPricingUtil.getProductPrice(accountId, prod, priceClass, transactionDate, membershipTypeId, startDate, endDate, joinDate);
		system.assert(prodPrices.containsKey(prod.Id));

		system.assertEquals(expectedPrice, prodPrices.get(prod.Id));
	}
	
	public static void assertProviderPricing(Account providerAccount, Decimal expectedProviderPricing){
		NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType();
        
        assertProviderPricing(providerAccount, expectedProviderPricing, providerMT);
	}
	
	public static void assertProviderPricing(Account providerAccount, Decimal expectedProviderPricing, NU__MembershipType__c providerMT){
        assertProviderPricingWithTerm(providerAccount, expectedProviderPricing, providerMT, null, null, null);
	}
	
	public static void assertProviderPricingWithTerm(Account providerAccount, Decimal expectedProviderPricing, NU__MembershipType__c providerMT, Date startDate, Date endDate, Date joinDate){
		NU__Product__c membershipProduct = NU.DataFactoryProduct.insertDefaultMembershipProduct(providerMT.NU__Entity__c);
        
        NU__MembershipTypeProductLink__c primaryMTPL = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(providerMT.Id, membershipProduct.Id);
        
        assertExpectedMembershipPricingCalculated(providerAccount.Id, membershipProduct, NU.Constant.PRICE_CLASS_DEFAULT, Date.Today(), providerMT.Id, expectedProviderPricing, startDate, endDate, joinDate);
	}
}