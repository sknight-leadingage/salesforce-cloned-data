//handles triggers related to NU__Event__c
//currently handles one scenario, where the NU__Event__c.Current_Year_Annual_Event__c can only have one record set to true

//Developed by NimbleUser, 2013 (MG, NF)

public with sharing class EventTriggerHandlers extends NU.TriggerHandlersBase
{
	//define error messages
    final string CURRENT_YEAR_EVENT_ALREADY_EXISTS = 'Another event is already marked as the current year event.';
    
     public override void onBeforeInsert(List<sObject> newRecords)
    {
        Map<Id, NU__Event__c> newRecordMap = new Map<Id, NU__Event__c>();
        
        for(sObject obj :newRecords)
        {
            newRecordMap.put(obj.Id, (NU__Event__c)obj);
        }
        
        ValidateCurrentYearEvent(newRecordMap);
    }
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) 
    {
        ValidateCurrentYearEvent((Map<Id, NU__Event__c>)newRecordMap);
    }
    
   // public override void onBeforeDelete(Map<Id, sObject> oldRecordMap) { }
   // public override void onAfterInsert(Map<Id, sObject> newRecordMap) { }
   // public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){ }
    
    //public override void onAfterDelete(Map<Id, sObject> oldRecordMap) {}
    
    public override void onAfterUndelete(Map<Id, sObject> newRecordMap) 
    { 
        ValidateCurrentYearEvent((Map<Id, NU__Event__c>)newRecordMap);
    } 
    
    //throws an error if more than one NU__Event__c record has Current_Year_Annual_Event__c set to true
    //only one event can be 'current' at at time
    //affects exhibitor point calculations
    private void ValidateCurrentYearEvent(Map<Id, NU__Event__c> events)
    {
		//create a map of all events where Current_Year_Annual_Event__c = true
        Map<Id, NU__Event__c> currentYearEvents = new Map<Id, NU__Event__c>([SELECT Id, Name FROM NU__Event__c WHERE Current_Year_Annual_Event__c = true]);
        
        //throw an error if the event count > 1
        for(NU__Event__c evt :events.values())
        {
            if(evt.Current_Year_Annual_Event__c && currentYearEvents != null && currentYearEvents.size() > 0 
                && (!currentYearEvents.containsKey(evt.Id) || currentYearEvents.size() > 1) )
            {
                evt.addError(CURRENT_YEAR_EVENT_ALREADY_EXISTS);
            }
        }
    }
}