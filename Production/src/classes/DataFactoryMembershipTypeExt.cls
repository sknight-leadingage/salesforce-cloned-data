/**
 * @author NimbleUser
 * @date Updated: 3/7/13
 * @description This class provides various functions for creating and inserting Membership Types in addition
 * to those found in NU.DataFactoryMembershipType. It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Membership Types without inserting them. The insert* functions create Membership Types
 * and insert them.
 */
public with sharing class DataFactoryMembershipTypeExt {
    public static final Integer DEFAULT_MEMBERSHIP_TERM = 12;
    public static final Integer DEFAULT_GRACE_PERIOD = 1;
	
	public static NU__MembershipType__c createDefaultCompanyAnnualMembershipType(Id entityId, String name, String annualStartMonth, String accountMembershipField){
		system.assertNotEquals(null, entityId);
        system.assert(String.isNotEmpty(name), 'The membership type name is empty.');
        system.assert(String.isNotEmpty(annualStartMonth), 'The annual start month is empty.');
        system.assert(String.isNotEmpty(accountMembershipField), 'The account membership field is empty.');
        
        return new NU__MembershipType__c(
            NU__Entity__c = entityId,
            Name = name,
            NU__Term__c = DEFAULT_MEMBERSHIP_TERM,
            NU__AccountMembershipField__c = accountMembershipField,
            NU__Category__c = NU.Constant.MEMBERSHIP_TYPE_CATEGORY_COMPANY,
            NU__AnnualStartMonth__c = annualStartMonth,
            NU__RenewalType__c = Constant.MEMBERSHIP_TYPE_RENEWAL_ANNIVERSARY,
            NU__GracePeriod__c = DEFAULT_GRACE_PERIOD,
            Grants_National_Membership__c = true
        );
        
    }
    
    public static NU__MembershipType__c createProviderMembershipType(Id entityId){
    	NU__MembershipType__c providerMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.PROVIDER_MEMBERSHIP_TYPE_NAME, Constant.PROVIDER_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	providerMT.NU__FlowdownManagerClass__c = Constant.PROVIDER_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	providerMT.NU__AccountJoinOnField__c = Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return providerMT;
    }
    
    public static NU__MembershipType__c insertProviderMembershipType(Id entityId){
    	NU__MembershipType__c providerMT = createProviderMembershipType(entityId);
    	
    	insert providerMT;
    	return providerMT;
    }
    
    public static NU__MembershipType__c createCASTMembershipType(Id entityId){
    	NU__MembershipType__c castMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.CAST_MEMBERSHIP_TYPE_NAME, Constant.CAST_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	castMT.NU__FlowdownManagerClass__c = Constant.CAST_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	castMT.NU__AccountJoinOnField__c = Constant.CAST_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return castMT;
    }
    
    public static NU__MembershipType__c insertCASTMembershipType(Id entityId){
    	NU__MembershipType__c castMT = createCASTMembershipType(entityId);
    	
    	insert castMT;
    	return castMT;
    }
    
    public static NU__MembershipType__c createIAHSAMembershipType(Id entityId){
    	NU__MembershipType__c iahsaMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.IAHSA_MEMBERSHIP_TYPE_NAME, Constant.IAHSA_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.IAHSA_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	iahsaMT.NU__FlowdownManagerClass__c = Constant.IAHSA_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	iahsaMT.NU__AccountJoinOnField__c = Constant.IAHSA_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return iahsaMT;
    }
    
    public static NU__MembershipType__c insertIAHSAMembershipType(Id entityId){
    	NU__MembershipType__c iahsaMT = createIAHSAMembershipType(entityId);
    	
    	insert iahsaMT;
    	return iahsaMT;
    }
    
    public static NU__MembershipType__c createLTQAMembershipType(Id entityId){
    	NU__MembershipType__c ltqaMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.LTQA_MEMBERSHIP_TYPE_NAME, Constant.LTQA_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.LTQA_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	ltqaMT.NU__FlowdownManagerClass__c = Constant.LTQA_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	ltqaMT.NU__AccountJoinOnField__c = Constant.LTQA_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return ltqaMT;
    }
    
    public static NU__MembershipType__c createJointStateProviderMembershipType(Id entityId){
    	NU__MembershipType__c jointStateProviderMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME, Constant.PROVIDER_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	jointStateProviderMT.NU__FlowdownManagerClass__c = Constant.PROVIDER_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	jointStateProviderMT.NU__AccountJoinOnField__c = Constant.PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return jointStateProviderMT;
    }
    
    public static NU__MembershipType__c insertLTQAMembershipType(Id entityId){
    	NU__MembershipType__c ltqaMT = createLTQAMembershipType(entityId);
    	
    	insert ltqaMT;
    	return ltqaMT;
    }
    
    public static NU__MembershipType__c createAssociateMembershipType(Id entityId){
    	NU__MembershipType__c associateMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.ASSOCIATE_MEMBERSHIP_TYPE_NAME, Constant.ASSOCIATE_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	associateMT.NU__FlowdownManagerClass__c = Constant.ASSOCIATE_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	associateMT.NU__AccountJoinOnField__c = Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return associateMT;
    }
    
    public static NU__MembershipType__c createCorporateAllianceMembershipType(Id entityId){
    	NU__MembershipType__c corporateAllianceMT = createDefaultCompanyAnnualMembershipType(entityId, Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_NAME, Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ANNUALSTART_MONTH, Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    	corporateAllianceMT.NU__FlowdownManagerClass__c = Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS;
    	corporateAllianceMT.NU__AccountJoinOnField__c = Constant.CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD;
    	
    	return corporateAllianceMT;
    }
    
    public static NU__MembershipType__c insertAssociateMembershipType(Id entityId){
    	NU__MembershipType__c associateMT = createAssociateMembershipType(entityId);
    	
    	insert associateMT;
    	return associateMT;
    }
    
    /**
	 * @description Inserts the CAST Membership Type with the default expected values using a generic inserted entity.
	 * This should only be used in test code.
	 * @return an inserted CAST Membership Type. 
	 */    
    public static NU__MembershipType__c insertCASTMembershipType(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
    	NU__MembershipType__c castMT = insertCASTMembershipType(anyEntity.Id);
    	
    	return castMT;
    }
    
    /**
	 * @description Inserts the Provider Membership Type with the default expected values using a generic inserted entity.
	 * This should only be used in test code.
	 * @return an inserted Provider Membership Type. 
	 */
    public static NU__MembershipType__c insertProviderMembershipType(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
        NU__MembershipType__c providerMT = insertProviderMembershipType(anyEntity.Id);
        
        return providerMT;
    }
    
    /**
	 * @description Inserts the IAHSA Membership Type with the default expected values using a generic inserted entity.
	 * This should only be used in test code.
	 * @return an inserted IAHSA Membership Type. 
	 */
    public static NU__MembershipType__c insertIAHSAMembershipType(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
        NU__MembershipType__c iahsaMT = insertIAHSAMembershipType(anyEntity.Id);
        
        return iahsaMT;
    }
    
    /**
	 * @description Inserts the LTQA Membership Type with the default expected values using a generic inserted entity.
	 * This should only be used in test code.
	 * @return an inserted LTQA Membership Type. 
	 */
    public static NU__MembershipType__c insertLTQAMembershipType(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
        NU__MembershipType__c ltqaMT = insertLTQAMembershipType(anyEntity.Id);
        
        return ltqaMT;
    }
    
    /**
	 * @description Inserts the Associate Membership Type with the default expected values using a generic inserted entity.
	 * This should only be used in test code.
	 * @return an inserted Associate Membership Type. 
	 */
    public static NU__MembershipType__c insertAssociateMembershipType(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
        NU__MembershipType__c associateMT = insertAssociateMembershipType(anyEntity.Id);
        
        return associateMT;
    }
    
    /**
	 * @description Inserts the Joint State Provider Membership Type with the default expected values using a generic inserted entity.
	 * The Membership Type Product Link and membership product records are also inserted. This should only be used in test code.
	 * @return an inserted Joint State Provider Membership Type. 
	 */
    public static NU__MembershipType__c insertJointStateProviderMembershipTypeWithProduct(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
    	NU__MembershipType__c jointStateProviderMT = createJointStateProviderMembershipType(anyEntity.Id);
    	
    	insert jointStateProviderMT;
    	
    	NU__Product__c membershipProduct = NU.DataFactoryProduct.createMembershipProduct(Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME, 100, anyEntity.Id);
		insert membershipProduct;
		
		NU__MembershipTypeProductLink__c mtpl = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(jointStateProviderMT.Id, membershipProduct.Id);
    	
    	return jointStateProviderMT;
    }
    
    /**
	 * @description Inserts the Corporate Alliance Membership Type with the default expected values using a generic inserted entity.
	 * This should only be used in test code.
	 * @return an inserted Joint State Provider Membership Type. 
	 */
    public static NU__MembershipType__c insertCorporateAllianceMembershipType(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
    	NU__MembershipType__c corporateAllianceMT = createCorporateAllianceMembershipType(anyEntity.Id);
    	
    	insert corporateAllianceMt;
    	
    	return corporateAllianceMT;
    }
}