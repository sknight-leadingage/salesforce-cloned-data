public with sharing class EduSpeakerListings_Controller extends EduReportsControllerBase {
    public List<Conference_Session_Speaker__c> SumList { get;set; }
    
    public string SLIST_SNUMBER = 'Speaker Listing (By Session #)';
    public string SLIST_SPEAKER = 'Speaker Listing (By Speaker)';
    public string SLIST_COMPANY = 'Speaker Listing (By Company)';
    public string SLIST_TIMESLOT = 'Speaker Listing (By Timeslot)';

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(SLIST_SNUMBER, SLIST_SNUMBER));
        options.add(new SelectOption(SLIST_SPEAKER, SLIST_SPEAKER));
        options.add(new SelectOption(SLIST_COMPANY, SLIST_COMPANY));
        options.add(new SelectOption(SLIST_TIMESLOT, SLIST_TIMESLOT));
        return options; 
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == SLIST_SNUMBER) {
            WizardStep = 2;
        } else if (RTypeSel == SLIST_SPEAKER) {
            WizardStep = 3;
        } else if (RTypeSel == SLIST_COMPANY) {
            WizardStep = 4;
        } else if (RTypeSel == SLIST_TIMESLOT) {
            WizardStep = 5;
        }
        SetQuery();
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    
    public string getPageSubtitle() {
        if (WizardStep == 2) {
            return 'Faculty Index';
        }
        else if (WizardStep == 3) {
            return 'Speaker Listing';
        }
        else if (WizardStep == 4) {
            return 'Speakers by Company';
        }
        else if (WizardStep == 5) {
            return 'Speakers by Timeslot';
        }
        else {
            return '';
        }
    }
    
    public override string getPageTitle() {
        return Conference != null ? Conference.Name : '';
    }
    
    public EduSpeakerListings_Controller() {
        iWizardMax = 5;
        WizardStep = 1;
        SetConPageSize = 255;
        //SetQuery();
    }
    
    public override void SetConRefreshData() {
        SumList = setCon.getRecords();
    }
    
    public void SetQuery() {
        string sOrderBy = '';
        if (RTypeSel == SLIST_SNUMBER) {
            sOrderBy = 'ORDER BY Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Session_Number_Numeric__c, Speaker__r.LastName, Speaker__r.FirstName';
        } else if (RTypeSel == SLIST_SPEAKER) {
            sOrderBy = 'ORDER BY Speaker__r.LastName, Speaker__r.FirstName, Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Session_Number_Numeric__c';
        } else if (RTypeSel == SLIST_COMPANY) {
            sOrderBy = 'ORDER BY Speaker__r.Speaker_Company_Name_Rollup__c, Speaker__r.LastName, Speaker__r.FirstName, Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Session_Number_Numeric__c';
        } else if (RTypeSel == SLIST_TIMESLOT) {
            sOrderBy = 'ORDER BY Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Session_Number_Numeric__c, Speaker__r.LastName, Speaker__r.FirstName';
        }

        SetConQuery = 'SELECT Speaker__r.FirstName, Speaker__r.LastName, Session__r.Session_Number__c, Session__r.Session_Number_Numeric__c, Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Timeslot__r.Session_Date__c, Session__r.Timeslot__r.Session_Time__c, Session__r.Timeslot__c, Session__r.Timeslot__r.Timeslot__c, Role__c, Session__r.Notes__c, Speaker__r.LeadingAge_ID__c, Speaker__r.Speaker_Company_Name_Rollup__c FROM Conference_Session_Speaker__c WHERE (Role__c = \'Key Contact\' OR Role__c = \'Speaker\') AND Session__r.Conference__r.Id = :EventId ' + sOrderBy;
        ResetCon();
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}