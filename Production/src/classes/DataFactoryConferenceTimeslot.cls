public with sharing class DataFactoryConferenceTimeslot {
	
	public static Conference_Timeslot__c createTimeslot(Id conferenceId, Datetime start, DateTime endTime){
		Conference_Timeslot__c timeslot = new Conference_Timeslot__c(
        	Start__c = start,
        	End__c = endTime,
        	Conference__c = conferenceId,
        	Timeslot_Code__c = 'A'
        );
        
        return timeslot;
	}
	
	public static Conference_Timeslot__c createTimeslot(Id conferenceId){
		return createTimeslot(conferenceId, DateTime.now(), DateTime.now().addHours(1));
	}
	
	public static Conference_Timeslot__c insertTimeslot(Id conferenceId, Datetime start, DateTime endTime){
		Conference_Timeslot__c newTS = createTimeslot(conferenceId, start, endTime);
		
		insert newTS;
		return newTS;
	}
	
	public static Conference_Timeslot__c insertTimeslot(Id conferenceId){
		Conference_Timeslot__c newTs = createTimeslot(conferenceId);
		
		insert newTS;
		return newTS;
	}
}