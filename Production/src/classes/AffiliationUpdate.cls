public with sharing class AffiliationUpdate implements Database.Batchable<NU__Affiliation__c> {

    public static void run() {
        Database.executeBatch(new AffiliationUpdate(), 500);
    }

    public Iterable<NU__Affiliation__c> start(Database.BatchableContext context) {

        return (Iterable<NU__Affiliation__c>)Database.getQueryLocator('select Id from NU__Affiliation__c where NU__Status__c = null');
    }

    public void execute(Database.BatchableContext context, List<NU__Affiliation__c> affiliations) {
         for (NU__Affiliation__c affiliation : affiliations) {
            affiliation.NU__Status__c = 'Active';
         }
         update affiliations;
    }

    public void finish(Database.BatchableContext context) {

    }
}