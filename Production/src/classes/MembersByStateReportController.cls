// visualforce page controller for MembersByStateReport.page
// outputs a printable list of Members for advocacy work from a specified US Congressional District
// Borrowed from CongressionalDistrictReport Developed by NimbleUser 2014 (NF)


public class MembersByStateReportController {
    public string SelectedState {get; set;} //the state to be included in the report
    public integer RecordsCount {get; set;} //the number of rows returned in the query
    public integer SelectedLayout {get;set;} // to hide certin layout aspects
    public string SelectedTypeProvider {get; set;} //the Provider type
    public Boolean isCheckedHideMenus {get;set;} //checkbox for hiding SF menus in the output
    public Boolean isCheckedRenderAsPDF {get;set;}  //checkbox for rendering the page as a PDF (has some display quirks; hidden for now)
    public string HeadlineAddones {get;set;} // adding headline items between LeadingAge and Members
    public string FooterText{get; set;} // adding footer text
    public map<Id, decimal> NursingBeds { get; set; }
    public map<Id, decimal> AssistedLivingBeds { get; set; }
    public map<Id, decimal> SeniroHousingUnits { get; set; }


    //Catching null and converting it to 0
    
        	public static decimal getNotNull(decimal s){
					    if (s == null){
					        return 0;
					    }
					    else {
					        return s;
					    }
        	}
        	
    //constructor
    public MembersByStateReportController() {
    }

    //run refreshes the page and requeries the Records list
    public void Run() {
    }

   private list<SelectOption> getPicklistValues(SObject obj, String fld)
   {
      list<SelectOption> options = new list<SelectOption>();
      // Get the object type of the SObject.
      Schema.sObjectType objType = obj.getSObjectType(); 
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fld).getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a : values)
      { 
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }
      return options;
   }
    //retrieve records for a selected distrct
    private List <Account> records_priv = null;
    public List <Account> records {
        get {
            if (SelectedState != null)
            {
            	        	string provdertype = '';
        	if(SelectedTypeProvider != '-1' )
        	{
        		provdertype = 'AND Provider_Type__c INCLUDES(\'' + SelectedTypeProvider + '\') ';
        	}
            records_priv = Database.query(
                   'SELECT ' +
                   'AL_Program_Services__c, ' +
                   'BillingCity, ' +
                   'BillingPostalCode, ' +
                   'BillingState, ' +
                   'BillingStreet, ' +
                   'CCRC_Program_Services__c, ' +
                   'HCBS_Program_Services__c, ' +
                   'Housing_Program_Services__c, ' +
                   'Id, ' +
                   'isLeadingAgeMember__c, ' +
                   'Name, ' +
                   'Nursing_Program_Services__c, ' +
                   'Number_Of_Nursing_Beds__c,' +    // Bed counts for the report
	                'ICF_Beds__c,' + 
	                'Number_of_Personal_Care_Beds__c,' +
	                'Personal_Care_Beds__c,' +
	                'Product_And_Services_Listing__c, ' +
	                'SNF_Medicare_Beds__c,' +
	                'SNF_Medicaid_Beds__c,' +
	                'SNF_Dual_Certified_Beds__c,'+
	                'SNF_Total_License_Beds__c,' +
	                'Number_Of_Assisted_Living_Units__c,' +
	                'Number_of_Independent_Living_Units__c,' +
	                'Total_Housing_Units__c,' +
	                'Total_Number_Of_HUD_Units__c,' +
	                'ILU_Fed_Subsidized__c,' + 
	                'ILU_Tax_Credit_Income_Rest__c,' +
	                'ILU_Market_Rate__c,' +
	                'ILU_Other__c,' +
                   'Provider_Type__c, ' +
                   'RecordType.Name, ' +
                       '(SELECT ' +
                       'Id, ' +
                       'Name, ' +
                       'NU__Account__r.Name, ' +
                       'NU__Account__r.PersonEmail, ' +
                       'NU__Account__r.PersonTitle, ' +
                       'NU__Role__c ' +
                       'FROM NU__Affiliates__r ' +
                       'WHERE NU__Role__c INCLUDES (\'LeadingAge Primary Contact\') ' +
                       ') ' +
                   'FROM Account ' +
                   'WHERE BillingState = :SelectedState AND (RecordType.Name != \'Individual\' AND RecordType.Name != \'Individual Associate\' AND RecordType.Name != \'Corporate Alliance/Sponsor\') AND isLeadingAgeMember__c = \'Yes\''
                    +
                    provdertype +
                   'ORDER BY Name' );
                RecordsCount = records_priv.size();
                ReformatServices();
            }
            else {
                records_priv = new List<Account>();
            }
            
            return records_priv;
        }
    }

    //combines several fields into one; cleans picklist items for display    
    private void ReformatServices() {
    	
    	decimal NN = 0;
        decimal AA = 0;
        decimal HH = 0;
        
        if (NursingBeds == null){
        	NursingBeds = new Map<Id, decimal>();
        }
        if (AssistedLivingBeds == null){
        	AssistedLivingBeds = new Map<Id, decimal>();
        }
        if (SeniroHousingUnits == null){
        	SeniroHousingUnits = new Map<Id, decimal>();
        }
        
        for (Account a : records_priv) {
            if (a.Provider_Type__c != null) {
                a.Provider_Type__c = a.Provider_Type__c.replace(';',', ');
            }
            if (a.AL_Program_Services__c != null) {
                a.AL_Program_Services__c = a.AL_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.AL_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.AL_Program_Services__c;
                }
            }
            if (a.CCRC_Program_Services__c != null) {
                a.CCRC_Program_Services__c = a.CCRC_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.CCRC_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.CCRC_Program_Services__c;
                }
            }
            if (a.HCBS_Program_Services__c != null) {
                a.HCBS_Program_Services__c = a.HCBS_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.HCBS_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.HCBS_Program_Services__c;
                }
            }
            if (a.Housing_Program_Services__c != null) {
                a.Housing_Program_Services__c = a.Housing_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.Housing_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.Housing_Program_Services__c;
                }
            }
            if (a.Nursing_Program_Services__c != null) {
                a.Nursing_Program_Services__c = a.Nursing_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.Nursing_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.Nursing_Program_Services__c;
                }
            }
            
            //truncate to 175 chars
            if (a.Provider_Type__c != null && a.Provider_Type__c.length() >= 175) {
                a.Provider_Type__c = a.Provider_Type__c.substring(0,175);
                a.Provider_Type__c = a.Provider_Type__c.substring(0,a.Provider_Type__c.lastIndexOf(' '));
                a.Provider_Type__c = a.Provider_Type__c + '...';
            }
            
            
            NN = getNotNull(a.SNF_Total_License_Beds__c) + getNotNull(a.SNF_Dual_Certified_Beds__c) + getNotNull(a.Number_Of_Nursing_Beds__c) + getNotNull(a.ICF_Beds__c) + getNotNull(a.Number_of_Personal_Care_Beds__c) + getNotNull(a.Personal_Care_Beds__c) + getNotNull(a.SNF_Medicare_Beds__c) + getNotNull(a.SNF_Medicaid_Beds__c);
            NursingBeds.put(a.id,NN );
            AA = getNotNull(a.Number_of_Independent_Living_Units__c) + getNotNull(a.Number_Of_Assisted_Living_Units__c);
            AssistedLivingBeds.put(a.id,AA) ;
            HH = getNotNUll(a.ILU_Other__c) + getNotNull(a.ILU_Market_Rate__c) + getNotNull(a.ILU_Tax_Credit_Income_Rest__c) + getNotNull(a.ILU_Fed_Subsidized__c) + getNotNull(a.Total_Number_Of_HUD_Units__c) + getNotNull(a.Total_Housing_Units__c);
            SeniroHousingUnits.put(a.id, HH) ; 
            
        }
    }

	//Get Provder types
	
	public List<SelectOption> getTypes(){
		list<SelectOption> typeOptions = getPicklistValues(new Account(Name = 'ProviderType'), 'Provider_Type__c');
		 return typeOptions;
	}
    //outputs a (drop-down) list of States
    public List<SelectOption> getStates() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('AL', 'Alabama'));
        options.add(new SelectOption('AK', 'Alaska'));
        options.add(new SelectOption('AZ', 'Arizona'));
        options.add(new SelectOption('AR', 'Arkansas'));
        options.add(new SelectOption('CA', 'California'));
        options.add(new SelectOption('CO', 'Colorado'));
        options.add(new SelectOption('CT', 'Connecticut'));
        options.add(new SelectOption('DE', 'Delaware'));
        options.add(new SelectOption('DC', 'District of Columbia'));
        options.add(new SelectOption('FL', 'Florida'));
        options.add(new SelectOption('GA', 'Georgia'));
        options.add(new SelectOption('HI', 'Hawaii'));
        options.add(new SelectOption('ID', 'Idaho'));
        options.add(new SelectOption('IL', 'Illinois'));
        options.add(new SelectOption('IN', 'Indiana'));
        options.add(new SelectOption('IA', 'Iowa'));
        options.add(new SelectOption('KS', 'Kansas'));
        options.add(new SelectOption('KY', 'Kentucky'));
        options.add(new SelectOption('LA', 'Louisiana'));
        options.add(new SelectOption('ME', 'Maine'));
        options.add(new SelectOption('MD', 'Maryland'));
        options.add(new SelectOption('MA', 'Massachusetts'));
        options.add(new SelectOption('MI', 'Michigan'));
        options.add(new SelectOption('MN', 'Minnesota'));
        options.add(new SelectOption('MS', 'Mississippi'));
        options.add(new SelectOption('MO', 'Missouri'));
        options.add(new SelectOption('MT', 'Montana'));
        options.add(new SelectOption('NE', 'Nebraska'));
        options.add(new SelectOption('NV', 'Nevada'));
        options.add(new SelectOption('NH', 'New Hampshire'));
        options.add(new SelectOption('NJ', 'New Jersey'));
        options.add(new SelectOption('NM', 'New Mexico'));
        options.add(new SelectOption('NY', 'New York'));
        options.add(new SelectOption('NC', 'North Carolina'));
        options.add(new SelectOption('ND', 'North Dakota'));
        options.add(new SelectOption('OH', 'Ohio'));
        options.add(new SelectOption('OK', 'Oklahoma'));
        options.add(new SelectOption('OR', 'Oregon'));
        options.add(new SelectOption('PA', 'Pennsylvania'));
        options.add(new SelectOption('RI', 'Rhode Island'));
        options.add(new SelectOption('SC', 'South Carolina'));
        options.add(new SelectOption('SD', 'South Dakota'));
        options.add(new SelectOption('TN', 'Tennessee'));
        options.add(new SelectOption('TX', 'Texas'));
        options.add(new SelectOption('UT', 'Utah'));
        options.add(new SelectOption('VT', 'Vermont'));
        options.add(new SelectOption('VA', 'Virginia'));
        options.add(new SelectOption('WA', 'Washington'));
        options.add(new SelectOption('WV', 'West Virginia'));
        options.add(new SelectOption('WI', 'Wisconsin'));
        options.add(new SelectOption('WY', 'Wyoming'));
        return options;
    }

    //specify the full state name, given the selected abbreviation
    public string getSelectedStateName() {
        return SelectedState == 'AL' ? 'Alabama' :
        SelectedState == 'AL' ? 'Alabama' :
        SelectedState == 'AK' ? 'Alaska' :
        SelectedState == 'AZ' ? 'Arizona' :
        SelectedState == 'AR' ? 'Arkansas' :
        SelectedState == 'CA' ? 'California' :
        SelectedState == 'CO' ? 'Colorado' :
        SelectedState == 'CT' ? 'Connecticut' :
        SelectedState == 'DE' ? 'Delaware' :
        SelectedState == 'DC' ? 'District of Columbia' :
        SelectedState == 'FL' ? 'Florida' :
        SelectedState == 'GA' ? 'Georgia' :
        SelectedState == 'HI' ? 'Hawaii' :
        SelectedState == 'ID' ? 'Idaho' :
        SelectedState == 'IL' ? 'Illinois' :
        SelectedState == 'IN' ? 'Indiana' :
        SelectedState == 'IA' ? 'Iowa' :
        SelectedState == 'KS' ? 'Kansas' :
        SelectedState == 'KY' ? 'Kentucky' :
        SelectedState == 'LA' ? 'Louisiana' :
        SelectedState == 'ME' ? 'Maine' :
        SelectedState == 'MD' ? 'Maryland' :
        SelectedState == 'MA' ? 'Massachusetts' :
        SelectedState == 'MI' ? 'Michigan' :
        SelectedState == 'MN' ? 'Minnesota' :
        SelectedState == 'MS' ? 'Mississippi' :
        SelectedState == 'MO' ? 'Missouri' :
        SelectedState == 'MT' ? 'Montana' :
        SelectedState == 'NE' ? 'Nebraska' :
        SelectedState == 'NV' ? 'Nevada' :
        SelectedState == 'NH' ? 'New Hampshire' :
        SelectedState == 'NJ' ? 'New Jersey' :
        SelectedState == 'NM' ? 'New Mexico' :
        SelectedState == 'NY' ? 'New York' :
        SelectedState == 'NC' ? 'North Carolina' :
        SelectedState == 'ND' ? 'North Dakota' :
        SelectedState == 'OH' ? 'Ohio' :
        SelectedState == 'OK' ? 'Oklahoma' :
        SelectedState == 'OR' ? 'Oregon' :
        SelectedState == 'PA' ? 'Pennsylvania' :
        SelectedState == 'RI' ? 'Rhode Island' :
        SelectedState == 'SC' ? 'South Carolina' :
        SelectedState == 'SD' ? 'South Dakota' :
        SelectedState == 'TN' ? 'Tennessee' :
        SelectedState == 'TX' ? 'Texas' :
        SelectedState == 'UT' ? 'Utah' :
        SelectedState == 'VT' ? 'Vermont' :
        SelectedState == 'VA' ? 'Virginia' :
        SelectedState == 'WA' ? 'Washington' :
        SelectedState == 'WV' ? 'West Virginia' :
        SelectedState == 'WI' ? 'Wisconsin' :
        SelectedState == 'WY' ? 'Wyoming' : '(not specified)';
    }
}