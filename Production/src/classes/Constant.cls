/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class provides common constants that can be used throughout the org.
 */
public with sharing class Constant {
    public static final String ACCOUNT_RECORD_TYPE_NAME_INDIVIDUAL = 'Individual';
    public static final String ACCOUNT_RECORD_TYPE_NAME_INDIVIDUAL_ASSOCIATE = 'Individual Associate';
    public static final String ACCOUNT_RECORD_TYPE_NAME_BUSINESS_ACCOUNT = 'Business Account';
    public static final String ACCOUNT_RECORD_TYPE_NAME_COMPANY = 'Company';
    public static final String ACCOUNT_RECORD_SUBTYPE_NAME_CORPORATE = 'Corporate/Business Firm';
    public static final String ACCOUNT_RECORD_SUBTYPE_NAME_UNIVERSITY = 'University/College';
    public static final String ACCOUNT_RECORD_TYPE_NAME_CORPORATE_ALLIANCE_SPONSOR = 'Company'; //We got rid of Corporate Alliance/Sponsor rec type
    public static final String ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE = 'Multi-site Organization';
    public static final String ACCOUNT_RECORD_TYPE_NAME_ORGANIZATION = 'Organization';
    public static final String ACCOUNT_RECORD_TYPE_NAME_PROVIDER = 'Provider';
    public static final String ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER = 'State Partner';
    public static final String ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER_PORTAL = 'State Partner Portal';
    public static final String ACCOUNT_TYPE_NAME_CORPORATE_ALLIANCE_SPONSOR = 'Corporate Alliance';
    
    public static final String ACCOUNT_COMPANY_TYPE_NAME_MANAGEMENT_COMPANY = 'Management Company';
    //public static final String ACCOUNT_COMPANY_TYPE_NAME_CORPORATE_ALLIANCE = 'Corporate Alliance/Sponsor';
    
    public static final String ONLINE_SUBSCRITPION_PRODUCT_GENERAL_INFORMATION = 'General Information';		
    public static final String ONLINE_SUBSCRITPION_PRODUCT_EXHIBITS_AND_ADVERTISTING = 'Exhibits and Advertising';		
    public static final String ONLINE_SUBSCRITPION_PRODUCT_EDUCATION_AND_CONFERENCES = 'Education and Conferences';
    
    public static final String MEMBERSHIP_TYPE_RENEWAL_ANNIVERSARY = 'Anniversary';
    public static final String MEMBERSHIP_TYPE_RENEWAL_ANNUAL = 'Annual';
    
    public static final String JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME = 'Provider - Joint State';
    public static final String NON_JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME = 'Provider - Non Joint State';
    
    public static final String LEADINGAGE_MEMBER = 'Member';
    public static final String LEADINGAGE_NONMEMBER = 'Non-member';
    
    public static final String STATE_MEMBER = 'Member';
    public static final String STATE_NONMEMBER = 'Non-member';
        
    public static final String CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_NAME = 'Corporate Alliance';
    public static final String CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ANNUALSTART_MONTH = '01';
    public static final String CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD = 'NU__Membership__c';
    public static final String CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD = 'NU__JoinOn__c';
    public static final String CORPORATE_ALLIANCE_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS = 'CustomPersonOnlyMembershipFlowdownMgr';
    
    public static final String PROVIDER_MEMBERSHIP_TYPE_NAME = 'Provider';
    public static final String PROVIDER_MEMBERSHIP_TYPE_ANNUALSTART_MONTH = '01';
    public static final String PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD = 'Provider_Membership__c';
    public static final String PROVIDER_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD = 'Provider_Join_On__c';
    public static final String PROVIDER_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS = 'CustomProviderMembershipFlowdownManager';
    
    public static final String CAST_MEMBERSHIP_TYPE_NAME = 'CAST';
    public static final String CAST_MEMBERSHIP_TYPE_ANNUALSTART_MONTH = '01';
    public static final String CAST_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD = 'CAST_Membership__c';
    public static final String CAST_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD = 'CAST_Join_On__c';
    public static final String CAST_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS = 'CustomPersonOnlyMembershipFlowdownMgr';
    
    public static final String IAHSA_MEMBERSHIP_TYPE_NAME = 'IAHSA';
    public static final String IAHSA_MEMBERSHIP_TYPE_ANNUALSTART_MONTH = '01';
    public static final String IAHSA_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD = 'IAHSA_Membership__c';
    public static final String IAHSA_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD = 'IAHSA_Join_On__c';
    public static final String IAHSA_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS = 'CustomMembershipFlowdownManager';
    
    public static final String LTQA_MEMBERSHIP_TYPE_NAME = 'LTQA';
    public static final String LTQA_MEMBERSHIP_TYPE_ANNUALSTART_MONTH = '01';
    public static final String LTQA_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD = 'LTQA_Membership__c';
    public static final String LTQA_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD = 'LTQA_Join_On__c';
    public static final String LTQA_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS = 'CustomMembershipFlowdownManager';
    
    public static final String ASSOCIATE_MEMBERSHIP_TYPE_NAME = 'Associate';
    public static final String ASSOCIATE_MEMBERSHIP_TYPE_ANNUALSTART_MONTH = '01';
    public static final String ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD = 'NU__Membership__c';
    public static final String ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD = 'NU__JoinOn__c';
    public static final String ASSOCIATE_MEMBERSHIP_TYPE_FLOWDOWN_MANAGER_CLASS = 'CustomMembershipFlowdownManager';
    
    public static final String ACCOUNT_PROVIDER_RECORD_TYPE_ID = AccountRecordTypeUtil.getProviderRecordType().Id;
    public static final String ACCOUNT_COMPANY_RECORD_TYPE_ID = AccountRecordTypeUtil.getCompanyRecordType().Id;
    public static final String ACCOUNT_CORPORATE_ALLIANCE_SPONSOR_RECORD_TYPE_ID = AccountRecordTypeUtil.getCorporateAllianceSponsorRecordType().Id;
    public static final String ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID = AccountRecordTypeUtil.getIndividualRecordType().Id;
    public static final String ACCOUNT_MULTI_SITE_RECORD_TYPE_ID = AccountRecordTypeUtil.getMultiSiteRecordType().Id;
    public static final String ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID = AccountRecordTypeUtil.getStatePartnerRecordType().Id;
    public static final String ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID = AccountRecordTypeUtil.getStatePartnerPortalRecordType().Id;
    
    
    public static final String ACCOUNT_STATUS_ACTIVE = 'Active';
    public static final String ACCOUNT_STATUS_INACTIVE = 'Inactive';
    
    public static final String AFFILIATION_STATUS_ACTIVE = 'Active';
    public static final String AFFILIATION_STATUS_INACTIVE = 'Inactive';
    
    public static final string AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT = 'LeadingAge Primary Contact';
    public static final string AFFILIATION_ROLE_COMPANY = 'Company';    
    //TODO Add Remaining Roles (need to delete then reschdule jobs when saving)
    
    public static final String MEMBERSHIP_TYPE_STATUS_ACTIVE = 'Active';
    
    public static final String MEMBERSHIP_TYPE_STAGE_BOTH = 'Both';
    public static final String MEMBERSHIP_TYPE_STAGE_RENEW = 'Renew';
    
    public static final String MEMBERSHIP_STATUS_CURRENT = 'Current';
    public static final String MEMBERSHIP_STATUS_FUTURE = 'Future';
    
    public static final String MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY = 'Primary';
    public static final String MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_REQUIRED = 'Required';
    public static final String MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_OPTIONAL = 'Optional';
    
    public static final String MEMBERSHIP_RETENTION_NEW = 'New';
    public static final String MEMBERSHIP_RETENTION_RENEWING = 'Renewing';
    
    public static final String SUBSCRIPTION_STATUS_ACTIVE = 'Active';
    public static final String SUBSCRIPTION_STATUS_INACTIVE = 'Inactive';
    public static final String SUBSCRIPTION_STATUS_UNSUBSCRIBE = 'Unsubscribe';
    public static final String SUBSCRIPTION_STATUS_HELD = 'Held';
    
    public static final String ORDER_RECORD_TYPE_MEMBERSHIP = 'Membership';
    public static final String ORDER_RECORD_TYPE_SPONSORSHIP = 'Sponsorship';
    public static final String ORDER_RECORD_TYPE_ADVERTISING = 'Advertising';
    public static final String ORDER_RECORD_TYPE_EXHIBITOR = 'Exhibitor';
    public static final String ORDER_RECORD_TYPE_ONLINE_SUBSCRIPTION = 'Online Subscription';
    
    public static final String ENTITY_ACTIVE = 'Active';
    public static final String ENTITY_CREDIT_CARD_ISSUERS_ACTIVE = 'Active';
    
    public static final String BATCH_RECORD_TYPE_AUTOMATIC = 'Automatic';
    public static final String BATCH_RECORD_TYPE_MANUAL = 'Manual';
    
    public static final String BATCH_STATUS_OPEN = 'Open';
    public static final String BATCH_STATUS_PENDING = 'Pending';
    public static final String BATCH_STATUS_POSTED = 'Posted';
    public static final String BATCH_STATUS_EXPORTED = 'Exported';
    
    public static final String ORDER_STATUS_CANCELLED = 'Cancelled';
    public static final String ORDER_ITEM_LINE_STATUS_ACTIVE = 'Active';
    public static final String ORDER_ITEM_LINE_STATUS_CANCELLED = 'Cancelled';
    
    public static final String JOINT_BILLING_ITEM_LINE_TYPE_NEW = 'New';		  
    public static final String JOINT_BILLING_ITEM_LINE_TYPE_RENEW = 'Rnew';		
    public static final String JOINT_BILLING_ITEM_LINE_TYPE_ADJUSTMENT = 'Adjustment';		   
    public static final String JOINT_BILLING_ITEM_LINE_TYPE_CANCELED = 'Canceled';
    
    public static final String DEAL_STATUS_NEW = 'New';
    public static final String DEAL_STATUS_READY_FOR_CONVERT = 'Ready for Convert';
    
    public static final String CEO_TITLE = 'CEO';
    public static final String CFO_TITLE = 'CFO';
    
    public static final String BILL_JOINTLY_MULTISITE_ONLY_VAL_RULE_MSG = 'Only Multi-Site Organizations can be billed jointly.';
    
    public static final String STATE_PARTNER_EDIT_VAL_MSG = 'You can only edit providers in your state or whose multi-site organization is in your state.';
    public static final String STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG = 'You can only edit provider memberships in your state.';
    
    public static final String DEAL_EVENT_ENTITY_DIFFERENT_FROM_DEAL_ITEMS_ENTITY_VAL_MSG = 'The event must be from the same entity as the deal items.';
    
    public static final String CONFERENCE_PROPOSAL_STATUS_ACCEPTED = 'Accepted';
    public static final String CONFERENCE_PROPOSAL_STATUS_MAYBE = 'Maybe';
    public static final String CONFERENCE_PROPOSAL_STATUS_REJECTED = 'Rejected';
    
    public static final String CONFERENCE_SPEAKER_ROLE_SPEAKER = 'Speaker';
    public static final String CONFERENCE_SPEAKER_ROLE_KEY_CONTACT = 'Key Contact';
    
    public static final String LEADINGAGE_ENTITY_NAME = 'LeadingAge';
    public static final String IAHSA_ENTITY_NAME = 'International Association of Homes and Services for the Ageing';
    
    public static final String INTEGRATION_USER_PROFILE_NAME = 'Nimble AMS Integration User';
    public static final String STATE_ASSOCIATION_PARTNER_USER_PROFILE_NAME = 'State Association Partner User';
    
    public static final String GENERAL = 'General';
    
    public static final String EVENT_TYPE_CONFERENCE = 'Conference';
    
    public static final Integer PROPOSAL_NUMBER_STARTING_NUMBER = 1;
    
    public static final String JOINT_BILLING_ITEM_LINE_CATEGORY_2Y1 = '2Y1';
    public static final String JOINT_BILLING_ITEM_LINE_CATEGORY_REG = 'Reg';
    
    public static final Decimal PROVIDER_SERVICE_FEE_PERCENTAGE = 0.15;
}