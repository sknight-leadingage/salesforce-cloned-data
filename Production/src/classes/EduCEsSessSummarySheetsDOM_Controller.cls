public with sharing class EduCEsSessSummarySheetsDOM_Controller extends EduReportsControllerBase {
    private string mtype{get;set;}
    public string getMType() {
        if (mtype == null || mtype == '') {
            mtype = ApexPages.CurrentPage().getParameters().get('mt');
            if (mtype == null) {
                mtype = '';
            }
        }
        return mtype;
    }

    public List<Conference_Session__c> SumList { get;set; }
    
    public string getPageSubtitle() {
        if (getMType() == '') {
            return 'Session Summary Sheets with Domains of Practice';
        }
        else if (getMType() == 'sn') {
            return 'Session Summary Sheets (by Session #)';
        }
        else if (getMType() == 'ts') {
            return 'Session Summary Sheets (by Timeslot)';
        }
        else {
            return '';
        }
    }
    
    public override string getPageTitle() {
        return RptRenderAs == '' && Conference != null ? Conference.Name + ': ' + getPageSubtitle() : '';
    }
    
    public EduCEsSessSummarySheetsDOM_Controller() {
        SetConPageSize = 255;
        SetQuery();
    }
    
    public override void SetConRefreshData() {
        SumList = setCon.getRecords();
    }
    
    public void SetQuery() {
        SetConQuery = 'SELECT Expected_Attendance__c, Session_Title_Full__c, Timeslot__r.Timeslot__c, Learning_Objective_1__c, Learning_Objective_2__c, Learning_Objective_3__c, NASBA__c, Kansas__c, Missouri__c, NASBA__r.Full_Name__c, Kansas__r.Full_Name__c, Missouri__r.Full_Name__c, (SELECT Speaker__r.Name, Speaker__r.PersonTitle, Speaker__r.Speaker_Company_Name_Rollup__c, Speaker__r.ShippingCity, Speaker__r.ShippingState, Speaker__r.Phone, Speaker__r.PersonEmail, Role__c FROM Conference_Session_Speakers__r WHERE Role__c = \'Moderator\' OR Role__c = \'Speaker\' OR Role__c = \'Key Contact\' ORDER BY Speaker__r.LastName) FROM Conference_Session__c WHERE Conference__c = :EventId';

        if (getMType() == '') {
            SetConQuery += ' ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        else if (getMType() == 'sn') {
            SetConQuery += ' ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        else if (getMType() == 'ts') {
            SetConQuery += ' ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}