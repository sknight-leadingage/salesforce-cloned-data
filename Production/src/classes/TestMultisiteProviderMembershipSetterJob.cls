/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMultisiteProviderMembershipSetterJob {

    static void assertProviderMembership(Id expectedProviderMembershipId, Set<Id> accountIds){
    	List<Account> accounts = getAccounts(accountIds);
    	
    	for (Account account : accounts){
    		system.assertEquals(expectedProviderMembershipId, account.Provider_Membership__c);
    	}
    }
    
    static void assertProviderMembership(Id expectedProviderMembershipId, Id accountId){
    	assertProviderMembership(expectedProviderMembershipId, new Set<Id>{ accountId });
    }
    
    static void assertNoProviderMembership(Set<id> accountids){
    	assertProviderMembership(null, accountIds);
    }
    
    static void assertNoProviderMembership(Id accountId){
    	assertProviderMembership(null, accountId);
    }
    
    static void assertMSOsProviderMembershipNotNullifiedFromRevocationFlowdown(Id expectedProviderMembershipId){
    	NU__Membership__c providerMembership = 
    	[select id,
    	        name,
    	        NU__Account__r.Provider_Membership__c
    	   from NU__Membership__c
    	  where id = :expectedProviderMembershipId];
    	
    	system.assertEquals(expectedProviderMembershipId, providerMembership.NU__Account__r.Provider_Membership__c);
    }
    
    static List<Account> getAccounts(Set<Id> accountIds){
    	return [select id,
    	               name,
    	               Provider_Membership__c
    	          from Account
    	         where id in :accountIds];
    }
    
    static Account insertMSOWithProviderMembers(Integer numberOfProviderMembers){
    	Account mso = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	List<Account> providerAccounts = new List<Account>();
    	for (Integer i = 0; i < numberOfProviderMembers; ++i){
    		Account providerAccount = DataFactoryAccountExt.createProviderAccount(null);
    		providerAccounts.add(providerAccount);
    	}
    	
    	insert providerAccounts;
    	
    	Set<Id> providerAccountIds = NU.CollectionUtil.getSobjectIds(providerAccounts);
    	
    	NU.DataFactoryAffiliation.insertPrimaryAffiliations(mso.Id, providerAccountIds);
    	
    	List<NU__Membership__c> providerMemberships = new List<NU__Membership__c>();
    	
    	for (Account providerAccount : providerAccounts){
    		NU__Membership__c providerMembership = DataFactoryMembershipExt.createCurrentProviderMembership(providerAccount.Id);
    		providerMemberships.add(providerMembership);
    	}
    	
    	insert providerMemberships;
    	
    	return mso;
    }
    
    static void executeSetterJob(){
    	Test.startTest();
    	
    	MultiSiteProviderMembershipSetterJob.executeNow();
    	
    	Test.stopTest();
    }
    
    static testmethod void msoProviderMembershipNotSetWithNoProvidersTest(){
    	Account mso = DataFactoryAccountExt.insertMultiSiteAccount();
    	
    	executeSetterJob();
    	
    	assertNoProviderMembership(mso.Id);
    }
    
    static testmethod void msoProviderMembershipNotSetWithOneProviderMemberTest(){
    	Account mso = insertMSOWithProviderMembers(1);
    	
    	executeSetterJob();
    	
    	assertNoProviderMembership(mso.Id);
    }
    
    static testmethod void msoProviderMembershipSetWithTwoProviderMembersTest(){
    	Account mso = insertMSOWithProviderMembers(2);
    	
    	executeSetterJob();
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	assertProviderMembership(expectedProviderMembershipId, mso.Id);
    }
    
    static testmethod void msoProviderMembershipSetWithFlowdownWithTwoProviderMembersTest(){
    	Account mso = insertMSOWithProviderMembers(2);
    	Account msoEmployee = DataFactoryAccountExt.insertIndividualAccount();
    	NU.DataFactoryAffiliation.insertAffiliation(msoEmployee.Id, mso.Id, true);
    	
    	
    	executeSetterJob();
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	assertProviderMembership(expectedProviderMembershipId, new Set<Id>{ mso.Id, msoEmployee.Id });
    }
    
    static testmethod void msoProviderMembershipSetWithNoFlowdownTwoLevelsWithTwoProviderMembersTest(){
    	Account mso = insertMSOWithProviderMembers(2);

    	Account nonMemberProvider = DataFactoryAccountExt.insertProviderAccount();
    	Account nonMemberProviderEmployee = DataFactoryAccountExt.insertIndividualAccount();
    	NU.DataFactoryAffiliation.insertAffiliation(nonMemberProviderEmployee.Id, nonMemberProvider.Id, true);
    	NU.DataFactoryAffiliation.insertAffiliation(nonMemberProvider.Id, mso.Id, true);
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	executeSetterJob();
    	
    	assertProviderMembership(expectedProviderMembershipId, new Set<Id>{ mso.Id });
    	assertNoProviderMembership(new Set<Id>{ nonMemberProvider.Id, nonMemberProviderEmployee.Id });
    }
    
    static testmethod void msoProviderMembershipRevokedFromOneOfTwoProviderMembershipsLapsingTest(){
    	Integer numberOfProviders = 2;
    	
    	Account mso = insertMSOWithProviderMembers(numberOfProviders);
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	new MultisiteProviderMembershipSetter(mso.Id).Set();
    	
    	assertProviderMembership(expectedProviderMembershipId, mso.Id);

    	Account providerMember = msoQueried.NU__Accounts1__r[0];
    	providerMember.Provider_Lapsed_On__c = Date.Today().addDays(-2);
    	update providerMember;
    	
    	NU__Membership__c membershipPatchLapsedOn = new NU__Membership__c(Id = expectedProviderMembershipId, Last_Provider_Lapsed_On__c = providerMember.Provider_Lapsed_On__c);
    	update membershipPatchLapsedOn;
    	
    	executeSetterJob();
    	
    	assertNoProviderMembership(mso.Id);
    	assertMSOsProviderMembershipNotNullifiedFromRevocationFlowdown(expectedProviderMembershipId);
    }
    
    static testmethod void msoProviderMembershipWithOneFlowdownLevelRevokedFromOneOfTwoProviderMembershipsLapsingTest(){
    	Integer numberOfProviders = 2;
    	
    	Account mso = insertMSOWithProviderMembers(numberOfProviders);
    	Account msoEmployee = DataFactoryAccountExt.insertIndividualAccount();
    	NU.DataFactoryAffiliation.insertAffiliation(msoEmployee.Id, mso.Id, true);
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	new MultisiteProviderMembershipSetter(mso.Id).Set();
    	
    	assertProviderMembership(expectedProviderMembershipId, new Set<Id>{ mso.Id, msoEmployee.Id });
    	
    	Account providerMember = msoQueried.NU__Accounts1__r[0];
    	providerMember.Provider_Lapsed_On__c = Date.Today().addDays(-2);
    	update providerMember;
    	
    	NU__Membership__c membershipPatchLapsedOn = new NU__Membership__c(Id = expectedProviderMembershipId, Last_Provider_Lapsed_On__c = providerMember.Provider_Lapsed_On__c);
    	update membershipPatchLapsedOn;
    	
    	executeSetterJob();
    	
    	assertNoProviderMembership(new Set<Id>{ mso.Id, msoEmployee.Id });
    	assertMSOsProviderMembershipNotNullifiedFromRevocationFlowdown(expectedProviderMembershipId);
    }
    
    static testmethod void msoProviderMembershipChangedToDifferentProviderMembershipBecauseOriginalOneLapsedTest(){
    	Account mso = insertMSOWithProviderMembers(3);
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	new MultisiteProviderMembershipSetter(mso.Id).Set();
    	
    	assertProviderMembership(expectedProviderMembershipId, mso.Id);
    	
    	Account providerMember = [select id from Account where id in (select NU__Account__c from NU__Membership__c where id = :expectedProviderMembershipId)];
    	
    	providerMember.Provider_Lapsed_On__c = Date.Today().addDays(-2);
    	update providerMember;
    	
    	NU__Membership__c membershipPatchLapsedOn = new NU__Membership__c(Id = expectedProviderMembershipId, Last_Provider_Lapsed_On__c = providerMember.Provider_Lapsed_On__c);
    	update membershipPatchLapsedOn;
    	
    	msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	executeSetterJob();
    	
    	assertProviderMembership(expectedProviderMembershipId, mso.Id);
    }
    
    static testmethod void corporateMSOProviderMembershipNotRevokedWhenLessThanTwoProviderMembersTest(){
    	Integer numberOfProviders = 2;
    	
    	Account mso = insertMSOWithProviderMembers(numberOfProviders);
    	mso.Multi_Site_Corporate__c = true;
    	update mso;
    	
    	Account msoQueried = AccountQuerier.getMultiSiteOrgWithProviderMembers(mso.Id);
    	Id expectedProviderMembershipId = MultisiteProviderMembershipSetter.getLatestProviderMembershipId(msoQueried);
    	
    	new MultisiteProviderMembershipSetter(mso.Id).Set();
    	
    	assertProviderMembership(expectedProviderMembershipId, mso.Id);
    	
    	Account providerMember = msoQueried.NU__Accounts1__r[0];
    	providerMember.Provider_Lapsed_On__c = Date.Today().addDays(-2);
    	update providerMember;
    	
    	NU__Membership__c membershipPatchLapsedOn = new NU__Membership__c(Id = expectedProviderMembershipId, Last_Provider_Lapsed_On__c = providerMember.Provider_Lapsed_On__c);
    	update membershipPatchLapsedOn;
    	
    	executeSetterJob();
    	
    	assertProviderMembership(expectedProviderMembershipId, mso.Id);
    	assertMSOsProviderMembershipNotNullifiedFromRevocationFlowdown(expectedProviderMembershipId);
    }
}