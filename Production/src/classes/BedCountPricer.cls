public with sharing class BedCountPricer extends MembershipPricerBase implements IProductPricer {
	
	private String getNotNull(String s){
		if (s == null){
				return '';
		}
		else {
				return s;
		}
	}
	
    private Boolean isStateDues(NU__MembershipTypeProductLink__c mtpl){
        return mtpl != null &&
               (mtpl.State_Dues__c == true);
    }
    
    public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
		//customer & product entity should match
		if (
				(customer != null && customer.NU__PrimaryEntity__c != null && productToPrice != null && productToPrice.NU__Entity__c != null
					&& !getNotNull(customer.NU__PrimaryEntity__r.Name).equalsIgnoreCase(productToPrice.NU__Entity__r.Name))
				||
				(customer == null || customer.NU__PrimaryEntity__c == null || customer.NU__PrimaryEntity__r.Name == null)
			)
		{
			system.debug('   BedCountPricer:: customer entity and product entity not equal');
			return null;
		}
		
		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		NU__MembershipTypeProductLink__c mtpl = null;
		if (membershipType != null && productToPrice != null) {
			mtpl = findMembershipTypeProductLink(membershipType, productToPrice);
		}
		else {
			system.debug('   BedCountPricer:: membershipType or productToPrice is null');
			return null;
		}
		
		if (mtpl == null){
			system.debug('   BedCountPricer:: Membership Type Product Link is null');
			return null;
		}
		
		//poduct type should be membership
		if (productPricingRequest.MembershipTypeId == null){
			system.debug('   BedCountPricer:: Membership Type Id is null');
			return null;
		}
		
		//Check the State Dues checkbox - it should be checked (E.g. State Dues Membership Type Product Link)
		if (isStateDues(mtpl) == false){
			system.debug('   BedCountPricer:: Is not State Dues');
			return null;
		}
		
		Decimal providerDuesPrice = 0;
		
		if(customer.For_Profit__c)
		{
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Assisted Living Bed') != false)
			{
			   if(customer.Number_Of_Assisted_Living_Units__c != null && customer.NU__PrimaryEntity__r.FP_Assisted_Living_Beds__c != null)
			   providerDuesPrice = customer.Number_Of_Assisted_Living_Units__c * customer.NU__PrimaryEntity__r.FP_Assisted_Living_Beds__c ;
			}
		  		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Independent Living Unit') != false)
			{
				if(customer.Number_of_Independent_Living_Units__c != null && customer.NU__PrimaryEntity__r.FP_Independent_Living_Units__c != null)
			   providerDuesPrice = customer.Number_of_Independent_Living_Units__c * customer.NU__PrimaryEntity__r.FP_Independent_Living_Units__c ;
			}
		  		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Intermediate Care Bed') != false)
			{
				if(customer.Number_of_Intermedicate_Care_Beds__c != null && customer.NU__PrimaryEntity__r.FP_Intermedicate_Care_Beds__c != null)
			   providerDuesPrice = customer.Number_of_Intermedicate_Care_Beds__c * customer.NU__PrimaryEntity__r.FP_Intermedicate_Care_Beds__c ;
			}
				
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Nursing Home Bed') != false)
			{
				if(customer.Number_Of_Nursing_Beds__c != null && customer.NU__PrimaryEntity__r.FP_Nursing_Home_Beds__c != null)
			   providerDuesPrice = customer.Number_Of_Nursing_Beds__c * customer.NU__PrimaryEntity__r.FP_Nursing_Home_Beds__c ;
			}	
		 		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Personal Care Bed') != false)
			{
				if(customer.Number_of_Personal_Care_Beds__c != null && customer.NU__PrimaryEntity__r.FP_Personal_Care_Beds__c != null)
			   providerDuesPrice = customer.Number_of_Personal_Care_Beds__c * customer.NU__PrimaryEntity__r.FP_Personal_Care_Beds__c ;
			}	
			
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('ICF/IID Bed') != false)
			{
				if(customer.ICF_Beds__c != null && customer.NU__PrimaryEntity__r.FP_ICF_IID_Beds__c != null)
			   providerDuesPrice = customer.ICF_Beds__c * customer.NU__PrimaryEntity__r.FP_ICF_IID_Beds__c ;
			} 
			
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Skilled Nursing Facility Beds') != false)
			{
				if(customer.Number_of_Skilled_Nursing_Facility_Beds__c != null && customer.NU__PrimaryEntity__r.FP_Skilled_Nursing_Facility_Units__c != null)
			   providerDuesPrice = customer.Number_of_Skilled_Nursing_Facility_Beds__c * customer.NU__PrimaryEntity__r.FP_Skilled_Nursing_Facility_Units__c ;
			} 
			 		  		
		}
		  //     -> Non-profit ICF/IID Bed
		  //            Bed count from Account
		  //            Unit cost from Entity
		  //            Also National Dues portion (Is calculated automatically in the order if the national dues is added as a primary product)
		else
		{
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Assisted Living Bed') != false)
			{
				if(customer.Number_Of_Assisted_Living_Units__c != null && customer.NU__PrimaryEntity__r.NP_Assisted_Living_Beds__c != null)
			   providerDuesPrice = customer.Number_Of_Assisted_Living_Units__c * customer.NU__PrimaryEntity__r.NP_Assisted_Living_Beds__c ;
			}
		  		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Independent Living Unit') != false)
			{
				if(customer.Number_of_Independent_Living_Units__c != null && customer.NU__PrimaryEntity__r.NP_Independent_Living_Units__c != null)
			   providerDuesPrice = customer.Number_of_Independent_Living_Units__c * customer.NU__PrimaryEntity__r.NP_Independent_Living_Units__c ;
			}
		  		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Intermediate Care Bed') != false)
			{
				if(customer.Number_of_Intermedicate_Care_Beds__c != null && customer.NU__PrimaryEntity__r.NP_Intermedicate_Care_Beds__c != null)
			   providerDuesPrice = customer.Number_of_Intermedicate_Care_Beds__c * customer.NU__PrimaryEntity__r.NP_Intermedicate_Care_Beds__c ;
			}
		  		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Nursing Home Bed') != false)
			{
				if(customer.Number_Of_Nursing_Beds__c != null && customer.NU__PrimaryEntity__r.NP_Nursing_Home_Beds__c != null)
			   providerDuesPrice = customer.Number_Of_Nursing_Beds__c * customer.NU__PrimaryEntity__r.NP_Nursing_Home_Beds__c ;
			}	
		  		
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Personal Care Bed') != false)
			{
				if(customer.Number_of_Personal_Care_Beds__c != null && customer.NU__PrimaryEntity__r.NP_Personal_Care_Beds__c != null)
			   providerDuesPrice = customer.Number_of_Personal_Care_Beds__c * customer.NU__PrimaryEntity__r.NP_Personal_Care_Beds__c ;
			}	 
			
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('ICF/IID Bed') != false)
			{
				if(customer.ICF_Beds__c != null && customer.NU__PrimaryEntity__r.NP_ICF_IID_Beds__c != null)
			   providerDuesPrice = customer.ICF_Beds__c * customer.NU__PrimaryEntity__r.NP_ICF_IID_Beds__c ;
			} 
			
			if(getNotNull(productToPrice.Name).equalsIgnoreCase('Skilled Nursing Facility Beds') != false)
			{
				if(customer.Number_of_Skilled_Nursing_Facility_Beds__c != null && customer.NU__PrimaryEntity__r.NP_Skilled_Nursing_Facility_Units__c != null)
			   providerDuesPrice = customer.Number_of_Skilled_Nursing_Facility_Beds__c * customer.NU__PrimaryEntity__r.NP_Skilled_Nursing_Facility_Units__c ;
			} 		
		}
		
		if (providerDuesPrice > 0) {
			return providerDuesPrice;
		}
		else {
			return null;
		}
    }
}