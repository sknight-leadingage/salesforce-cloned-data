/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAppendZZZToEmails {

	private static void Setup() {
		List<Account> accountList = new List<Account>();
		for (integer i = 0; i < 10; i++) {
			Account a = NU.DataFactoryAccount.createIndividualAccount();
			a.NU__OtherEmail__c = i + '@nimbleuser.com';
			accountList.add(a);
		}
		insert accountList;
	}

    static testMethod void TestAppendZZZ() {
    	Setup();
		test.startTest();
		Database.executeBatch(new AppendZZZtoEmails());
		test.stopTest();

		Organization org = [select Id, IsSandbox from Organization limit 1];
		List<Account> accountList = [SELECT PersonEmail, NU__OtherEmail__c, NU__PersonEmail__c, NU__PrimaryContactEmail__c FROM Account];

		if (org.IsSandbox || test.isRunningTest()) {
			for (Account a : accountList) {
/*
				if (String.isBlank(a.PersonEmail) || a.PersonEmail.endsWith('.zzz')) {
					System.assert (true);
				}
				else {
					System.assert (false, 'PersonEmail is invalid');
				}
*/	
				if (String.isBlank(a.NU__OtherEmail__c) || a.NU__OtherEmail__c.endsWith('.zzz')) {
					System.assert (true);
				}
				else {
					System.assert (false, 'NU__OtherEmail__c is invalid');
				}
	
				if (String.isBlank(a.NU__PersonEmail__c) || a.NU__PersonEmail__c.endsWith('.zzz')) {
					System.assert (true);
				}
				else {
					System.assert (false, 'NU__PersonEmail__c is invalid');
				}

				if (String.isBlank(a.NU__PrimaryContactEmail__c) || a.NU__PrimaryContactEmail__c.endsWith('.zzz')) {
					System.assert (true);
				}
				else {
					System.assert (false, 'NU__PrimaryContactEmail__c is invalid');
				}
			}
		}
    }
}