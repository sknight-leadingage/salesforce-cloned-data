/*
* ProgramRevenue Woo test methods
*/
@isTest
private class TestAccountTrigger_ProgramRevenue {
	static testmethod void setProviderRevenueOnInsertAndUpdateTest(){
		decimal PSRVal1 = 900000;
		decimal PSRVal2 = 950000;
		String PSRYear = string.valueof(DateTime.Now().year());
		
		//Step 1: Create an Account and Assign Program Revenue
		Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
		
		Account statePartnerProvider = DataFactoryAccountExt.createProviderAccount(PSRVal1);
		statePartnerProvider.Revenue_Year_Submitted__c = PSRYear;
		statePartnerProvider.State_Partner_ID__c = statePartnerAccount.Id;
		
       	insert statePartnerProvider;
       	
       	List<Account> lTestData = [SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID = :statePartnerProvider.Id];
       	
       	system.assertEquals(lTestData.size(), 1, 'Failed to create the Test account!');
       	
       	Account aTestData = lTestData[0];
       	system.assertNotEquals(aTestData, null);
       	system.assertEquals(aTestData.Program_Service_Revenue__c, PSRVal1);
       	system.assertEquals(aTestData.Multi_Site_Program_Service_Revenue__c, null);
       	system.assertEquals(aTestData.Program_Revenue_History__r.size() > 0, true);

		ProgramRevenueHistory__c psrhItem = null;
       	for(ProgramRevenueHistory__c psrh : aTestData.Program_Revenue_History__r) {
       		if (psrh.Revenue_Year_Submitted__c == PSRYear && psrh.Program_Service_Revenue__c == PSRVal1) {
       			psrhItem = psrh;
       		}
       	}
       	system.assertNotEquals(psrhItem, null, 'No history entry created for Account: ' + aTestData.Id +
       											', with Program_Service_Revenue__c = ' + aTestData.Program_Service_Revenue__c +
       											' and Revenue_Year_Submitted__c = ' + aTestData.Revenue_Year_Submitted__c);
        
        //Step 2: Update PSR with a new value
		statePartnerProvider.Program_Service_Revenue__c = PSRVal2;
       	update statePartnerProvider;
       	
       	lTestData = [SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID = :statePartnerProvider.Id];
        
		aTestData = lTestData[0];
       	system.assertEquals(aTestData.Program_Service_Revenue__c, PSRVal2, 'Update to PSR failed, Account: ' + aTestData.Id + 
       																		', Old Value = ' + PSRVal1 + ', New Value = ' + PSRVal2);
       	system.assertEquals(aTestData.Multi_Site_Program_Service_Revenue__c, null);
       	system.assertEquals(aTestData.Program_Revenue_History__r.size() > 0, true);
       	
		psrhItem = null;
       	for(ProgramRevenueHistory__c psrh : aTestData.Program_Revenue_History__r) {
       		if (psrh.Revenue_Year_Submitted__c == PSRYear && psrh.Program_Service_Revenue__c == PSRVal2) {
       			psrhItem = psrh;
       		}
       	}
       	system.assertNotEquals(psrhItem, null, 'No history entry created for PSR Update. Account: ' + aTestData.Id +
       											', with Program_Service_Revenue__c = ' + aTestData.Program_Service_Revenue__c +
       											' and Revenue_Year_Submitted__c = ' + aTestData.Revenue_Year_Submitted__c);
																				
		//Step 3: Update PSR with a previous value (E.g. return current PSR to a prior value)
		statePartnerProvider.Program_Service_Revenue__c = PSRVal1;
       	update statePartnerProvider;
       	
       	lTestData = [SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID = :statePartnerProvider.Id];
        
		aTestData = lTestData[0];
       	system.assertEquals(aTestData.Program_Service_Revenue__c, PSRVal1, 'Update to PSR failed, Account: ' + aTestData.Id + 
       																		', Old Value = ' + PSRVal2 + ', New Value = ' + PSRVal1);
       	system.assertEquals(aTestData.Multi_Site_Program_Service_Revenue__c, null);
       	system.assertEquals(aTestData.Program_Revenue_History__r.size() > 0, true);
       	
		psrhItem = null;
       	for(ProgramRevenueHistory__c psrh : aTestData.Program_Revenue_History__r) {
       		if (psrh.Revenue_Year_Submitted__c == PSRYear && psrh.Program_Service_Revenue__c == PSRVal1) {
       			psrhItem = psrh;
       		}
       	}
       	system.assertNotEquals(psrhItem, null, 'No history entry created for PSR Update. Account: ' + aTestData.Id +
       											', with Program_Service_Revenue__c = ' + aTestData.Program_Service_Revenue__c +
       											' and Revenue_Year_Submitted__c = ' + aTestData.Revenue_Year_Submitted__c);
	}
	
	static testmethod void setMultiSiteRevenueOnInsertAndUpdateTest(){
		decimal PSRVal1 = 900000;
		decimal PSRVal2 = 950000;
		String PSRYear = string.valueof(DateTime.Now().year());
		
		//Step 1: Create an Account and Assign Program Revenue
		Account statePartnerAccount = DataFactoryAccountExt.insertStatePartnerAccount();
		
		Account statePartnerMSO = DataFactoryAccountExt.createMultiSiteAccount(PSRVal1);
		statePartnerMSO.Revenue_Year_Submitted__c = PSRYear;
		statePartnerMSO.State_Partner_ID__c = statePartnerAccount.Id;
		
       	insert statePartnerMSO;
       	
       	List<Account> lTestData = [SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID = :statePartnerMSO.Id];
       	
       	system.assertEquals(lTestData.size(), 1, 'Failed to create the Test account!');
       	
       	Account aTestData = lTestData[0];
       	system.assertNotEquals(aTestData, null);
       	system.assertEquals(aTestData.Multi_Site_Program_Service_Revenue__c, PSRVal1);
       	system.assertEquals(aTestData.Program_Service_Revenue__c, null);
       	system.assertEquals(aTestData.Program_Revenue_History__r.size() > 0, true);

		ProgramRevenueHistory__c psrhItem = null;
       	for(ProgramRevenueHistory__c psrh : aTestData.Program_Revenue_History__r) {
       		if (psrh.Revenue_Year_Submitted__c == PSRYear && psrh.Program_Service_Revenue__c == PSRVal1) {
       			psrhItem = psrh;
       		}
       	}
       	system.assertNotEquals(psrhItem, null, 'No history entry created for Account: ' + aTestData.Id +
       											', with Multi_Site_Program_Service_Revenue__c = ' + aTestData.Multi_Site_Program_Service_Revenue__c +
       											' and Revenue_Year_Submitted__c = ' + aTestData.Revenue_Year_Submitted__c);
        
        //Step 2: Update PSR with a new value
		statePartnerMSO.Multi_Site_Program_Service_Revenue__c = PSRVal2;
       	update statePartnerMSO;
       	
       	lTestData = [SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID = :statePartnerMSO.Id];
        
		aTestData = lTestData[0];
       	system.assertEquals(aTestData.Multi_Site_Program_Service_Revenue__c, PSRVal2, 'Update to Multi-Site PSR failed, Account: ' + aTestData.Id + 
       																		', Old Value = ' + PSRVal1 + ', New Value = ' + PSRVal2);
       	system.assertEquals(aTestData.Program_Service_Revenue__c, null);
       	system.assertEquals(aTestData.Program_Revenue_History__r.size() > 0, true);
       	
		psrhItem = null;
       	for(ProgramRevenueHistory__c psrh : aTestData.Program_Revenue_History__r) {
       		if (psrh.Revenue_Year_Submitted__c == PSRYear && psrh.Program_Service_Revenue__c == PSRVal2) {
       			psrhItem = psrh;
       		}
       	}
       	system.assertNotEquals(psrhItem, null, 'No history entry created for PSR Update. Account: ' + aTestData.Id +
       											', with Multi_Site_Program_Service_Revenue__c = ' + aTestData.Multi_Site_Program_Service_Revenue__c +
       											' and Revenue_Year_Submitted__c = ' + aTestData.Revenue_Year_Submitted__c);
																				
		//Step 3: Update PSR with a previous value (E.g. return current PSR to a prior value)
		statePartnerMSO.Multi_Site_Program_Service_Revenue__c = PSRVal1;
       	update statePartnerMSO;
       	
       	lTestData = [SELECT Id, Name, Revenue_Year_Submitted__c, Program_Service_Revenue__c, Multi_Site_Program_Service_Revenue__c,
            (Select Account__c,Revenue_Year_Submitted__c,Program_Service_Revenue__c FROM Program_Revenue_History__r)  FROM Account WHERE ID = :statePartnerMSO.Id];
        
		aTestData = lTestData[0];
       	system.assertEquals(aTestData.Multi_Site_Program_Service_Revenue__c, PSRVal1, 'Update to Multi-Site PSR failed, Account: ' + aTestData.Id + 
       																		', Old Value = ' + PSRVal2 + ', New Value = ' + PSRVal1);
       	system.assertEquals(aTestData.Program_Service_Revenue__c, null);
       	system.assertEquals(aTestData.Program_Revenue_History__r.size() > 0, true);
       	
		psrhItem = null;
       	for(ProgramRevenueHistory__c psrh : aTestData.Program_Revenue_History__r) {
       		if (psrh.Revenue_Year_Submitted__c == PSRYear && psrh.Program_Service_Revenue__c == PSRVal1) {
       			psrhItem = psrh;
       		}
       	}
       	system.assertNotEquals(psrhItem, null, 'No history entry created for PSR Update. Account: ' + aTestData.Id +
       											', with Multi_Site_Program_Service_Revenue__c = ' + aTestData.Multi_Site_Program_Service_Revenue__c +
       											' and Revenue_Year_Submitted__c = ' + aTestData.Revenue_Year_Submitted__c);
	}
}