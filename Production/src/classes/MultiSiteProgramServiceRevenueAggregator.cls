/**
 * @author NimbleUser
 * @date Updated: 2/6/13
 * @description This class aggregates the Program Service Revenue from all the MultiSite Providers
 * to their primary affiliation MultiSite accounts except for "Under Construction" providers.
 */
global class MultiSiteProgramServiceRevenueAggregator implements Database.Batchable<sObject>, Schedulable {
	
	private List<Account> getMultiSiteProviderAccounts(List<Account> multiSiteAccounts){
		return [Select id,
		               name,
		               Program_Service_Revenue__c,
		               Under_Construction__c,
		               NU__PrimaryAffiliation__c
		          from Account
		         where NU__PrimaryAffiliation__c in :multiSiteAccounts
		           and Multi_Site_Enabled__c = true
		           and Program_Service_Revenue__c != null];
	}
	
	global static void scheduleAt1AM() {
		System.schedule('Multi Site Program Service Revenue Aggregator', '0 0 1 * * ?', new MultiSiteProgramServiceRevenueAggregator());
	}

	global void execute(SchedulableContext context) {
		Database.executeBatch(new MultiSiteProgramServiceRevenueAggregator());
	}
	
	public void aggregate(Id multiSiteAccountId){
		Account multiSiteAccount = new Account(Id = multiSiteAccountId);
		
		execute(null, new List<Account>{ multiSiteAccount });
	}
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		Id multisiteRecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID;
		
		return Database.getQueryLocator('select id, name, Multi_Site_Program_Service_Revenue__c from Account where RecordTypeId = :multisiteRecordTypeId');
	}
	
	global void execute(Database.BatchableContext context, List<Account> multiSiteAccounts) {
		Map<Id, Account> multiSiteAccountsMap = new Map<Id, Account>(multiSiteAccounts);
		List<Account> multiSiteProviderAccounts = getMultiSiteProviderAccounts(multiSiteAccounts);
		
		for (Account multiSiteAccount : multiSiteAccounts){
			multiSiteAccount.Multi_Site_Program_Service_Revenue__c = 0;
		}
		
		for (Account multiSiteProviderAccount : multiSiteProviderAccounts){
			if (multiSiteProviderAccount.Under_Construction__c == true){
				continue;
			}

			Account multiSiteAccount = multiSiteAccountsMap.get(multiSiteProviderAccount.NU__PrimaryAffiliation__c);
			
			multiSiteAccount.Multi_Site_Program_Service_Revenue__c += multiSiteProviderAccount.Program_Service_Revenue__c;
		}
		

		update multiSiteAccounts;
	}
	
	global void finish(Database.BatchableContext context) {
		
	}
}