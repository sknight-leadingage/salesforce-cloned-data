global class BatchUpdatePrimaryContact implements Database.Batchable<sObject>, Schedulable
{
	public final String Query = 'SELECT NU__ParentAccount__c, NU__ParentAccount__r.NU__PrimaryContact__c, NU__ParentAccount__r.NU__PrimaryContactEmail__c, NU__ParentAccount__r.NU__PrimaryContactName__c, NU__Account__c, NU__Account__r.Name, NU__Account__r.PersonEmail, NU__IsPrimary__c  FROM NU__Affiliation__c WHERE NU__Role__c INCLUDES(\'' + Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT + '\') ORDER BY NU__ParentAccount__c';
	
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
    	Account a = new Account();
    	Map<Id,Account> mapAccounts = new Map<Id,Account>();
		for(NU__Affiliation__c af : (List<NU__Affiliation__c>)scope){
			a = new Account(Id = af.NU__ParentAccount__c, NU__PrimaryContact__c = af.NU__Account__c, NU__PrimaryContactEmail__c = af.NU__Account__r.PersonEmail, NU__PrimaryContactName__c = af.NU__Account__r.Name);
			if (mapAccounts.ContainsKey(af.NU__ParentAccount__c)){
				if (af.NU__IsPrimary__c == true) {
					mapAccounts.put(af.NU__ParentAccount__c, a);
				}
			} else {
				mapAccounts.put(af.NU__ParentAccount__c, a);
			}
		}
		
		if (mapAccounts.size() > 0){
			update mapAccounts.values();
		}
	}

	global void finish(Database.BatchableContext BC)
	{
	}

	public static void schedule() {
		System.schedule('BatchUpdatePrimaryContact', '0 0 3 * * ?', new BatchUpdatePrimaryContact());
	}

    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }
}