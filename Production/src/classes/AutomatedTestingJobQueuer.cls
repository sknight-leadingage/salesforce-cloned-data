//run unit tests nightly at 4am

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new AutomatedTestingJobQueuer());

//To schedule nightly in an anonymous window in Eclipse or Developer Console:
//AutomatedTestingJobQueuer.createDaily4AMScheduledJob();

//To unschedule, delete the job from the list of scheduled jobs.


global with sharing class AutomatedTestingJobQueuer implements schedulable {
	
	global void execute(SchedulableContext SC) {
		doExecute();
	}
	
	@future (callout=true)
	public static void doExecute(){
		enqueueUnitTests();
	}
	
	webservice static void enqueueUnitTests(){
		system.debug(' In synchronousDoExecute');
		
		AutomatedTestingQueue__c atq = enqueueTests();
		
		if (atq != null){			
			insert atq;
		}
	}

	// Enqueue all custom classes beginning with "Test".  
    public static AutomatedTestingQueue__c enqueueTests() {
        ApexClass[] testClasses = 
           [SELECT Id,
                   Name
              FROM ApexClass ];
      //      WHERE Name LIKE 'Test%'
      //        and NamespacePrefix = null];

        Integer testClassCnt = NU.CollectionUtil.getListCount(testClasses);
        
        system.debug('   enqueueTests::testClassCnt ' + testClassCnt);
            
        if (testClassCnt > 0) {        	
            ApexTestQueueItem[] queueItems = new List<ApexTestQueueItem>();
            
            for (ApexClass testClass : testClasses) {
            	system.debug('   enqueueTests::testClass ' + testClass);
            	
                queueItems.add(new ApexTestQueueItem(ApexClassId=testClass.Id));
            }

            insert queueItems;

            // Get the job ID of the first queue item returned. 
    
            ApexTestQueueItem item = 
               [SELECT ParentJobId FROM ApexTestQueueItem 
                WHERE Id=:queueItems[0].Id LIMIT 1];

            system.debug('   enqueueTests::item ' + item);
            
            AutomatedTestingQueue__c atq = new AutomatedTestingQueue__c(
				AsyncId__c = item.parentjobid,
				NumberOfTestClassesEnqueued__c = testClassCnt
			);

            return atq;
        }
        
        return null;
    }

	public static void createDaily4AMScheduledJob(){
		AutomatedTestingJobQueuer atj = new AutomatedTestingJobQueuer();  
		string sch = '0 0 4 * * ?';  
		system.schedule('Enqueue Unit Tests 4 AM',sch,atj);
	}

}