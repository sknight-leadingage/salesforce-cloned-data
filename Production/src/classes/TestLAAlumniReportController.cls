/*
* Test methods for the LeadershipAcademyAlumniReport_Controller
*/
@isTest(SeeAllData=true)

private class TestLAAlumniReportController{

    static LeadershipAcademyAlumniReport_Controller loadController(){
       PageReference pageRef = Page.LeadershipAcademyAlumniReport;
       Test.setCurrentPage(pageRef);
        
       string SelectedAcademyYear = '2016';
        
       pageRef.getParameters().put(LeadershipAcademyControllerBase.ACADEMY_YEAR_PARAM, SelectedAcademyYear);
        
       LeadershipAcademyAlumniReport_Controller AppController = new LeadershipAcademyAlumniReport_Controller();
        
       return AppController;
     }
    
    static testmethod void DisplayResultTest(){
        LeadershipAcademyAlumniReport_Controller AppController = loadController();
        
        system.assert(AppController.getEventList().size() > 0, 'Event List should have multiple values. Size is: ' + AppController.getEventList().size());
        
        AppController.SelectedEvent = 'a0cd0000003TfPQ'; //2014LeadingAge Annual Meeting
        AppController.Event_onChange();
    }
    
   
}