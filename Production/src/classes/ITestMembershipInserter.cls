/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This interface allows a membership to be inserted for a given account.
 * It's used to for membership flowdown testing.
 */
public interface ITestMembershipInserter {
	NU__Membership__c insertMembership(Id accountId);
}