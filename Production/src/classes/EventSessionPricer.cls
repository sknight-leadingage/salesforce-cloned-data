public with sharing class EventSessionPricer implements IProductPricer {
	public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
		if (productPricingRequest.EventId == null){
			return null;
		}
		
		Decimal price = productToPrice.NU__ListPrice__c;
		
		if (specialPrice != null) {
			NU__Event__c event = (NU__Event__c) extraParams.get('Event');
			
            Boolean isEarlyDate = productPricingRequest.TransactionDate <= event.NU__EarlyRegistrationCutOffDate__c;
            Boolean isEarlyPrice = specialPrice.NU__EarlyPrice__c != null;
            Boolean isLateDate = productPricingRequest.TransactionDate > event.NU__RegularRegistrationCutOffDate__c;
            Boolean isLatePrice = specialPrice.NU__LatePrice__c != null;
            
            if (isEarlyDate && isEarlyPrice) {
            	price = specialPrice.NU__EarlyPrice__c;
            }
            else if (isLateDate && isLatePrice) {
            	price = specialPrice.NU__LatePrice__c;
            }
            else {
            	price = specialPrice.NU__DefaultPrice__c;
            }
        }
		
		return price;
	}
}