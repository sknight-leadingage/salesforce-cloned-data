public with sharing class CustomPersonOnlyMembershipFlowdownMgr extends CustomMembershipFlowdownManagerBase {
	protected override void setInstanceMembers(){
		additionalFlowdownNeeded = false;
    	flowdownToIndividualsOnly = true;
    }
}