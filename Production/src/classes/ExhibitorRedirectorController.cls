public with sharing class ExhibitorRedirectorController {
	private Id objectId = null;
	private Boolean IsNew { get { return objectId == null;} }
    
	// if the id is set, then we're editing the record, not creating a new one
	public ExhibitorRedirectorController(ApexPages.StandardController standardController){
    	if (standardController != null){
        	objectId = standardController.getId();
    	}
	}
    
	public virtual PageReference redirect(){
    	if (IsNew){
        	return getNewRedirect();
    	}
   	 
    	return getEditRedirect();
	}
    
	// for creating a new custom record, redirect to the custom page in the order process. Also sets the return URL.
	public PageReference getNewRedirect(){
    	PageReference ref = Page.OrderExhibitorProducts;
    	ref.getParameters().put('ret', '/' + Schema.SObjectType.Exhibitor__c.getKeyPrefix() + '/o');
    	return ref;
	}

	// for editing a custom record, redirect to the custom page in the order process, modifying the order item that is assigned to the record.
	public PageReference getEditRedirect(){
    	PageReference ref = Page.OrderExhibitorProducts;
    	Id orderItemId = null;
   	 
    	if (this.objectId != null) {
        	Exhibitor__c exhibitor = [SELECT
            	Order_Item_Line__r.NU__OrderItem__c,
            	Order_Item_Line__r.NU__OrderItem__r.NU__Status__c,
            	Order_Item_Line__r.NU__OrderItem__r.NU__Balance__c
            	FROM Exhibitor__c WHERE Id = :objectId];
        	orderItemId = exhibitor.Order_Item_Line__r.NU__OrderItem__c;
       	 
        	// if the status of the order item is cancelled, and we owe them a refund, redirect to the payment/refund page of the order process.
        	if (exhibitor.Order_Item_Line__r.NU__OrderItem__r.NU__Status__c == 'Cancelled' &&
            	exhibitor.Order_Item_Line__r.NU__OrderItem__r.NU__Balance__c < 0)
{
            	ref = Page.NU__OrderPayment;
        	}
    	}
   	 
    	Map<String, String> params = ref.getParameters();
    	params.put('orderItemId', orderItemId);
   	 
    	return ref;
	}
}