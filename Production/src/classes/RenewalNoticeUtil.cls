public with sharing class RenewalNoticeUtil {
	public RenewalNoticeUtil() {}
	
	private static RenewalNoticeUtil m_Instance;
    public static RenewalNoticeUtil Instance {
        get {
            if (m_Instance == null) {
                m_Instance = new RenewalNoticeUtil();
            }
            return m_Instance;
            
        }
    }
	
	public static Date invoiceDateOverride = null;
	public static String membershipRelationship = null;
	public static String affiliationRole = null;
	public static Boolean showAllProducts = false;
	
	private static List<SelectOption> privMembershipOptions;
    public List<SelectOption> MembershipOptions {
    	get {
    		if (privMembershipOptions == null) {
    			privMembershipOptions = calculateMembershipOptions();
    		}
    		return privMembershipOptions;
    	}
    }
    
    private static List<SelectOption> privRoleOptions;
    public List<SelectOption> RoleOptions {
    	get {
    		if (privRoleOptions == null) {
    			privRoleOptions = calculateRoleOptions();
    		}
    		return privRoleOptions;
    	}
    }
	
	public static Set<Id> accountIds = null;
	
	private static Set<Id> membershipTypeIds = null;
	private static List<NU__MembershipType__c> membershipTypes = null;
	public List<NU__MembershipType__c> getMembershipTypes() {
		if (membershipTypes == null) {
			membershipTypes = membershipTypesQuery();
		}
		
		return membershipTypes;
	}
	
	private List<NU__MembershipType__c> membershipTypesQuery() {
		return [SELECT Id, NU__AccountMembershipField__c, Membership_Renewals_Remittance_Text__c FROM NU__MembershipType__c WHERE NU__Status__c = :Constant.MEMBERSHIP_TYPE_STATUS_ACTIVE];
	}
	
	private static List<NU__MembershipTypeProductLink__c> mtpls = null;
	public List<NU__MembershipTypeProductLink__c> getMtpls() {
		if (mtpls == null) {
			mtpls = mtplQuery();
		}
		
		return mtpls;
	}
	
	private List<NU__MembershipTypeProductLink__c> mtplQuery() {
    	return (membershipTypeIds == null || membershipTypeIds.size() == 0 ? new List<NU__MembershipTypeProductLink__c>() :
    		[SELECT Id,
    			NU__Purpose__c,
    			NU__Product__c,
    			NU__Product__r.Name,
    			NU__Product__r.NU__ListPrice__c,
    			NU__MembershipType__c
    			FROM NU__MembershipTypeProductLink__c
    			WHERE NU__Status__c = :Constant.MEMBERSHIP_TYPE_STATUS_ACTIVE
    				AND (NU__Stage__c = :Constant.MEMBERSHIP_TYPE_STAGE_BOTH OR
    					NU__Stage__c = :Constant.MEMBERSHIP_TYPE_STAGE_RENEW)
    				AND NU__MembershipType__c IN :membershipTypeIds
    			Order by NU__DisplayOrder__c, NU__Product__r.Name]);
    }
	
	public static List<Account> validatedAccounts = null;
	public List<Account> getValidatedAccounts() {
		if (validatedAccounts == null) {
			List<Account> accountsToValidate = queryAccounts();
			validatedAccounts = new List<Account>();
	        
	        membershipTypeIds = new Set<Id>();
	        
	        for (Account account: accountsToValidate) {
	            // does the account have a valid membership?
	            if (account.getSObject(membershipRelationship) != null) {
	            	NU__Membership__c membership = (NU__Membership__c)account.getSObject(membershipRelationship);
	            	//Integer gracePeriod = Integer.valueOf(membership.NU__MembershipType__r.NU__GracePeriod__c);
		            //if (Date.today() <= membership.NU__EndDate__c.addMonths(gracePeriod)) {
		                validatedAccounts.add(account);
		                
		                // populate membershipTypeIds for later usage
		            	membershipTypeIds.add(membership.NU__MembershipType__c);
		            //}
	            }
	        }
		}
		
		return (validatedAccounts != null ? validatedAccounts : new List<Account>());
    }
    
    public List<Account> queryAccounts() {
    	List<Account> accounts = new List<Account>();
    	
    	String accountQuery = accountQuery();
		if (accountQuery != null) {
			accounts = Database.query(accountQuery);
		}
		
		return accounts;
    }
    
    private static Date theDate = Date.today();
	private static DateTime now = DateTime.now();
    private String accountQuery() {
    	if (membershipRelationship != null && accountIds != null) {
    		
    		String accountQuery = 'SELECT Id, Name, LeadingAge_ID__c, IsPersonAccount, PersonContactId, PersonEmail, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,'
    			+ membershipRelationship + '.Id, ' + membershipRelationship + '.NU__EndDate__c, ' + membershipRelationship + '.NU__OrderItemLine__r.NU__MembershipTypeProductLink__c, ' 
    			+ membershipRelationship + '.NU__MembershipType__r.NU__Entity__c, ' + membershipRelationship + '.NU__MembershipType__r.NU__Entity__r.Name, ' 
    			+ membershipRelationship + '.NU__MembershipType__r.NU__StartDateControl__c, ' + membershipRelationship + '.NU__MembershipType__r.NU__GracePeriod__c, ' 
    			+ membershipRelationship + '.NU__MembershipType__r.NU__Term__c, '
    			+ membershipRelationship + '.MembershipTypeProductName__c';
			
			if (affiliationRole != null && affiliationRole != '') {
				accountQuery += ', (SELECT Id, NU__Account__c, NU__Account__r.Name, NU__Account__r.IsPersonAccount, NU__Account__r.PersonContactId, NU__Account__r.PersonEmail,'
					+ ' NU__Account__r.BillingStreet, NU__Account__r.BillingCity, NU__Account__r.BillingState, NU__Account__r.BillingPostalCode, NU__Account__r.BillingCountry'
					+ ' FROM NU__Affiliates__r WHERE NU__Role__c INCLUDES(:affiliationRole) AND NU__Status__c=\'' + Constant.AFFILIATION_STATUS_ACTIVE + '\' AND'
					+ ' NU__Account__r.IsPersonAccount = TRUE AND'
					+ ' (NU__EndDate__c = null OR NU__EndDate__c > :theDate) AND'
					+ ' (NU__RemovalDate__c = null OR NU__RemovalDate__c > :now))';
			}
			
			accountQuery += ' FROM Account WHERE Id IN :accountIds';
			System.debug('accountQuery = ' + accountQuery);
    		
    		return accountQuery;
    	}
    	else {
    		return null;
    	}
    }
    
    private List<SelectOption> calculateMembershipOptions() {
    	if (membershipTypes == null) {
    		membershipTypes = membershipTypesQuery();
    	}
    	
    	Set<SelectOption> setOptions = new Set<SelectOption>();
		for (NU__MembershipType__c mt : membershipTypes) {
			Schema.DescribeFieldResult describe = Account.getSObjectType().getDescribe().fields.getMap().get(mt.NU__AccountMembershipField__c).getDescribe();
			String relationshipName = describe.getRelationshipName();
			
			setOptions.add(new SelectOption(relationshipName, describe.getLabel()));
		}
		
		List<SelectOption> options = new List<SelectOption>(setOptions);
		options.sort();
		
		return options;
    }
    
    private List<SelectOption> calculateRoleOptions() {
    	List<SelectOption> options = new List<SelectOption>();
    	
    	Schema.DescribeFieldResult fieldResult = NU__Affiliation__c.NU__Role__c.getDescribe();      
		for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()) {
			if (ple.getLabel().contains('Primary Contact')) {
				options.add(new SelectOption(ple.getLabel(), ple.getValue()));
			}
		}
		
		options.sort();
		
		return options;
    }
}