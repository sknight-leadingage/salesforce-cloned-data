public without sharing class ItemizedPdfController {

	// Production folder: 00ld0000001jWeH, Staging folder: 00lK0000000h2YOIAY
	
	public string PageCType { get; set; } 

    public void SetPageType()
       {
           PageCType = 'application/vnd.ms-excel#Estimated_Annual_Dues.xls';  
       }

    public string SelectedState {get; set; } //the state to be included in the report
    
    private User currentUser {get; set;}
    private User p_pageUser = null;

    //constructor
    public ItemizedPdfController() {
        //get user information    
        Id userId = UserInfo.getUserId();
        currentUser = [Select id, name, Contact.Account.State_Partner_Id__c, Contact.Account.State_Partner_Id__r.Name, Profile.Name from User where id = :userId];
    //get form information
        if (!isUserLeadingAgeStaff)
            SelectedState = String.ValueOf(currentUser.Contact.Account.State_Partner_Id__r.name);
    }
    

    //outputs a (drop-down) list of State Partners, visible to LeadingAge Staff only 
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Account> lData = [
            SELECT ID,
                Name,
                BillingState
            FROM Account
            WHERE RecordType.DeveloperName = 'State_Partner'
            ORDER BY Name];
        for (Account a: lData) {
            options.add(new SelectOption(a.Name, a.Name));
        }
        return options;
    }
    
    //retrieve all account records for a selected state, plus joint billing data for this year and last year
    public List<Document> getProviderAccountsEstimated(){
        if (selectedState == null || selectedState == '-1')
        {
            return new List<Document>(); 
        }
        else
        {
			String queryName = selectedState.replace(' ', '_');
            return (List<Document>)Database.query('SELECT FolderId, Id, Name, LastModifiedDate FROM Document Where (FolderId = \'00lK0000000h2YOIAY\' or FolderId = \'00ld0000001jWeH\')  and name like \'%' + queryName + '%\'');
        }
    }

    //re-runs the report, if the state is changed in the drop-down list visible to LeadingAge staff only
    public void State_onChange() {
        Run();
    }

    //re-runs the report, if the run button is clicked
    public void Run() {
    }

    //retireve information about the user
    private User pageUser
    {
        get
        {
            if (p_pageUser == null)
            {
                p_pageUser = [SELECT ID, CompanyName, Contact.Account.ID FROM User WHERE ID = :UserInfo.getUserID()];
            }
            return p_pageUser;
        }
    }

    //determine if the user has access to all state partner reports, or just a single state partner report
    //When logged in as a State Partner Portal user, use the user --> Contact --> Account.State_Partner_Id
    //to determine the corresponding "State Partner" account.

    //SK TODO 20130308: Need to find out how to do this by id... or if this is even correct

    public boolean isUserLeadingAgeStaff
    {
        get
        {
            return (pageUser.CompanyName != null && pageUser.CompanyName == 'LeadingAge');
        }
    }
}