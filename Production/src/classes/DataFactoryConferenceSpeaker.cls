/**
 * @author NimbleUser
 * @date Updated: 3/22/13
 * @description This class provides various functions for creating and inserting Conference Speakers
 * It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Conference Speakers without inserting them. 
 * The insert* functions create Conference Speakers and insert them.
 */
public with sharing class DataFactoryConferenceSpeaker {

    public static Conference_Speaker__c createConferenceSpeaker(){
        Account account = DataFactoryAccountExt.insertIndividualAccount();
        
        return createConferenceSpeaker(account.Id);
    }

    public static Conference_Speaker__c createConferenceSpeaker(Id accountId){
        Integer len = 10;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
        
        return new Conference_Speaker__c(
            Account__c = accountId,
            First_Name__c = 'Test',
            Last_Name__c = 'TestLastName',
            Email__c = 'test' + pwd + '@example.com'
        );
    }
    
    public static Conference_Speaker__c insertConferenceSpeaker(){
        Conference_Speaker__c speaker = createConferenceSpeaker();
        
        insert speaker;
        return speaker;
    }
    
    public static Conference_Speaker__c insertConferenceSpeakerWithoutAccount(){
        Conference_Speaker__c speaker = createConferenceSpeaker(null);
        
        insert speaker;
        return speaker;
    }
}