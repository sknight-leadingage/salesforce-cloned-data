/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestDefaultProductPricer {

    static testMethod void productListPriceTest() {
        NU__Product__c defaultProduct = NU.DataFactoryProduct.insertDefaultProduct();
        
        DefaultProductPricer dpp = new DefaultProductPricer();
        
        Decimal productPrice = dpp.calculateProductPrice(defaultProduct, null, null, null, null);
        
        system.assertEquals(defaultProduct.NU__ListPrice__c, productPrice);
    }
    
    static testmethod void specialPriceUsedTest(){
    	NU__Product__c defaultProduct = NU.DataFactoryProduct.insertDefaultProduct();
    	
    	NU__SpecialPrice__c specialDefaultProductPrice = NU.DataFactorySpecialPrice.insertSpecialPrice(defaultProduct.Id, NU.Constant.PRICE_CLASS_DEFAULT, 100);
        
        DefaultProductPricer dpp = new DefaultProductPricer();
        
        Decimal productPrice = dpp.calculateProductPrice(defaultProduct, null, specialDefaultProductPrice, null, null);
        
        system.assertEquals(specialDefaultProductPrice.NU__DefaultPrice__c, productPrice);
    }
}