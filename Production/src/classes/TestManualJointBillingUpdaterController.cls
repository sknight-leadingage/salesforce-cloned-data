/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestManualJointBillingUpdaterController {
    static Account StatePartner = null;
    static Account ProviderAccount = null;
    static Account ProviderAccountQueried = null;
    
    static NU__MembershipType__c JointStateProviderMT = null;
    static NU__Membership__c JointStateProviderMembership = null;
    
    static Joint_Billing__c JB = null;
    
    private static void loadTestData(){
        StatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
        ProviderAccount = DataFactoryAccountExt.insertProviderAccount(500000);
        ProviderAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);

        NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, statePartner.Id, true);
        
        JointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        JointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
        
        JB = DataFactoryJointBilling.insertJointBillingWithItemAndItemLine(statePartner.Id, providerAccount.Id, jointStateProviderMembership.NU__EndDate__c, providerAccountQueried.Dues_Price__c);
    }

    private static Joint_Billing__c getJointBillingById(Id jointBillingId){
        return [select id,
                       name,
                       Amount__c,
                       Bill_Date__c,
                       Start_Date__c,
                       End_Date__c
                  from Joint_Billing__c
                 where id = :jointBillingId];
    }
    
    private static void assertJointBillingDuesPrice(Id jointBillingId, Decimal expectedDuesPrice){
        Joint_Billing__c jbQueried = getJointBillingById(jointBillingId);
        
        system.assertEquals(expectedDuesPrice, jbQueried.Amount__c);
    }
    
    private static void updateJointBilling(Account statePartnerToUpdate){
        ApexPages.Standardcontroller sc = new ApexPages.StandardController(statePartnerToUpdate);
        
        ManualJointBillingUpdaterController mjbuc = new ManualJointBillingUpdaterController(sc);
        
        mjbuc.updateStateProviderJointBilling();
    }

    static testMethod void noJointBillingsToUpdateTest() {
        Account localStatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
        
        updateJointBilling(localStatePartner);
        
        TestUtil.assertPageMessagesHasMessage(ManualJointBillingUpdaterController.NO_JOINT_BILLINGS_TO_UPDATE);
    }
}