/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCustomMembershipTypeTrigger {
	
	static NU__MembershipType__c membershipType;
	static NU__Membership__c membership;
	static Account individual;
	
	static final Integer testGracePeriod = 8;
	static final String testName = 'Membership Type Name Change';
	
	private static void setupTest() {
		individual = NU.DataFactoryAccount.insertIndividualAccount();
		membershipType = NU.DataFactoryMembershipType.insertDefaultMembershipType();
		membership = NU.DataFactoryMembership.insertMembership(individual.Id, membershipType.Id, Date.newInstance(Date.today().year(), 1, 1), Date.newInstance(Date.today().year(), 12, 31));
	}

    static testMethod void BasicPopulationTest() {
        setupTest();
        
        List<NU__MembershipType__c> membershipTypes = membershipTypesQuery();
        System.assert(membershipTypes != null && membershipTypes.size() == 1);
        NU__MembershipType__c testMembershipType = membershipTypes.get(0);
        
        List<NU__Membership__c> memberships = membershipsQuery();
        System.assert(memberships != null && memberships.size() == 1);
        NU__Membership__c testMembership = memberships.get(0);
        
        basicAsserts(testMembershipType, testMembership);
    }
    
    static testMethod void BasicUpdateTest() {
        setupTest();
        
        Test.startTest();
        membershipType.NU__GracePeriod__c = testGracePeriod;
        membershipType.Name = testName;
		update membershipType;
        Test.stopTest();
        
        List<NU__MembershipType__c> membershipTypes = membershipTypesQuery();
        System.assert(membershipTypes != null && membershipTypes.size() == 1);
        NU__MembershipType__c testMembershipType = membershipTypes.get(0);
        
        List<NU__Membership__c> memberships = membershipsQuery();
        System.assert(memberships != null && memberships.size() == 1);
        NU__Membership__c testMembership = memberships.get(0);
        
        basicAsserts(testMembershipType, testMembership);
    }
    
    static testMethod void MassUpdateTest() {
        setupTest();
        
        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < 250; i++) {
        	accounts.add(NU.DataFactoryAccount.CreateIndividualAccount());
        }
        insert accounts;
        
        List<NU__Membership__c> bulkMemberships = new List<NU__Membership__c>();
        for (Account account : accounts) {
        	bulkMemberships.add(NU.DataFactoryMembership.createMembership(account.Id, membershipType.Id, Date.newInstance(Date.today().year(), 1, 1), Date.newInstance(Date.today().year(), 12, 31)));
        }
        insert bulkMemberships;
        
        Test.startTest();
        MembershipTypeBatchUpdate.batchSize = 500;
        membershipType.NU__GracePeriod__c = testGracePeriod;
        membershipType.Name = testName;
		update membershipType;
        Test.stopTest();
        
        List<NU__MembershipType__c> membershipTypes = membershipTypesQuery();
        System.assert(membershipTypes != null && membershipTypes.size() == 1);
        NU__MembershipType__c testMembershipType = membershipTypes.get(0);
        
        List<NU__Membership__c> memberships = membershipsQuery();
        System.assert(memberships != null && memberships.size() > 0);
        
        for (NU__Membership__c testMembership : memberships) {
        	basicAsserts(testMembershipType, testMembership);
        }
    }
    
    private static void basicAsserts(NU__MembershipType__c testMembershipType, NU__Membership__c testMembership) {
    	System.assert(testMembership.MembershipTypeGracePeriod__c == testMembershipType.NU__GracePeriod__c);
        System.assert(testMembership.MembershipTypeName__c == testMembershipType.Name);
    }
    
    private static List<NU__MembershipType__c> membershipTypesQuery() {
    	return [SELECT Id, Name, NU__GracePeriod__c FROM NU__MembershipType__c WHERE Id = :membershipType.Id];
    }
    
    private static List<NU__Membership__c> membershipsQuery() {
    	return [SELECT Id, MembershipTypeGracePeriod__c, MembershipTypeName__c FROM NU__Membership__c WHERE NU__MembershipType__c = :membershipType.Id];
    }
}