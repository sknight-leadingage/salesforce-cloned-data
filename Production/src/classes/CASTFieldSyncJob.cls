global class CASTFieldSyncJob implements Schedulable, Database.Batchable<sObject> {
	static Date todaysDate = Date.Today();

	global static void scheduleAt12AM() {
		System.schedule('CAST FIELD SYNC', '0 0 0 * * ?', new CASTFieldSyncJob());
	}
	
	global void execute(SchedulableContext context) {
		Database.executeBatch(new CASTFieldSyncJob());
	}
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		String accountQuery = 'select id, NU__Membership__c, CAST_Member_Thru__c, CAST_Lapsed_On__c, NU__Membership__r.MembershipTypeProductName__c, NU__Membership__r.NU__EndDate__c, CAST_Silver_Premier_Member__c, CAST_Silver_Premier_Supporter__c, CAST_Gold_Partner__c, NU__LapsedOn__c, CAST_Membership__c, CAST_Membership__r.NU__EndDate__c, CAST_Membership__r.Lapsed_On__c, CAST_LeadingAge_Provider__c, Provider_Lapsed_On__c, Provider_Membership__r.NU__EndDate__c, CAST_IAHSA_Provider__c, IAHSA_Lapsed_On__c, CAST_University__c, IAHSA_Membership__r.NU__EndDate__c from Account';
		
		return Database.getQueryLocator(accountQuery);
	}
	
	global void execute(Database.BatchableContext context, List<Account> accounts) {
		Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
		
		for (Account acct : accounts){
			Date calculatedCASTLapsedOn = calculateCASTLapsedOn(acct);
			
			if (calculatedCASTLapsedOn != acct.CAST_Lapsed_On__c){
				acct.CAST_Lapsed_On__c = calculatedCASTLapsedOn;
				accountsToUpdate.put(acct.Id, acct);
			}
			
			Date calculatedCASTMemberThru = calculateCASTMemberThru(acct);
			
			if (calculatedCASTMemberThru != acct.CAST_Member_Thru__c){
				acct.CAST_Member_Thru__c = calculatedCASTMemberThru;
				accountsToUpdate.put(acct.Id, acct);
			}
		}
		
		if (accountsToUpdate.isEmpty() == false){
			update accountsToUpdate.values();
		}
	}
	
	global void finish(Database.BatchableContext context) {
		if (Test.isRunningTest() == false){
			BatchApexUtil.sendErrorEmailWhenJobFinishesWithErrors( context.getJobId() );
		}
	}
	
	private Date calculateCASTLapsedOn(Account acct){
		Date castLapsedOn = null;
		
		if (acct.NU__Membership__c != null &&
		    (acct.NU__Membership__r.MembershipTypeProductName__c == 'CAST Business Associate' ||
		     acct.NU__Membership__r.MembershipTypeProductName__c == 'CAST Supporter' ||
		     acct.CAST_Silver_Premier_Member__c == true ||
		     acct.CAST_Silver_Premier_Supporter__c == true ||
		     acct.CAST_Gold_Partner__c == true)){

			castLapsedOn = acct.NU__LapsedOn__c;
		}
		else if (acct.CAST_Membership__c != null){
			castLapsedOn = acct.CAST_Membership__r.Lapsed_On__c;
		}
		else if (acct.CAST_LeadingAge_Provider__c == true){
			castLapsedOn = acct.Provider_Lapsed_On__c;
		}
		else if (acct.CAST_IAHSA_Provider__c == true){
			castLapsedOn = acct.IAHSA_Lapsed_On__c;
		}
		
		return castLapsedOn;
	}
	
	private Date calculateCASTMemberThru(Account acct){
		Date castMemberThru = null;
		
		if (acct.CAST_Membership__r.NU__EndDate__c > todaysDate){
			castMemberThru = acct.CAST_Membership__r.NU__EndDate__c;
		}
		else if (acct.NU__Membership__r.NU__EndDate__c > todaysDate &&
			    (acct.NU__Membership__r.MembershipTypeProductName__c == 'CAST Business Associate' ||
			     acct.NU__Membership__r.MembershipTypeProductName__c == 'CAST Supporter' ||
			     (acct.NU__Membership__r.MembershipTypeProductName__c == 'Gold Partner' && acct.CAST_Gold_Partner__c ) ||
			     (acct.NU__Membership__r.MembershipTypeProductName__c == 'Silver Premier'
			       && (acct.CAST_Silver_Premier_Member__c || acct.CAST_Silver_Premier_Supporter__c)))){
			castMemberThru = acct.NU__Membership__r.NU__EndDate__c;
		}
		else if (acct.Provider_Membership__r.NU__EndDate__c > todaysDate &&
		         acct.CAST_LeadingAge_Provider__c){
		    castMemberThru = acct.Provider_Membership__r.NU__EndDate__c;
     	}
     	else if (acct.IAHSA_Membership__r.NU__EndDate__c > todaysDate &&
     	         acct.CAST_IAHSA_Provider__c){
     		castMemberThru = acct.IAHSA_Membership__r.NU__EndDate__c;
 	    }
		
		return castMemberThru;
	}
}