global with sharing class SponsorshipOrderItemTypeCartSubmitter implements NU.IOrderItemTypeCartSubmitter {
    Map<Id, Sponsorship__c> cartItemLineSponsorshipMap;
    Map<Sponsorship__c, NU__OrderItemLine__c> sponsorshipsToUpdateMap;
    
    global NU.OperationResult beforeOrderItemSaved(NU__CartItem__c cartItem, NU__OrderItem__c orderItem) {  
        if (cartItemLineSponsorshipMap == null) {
            cartItemLineSponsorshipMap = new Map<Id, Sponsorship__c>();
        }
        
        List<Id> ids = new List<Id>();
        for (NU__CartItemLine__c cartItemLine : cartItem.NU__CartItemLines__r) {
            ids.add(cartItemLine.Id);
        }
        List<NU__CartItemLine__c> sponsorshipCartItemLines = [SELECT Id,
            NU__OrderItemLine__r.Sponsorship__c,
            NU__Product__c,
            NU__Data__c
            FROM NU__CartItemLine__c
            WHERE Id IN :ids];
        
        //for (NU__CartItemLine__c cartItemLine : cartItem.NU__CartItemLines__r) {
        for (NU__CartItemLine__c cartItemLine : sponsorshipCartItemLines) {
            SponsorshipCartItemLineData d = null;
            if (cartItemLine.NU__Data__c != null) {
                d = (SponsorshipCartItemLineData)JSON.deserialize(cartItemLine.NU__Data__c, SponsorshipCartItemLineData.class);
            }
            Sponsorship__c sponsorship = createSponsorshipFromCartItemLine(cartItem, cartItemLine, d);
            cartItemLineSponsorshipMap.put(cartItemLine.Id, sponsorship);
        }
        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global NU.OperationResult afterOrderItemsSaved() {

        if (cartItemLineSponsorshipMap != null && cartItemLineSponsorshipMap.isEmpty() == false) {
            System.debug(cartItemLineSponsorshipMap.values());
            upsert cartItemLineSponsorshipMap.values();
        }

        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global void beforeOrderItemLineSaved(NU__CartItemLine__c cartItemLine, NU__OrderItemLine__c orderItemLineToSave) {
        if (sponsorshipsToUpdateMap == null) {
            sponsorshipsToUpdateMap = new Map<Sponsorship__c, NU__OrderItemLine__c>();
        }
        
        if (cartItemLineSponsorshipMap != null && cartItemLineSponsorshipMap.containsKey(cartItemLine.Id)){
            Sponsorship__c sponsorship = cartItemLineSponsorshipMap.get(cartItemLine.Id);
            orderItemLineToSave.Sponsorship__c = sponsorship.Id;
            sponsorshipsToUpdateMap.put(sponsorship, orderItemLineToSave);
        }
    }

    global NU.OperationResult afterOrderItemLinesSaved() {

        if (sponsorshipsToUpdateMap != null && !sponsorshipsToUpdateMap.isEmpty()) {
            for (Sponsorship__c sponsorship : sponsorshipsToUpdateMap.keySet()) {
                sponsorship.Order_Item_Line__c = sponsorshipsToUpdateMap.get(sponsorship).Id;
            }
            
            List<Sponsorship__c> sponsorshipsToUpdate = new List<Sponsorship__c>(sponsorshipsToUpdateMap.keySet());
            upsert sponsorshipsToUpdate;
        }
        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global void populateCartSubmissionResult(NU.CartSubmissionResult result) {
        
    }
    
    private Sponsorship__c createSponsorshipFromCartItemLine(NU__CartItem__c sponsorshipCI, NU__CartItemLine__c sponsorshipCIL, SponsorshipCartItemLineData d) {
        Id sponsorshipId = null;
        
        if (sponsorshipCIL.NU__OrderItemLine__c != null) {
            sponsorshipId = sponsorshipCIL.NU__OrderItemLine__r.Sponsorship__c;
        }
        
        Sponsorship__c sponsorship = null;
        if (d != null) {
            sponsorship = new Sponsorship__c(
                Id = sponsorshipId,
                Account__c = sponsorshipCI.NU__Customer__c,
                Product__c = sponsorshipCIL.NU__Product__c,
                Event__c = d.SponsorEvent
            );
        }
        else {
            sponsorship = new Sponsorship__c(
                Id = sponsorshipId,
                Account__c = sponsorshipCI.NU__Customer__c,
                Product__c = sponsorshipCIL.NU__Product__c
            );
        }
        
        return sponsorship;
    }
}