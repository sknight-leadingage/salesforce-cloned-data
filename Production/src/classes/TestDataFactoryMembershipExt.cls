/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the DataFactoryMembershipExt class logic.
 */
@isTest
private class TestDataFactoryMembershipExt {

    static testMethod void insertCurrentProviderMembershipTest() {
        Account providerAcct = DataFactoryAccountExt.insertProviderAccount();
        NU__Membership__c providerMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAcct.Id);
        
        system.assert(providerMembership != null, 'The provider membership is null.');
        system.assert(providerMembership.Id != null, 'The provider membership is not inserted.');
        system.assertEquals(providerAcct.Id, providerMembership.NU__Account__c, 'The provider membership\'s account is not the supplied account.');
        
        
        Account providerAcctQueried = AccountQuerier.getAccountById(providerAcct.Id);
        system.assertEquals(providerMembership.Id, providerAcctQueried.Provider_Membership__c);
    }
}