/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBedCountPricer {

    static NU__MembershipTypeProductLink__c findMembershipTypeProductLink(NU__MembershipType__c membershipType, NU__Product__c membershipProduct){
		for (NU__MembershipTypeProductLink__c mtpl : membershipType.NU__MembershipTypeProductLinks__r){
			if (mtpl.NU__Product__c == membershipProduct.Id){
				return mtpl;
			}
		}
		
		return null;
	}
	static Integer ThisYearCalculated(Decimal SelectedStateBillingQuarter){
	    Integer y = Date.today().year();
	    Integer m = Date.today().month();
	    if (SelectedStateBillingQuarter != null) {
	        Decimal SelectedStateBillingQuarterStartingMonth = ((SelectedStateBillingQuarter - 1) * 3) + 1;
	        
	        if (SelectedStateBillingQuarterStartingMonth > m)
	           y = y - 1;
	        return y;
	    }
	    return 1;
   }
   
    static Date calculateNextStartDateFromBillingQuarter(Decimal billingQuarter){
        Date todaysDate = Date.Today();
        
        Integer nextYear = ThisYearCalculated(billingQuarter) + 1;
        
        Date nextJanuary = Date.NewInstance(nextYear, 1, 1);
        Date nextStartDate = nextJanuary;
        
        if (billingQuarter == 2){
            nextStartDate = Date.newInstance(nextYear, 4, 1);
        }
        else if (billingQuarter == 3){
            nextStartDate = Date.newInstance(nextYear, 7, 1);
        }
        else if (billingQuarter == 4){
            nextStartDate = Date.newInstance(nextYear, 10, 1);
        }
        
        return nextStartDate;
    }
    
    static Date calculateNextEndDate(Date startDate, Integer termInMonths){
        return startDate.addMonths(termInMonths).addDays(-1);
    }
    static testMethod void TestingBedCountPricer() {
        // TO DO: implement unit test
        //NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
        //NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertProviderMembershipType(anyEntity);
        
        NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        
        Id EntId = providerMT.NU__Entity__c;
        NU__Entity__c anyEntity = new NU__Entity__c();
        anyEntity = [SELECT Id, Name FROM NU__Entity__c WHERE Id = :EntId];
        anyEntity.Name = 'LeadingAge';
        anyEntity.NU__Status__c = 'Active';
        anyEntity.FP_Assisted_Living_Beds__c = 100;
        anyEntity.FP_Independent_Living_Units__c = 100;
        anyEntity.FP_Intermedicate_Care_Beds__c = 100;
        anyEntity.FP_Nursing_Home_Beds__c = 100;
        anyEntity.FP_Personal_Care_Beds__c = 100;
        anyEntity.FP_Skilled_Nursing_Facility_Units__c = 100;
        anyEntity.FP_ICF_IID_Beds__c = 100;
        anyEntity.NP_Assisted_Living_Beds__c = 10;
        anyEntity.NP_Independent_Living_Units__c = 10;
        anyEntity.NP_Intermedicate_Care_Beds__c = 10;
        anyEntity.NP_Nursing_Home_Beds__c = 10;
        anyEntity.NP_Personal_Care_Beds__c = 10;
        anyEntity.NP_Skilled_Nursing_Facility_Units__c = 10;
        anyEntity.NP_ICF_IID_Beds__c = 10;
        update anyEntity;
        system.assertNotEquals(null, anyEntity.Name);
        
    	NU__GLAccount__c glAccount = NU.DataFactoryGLAccount.insertRevenueGLAccount(anyEntity.Id);
    	Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
    	providerAccount.NU__PrimaryEntity__c = anyEntity.Id;
    	providerAccount.Number_Of_Assisted_Living_Units__c = 1;			
        providerAccount.Number_of_Independent_Living_Units__c = 2;	
        providerAccount.Number_of_Intermedicate_Care_Beds__c = 3;	
        providerAccount.Number_Of_Nursing_Beds__c = 4;	
        providerAccount.Number_of_Personal_Care_Beds__c = 5;
        providerAccount.ICF_Beds__c = 6;
        providerAccount.Number_of_Skilled_Nursing_Facility_Beds__c = 7;	
    	update providerAccount;
    	
    	providerAccount = [select id,
                NU__Member__c,
                NU__MembershipType__c,
                IAHSA_Member__c,
                IAHSA_Membership__c,
                CAST_Member__c,
                Provider_Member__c,
                Provider_Lapsed_On__c,
                Provider_Member_Thru__c,
                Provider_Membership__c,
                Multi_Site_Dues_Price__c,
                Dues_Price__c,
                RecordType.Name,
                RecordTypeId,
                Revenue_Year_Submitted__c,
                Program_Service_Revenue__c,
                Under_Construction__c,
                For_Profit__c,										//Bedcount for the account and for-profit or not
                Number_Of_Assisted_Living_Units__c,					
                Number_of_Independent_Living_Units__c,
                Number_of_Intermedicate_Care_Beds__c,
                Independent_Living_Beds__c,
                Number_of_Skilled_Nursing_Facility_Beds__c,
                Number_Of_Nursing_Beds__c,
                Number_of_Personal_Care_Beds__c,
                ICF_Beds__c,
                NU__PrimaryEntity__r.Id,            				//Entity id, name and bedcount prices
                NU__PrimaryEntity__r.Name,
                NU__PrimaryEntity__r.FP_Assisted_Living_Beds__c,
                NU__PrimaryEntity__r.FP_Independent_Living_Units__c,
                NU__PrimaryEntity__r.FP_Intermedicate_Care_Beds__c,
                NU__PrimaryEntity__r.FP_Nursing_Home_Beds__c,
                NU__PrimaryEntity__r.FP_Personal_Care_Beds__c,
                NU__PrimaryEntity__r.FP_Skilled_Nursing_Facility_Units__c,
                NU__PrimaryEntity__r.FP_ICF_IID_Beds__c,
                NU__PrimaryEntity__r.NP_Assisted_Living_Beds__c,
                NU__PrimaryEntity__r.NP_Independent_Living_Units__c,
                NU__PrimaryEntity__r.NP_Intermedicate_Care_Beds__c,
                NU__PrimaryEntity__r.NP_Nursing_Home_Beds__c,
                NU__PrimaryEntity__r.NP_Personal_Care_Beds__c,
                NU__PrimaryEntity__r.NP_Skilled_Nursing_Facility_Units__c,
                NU__PrimaryEntity__r.NP_ICF_IID_Beds__c				
				
           from Account
          where id = :providerAccount.Id]; 
    	 
    	CustomPricingManager cpm = new CustomPricingManager();
    	
        NU__MembershipType__c JointStateProviderMembershipType;
        Id JointStateProviderProductId;
        JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
        
        if (JointStateProviderMembershipType != null){
            JointStateProviderProductId = JointBillingUtil.getJointStateProviderProductId(JointStateProviderMembershipType);
        }
        
        NU__Product__c productToPrice = [select id,
                     Name,
                     RecordType.Name,
                     NU__Entity__c,
                     NU__Entity__r.Name,
                     NU__Event__c,
                     NU__Event__r.Name,
                     NU__Event__r.NU__ShortName__c,
                     NU__EventSessionEndDate__c,
                     NU__EventSessionGroup__c,
                     NU__EventSessionGroup__r.Name,
                     NU__EventSessionSpecialVenueInstructions__c,
                     NU__EventSessionStartDate__c,
                     NU__IsEventBadge__c,
                     NU__IsFee__c,
                     NU__IsShippable__c,
                     NU__IsTaxable__c,
                     NU__ListPrice__c,
                     NU__ShortName__c,
                     NU__QuantityMax__c,
                     NU__Status__c,
                     (select NU__PriceClasses__c,
                             NU__DefaultPrice__c,
                             NU__EarlyPrice__c,
                             NU__LatePrice__c,
                             Name
                        from NU__SpecialPrices__r
                     )
                from NU__Product__c
                where id = :JointStateProviderProductId]; 
        
        NU__MembershipTypeProductLink__c mtpl = findMembershipTypeProductLink(JointStateProviderMembershipType, productToPrice);
        mtpl.State_Dues__c = true;
        update mtpl;
    	
    	List<NU.ProductPricingInfo> productPricingInfos = new List<NU.ProductPricingInfo>();
        NU.ProductPricingInfo ppi = new NU.ProductPricingInfo();
        ppi.ProductId = jointStateProviderProductId;
        ppi.Quantity = 1;
        Date startDate = calculateNextStartDateFromBillingQuarter(1.0);
        ppi.StartDate = startDate;
        ppi.EndDate = calculateNextEndDate(startDate, (Integer) JointStateProviderMembershipType.NU__Term__c);
        ppi.JoinDate = system.today();
        productPricingInfos.add(ppi);

    	NU.PriceClassRequest pcr = new NU.PriceClassRequest();
        pcr.AccountId = providerAccount.Id;
        pcr.MembershipTypeId = providerMT.Id;
    	
    	NU.ProductPricingRequest productPricingRequest = new NU.ProductPricingRequest();
    	productPricingRequest.AccountId = providerAccount.Id;
        productPricingRequest.TransactionDate = system.today();
        productPricingRequest.ProductPricingInfos = productPricingInfos;
        productPricingRequest.PriceClassName = cpm.getPriceClass(pcr);
        productPricingRequest.MembershipTypeId = providerMT.Id;
        
        //productToPrice.NU__Entity__c.Id = providerAccount.customer.NU__PrimaryEntity__r.id;
        system.debug('   Entity of customer:'+ providerAccount.NU__PrimaryEntity__r.Name);
        system.debug('   Entity of productToPrice:'+ productToPrice.NU__Entity__r.Name);
        //insert productPricingRequest;
    	//productPricingRequest = null;
    	decimal result = 0;
    	
    	//NU__Product__c productToPrice = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_MEMBERSHIP, anyEntity.Id, glAccount.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_MEMBERSHIP);
    	NU__SpecialPrice__c specialDefaultProductPrice = NU.DataFactorySpecialPrice.insertSpecialPrice(productToPrice.Id, NU.Constant.PRICE_CLASS_DEFAULT, 100);
    	
        Map<String, object> extraPricingParams = new Map<String, Object>{ 
            'MembershipType' => JointStateProviderMembershipType
        };
        
        BedCountPricer Cal0 = new BedCountPricer();
        
        // None-Profit
        productToPrice.Name = 'Assisted Living Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 10);
    	
    	productToPrice.Name = 'Independent Living Unit';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 20);
    	
    	productToPrice.Name = 'Intermediate Care Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 30);
    	
    	productToPrice.Name = 'Nursing Home Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 40);
    	
    	productToPrice.Name = 'Personal Care Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 50);
    	
    	productToPrice.Name = 'ICF/IID Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 60);
    	
    	productToPrice.Name = 'Skilled Nursing Facility Beds';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 70);
    	
    	providerAccount.For_Profit__c = true;
    	update providerAccount;
    	//For-Profit
    	productToPrice.Name = 'Assisted Living Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 100);
    	
    	productToPrice.Name = 'Independent Living Unit';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 200);
    	
    	productToPrice.Name = 'Intermediate Care Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 300);
    	
    	productToPrice.Name = 'Nursing Home Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 400);
    	
    	productToPrice.Name = 'Personal Care Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 500);
    	
    	productToPrice.Name = 'ICF/IID Bed';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 600);
    	
    	productToPrice.Name = 'Skilled Nursing Facility Beds';
        update productToPrice;
    	result = Cal0.calculateProductPrice(productToPrice, providerAccount, specialDefaultProductPrice, productPricingRequest, extraPricingParams);
    	system.assertEquals(result, 700);
    }
}