/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestJointBillingUpdater {
    
    static Account StatePartner = null;
    static Account ProviderAccount = null;
    static Account ProviderAccountQueried = null;
    
    static NU__MembershipType__c JointStateProviderMT = null;
    static NU__Membership__c JointStateProviderMembership = null;
    
    static Joint_Billing__c JB = null;
    static Joint_Billing__c JBQueried = null;
    static Date JoinDateToUse = null;
    
    private static void loadTestData(){
    	if (JoinDateToUse == null){
    		JoinDateToUse = Date.valueOf(String.valueOf(Date.Today().year()) + '-01-01');
    	}
    	
        StatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
        ProviderAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, StatePartner.Id, JoinDateToUse);
        ProviderAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);
        
        JointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        JointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
        
        JB = DataFactoryJointBilling.insertJointBillingWithItemAndItemLine(statePartner.Id, providerAccount.Id, jointStateProviderMembership.NU__EndDate__c, providerAccountQueried.Dues_Price__c);
        JBQueried = getJointBillingById(JB.Id);
    }
    
    private static void updateProviderAccountProviderJoinOn(Date providerJoinOn){
    	ProviderAccount.Provider_Join_On__c = providerJoinOn;
    	update ProviderAccount;
    }
    
    private static Joint_Billing__c getJointBillingById(Id jointBillingId){
        return [select id,
                       name,
                       Amount__c,
                       Bill_Date__c,
                       Start_Date__c,
                       End_Date__c
                  from Joint_Billing__c
                 where id = :jointBillingId];
    }
    
    private static Joint_Billing_Item_Line__c getFirstAdjustmentJointBillingItemLine(Id jointBillingId, Id providerId){
        return
        [select id,
                name,
                Amount__c,
                Dues_price__c,
                Program_Service_Revenue__c,
                Revenue_Year_Submitted__c
           from Joint_Billing_Item_Line__c
          where Joint_Billing_Item__r.Joint_Billing__c = :jointBillingId
            and Joint_Billing_Item__r.Account__c = :providerId
            and Type__c = :Constant.JOINT_BILLING_ITEM_LINE_TYPE_ADJUSTMENT
          order by createddate
          limit 1];
    }
    
    private static void updateJointBilling(){
        Test.startTest();
        
        new JointBillingUpdater().execute(null);
        
        Test.stopTest();
    }
    
    private static void assertJointBillingDuesPrice(Id jointBillingId, Decimal expectedDuesPrice){
        Joint_Billing__c jbQueried1 = getJointBillingById(jointBillingId);
        
       	system.assertEquals(expectedDuesPrice, jbQueried1.Amount__c);
    }
    
    private static void assertNewProviderProratedPricing(Integer numOfDaysAfterStartDateForJoinDate, Decimal expectedDiscountAmount, Integer numOfDaysAfterStartDateForManualJoinDate){
    	loadTestData();
    	
    	Date providerJoinOn = null;
    	
    	if (numOfDaysAfterStartDateForJoinDate != null){
    		providerJoinOn = JBQueried.Start_Date__c.addDays(numOfDaysAfterStartDateForJoinDate);
    	}
    	
    	if (numOfDaysAfterStartDateForManualJoinDate != null){
    		JointBillingUtil.ProviderJoinOnToUseForTesting = JBQueried.Start_Date__c.addDays(numOfDaysAfterStartDateForManualJoinDate);
    	}
    	
    	Account providerAccount2 = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(providerAccount.Program_Service_Revenue__c + 50000000, statePartner.Id, providerJoinOn);
        
        updateJointBilling();
		
		Account providerAccount2Queried = AccountQuerier.getAccountById(providerAccount2.Id);
        Decimal expectedDuesPrice = providerAccount2Queried.Dues_Price__c * expectedDiscountAmount;
        
        assertJointBillingDuesPrice(jb.Id, expectedDuesPrice + providerAccountQueried.Dues_Price__c);
        
        Joint_Billing_Item_Line__c jbil = 
        [select id,
        		Amount__c,
                name,
                Dues_price__c,
                Program_Service_Revenue__c,
                Revenue_Year_Submitted__c
           from Joint_Billing_Item_Line__c
          where Joint_Billing_Item__r.Joint_Billing__c = :jb.Id
            and Joint_Billing_Item__r.Account__c = :providerAccount2Queried.Id];
          
        system.assertEquals(providerAccount2Queried.Dues_Price__c, jbil.Dues_Price__c, 'The joint billing item line\'s Dues Price copied amount differs from the new provider\'s dues price.');
        system.assertEquals(expectedDuesPrice, jbil.Amount__c, 'The joint billing item line\'s actual dues amount does not match the expected prorated dues amount.');
        system.assertEquals(providerAccount2Queried.Program_Service_Revenue__c, jbil.Program_Service_Revenue__c);
        system.assertEquals(providerAccount2Queried.Revenue_Year_Submitted__c, jbil.Revenue_Year_Submitted__c);
        
        NU__Membership__c provider2Membership =
        [select id,
                NU__Amount__c,
                NU__StartDate__c,
                NU__EndDate__c
           from NU__Membership__c
          where NU__Account__c = :providerAccount2.Id];
          
        system.assertEquals(expectedDuesPrice, provider2Membership.NU__Amount__c, 'The new provider\'s membership amount does not equal the expected amount.');
    }

    static testmethod void cancelledDuesTest(){
        loadTestData();
        
        providerAccount.Provider_Lapsed_On__c = Date.Today();
        
        update providerAccount;
        
        updateJointBilling();
        
        assertJointBillingDuesPrice(jb.Id, 0);
    }
    
    static testmethod void providerAddedLaterDuesTest(){
        loadTestData();
        
        Account providerAccount2 = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(50000000, statePartner.Id, Date.Today());
        providerAccount2.Revenue_Year_Submitted__c = String.valueOf(Date.Today().year());
        update providerAccount2; 
        
        updateJointBilling();
        
        Account providerAccount2Queried = AccountQuerier.getAccountById(providerAccount2.Id);
        assertJointBillingDuesPrice(jb.Id, providerAccount2Queried.Dues_Price__c + providerAccountQueried.Dues_Price__c);
        
        Joint_Billing_Item_Line__c jbil = 
        [select id,
                name,
                Dues_price__c,
                Program_Service_Revenue__c,
                Revenue_Year_Submitted__c
           from Joint_Billing_Item_Line__c
          where Joint_Billing_Item__r.Joint_Billing__c = :jb.Id
            and Joint_Billing_Item__r.Account__c = :providerAccount2Queried.Id];
          
        system.assertEquals(providerAccount2Queried.Dues_Price__c, jbil.Dues_Price__c);
        system.assertEquals(providerAccount2Queried.Program_Service_Revenue__c, jbil.Program_Service_Revenue__c);
        system.assertEquals(providerAccount2Queried.Revenue_Year_Submitted__c, jbil.Revenue_Year_Submitted__c);
    }
/*    
    static testmethod void duesPriceOverrideIncreaseAdjustmentTest(){
        loadTestData();

        Decimal increaseAmount = 10;
        ProviderAccountQueried.Dues_Price_Override__c = ProviderAccountQueried.Dues_Price__c + increaseAmount;
        update ProviderAccountQueried;
        
        updateJointBilling();
        
        Joint_Billing_Item_Line__c adjustmentJBIL = getFirstAdjustmentJointBillingItemLine(jb.Id, ProviderAccountQueried.Id);
            
        system.assertEquals(increaseAmount, adjustmentJBIL.Amount__c);
        assertJointBillingDuesPrice(jb.Id, ProviderAccountQueried.Dues_Price_Override__c);
    }
    
    static testmethod void duesPriceOverrideDecreaseAdjustmentTest(){
        loadTestData();

        Decimal increaseAmount = -10;
        ProviderAccountQueried.Dues_Price_Override__c = ProviderAccountQueried.Dues_Price__c + increaseAmount;
        update ProviderAccountQueried;
        
        updateJointBilling();
        
        Joint_Billing_Item_Line__c adjustmentJBIL = getFirstAdjustmentJointBillingItemLine(jb.Id, ProviderAccountQueried.Id);
            
        system.assertEquals(increaseAmount, adjustmentJBIL.Amount__c);
        assertJointBillingDuesPrice(jb.Id, ProviderAccountQueried.Dues_Price_Override__c);
    }
    
    static testmethod void nonCorporateMSONotAddedTest(){
        loadTestData();
        
        Account nonCorporateMSO = DataFactoryAccountExt.insertMultiSiteAccount(1000000, StatePartner.Id);
        
        updateJointBilling();
        
        Joint_Billing__c updatedJB = getJointBillingById(JB.Id);
        
        system.assertEquals(JBQueried.Amount__c, updatedJB.Amount__c);
    }

    static testmethod void corporateMSOJointBilledTest(){
        loadTestData();
        
        Account corporateMSO = DataFactoryAccountExt.insertCorporateMultiSiteAccount(1000000, StatePartner.Id);
        
        List<Account> multiSiteEnabledProviders = DataFactoryAccountExt.insertMultiSiteEnabledProviderAccounts(new List<Decimal>{ 1000000 }, corporateMSO.Id);
        
        updateJointBilling();
    }
    
    static testmethod void providerJoinsInFirstHalfOfYearProratedPricingTest(){
    	assertNewProviderProratedPricing(20, ProviderMembershipPricer.NEW_MEMBER_JOIN_DISCOUNT, null);
    }
    
    static testmethod void providerJoinsInSecondHalfOfYearProratedPricingTest(){
    	assertNewProviderProratedPricing(200, ProviderMembershipPricer.NEW_MEMBER_JOIN_SECOND_HALF_OF_YEAR_PRICE, null);
    }
    
    static testmethod void providerJoinsInFirstHalfOfYearWithoutProviderJoinOnSetProratedPricingTest(){
    	assertNewProviderProratedPricing(null, ProviderMembershipPricer.NEW_MEMBER_JOIN_DISCOUNT, 20);
    }
    
    static testmethod void providerJoinsInSecondHalfOfYearWithoutProviderJoinOnSetProratedPricingTest(){
    	assertNewProviderProratedPricing(null, ProviderMembershipPricer.NEW_MEMBER_JOIN_SECOND_HALF_OF_YEAR_PRICE, 200);
    }*/
}