public with sharing class SponsorshipCartItemLineData {
    public Id SponsorEvent { get; set; }
    public String SponsorBenefits { get; set; }
    public Decimal CompEventRegs { get; set; }
    
    public SponsorshipCartItemLineData(Id evt, String benefits, Decimal eventRegs) {
        SponsorEvent = evt;
        SponsorBenefits = benefits;
        CompEventRegs = eventRegs;
    }
}