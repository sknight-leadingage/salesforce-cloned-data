public with sharing class IAHSAMembershipPricer extends MembershipPricerBase implements IProductPricer {
	public static final Decimal SECOND_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT = 0.75;
	public static final Decimal THIRD_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT = 0.50;
	
	// Used to test the prorating rules
	public static Date testingJoinDate = null;
	
	public Decimal calculateProductPrice(NU__Product__c productToPrice, Account customer, NU__SpecialPrice__c specialPrice, NU.ProductPricingRequest productPricingRequest, Map<String, Object> extraParams){
		if (productPricingRequest.MembershipTypeId == null){
			return null;
		}
		
		NU__MembershipType__c membershipType = (NU__MembershipType__c) extraParams.get('MembershipType');
		
		if (isPrimaryMembershipProductOfMembership(membershipType, productToPrice, Constant.IAHSA_MEMBERSHIP_TYPE_NAME) == false){
		    return null;
		}
		
		Decimal iahsaMembershipPrice = new DefaultProductPricer().calculateProductPrice(productToPrice, customer, specialPrice, productPricingRequest, extraParams);
		
		// Prorate rules apply for new joins
		if(customer.IAHSA_Membership__c == null)
		{
			NU.ProductPricingInfo ppi = findMembershipProductPricingInfo(productToPrice.Id, productPricingRequest);
			
			Date joinDate = testingJoinDate != null ? testingJoinDate : Date.Today();
			
			Date startDate = ppi.StartDate;
			
			if (isJoiningInSecondQuarter(startDate, joinDate)){
				iahsaMembershipPrice *= SECOND_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT;
			}
			else if (isJoiningInThirdQuarter(startDate, joinDate)){
				iahsaMembershipPrice *= THIRD_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT;
			}
		}
		
		return iahsaMembershipPrice;
	}
	
	private Boolean isJoiningInSecondQuarter(Date startDate, Date joinDate){
		return startDate.addMonths(3) <= joinDate && joinDate < startDate.addMonths(6);
	}
	
	private Boolean isJoiningInThirdQuarter(Date startDate, Date joinDate){
		return startDate.addMonths(6) <= joinDate && joinDate < startDate.addMonths(9);
	}
}