global class EktronProviderSoapApi {
   //static String LeadingAgeEntityId = 'a0WJ0000000QXbsMAG'; // devnu
   //static String IahsaEntityId = 'a0WJ0000000QXbxMAG'; // devnu
   static String LeadingAgeEntityId = 'a0Wd0000001eYKHEA2'; // staging
   static String IahsaEntityId = 'a0WJ0000000QXc2MAG';  // staging
   
   //Use entity when determining access if using two ektron databases
   webService static Boolean CheckGroupAccess(String accountId, Long groupId, String entityId) {
        
        Boolean hasAccess = false;
        List<Account> accts = [SELECT FirstName, LastName FROM Account WHERE Id = :accountId];
        
        if (accts.size() > 0)
        {
             Account acct = accts[0];
            //Add account to all groups
            //if(acct.FirstName == 'Mendel' && acct.LastName == 'Guillaume')
            if(acct != null)
            {
                hasAccess = true;
            }
        }
        
        return hasAccess;
    }
        
    //Use entity when determining access if using two ektron databases
    webService static Boolean CheckCommunityGroupAccess(String accountId, Long groupId, String entityId) {
        Boolean hasAccess = false;
        List<Account> accts = [SELECT FirstName, LastName FROM Account WHERE Id = :accountId];
        
        if (accts.size() > 0)
        {
             Account acct = accts[0];
            //Add account to all community groups
            if(acct.FirstName == 'Mendel' && acct.LastName == 'Guillaume')
            {
                hasAccess = true;
            }
        }
        return hasAccess;
    }
}