/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestInvoiceEstimatedAnnualDuesController {

    static InvoiceEstimatedAnnualDuesController controller = null;

    static testMethod void TestAsNationalUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='LeadingAge Standard User'];
        User testUser = new User(Alias = 'newUser1', Email = System.now().millisecond() + 'newuser1@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName = DataFactoryUser.GetUsernamePrefix() + 'newuserfsfsd243gdfdgre1@testorg.com', CompanyName = 'LeadingAge');
            
        Test.startTest();   
        System.runAs(testUser) {
            controller = new InvoiceEstimatedAnnualDuesController();
            controller.getItems();
            controller.State_onChange();
        }
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error)); // no errors thrown
    }

    static testMethod void TestAsStateUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='State Standard User'];
        User testUser = new User(Alias = 'newUser2', Email = System.now().millisecond() + 'newuser2@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='America/Los_Angeles', UserName = DataFactoryUser.GetUsernamePrefix() + 'newusegsfd45543r2@testorg.com');

        Test.startTest();   
        System.runAs(testUser) {
            controller = new InvoiceEstimatedAnnualDuesController();
            controller.getItems();
            controller.State_onChange();
        }
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error)); // no errors thrown
    }
    
    static testMethod void TestAsWAUser() {
        Account statePartnerAcc = DataFactoryAccountExt.createStatePartnerAccount();
        statePartnerAcc.Name = 'LeadingAge Washington';
        statePartnerAcc.Dues_Billing_Quarter__c = 1;
        insert statePartnerAcc;
        
        User testUser = DataFactoryUser.insertStatePartnerPortalUser(statePartnerAcc.Id, 'LeadingAge Washington');

        Test.startTest();   
        System.runAs(testUser) {
            controller = new InvoiceEstimatedAnnualDuesController();
            controller.getItems();
            controller.SelectedState = statePartnerAcc.Id;
            controller.State_onChange();
        }
        Test.stopTest();
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error)); // no errors thrown
    }
}