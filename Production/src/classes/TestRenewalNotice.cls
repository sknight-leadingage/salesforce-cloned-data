@isTest
private class TestRenewalNotice {
	static NU__Entity__c entity = null;
	static NU__MembershipType__c membershipType = null;
	
	static Account account = null;
	static Account primaryContact = null;
	static NU__Membership__c membership = null;
	
	static PageReference pageRef = null;
	static RenewalNoticeController controller = null;
	
	private static void setupTest() {
		entity = NU.DataFactoryEntity.insertEntity();

		membershipType = DataFactoryMembershipTypeExt.insertProviderMembershipType(entity.Id);
		
		NU__Product__c product = NU.DataFactoryProduct.createMembershipProduct('Test Product', 1000.0, entity.Id);
		insert product;
		NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(membershipType.Id, product.Id);
		
		account = DataFactoryAccountExt.insertProviderAccount();
		
		membership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(account.Id, membershipType.Id);
        insert membership;
		
		pageRef = new PageReference('/apex/RenewalNotice');
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('id',account.Id);
	}
	
	static testMethod void NothingPassedIn_Test() {
		setupTest();
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>()));
        
        basicAsserts(0, 1);
	}
	
	static testMethod void AccountPassedIn_NoSelection_Test() {
		setupTest();
        
        Test.startTest();
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(0, 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Error)); // no membership renewal selection
	}
	
	static testMethod void AccountPassedIn_WithSelection_Test() {
		setupTest();
		delete membership;
        
        Test.startTest();
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        controller.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(0, 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); // no memberships found
	}
	
	static testMethod void AccountPassedIn_WithSelection_ExistingMembership_Test() {
		setupTest();
        
        Test.startTest();
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        controller.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(1, 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); // no role selected
	}
	
	// removed grace period check on 2/19
	/*static testMethod void AccountPassedIn_WithSelection_OldMembership_Test() {
		setupTest();
        
        Test.startTest();
        membership.NU__StartDate__c = membership.NU__StartDate__c.addYears(-2);
        membership.NU__EndDate__c = membership.NU__EndDate__c.addYears(-2);
        update membership;
        
        primaryContact = NU.DataFactoryAccount.insertIndividualAccount();
        primaryContact.PersonEmail = 'bwatson@nimbleuser.com';
        update primaryContact; 
        
        NU__Affiliation__c affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(0, 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); // no valid memberships
	}*/
	
	static testMethod void AccountPassedIn_WithSelection_NoPrimaryContact_Test() {
		setupTest();
        
        Test.startTest();  
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));      
        setBasicSelections();
        
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(1, 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); //no primary contact
	}
	
	static testMethod void AccountPassedIn_WithSelection_PrimaryContact_Test() {
		setupTest();
        
        Test.startTest();
        primaryContact = NU.DataFactoryAccount.insertIndividualAccount();
        NU__Affiliation__c affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(1, 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); //no e-mail address
	}
	
	static testMethod void AccountPassedIn_WithSelection_PrimaryContact_NoMessages_Test() {
		setupTest();
        
        Test.startTest();
        primaryContact = NU.DataFactoryAccount.insertIndividualAccount();
        primaryContact.PersonEmail = 'bwatson@nimbleuser.com';
        update primaryContact; 
        
        NU__Affiliation__c affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(1, 1);
        System.assert(!ApexPages.hasMessages()); //no messages!
	}
	
	static testMethod void AccountPassedIn_WithSelection_MoreThanOnePrimaryContact_Test() {
		setupTest();
        
        Test.startTest();
        primaryContact = NU.DataFactoryAccount.insertIndividualAccount();
        primaryContact.PersonEmail = 'bwatson@nimbleuser.com';
        update primaryContact; 
        
        NU__Affiliation__c affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        NU__Affiliation__c affiliation2 = NU.DataFactoryAffiliation.createAffiliation(NU.DataFactoryAccount.insertIndividualAccount().Id, account.Id, true);
        affiliation2.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation2;
        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        controller.Generate();
        Test.stopTest();
        
        basicAsserts(1, 1);
        System.assert(controller.Accounts[0].NU__Affiliates__r.size() > 1);
        System.assert(ApexPages.hasMessages(ApexPages.Severity.Warning)); //more than one primary contact found
	}
	
	static testMethod void AccountPassedIn_Print_Test() {
		setupTest();
		
		List<Task> tasks = [SELECT Id FROM Task WHERE WhatId = :account.Id];
		System.assert(tasks.size() == 0);
        
        Test.startTest();        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        controller.Generate();
        controller.Print();
        Test.stopTest();
        
        tasks = [SELECT Id FROM Task WHERE WhatId = :account.Id];
        System.assert(tasks.size() > 0);
	}
	
	static testMethod void AccountPassedIn_Bill_Test() {
		setupTest();
		
		List<MembershipBilling__c> memberBillings = [SELECT Id FROM MembershipBilling__c];
		System.assert(memberBillings.size() == 0);
		
		List<MembershipBillingLine__c> memberBillingLines = [SELECT Id FROM MembershipBillingLine__c];
		System.assert(memberBillingLines.size() == 0);
        
        Test.startTest();        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        ViewRenewalNoticeController viewController = new ViewRenewalNoticeController();
        viewController.accountId = account.Id;
        controller.Generate();
        controller.Bill();
        Test.stopTest();
        
        memberBillings = [SELECT Id, Year__c, MembershipType__c FROM MembershipBilling__c];
		System.assert(memberBillings.size() == 1);
		MembershipBilling__c memberBilling = memberBillings.get(0);
		
		System.assert(memberBilling.Year__c == String.valueOf(Date.today().year()+1));
		System.assert(memberBilling.MembershipType__c == membershipType.Id);
		
		memberBillingLines = [SELECT Id, Account__c, AmountBilled__c, BillDate__c, MembershipBilling__c FROM MembershipBillingLine__c];
		System.assert(memberBillingLines.size() == 1);
		MembershipBillingLine__c memberBillingLine = memberBillingLines.get(0);
		
		System.assert(memberBillingLine.Account__c == account.Id);
		System.assert(memberBillingLine.AmountBilled__c == 1000.0);
		System.assert(memberBillingLine.BillDate__c == Date.today());
		System.assert(memberBillingLine.MembershipBilling__c == memberBilling.Id);
	}
	
	static testMethod void AccountPassedIn_WithSelection_PrimaryContact_Email_Test() {
		setupTest();
        
        Test.startTest();
        primaryContact = NU.DataFactoryAccount.insertIndividualAccount();
        primaryContact.PersonEmail = 'bwatson@nimbleuser.com';
        update primaryContact; 
        
        NU__Affiliation__c affiliation = NU.DataFactoryAffiliation.createAffiliation(primaryContact.Id, account.Id, true);
        affiliation.NU__Role__c = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        insert affiliation;
        
        controller = new RenewalNoticeController(new ApexPages.StandardSetController(new List<Account>{account}));
        setBasicSelections();
        controller.Generate();
        controller.Email();
        Test.stopTest();
        
        basicAsserts(1, 1);
        System.assert(!ApexPages.hasMessages());
	}
	
	private static void basicAsserts(Integer accountSize, Integer membershipOptionsSize) {
		System.assert(controller.Accounts.size() == accountSize);
        System.assert(RenewalNoticeUtil.Instance.MembershipOptions.size() == membershipOptionsSize);
        System.assert(RenewalNoticeUtil.Instance.RoleOptions.size() > 0);
	}
	
	private static void setBasicSelections() {
		controller.membershipRelationship = RenewalNoticeUtil.Instance.MembershipOptions.get(0).getValue();
        controller.affiliationRole = RenewalNoticeUtil.Instance.RoleOptions.get(0).getValue();
        controller.DummyOrder.NU__InvoiceDate__c = Date.today().addDays(-1);
        controller.showAllProducts = false;
	}
}