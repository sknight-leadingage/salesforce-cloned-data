/* 
* Test Code for Manage Account Status 
*/
@isTest
private class TestManageAccountWizard {
     static final integer WizardMax = 4; //Remember to change this if more steps are added
     
     static testmethod void CheckPicklistsAndBaseVars() {
        Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
	    NU__Affiliation__c affTestProvInd = new NU__Affiliation__c(NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
	    insert affTestProvInd;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',individualAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
        
		List<SelectOption> testOpts = mc.getaccStatuses();
		system.assertNotEquals(testOpts.size(), 0, 'Account Status picklist contains no values.');
		
		testOpts.clear();
		testOpts = mc.getaffEnableList();
		system.assertNotEquals(testOpts.size(), 0, 'Affiliations picklist contains no values.');
		
		system.assertEquals(mc.WizardMax != null && mc.WizardMax > 0, true, 'Wizard Max is zero or null.');
     }
     

     static testmethod void ChangeStatusActivetoInactiveTest(){
        Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
	    NU__Affiliation__c affTestProvInd = new NU__Affiliation__c(NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
	    insert affTestProvInd;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',individualAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(individualAccount, null, 'No User Record Found.');

        //Opening Step of Wizard
        system.assertEquals(1, mc.WizardStep);

        //Choose Yes
        mc.accStatus='Inactive';
        mc.WizardNext();
        system.assertEquals(2, mc.WizardStep);
        
        //Confirming to change the status
        individualAccount.NU__Status__c = 'Inactive';
        mc.WizardNext();
        
        //At the end of Wizard step
        system.assertEquals(3, mc.WizardStep);
        List<Account> tempAccount = [SELECT NU__Status__c FROM Account WHERE Id = :individualAccount.Id];
        
        for(Account acc: tempAccount){
            system.assertEquals('Inactive', acc.NU__Status__c);
        }
        
        List<NU__Affiliation__c> AffAccount = [SELECT NU__Status__c FROM NU__Affiliation__c WHERE NU__Account__c = :individualAccount.Id];
        
        for(NU__Affiliation__c aff: AffAccount){
            system.assertEquals('Inactive', aff.NU__Status__c);
        }
        
   }     
   static testmethod void ChangeStatusInactivetoActiveTest(){
        Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
        DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
	    NU__Affiliation__c affTestProvInd = new NU__Affiliation__c(NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
	    insert affTestProvInd;

        PageReference testPr = Page.ManageAccount;
        Test.setCurrentPage(testPr);
        testPr.getParameters().put('pv0',individualAccount.Id);

        ManageAccount_Controller mc = new ManageAccount_Controller();
       
        system.assertNotEquals(individualAccount, null, 'No User Record Found.');

        //Opening Step of Wizard
        system.assertEquals(1, mc.WizardStep);

        //Choose Yes
        mc.accStatus='Active';
        mc.WizardNext();
        system.assertEquals(2, mc.WizardStep);
        
        //Confirming to change the status
        individualAccount.NU__Status__c = 'Active';
        
		List<SelectOption> testOpts = mc.getaffEnableList();
		system.assertNotEquals(testOpts.size(), 0, 'Affiliations picklist contains no values.');
        
        mc.affEnable = testOpts[0].getValue();
        mc.WizardNext();
        
        //At the end of Wizard step
        system.assertEquals(3, mc.WizardStep);
        List<Account> tempAccount = [SELECT NU__Status__c FROM Account WHERE Id = :individualAccount.Id];
        
        for(Account acc: tempAccount){
            system.assertEquals('Active', acc.NU__Status__c);
        }
        
        List<NU__Affiliation__c> AffAccount = [SELECT NU__Status__c FROM NU__Affiliation__c WHERE NU__Account__c = :individualAccount.Id];
        
        for(NU__Affiliation__c aff: AffAccount){
            system.assertEquals('Active', aff.NU__Status__c);
        }
        
    }
}