trigger AccountTrigger_ProgramRevenue on Account (BEFORE UPDATE, AFTER INSERT) {
	NU.TriggerHandlerManager.executeHandlers('AccountTrigger_ProgramRevenue', new AccountTrigger_ProgramRevenueHandler());
}