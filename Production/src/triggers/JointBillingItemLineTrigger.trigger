trigger JointBillingItemLineTrigger on Joint_Billing_Item_Line__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	NU.TriggerHandlerManager.executeHandlers('JointBillingItemLineTrigger', new JointBillingItemLineTriggerHandlers());
}