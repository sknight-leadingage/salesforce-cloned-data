trigger SessionSpeakerTrigger on Conference_Session_Speaker__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	NU.TriggerHandlerManager.executeHandlers('SessionSpeakerTrigger', new SessionSpeakerTriggerHandlers());
}