trigger AffiliationTrigger on NU__Affiliation__c (before update, after update) {
	NU.TriggerHandlerManager.executeHandlers('AffiliationTrigger', new AffiliationTriggerHandlers());
}