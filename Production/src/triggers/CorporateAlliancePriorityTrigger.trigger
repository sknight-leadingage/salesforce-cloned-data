trigger CorporateAlliancePriorityTrigger on Sponsorship__c (after insert, after update) {
	NU.TriggerHandlerManager.executeHandlers('CorporateAlliancePriority_Trigger', new CorporateAlliancePriority_Trigger());
}