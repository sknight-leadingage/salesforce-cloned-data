trigger MembershipBillingsTrigger on MembershipBilling__c (before insert, before update) {
	// need to check if there are already records for the membership type and year that the inserted/updated records are assigned
	List<MembershipBilling__c> membershipBillings = [SELECT Id, MembershipType__c, Year__c FROM MembershipBilling__c];
	Map<String,MembershipBilling__c> membershipBillingsMap = new Map<String,MembershipBilling__c>();
	
	// assign one record to a given membership type and year
	for (MembershipBilling__c memberBilling : membershipBillings) {
		membershipBillingsMap.put(memberBilling.Year__c + '' + memberBilling.MembershipType__c, memberBilling);
	}
	
	// loop through the records that are being inserted/updated
	for (MembershipBilling__c memberBilling : Trigger.new) {
		String mapKey = memberBilling.Year__c + '' + memberBilling.MembershipType__c;
		
		// if a match is found, and it's not the current record, throw an error - cannot have more than one record assigned to a given membership type and year
		if (membershipBillingsMap.get(mapKey) != null && 
			(membershipBillingsMap.get(mapKey).Id != memberBilling.Id || memberBilling.Id == null)) {
			
			memberBilling.addError(MembershipBilling.DUPLICATE_MEMBERSHIP_BILLING_FOUND);
		}
		// add it to the map so that if other records are being inserted/updated in the same execution, this membership type and year cannot be assigned to them
		else {
			membershipBillingsMap.put(mapKey, memberBilling);
		}
	}
}