<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>IsCouponOrderItemLinePopulation</fullName>
        <field>IsCouponOrderItemLine__c</field>
        <literalValue>1</literalValue>
        <name>Is Coupon Order Item Line Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OrderItemLineIsShippablePopulation</fullName>
        <field>IsShippable__c</field>
        <literalValue>1</literalValue>
        <name>Order Item Line Is Shippable Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order Item Line Is Coupon Order Item Line Population</fullName>
        <actions>
            <name>IsCouponOrderItemLinePopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order Item Line Is Shippable Population</fullName>
        <actions>
            <name>OrderItemLineIsShippablePopulation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
