<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Membership__c</defaultLandingTab>
    <description>Manage your members and related areas.</description>
    <label>Memberships</label>
    <logo>NimbleAMS/LeadingAge_Logo.gif</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>Membership__c</tab>
    <tab>Subscription__c</tab>
    <tab>Product__c</tab>
    <tab>Deal__c</tab>
    <tab>MembershipBilling__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Idea</tab>
    <tab>Joint_Billing__c</tab>
    <tab>Joint_State_Provider_Billing</tab>
    <tab>Awards__c</tab>
</CustomApplication>
