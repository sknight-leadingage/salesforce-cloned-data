<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Nimble AMS Setup</description>
    <label>Nimble AMS Setup</label>
    <logo>NimbleAMS/LeadingAge_Logo.gif</logo>
    <tab>standard-Account</tab>
    <tab>Batch__c</tab>
    <tab>Committee__c</tab>
    <tab>CommitteePosition__c</tab>
    <tab>Event__c</tab>
    <tab>Product__c</tab>
    <tab>PriceClass__c</tab>
    <tab>MembershipType__c</tab>
    <tab>MembershipTypeProductLink__c</tab>
    <tab>Category__c</tab>
    <tab>Configuration__c</tab>
    <tab>Donation__c</tab>
    <tab>standard-report</tab>
    <tab>Awards__c</tab>
</CustomApplication>
