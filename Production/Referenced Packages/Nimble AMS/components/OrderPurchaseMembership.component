<apex:component controller="NU.OrderPurchaseMembership" allowDML="true">

    <apex:attribute name="c" type="NU.OrderController" description="OrderController" assignTo="{!Controller}"/>

    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd();NUBind()" id="ASBind"/>

    <apex:actionRegion >

    <apex:actionFunction name="startDateChanged" action="{!OnStartDateChanged}" rerender="Msgs,Main" status="ASBind">
        <apex:param name="firstParam" assignTo="{!SelectedDues}" value="" />
    </apex:actionFunction>

    <apex:actionFunction name="endDateChanged" action="{!OnEndDateChanged}" rerender="Msgs,Main" status="ASBind">
        <apex:param name="firstParam" assignTo="{!SelectedDues}" value="" />
    </apex:actionFunction>

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/hands32.png)"/>
                <h1 class="pageType">{!c.Subheader}&nbsp;</h1>
                <h2 class="pageDescription">{!IF(IsEDIT, 'Edit', 'Add')} Membership</h2>
            </div>
        </div>
    </div>

    <apex:pageMessages escape="{!c.EscapeMessages}" id="Msgs" />

    <apex:pageBlock mode="maindetail" Id="Main">

        <apex:pageBlockSection columns="2">
            <apex:pageBlockSectionItem >
                <apex:outputText value="{!$ObjectType.NU__CartItem__c.Fields.NU__Customer__c.Label}" />
                <apex:outputPanel layout="none">
                    <apex:inputField value="{!Controller.CurrentCartItem.NU__Customer__c}" rendered="{!!IsEdit}">
                        <apex:actionSupport event="onchange" action="{!OnAccountSelected}" rerender="Msgs,Main" status="ASBind"/>
                    </apex:inputField>
                    <apex:outputField value="{!Controller.CurrentCartItem.NU__Customer__c}" rendered="{!IsEdit}"/>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.CurrentCartItem.NU__Customer__c != null}">
                <apex:outputLabel />
                <input type="button" value="Current Membership Detail" class="btn" id="MembershipDetail"
                    onmouseout="if (this.nuWasClicked) {var membershipDetailHover = LookupHoverDetail.getHover('MembershipDetail'); membershipDetailHover.hide(); LookupHoverDetail.hovers['MembershipDetail']=null;  }"
                    onclick="this.nuWasClicked = true;LookupHoverDetail.getHover('MembershipDetail', '/apex/{!JSENCODE(MembershipDetailPage)}?id={!JSENCODE(Controller.CurrentCartItem.Customer__c)}&nocache={!JSENCODE(Ticks)}').showNow();"
                    style="{! If(IsNull(DummyAccount.Membership__c), 'display:none;', '' )}" />
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!Controller.CurrentCartItem.NU__Customer__c == null}">
                <apex:outputText />
                <apex:outputText />
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && !IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__Entity__c}">
                    <apex:selectOptions value="{!EntityOptions}"/>
                    <apex:actionSupport event="onchange" onsubmit="if(!confirmEntitySwitch()) { this.value = '{!Controller.CurrentCartItem.NU__Entity__c}'; return false; }" action="{!OnEntitySelected}" rerender="Msgs,Main" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && IsEdit}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__Entity__c.Label}" />
                <apex:outputField value="{!Controller.CurrentCartItem.NU__Entity__c}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!ShowEntitySelection && Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null}">
                <apex:outputText />
                <apex:outputText />
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null}">
                <apex:outputText value="{!$ObjectType.NU__Membership__c.Fields.NU__MembershipType2__c.Label}" />
                <apex:outputPanel layout="none">
                    <apex:selectList multiselect="false" size="1" value="{!Data.MembershipData.MembershipType}" rendered="{!!IsEdit}">
                        <apex:selectOption itemvalue="" itemLabel="Please select a membership type"/>
                        <apex:selectOptions value="{!MembershipTypeOptions}"/>
                        <apex:actionSupport event="onchange" action="{!OnMembershipTypeSelected}" rerender="Msgs,Main" status="ASBind"/>
                    </apex:selectList>
                    <apex:outputField value="{!DummyMembership.NU__MembershipType2__c}" rendered="{!IsEdit}" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null}">
                <apex:outputText />
                <apex:outputText />
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Data.MembershipData.MembershipType != null}">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__PriceClass__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!Controller.CurrentCartItem.NU__PriceClass__c}">
                    <apex:selectOption itemvalue="" itemLabel="Please select a price class" rendered="{!Controller.CurrentCartItem.NU__PriceClass__c == null}"/>
                    <apex:selectOptions value="{!PriceClassOptions}"/>
                    <apex:actionSupport event="onchange" action="{!OnPriceClassSelected}" rerender="Msgs,Main" status="ASBind"/>
                </apex:selectList>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Data.MembershipData.MembershipType != null}">
                <apex:outputText />
                <apex:outputText />
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Data.MembershipData.MembershipType != null && Controller.CurrentCartItem.NU__PriceClass__c != null}">
                <apex:outputLabel value="Stage"/>
                <apex:outputPanel layout="none">
                    <apex:selectList size="1" value="{!Filter}" rendered="{!!IsEdit}">
                        <apex:selectOption itemLabel="Join" itemValue="Join" />
                        <apex:selectOption itemLabel="Renew" itemValue="Renew" />
                        <apex:selectOption itemLabel="Both" itemValue="Both" />
                        <apex:actionSupport event="onchange" action="{!OnFilterSelected}" rerender="Msgs,Main" status="ASBind"/>
                    </apex:selectList>
                    <apex:outputText value="{!Filter}" rendered="{!IsEdit}" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Data.MembershipData.MembershipType != null && Controller.CurrentCartItem.NU__PriceClass__c != null}">
                <apex:outputText />
                <apex:outputText />
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.Entity2__c != null &&
                    Data.MembershipData.MembershipType != null &&
                    Controller.CurrentCartItem.PriceClass__c != null &&
                    (Filter != 'Renew' || ShowJoinOnForRenew)}"
                    datastyle="min-width: 40%; width: 40%;">
                <apex:outputText value="{!$ObjectType.Account.Fields.NU__JoinOn__c.Label}" />
                <c:Required >
                    <apex:inputField value="{!DummyMembership.NU__StartDate__c}" styleClass="hideDateLink joinOn">
                        <apex:actionSupport event="onchange" action="{!RefreshForJoinOnChange}" rerender="Msgs,Main" status="ASBind" />
                    </apex:inputField>
                    &nbsp;[ <a href="javascript:void(0)" onclick="FillJoinOn()">Copy from Start Date</a> ]
                </c:Required>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!Controller.Cart.Entity2__c != null &&
                    Data.MembershipData.MembershipType != null &&
                    Controller.CurrentCartItem.PriceClass__c != null &&
                    (Filter != 'Renew' || ShowJoinOnForRenew)}">
                <apex:outputText />
                <apex:outputText />
            </apex:pageBlockSectionItem>

            <apex:pageBlockSectionItem rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__PriceClass__c != null && IsAutoRenewMembershipTypeSelected}">
                <apex:outputLabel for="autoRenewCheckbox" value="Automatically Renew?" />
                <apex:inputCheckbox id="autoRenewCheckbox" value="{!Data.MembershipData.AutoRenew}"/>
            </apex:pageBlockSectionItem>

        </apex:pageBlockSection>

        <apex:pageBlockSection title="Membership Products" columns="1" rendered="{!Controller.Cart.NU__Entity2__c != null && Data.MembershipData.MembershipType != null && Controller.CurrentCartItem.NU__PriceClass__c != null}">
            <apex:outputText value="There are no membership products for this membership type and stage." rendered="{!duesProdsCount == 0}" />

            <apex:pageBlockTable value="{!DuesProducts}" id="table" var="oli" rendered="{! duesProdsCount > 0}">
                <apex:column >
                    <apex:facet name="header">Purchase</apex:facet>
                    <apex:inputField value="{!oli.NU__IsInCart__c}" styleClass="{!IF(oli.Id == null && oli.NU__OrderItemLine__c == null,'rrc','rrc rrs')}" rendered="{!!oli.Product2__r.NU__TrackInventory__c || oli.Product2__r.NU__InventoryOnHand__c > 0}"/>
                    <input type="radio" name="membership" class="rrr memsel" />
                </apex:column>
                <apex:column value="{!oli.NU__Product2__c}"/>
                <apex:column value="{!oli.MembershipTypeProductLink__r.NU__Stage__c}"/>
                <apex:column >
                    <apex:facet name="header">{!$ObjectType.NU__Membership__c.fields.NU__StartDate__c.label}</apex:facet>
                    <c:Required >
                        <apex:inputField value="{!EnrollmentDates[oli.NU__MembershipTypeProductLink__c].StartDate__c}"
                                         onchange="startDateChanged('{!oli.NU__MembershipTypeProductLink__c}');"
                                         styleClass="hideDateLink startDate" />
                       </c:Required>
                </apex:column>
                <apex:column >
                    <apex:facet name="header">{!$ObjectType.NU__Membership__c.fields.NU__EndDate__c.label}</apex:facet>
                    <c:Required >
                        <apex:inputField value="{!EnrollmentDates[oli.NU__MembershipTypeProductLink__c].EndDate__c}"
                                         onchange="endDateChanged('{!oli.NU__MembershipTypeProductLink__c}');"
                                         styleClass="hideDateLink" />
                    </c:Required>
                </apex:column>
                <apex:column styleClass="number" headerClass="number">
                    <apex:facet name="header">Price</apex:facet>
                    <div style="display: inline-block">
                        <c:Required >
                            <apex:inputField value="{!oli.NU__UnitPrice__c}" required="false" styleClass="inputPrice" onfocus="this.maxLength = {! UnitPriceMaxLength };" />
                        </c:Required>
                    </div>
                </apex:column>

            </apex:pageBlockTable>
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Other Products" columns="1" rendered="{!Controller.Cart.NU__Entity2__c != null && Data.MembershipData.MembershipType != null && Controller.CurrentCartItem.NU__PriceClass__c != null && ShowOtherProducts}">
            <apex:pageBlockTable value="{!OtherProducts}" id="table" var="oli">
                <apex:column styleClass="purCol">
                    <apex:facet name="header">Purchase</apex:facet>
                    <apex:inputField value="{!oli.NU__IsInCart__c}" styleClass="{!IF(oli.Id == '','selectableRow','unselectableRow')}"/>
                </apex:column>
                <apex:column value="{!oli.NU__Product2__c}"/>
                <apex:column value="{!oli.MembershipTypeProductLink__r.NU__Stage__c}"/>
                <apex:column styleClass="number" headerClass="number">
                    <apex:facet name="header">Price</apex:facet>
                    <div style="display: inline-block"><c:Required ><apex:inputField value="{!oli.NU__UnitPrice__c}" styleClass="inputPrice" required="false" onfocus="this.maxLength = {! UnitPriceMaxLength };" /></c:Required></div>
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlockSection>

        <apex:pageBlockButtons location="both">
            <apex:commandButton value="Save"
                                styleClass="green"
                                action="{!Save}"
                                rendered="{!Controller.Cart.NU__Entity2__c != null && Controller.CurrentCartItem.NU__Customer__c != null && Data.MembershipData.MembershipType != null && Controller.CurrentCartItem.NU__PriceClass__c != null && duesProdsCount > 0}"
                                rerender="Msgs,Main"
                                status="ASBind"/>
            <apex:commandButton value="Cancel" action="{!Cancel}" rerender="Msgs,Main" status="ASBind"/>
        </apex:pageBlockButtons>

        <script type="text/javascript">
            function FillJoinOn() {
                var radio = $(".startDate").closest("tr").find("input[type='radio']:checked");
                if (radio.length) {
                    var joinOn = $(".joinOn");
                    joinOn.val(radio.closest("tr").find(".startDate").val());
                    joinOn.trigger("change");
                } else {
                    alert("You must select a membership product first");
                }
            }
        </script>

    </apex:pageBlock>

    </apex:actionRegion>

</apex:component>