<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <customSettingsVisibility>Protected</customSettingsVisibility>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>AESEncryptionIV__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Length must be 16 characters</inlineHelpText>
        <label>AES Encryption IV</label>
        <length>16</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AESEncryptionKey__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Length must be 32 characters</inlineHelpText>
        <label>AES Encryption Key</label>
        <length>32</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AccountAddressesToFlowdown__c</fullName>
        <defaultValue>&quot;Billing,Shipping,Mailing,Other&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>Enter either &quot;Billing&quot;, &quot;Shipping&quot;, &quot;Mailing&quot;, &quot;Other&quot;, or some combination separated by commas to specify the account addresses that should flowdown to their primary affiliations. Mailing and Other will only flow down with Person Account Parents.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter either &quot;Billing&quot;, &quot;Shipping&quot;, &quot;Mailing&quot;, &quot;Other&quot;, or some combination separated by commas to specify the account addresses that should flowdown to their primary affiliations. Mailing and Other will only flow down with Person Account Parents.</inlineHelpText>
        <label>Account Addresses To Flowdown</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AffiliationPrintRosterFieldSet__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Override the field set used when printing the affiliation roster</inlineHelpText>
        <label>Affiliation Print Roster Field Set</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AffiliationViewRosterFieldSet__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Override the field set used when viewing the affiliation roster</inlineHelpText>
        <label>Affiliation View Roster Field Set</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BulkPricingImplementation__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The custom class that implements the IBulkPricingManager. If blank, the StandardBulkPricingManager is used.</inlineHelpText>
        <label>Bulk Pricing Implementation</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BulkSalesTaxDefaulter__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>A custom class that implements IBulkSalesTaxDefaulter to determine which sales tax to apply. If blank, BulkStateSalesTaxDefaulter is used.</inlineHelpText>
        <label>Bulk Sales Tax Defaulter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CanBackorderStaffView__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Allow merchandise products to still be purchased in Staff View if there is no available inventory.</description>
        <externalId>false</externalId>
        <inlineHelpText>Allow merchandise products to still be purchased in Staff View if there is no available inventory.</inlineHelpText>
        <label>zCan Backorder Staff View (DEPRECATED)</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>DefaultEntity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Default Entity</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DeferredRevenueRecognition__c</fullName>
        <defaultValue>&quot;Summarized&quot;</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Deferred Revenue Recognition</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DetailedEventPicklist__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Displays the event date and city for each event in the picklist in the order process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Displays the event date and city for each event in the picklist in the order process.</inlineHelpText>
        <label>Detailed Event Picklist</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EmailServiceFromAddress__c</fullName>
        <deprecated>false</deprecated>
        <description>The from address to use when sending emails through a third-party service</description>
        <externalId>false</externalId>
        <inlineHelpText>The from address to use when sending emails through a third-party service</inlineHelpText>
        <label>Email Service From Address</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EmailServiceFromName__c</fullName>
        <deprecated>false</deprecated>
        <description>The from display name to use when sending emails through a third-party service</description>
        <externalId>false</externalId>
        <inlineHelpText>The from display name to use when sending emails through a third-party service</inlineHelpText>
        <label>Email Service From Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EmailServicePassword__c</fullName>
        <deprecated>false</deprecated>
        <description>The password for a third-party email service</description>
        <externalId>false</externalId>
        <inlineHelpText>The password for a third-party email service</inlineHelpText>
        <label>Email Service Password</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EmailServiceUsername__c</fullName>
        <deprecated>false</deprecated>
        <description>The username for a third-party email service</description>
        <externalId>false</externalId>
        <inlineHelpText>The username for a third-party email service</inlineHelpText>
        <label>Email Service Username</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EmailService__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Send emails through Salesforce, or a third-party service such as SendGrid</inlineHelpText>
        <label>Email Service</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EnablePilotFeatures__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If enabled, pilot features will be able to be used in the org. This currently controls Cash Prepayments and Credit Refunds.</description>
        <externalId>false</externalId>
        <label>Enable Pilot Features</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EncryptionKey__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Encryption Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ErrorEmailAddresses__c</fullName>
        <defaultValue>&quot;dev@nimbleams.com&quot;</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Comma separated list of email addresses</inlineHelpText>
        <label>Error Email Addresses</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GeolocationAddress__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Geolocation Address</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GracePeriod__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Grace Period (Months)</label>
        <precision>2</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HelpURL__c</fullName>
        <defaultValue>&quot;http://help.nimbleams.com&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>Root host name for the site to use with &quot;Help for this page&quot; link for custom objects. For tabs with an embedded landing page, the link is encoded in the landing page source. In development, this setting might be changed to http://localhost:5000 or such.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter the Help site URL, such as http://help.nimbleams.com</inlineHelpText>
        <label>Help URL</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>InstallmentPaymentsRoundingMethod__c</fullName>
        <defaultValue>&quot;Half Up&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>If paying off an order in installments, this is the rounding calculation that is used to determine the installment amount. The difference will be adjusted on the last payment.</description>
        <externalId>false</externalId>
        <inlineHelpText>If paying off an order in installments, this is the rounding calculation that is used to determine the installment amount. The difference will be adjusted on the last payment.</inlineHelpText>
        <label>Installment Payments Rounding Method</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InvoiceEmailFromOrgWideId__c</fullName>
        <deprecated>false</deprecated>
        <description>The salesforce Id of the Organization-Wide email address to use as the &quot;From&quot; in emails. If this isn&apos;t set, the email address of the user sending the email is used instead.</description>
        <externalId>false</externalId>
        <inlineHelpText>The salesforce Id of the Organization-Wide email address to use as the &quot;From&quot; in Invoice Emails. If this isn&apos;t set, the email address of the user sending the invoice(s) is used instead.</inlineHelpText>
        <label>Email From Org-Wide Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InvoiceEmailToContactId__c</fullName>
        <deprecated>false</deprecated>
        <description>The salesforce id of the contact who receives copies of outbound invoice and order confirmation emails. This is required if invoices or order confirmations are sent via email.</description>
        <externalId>false</externalId>
        <inlineHelpText>The salesforce id of the contact who receives copies of outbound Invoice Emails. This is required if invoices are sent via email.</inlineHelpText>
        <label>Email To Contact Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LoginType__c</fullName>
        <defaultValue>&quot;Email&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>Declares which type of login will be used by Self Service.  Possible values are &apos;Username&apos; or &apos;Email&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>Declares which type of login will be used by Self Service.  Possible values are &apos;Username&apos; or &apos;Email&apos;.</inlineHelpText>
        <label>Login Type</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MapQuestAPIKey__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>MapQuest API Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MembershipDetailPage__c</fullName>
        <defaultValue>&quot;NU__MembershipDetail&quot;</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Membership Detail Page</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MembershipTermCalculator__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Membership Term Calculator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderCSSStaticResource__c</fullName>
        <defaultValue>&apos;/resource/1423250655000/NU__OrderCSS&apos;</defaultValue>
        <deprecated>false</deprecated>
        <description>The URL of the static resource containing the CSS to use for the order process pages.</description>
        <externalId>false</externalId>
        <inlineHelpText>The URL of the static resource containing the CSS to use for the order process pages.</inlineHelpText>
        <label>zOrder CSS Static Resource (DEPRECATED)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderJSStaticResource__c</fullName>
        <defaultValue>&apos;/resource/1411756934000/NU__OrderJS&apos;</defaultValue>
        <deprecated>false</deprecated>
        <description>The URL of the static resource containing the javascript to use across the order pages.</description>
        <externalId>false</externalId>
        <inlineHelpText>The URL of the static resource containing the javascript to use across the order pages.</inlineHelpText>
        <label>zOrder JS Static Resource (DEPRECATED)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrgAccountAddressesToFlowdown__c</fullName>
        <deprecated>false</deprecated>
        <description>Enter either &quot;Billing&quot;, &quot;Shipping&quot;, or &quot;Billing, Shipping&quot;, to specify the organization account addresses that should flowdown to their primary affiliations.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter either &quot;Billing&quot;, &quot;Shipping&quot;, or &quot;Billing, Shipping&quot;, to specify the organization account addresses that should flowdown to their primary affiliations.</inlineHelpText>
        <label>zOrg Acct Addrs To Flowdown (DEPRECATED)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OverridableInvoiceNumber__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Overridable Invoice Number</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PersonAccountAddressesToFlowdown__c</fullName>
        <deprecated>false</deprecated>
        <description>Enter either &quot;Billing&quot;, &quot;Shipping&quot;, &quot;Mailing&quot;, &quot;Other&quot;, or some combination separated by commas  to specify the person account addresses that should flowdown to their primary affiliations.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter either &quot;Billing&quot;, &quot;Shipping&quot;, &quot;Mailing&quot;, &quot;Other&quot;, or some combination separated by commas  to specify the person account addresses that should flowdown to their primary affiliations.</inlineHelpText>
        <label>zPrsn Acct Addrs To Flowdown (DEPRECATED</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PrepaymentPayableGLAccountId__c</fullName>
        <deprecated>false</deprecated>
        <description>The Salesforce Id of the GL Account to use for Cash Prepayments. When entered, cash prepayments are allowed. When blank, cash prepayments are not allowed.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Salesforce Id of the GL Account to use for Cash Prepayments. When entered, cash prepayments are allowed. When blank, cash prepayments are not allowed.</inlineHelpText>
        <label>Prepayment Payable GL Account Id</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PricingImplementation__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Pricing Implementation</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RecurringPaymentProcessedNotifier__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The custom class that notifies users when recurring payments have been processed</inlineHelpText>
        <label>Recurring Payment Processed Notifier</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RegistrationValidator__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The custom class that implements IRegistrationValidator2. If blank, StandardRegistrationValidator is used.</inlineHelpText>
        <label>Registration Validator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RememberOrderBatch__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Remember the most recently selected value of the batch dropdown in the order process</inlineHelpText>
        <label>Remember Order Batch</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RememberOrderEntity__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Remember the most recently selected value of the entity dropdown in the order process</inlineHelpText>
        <label>Remember Order Entity</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SalesTaxDefaulter__c</fullName>
        <deprecated>false</deprecated>
        <description>The Sales Tax Defaulter class to use to default the Sales Tax object in the order process.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Sales Tax Defaulter class to use to default the Sales Tax object in the order process.</inlineHelpText>
        <label>Sales Tax Defaulter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SelfServiceCalloutKey__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>zSelf Service Callout Key (DEPRECATED)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SelfServiceCalloutUrl__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>zSelf Service Callout Url (DEPRECATED)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SelfServiceUrl__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>zSelf Service Url (DEPRECATED)</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShippingCalculator__c</fullName>
        <deprecated>false</deprecated>
        <description>The class that performs shipping and handling calculations. If not specified, the StandardShippingCalculator is used.</description>
        <externalId>false</externalId>
        <inlineHelpText>The class that performs shipping and handling calculations. If not specified, the StandardShippingCalculator is used.</inlineHelpText>
        <label>Shipping Calculator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShowMembershipRepriceButton__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Shows a refresh button next to each dues product on the purchase membership page to recalculate the price after changing the term.</inlineHelpText>
        <label>Show Membership Re-price Button</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TransactionReferenceGroup__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The transaction reference group used when generating a given set of transactions. This number is maintained by the system.</description>
        <externalId>false</externalId>
        <inlineHelpText>The transaction reference group used when generating a given set of transactions. This number is maintained by the system.</inlineHelpText>
        <label>Transaction Reference Group</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Nimble AMS Settings</label>
</CustomObject>
