<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If blank, all accounts are eligible to use the coupon code</inlineHelpText>
        <label>zAccount (DEPRECATED)</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Coupon Codes</relationshipLabel>
        <relationshipName>Coupon_Codes</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If left blank, system will automatically generate</inlineHelpText>
        <label>Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CouponRule__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Coupon Rule</label>
        <referenceTo>CouponRule__c</referenceTo>
        <relationshipLabel>Coupon Codes</relationshipLabel>
        <relationshipName>Coupon_Codes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If left blank, system will automatically populate based on the coupon rule</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>StartDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If left blank, system will automatically populate based on the coupon rule</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StatusFlag__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(!ISBLANK(Status__c), IMAGE(
&quot;/resource/&quot; + $Setup.Namespace__c.Prefix__c + &quot;StatusIcons/&quot; +
CASE(Status__c,
&quot;Future&quot;, &quot;Future&quot;,
&quot;Active&quot;, &quot;Active&quot;,
&quot;Expired&quot;, &quot;Inactive&quot;,
&quot;blank&quot;) + &quot;.png&quot;,
&quot;Status Flag&quot;),
&apos;&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Blue = Future
Green = Active
Gray = Inactive</inlineHelpText>
        <label>Status Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(!ISBLANK(EndDate__c) &amp;&amp; EndDate__c &lt; TODAY(), &apos;Expired&apos;, 
    IF(!ISBLANK(StartDate__c) &amp;&amp; StartDate__c &gt; TODAY(), &apos;Future&apos;, &apos;Active&apos;)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Coupon Code</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CouponRule__c</columns>
        <columns>Account__c</columns>
        <columns>Code__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Coupon Codes</label>
    </listViews>
    <listViews>
        <fullName>All_Active</fullName>
        <columns>NAME</columns>
        <columns>CouponRule__c</columns>
        <columns>Account__c</columns>
        <columns>Code__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>ACTIVE</value>
        </filters>
        <label>All Coupon Codes - Active</label>
    </listViews>
    <listViews>
        <fullName>All_Expired</fullName>
        <columns>NAME</columns>
        <columns>CouponRule__c</columns>
        <columns>Account__c</columns>
        <columns>Code__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>EXPIRED</value>
        </filters>
        <label>All Coupon Codes - Expired</label>
    </listViews>
    <listViews>
        <fullName>All_Future</fullName>
        <columns>NAME</columns>
        <columns>CouponRule__c</columns>
        <columns>Account__c</columns>
        <columns>Code__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>FUTURE</value>
        </filters>
        <label>All Coupon Codes - Future</label>
    </listViews>
    <listViews>
        <fullName>All_Not_Expired</fullName>
        <columns>NAME</columns>
        <columns>CouponRule__c</columns>
        <columns>Account__c</columns>
        <columns>Code__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>EXPIRED</value>
        </filters>
        <label>All Coupon Codes - Not Expired</label>
    </listViews>
    <nameField>
        <displayFormat>Coupon Code {0000000}</displayFormat>
        <label>Coupon Code Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Coupon Codes</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>CouponRule__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StartDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>EndDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StatusFlag__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>CouponRule__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Code__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StartDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EndDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StatusFlag__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>CouponRule__c</lookupFilterFields>
        <lookupFilterFields>Account__c</lookupFilterFields>
        <lookupFilterFields>Code__c</lookupFilterFields>
        <lookupFilterFields>StartDate__c</lookupFilterFields>
        <lookupFilterFields>EndDate__c</lookupFilterFields>
        <lookupFilterFields>Status__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>CouponRule__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Code__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StartDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>EndDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StatusFlag__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>CouponRule__c</searchFilterFields>
        <searchFilterFields>Code__c</searchFilterFields>
        <searchFilterFields>StartDate__c</searchFilterFields>
        <searchFilterFields>EndDate__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchResultsAdditionalFields>CouponRule__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Code__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StartDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EndDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StatusFlag__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>StartDateBeforeEndDate</fullName>
        <active>true</active>
        <description>The Start Date must be before the End Date.</description>
        <errorConditionFormula>AND (ISBLANK( StartDate__c ) = False,
ISBLANK( EndDate__c ) = False,
StartDate__c &gt; EndDate__c)</errorConditionFormula>
        <errorMessage>The Start Date must be before the End Date.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>GenerateCouponCode</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Generate Coupon Code</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/{!$Setup.Namespace__c.Prefix__c}GenerateCoupon?CouponRuleId={!CouponRule__c.Id}</url>
    </webLinks>
</CustomObject>
