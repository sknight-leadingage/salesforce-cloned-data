<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpMembership</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Membership Types define the different types / levels of membership available to individuals and organizations.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>AccountJoinOnField__c</fullName>
        <deprecated>false</deprecated>
        <description>When a value is selected and a new membership is submitted or created manually for this membership type, the member account&apos;s join on field matching the selected value will be updated.</description>
        <externalId>false</externalId>
        <inlineHelpText>This Membership Type updates this field on the Account record during membership joins.</inlineHelpText>
        <label>Account Join On Field</label>
        <picklist>
            <picklistValues>
                <fullName>NU__JoinOn__c</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST_Join_On__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IAHSA_Join_On__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LTQA_Join_On__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Provider_Join_On__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>State_Provider_Join_On__c</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AccountMembershipField__c</fullName>
        <deprecated>false</deprecated>
        <description>When a value is selected and a current or future membership is submitted or created manually  for this membership type, the member account&apos;s membership field matching the selected value will be updated.</description>
        <externalId>false</externalId>
        <inlineHelpText>This Membership Type updates this field on the Account record. This field also impacts the Member Type field on the account.</inlineHelpText>
        <label>Account Membership Field</label>
        <picklist>
            <picklistValues>
                <fullName>NU__Membership__c</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Provider_Membership__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IAHSA_Membership__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CAST_Membership__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LTQA_Membership__c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>State_Provider_Membership__c</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AccountingMethod__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Cash - Income (or deferred income) is recognized at the time of payment.
Accrual - Income (or deferred income) is recognized at the time of the billing.</inlineHelpText>
        <label>Accounting Method</label>
        <picklist>
            <picklistValues>
                <fullName>Cash</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AnnualStartMonth__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Annual Start Month</label>
        <picklist>
            <picklistValues>
                <fullName>01</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>02</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>03</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>04</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>05</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>06</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>07</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>08</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>09</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>11</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>12</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AutoRenewRepricingDaysBeforeRenewal__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of days before the auto renewing membership&apos;s renewal date that the membership&apos;s dues amount begins being repriced. If blank, it defaults to 30 days before the renewal date.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of days before the auto renewing membership&apos;s renewal date that the membership&apos;s dues amount begins being repriced. If blank, it defaults to 30 days before the renewal date.</inlineHelpText>
        <label>Auto Renew Repricing Days Before Renewal</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <deprecated>false</deprecated>
        <description>The category for this membership type. If &quot;Company&quot; is chosen, flowdown rules apply. If &quot;Individual&quot; is chosen, flowdown rules are ignored. If Both is chosen for an Organization then flowdown rules apply.</description>
        <externalId>false</externalId>
        <inlineHelpText>If &quot;Company&quot; is chosen, flowdown rules apply.</inlineHelpText>
        <label>Category</label>
        <picklist>
            <picklistValues>
                <fullName>Individual</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Company</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Both</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>DaysBeforeExpiration__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>If Auto Renew is enabled, this determines the day in which the member will be renewed, in respect to the membership&apos;s end date. If blank, members will renew on their current membership end date.</description>
        <externalId>false</externalId>
        <inlineHelpText>If Auto Renew is enabled, this determines the day in which the member will be renewed, in respect to the membership&apos;s end date. If blank, members will renew on their current membership end date.</inlineHelpText>
        <label>Days Before Expiration</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Displays on the web</inlineHelpText>
        <label>Description</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Membership Types</relationshipLabel>
        <relationshipName>MembershipTypes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>FlowdownManagerClass__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Indicates which Membership Flowdown Manager class to use when processing membership flowdown from a &quot;Company&quot; account to its affiliates.</inlineHelpText>
        <label>Flowdown Manager Class</label>
        <picklist>
            <picklistValues>
                <fullName>CustomMembershipFlowdownManager</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>CustomProviderMembershipFlowdownManager</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CustomPersonOnlyMembershipFlowdownMgr</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>GracePeriod__c</fullName>
        <deprecated>false</deprecated>
        <description>Default - If period = 0 — there is no grace period and the paid thru date is the true expiration date
If period &gt; 0 — there is a grace period where the membership privileges continue</description>
        <externalId>false</externalId>
        <inlineHelpText>If period = 0, there is no grace period and the paid thru date is the true expiration date</inlineHelpText>
        <label>Grace Period (Months)</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsAutoRenew__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, memberships of this membership type may be automatically renewed upon expiration. Members&apos; credit cards will automatically be billed.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, this membership automatically renews upon expiration</inlineHelpText>
        <label>Auto Renewable</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsFlowdownEnabled__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>When flowdown is enabled, added an organization to a group can trigger adding that organization&apos;s members to another group.</description>
        <externalId>false</externalId>
        <label>zFlowdown Enabled (DEPRECATED)</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ProrationOverride__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Proration Override</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RenewalType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Anniversary - Memberships that can start on any month
Annual - Memberships that start on a set month (defined by the Annual Start Month field)</inlineHelpText>
        <label>Renewal Type</label>
        <picklist>
            <picklistValues>
                <fullName>Anniversary</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Annual</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>SelfServiceEnabled__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field is used to identify membership types that will be available for online joining and renewing.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, membership type can be purchased online.</inlineHelpText>
        <label>Self Service Enabled</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>StartDateControl__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to calculate the term start date for new joins when they join mid month.  This is a numeric day of the month field.  If the current system date is before this date, the term start date is the first day of the current month.  If the current system date is equal to or after this date, the term start date is the first day of the next month.  Example:  15 is entered.  If they join on 07/10 ... the term start would be 07/01.  If they join on 07/25 ... the term start would be 08/01.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines cut off day for new memberships. If sign up date is = or &gt; than this date, membership will begin the first of the next month, otherwise, membership will begin the first of the current month.</inlineHelpText>
        <label>Start Date Control (Day)</label>
        <picklist>
            <picklistValues>
                <fullName>01</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>02</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>03</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>04</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>05</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>06</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>07</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>08</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>09</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>11</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>12</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>13</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>14</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>15</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>16</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>17</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>18</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>19</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>20</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>21</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>22</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>23</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>24</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>25</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>26</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>27</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>28</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>29</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>30</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>31</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Term__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to track the number of months for the term.</description>
        <externalId>false</externalId>
        <inlineHelpText>Can be overridden at the Membership Type Product Link level.</inlineHelpText>
        <label>Term (Months)</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Membership Type</label>
    <listViews>
        <fullName>NU_AllActiveMembershipTypes</fullName>
        <columns>NAME</columns>
        <columns>Category__c</columns>
        <columns>Grants_National_Membership__c</columns>
        <columns>Grants_State_Membership__c</columns>
        <columns>RenewalType__c</columns>
        <columns>AccountingMethod__c</columns>
        <columns>Term__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <columns>FlowdownManagerClass__c</columns>
        <columns>AccountMembershipField__c</columns>
        <columns>AccountJoinOnField__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </filters>
        <label>All Active Membership Types</label>
    </listViews>
    <listViews>
        <fullName>NU_AllActiveRecurringMembershipTypes</fullName>
        <columns>NAME</columns>
        <columns>Category__c</columns>
        <columns>RenewalType__c</columns>
        <columns>AccountingMethod__c</columns>
        <columns>Term__c</columns>
        <columns>AccountJoinOnField__c</columns>
        <columns>AccountMembershipField__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <columns>FlowdownManagerClass__c</columns>
        <columns>Entity__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>IsAutoRenew__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </filters>
        <label>All Active Recurring Membership Types</label>
    </listViews>
    <listViews>
        <fullName>NU_AllInactiveMembershipTypes</fullName>
        <columns>NAME</columns>
        <columns>Category__c</columns>
        <columns>RenewalType__c</columns>
        <columns>AccountingMethod__c</columns>
        <columns>Term__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <columns>FlowdownManagerClass__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>Active</value>
        </filters>
        <label>All Inactive Membership Types</label>
    </listViews>
    <listViews>
        <fullName>NU_AllInactiveRecurringMembershipTypes</fullName>
        <columns>NAME</columns>
        <columns>Category__c</columns>
        <columns>RenewalType__c</columns>
        <columns>AccountingMethod__c</columns>
        <columns>Term__c</columns>
        <columns>AccountJoinOnField__c</columns>
        <columns>AccountMembershipField__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <columns>FlowdownManagerClass__c</columns>
        <columns>Entity__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>IsAutoRenew__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </filters>
        <label>All Inactive Recurring Membership Types</label>
    </listViews>
    <listViews>
        <fullName>NU_AllMembershipTypes</fullName>
        <columns>NAME</columns>
        <columns>Entity__c</columns>
        <columns>Grants_National_Membership__c</columns>
        <columns>Category__c</columns>
        <columns>RenewalType__c</columns>
        <columns>AccountingMethod__c</columns>
        <columns>Term__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <columns>Status__c</columns>
        <columns>FlowdownManagerClass__c</columns>
        <columns>AccountMembershipField__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Membership Types</label>
    </listViews>
    <listViews>
        <fullName>NU_AllRecurringMembershipTypes</fullName>
        <columns>NAME</columns>
        <columns>Category__c</columns>
        <columns>RenewalType__c</columns>
        <columns>AccountingMethod__c</columns>
        <columns>Term__c</columns>
        <columns>AccountJoinOnField__c</columns>
        <columns>AccountMembershipField__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <columns>Status__c</columns>
        <columns>FlowdownManagerClass__c</columns>
        <columns>Entity__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>IsAutoRenew__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>All Recurring Membership Types</label>
    </listViews>
    <nameField>
        <label>Membership Type Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Membership Types</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RenewalType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>AccountingMethod__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Term__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SelfServiceEnabled__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>FlowdownManagerClass__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Entity__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Category__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RenewalType__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AccountingMethod__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Term__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SelfServiceEnabled__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>FlowdownManagerClass__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Entity__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Category__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RenewalType__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>AccountingMethod__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Term__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SelfServiceEnabled__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>FlowdownManagerClass__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Entity__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>AccountingMethod__c</searchFilterFields>
        <searchFilterFields>Category__c</searchFilterFields>
        <searchFilterFields>Term__c</searchFilterFields>
        <searchFilterFields>SelfServiceEnabled__c</searchFilterFields>
        <searchFilterFields>FlowdownManagerClass__c</searchFilterFields>
        <searchFilterFields>Entity__c</searchFilterFields>
        <searchResultsAdditionalFields>Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RenewalType__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>AccountingMethod__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Term__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SelfServiceEnabled__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>FlowdownManagerClass__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Entity__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>AccountJoinOnFieldRequired</fullName>
        <active>true</active>
        <description>The Account Join On Field is required.</description>
        <errorConditionFormula>ISBLANK(TEXT(AccountJoinOnField__c))</errorConditionFormula>
        <errorMessage>The Account Join On Field is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>AccountMembershipFieldRequired</fullName>
        <active>true</active>
        <description>The Account Membership Field is required.</description>
        <errorConditionFormula>ISBLANK(TEXT(AccountMembershipField__c))</errorConditionFormula>
        <errorMessage>The Account Membership Field is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>AccountingMethodRequired</fullName>
        <active>true</active>
        <description>The Accounting Method is required.</description>
        <errorConditionFormula>ISBLANK(TEXT(AccountingMethod__c))</errorConditionFormula>
        <errorMessage>The Accounting Method is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>AnnualStartMonthRequired</fullName>
        <active>true</active>
        <errorConditionFormula>ISPICKVAL(RenewalType__c, &apos;Annual&apos;) &amp;&amp; ISBLANK(TEXT(AnnualStartMonth__c))</errorConditionFormula>
        <errorDisplayField>AnnualStartMonth__c</errorDisplayField>
        <errorMessage>You must enter a value</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CategoryRequired</fullName>
        <active>true</active>
        <description>The Category is required.</description>
        <errorConditionFormula>ISBLANK(TEXT(Category__c))</errorConditionFormula>
        <errorMessage>The category is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>DaysBeforeExpirationNotNegative</fullName>
        <active>true</active>
        <errorConditionFormula>IsBlank(  DaysBeforeExpiration__c ) = False &amp;&amp;
DaysBeforeExpiration__c &lt; 0</errorConditionFormula>
        <errorDisplayField>DaysBeforeExpiration__c</errorDisplayField>
        <errorMessage>The Days Before Expiration cannot be negative.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>FlowdownManagerClassRequired</fullName>
        <active>false</active>
        <description>The flowdown manager class is required when the category is &quot;Company&quot;. Initially, the manager was needed, but if no flowdown is required, the manager should just be left blank.</description>
        <errorConditionFormula>AND ( TEXT(  Category__c  ) = &apos;Company&apos;,
            IsBlank ( Text(  FlowdownManagerClass__c  ) ))</errorConditionFormula>
        <errorMessage>The flowdown manager class is required when the category is &quot;Company&quot;.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>GracePeriodNotNegative</fullName>
        <active>true</active>
        <errorConditionFormula>GracePeriod__c &lt; 0</errorConditionFormula>
        <errorDisplayField>GracePeriod__c</errorDisplayField>
        <errorMessage>The grace period can not be negative.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>GracePeriodRequired</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(GracePeriod__c)</errorConditionFormula>
        <errorDisplayField>GracePeriod__c</errorDisplayField>
        <errorMessage>The grace period is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RenewalTypeRequired</fullName>
        <active>true</active>
        <description>The Renewal Type is required.</description>
        <errorConditionFormula>ISBLANK(TEXT(RenewalType__c))</errorConditionFormula>
        <errorMessage>The Renewal Type is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RepricingDaysNotNegative</fullName>
        <active>true</active>
        <errorConditionFormula>IsBlank(  AutoRenewRepricingDaysBeforeRenewal__c ) = False &amp;&amp;
AutoRenewRepricingDaysBeforeRenewal__c &lt; 0</errorConditionFormula>
        <errorDisplayField>AutoRenewRepricingDaysBeforeRenewal__c</errorDisplayField>
        <errorMessage>The Auto Renew Repricing Days Before Renewal cannot be negative.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>StartDateControlRequired</fullName>
        <active>true</active>
        <errorConditionFormula>ISPICKVAL(RenewalType__c, &apos;Anniversary&apos;) &amp;&amp; ISBLANK(TEXT(StartDateControl__c))</errorConditionFormula>
        <errorDisplayField>StartDateControl__c</errorDisplayField>
        <errorMessage>You must enter a value</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>TermAtLeastOneMonth</fullName>
        <active>true</active>
        <description>The term must be greater than zero.</description>
        <errorConditionFormula>Term__c &lt; 1</errorConditionFormula>
        <errorMessage>The term must be at least one month.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>TermForAnniversaryRequired</fullName>
        <active>true</active>
        <description>The term for an anniversary renewal type is required.</description>
        <errorConditionFormula>AND( IsBlank(  Term__c  ),
           Text(  RenewalType__c  ) = &apos;Anniversary&apos;)</errorConditionFormula>
        <errorMessage>The term is required for Anniversary renewal types.</errorMessage>
    </validationRules>
</CustomObject>
