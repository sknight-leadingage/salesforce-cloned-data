<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fieldSets>
        <fullName>ProductColumns</fullName>
        <description>Product Related List</description>
        <displayedFields>
            <field>OrderItem__r.Customer__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Quantity__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Product2__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Product Columns</label>
    </fieldSets>
    <fields>
        <fullName>AdjustmentDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The date of the last adjustment.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date of the last adjustment.</inlineHelpText>
        <label>Adjustment Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Coupon__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The coupon record created from this coupon order line.</description>
        <externalId>false</externalId>
        <inlineHelpText>The coupon record created from this coupon order line.</inlineHelpText>
        <label>Coupon</label>
        <referenceTo>Coupon__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DeferredSchedule__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Specifies the deferred schedule used to recognize this purchase&apos;s deferred revenue.</inlineHelpText>
        <label>Deferred Schedule</label>
        <referenceTo>DeferredSchedule__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Donation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Donation</label>
        <referenceTo>Donation__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DownloadUrl__c</fullName>
        <deprecated>false</deprecated>
        <description>The download URL is generated to require users to login and validate access to content before retrieving it.</description>
        <externalId>false</externalId>
        <formula>IF(Product2__r.IsDownloadable__c, $Setup.NimbleAMSSettings__c.DownloadProxyUrl__c + &apos;?id=&apos; + Id, &apos;&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The download URL is generated to require users to login and validate access to content before retrieving it.</inlineHelpText>
        <label>Download URL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EventBadge__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The badge associated with this badge order line.</description>
        <externalId>false</externalId>
        <inlineHelpText>The badge associated with this badge order line.</inlineHelpText>
        <label>Event Badge</label>
        <referenceTo>EventBadge__c</referenceTo>
        <relationshipLabel>Badge Order Item Lines</relationshipLabel>
        <relationshipName>BadgeOrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExpectedRecurringPayment__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount expected to be paid by automatic recurring payments.</description>
        <externalId>false</externalId>
        <formula>IF(Membership__c != null &amp;&amp; Membership__r.RecurringPayment__c != null &amp;&amp; ISPICKVAL(Membership__r.RecurringPayment__r.Status__c,&apos;Active&apos;),TotalPrice__c,0.0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The amount expected to be paid by automatic recurring payments.</inlineHelpText>
        <label>Expected Recurring Payment</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IsCouponOrderItemLine__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Is Coupon Order Item Line</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsShippable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Determines whether or not this product purchase is shippable.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines whether or not this product purchase is shippable.</inlineHelpText>
        <label>Shippable</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsTaxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field indicates whether the product purchased is taxable or not and is set from the &quot;Is Taxable&quot; field on the product. It was created to be able to calculate the &quot;Taxable Amount&quot; on the order.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field indicates whether the product purchased is taxable or not and is set from the &quot;Is Taxable&quot; field on the product. It was created to be able to calculate the &quot;Taxable Amount&quot; on the order.</inlineHelpText>
        <label>Taxable</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MembershipTypeProductLink__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Membership Type Product Link</label>
        <referenceTo>MembershipTypeProductLink__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Membership__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The membership enrollment record created from this membership order line.</description>
        <externalId>false</externalId>
        <inlineHelpText>The membership enrollment record created from this membership order line.</inlineHelpText>
        <label>Membership</label>
        <referenceTo>Membership__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Merchandise__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Merchandise</label>
        <referenceTo>Merchandise__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Miscellaneous__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Miscellaneous</label>
        <referenceTo>Miscellaneous__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrderItem__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Order Item</label>
        <referenceTo>OrderItem__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Product2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Registration2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Registration</label>
        <referenceTo>Registration2__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Registration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field is no longer used. Use the other Registration field to the &quot;Registration2&quot; object.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is no longer used. Use the other Registration field to the &quot;Registration2&quot; object.</inlineHelpText>
        <label>zRegistration (DEPRECATED)</label>
        <referenceTo>Registration__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Subscription__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Subscription</label>
        <referenceTo>Subscription__c</referenceTo>
        <relationshipLabel>Order Item Lines</relationshipLabel>
        <relationshipName>OrderItemLines</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>TotalPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>UnitPrice__c * Quantity__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TransactionDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>OrderItem__r.TransactionDate__c</formula>
        <label>Transaction Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>11</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Order Item Line</label>
    <nameField>
        <displayFormat>Order Item Line {0000000}</displayFormat>
        <label>Order Item Line Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Order Item Lines</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>CancelledReadOnly</fullName>
        <active>true</active>
        <errorConditionFormula>ISPICKVAL(PRIORVALUE(Status__c), &apos;Cancelled&apos;)</errorConditionFormula>
        <errorMessage>Cancelled records cannot be modified.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CheckInventoryOnHand</fullName>
        <active>true</active>
        <errorConditionFormula>AND(Product2__r.TrackInventory__c,
    Product2__r.RecordTypeName__c != &apos;Merchandise&apos; || !$Setup.NimbleAMSPublicSettings__c.CanBackorderStaffView__c,
    Quantity__c - IF(ISPICKVAL(Status__c, &apos;Active&apos;) &amp;&amp; !ISNEW(), PRIORVALUE(Quantity__c), 0) &gt; Product2__r.InventoryOnHand__c)</errorConditionFormula>
        <errorMessage>Quantity exceeds inventory on hand.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>PriceMustBePositive</fullName>
        <active>false</active>
        <errorConditionFormula>UnitPrice__c &lt; 0 &amp;&amp; Product2__r.RecordType.Name != &apos;Coupon&apos;</errorConditionFormula>
        <errorDisplayField>UnitPrice__c</errorDisplayField>
        <errorMessage>The unit price cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ProductRequired</fullName>
        <active>true</active>
        <description>The product is required.</description>
        <errorConditionFormula>ISBLANK( Product2__c )</errorConditionFormula>
        <errorMessage>The product is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>QuantityMustBePositive</fullName>
        <active>true</active>
        <errorConditionFormula>Quantity__c &lt;= 0 &amp;&amp; NOT(ISPICKVAL(Status__c, &apos;Cancelled&apos;))</errorConditionFormula>
        <errorDisplayField>Quantity__c</errorDisplayField>
        <errorMessage>The quantity must be greater than zero.</errorMessage>
    </validationRules>
</CustomObject>
