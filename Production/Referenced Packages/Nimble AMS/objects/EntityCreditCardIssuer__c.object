<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Defines the Credit Card Issuers available for a given entity.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>BankAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bank Account</label>
        <referenceTo>BankAccount__c</referenceTo>
        <relationshipLabel>Entity Credit Card Issuers</relationshipLabel>
        <relationshipName>EntityCreditCardIssuers</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CreditCardIssuer__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Credit Card Issuer</label>
        <referenceTo>CreditCardIssuer__c</referenceTo>
        <relationshipLabel>Credit Cart Issuers</relationshipLabel>
        <relationshipName>CreditCartIssuers</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Credit Card Issuers</relationshipLabel>
        <relationshipName>CreditCardIssuers</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If Active, this credit card can be used for this entity</inlineHelpText>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Entity Credit Card Issuer</label>
    <nameField>
        <displayFormat>Entity CC Issuer {0000000}</displayFormat>
        <label>Entity Credit Cart Issuer Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Entity Credit Card Issuers</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>BankAccountEntityMustMatch</fullName>
        <active>true</active>
        <description>The Bank Account&apos;s Entity must match the Entity selected.</description>
        <errorConditionFormula>AND( IsBlank( Entity__c )=False, 
Entity__c &lt;&gt; BankAccount__r.Entity__c)</errorConditionFormula>
        <errorDisplayField>BankAccount__c</errorDisplayField>
        <errorMessage>The Bank Account&apos;s Entity must match the Entity selected.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>BankAccountRequired</fullName>
        <active>true</active>
        <description>The Bank Account is required.</description>
        <errorConditionFormula>IsBlank( BankAccount__c )</errorConditionFormula>
        <errorMessage>The Bank Account is required.</errorMessage>
    </validationRules>
</CustomObject>
