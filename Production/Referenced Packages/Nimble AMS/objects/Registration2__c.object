<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>RegistrationRedirector</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>RegistrationRedirector</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpEvents</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fieldSets>
        <fullName>RegistrationColumns</fullName>
        <description>Registration Related List</description>
        <displayedFields>
            <field>Account__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>OrderItem__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>PriceClass__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Amount__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Status__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Event2__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Registration Columns</label>
    </fieldSets>
    <fields>
        <fullName>Account__c</fullName>
        <deprecated>false</deprecated>
        <description>The individual who registered for the event.</description>
        <externalId>false</externalId>
        <inlineHelpText>Registrant</inlineHelpText>
        <label>Account</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Only people can be registered for events.</errorMessage>
            <filterItems>
                <field>Account.PersonContact__c</field>
                <operation>notEqual</operation>
                <value></value>
            </filterItems>
            <infoMessage>Only people can be registered for events.</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Registrations</relationshipLabel>
        <relationshipName>Registrations2</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ExternalAmount__c + OrderItem__r.GrandTotal__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Total amount for registration</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Balance__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount still owed or to be refunded for this order item.</description>
        <externalId>false</externalId>
        <formula>OrderItem__r.Balance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The amount still owed or to be refunded for this order item.</inlineHelpText>
        <label>Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CancellationReason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cancellation Reason</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>EntityName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Event2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Event</label>
        <referenceTo>Event__c</referenceTo>
        <relationshipLabel>Registrations</relationshipLabel>
        <relationshipName>Registrations3</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EventName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Event2__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Event Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EventStartDate2__c</fullName>
        <deprecated>false</deprecated>
        <description>The start date for an event, displayed in time zone where the event is held.</description>
        <externalId>false</externalId>
        <formula>Event2__r.StartDate__c</formula>
        <label>Event Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>EventStartDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Event2__r.StartDate__c</formula>
        <label>zEvent Start Date (DEPRECATED)</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Event__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Event</label>
        <referenceTo>Event__c</referenceTo>
        <relationshipLabel>Registrations</relationshipLabel>
        <relationshipName>Registrations2</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Used in importing legacy data. Dollar Amount is added to Amount field.</inlineHelpText>
        <label>External Amount</label>
        <precision>11</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExternalId__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>FullName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>trim( Account__r.PersonContact__r.FirstName &amp; &quot; &quot; &amp; Account__r.PersonContact__r.LastName )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Used for reporting.</inlineHelpText>
        <label>Full Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Order Item registration was purchased through.</inlineHelpText>
        <label>Order Item</label>
        <referenceTo>OrderItem__c</referenceTo>
        <relationshipLabel>Registrations</relationshipLabel>
        <relationshipName>Registrations2</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PriceClass__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>OrderItem__r.PriceClass__r.Name</formula>
        <label>Price Class</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PrimaryAffiliation__c</fullName>
        <deprecated>false</deprecated>
        <description>Customer&apos;s engagement account when the registration record was created (customer&apos;s primary affiliation)</description>
        <externalId>false</externalId>
        <formula>OrderItem__r.CustomerPrimaryAffiliation__r.Name</formula>
        <inlineHelpText>Customer&apos;s engagement account when the registration record was created (customer&apos;s primary affiliation)</inlineHelpText>
        <label>Engagement Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RegistrantAddress__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Account__r.PersonContact__r.MailingStreet  &amp; BR() &amp;
 Account__r.PersonContact__r.MailingCity  &amp; &apos;, &apos; &amp;
 Account__r.PersonContact__r.MailingState  &amp; &apos; &apos; &amp;
 Account__r.PersonContact__r.MailingPostalCode</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Registrant Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RegistrantEmail__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Registrant Email</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RegistrationType__c</fullName>
        <deprecated>false</deprecated>
        <description>Registration type of the attendee. Registration type is used to limit the selection of registration products online.</description>
        <externalId>false</externalId>
        <inlineHelpText>Registration Type of the attendee.</inlineHelpText>
        <label>Registration Type</label>
        <picklist>
            <picklistValues>
                <fullName>Attendee</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Search__c</fullName>
        <deprecated>false</deprecated>
        <description>Contains related object value(s) so that these records show up in a global search. This field should not be shown on page layouts.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contains related object value(s) so that these records show up in a global search. This field should not be shown on page layouts.</inlineHelpText>
        <label>Search</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>StatusFlag__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IMAGE(
&quot;/resource/&quot; + $Setup.Namespace__c.Prefix__c + &quot;StatusIcons/&quot; +
CASE(Status__c,
&quot;Active&quot;, &quot;Active.png&quot;,
&quot;Cancelled&quot;, &quot;Cancelled.png&quot;,
&quot;s.gif&quot;),
&quot;Status Flag&quot;)</formula>
        <inlineHelpText>Red = Cancelled
Green = Active</inlineHelpText>
        <label>Status Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>TotalPayment__c</fullName>
        <deprecated>false</deprecated>
        <description>The sum of the payments and refunds applied to this order item.</description>
        <externalId>false</externalId>
        <formula>OrderItem__r.TotalPayment__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The sum of the payments and refunds applied to this order item.</inlineHelpText>
        <label>Total Payment</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Registration</label>
    <listViews>
        <fullName>AllRegistrations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>PrimaryAffiliation__c</columns>
        <columns>Event__c</columns>
        <columns>EventStartDate2__c</columns>
        <columns>PriceClass__c</columns>
        <columns>Amount__c</columns>
        <columns>StatusFlag__c</columns>
        <columns>Status__c</columns>
        <columns>OrderItem__c</columns>
        <columns>ExternalId__c</columns>
        <columns>EntityName__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Registrations</label>
    </listViews>
    <listViews>
        <fullName>AllRegistrationsActive</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>PrimaryAffiliation__c</columns>
        <columns>Event__c</columns>
        <columns>EventStartDate2__c</columns>
        <columns>PriceClass__c</columns>
        <columns>Amount__c</columns>
        <columns>StatusFlag__c</columns>
        <columns>Status__c</columns>
        <columns>OrderItem__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </filters>
        <label>All Registrations - Active</label>
    </listViews>
    <listViews>
        <fullName>AllRegistrationsCancelled</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>PrimaryAffiliation__c</columns>
        <columns>Event__c</columns>
        <columns>Registration_Group__c</columns>
        <columns>Amount__c</columns>
        <columns>StatusFlag__c</columns>
        <columns>Status__c</columns>
        <columns>OrderItem__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </filters>
        <label>All Registrations - Cancelled</label>
    </listViews>
    <listViews>
        <fullName>LastWeeksRegistrations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>PrimaryAffiliation__c</columns>
        <columns>Event__c</columns>
        <columns>EventStartDate2__c</columns>
        <columns>PriceClass__c</columns>
        <columns>Amount__c</columns>
        <columns>StatusFlag__c</columns>
        <columns>Status__c</columns>
        <columns>OrderItem__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>LAST_WEEK</value>
        </filters>
        <label>Last Week&apos;s Registrations</label>
    </listViews>
    <listViews>
        <fullName>ThisWeeksRegistrations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Event__c</columns>
        <columns>Registration_Group__c</columns>
        <columns>StatusFlag__c</columns>
        <columns>Status__c</columns>
        <columns>ShowCare_AmountPaid__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>THIS_WEEK</value>
        </filters>
        <label>This Week&apos;s Registrations</label>
    </listViews>
    <listViews>
        <fullName>TodaysRegistrations</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Event__c</columns>
        <columns>Registration_Group__c</columns>
        <columns>StatusFlag__c</columns>
        <columns>Status__c</columns>
        <columns>ShowCare_AmountPaid__c</columns>
        <columns>LeadingAge_Start__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>Today&apos;s Registrations</label>
    </listViews>
    <nameField>
        <displayFormat>Registration {0000000}</displayFormat>
        <label>Registration Id</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Registrations</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PrimaryAffiliation__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Event__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ShowCare_AmountPaid__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StatusFlag__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OrderItem__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PrimaryAffiliation__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Event__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EventStartDate2__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PriceClass__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Amount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StatusFlag__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>OrderItem__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PrimaryAffiliation__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Event__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>EventStartDate2__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PriceClass__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Amount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StatusFlag__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>OrderItem__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>PrimaryAffiliation__c</searchFilterFields>
        <searchFilterFields>Event__c</searchFilterFields>
        <searchFilterFields>PriceClass__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PrimaryAffiliation__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Event__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EventStartDate2__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StatusFlag__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ShowCare_AmountPaid__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>OrderItem__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>OrderRequired</fullName>
        <active>false</active>
        <errorConditionFormula>ISNULL(  OrderItem__c  )</errorConditionFormula>
        <errorMessage>An order is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RegistrantRequired</fullName>
        <active>true</active>
        <errorConditionFormula>IsNull( Account__c )</errorConditionFormula>
        <errorMessage>The registrant is required.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>NewRegistration</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>New Registration</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/{!$Setup.Namespace__c.Prefix__c}OrderEventSessionProducts?billTo={!Account.Id}&amp;ret={!URLENCODE(&apos;/&apos; + Account.Id)}</url>
    </webLinks>
    <webLinks>
        <fullName>SendCancelledEmail</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>Send Cancelled Email</masterLabel>
        <openType>newWindow</openType>
        <page>SendRegistrationCancelled</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
</CustomObject>
