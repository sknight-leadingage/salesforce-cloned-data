<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpMembership</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Category__c</fullName>
        <deprecated>false</deprecated>
        <description>This field is used to segregate products by member type. For example, if individual is chosen, the product will only display during the individual membership process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines whether the product displays for individuals, organizations or both.</inlineHelpText>
        <label>Category</label>
        <picklist>
            <picklistValues>
                <fullName>Individual</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Company</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>DisplayOrder__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>Dictates the order in which the linked membership product displays. 0 being the first item shown, then 1 and so on.</description>
        <externalId>false</externalId>
        <inlineHelpText>Set display order for each Membership Type Product Link within a Membership Type for the web and back-office.</inlineHelpText>
        <label>Display Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Product__r.Entity__r.Name</formula>
        <label>Entity</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IsQuantityEditable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Note: Only supported through the Bundle API. When checked, online users can specify the quantity of a product to purchase.</description>
        <externalId>false</externalId>
        <inlineHelpText>Note: Only supported through the Bundle API. Check this box to allow online users to specify the quantity of a product.</inlineHelpText>
        <label>Is Quantity Editable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MembershipType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Membership Type</label>
        <referenceTo>MembershipType__c</referenceTo>
        <relationshipLabel>Membership Type Product Links</relationshipLabel>
        <relationshipName>MembershipTypeProductLinks</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ParentLink__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Used for multi-year memberships</inlineHelpText>
        <label>Parent Link</label>
        <referenceTo>MembershipTypeProductLink__c</referenceTo>
        <relationshipLabel>Related Links</relationshipLabel>
        <relationshipName>RelatedLinks</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PriceOverride__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Overrides Product setting</inlineHelpText>
        <label>Price Override</label>
        <precision>11</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR 2</booleanFilter>
            <errorMessage>Please choose a Membership or Donation Product.</errorMessage>
            <filterItems>
                <field>Product__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Membership</value>
            </filterItems>
            <filterItems>
                <field>Product__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Donation</value>
            </filterItems>
            <infoMessage>Only Membership and Donation Products may be selected.</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Membership Type Product Links</relationshipLabel>
        <relationshipName>MembershipTypeProductLinks</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ProrationRule__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Proration rule to use for Annual Joins.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Proration rule to use for Annual Joins.</inlineHelpText>
        <label>Proration Rule</label>
        <referenceTo>ProrationRule__c</referenceTo>
        <relationshipLabel>Membership Type Product Links</relationshipLabel>
        <relationshipName>MembershipTypeProductLinks</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Purpose__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Primary-main membership product
Required-membership products such as processing fees
Optional-membership products such as chapters
Donation-membership products such as donations</inlineHelpText>
        <label>Purpose</label>
        <picklist>
            <picklistValues>
                <fullName>Primary</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Required</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Optional</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Donation</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>SelfServiceEnabled__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field is used to identify memberships that will be available for online joining and renewing.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, membership type can be purchased online.</inlineHelpText>
        <label>Self Service Enabled</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Stage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Required if the Purpose is Primary.</inlineHelpText>
        <label>Stage</label>
        <picklist>
            <picklistValues>
                <fullName>Join</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Renew</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Both</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>TermOverrideMonths__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Overrides Membership Type setting</inlineHelpText>
        <label>Term Override (Months)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Membership Type Product Link</label>
    <listViews>
        <fullName>NU_AllActiveMembershipTypeProductLinks</fullName>
        <columns>NAME</columns>
        <columns>MembershipType__c</columns>
        <columns>Product__c</columns>
        <columns>Purpose__c</columns>
        <columns>Stage__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </filters>
        <label>All Active Membership Type Product Links</label>
    </listViews>
    <listViews>
        <fullName>NU_AllInactiveMembershipTypeProdLinks</fullName>
        <columns>NAME</columns>
        <columns>MembershipType__c</columns>
        <columns>Product__c</columns>
        <columns>Purpose__c</columns>
        <columns>Stage__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </filters>
        <label>All Inactive Membership Type Prod Links</label>
    </listViews>
    <listViews>
        <fullName>NU_AllMembershipTypeProductLinks</fullName>
        <columns>NAME</columns>
        <columns>MembershipType__c</columns>
        <columns>Product__c</columns>
        <columns>Purpose__c</columns>
        <columns>Stage__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Membership Type Product Links</label>
    </listViews>
    <nameField>
        <displayFormat>MemberTypeProdLink {0000000}</displayFormat>
        <label>Membership Type Product Link Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Membership Type Product Links</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>MemberTypeEntityAndProdEntityMustMatch</fullName>
        <active>true</active>
        <description>DEPRECATED</description>
        <errorConditionFormula>false</errorConditionFormula>
        <errorMessage>DEPRECATED</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>MembershipProductsCanOnlyBePrimary</fullName>
        <active>true</active>
        <description>Only Membership Products can have a Primary purpose.</description>
        <errorConditionFormula>Text(Purpose__c) = &apos;Primary&apos; &amp;&amp;
Product__r.RecordTypeName__c  != &apos;Membership&apos;</errorConditionFormula>
        <errorDisplayField>Product__c</errorDisplayField>
        <errorMessage>Only Membership Products can have a Primary purpose.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ParentMTPLEntityMustMatch</fullName>
        <active>true</active>
        <description>The parent link&apos;s entity must match this membership type product link&apos;s entity.</description>
        <errorConditionFormula>AND(ISBLANK(ParentLink__c) = false,
ParentLink__r.MembershipType__r.Entity__c &lt;&gt; MembershipType__r.Entity__c)</errorConditionFormula>
        <errorMessage>The parent link&apos;s entity must match this membership type product link&apos;s entity.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ProrationAppliedToAnnualPrimaryJoinOnly</fullName>
        <active>true</active>
        <errorConditionFormula>AND( IsBlank( ProrationRule__c ) = false,
     OR (Text(Purpose__c) &lt;&gt; &apos;Primary&apos;,
         Text(Stage__c) = &apos;Renew&apos;,
         Text(MembershipType__r.RenewalType__c) &lt;&gt; &apos;Annual&apos;)
   )</errorConditionFormula>
        <errorDisplayField>ProrationRule__c</errorDisplayField>
        <errorMessage>Proration can only be applied to Annual Primary Join membership products.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ProrationTermDiffersFromMembershipTerm</fullName>
        <active>true</active>
        <errorConditionFormula>AND( IsBlank( ProrationRule__c ) = false,
     OR( AND( IsBlank( TermOverrideMonths__c ) = false,
              TermOverrideMonths__c &lt;&gt; ProrationRule__r.MaxTermMonth__c),
         MembershipType__r.Term__c &lt;&gt; ProrationRule__r.MaxTermMonth__c
       )
    )</errorConditionFormula>
        <errorMessage>The membership term does not match the proration rule&apos;s term.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>StageRequired</fullName>
        <active>true</active>
        <errorConditionFormula>(ISPICKVAL(Purpose__c, &apos;Primary&apos;) || ISPICKVAL(Purpose__c, &apos;Required&apos;)) &amp;&amp; ISBLANK(TEXT(Stage__c))</errorConditionFormula>
        <errorDisplayField>Stage__c</errorDisplayField>
        <errorMessage>Stage is required when Purpose is &quot;Primary&quot; or &quot;Required&quot;</errorMessage>
    </validationRules>
</CustomObject>
