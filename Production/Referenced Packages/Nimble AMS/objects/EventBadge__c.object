<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpEvents</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Contains the badges for an event.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fieldSets>
        <fullName>EventBadgeAddEdit</fullName>
        <description>Dialog for adding/editing event badges</description>
        <displayedFields>
            <field>Salutation__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>MiddleName__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Suffix__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>CasualName__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>ProfessionalTitle__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Company__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>City__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>State__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Designation__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>Event Badge Add Edit</label>
    </fieldSets>
    <fields>
        <fullName>BadgeClass__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Sub-type for registration badges</inlineHelpText>
        <label>Badge Class</label>
        <picklist>
            <controllingField>BadgeType__c</controllingField>
            <picklistValues>
                <fullName>Member</fullName>
                <controllingFieldValues>Registrant</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Speaker</fullName>
                <controllingFieldValues>Registrant</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Guest</fullName>
                <controllingFieldValues>Guest</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Registrant</fullName>
                <controllingFieldValues>Registrant</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>BadgeLogo__c</fullName>
        <deprecated>false</deprecated>
        <description>Logo of the event</description>
        <externalId>false</externalId>
        <formula>Registration2__r.Event2__r.BadgeLogo__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Badge Logo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BadgeType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Badge Type</label>
        <picklist>
            <picklistValues>
                <fullName>Guest</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Registrant</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>CasualName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If account has no casual name, first name will be automatically loaded</inlineHelpText>
        <label>Casual Name</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>City</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Company</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Designation__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Designation</label>
        <picklist>
            <picklistValues>
                <fullName>CAE</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>EntityLogo__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Registration2__r.OrderItem__r.Order__r.Entity__r.LogoURL__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Entity Logo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Event__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the event for which the badge was created</description>
        <externalId>false</externalId>
        <formula>Registration2__r.EventName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Event</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>FirstName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>First Name</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LastName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Last Name</label>
        <length>50</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MiddleName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Middle Name</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>FirstName__c &amp; &apos; &apos; &amp; LastName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderItemLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Order Item Line</label>
        <referenceTo>OrderItemLine__c</referenceTo>
        <relationshipLabel>Event Badges</relationshipLabel>
        <relationshipName>EventBadges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Price__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(OrderItemLine__r.Product2__r.IsEventBadge__c, OrderItemLine__r.TotalPrice__c, null)</formula>
        <inlineHelpText>Price paid for badge</inlineHelpText>
        <label>Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ProfessionalTitle__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Professional Title</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Registration2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Registration</label>
        <referenceTo>Registration2__c</referenceTo>
        <relationshipLabel>Event Badges</relationshipLabel>
        <relationshipName>EventBadges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Registration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field is no longer used. Use the other &quot;Registration&quot; field to the &quot;Registration2&quot; object.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is no longer used. Use the other &quot;Registration&quot; field to the &quot;Registration2&quot; object.</inlineHelpText>
        <label>zRegistration (DEPRECATED)</label>
        <referenceTo>Registration__c</referenceTo>
        <relationshipLabel>Event Badges</relationshipLabel>
        <relationshipName>EventBadges</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Salutation__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Salutation</label>
        <picklist>
            <picklistValues>
                <fullName>Mr.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ms.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Mrs.</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Miss</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ShowEntityLogo__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(Registration2__r.Event2__r.EntityLogoOnBadgeEnabled__c,&apos;Yes&apos;,&apos;No&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Show Entity Logo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>State</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TEXT(OrderItemLine__r.Status__c)</formula>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Suffix__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Suffix</label>
        <picklist>
            <picklistValues>
                <fullName>II</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>III</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IV</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Jr</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sr</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Event Badge</label>
    <listViews>
        <fullName>All_Event_Badges</fullName>
        <columns>Event__c</columns>
        <columns>NAME</columns>
        <columns>Name__c</columns>
        <columns>BadgeClass__c</columns>
        <columns>BadgeType__c</columns>
        <columns>Price__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Event Badges</label>
    </listViews>
    <listViews>
        <fullName>All_Guest_Event_Badges</fullName>
        <columns>NAME</columns>
        <columns>Name__c</columns>
        <columns>Status__c</columns>
        <columns>BadgeType__c</columns>
        <columns>Price__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>BadgeType__c</field>
            <operation>equals</operation>
            <value>Guest</value>
        </filters>
        <label>All Guest Event Badges</label>
    </listViews>
    <listViews>
        <fullName>All_Registrant_Event_Badges</fullName>
        <columns>NAME</columns>
        <columns>Name__c</columns>
        <columns>Status__c</columns>
        <columns>BadgeType__c</columns>
        <columns>Price__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>BadgeType__c</field>
            <operation>equals</operation>
            <value>Registrant</value>
        </filters>
        <label>All Registrant Event Badges</label>
    </listViews>
    <nameField>
        <displayFormat>Event Badge {0000000}</displayFormat>
        <label>Event Badge Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Event Badges</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Registration2__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Company__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>BadgeClass__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>BadgeType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Price__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <excludedStandardButtons>New</excludedStandardButtons>
        <listViewButtons>EventBadgeLabels</listViewButtons>
        <lookupDialogsAdditionalFields>Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BadgeClass__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BadgeType__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Price__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BadgeType__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Price__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Name__c</searchFilterFields>
        <searchFilterFields>BadgeClass__c</searchFilterFields>
        <searchFilterFields>BadgeType__c</searchFilterFields>
        <searchFilterFields>Price__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchResultsAdditionalFields>Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>BadgeClass__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>BadgeType__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Price__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>BadgeClassRequired</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(TEXT(BadgeType__c))</errorConditionFormula>
        <errorDisplayField>BadgeClass__c</errorDisplayField>
        <errorMessage>The badge class is required</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>BadgeTypeReadOnly</fullName>
        <active>true</active>
        <errorConditionFormula>TEXT(PRIORVALUE(BadgeType__c)) != TEXT(BadgeType__c)</errorConditionFormula>
        <errorDisplayField>BadgeType__c</errorDisplayField>
        <errorMessage>Badge Type cannot be modified.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>BadgeTypeRequired</fullName>
        <active>true</active>
        <errorConditionFormula>ISBLANK(TEXT(BadgeType__c))</errorConditionFormula>
        <errorDisplayField>BadgeType__c</errorDisplayField>
        <errorMessage>The badge type is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CancelledReadOnly</fullName>
        <active>true</active>
        <errorConditionFormula>AND(PRIORVALUE(Status__c) = &apos;Cancelled&apos;,
         IsChanged(Status__c) = FALSE,
         OR(IsChanged(BadgeType__c),
                IsChanged(City__c),
                IsChanged(Company__c),
                IsChanged(Designation__c),
                IsChanged(FirstName__c),
                IsChanged(CasualName__c),
                IsChanged(LastName__c),
                IsChanged(MiddleName__c),
                IsChanged(ProfessionalTitle__c),
                IsChanged(Registration2__c),
                IsChanged(Salutation__c), 
                IsChanged(State__c),
                IsChanged(Suffix__c)
         )
)</errorConditionFormula>
        <errorMessage>Cancelled records cannot be modified.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RegistrationReadOnly</fullName>
        <active>true</active>
        <errorConditionFormula>NOT(ISNEW()) &amp;&amp; PRIORVALUE(Registration2__c) &lt;&gt; Registration2__c</errorConditionFormula>
        <errorDisplayField>Registration2__c</errorDisplayField>
        <errorMessage>Registration cannot be modified.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RegistrationRequired</fullName>
        <active>true</active>
        <description>The registration is required.</description>
        <errorConditionFormula>IsBlank(   Registration2__c  )</errorConditionFormula>
        <errorDisplayField>Registration2__c</errorDisplayField>
        <errorMessage>The registration is required.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>EventBadgeLabels</fullName>
        <availability>online</availability>
        <description>Generate badge labels for selected individuals.</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Event Badge Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>var ids = {!GETRECORDIDS( $ObjectType.EventBadge__c )};

if(ids.length &lt; 1 ) {
alert(&apos;Please select at least one badge.&apos;)
}
else {
var sfInstance = null;
var namespace = &apos;{!$Setup.Namespace__c.Prefix__c}&apos;;

if (window.location.host.indexOf(&apos;cloudforce&apos;) != -1) {
	var url = document.getElementById(&apos;timeoutlogo&apos;).value;
	var domain = url.match(/:\/\/([^\.]*)\./);
	sfInstance = window.location.host.split(&apos;.&apos;,1)[0] + &apos;--nu.&apos; + domain[1];
} else {
	var url = window.location.toString();
	var domain = url.match(/:\/\/([^\.]*)\./);
        if (namespace != &apos;&apos;) { sfInstance = &apos;nu.&apos; + domain[1]; }
        else { sfInstance = &apos;c.&apos; + domain[1]; }
}

this.form.action = &apos;https://&apos; + sfInstance + &apos;.visual.force.com/apex/&apos; + namespace + &apos;EventBadgeLabels&apos;;
this.form.onsubmit = function() { return true }
this.form.submit();
}</url>
    </webLinks>
</CustomObject>
