<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpEntityCrossovers</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Enables products from multiple entities to be sold within the same order with balancing inter-entity transactions.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>DueFromGLAccount__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Due-from GL Account. The GL Account specified must be tied to the Secondary Entity.</description>
        <externalId>false</externalId>
        <inlineHelpText>Due-from GL Account. The GL Account specified must be tied to the Secondary Entity.</inlineHelpText>
        <label>Due-from GL Account</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Due-from GL Account Entity must be the same as Crossover Secondary Entity.</errorMessage>
            <filterItems>
                <field>GLAccount__c.Entity__c</field>
                <operation>equals</operation>
                <valueField>$Source.SecondaryEntity__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Entity Crossovers (Due-from GL Account)</relationshipLabel>
        <relationshipName>EntityCrossoversDueFrom</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DueToGLAccount__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Due-to GL Account. The GL Account specified must be tied to the Primary Entity.</description>
        <externalId>false</externalId>
        <inlineHelpText>Due-to GL Account. The GL Account specified must be tied to the Primary Entity.</inlineHelpText>
        <label>Due-to GL Account</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Due-to GL Account Entity must be the same as Crossover Primary Entity.</errorMessage>
            <filterItems>
                <field>GLAccount__c.Entity__c</field>
                <operation>equals</operation>
                <valueField>$Source.PrimaryEntity__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Entity Crossovers (Due-to GL Account)</relationshipLabel>
        <relationshipName>EntityCrossoversDueTo</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>PrimaryEntity__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The primary entity that will receive payment for the order. Products from the secondary entity will be purchasable when on the primary entity&apos;s self service site (if Self Service Enabled) or when placing an order in staff view for this entity.</description>
        <externalId>false</externalId>
        <inlineHelpText>The primary entity that will receive payment for the order</inlineHelpText>
        <label>Primary Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Entity Crossovers</relationshipLabel>
        <relationshipName>PrimaryEntityCrossovers</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SecondaryEntity__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The entity whose products can also be purchased. Products from this entity will be purchasable when on the primary entity&apos;s self service site (if Self Service Enabled) or when placing an order in staff view for the primary entity.</description>
        <externalId>false</externalId>
        <inlineHelpText>The entity whose products can also be purchased</inlineHelpText>
        <label>Secondary Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Entity Crossovers (Secondary Entity)</relationshipLabel>
        <relationshipName>SecondaryEntityCrossovers</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SelfServiceEnabled__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, the secondary entity&apos;s products will be purchasable on the primary entity&apos;s self service site</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, the secondary entity&apos;s products will be purchasable on the primary entity&apos;s self service site</inlineHelpText>
        <label>Self Service Enabled</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Entity Crossover</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>PrimaryEntity__c</columns>
        <columns>SecondaryEntity__c</columns>
        <columns>DueToGLAccount__c</columns>
        <columns>DueFromGLAccount__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Entity Crossovers</label>
    </listViews>
    <listViews>
        <fullName>AllSSDisabledEntityCrossovers</fullName>
        <columns>NAME</columns>
        <columns>PrimaryEntity__c</columns>
        <columns>SecondaryEntity__c</columns>
        <columns>DueToGLAccount__c</columns>
        <columns>DueFromGLAccount__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SelfServiceEnabled__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>All SS Disabled Entity Crossovers</label>
    </listViews>
    <listViews>
        <fullName>AllSSEnabledEntityCrossovers</fullName>
        <columns>NAME</columns>
        <columns>PrimaryEntity__c</columns>
        <columns>SecondaryEntity__c</columns>
        <columns>DueToGLAccount__c</columns>
        <columns>DueFromGLAccount__c</columns>
        <columns>SelfServiceEnabled__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>SelfServiceEnabled__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>All SS Enabled Entity Crossovers</label>
    </listViews>
    <nameField>
        <displayFormat>Entity Crossover {0000000}</displayFormat>
        <label>Entity Crossover Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Entity Crossovers</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>PrimaryEntity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SecondaryEntity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DueToGLAccount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DueFromGLAccount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SelfServiceEnabled__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>PrimaryEntity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SecondaryEntity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DueToGLAccount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DueFromGLAccount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>SelfServiceEnabled__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>PrimaryEntity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SecondaryEntity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DueToGLAccount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DueFromGLAccount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SelfServiceEnabled__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>ExternalID__c</searchFilterFields>
        <searchFilterFields>PrimaryEntity__c</searchFilterFields>
        <searchFilterFields>SecondaryEntity__c</searchFilterFields>
        <searchFilterFields>DueToGLAccount__c</searchFilterFields>
        <searchFilterFields>DueFromGLAccount__c</searchFilterFields>
        <searchFilterFields>SelfServiceEnabled__c</searchFilterFields>
        <searchResultsAdditionalFields>PrimaryEntity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SecondaryEntity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DueToGLAccount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DueFromGLAccount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SelfServiceEnabled__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>DueFromGLEntityCrossoverSecondaryEntity</fullName>
        <active>true</active>
        <errorConditionFormula>DueFromGLAccount__r.Entity__c &lt;&gt; SecondaryEntity__c</errorConditionFormula>
        <errorMessage>Due-from GL Account Entity must be the same as Crossover Secondary Entity.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>DueToGLEntityCrossoverPrimaryEntity</fullName>
        <active>true</active>
        <errorConditionFormula>DueToGLAccount__r.Entity__c  &lt;&gt;  PrimaryEntity__c</errorConditionFormula>
        <errorMessage>Due-to GL Account Entity must be the same as Crossover Primary Entity.</errorMessage>
    </validationRules>
</CustomObject>
