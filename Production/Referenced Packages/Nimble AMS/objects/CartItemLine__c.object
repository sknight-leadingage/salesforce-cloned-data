<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>HelpOrders</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>The line items for a cart order.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>CartItem__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Cart Item that contains this Cart Item Line record</inlineHelpText>
        <label>Cart Item</label>
        <referenceTo>CartItem__c</referenceTo>
        <relationshipLabel>Cart Item Lines</relationshipLabel>
        <relationshipName>CartItemLines</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Data__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Used to serialize other objects.</inlineHelpText>
        <label>Data</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>ExternalId__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IsCommunityEditable__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Determines if this cart item line is editable in Community Hub.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines if this cart item line is editable in Community Hub.</inlineHelpText>
        <label>Is Community Editable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsCouponCartItemLine__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Is Coupon Cart Item Line</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsInCart__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field is a system field that is used to indicate if the order line has been checked in the order process. If it is checked, then it is considered purchased.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, then Cart Item Line is considered purchased.</inlineHelpText>
        <label>In Cart</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsShippable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Determines whether or not this product purchase is shippable.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines whether or not this product purchase is shippable.</inlineHelpText>
        <label>Shippable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsTaxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>When checked, this cart item line is considered taxable. By default, it derives its value from the product&apos;s taxable status.</description>
        <externalId>false</externalId>
        <inlineHelpText>When checked, this cart item line is considered taxable. By default, it derives its value from the product&apos;s taxable status.</inlineHelpText>
        <label>Taxable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MembershipTypeProductLink__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Where the value of OrderItemLine.MembershipTypeProductLink comes from.</description>
        <externalId>false</externalId>
        <label>Membership Type Product Link</label>
        <referenceTo>MembershipTypeProductLink__c</referenceTo>
        <relationshipLabel>Cart Item Lines</relationshipLabel>
        <relationshipName>CartItemLines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OrderItemLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Reference for order adjustment process</inlineHelpText>
        <label>Order Item Line</label>
        <referenceTo>OrderItemLine__c</referenceTo>
        <relationshipLabel>Cart Item Lines</relationshipLabel>
        <relationshipName>CartItemLines</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Cart Item Lines</relationshipLabel>
        <relationshipName>CartItemLines2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Cart Item Lines</relationshipLabel>
        <relationshipName>CartItemLines</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SubTotal__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Quantity__c *  UnitPrice__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Sub Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tax__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tax</label>
        <precision>11</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>SubTotal__c +  Tax__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>11</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Cart Item Line</label>
    <nameField>
        <displayFormat>Cart Item Line {0000000}</displayFormat>
        <label>Cart Item Line Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Cart Item Lines</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>CheckInventoryOnHand</fullName>
        <active>true</active>
        <errorConditionFormula>AND(Product2__r.TrackInventory__c,
    Product2__r.RecordTypeName__c != &apos;Merchandise&apos; || !$Setup.NimbleAMSPublicSettings__c.CanBackorderStaffView__c,
    Quantity__c - IF(ISNEW(), 0, PRIORVALUE(Quantity__c)) &gt; Product2__r.InventoryOnHand__c)</errorConditionFormula>
        <errorMessage>Quantity exceeds inventory on hand.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EntityMustMatch</fullName>
        <active>true</active>
        <errorConditionFormula>Product2__r.Entity__c &lt;&gt; IF(ISBLANK(CartItem__r.Entity__c),CartItem__r.Cart__r.Entity2__c,CartItem__r.Entity__c)</errorConditionFormula>
        <errorMessage>The product entity must match the cart item&apos;s entity.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>PriceMustBePositive</fullName>
        <active>false</active>
        <description>The unit price can not be negative.</description>
        <errorConditionFormula>UnitPrice__c &lt; 0 &amp;&amp; Product2__r.RecordType.Name != &apos;Coupon&apos;</errorConditionFormula>
        <errorMessage>The unit price can not be negative.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>QuantityMustBePositive</fullName>
        <active>true</active>
        <errorConditionFormula>Quantity__c &lt;= 0</errorConditionFormula>
        <errorMessage>The quantity must be greater than zero.</errorMessage>
    </validationRules>
</CustomObject>
