<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Reset_Last_Processed_Date</fullName>
        <field>KWD_Last_Processed_DT__c</field>
        <name>Reset Last Processed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Reset Processed Date %28Contact%29</fullName>
        <actions>
            <name>Reset_Last_Processed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(  ISCHANGED( MailingStreet),  ISCHANGED( MailingCity),  ISCHANGED( MailingState),  ISCHANGED( MailingPostalCode)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
