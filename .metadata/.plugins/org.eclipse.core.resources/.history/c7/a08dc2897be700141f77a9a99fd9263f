public without sharing class AccountTriggerSubscriptionsHandler extends NU.TriggerHandlersBase
{
    //[SK] Yes, 'without sharing' is intentional, argle bargle triggers do as trigger does.
    
    //onBeforeUpdate handles change of email address (flips Held subs back to Active)
    //onAfterUpdate handles general Subscription Logic
    //onAfterDelete handles subs as related to the Merge Process
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {
        Set<Id> aid = new Set<Id>();
        List<Account> al = newRecordMap.values();
        for (Account a : al)
        {
            Account beforeUpdate = (Account)oldRecordMap.get(a.Id);
            if (beforeUpdate.PersonEmailBouncedDate != null && a.PersonEmailBouncedDate == null) {
                aid.add(a.Id);
            }
            else if (beforeUpdate.PersonEmail != a.PersonEmail) {
                a.PersonEmailBouncedDate = null;
                a.PersonEmailBouncedReason = null;
                aid.add(a.Id);
            }
        }
        if (aid.size() > 0) {
            List<Online_Subscription__c> asubs = [SELECT Id, Status__c, System_End_Date__c FROM Online_Subscription__c WHERE Account__r.Id IN :aid AND Status__c = 'Held'];
            for(Online_Subscription__c asub : asubs) {
                asub.System_End_Date__c = null;
                asub.Status__c = 'Active';
            }
            update asubs;
        }
    }
    
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap)
    {
        if (trigger.isUpdate){
    
            List<Account> NewMembers = new List<Account>();
            List<Account> ExpiredMembers = new List<Account>();
            List<Account> oldTriggerAccounts = oldRecordMap.values();
    
            for (Account oldAccount: oldTriggerAccounts) {
                Account newAccount = (Account)newRecordMap.get(oldAccount.Id);               
                
                if (oldAccount.LeadingAge_Member_Individual__c == Constant.LEADINGAGE_NONMEMBER  && newAccount.LeadingAge_Member_Individual__c == Constant.LEADINGAGE_MEMBER && newAccount.IsPersonAccount){
                    NewMembers.add(newAccount);
                }
                
                if (oldAccount.LeadingAge_Member_Individual__c == Constant.LEADINGAGE_MEMBER && newAccount.LeadingAge_Member_Individual__c == Constant.LEADINGAGE_NONMEMBER && newAccount.IsPersonAccount){
                    ExpiredMembers.add(newAccount);
                }                                 
            }
    
            if (NewMembers.size() > 0){
    
                
                List<NU__Product__c> subs = [SELECT Id FROM NU__Product__c WHERE Membership_Default__c = true];
                List<Online_Subscription__c> MemberSubs = new List<Online_Subscription__c>(); 
                //List<Online_Subscription__c> DeleteSubs = new List<Online_Subscription__c>();
                Set<Id> memberDefaultExisting = new Set<Id>(); 
                            
                List<Account> myNewMembers = [Select id, Name, (Select id, Status__c, Start_Date__c, End_Date__c, System_End_Date__c, Product__c, Product__r.Id, Product__r.Membership_Default__c from Online_Subscriptions__r ) From Account where id in :NewMembers];
                
                DateTime dtNow = DateTime.Now();
                
                 for(Account a: myNewMembers){
                     Boolean alreadyDeletedSubs = false;                 
                    
                     memberDefaultExisting.clear();                  
                     
                     /**
                        This loop runs for people who have at least one subscription
                        We're removing the ability to delete subs and member defaults get added by default so those are processed here 
                     **/
                     for(Online_Subscription__c userSub: a.Online_Subscriptions__r) {
                        if (userSub.System_End_Date__c != null && userSub.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE &&  userSub.System_End_Date__c.addDays(7) >= dtNow) {
                            Online_Subscription__c newSub = new Online_Subscription__c(Account__c = a.Id, Product__c = userSub.Product__r.Id, Id = userSub.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, End_Date__c = userSub.End_Date__c, System_End_Date__c = null);
                            if (userSub.System_End_Date__c == userSub.End_Date__c) {
                                newSub.End_Date__c = null;
                            }
                            MemberSubs.add(newSub);
                        }
                        
                        if (userSub.Product__r.Membership_Default__c == true) {
                            memberDefaultExisting.add(userSub.Product__r.Id);
                        }
                     }
                     
                     /** If you're a new member or somehow didn't have any of the default subs, we add the member defaults here**/ 
                     if (subs.size() > memberDefaultExisting.size()) {
                        for(NU__Product__c sub: subs){
                            if (!memberDefaultExisting.contains(sub.Id)) {
                                Online_Subscription__c newSub = new Online_Subscription__c(Account__c = a.Id, Product__c = sub.Id);
                                MemberSubs.add(newSub);
                            }
                        }
                     }
                 }
                
                //if (DeleteSubs.size() > 0) { delete DeleteSubs;}              
                if (MemberSubs.size() > 0) { upsert MemberSubs; }
            }
            
            if (ExpiredMembers.size() > 0){                       
                List<Online_Subscription__c> ExpiredSubs = new List<Online_Subscription__c>();
                string sA = Constant.SUBSCRIPTION_STATUS_ACTIVE;
                //List<Account> oldAccounts = [Select id, Name, (Select id, Status__c, End_Date__c from Online_Subscriptions__r WHERE Product__r.Membership_Restricted__c = true AND Status__c = :sA) From Account where id in :ExpiredMembers];
                
                //Unsubscribe ALL currently active subscriptions (not only member benefits)
                List<Account> oldAccounts = [Select id, Name, (Select id, Status__c, End_Date__c from Online_Subscriptions__r WHERE Status__c = :sA) From Account where id in :ExpiredMembers];
                
                for (Account a : oldAccounts){
                    List<Online_Subscription__c> mySubs = a.Online_Subscriptions__r;//[SELECT Id FROM Online_Subscription__c WHERE Product__r.Membership_Restricted__c = true AND Account__c = :a.id];
                    DateTime dtNow = DateTime.Now();                                
                    for(Online_Subscription__c sub : mySubs){
                        Online_Subscription__c existingSub = new Online_Subscription__c(id = sub.id, status__c = Constant.SUBSCRIPTION_STATUS_INACTIVE, End_Date__c = sub.End_Date__c, System_End_Date__c = dtNow);
                        if (existingSub.End_Date__c == null || existingSub.End_Date__c < dtNow) {
                            existingSub.End_Date__c = dtNow;
                        }
                        ExpiredSubs.add(existingSub);
                    }
                }
                
                if (ExpiredSubs.size() > 0) { update ExpiredSubs; }                   
           }
            
        }
    }
    
    public override void onAfterDelete(Map<Id, sObject> oldRecordMap) {
        //See: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_triggers_merge_statements.htm
        List<Account> oldTriggerAccounts = oldRecordMap.values();
        Set<Id> winningIds = new Set<Id>();
        for(Account winningId : oldTriggerAccounts) {
            if (winningId.MasterRecordId != null) {
                winningIds.add(winningId.MasterRecordId);
            }
        }

        if (winningIds.size() > 0) {
            List<Online_Subscription__c> toDelete = new List<Online_Subscription__c>();
            List<Online_Subscription__c> toUpdate = new List<Online_Subscription__c>();
            List<Online_Subscription__c> subsCurrent = [SELECT Id, Account__c, Product__c, Product__r.Id, Start_Date__c, Status__c, End_Date__c, System_End_Date__c FROM Online_Subscription__c WHERE Account__c = :winningIds ORDER BY Product__c, Start_Date__c DESC];
 
            integer subI = 0;
            Id lastProdId = null;
            for(Online_Subscription__c subCurrent : subsCurrent) {
                if (lastProdId != null && lastProdId == subCurrent.Product__r.Id) {
                    subI++;
                }
                else {
                    subI = 0;
                    if (subCurrent.Status__c == 'Held') {
                        toUpdate.add(subCurrent); //Defer this for later so that dupes are deleted before we try to update (Validation reasons)
                    }
                }
                
                if (subI > 0) {
                    toDelete.add(subCurrent);
                }

                lastProdId = subCurrent.Product__r.Id;
            }

            //Delete before update (Validation reasons)
            if (toDelete.size() > 0) {
                delete toDelete;
            }

            if (toUpdate.size() > 0) {
                for(Online_Subscription__c subUpdate : toUpdate) {
                    subUpdate.Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE;
                    subUpdate.End_Date__c = null;
                    subUpdate.System_End_Date__c = null;
                }
                update toUpdate;
            }
        }
    }
    
}