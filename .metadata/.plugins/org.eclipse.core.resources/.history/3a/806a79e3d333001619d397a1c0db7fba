<apex:page controller="CreateCompany_Controller">
<apex:stylesheet value="{!URLFOR($Resource.LeadingAgeStyle, 'LeadingAge.css')}"/>
 
<apex:pageMessages id="msg" />
 <apex:form >
      
    <apex:outputText value="{!strDebug}"/>
    <apex:pageBlock mode="maindetail">
        <apex:outputText styleClass="title" value="Add a {!GetMSOorMCDescription}"/>        
        
        <apex:outputPanel rendered="{!IF(WizardStep=1,true,false)}">
            <p>Are you adding a new MSO, Management Company or a Provider?</p>
            <apex:selectRadio value="{!companyTypes}" label="Please select a Company Type from the list" required="true">
                <apex:selectOptions value="{!companyTypesItems}"></apex:selectOptions>
            </apex:selectRadio>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(WizardStep=2,true,false)}">
            <apex:PageBlockSection columns="1">
            <apex:facet name="header">
                 <span class="header">{!GetMSOorMCDescription} Information</span>
             </apex:facet> 
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!GetMSOorMCDescription} Name" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputText value="{!sNewMSOMCName}" size="50"  label="Please enter MSO Name" required="true"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
             <br/>
              <strong>{!GetMSOorMCDescription} Address</strong>
              <apex:inputfield value="{!newMSOMC.BillingStreet}" required="true"/>
              <apex:inputfield value="{!newMSOMC.BillingCity}" required="true"/>
              <!-- <apex:inputfield value="{!newProvider.BillingState}"/> -->
              <apex:pageblockSectionItem >
                  <apex:outputLabel value="State"></apex:outputLabel>               
                  <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <c:SelectAddressState ValueIn="{!newMSOMC}" required="true" label="Please select a State from the list"></c:SelectAddressState>
                    </div>
                </apex:outputPanel>
              </apex:pageblockSectionItem>
              <apex:inputfield value="{!newMSOMC.BillingPostalCode}" required="true"/>
            </apex:PageBlockSection>
            
            <apex:PageBlockSection columns="1">
            <apex:facet name="header">
                 <span class="header">Primary Contact Information</span>
             </apex:facet> 
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="First Name" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputText value="{!sMFirstName}" label="Please enter Primary Contact first name" required="true"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Last Name" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputText value="{!sMLastName}" label="Please enter Primary Contact last name" required="true" />
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
              
          <apex:pageBlockSectionItem >
                <apex:outputLabel value="Email Address" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>  
                          <apex:inputText value="{!sMPersonEmailAddress}" label="Please enter Primary Contact email address" required="true"/>
                     </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
              
              <apex:inputfield value="{!newMContact.Phone}" required="true" />
              <apex:inputfield value="{!newMContact.PersonTitle}" required="true"/>
              <apex:inputfield value="{!newMContact.Operational_Level__pc}" required="true"/>
              <apex:inputfield value="{!newMContact.Organizational_Function_Expertise__pc}" required="true"/>  
              
            </apex:PageBlockSection>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(WizardStep=3,true,false)}">
            <p>Congratulations!  You've added a new {!GetMSOorMCDescription}.  Do you want attach provider sites to this {!GetMSOorMCDescription}?</p>
            <apex:selectRadio value="{!optYesNoSelected}" label="Do you want to attach provider sites?" required="true">
                <apex:selectOption itemValue="1" itemLabel="Yes" />
                <apex:selectOption itemValue="2" itemLabel="No" />
            </apex:selectRadio>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(WizardStep=4,true,false)}">
            <p>Does this provider belong to an MSO or a Management Company?</p>
            <apex:selectRadio value="{!optChooseMSO}" label="Please indicate whether the provider belongs to an MSO or a Management Company" required="true">
                <apex:selectOption itemValue="1" itemLabel="Yes"/>
                <apex:selectOption itemValue="2" itemLabel="No"/>
            </apex:selectRadio>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!IF(WizardStep=5,true,false)}" id="step2">     
            <br/>
            Search for your MSO/MC by entering a search criteria.  Double click the select button from the list to make your selection.    
            <br/><br/>
            <apex:inputText value="{!sFindMSOSearchText}" /> <apex:commandButton action="{!performSearch}" value="Search" />
            <apex:PageBlockTable value="{!dsMSOList}" var="a" >
            <apex:column headervalue="Select"  width="50px">
                  <apex:outputtext value="Select" />
                  <apex:actionSupport action="{!selectRow}" event="onclick" rerender="step2">
                    <apex:param name="selAccId" value="{!a.Id}" assignTo="{!selectedMSO_Id}" />
                    <apex:param name="selAccName" value="{!a.Name}" assignTo="{!selectedMSO_Name}" />
                </apex:actionSupport>    
              </apex:column>
              <apex:column headervalue="Name" width="400px">
                  <apex:outputtext value="{!a.Name}"  />
              </apex:column>
              <apex:column headervalue="Type" width="100px">
                  <apex:outputtext value="{!IF(a.Type != '',a.Type,'Multi-Site Organization')}"  />
              </apex:column>
                <apex:column headervalue="Address"  width="140px">
                  <apex:outputtext value="{!a.BillingStreet}" />
              </apex:column>
              <apex:column headervalue="City"  width="75px">
                  <apex:outputtext value="{!a.BillingCity}" />
              </apex:column>
               <apex:column headervalue="State"  width="50px">
                  <apex:outputtext value="{!a.BillingState}" />
              </apex:column>   
              <apex:column headervalue="Zip"  width="50px">
                  <apex:outputtext value="{!a.BillingPostalCode}" />
              </apex:column>  
               <apex:column headervalue="Status"  width="50px">
                  <apex:outputtext value="{!a.LeadingAge_Member_Company__c}" />
              </apex:column>            
             
                
            </apex:PageBlockTable>
            
            <br/><br/>
            <span class="title"><b><i>Selected MSO:</i></b>&nbsp;&nbsp;<apex:outputtext value="{!selectedMSO_Name}"  /></span>            
            <br/>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!IF(WizardStep=6,true,false)}">
            <apex:PageBlockSection columns="1">
            <apex:facet name="header">
                 <span class="header">Provider Information</span>
             </apex:facet> 
             <!--<apex:inputfield value="{!newProvider.Name}"/>-->
            <apex:pageBlockSectionItem rendered="{!IF(selectedMSO_Id == null,false,true)}">
                <apex:outputLabel value="Parent Company" />
                <apex:outputText value="{!selectedMSO_Name}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Provider Name" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputText value="{!sNewProviderName}" size="40" label="Please enter Provider Name" required="true"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
             <br/>
              <strong>Provider Address</strong>
              <apex:inputfield value="{!newProvider.BillingStreet}" required="true" />
              <apex:inputfield value="{!newProvider.BillingCity}" required="true" />
              <!-- <apex:inputfield value="{!newProvider.BillingState}"/> -->
              <apex:pageblockSectionItem >
                  <apex:outputLabel value="State"></apex:outputLabel>               
                  <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <c:SelectAddressState ValueIn="{!newProvider}" required="true" label="Please select a State from the list"></c:SelectAddressState>
                    </div>
                </apex:outputPanel>
              </apex:pageblockSectionItem>
              <apex:inputfield value="{!newProvider.BillingPostalCode}"  required="true"/>
            </apex:PageBlockSection>
            
            <apex:PageBlockSection columns="1">
            <apex:facet name="header">
                 <span class="header">Primary Contact Information</span>
             </apex:facet> 
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="First Name" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputText value="{!sFirstName}" label="Please enter Primary Contact first name" required="true"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Last Name" />
                <apex:outputPanel >
                    <div class="requiredInput">
                        <div class="requiredBlock"></div>
                        <apex:inputText value="{!sLastName}" label="Please enter Primary Contact last name" required="true"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
              
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Email Address" />
                <apex:outputPanel >
                    <div class="requiredInput">
                     <div class="requiredBlock"></div>
                          <apex:inputText value="{!sPersonEmailAddress}" label="Please enter Primary Contact email address" required="true"/>
                      </div>
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
              
              <apex:inputfield value="{!newContact.Phone}" required="true"/>
              
              <apex:inputfield value="{!newContact.PersonTitle}" required="true"/>
              <apex:inputfield value="{!newContact.Operational_Level__pc}" required="true"/>
              <apex:inputfield value="{!newContact.Organizational_Function_Expertise__pc}" required="true"/>              
              
            </apex:PageBlockSection>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(WizardStep=7,true,false)}">
            
            <br/><br/>
            <!-- Added Extra set of buttons so user can go back on validation messages -->
            <apex:commandButton action="{!WizardBack}" value="Back" rendered="{!IF(WizardStep > 1 && WizardStep != WizardMax,true,false)}" immediate="true" />    
            <apex:commandButton action="{!WizardNext}" value="Next" rendered="{!IF(WizardStep < WizardMax-1,true,false)}"/>
            <apex:commandButton action="{!WizardNext}" value="Save" rendered="{!IF(WizardStep == WizardMax-1,true,false)}"/>
            <br/><br/>
            
            <apex:PageBlockSection title="Program Revenue" columns="1">
            <apex:facet name="header">
                 <span class="header">Program Revenue</span>
             </apex:facet>  
              <apex:inputfield value="{!newProvider.Program_Service_Revenue__c}" required="true"/>
              <apex:inputfield value="{!newProvider.Revenue_Year_Submitted__c}" required="true"/> 
              <apex:inputCheckbox value="{!newProvider.Under_Construction__c}"/> 
              <apex:inputfield value="{!newProvider.Provider_Join_On__c}" required="true"/>           
            </apex:PageBlockSection>            
            
            <apex:PageBlockSection title="General Provider Info" columns="2">  
            <apex:facet name="header">
                 <span class="header">General Provider Info</span>
             </apex:facet>              
              <apex:inputfield value="{!newProvider.National_Provider_Id__c}"/>       
              <apex:inputfield value="{!newProvider.Number_Of_Clients__c}"/>  
              <apex:inputfield value="{!newProvider.Number_Of_Volunteers__c}"/>
              <apex:inputCheckbox value="{!newProvider.For_Profit__c}"/>
              <apex:inputfield value="{!newProvider.Number_Of_Nursing_Beds__c}"/>
              <apex:inputfield value="{!newProvider.Religious_Affiliation__c}"/> 
              <apex:inputfield value="{!newProvider.NumberOfEmployees}"/>      
              <apex:inputfield value="{!newProvider.Sponsored_By__c}"/> 
              <apex:inputCheckbox value="{!newProvider.Rural_Provider__c}"/>   
              <apex:inputfield value="{!newProvider.Sponsor_Type__c}"/> 
              <apex:inputCheckbox value="{!newProvider.Hospital_Based__c}"/>                    
              <apex:inputCheckbox value="{!newProvider.County_Owned__c}"/>               
              <apex:inputCheckbox value="{!newProvider.University_Based__c}"/>  
              <apex:inputCheckbox value="{!newProvider.Village__c}"/>                                                        
            </apex:PageBlockSection>
            
            
            <apex:PageBlockSection title="Provider Type" columns="2">      
                   <apex:inputField value="{!newProvider.Provider_Type__c}" required="true"/>
            </apex:PageBlockSection>  

            <apex:PageBlockSection title="Assisted Living" columns="1">
              <apex:inputCheckbox value="{!newProvider.Accepts_Medicaid__c}"/>  
              <apex:inputfield value="{!newProvider.Number_Of_Assisted_Living_Residents__c}"/>
              <apex:inputfield value="{!newProvider.Number_Of_Assisted_Living_Units__c}"/>  
              <apex:inputField value="{!newProvider.AL_Program_Services__c}"/>                      
            </apex:PageBlockSection> 

            <apex:PageBlockSection title="CCRC" columns="1">
              <apex:inputfield value="{!newProvider.Number_Of_Buildings__c}"/>  
              <apex:inputfield value="{!newProvider.CCRC_Program_Services__c}"/>                            
            </apex:PageBlockSection> 
            
            
            <apex:PageBlockSection title="Nursing Care" columns="2">            
              <apex:inputfield value="{!newProvider.SNF_Dual_Certified_Beds__c}"/>
              <apex:inputfield value="{!newProvider.SNF_Medicaid_Beds__c}"/>     
              <apex:inputfield value="{!newProvider.SNF_Medicare_Beds__c}"/>
              <apex:inputCheckbox value="{!newProvider.SNF_Medicare_Certified__c}"/>  
              <apex:inputCheckbox value="{!newProvider.SNF_Medicaid_Certified__c}"/>    
              <apex:inputfield value="{!newProvider.SNF_Total_License_Beds__c}"/>
              <apex:inputfield value="{!newProvider.Nursing_Program_Services__c}"/>                          
            </apex:PageBlockSection> 
                                                
            
            <apex:PageBlockSection title="Senior Housing" columns="2">  
              <apex:inputfield value="{!newProvider.Total_Housing_Units__c}"/>
              <apex:inputfield value="{!newProvider.Total_Number_Of_HUD_Units__c}"/>             
              <apex:inputCheckbox value="{!newProvider.Rent_Subsidized__c}"/>
              <apex:inputCheckbox value="{!newProvider.Funding_Source__c}"/>
              <apex:inputfield value="{!newProvider.ILU_Fed_Subsidized__c}"/>  
              <apex:inputfield value="{!newProvider.ILU_Tax_Credit_Income_Rest__c}"/>  
              <apex:inputfield value="{!newProvider.ILU_Market_Rate__c}"/>    
              <apex:inputfield value="{!newProvider.ILU_Other__c}"/>   
              <apex:inputfield value="{!newProvider.HUD_Type__c}"/>     
              <apex:inputfield value="{!newProvider.Housing_Program_Services__c}"/>              
            </apex:PageBlockSection> 
            
           <apex:PageBlockSection title="HCBS" columns="1">
              <apex:inputfield value="{!newProvider.Other_Services_Description__c}"/>  
              <apex:inputfield value="{!newProvider.HCBS_Program_Services__c}"/>                            
            </apex:PageBlockSection> 
                       
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(WizardStep=8,true,false)}">            
            <apex:outputPanel >
                <p>Congratulation. You have successfully added a new company to the system.<br/><br/>
                    To add another company click <a href="/apex/CreateCompany">here</a>.            
                </p>
            </apex:outputPanel>
        </apex:outputPanel>
    </apex:pageBlock>

    <apex:commandButton action="{!WizardBack}" value="Back" rendered="{!IF(WizardStep > 1 && WizardStep != WizardMax,true,false)}" immediate="true" />    
    <apex:commandButton action="{!WizardNext}" value="Next" rendered="{!IF(WizardStep < WizardMax-1,true,false)}"/>
    <apex:commandButton action="{!WizardNext}" value="Save" rendered="{!IF(WizardStep == WizardMax-1,true,false)}"/>
 </apex:form>
</apex:page>