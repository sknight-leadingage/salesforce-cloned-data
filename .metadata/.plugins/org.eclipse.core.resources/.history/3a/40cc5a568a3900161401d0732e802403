//nightly bulk update the Current_Year_Priority_Points__c field on all account records, according to LeadingAge exhibitor points rules.

//run as a batch apex job because it affects more than 100 accounts. 

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new BatchUpdateCurrentYearAwardedPoints());

//To schedule nightly in an anonymous window in Eclipse or Developer Console:
//BatchUpdateCurrentYearAwardedPoints.schedule();

//To unschedule, delete the job from the list of scheduled jobs.

//Developed by NimbleUser, 2013 (MG, NF)

global class BatchUpdateCurrentYearAwardedPoints implements Database.Batchable<sObject>, Schedulable
{

    //batch apex jobs require three methods: start, execute, and finish; run in that order
    //schedulable batch apex jobs require one method: execute

    //batch apex methods:
    
    global final String Query = 'SELECT ID, IsPersonAccount, NU__Member__c, Provider_Member__c, Current_Year_Exhibitor__c, Current_Year_Priority_Points__c' +
            ' FROM Account WHERE IsPersonAccount = false';
   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        
        //retrieve the id of the current event
        Integer memberYear = Date.today().year();
        NU__Event__c evt = null;
        List<NU__Event__c> temp = [SELECT Id, NU__StartDate__c FROM NU__Event__c WHERE Current_Year_Annual_Event__c = true limit 1];
        if(temp != null && temp.size() > 0)
        {
			evt = temp[0];
			memberYear = evt.NU__StartDate__c.year();
        }
        else {
        	throw new ApplicationException('No \'Current Year Annual Event\' event could be found.  Please review the Events list and make sure an event is flagged as the current Exhibitor event.');
        }
        
        Integer Year = evt.NU__StartDate__c.year();
        Year -= 1;
        
        List<Priority_Points_Exhibitor_Archive__c> PPEA = [SELECT PPEA_Account__r.Id, Event_Start_Date__c From Priority_Points_Exhibitor_Archive__c Where CALENDAR_YEAR(Event_Start_Date__c) = :Year LIMIT 2];
        // Checking if record of previous year exists or not
        if ((PPEA == null || (PPEA != null && PPEA.size() == 0)))
        {
        	 throw new ApplicationException('There is no Priority Point Exhibitor record from last year');
        }


        //build a list of accounts to review
        List<ID> accountIds = new List<ID>();
        for(sobject s : scope)
        {
            accountIds.add(s.Id);
        }

        //map account id to the sum of exhibitor points for each account
        AggregateResult[] groupedResults = null;
        Map<ID, Decimal> priorityPointMap = new Map<Id, Decimal>();
		Map<Id, Boolean> accountIsMemberMap = new Map<Id, Boolean>();
        if(evt != null)
        {
            groupedResults = [SELECT Account__c, SUM(Exhibitor_Priority_Points_Calculated__c) FROM Exhibitor__c 
                               WHERE Event__c = :evt.Id and Status__c != 'Cancelled' and Account__c in :accountIds
                               GROUP BY Account__c];
        
            for (AggregateResult ar : groupedResults)  
            {
                priorityPointMap.put((ID)ar.get('Account__c'), (Decimal)ar.get('expr0'));
            }
            
            List<Account> accountMemberships = [SELECT Id,
            	(SELECT Id, NU__StartDate__c, NU__EndDate__c
            		FROM NU__Memberships__r
            		WHERE NU__Status__c != 'Cancelled' AND Grants_National_Membership__c = true AND CALENDAR_YEAR(NU__StartDate__c) = :memberYear)
            	FROM Account WHERE Id = :accountIds];
            
            for (Account aWM : accountMemberships) {
            	if (aWM.NU__Memberships__r != null && aWM.NU__Memberships__r.size() > 0) {
					accountIsMemberMap.put(aWM.Id, true);
            	}
            	else {
            		accountIsMemberMap.put(aWM.Id, false);
            	}
            	
            }
        }
        
        //apply additional points and store final result in a list to update
        List<Account> accounts = new List<Account>();
        
        List<Account> scopeAccounts = (List<Account>)scope;

        for(Account a : scopeAccounts)
        {
            Decimal points = 0;
             
            //members get one point
            if (accountIsMemberMap.containsKey(a.Id) && accountIsMemberMap.get(a.Id) == true)
            { 
                points += 1.0;
            }
            
            //add in exhibitor points from map (floor space calcuations)
            if(priorityPointMap.containsKey(a.Id))
            {
                points += priorityPointMap.get(a.Id);
                a.Current_Year_Exhibitor__c = true;
            }
            else
            {
                a.Current_Year_Exhibitor__c = false;
            }
             
            //current exhibitors get one point
            if(a.Current_Year_Exhibitor__c)
            {
                points += 1.0;
            }

            //store temporary result in account.Current_Year_Priority_Points__c                        
            if(points != a.Current_Year_Priority_Points__c)
            {
                a.Current_Year_Priority_Points__c = points.intValue();
                accounts.add(a);
            }

       }
        
        //update accounts
        if(accounts.size() > 0)
        {
            update accounts;
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        //do nothing upon finish. could send an email upon success or failure, if desired.
    }

    //scheduled batch apex methods:

    //to schedule daily at 3am
    public static void schedule() {
        System.schedule('Exhibitor Priority Points Nightly Batch Update 3am', '0 0 3 * * ?', new BatchUpdateCurrentYearAwardedPoints());
    }

    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }

}