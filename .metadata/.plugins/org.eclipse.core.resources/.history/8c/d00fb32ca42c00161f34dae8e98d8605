//annually bulk update all Priority Points fields on all account records, according to LeadingAge exhibitor points rules.
//adds the current year's points value to the previous years' points value, then clears all current year fields for the upcoming year 

//run as a batch apex job because it affects more than 100 accounts. 

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new BatchUpdatePreviousYearAwardedPoints());

//To schedule in an anonymous window in Eclipse or Developer Console:
//BatchUpdatePreviousYearAwardedPoints.schedule();
//but it is preferable to only run this on demand, since it only happens once a year

//To unschedule, delete the job from the list of scheduled jobs.

//Developed by NimbleUser, 2013 (MG, NF)

//October 2015, renamed from BatchUpdatePreviousYearAwardedPoints
//
global class BatchUpdateAndArchiveYearAwardedPoints implements Database.Batchable<sObject>, Schedulable
{
    //batch apex jobs require three methods: start, execute, and finish; run in that order
    //schedulable batch apex jobs require one method: execute

	//batch apex methods:

    global final String Query = 'SELECT ID, LeadingAge_ID__c, Total_Priority_Points__c, Current_Year_Exhibitor__c, Current_Year_Priority_Points__c, Current_Year_Priority_Point_Adjustments__c, Previous_Years_Priority_Points__c FROM Account WHERE IsPersonAccount = false';
   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	
    	NU__Event__c CurrentEvent = [Select Id, Name, NU__StartDate__c from NU__Event__c where Current_Year_Annual_Event__c = true limit 1];
    	List<Priority_Points_Exhibitor_Archive__c> PPEAEvent = [Select Id From Priority_Points_Exhibitor_Archive__c where Event__c = :CurrentEvent.Id LIMIT 2];
    	
    	if (PPEAEvent != null && PPEAEvent.size() > 0) {
    		throw new ApplicationException('The Priority Points for event "' + CurrentEvent.Name + '" have already been archived!');
    	}
    	
       return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
       List<Account> accounts = new List<Account>();
       List<Account> accountsScope = new List<Account>();
       
       NU__Event__c CurrentEvent = [Select Id, NU__StartDate__c from NU__Event__c where Current_Year_Annual_Event__c = true limit 1];
       List<Priority_Points_Exhibitor_Archive__c> PPEAupdates = new List<Priority_Points_Exhibitor_Archive__c>();
       
       accountsScope =  (List<Account>)scope;
       for(Account a : accountsScope)
       {
			// Arhcive line
			Priority_Points_Exhibitor_Archive__c PPEA = new Priority_Points_Exhibitor_Archive__c();
			//preset account values to zero, when null
            if(a.Current_Year_Priority_Points__c == null)
            {
                a.Current_Year_Priority_Points__c = 0;
            }
            
            if(a.Current_Year_Priority_Point_Adjustments__c == null)
            {
                a.Current_Year_Priority_Point_Adjustments__c = 0;
            }
            
            if(a.Previous_Years_Priority_Points__c == null)
            {
                a.Previous_Years_Priority_Points__c = 0;
            }
            // Archiving Current year information
            
            PPEA.PPEA_Account__c = a.Id;
            PPEA.Current_Year_Priority_Point_Adjustments__c = a.Current_Year_Priority_Point_Adjustments__c;
            PPEA.Current_Year_Priority_Points__c = a.Current_Year_Priority_Points__c;
            PPEA.Event__c = CurrentEvent.Id;
            PPEA.Event_Start_Date__c = date.newinstance(CurrentEvent.NU__StartDate__c.year(),CurrentEvent.NU__StartDate__c.month(),CurrentEvent.NU__StartDate__c.day());
            PPEA.LeadingAge_ID__c = a.LeadingAge_ID__c;
            PPEA.Previous_Years_Priority_Points__c = a.Previous_Years_Priority_Points__c;
            PPEA.Total_Priority_Points__c = a.Total_Priority_Points__c;
			//add the current year's points value to the previous years' points value, then clear all current year fields for the upcoming year 
            if(a.Current_Year_Priority_Points__c != 0 || a.Current_Year_Priority_Point_Adjustments__c != 0 || a.Current_Year_Exhibitor__c)
            {
                a.Current_Year_Exhibitor__c = false;
                a.Previous_Years_Priority_Points__c = a.Previous_Years_Priority_Points__c + a.Current_Year_Priority_Points__c + a.Current_Year_Priority_Point_Adjustments__c;
                a.Current_Year_Priority_Points__c = 0;
                a.Current_Year_Priority_Point_Adjustments__c = 0;
                accounts.add(a);
            }
            
            PPEAupdates.add(PPEA);
       }

		//update accounts
       if(accounts.size() > 0)
       {
            update accounts;
       }
       
       //update archive
       if(PPEAupdates.size() > 0)
       {
            insert PPEAupdates;
       }
   }

   global void finish(Database.BatchableContext BC)
   {
		//do nothing upon finish. could send an email upon success or failure, if desired.
   }

	//scheduled batch apex methods:

	//to schedule daily at 3am (disabled because this is an annual job)
	public static void schedule() {
		//System.schedule('Exhibitor Priority Points Annual Batch Update', '0 0 3 * * ?', new BatchUpdatePreviousYearAwardedPoints());
	}

    global void execute(SchedulableContext context) {
        Database.executeBatch(this, 100);
    }
}