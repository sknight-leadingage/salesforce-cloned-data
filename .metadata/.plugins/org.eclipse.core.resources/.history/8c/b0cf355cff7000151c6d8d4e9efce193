public class AccountTriggerHandlers extends NU.TriggerHandlersBase {
    Id multiSiteOrgRecordTypeId = Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID;
    
    static List<Account> insertedIndividualsInContext = new List<Account>();
    static Set<Id> statesCanEditAccountIds = new Set<Id>();
    static boolean isExecuting = false;

    private User currentUserPriv = null;
    private User CurrentUser {
        get {
            if (currentUserPriv == null){
                currentUserPriv = UserQuerier.getCurrentUserWithStateInformation();
            }
            
            return currentUserPriv;
        }
    }
    
    private Boolean isStatePartnerUser(){
        return CurrentUser.ContactId != null ||
               UserQuerier.UserStatePartnerAccount != null;
    }

    private Id userStatePartnerIdPriv = null;
    private Boolean userStatePartnerIdCalculated = false;
    private Id UserStatePartnerId {
        get {
            if (userStatePartnerIdCalculated == false &&
                isStatePartnerUser() == true){

                userStatePartnerIdPriv = 
                UserQuerier.UserStatePartnerAccount != null ?
                    UserQuerier.UserStatePartnerAccount.Id :
                    CurrentUser.Contact.Account.State_Partner_Id__c;
                
                
                userStatePartnerIdCalculated = true;
            }
            
            return userStatePartnerIdPriv;
        }
    }
    
    private void populateInsertedIndividualInContext(List<Account> newAccounts){
        for (Account newAcct : newAccounts){            
            if (newAcct.IsPersonAccount == true){
                insertedIndividualsInContext.add(newAcct);
            }
        }
    }
    
    private Boolean isNewlyInsertedIndividual(Account individual){
        for (Account insertedIndividual : insertedIndividualsInContext){
            if (individual.IsPersonAccount == true &&
                insertedIndividual.FirstName == individual.FirstName &&
                insertedIndividual.LastName == individual.LastName){
                return true;
            }
        }
        return false;
    }

    public void validateMultiSiteBillJointly(List<Account> accounts){
        for (Account acct : accounts){
            if (acct.RecordTypeId != multiSiteOrgRecordTypeId &&
                acct.Multi_Site_Bill_Jointly__c == true){
                acct.addError(Constant.BILL_JOINTLY_MULTISITE_ONLY_VAL_RULE_MSG);
            }
        }
    }
    
    public void defaultStatePartnerForStatePartnerCreatedAccounts(List<Account> newAccounts){
        if (isStatePartnerUser() == false){
            return;
        }
        
        for (Account acct : newAccounts){
            if (acct.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID ||
                acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID ||
                acct.IsPersonAccount == true){
                
                acct.State_Partner_Id__c = UserStatePartnerId;
            }
        }
    }

    public void setOwnerToGeneralStatePartnerUserOnBeforeInsert(List<Account> newRecords){
        Set<Id> primaryAffilIds = new Set<Id>();
        List<Account> accountsWithPrimaryAffils = new List<Account>();
        
        for (Account acct : newRecords){

            //Reset as GeneralStatePartnerUser if multi-site org or state partner id is populated 
            if ((acct.State_Partner_Id__c != null ||
                 acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID
                )
                 &&
                acct.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID){

                //exclude State Partners using Nimble AMS (eg. Washington and Kentucky)
                if (!UserQuerier.UserStatePartnerAccountUsesNimbleAMS) {
                    acct.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                }
            }

            //invstigate further if primary affiliation is populated, and user State Partner is not using Nimble AMS (eg. Washington and Kentucky)
            if (acct.NU__PrimaryAffiliation__c != null && !UserQuerier.UserStatePartnerAccountUsesNimbleAMS){
                accountsWithPrimaryAffils.add(acct);
                primaryAffilIds.add(acct.NU__PrimaryAffiliation__c);
            }
        }
        
        if (accountsWithPrimaryAffils.size() > 0){
            Map<Id, Account> primaryAffilAccounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(primaryAffilIds);

            for (Account accountWithPrimaryAffil : accountsWithPrimaryAffils){
                Account primaryAffilAccount = primaryAffilAccounts.get(accountWithPrimaryAffil.NU__PrimaryAffiliation__c);
                
                if ((primaryAffilAccount.State_Partner_Id__c != null &&
                     accountWithPrimaryAffil.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID) ||
                     
                     // If the parent's owner is the general state partner then one of its "parents"
                     // are associated with a State.
                     primaryAffilAccount.OwnerId == StatePartnerOwnerUserId.GeneralStatePartnerUserId){
                     accountWithPrimaryAffil.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                }
            }
        }
    }
    
    public void updateOwnershipOnBeforeUpdate(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
        /*
           Set the account's owner ids based on their primary affiliation. If
           an account becomes primary affiliatied with an account linked to a
           state partner, set its owner to the "General State Partner" user.
        */
        
        List<Account> accountsWithUpdatedPrimaryAffils = new List<Account>();
        
        List<Account> accountsNowOwnedByGeneralStatePartnerUser = new List<Account>();
        
        for (Account oldAcct : oldAccounts.values()){
            Account newAcct = newAccounts.get(oldAcct.Id);
            
            if (oldAcct.NU__PrimaryAffiliation__c != newAcct.NU__PrimaryAffiliation__c){
                
                if (newAcct.NU__PrimaryAffiliation__c != null){
                    accountsWithUpdatedPrimaryAffils.add(newAcct);
                }
            }
            
            // account has become assigned to a state partner
            // so set its owner to the general state partner user.
            if (oldAcct.State_Partner_Id__c == null
                && newAcct.State_Partner_Id__c != null
                && newAcct.RecordTypeId != Constant.ACCOUNT_STATE_PARTNER_PORTAL_RECORD_TYPE_ID
                && !UserQuerier.UserStatePartnerAccountUsesNimbleAMS){
                newAcct.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                accountsNowOwnedByGeneralStatePartnerUser.add(newAcct);
            }
            
            if (oldAcct.OwnerId != StatePartnerOwnerUserId.GeneralStatePartnerUserId &&
                newAcct.OwnerId == StatePartnerOwnerUserId.GeneralStatePartnerUserId){
                accountsNowOwnedByGeneralStatePartnerUser.add(newAcct);
            }
        }
        
        if (accountsWithUpdatedPrimaryAffils.size() > 0){
            Set<Id> allChangedPrimaryAffilIds = NU.CollectionUtil.getLookupIds(accountsWithUpdatedPrimaryAffils, 'NU__PrimaryAffiliation__c');
            Map<Id, Account> primaryAffilAccounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(allChangedPrimaryAffilIds);
            
            if (accountsWithUpdatedPrimaryAffils.size() > 0){
                
                
                for (Account accountWithUpdatedPrimaryAffil : accountsWithUpdatedPrimaryAffils){
                    Account queriedPrimaryAffilAccount = primaryAffilAccounts.get(accountWithUpdatedPrimaryAffil.NU__PrimaryAffiliation__c);
                    
                    if ((queriedPrimaryAffilAccount.State_Partner_Id__c != null
                        || queriedPrimaryAffilAccount.OwnerId == StatePartnerOwnerUserId.GeneralStatePartnerUserId)
                        && !UserQuerier.UserStatePartnerAccountUsesNimbleAMS){
                        accountWithUpdatedPrimaryAffil.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                    }
                }
            }
        }
        
        List<Account> accountsToUpdate = new List<Account>();
        
        if (accountsNowOwnedByGeneralStatePartnerUser.size() > 0){
            // Update child primary affiliations' owner ids to the
            // general state partner user.
            
            List<Account> childAccounts = 
            [select id
               from Account
              where NU__PrimaryAffiliation__c in :accountsNowOwnedByGeneralStatePartnerUser
                and OwnerId != :StatePartnerOwnerUserId.GeneralStatePartnerUserId];
                
            for (Account childAccount : childAccounts){
                if (!UserQuerier.UserStatePartnerAccountUsesNimbleAMS) {
                    childAccount.OwnerId = StatePartnerOwnerUserId.GeneralStatePartnerUserId;
                    accountsToUpdate.add(childAccount);
                }
            }
        }
        
        if (accountsToUpdate.size() > 0){
            update accountsToUpdate;
        }
    }
    
    public void validateStatePartnerAccountEditing(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
        // Enforce what state partner portal users can edit.

        // not a state partner portal user.
        if (isStatePartnerUser() == false){
            return;
        }
        
        Set<Id> oldPrimaryAffiliationIds = NU.CollectionUtil.getLookupIds(oldAccounts.values(), 'NU__PrimaryAffiliation__c');
        
        Set<Id> accountsWithStatePartnerIds = new Set<Id>();
        accountsWithStatePartnerIds.addAll(oldPrimaryAffiliationIds);
        accountsWithStatePartnerIds.addAll(newAccounts.keyset());
        
        
        Map<Id, Account> queriedUpdatedAccounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds( accountsWithStatePartnerIds );
        Id userStatePartnerId = UserStatePartnerId;
        
        for (Account updatedAccount : newAccounts.values()){
            if (isNewlyInsertedIndividual(updatedAccount)){
                continue;
            }
            
            Account queriedUpdatedAccount = queriedUpdatedAccounts.get(updatedAccount.Id);
            Account oldAcct = oldAccounts.get(updatedAccount.Id);
            Account newAcct = newAccounts.get(updatedAccount.Id);
            string UserStatePartnerName = UserQuerier.UserStatePartnerAccount != null? UserQuerier.UserStatePartnerAccount.Name : 'No State';
            Id oldPrimaryAffiliationId = oldAcct.NU__PrimaryAffiliation__c;
            Account oldPrimaryAffiliation = queriedUpdatedAccounts.get(oldPrimaryAffiliationId);
            
            //May have to tweak for providers too.
            Boolean updatedAccountWasInState = oldAcct.NU__PrimaryAffiliation__c != null &&
                                               newAcct.NU__PrimaryAffiliation__c == null &&
                                               oldPrimaryAffiliation != null &&
                                               oldPrimaryAffiliation.State_Partner_Id__c == userStatePartnerId;
            
            Boolean updatedAccountInState = queriedUpdatedAccount.State_Partner_Id__c == userStatePartnerId ||
            	(queriedUpdatedAccount.State_Partner_Name__c != null && queriedUpdatedAccount.State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
            	(queriedUpdatedAccount.Parent_State_Partner_Name__c != null && queriedUpdatedAccount.Parent_State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
            	(queriedUpdatedAccount.Grandparent_State_Partner_Name__c != null && queriedUpdatedAccount.Grandparent_State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
            	(queriedUpdatedAccount.State_Partner_Permissions__c != null && queriedUpdatedAccount.State_Partner_Permissions__c.containsIgnoreCase(UserStatePartnerName)) ||
				queriedUpdatedAccount.Id == userStatePartnerId;

            Boolean updatedAccountInStateORPrimaryAffiliationInState = 
              updatedAccountInState ||
              (queriedUpdatedAccount.NU__PrimaryAffiliation__c != null &&
               (queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Id__c == userStatePartnerId ||
            	(queriedUpdatedAccount.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
            	(queriedUpdatedAccount.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
                (queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Permissions__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Permissions__c.containsIgnoreCase(UserStatePartnerName)) || 
                queriedUpdatedAccount.NU__PrimaryAffiliation__c == userStatePartnerId));
            
            Boolean updatedAccountsMSOInState = 
                queriedUpdatedAccount.NU__PrimaryAffiliation__c != null &&
                queriedUpdatedAccount.NU__PrimaryAffiliation__r.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
                (
                	queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Id__c == userStatePartnerId || 
                	(queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Permissions__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Permissions__c.containsIgnoreCase(UserStatePartnerName)) ||
	            	(queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Name__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
	            	(queriedUpdatedAccount.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName)) ||
	            	(queriedUpdatedAccount.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c != null && queriedUpdatedAccount.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c.containsIgnoreCase(UserStatePartnerName))
                );
            
            Boolean statePartnerCanEdit = updatedAccountInStateORPrimaryAffiliationInState ||
                                          updatedAccountsMSOInState ||
                                          updatedAccountWasInState ||
                                          statesCanEditAccountIds.contains(updatedAccount.Id);
               
            
            if (statePartnerCanEdit == false){
                updatedAccount.addError(Constant.STATE_PARTNER_EDIT_VAL_MSG);
            }
            else if (updatedAccountsMSOInState == true &&
                     updatedAccountInState == false &&
                     (oldAcct.Provider_Membership__c != newAcct.Provider_Membership__c ||
                      oldAcct.Provider_Lapsed_On__c != newAcct.Provider_Lapsed_On__c)){
                updatedAccount.addError(Constant.STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG);
            }
            else{
                statesCanEditAccountIds.add(updatedAccount.Id);
            }
        }
    }
    
    private List<Account> getLatestAccountInfos(List<Account> accounts){
        return [select id,
                        name,
                        State_Partner_Name__c,
                        NU__PrimaryAffiliation__c,
                        NU__PrimaryAffiliation__r.Id,
                        NU__PrimaryAffiliation__r.Provider_Membership__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__StartDate__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__EndDate__c,
                        NU__PrimaryAffiliation__r.Provider_Membership__r.NU__MembershipType__c,
                        NU__PrimaryAffiliation__r.RecordTypeId,
                        NU__PrimaryAffiliation__r.Multi_Site_Corporate__c,
                        Provider_Membership__c,
                        Provider_Membership__r.NU__MembershipType__c,
                        Provider_Membership__r.NU__StartDate__c,
                        Provider_Membership__r.NU__EndDate__c,
                        RecordTypeId,
                        (Select id,
                                name,
                                NU__MembershipType__c,
                                NU__StartDate__c,
                                NU__EndDate__c
                           from NU__Memberships__r)
                   from Account
                  where id in :accounts];
    }
    
    private Boolean isCorporateMSOWithProviderMembership(Account account){
        return account.NU__PrimaryAffiliation__r.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
               account.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == true &&
               account.NU__PrimaryAffiliation__r.Provider_Membership__c != null;
    }
    
    private Boolean needsCorporateProviderMembershipCreated(Account account){
        NU__Membership__c parentMSOMembership = account.NU__PrimaryAffiliation__r.Provider_Membership__r;
        
        for (NU__Membership__c accountMembership : account.NU__Memberships__r){
            if (accountMembership.NU__MembershipType__c == parentMSOMembership.NU__MembershipType__c &&
                accountMembership.NU__StartDate__c == parentMSOMembership.NU__StartDate__c &&
                accountMembership.NU__EndDate__c == parentMSOMembership.NU__EndDate__c){
                
                return false;
            }
        }
        
        return true;
    }
    
    private void createCorporateProviderMemberships(List<Account> newCorporateProvidersToCreateMembershipsFor){
        if (newCorporateProvidersToCreateMembershipsFor.size() > 0){
            List<NU__Membership__c> newCorporateProviderMemberships = new List<NU__Membership__c>();
            
            for (Account corporateProvider : newCorporateProvidersToCreateMembershipsFor){
                NU__Membership__c corporateProviderMembership = new NU__Membership__c(
                    NU__Account__c = corporateProvider.Id,
                    NU__StartDate__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__StartDate__c,
                    NU__EndDate__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__EndDate__c,
                    NU__MembershipType__c = corporateProvider.NU__PrimaryAffiliation__r.Provider_Membership__r.NU__MembershipType__c,
                    NU__ExternalAmount__c = 0
                );
                
                newCorporateProviderMemberships.add(corporateProviderMembership);
            }
            
            insert newCorporateProviderMemberships;
        }
    }
    
    private void createProviderMembershipsForNewCorporateProvidersOnAfterInsert(List<Account> newAccounts){
        List<Account> potentialNewCorporateProviders = new List<Account>();
        
        for (Account newAccount : newAccounts){
            if (newAccount.NU__PrimaryAffiliation__c != null &&
                newAccount.Multi_Site_Enabled__c == true){
                potentialNewCorporateProviders.add(newAccount);
            }
        }
        
        if (potentialNewCorporateProviders.size() > 0){
            List<Account> latestAccountInfos = getLatestAccountInfos(potentialNewCorporateProviders);
            List<Account> newCorporateProvidersToCreateMembershipsFor = new List<Account>();
            
            for (Account latestAccountInfo : latestAccountInfos){
                if (isCorporateMSOWithProviderMembership(latestAccountInfo)){
                    newCorporateProvidersToCreateMembershipsFor.add(latestAccountInfo);
                }
            }
            
            createCorporateProviderMemberships(newCorporateProvidersToCreateMembershipsFor);
        }
    }
    
    private void createProviderMembershipsForNewCorporateProviders(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
        Boolean bNeedsDeeperInspection = false;
        
        for (Account oldAccount : oldAccounts.values()){
            Account newAccount = newAccounts.get(oldAccount.Id);
            
            if ((oldAccount.NU__PrimaryAffiliation__c != newAccount.NU__PrimaryAffiliation__c &&
                  newAccount.NU__PrimaryAffiliation__c != null) 
                  
                  ||
                
                 (oldAccount.Multi_Site_Enabled__c == false &&
                  newAccount.Multi_Site_Enabled__c == true)
                
                )
                {
                    
                bNeedsDeeperInspection = true;
                break;
            }
        }
        
        
        if (bNeedsDeeperInspection) {
            Map<Id, Account> latestAccountInfos = new Map<Id, Account>( getLatestAccountInfos(newAccounts.values()) );
            List<Account> newCorporateProvidersToCreateMembershipsFor = new List<Account>();
            
            for (Account oldAccount : oldAccounts.values()){
                Account newAccount = newAccounts.get(oldAccount.Id);
                Account latestAccountInfo = latestAccountInfos.get(oldAccount.Id);
                
                if (((oldAccount.NU__PrimaryAffiliation__c != newAccount.NU__PrimaryAffiliation__c &&
                      newAccount.NU__PrimaryAffiliation__c != null) 
                      
                      ||
                    
                     (oldAccount.Multi_Site_Enabled__c == false &&
                      newAccount.Multi_Site_Enabled__c == true)
                    
                    )
                    
                     &&
                    newAccount.Multi_Site_Enabled__c == true &&
                    isCorporateMSOWithProviderMembership(latestAccountInfo) &&
                    needsCorporateProviderMembershipCreated(latestAccountInfo)){
                        
                    newCorporateProvidersToCreateMembershipsFor.add(latestAccountInfo);
                }
            }
            
            createCorporateProviderMemberships(newCorporateProvidersToCreateMembershipsFor);
        }
    }
    
    private void setStatePartnerFieldsForStateUsers(List<Account> newAccounts){
        Account stateUserStatePartnerAccount = UserQuerier.UserStatePartnerAccount;
        
        if (stateUserStatePartnerAccount == null){
            return;
        }
        
        for (Account newAccount : newAccounts){
            newAccount.State_Partner_Id__c = stateUserStatePartnerAccount.Id;
            newAccount.State_Partner_Name__c = stateUserStatePartnerAccount.Name;
        }
    }
    
    private void setCompanyManagerBasedOnExpertise(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts){
        Map<Id, Account> newAccountsAffil = new Map<Id, Account>([SELECT
            Id,
            Organizational_Function_Expertise__pc,
            IsPersonAccount,
            NU__PrimaryAffiliationRecord__c,
            NU__PrimaryAffiliationRecord__r.Id,
            NU__PrimaryAffiliationRecord__r.NU__IsCompanyManager__c
            FROM Account
            WHERE Id IN :newAccounts.keyset()
        ]);
        
        List<NU__Affiliation__c> changes = new List<NU__Affiliation__c>();
        
        for (Account changedAcc : newAccounts.values()){
            Account accWithAff = newAccountsAffil.get(changedAcc.Id);
            if (changedAcc != null && changedAcc.Organizational_Function_Expertise__pc != null && accWithAff != null &&
                changedAcc.IsPersonAccount && (changedAcc.Organizational_Function_Expertise__pc.containsIgnoreCase('Marketing/Sales') ||
                changedAcc.Organizational_Function_Expertise__pc.containsIgnoreCase('PR/Communications')) &&
                accWithAff.NU__PrimaryAffiliationRecord__c != null &&
                accWithAff.NU__PrimaryAffiliationRecord__r.NU__IsCompanyManager__c != true) {
                        changes.add(new NU__Affiliation__c(Id = accWithAff.NU__PrimaryAffiliationRecord__r.Id, NU__IsCompanyManager__c = true));
            }
            /*
            else if (changedAcc != null && changedAcc.Organizational_Function_Expertise__pc != null && accWithAff != null &&
                changedAcc.IsPersonAccount && (!changedAcc.Organizational_Function_Expertise__pc.containsIgnoreCase('Marketing/Sales') &&
                !changedAcc.Organizational_Function_Expertise__pc.containsIgnoreCase('PR/Communications')) &&
                accWithAff.NU__PrimaryAffiliationRecord__c != null &&
                accWithAff.NU__PrimaryAffiliationRecord__r.NU__IsCompanyManager__c == true) {
                        changes.add(new NU__Affiliation__c(Id = accWithAff.NU__PrimaryAffiliationRecord__r.Id, NU__IsCompanyManager__c = false));
            }*/
        }
        
        if (!changes.isEmpty()){
            update changes;
        }
    }
    
    /*
    private void setCorporateAllianceMembershipPriority(Map<Id, Account> newAccounts){
        
               List<Id> IdList = new List<Id>();
       
       for(Account IdAcc : newAccounts.values())
       {
              if(IdAcc != null && IdAcc.Id != null){
                  IdList.add(IdAcc.Id);
              }
       }
        
         
        List<Account> accounts = [SELECT Id, Name, Member_Product_Name__c, LeadingAge_ID__c, NU__RecordTypeName__c, LeadingAge_Member_Company__c, Provider_Membership__c,
                                  NU__Membership__c, NU__Membership__r.MembershipTypeName__c, NU__Membership__r.MembershipTypeProductName__c, 
                                  CAST_Membership__c, CAST_Membership__r.MembershipTypeProductName__c, CAST_Member_Product_Name__c, 
                                  (SELECT Id, Account__c, ProductDesc__c FROM Sponsorships__r WHERE (ProductDesc__c LIKE 'AM%Annual%') ORDER BY Sponsorship_Date__c DESC) 
                                  FROM Account 
                                  WHERE NU__RecordTypeName__c = 'Corporate Alliance/Sponsor' AND Id IN :IdList 
                                  ORDER BY LeadingAge_Member_Company__c, LeadingAge_ID__c];
        
        for (Account a : accounts){
            Account d = new Account();
            
            if(a != Null && a.LeadingAge_Member_Company__c.equalsIgnoreCase('Member'))
                {
                    if (a.Sponsorships__r != null && a.Sponsorships__r.size() > 0) {
                        d = newAccounts.get(a.Id);
                        d.Membership_1__c = a.Sponsorships__r[0].ProductDesc__c;
                        d.Membership_2__c = a.CAST_Member_Product_Name__c;
                 }
                 else if(a != Null && a.NU__Membership__c != null &&
                  (a.Member_Product_Name__c.containsIgnoreCase('Gold') || a.Member_Product_Name__c.containsIgnoreCase('Silver Premier')
                   ||a.Member_Product_Name__c.equalsIgnoreCase('Business Associate') ||
                   a.Member_Product_Name__c.equalsIgnoreCase('CAST Business Associate')  )
                  )
                  {
                        d = newAccounts.get(a.Id);
                        d.Membership_1__c = a.Member_Product_Name__c;
                        if (!d.Membership_1__c.containsIgnoreCase('CAST Business Associate')) {
                            d.Membership_2__c = a.CAST_Membership__r.MembershipTypeProductName__c;
                        }
                        else {
                            d.Membership_2__c = '';
                        }
                  }

                }
                else{
                    d = new Account(Id = a.Id, Membership_1__c ='', Membership_2__c ='' );
                }
                
        }
    }
    */
    
    /* When account status is changed from Active to Inactive, trigger will query all affiliations for that account and set them to inactive and the other way around.  */
    private void updateAffiliationStatusIfAccountStatusUpdated(Map<Id, Account> oldAccounts, Map<Id, Account> newAccounts){
    
    Set<Id> accountsWithUpdatedStatus = new Set<Id>();
        
        for (Account oldAcct : oldAccounts.values()){
            Account newAcct = newAccounts.get(oldAcct.Id);
            
            if (oldAcct.NU__Status__c != newAcct.NU__Status__c){
                
                if (newAcct.NU__Status__c != null){
                    accountsWithUpdatedStatus.add(newAcct.Id);
                }
            }         
        }
        if (accountsWithUpdatedStatus.size() > 0){
            
            List<NU__Affiliation__c> AffiliationAccounts = [SELECT Id, NU__Account__c, NU__Status__c FROM NU__Affiliation__c WHERE NU__Account__c in:accountsWithUpdatedStatus];
            
            for(NU__Affiliation__c af : AffiliationAccounts) {
                Account a = newAccounts.get(af.NU__Account__c);
                
                if (a.NU__Status__c == Constant.ACCOUNT_STATUS_ACTIVE){
                    af.NU__Status__c = Constant.AFFILIATION_STATUS_ACTIVE;
                }
                
                if (a.NU__Status__c == Constant.ACCOUNT_STATUS_INACTIVE){
                    af.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
                }
    
            }
            
            update AffiliationAccounts;
        }
        
    }

    public override void onBeforeInsert(List<Sobject> newRecords){
        List<Account> newAccounts = (List<Account>) newRecords;
        
        populateInsertedIndividualInContext(newAccounts);
        
        validateMultiSiteBillJointly(newAccounts);
        
        defaultStatePartnerForStatePartnerCreatedAccounts(newAccounts);

        setOwnerToGeneralStatePartnerUserOnBeforeInsert(newAccounts);
        
        setStatePartnerFieldsForStateUsers(newAccounts);
    }
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        List<Account> newAccounts = (List<Account>) newRecordMap.values();
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldRecordMap;
        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newRecordMap;
        
        validateMultiSiteBillJointly(newAccounts);
        updateOwnershipOnBeforeUpdate(oldAccountsMap, newAccountsMap);
        validateStatePartnerAccountEditing(oldAccountsMap, newAccountsMap);
        CorporateAllianceMembershipPriority.setCorporateAllianceMembershipPriority(newAccountsMap);
        
    }
    
    public override void onAfterUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
        Map<Id, Account> oldAccountsMap = (Map<Id, Account>) oldRecordMap;
        Map<Id, Account> newAccountsMap = (Map<Id, Account>) newRecordMap;
        
        createProviderMembershipsForNewCorporateProviders(oldAccountsMap, newAccountsMap);
        setCompanyManagerBasedOnExpertise(newAccountsMap, oldAccountsMap);
        
        //updateAffiliationStatusIfAccountStatusUpdated(oldAccountsMap, newAccountsMap);
    }
    
    public override void onAfterInsert(Map<Id, sObject> newRecordMap){
        List<Account> newAccounts = (List<Account>) newRecordMap.values();
        
        createProviderMembershipsForNewCorporateProvidersOnAfterInsert(newAccounts);
    }
}