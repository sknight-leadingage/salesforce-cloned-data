<apex:page controller="ARTrialBalanceController" >

	<head>
		<!-- the css styles for the progress bars -->
		<style>
		    .progressBar{
		        border:1px solid #DDDDDD;
		        height: 19px;
		        width: 300px;
		        -moz-border-radius: 5px;
		        -webkit-border-radius: 5px;
		    }
		    .progress{
		        border:1px solid #E78F08;
		        height: 100%;
		        margin: -1px;
		        text-align: center;
		        -moz-border-radius: 5px;
		        -webkit-border-radius: 5px;
		        line-height: 18px;
		    }
		    .BadStatus{
		        border:1px solid #FF55FF;
		        background-color: #CC0000;
		        color: white;

		    }
		</style>
		<!--
		    progressBar{background-color: #f8f8f8;}
		    progress{background-color: #F7B64B;}
		 -->
	</head>

	<body>

	<apex:sectionHeader title="AR Trial Balance" help="#"/>
	<apex:pageMessages />

	<apex:form >
    <apex:pageBlock title="Options" id="OptionsPB">

		<p><i>Note: When hitting submit, the task will run and populate AR Snapshot records which you can access with Salesforce Reports.</i></p>

		<apex:pageBlockSection columns="1" id="OptionsPBS">

			<apex:inputText value="{!FormFields.Name}" label="Snapshot Name" />

			<apex:inputField value="{!FormFields.AsOfDate.NU__EndDate__c}" label="As Of Date" />

			<apex:selectList label="Entity"
                               multiselect="false"
                               size="1"
                               value="{!FormFields.EntityId}"
                               required="true">
				<apex:selectOptions value="{!EntityOptions}" />
			</apex:selectList>

			<apex:selectList label="Detail Level"
                               multiselect="false"
                               size="1"
                               value="{!FormFields.DetailLevel}"
                               required="true">
				<apex:selectOptions value="{!DetailLevelOptions}" />
			</apex:selectList>


        </apex:pageBlockSection>

		<apex:pageBlockButtons >
                <apex:commandButton action="{!submit}" value="Submit"/>
        </apex:pageBlockButtons>

	</apex:pageBlock>

	<!-- This action poller will check the status of the batch job every 5 seconds -->
	<apex:actionStatus id="progressStatus" />
	<apex:actionPoller rerender="results" interval="5" status="progressStatus" />

	</apex:form>

	<apex:pageBlock title="Progress" id="results" rendered="{!BatchJobId != null}">

        <apex:outputText value="{!batchJob.job.Status}"/>

        <!-- Here with have two divs that construct our progresses bar. An outer which is the entire bar,
        and and inner that represents the percent complete. -->

        <div class="progressBar" style="background-color: {!CASE(batchJob.job.Status,'Aborted','#551A8B','Completed','#f8f8f8','Failed','#9E0508','#f8f8f8')};">
            <div class="progress" style="width: {!batchJob.percentComplete}%; background-color: {!CASE(batchJob.job.Status,'Aborted','#B23AEE','Completed','#20F472','Failed','#FFB6C1','#F7B64B')}; color: {!CASE(batchJob.job.Status,'Aborted','white','Failed','white','black')}">
                {!batchJob.percentComplete}%
            </div>
        </div>

	</apex:pageBlock>

	</body>

</apex:page>