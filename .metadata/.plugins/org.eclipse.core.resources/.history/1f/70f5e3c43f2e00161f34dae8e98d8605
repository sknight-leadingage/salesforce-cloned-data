global class JointBillingUpdater implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
    NU__MembershipType__c JointStateProviderMembershipType { get; set; }
    
    Id JointStateProviderProductId { get; set; } 
    
    global static void scheduleAt3AM() {
        System.schedule('Joint Billing Updater', '0 0 3 * * ?', new JointBillingUpdater());
    }

    global void execute(SchedulableContext context) {
        // This entire class assumes that one Joint Billing will be used in each batch.
        // If the performance becomes terrible, up the batch size and update the logic
        // in the execute and finish methods accordingly.
        Database.executeBatch(new JointBillingUpdater(), 1);
    }
    
    global JointBillingUpdater(){
        JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
        JointStateProviderProductId = JointBillingUtil.getJointStateProviderProductId(JointStateProviderMembershipType);
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        List<Joint_Billing__c> latestJointBillings = getLatestStateProviderJointBillings();     
        
        return Database.getQueryLocator('select id, name, State_Partner__c, Start_Date__c, End_Date__c from Joint_Billing__c where id in :latestJointBillings');
    }
    
    global void execute(Database.BatchableContext context, List<Joint_Billing__c> latestJointBillings) {
        Joint_Billing__c latestJointBilling = latestJointBillings[0];
        
        updateStatePartnerJointBilling(latestJointBilling, context);
    }
    
    global void finish(Database.BatchableContext context) {
        if (Test.isRunningTest() == false){
            BatchApexUtil.sendErrorEmailWhenJobFinishesWithErrors( context.getJobId() );
            
            //http://corycowgill.blogspot.com/2010/12/leveraging-scheduled-apex-to-link-batch.html
            Datetime sysTime = System.now();
            sysTime = sysTime.addSeconds(20);
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            ScheduleBatchUpdateProviderLapsedOn schBatchLapsed = new ScheduleBatchUpdateProviderLapsedOn();
            System.schedule('BatchUpdateProviderLapsedOn' + sysTime.getTime(),chron_exp,schBatchLapsed);
        }
    }

    public void updateStatePartnerJointBilling(Joint_Billing__c latestJointBilling){
        updateStatePartnerJointBilling(latestJointBilling, null);
    }
    
    public static Integer getNotNull(Integer s){
		    if (s == null){
		        return 0;
		    }
		    else {
		        return s;
		    }
    }
    
    public decimal changedBillingTypeNeedsPricingAdjustment(Joint_Billing_Item__c billItem, decimal calculatedDues) {
    	Decimal returnPricing = 0.0;
    	if (billItem.Amount__c != calculatedDues && calculatedDues != null) {
    		returnPricing = calculatedDues;
    	}
    	return returnPricing;
    }
    
    public void updateStatePartnerJointBilling(Joint_Billing__c latestJointBilling, Database.BatchableContext context){
        List<Joint_Billing_Item__c> existingBillingItems = getExistingBillingItems(latestJointBilling.Id);
        List<Joint_Billing_Item_Line__c> billingItemLinesToInsert = new List<Joint_Billing_Item_Line__c>();
        List<Account> providerAccounts = getProviderAccountsFromExistingBillingItems(existingBillingItems);
        List<Account> aOverrideCleanuplist = new List<Account>();
        
        JointBillingUtil.populateCustomPricingManagerCaches(providerAccounts, JointStateProviderMembershipType.Id, JointStateProviderProductId);
        Date todaysDate = Date.Today();
        
        System.debug('Processing Joint Billing [Billing Id: ' + (string)latestJointBilling.Id + ', State Partner Id: ' + (string)latestJointBilling.State_Partner__c + ']');
        
        NU.MembershipTermInfo jointBillingMembershipTerm = JointBillingUtil.calculateMembershipTermFromJointBilling(latestJointBilling, JointStateProviderMembershipType);
        // Create Adjustments for cancelled providers and changed dues amounts
        for (Joint_Billing_Item__c existingBillingItem : existingBillingItems){
            if (isCancelledProviderAccount(existingBillingItem, todaysDate)){
                Joint_Billing_Item_Line__c cancellingBillingItemLine = createCancellingBillingItemLine(existingBillingItem);
                cancellingBillingItemLine.Adjustment_Cancellation_Reason__c = existingBillingItem.Account__r.Cancellation_Reason__c;
                cancellingBillingItemLine.Provider_Lapsed_On__c = existingBillingItem.Account__r.Provider_Lapsed_On__c;
                cancellingBillingItemLine.Type__c = 'Canceled';
                billingItemLinesToInsert.add(cancellingBillingItemLine);
                Account aOverrideCleanup = new Account(Id = existingBillingItem.Account__r.Id, Cancellation_Reason__c = null, Provider_Lapsed_On__c = null);
                aOverrideCleanuplist.add(aOverrideCleanup);
            }
            else if (shouldUpdateDuesFromDuesPriceOverride(existingBillingItem)){
                Joint_Billing_Item_Line__c duesPriceOverrideAdjustmentBillingItemLine = createDuesPriceOverrideBillingItemLine(existingBillingItem);
                duesPriceOverrideAdjustmentBillingItemLine.Adjustment_Cancellation_Reason__c = existingBillingItem.Account__r.Dues_Price_Override_Reason__c;
                billingItemLinesToInsert.add(duesPriceOverrideAdjustmentBillingItemLine);
                Account aOverrideCleanup = new Account(Id = existingBillingItem.Account__r.Id, Dues_Price_Override_Reason__c = null, Dues_Price_Override__c = null);
                aOverrideCleanuplist.add(aOverrideCleanup);
            }
        }
        
        update aOverrideCleanuplist;
        // Create Billing Item and Billing Item Line for new Providers
        List<Account> newStatePartnerProviders = getNewStatePartnerProviders(latestJointBilling, providerAccounts);
        List<Joint_Billing_Item__c> newJointBillingItems = new List<Joint_Billing_Item__c>();
        List<NU__Membership__c> newMemberships = new List<NU__Membership__c>();
        List<JointBillingUtil.JointBillingItemAndLineAssociation> billingItemToLineMap = new List<JointBillingUtil.JointBillingItemAndLineAssociation>();
        
        CustomPricingManager.emptyAccountAndAccountMembershipCaches();
        
        JointBillingUtil.populateCustomPricingManagerAccountAndMembershipCaches(newStatePartnerProviders);
        
        Map<Id, List<Account>> msoCorporateProvidersMap = JointBillingUtil.getMSOCorporateProvidersMap(newStatePartnerProviders);
        
         //Moved up -- NU.MembershipTermInfo jointBillingMembershipTerm = JointBillingUtil.calculateMembershipTermFromJointBilling(latestJointBilling, JointStateProviderMembershipType);
        
        List<Account> msosWithoutProviders = new List<Account>();
        
        System.debug('Size of newStatePartnerProviders: ' + string.valueof(newStatePartnerProviders.size()));
        
        for (Account newStatePartnerProvider : newStatePartnerProviders){
            Joint_Billing_Item__c billingItem = JointBillingUtil.createJointBillingItem(newStatePartnerProvider, latestJointBilling);
            newJointBillingItems.add(billingItem);
            
            JointBillingUtil.ProviderJoinProrated = false;
            
            Date providerJoinOn = JointBillingUtil.getProviderJoinOnDate(newStatePartnerProvider);
            
            Map<Id, Decimal> duesPricing = JointBillingUtil.getProviderPricing(todaysDate, newStatePartnerProvider.Id, JointStateProviderProductId, JointStateProviderMembershipType.Id, jointBillingMembershipTerm.StartDate, jointBillingMembershipTerm.EndDate, providerJoinOn);
            
            Decimal jointStateProviderDuesPrice = duesPricing.get(JointStateProviderProductId);
            
            Joint_Billing_Item_Line__c billingItemLine = new Joint_Billing_Item_Line__c();
            
            if(newStatePartnerProvider.Provider_Membership__c != null )
            {  if(getNotNull(newStatePartnerProvider.Provider_Membership__r.NU__EndDate__c.year()) < (todaysDate.year() - 5))
            	{
            	billingItemLine = JointBillingUtil.createJointBillingItemLine(jointStateProviderDuesPrice, todaysDate, Constant.JOINT_BILLING_ITEM_LINE_TYPE_NEW, newStatePartnerProvider);
            	}
            	else
	            	{
	            		billingItemLine = JointBillingUtil.createJointBillingItemLine(jointStateProviderDuesPrice, todaysDate, Constant.JOINT_BILLING_ITEM_LINE_TYPE_RENEW, newStatePartnerProvider);
	            	}
            }
            else
        	{
        		billingItemLine = JointBillingUtil.createJointBillingItemLine(jointStateProviderDuesPrice, todaysDate, Constant.JOINT_BILLING_ITEM_LINE_TYPE_NEW, newStatePartnerProvider);
        	}
            
            JointBillingUtil.JointBillingItemAndLineAssociation assoc = new JointBillingUtil.JointBillingItemAndLineAssociation();
            
            assoc.jointBillingItem = billingItem;
            assoc.jointBillingItemLine = billingItemLine;
            
            billingItemToLineMap.add(assoc);
            billingItemLinesToInsert.add(billingItemLine);
            
            NU__Membership__c joinProviderMembership = DataFactoryMembershipExt.createMembership(newStatePartnerProvider, latestJointBilling, JointStateProviderMembershipType, jointStateProviderDuesPrice, jointBillingMembershipTerm.StartDate, jointBillingMembershipTerm.EndDate);
            newMemberships.add(joinProviderMembership);
            
            if (JointBillingUtil.isCorporateMSO(newStatePartnerProvider)){
                List<Account> corporateProviders = msoCorporateProvidersMap.get(newStatePartnerProvider.Id);
                
                if (NU.CollectionUtil.listHasElements(corporateProviders)) {
                    List<NU__Membership__c> corporateProviderMemberships = JointBillingUtil.createCorporateProviderMemberships(corporateProviders, joinProviderMembership);
                    newMemberships.addall(corporateProviderMemberships);
                } else {
                    try {
                        msosWithoutProviders.add(newStatePartnerProvider);
                    } catch(Exception e) {
                    }
                }
            }
        }
        
        if (newJointBillingItems.size() > 0){
            insert newJointBillingItems;
            
            JointBillingUtil.setBillingItemOnLines(billingItemToLineMap);
        }
        
        
        if (billingItemLinesToInsert.size() > 0){
            insert billingItemLinesToInsert;
        }
        
        if (newMemberships.size() > 0){
            try {
                insert newMemberships;
            } catch(DmlException e) {
                //Getting more information using: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_exception_methods.htm
                String sTemp = 'DML error inserting new memberships: [Line ' + string.valueof(e.getLineNumber()) + ', ' + e.getCause() + ']\r\n';
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    sTemp = sTemp + '[Row ' + string.valueof(e.getDmlIndex(i)) + ', ' + e.getDmlMessage(i) + ']\r\n';
                    System.debug('JointBillingError: ' + sTemp);
                }
                BatchApexUtil.sendWarningEmailDuringJobWithIncorrectData(context.getJobId(), sTemp, newMemberships);
            }
        }
        
        if (msosWithoutProviders.size() > 0 && context != null)
        {
            //These Accounts should have providers but don't -- instead of batch failing we alert someone instead
            BatchApexUtil.sendWarningEmailDuringJobWithIncorrectData(context.getJobId(), 'The following Accounts are flagged as MSOs but do not have any providers, and are therefore not being billed.  Please figure out if they are incorrectly flagged or if they need providers attaching.', msosWithoutProviders);
        }
    }
    
    public List<Joint_Billing__c> getLatestStateProviderJointBillings(){
        List<Account> statePartners =
        [select id,
                name,
                (Select id,
                        name
                        //State_Partner_Name__c
                   from Joint_Billings__r
                   order by Start_Date__c desc
                   limit 1)
           from Account
          where RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER];
        
        
        List<Joint_Billing__c> latestJointBillings = new List<Joint_Billing__c>();
        
        for (Account statePartner : statePartners){
            if (NU.CollectionUtil.listHasElements(statePartner.Joint_Billings__r)){
                latestJointBillings.add(statePartner.Joint_Billings__r[0]);
            }
        }
        
        return latestJointBillings;
    }
    
    private List<Joint_Billing_Item__c> getExistingBillingItems(Id jointBillingId){
        return [select id,
                       name,
                       Bill_Date__c,
                       Start_Date__c,
                       End_Date__c,
                       Amount__c,
                       Account__c,
                       Account__r.Id,
                       Account__r.Name,
                       Account__r.Billing_Type__c,
                       Account__r.Multi_Site_Program_Service_Revenue__c,
                       Account__r.Multi_Site_Dues_Price__c,
                       Account__r.Multi_Site_Corporate__c,
                       Account__r.Program_Service_Revenue__c,
                       Account__r.Revenue_Year_Submitted__c,
                       Account__r.Dues_Price__c,
                       Account__r.NU__Status__c,
                       Account__r.Provider_Lapsed_On__c,
                       Account__r.Cancellation_Reason__c,
                       Account__r.Dues_Price_Override_Reason__c,
                       Account__r.Provider_Membership__r.Last_Provider_Lapsed_On__c,
                       Account__r.NU__Membership__r.Last_Provider_Lapsed_On__c,
                       Account__r.NU__Membership__r.NU__EndDateOverride__c,
                       Account__r.Provider_Join_On__c,
                       Account__r.Dues_Price_Override__c,
                       Account__r.RecordTypeId,
                       Joint_Billing__c,
                       Joint_Billing__r.State_Partner__c,
                       Joint_Billing__r.Start_Date__c
                  from Joint_Billing_Item__c
                 where Joint_Billing__c = :jointBillingId];
    }
    
    private Boolean isCancelledProviderAccount(Joint_Billing_Item__c existingBillingItem, Date todaysDate){
        return existingBillingItem != null &&
               existingBillingItem.Account__r.Provider_Lapsed_On__c <= todaysDate &&
               existingBillingItem.Amount__c != 0 &&
               isProviderOrCorporateMSO(existingBillingItem.Account__r);
    }
    
    private Boolean hasProviderDuesPriceChanged(Joint_Billing_Item__c existingBillingItem, Decimal jointStateProviderDuesPrice){
        return existingBillingItem.Amount__c != jointStateProviderDuesPrice;
    }
    
    private Boolean shouldUpdateDuesFromDuesPriceOverride(Joint_Billing_Item__c existingBillingItem){
        return existingBillingItem.Account__r.Dues_Price_Override__c != null &&
               existingBillingItem.Amount__c != existingBillingItem.Account__r.Dues_Price_Override__c &&
               isProviderOrCorporateMSO(existingBillingItem.Account__r);
    }
    
    private Boolean isProviderOrCorporateMSO(Account acct){
        return acct != null &&
               (acct.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID ||
               (acct.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID &&
                acct.Multi_Site_Corporate__c == true));
    }
    
    private Joint_Billing_Item_Line__c createCancellingBillingItemLine(Joint_Billing_Item__c cancelledBillingItem){
        return JointBillingUtil.createAdjustmentJointBillingItemLine(cancelledBillingItem.Amount__c * -1, cancelledBillingItem.Account__r, cancelledBillingItem.Id);
    }
    
    private Joint_Billing_Item_Line__c createExistingAdjustmentBillingItemLine(Joint_Billing_Item__c existingBillingItem, Decimal jointStateProviderDuesPrice){
        system.debug('   createExistingAdjustmentBillingItemLine::existingBillingItem ' + existingBillingItem);
        system.debug('   createExistingAdjustmentBillingItemLine::jointStateProviderDuesPrice ' + jointStateProviderDuesPrice);
        
        Decimal adjustmentAmount = jointStateProviderDuesPrice - existingBillingItem.Amount__c;
        
        system.debug('   createExistingAdjustmentBillingItemLine::adjustmentAmount ' + adjustmentAmount);
        
        return JointBillingUtil.createAdjustmentJointBillingItemLine(adjustmentAmount, existingBillingItem.Account__r, existingBillingItem.Id);
    }
    
    private Joint_Billing_Item_Line__c createDuesPriceOverrideBillingItemLine(Joint_Billing_Item__c existingBillingItem){       
        Decimal adjustmentAmount = existingBillingItem.Account__r.Dues_Price_Override__c - existingBillingItem.Amount__c;
        
        return JointBillingUtil.createAdjustmentJointBillingItemLine(adjustmentAmount, existingBillingItem.Account__r, existingBillingItem.Id);
    }
    
    private List<Account> getProviderAccountsFromExistingBillingItems(List<Joint_Billing_Item__c> existingBillingItems){
        List<Account> providerAccounts = new List<Account>();
        
        for (Joint_Billing_Item__c existingBillingItem : existingBillingItems){
            providerAccounts.add( existingBillingItem.Account__r );
        }
        
        return providerAccounts;
    }
    
    private List<Account> getNewStatePartnerProviders(Joint_Billing__c latestJointBilling, List<Account> providerAccounts){
        return [select id,
                       name,
                       Dues_Price__c,
                       Provider_Join_On__c,
                       Program_Service_Revenue__c,
                       Revenue_Year_Submitted__c,
                       Multi_Site_Dues_Price__c,
                       Multi_Site_Program_Service_Revenue__c,
                       Multi_Site_Corporate__c,
                       Provider_Membership__c,
                       Provider_Membership__r.NU__EndDate__c,
                       Billing_Type__c,
                       RecordTypeId
                  from Account
                 where State_Partner_ID__c = :latestJointBilling.State_Partner__c
                   and State_Partner_ID__r.RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER
                   and id not in :providerAccounts
                   and IsPersonAccount = false
                   and (Provider_Member_Thru__c = null OR Provider_Member_Thru__c <= TODAY)
                   and (Provider_Join_On__c = YESTERDAY or Provider_Join_On__c = TODAY)
                   and Provider_Lapsed_On__c = null
                   and Multi_Site_Bill_Jointly__c = false
                   and NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c = false
                   and ((
                   			(Program_Service_Revenue__c != null OR 
                   			 Billing_Type__c IN('Negotiated Rate', 'Fixed Rate: Public Housing Authorities', 'Fixed Rate: Senior Centers','Fixed Rate: Villages','Complimentary','Trial','Special Agreement: VOA [Volunteers of America]','Special Agreement: POAH [Preservation of Affordable Housing]','Special Agreement: NCR [National Church Residences]','Special Agreement: TCB [The Community Builders]')	) and
                        RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER)
                       
                        OR 
                        
                        ( (Multi_Site_Program_Service_Revenue__c != null OR
                           Billing_Type__c IN('Negotiated Rate', 'Fixed Rate: Public Housing Authorities', 'Fixed Rate: Senior Centers','Fixed Rate: Villages','Complimentary','Trial','Special Agreement: VOA [Volunteers of America]','Special Agreement: POAH [Preservation of Affordable Housing]','Special Agreement: NCR [National Church Residences]','Special Agreement: TCB [The Community Builders]')	) and
                         RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE and
                         Multi_Site_Corporate__c = true)
                       )];
    }
}