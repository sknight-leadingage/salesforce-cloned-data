public with sharing class EduProposalStatus_Controller extends EduReportsControllerBase {
    public override string getPageTitle() {
        if (RTypeSel == null) {
            return 'Proposal Status';
        }
        else {
            return RTypeSel;
        }
    }
    
    public Map<string,integer> mTotals2 = new Map<string,integer>();
    
    public string PROPLIST_SNUMBER = 'Proposal Status (By Proposal #)';
    public string PROPLIST_SSUBMITTER = 'Proposal Status (By Submitter)';
    public string PROPLIST_SCOMPANY = 'Proposal Status (By Company)';
    public string PROPLIST_STRACK = 'Proposal Status (By Track)';
    public string PROPLIST_SSPONSOR = 'Proposal Status (By Sponsor)';
    
    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(PROPLIST_SNUMBER, PROPLIST_SNUMBER));
        options.add(new SelectOption(PROPLIST_SSUBMITTER, PROPLIST_SSUBMITTER));
        options.add(new SelectOption(PROPLIST_SCOMPANY, PROPLIST_SCOMPANY));
        options.add(new SelectOption(PROPLIST_STRACK, PROPLIST_STRACK));
        options.add(new SelectOption(PROPLIST_SSPONSOR, PROPLIST_SSPONSOR));
        return options; 
    }
    
    public List<Conference_Proposal__c> PropInfo { get; set; }
    public override void SetConRefreshData() {
        PropInfo = setCon.getRecords();
        if (RTypeSel == PROPLIST_SNUMBER) {
            GroupHeaderController.GroupTotals = mTotals2;
        } else if (RTypeSel == PROPLIST_STRACK) {
            GroupHeaderController.SecondGroupTotals = mTotals2;
        }
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == PROPLIST_SNUMBER) {
            WizardStep = 2;
        } else if (RTypeSel == PROPLIST_SSUBMITTER) {
            WizardStep = 3;
        } else if (RTypeSel == PROPLIST_SCOMPANY) {
            WizardStep = 4;
        } else if (RTypeSel == PROPLIST_STRACK) {
            WizardStep = 5;
        } else if (RTypeSel == PROPLIST_SSPONSOR) {
            WizardStep = 6;
        }
        SetQuery(1);
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    
    public EduProposalStatus_Controller(){
        iWizardMax = 6;
        WizardStep = 1;
        SetConPageSize = 225;
    }
    
    public void SetQuery(integer reset) {
        string QueryBase = 'SELECT Proposal_Number__c, Title__c, Submitter__r.Account__r.Name, Submitter__r.Account__r.Speaker_Company_Name_Rollup__c, Status__c FROM Conference_Proposal__c WHERE Conference__c = :EventId ';
    
        if (RTypeSel == PROPLIST_SNUMBER) {
            GroupHeaderController.GroupTotals = null;
            mTotals2 = null;
            mTotals2 = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Proposal_Number__c) sc, Status__c s FROM Conference_Proposal__c WHERE Conference__c = :EventId GROUP BY Status__c ORDER BY Status__c];
            for (AggregateResult ar : Totals ) {
                //GroupHeaderController.SecondGroupTotals.put((string)ar.get('afn') + (string)ar.get('s'), (integer)ar.get('sc'));
                mTotals2.put((string)ar.get('s'), (integer)ar.get('sc'));
            }
            GroupHeaderController.GroupTotals = mTotals2;
            QueryBase += ' ORDER BY Status__c, Proposal_Number__c';
        } else if (RTypeSel == PROPLIST_SSUBMITTER) {
            QueryBase += ' ORDER BY Status__c, Submitter__r.Account__r.LastName, Submitter__r.Account__r.FirstName, Proposal_Number__c';
        } else if (RTypeSel == PROPLIST_STRACK) {
            GroupHeaderController.GroupTotals = null;
            mTotals2 = null;
            mTotals2 = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Proposal_Number__c) sc, Conference_Track__r.Name afn, Status__c s FROM Conference_Proposal__c WHERE Conference__c = :EventId GROUP BY Conference_Track__r.Name, Status__c ORDER BY Conference_Track__r.Name, Status__c];
            for (AggregateResult ar : Totals ) {
                //GroupHeaderController.SecondGroupTotals.put((string)ar.get('afn') + (string)ar.get('s'), (integer)ar.get('sc'));
                mTotals2.put((string)ar.get('afn') + (string)ar.get('s'), (integer)ar.get('sc'));
            }
            GroupHeaderController.SecondGroupTotals = mTotals2;
            QueryBase = 'SELECT Proposal_Number__c, Title__c, Submitter__r.Account__r.Name, Submitter__r.Account__r.Speaker_Company_Name_Rollup__c, Conference_Track__r.Name, Status__c FROM Conference_Proposal__c WHERE Conference__c = :EventId ORDER BY Conference_Track__r.Name, Status__c, Proposal_Number__c';
        } else if (RTypeSel == PROPLIST_SCOMPANY) {
            QueryBase += ' ORDER BY Status__c, Submitter__r.Account__r.Speaker_Company_Name_Rollup__c, Proposal_Number__c';
        } else if (RTypeSel == PROPLIST_SSPONSOR) {
            QueryBase = 'SELECT Proposal_Number__c FROM Conference_Proposal__c WHERE Conference__c = :EventId LIMIT 1'; //Temp, change when feature is implemented
        }
        
        SetConQuery = QueryBase;
        if (reset == 1) {
            ResetCon();
        }
        setCon.setPageSize(SetConPageSize);
        setCon.first();
        SetConRefreshData();
    }
}