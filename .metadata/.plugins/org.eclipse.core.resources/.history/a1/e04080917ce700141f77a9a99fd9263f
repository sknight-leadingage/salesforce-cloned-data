<apex:component controller="NU.OrderTaxAndShippingController" allowDML="true">
    <apex:attribute name="c"
                    type="NU.OrderController"
                    description="OrderController"
                    assignTo="{!Controller}"/>

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/cash32.png)"/>
                <h1 class="pageType">{!c.Subheader}&nbsp;</h1>
                <h2 class="pageDescription">Tax &amp; Shipping</h2>
            </div>
        </div>
    </div>

    <apex:pageBlock title="Tax & Shipping Cart Items" id="CartItems">
        <apex:outputText value="There are no cart items with shippable or taxable items." rendered="{! TaxAndShippingCartItemsCount == 0 }" />
        <apex:pageMessages />
        <script type="text/javascript">
            <apex:outputText value="{!ShippingTotalOverrideDialogScript}" />
        </script>

        <apex:pageBlockTable value="{!TaxAndShippingCartItems}" var="ci" rendered="{! TaxAndShippingCartItemsCount > 0 }">
            <apex:column >
                <apex:commandLink value="Edit Shipping"
                                  action="{!editShippingCartItem}"
                                  rerender="ShippingDialogPanel"
                                  rendered="{! (ci.Id != null && ci.ShippingItemLineCount__c > 0)}"
                                  status="AS">
                    <apex:param name="cartItemToEdit" value="{!ci.Id}" assignTo="{!SelectedCartItemId}"  />
                </apex:commandLink>
                <apex:commandLink value="Edit Tax"
                                  action="{!editTax}"
                                  styleClass="orderEditTaxLink"
                                  rerender="TaxDialogPanel"
                                  rendered="{! (ci.Id != null && ci.TaxableAmount__c > 0)}"
                                  status="AS">
                    <apex:param name="cartItemToEdit" value="{!ci.Id}" assignTo="{!SelectedCartItemId}"  />
                </apex:commandLink>
            </apex:column>
            <apex:column headerValue="Entity" rendered="{!c.isCartMultiEntity == true}">
                <apex:outputField value="{!ci.Entity__r.NU__ShortName__c}" />
            </apex:column>
            <apex:column value="{!ci.NU__Customer__c}" />
            <apex:column value="{!ci.NU__ShipMethod__c}" />
            <apex:column headerValue="{! $ObjectType.CartItem__c.Fields.ShippingAddress__c.Label }">
                <c:Address street="{!ci.NU__ShippingStreet__c}" city="{!ci.NU__ShippingCity__c}"
                           state="{!ci.NU__ShippingState__c}" postalcode="{!ci.NU__ShippingPostalCode__c}"
                           country="{!ci.NU__ShippingCountry__c}" />
            </apex:column>
            <apex:column headerValue="{! $ObjectType.CartItem__c.Fields.TotalShipping__c.Label }" styleClass="number" headerClass="number">
                <apex:outputField value="{!ci.NU__TotalShipping__c}" rendered="{!ci.NU__ShipMethod__c == null}" />
                <apex:inputField value="{!ci.NU__TotalShipping__c}" rendered="{!ci.NU__ShipMethod__c != null}" />
            </apex:column>
            <apex:column value="{!ci.Customer__r.NU__TaxExempt__c}" />
            <apex:column value="{!ci.NU__TaxableAmount__c}" styleClass="number" headerClass="number" />
            <apex:column headerValue="{! $ObjectType.CartItem__c.Fields.SalesTax__c.Label}">
                <apex:outputField value="{!ci.NU__SalesTax__c}" /> <span class="leftSpace"><apex:outputField value="{!ci.SalesTax__r.NU__TaxRate__c}" /></span>
            </apex:column>
            <apex:column value="{!ci.NU__TotalTax__c}" styleClass="number" headerClass="number" />
        </apex:pageBlockTable>
        <div class="buttons" style="clear: both;">
            <apex:commandButton id="saveButton" rendered="{!AllowTotalShippingEdit}" value="Save" action="{!saveShippingTotalOverrides}" styleClass="green" rerender="CartItems" status="AS" />
            <apex:commandButton id="cancelButton" rendered="{!AllowTotalShippingEdit}" value="Cancel" action="{!cancelShippingTotalOverrides}" rerender="CartItems" status="AS" />
        </div>
    </apex:pageBlock>

    <c:CustomDialog id="Shipping" title="How do you want to ship?" closeable="false">
        <apex:outputPanel id="ShippingDialogPanel">
            <apex:pageMessages />
            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!ShippingDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:pageBlock mode="maindetail" id="orderShippingForm">
                <apex:pageblockSection columns="1" id="orderShippingFormInputs">
                    <apex:inputField value="{!SelectedCartItem.NU__ShipMethod__c}" />

                    <apex:inputField label="Street" id="Street" value="{!SelectedCartItem.NU__ShippingStreet__c}" />
                    <apex:inputField label="City" id="City" value="{!SelectedCartItem.NU__ShippingCity__c}" />
                    <apex:inputField label="State" id="State" value="{!SelectedCartItem.NU__ShippingState__c}" />
                    <apex:inputField label="Postal Code" id="PostalCode" value="{!SelectedCartItem.NU__ShippingPostalCode__c}" />
                    <apex:inputField label="Country" id="Country" value="{!SelectedCartItem.NU__ShippingCountry__c}" />
                </apex:pageblockSection>

                <apex:pageblockSection columns="1" id="orderShippingAddressesCol"
                    rendered="{! hasCustomerShippingAddress || hasCustomerBillingAddress || (ShowMailingAddress && hasCustomerMailingAddress) || (ShowOtherAddress && hasCustomerOtherAddress)}">
                    <apex:pageblocksectionitem >
                        <apex:outputPanel />
                        <apex:outputPanel styleClass="ShippingAddressContainer">
                            <div class="ShippingShippingAddressBlock" style="{! if (hasCustomerShippingAddress, '', 'display: none;') }">
                                <c:Address street="{!SelectedCartItem.Customer__r.ShippingStreet}"
                                           city="{!SelectedCartItem.Customer__r.ShippingCity}"
                                           state="{!SelectedCartItem.Customer__r.ShippingState}"
                                           postalcode="{!SelectedCartItem.Customer__r.ShippingPostalCode}"
                                              country="{!SelectedCartItem.Customer__r.ShippingCountry}" />
                                <a href="javascript:copyFromShipping();">Copy From Shipping</a>
                            </div>

                            <div class="ShippingBillingAddressBlock" style="{! if (hasCustomerBillingAddress, '', 'display: none;') }">
                                <c:Address street="{!SelectedCartItem.Customer__r.BillingStreet}"
                                           city="{!SelectedCartItem.Customer__r.BillingCity}"
                                              state="{!SelectedCartItem.Customer__r.BillingState}"
                                              postalcode="{!SelectedCartItem.Customer__r.BillingPostalCode}"
                                              country="{!SelectedCartItem.Customer__r.BillingCountry}" />
                                <a href="javascript:copyFromBilling();">Copy From Billing</a>
                            </div>

                            <div class="ShippingMailingAddressBlock" style="{! if (ShowMailingAddress && hasCustomerMailingAddress, '', 'display: none;') }">
                                <c:Address street="{!SelectedCartItem.Customer__r.PersonContact__r.MailingStreet}"
                                           city="{!SelectedCartItem.Customer__r.PersonContact__r.MailingCity}"
                                              state="{!SelectedCartItem.Customer__r.PersonContact__r.MailingState}"
                                              postalcode="{!SelectedCartItem.Customer__r.PersonContact__r.MailingPostalCode}"
                                           country="{!SelectedCartItem.Customer__r.PersonContact__r.MailingCountry}" />
                                <a href="javascript:copyFromMailing();">Copy From Mailing</a>
                            </div>

                            <div class="ShippingOtherAddressBlock" style="{! if (ShowOtherAddress && hasCustomerOtherAddress, '', 'display: none;') }">
                                <c:Address street="{!SelectedCartItem.Customer__r.PersonContact__r.OtherStreet}"
                                           city="{!SelectedCartItem.Customer__r.PersonContact__r.OtherCity}"
                                              state="{!SelectedCartItem.Customer__r.PersonContact__r.OtherState}"
                                              postalcode="{!SelectedCartItem.Customer__r.PersonContact__r.OtherPostalCode}"
                                              country="{!SelectedCartItem.Customer__r.PersonContact__r.OtherCountry}" />
                                <a href="javascript:copyFromOther();">Copy From Other</a>
                            </div>

                            <a id="ResetOriginalShippingAddress" href="javascript:resetAddress();">Reset To Original Shipping Address</a>
                        </apex:outputPanel>
                    </apex:pageblocksectionitem>

                </apex:pageblockSection>

            </apex:pageBlock>

            <div class="buttons" style="clear: both;">
                <apex:commandButton value="Save" action="{!saveShipping}" styleClass="green" rerender="ShippingDialogPanel, CartItems" status="AS" />
                <apex:commandButton value="Cancel" action="{!cancelShipping}" rerender="ShippingDialogPanel, CartItems" status="AS" />
            </div>

            <script type="text/javascript">
                var orderShippingFormInputsDiv = document.getElementById("{!$Component.orderShippingForm.orderShippingFormInputs}");

                if (orderShippingFormInputsDiv) { orderShippingFormInputsDiv.className = 'orderShippingFormInputs'; }

                var orderShippingAddressesColDiv = document.getElementById("{!$Component.orderShippingForm.orderShippingAddressesCol}");

                if (orderShippingAddressesColDiv) { orderShippingAddressesColDiv.className = 'orderShippingAddressesCol'; }

                var shippingAddress = {
                    street: "{!JSEncode(SelectedCartItem.Customer__r.ShippingStreet)}",
                    city: "{!JSEncode(SelectedCartItem.Customer__r.ShippingCity)}",
                    state: "{!JSEncode(SelectedCartItem.Customer__r.ShippingState)}",
                    postalCode: "{!JSEncode(SelectedCartItem.Customer__r.ShippingPostalCode)}",
                    country: "{!JSEncode(SelectedCartItem.Customer__r.ShippingCountry)}"
                };

                var billingAddress = {
                    street: "{!JSEncode(SelectedCartItem.Customer__r.BillingStreet)}",
                    city: "{!JSEncode(SelectedCartItem.Customer__r.BillingCity)}",
                    state: "{!JSEncode(SelectedCartItem.Customer__r.BillingState)}",
                    postalCode: "{!JSEncode(SelectedCartItem.Customer__r.BillingPostalCode)}",
                    country: "{!JSEncode(SelectedCartItem.Customer__r.BillingCountry)}"
                };

                var mailingAddress = {
                    street: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.MailingStreet)}",
                    city: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.MailingCity)}",
                    state: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.MailingState)}",
                    postalCode: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.MailingPostalCode)}",
                    country: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.MailingCountry)}"
                };

                var otherAddress = {
                    street: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.OtherStreet)}",
                    city: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.OtherCity)}",
                    state: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.OtherState)}",
                    postalCode: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.OtherPostalCode)}",
                    country: "{!JSEncode(SelectedCartItem.Customer__r.PersonContact__r.OtherCountry)}"
                };

                var originalAddress = {
                    street: "{!JSEncode(SelectedCartItem.ShippingStreet__c)}",
                    city: "{!JSEncode(SelectedCartItem.ShippingCity__c)}",
                    state: "{!JSEncode(SelectedCartItem.ShippingState__c)}",
                    postalCode: "{!JSEncode(SelectedCartItem.ShippingPostalCode__c)}",
                    country: "{!JSEncode(SelectedCartItem.ShippingCountry__c)}"
                };

                function copyFromShipping(){
                    copyAddressToFields(shippingAddress);
                }

                function copyFromBilling(){
                    copyAddressToFields(billingAddress);
                }

                function copyFromMailing(){
                    copyAddressToFields(mailingAddress);
                }

                function copyFromOther(){
                    copyAddressToFields(otherAddress);
                }

                function resetAddress(){
                    copyAddressToFields(originalAddress);
                }

                function copyAddressToFields(address){
                    var streetInput = document.getElementById("{!$Component.orderShippingForm.orderShippingFormInputs.Street}");

                    streetInput.value = address.street;

                    var cityInput = document.getElementById("{!$Component.orderShippingForm.orderShippingFormInputs.City}");

                    cityInput.value = address.city;

                    var stateInput = document.getElementById("{!$Component.orderShippingForm.orderShippingFormInputs.State}");

                    stateInput.value = address.state;

                    var postalCodeInput = document.getElementById("{!$Component.orderShippingForm.orderShippingFormInputs.PostalCode}");

                    postalCodeInput.value = address.postalCode;

                    var countryInput = document.getElementById("{!$Component.orderShippingForm.orderShippingFormInputs.Country}");

                    countryInput.value = address.country;
                }
            </script>

        </apex:outputPanel>
    </c:CustomDialog>

    <c:CustomDialog id="Tax" title="How do you want to tax?" closeable="false">
        <apex:outputPanel id="TaxDialogPanel">
            <apex:pageMessages />
            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!TaxDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:outputText value="There are no active sales tax records defined for entity {! JSENCODE(SelectedCartItem.Entity__r.Name) }" rendered="{!SalesTaxCount == 0}" />

            <apex:pageBlock mode="maindetail" id="orderShippingForm" rendered="{! SalesTaxCount > 0}">
                <apex:pageblockSection columns="1" id="orderShippingFormInputs">

                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.NU__CartItem__c.Fields.NU__SalesTax__c.Label}" />
                        <apex:selectList multiselect="false" size="1" value="{!SelectedCartItem.NU__SalesTax__c}">
                            <apex:selectOption itemvalue="" itemLabel="Please select a {!$ObjectType.NU__CartItem__c.Fields.NU__SalesTax__c.Label}" />
                            <apex:selectOptions value="{!SalesTaxSelectOptions}"/>
                            <apex:actionSupport event="onchange" action="{!SalesTaxSelected}" rerender="TaxDialogPanel" status="AS"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.NU__CartItem__c.Fields.NU__TotalTax__c.Label}" />
                        <apex:outputPanel >
                            <apex:outputField value="{!SelectedCartItem.NU__TotalTax__c}" />
                            <span class="leftSpace">
                                <apex:outputField value="{!SelectedCartItem.NU__TaxableAmount__c}" /> &nbsp;* &nbsp;<apex:outputField value="{!SelectedSalesTax.NU__TaxRate__c}" />
                            </span>
                        </apex:outputPanel>
                    </apex:pageBlockSectionItem>

                    <apex:pageBlockSectionItem >
                        <apex:outputText value="{!$ObjectType.Account.Fields.NU__TaxExempt__c.Label}" />
                        <apex:outputField value="{!SelectedCartItem.Customer__r.NU__TaxExempt__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageblockSection>
            </apex:pageBlock>

            <div class="buttons" style="clear: both;">
                   <apex:commandButton value="Save" action="{!saveTax}" rendered="{! SalesTaxCount > 0 }" styleClass="green" rerender="TaxDialogPanel, CartItems" status="AS" />
                <apex:commandButton value="Cancel" action="{! cancelTax }" rerender="TaxDialogPanel, CartItems" status="AS" />
            </div>
        </apex:outputPanel>
    </c:CustomDialog>

</apex:component>