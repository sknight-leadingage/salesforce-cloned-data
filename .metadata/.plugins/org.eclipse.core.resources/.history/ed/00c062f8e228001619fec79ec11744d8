trigger MembershipTrigger on NU__Membership__c (before delete, before insert, before update) {
	User currentUser = UserQuerier.getCurrentUserWithStateInformation();
	
	
	if (Trigger.isInsert){
		
		Set<Id> accountIds = NU.CollectionUtil.getLookupIds(Trigger.New, 'NU__Account__c');
	
		Map<Id, Account> accounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(accountIds);
		
		List<NU__MembershipType__c> providerMTs = MembershipTypeQuerier.getProviderMembershipTypes();
		Map<Id, NU__MembershipType__c> providerMTsMap = new Map<Id, NU__MembershipType__c>(providerMTs);
		Set<Id> providerMTIds = providerMTsMap.keySet();
		
		List<Account> accountsToUpdate = new List<Account>();
		
		for (NU__Membership__c newMembership : Trigger.New){
			Account account = accounts.get(newMembership.NU__Account__c);
			
			if (providerMTIds.contains(newMembership.NU__MembershipType__c) &&
			    ( // Rejoin
			      account.Provider_Lapsed_On__c < newMembership.NU__StartDate__c ||
			      // Renewal
			      account.Provider_Lapsed_On__c <= newMembership.NU__EndDate__c
			    )){
			    
			    account.Provider_Lapsed_On__c = null;
			    
			    accountsToUpdate.add(account);
		    }
		}
		
		if (accountsToUpdate.size() > 0){
			update accountsToUpdate;
		}
	}
	

	// not a state partner portal user.
	if (currentUser.ContactId == null){
		return;
	}
	
	// Validate that state partner portal users can only insert, update, and
	// delete memberships in their state.
	
	List<NU__Membership__c> affectedMemberships = Trigger.isDelete ? Trigger.old : Trigger.new;
	
	Set<Id> accountIds = NU.CollectionUtil.getLookupIds(affectedMemberships, 'NU__Account__c');
	
	Map<Id, Account> accounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(accountIds);
	
	Id userStatePartnerId = currentUser.Contact.Account.State_Partner_Id__c;
	
	for (NU__Membership__c affectedMembership : affectedMemberships){
		Account account = accounts.get(affectedMembership.NU__Account__c);
		
		if (account.State_Partner_Id__c != userStatePartnerId){
			affectedMembership.addError(Constant.STATE_PARTNER_EDIT_PROVIDER_MEMBERSHIP_VAL_MSG);
		}
	}
}