<apex:page title="AR Statements" standardController="Account" renderAs="{!render}" extensions="ARStatementController" showHeader="false" sidebar="false" standardStylesheets="false" recordsetvar="a" id="page">
	<style type="text/css">
        body { margin: 0px; }
        img { border: 0px; }
        .ui-dialog {
        	font-size: 75%;
        }
        #buttons {
            margin: 10px;
            font-size: 75%;
            font-family: Arial, Helvetica, sans-serif;
        }
        .btn {
            padding: 4px 3px;
            color: #333;
            margin: 1px;
            border: 1px solid #B5B5B5;
            border-bottom-color: #7F7F7F;
            background: #E8E8E9 url('/img/alohaSkin/btn_sprite.png') repeat-x right top;
            font-weight: bold;
            font-size: .9em;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }
        .btn:hover {
            background-position: right -30px;
        }
        .btn:active {
            background-position: right -60px;
            border-color: #585858;
            border-bottom-color: #939393;
        }
		.message{-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;background-color:#FFC;border-style:solid;border-width:1px;padding:6px 8px 6px 6px;margin:4px 20px}
		.errorM3{border-color:#C00}
		.errorM3 .msgIcon{background-image:url(/img/msg_icons/error24.png);background-position:0 0;width:24px;height:24px}
		.warningM3{border-color:#f90}
		.warningM3 .msgIcon{background-image:url(/img/msg_icons/warning24.png);background-position:0 0;width:24px;height:24px}
		.infoM3{border-color:#39f}
		.infoM3 .msgIcon{background-image:url(/img/msg_icons/info24.png);background-position:0 0;width:24px;height:24px}
		.confirmM3{border-color:#390}
		.confirmM3 .msgIcon{background-image:url(/img/msg_icons/confirm24.png);background-position:0 0;width:24px;height:24px}
		.message .messageText{margin-left:8px}
		.message .messageTable .messageCell{vertical-align:middle}
		.message .messageTable .messageCell h4{margin:0}
		.errorMsg{color:#d74c3b}
        
        .leftmargin { margin-left: 10px; }
		
		.requiredBlock {
		    background-color: rgb(204, 0, 0);
		    width: 3px;
		}
	</style>
    <style type="text/css" media="print">
        #buttons { display:none; visibility: hidden; }
    </style>
    <apex:stylesheet value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/smoothness/jquery-ui.css" />
	<apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" />
	<apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" />
	
	<apex:outputPanel rendered="{!!RenderPDF}">
	<div id="buttons">
		<apex:form id="form">
			<apex:pageMessages id="msgs" escape="false" />
			<apex:outputPanel >
		    	<script type="text/javascript">
		    		<apex:outputText value="{!invoiceDialogScript}" />
		    	</script>
		    </apex:outputPanel>
		    
		    <apex:actionRegion >
			    <apex:outputLabel >Select Entity:&nbsp;</apex:outputLabel>
				<span class="requiredBlock">&nbsp;</span><apex:selectList size="1" value="{!entityId}">
				    <apex:selectOption itemValue="" itemLabel="Select..."/>
				    <apex:selectOptions value="{!EntityOptions}"/>
				</apex:selectList>
			    <br /><br />
		        <apex:outputLabel >Statement Date Override:&nbsp;</apex:outputLabel>
		        <apex:inputField value="{!DummyOrder.NU__InvoiceDate__c}" />
		        <br /><br />
		        <apex:outputLabel >Minimum Balance Override:&nbsp;</apex:outputLabel>
		        <apex:inputText value="{!minimumBalanceOverride}" />
		        <br /><br />
		        <div>
				<!--  <apex:outputText value="{!$ObjectType.NU__Order__c.Fields.Invoice_Attention_To__c.Label}" />
				<apex:inputField value="{!Attentionto.Name}" />
				&nbsp;&nbsp;-->
                 if applicable available Invoice/Prime contact:&nbsp;&nbsp; <apex:selectList id="AdditionalInvoiceContacts"
                                     multiselect="false"
                                     size="1"
                                     value="{!Attentionto.Name }">
                            <apex:selectOption itemLabel="Please select a name" itemValue="" />
                            <apex:selectOptions value="{! AdditionalInvoiceContactsOpts }"/>
                        </apex:selectList>
			    </div>
	        </apex:actionRegion>

			<apex:outputPanel id="ARStatementActionButtons">
				<div class="buttons" style="clear: both; margin-top: 10px;">
					<apex:commandButton value="Generate"
				        action="{!Generate}"
				        rerender="ARStatementActionButtons,ARStatementComponent,msgs"
				        styleClass="btn"
				        title="Generates AR Statements based on criteria specified above." />
					<apex:commandButton value="PDF"
				   	    action="{!PDF}"
				   	    rendered="{!CanRender}"
				   	    styleClass="btn leftmargin"
				   	    title="Rerenders the page as a PDF." />
				   	<apex:commandButton value="Print"
				   	    action="{!Print}"
				   	    rendered="{!CanRender}"
				   	    styleClass="btn leftmargin"
				   	    title="Opens a dialog window to print the renewal notices." />
					<apex:commandButton value="Cancel"
					    action="{!cancel}"
					    styleClass="btn leftmargin"
					    title="Cancel invoicing and return to the last page." />
				</div>
			</apex:outputPanel>
			
		</apex:form>
		<hr/>
	</div>
	</apex:outputPanel>
    <c:ARStatement id="ARStatementComponent" accts="{!IF(CanRender,Accounts,null)}" attn="{!Attentionto}" />
</apex:page>