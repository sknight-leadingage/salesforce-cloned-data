global with sharing class CustomProviderMembershipFlowdownManager extends CustomMembershipFlowdownManagerBase {
	private Set<Id> changedProviderLapsedOnAcctIds = null;
	private Map<Id, NU__Affiliation__c> unchangedAffils = null;
	private Map<Id, NU__Affiliation__c> changedAffils = null;
	
	protected override List<NU__Affiliation__c> getChildAffiliations(Set<Id> changedMembershipAcctIds, String membershipFieldName, String membershipTypeLookupFieldName, String membershipRelationFieldName, String membershipJoinOnFieldName){

    	String childAffilQuery = String.format('Select Id, NU__ParentAccount__c, NU__ParentAccount__r.Provider_Lapsed_On__c, NU__ParentAccount__r.{4}, NU__ParentAccount__r.RecordTypeId, NU__ParentAccount__r.{2}, NU__ParentAccount__r.{3}.NU__Pending__c, NU__Account__c, NU__Account__r.RecordTypeId, NU__Account__r.Provider_Lapsed_On__c, NU__Account__r.{0}, NU__Account__r.{2}, NU__Account__r.IsPersonAccount, NU__Account__r.{3}.NU__EndDate__c, NU__Account__r.{3}.NU__Account__c, NU__Account__r.{3}.NU__Pending__c, NU__Account__r.{4}, NU__IsPrimary__c, Flowdown_Company_Membership__c from NU__Affiliation__c where NU__IsPrimary__c = true and NU__ParentAccount__c in {1}',
    	                                       new List<String> { membershipFieldName, NU.CollectionUtil.getIdList(changedMembershipAcctIds), membershipTypeLookupFieldName, membershipRelationFieldName, membershipJoinOnFieldName });
    	
    	system.debug('  getChildAffiliations::childAffilQuery ' + childAffilQuery);

    	return (List<NU__Affiliation__c>) Database.query(childAffilQuery);
    }
	
	protected override void additionalFlowdownDetected(Account unchangedAcct, Account changedAcct, Set<Id> changedAcctIds){
		if (unchangedAcct.Provider_Lapsed_On__c != changedAcct.Provider_Lapsed_On__c){
			if (changedProviderLapsedOnAcctIds == null){
				changedProviderLapsedOnAcctIds = new Set<Id>();
			}
			
			changedProviderLapsedOnAcctIds.add(unchangedAcct.Id);
			changedAcctIds.add(unchangedAcct.Id);
		}
	}
	
	protected override void additionalAccountUpdatedFlowdown(NU__Affiliation__c childAffil, Account unchangedParentAccount, Account changedParentAccount, Map<Id, Account> childAcctsToUpdateMap){
		system.debug('   additionalAccountUpdatedFlowdown::childAffil ' + childAffil);
		system.debug('   additionalAccountUpdatedFlowdown::changedProviderLapsedOnAcctIds ' + changedProviderLapsedOnAcctIds);
		system.debug('   additionalAccountUpdatedFlowdown::unchangedParentAccount ' + unchangedParentAccount);
		system.debug('   additionalAccountUpdatedFlowdown::changedParentAccount ' + changedParentAccount);
		
    	if (changedProviderLapsedOnAcctIds != null &&
    	    changedProviderLapsedOnAcctIds.contains(childAffil.NU__ParentAccount__c) &&
    	    childAffil.NU__Account__r.Provider_Lapsed_On__c == unchangedParentAccount.Provider_Lapsed_On__c){
    	    
    	    Account childAcctToUpdate = childAcctsToUpdateMap.containsKey(childAffil.NU__Account__c) ?
    	                                  childAcctsToUpdateMap.get(childAffil.NU__Account__c) :
    	                                  new Account(Id = childAffil.NU__Account__c);

            childAcctToUpdate.Provider_Lapsed_On__c = changedParentAccount.Provider_Lapsed_On__c;
            
            childAcctsToUpdateMap.put(childAcctToUpdate.Id, childAcctToUpdate);
	    }
    }
    
    protected override void additionalAffiliationUpdateFlowdownDetected(NU__Affiliation__c unchangedAffil, NU__Affiliation__c changedAffil, Set<Id> changedAffilIds){
    	if (unchangedAffils == null){
    		unchangedAffils = new Map<Id, NU__Affiliation__c>();
    	}
    	
    	if (changedAffils == null){
    		changedAffils = new Map<Id, NU__Affiliation__c>();
    	}
    	
    	if (unchangedAffil.NU__IsPrimary__c == true &&
    	    changedAffil.NU__IsPrimary__c == false){
    	    unchangedAffils.put(unchangedAffil.Id, unchangedAffil);
    	    changedAffils.put(changedAffil.Id, changedAffil);
	    }
    }
    
    protected override void additionalAffiliationUpdatedFlowdown(List<Account> accountsToUpdate){
    	for (NU__Affiliation__c changedAffil : changedAffils.values()){
    		Account accountToUpdate = findAccountInList(changedAffil.NU__Account__c, accountsToUpdate);
    		
    		if (accountToUpdate == null){
    			accountToUpdate = new Account(Id = changedAffil.NU__Account__c);
    			accountsToUpdate.add(accountToUpdate);
    		}

    		accountToUpdate.Provider_Lapsed_On__c = null;
    	}
    }
    
    protected override void setInstanceMembers(){
    	additionalFlowdownNeeded = true;
    	flowdownToIndividualsOnly = true;
    }
    
    private Account findAccountInList(Id accountId, List<Account> accountsToSearch){
    	for (Account searchAccount : accountsToSearch){
    		if (searchAccount.Id == accountId){
    			return searchAccount;
    		}
    	}
    	
    	return null;
    }
}