/**
 * @author NimbleUser
 * @date Updated: 2/18/13
 * @description This class provides various functions for creating and inserting Joint Billing records.
 * It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Joint Billings without inserting them. The insert* functions create 
 * Joint Billings and insert them.
 */
public with sharing class DataFactoryJointBilling {

	public static Joint_Billing__c createJointBilling(){
		Account statePartner = DataFactoryAccountExt.insertStatePartnerAccount();
		
		return new Joint_Billing__c(
			State_Partner__c = statePartner.Id,
			Start_Date__c = Date.newInstance(Date.Today().year(), 1, 1),
			End_Date__c = Date.newInstance(Date.Today().year(), 12, 31),
			Bill_Date__c = Date.Today()
		);
	}
	
	public static Joint_Billing__c createJointBilling(Id statePartnerAccountId, Date endDate){
		return new Joint_Billing__c(
			State_Partner__c = statePartnerAccountId,
			Start_Date__c = endDate.addDays(1),
			End_Date__c = endDate,
			Bill_Date__c = Date.Today()
		);
	}
	
	public static Joint_Billing__c insertJointBilling(){
		Joint_Billing__c jb = createJointBilling();
		insert jb;
		
		return jb;
	}
	
	public static Joint_Billing__c insertJointBillingWithItemAndItemLine(Id statePartnerAccountId, Id providerAccountId, Date endDate, Decimal duesPrice){
		Joint_Billing__c jb = createJointBilling(statePartnerAccountId, endDate);
		insert jb;
		
		Joint_Billing_Item__c jbi = JointBillingUtil.createJointBillingItem(providerAccountId, jb.Id);
		insert jbi;
		
		Joint_Billing_Item_Line__c jbil = JointBillingUtil.createJointBillingItemLine(duesPrice, Date.Today(), Constant.JOINT_BILLING_ITEM_LINE_TYPE_NEW);
		jbil.Joint_Billing_Item__c = jbi.Id;
		insert jbil;
		
		return jb;
	}
}