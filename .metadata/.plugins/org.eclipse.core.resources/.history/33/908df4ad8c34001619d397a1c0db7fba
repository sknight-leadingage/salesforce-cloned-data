/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the ConferenceProposalToSessionConverter class logi
 */
@isTest
private class TestConferenceProposalToSessionConverter {
    
    static void assertConstructorValidation(Set<Id> confProposalIds, String expectedValMsg){
        String actualValMsg = '';
        
        try{
            ConferenceProposalToSessionConverter converter = new ConferenceProposalToSessionConverter(confProposalIds);
        }
        catch (Exception ex){
            actualValMsg = ex.getMessage();
        }
        
        TestUtil.assertValidationMessage(actualValMsg, expectedValMsg);
    }
    
    static void assertConvertedSessions(Set<Id> proposalIdsToConvert, List<Conference_Session__c> convertedSessions){
        system.assert(NU.CollectionUtil.setHasElements(proposalIdsToConvert), 'There are no proposal ids to convert.');
        system.assert(NU.CollectionUtil.listHasElements(convertedSessions), 'There are no converted sessions.');
        
        // confirm all sessions inserted
        
        for (Conference_Session__c convertedSession : convertedSessions){
            system.assert(convertedSession.Id != null, 'The converted session was not inserted. ' + convertedSession);
        }
        
        List<Conference_Proposal__c> proposals = ConferenceProposalQuerier.getConferenceProposalsByIds(proposalIdsToConvert);
        
        //Map<Id, Conference_Proposal__c> proposalsMap = new Map<Id, Conference_Proposal__c>( props );
        Map<Id, Conference_Session__c> convertedSessionsMap = new Map<Id, Conference_Session__c>( convertedSessions );
        
        for (Conference_Proposal__c proposal : proposals){
            system.assert(proposal.Session__c != null, 'The session was not updated on the converted proposal. ' + proposal);
            
            Conference_Session__c session = convertedSessionsMap.get(proposal.Session__c);
            
            system.assert(session != null, 'The proposal has a session but was not part of the converted sessions.');
            assertSessionPropertiesSetFromProposal(proposal, session);
        }
    }
    
    static void assertSessionPropertiesSetFromProposal(Conference_Proposal__c proposal, Conference_Session__c session){
        system.assertEquals(proposal.Abstract__c, session.Abstract__c);
        system.assertEquals(proposal.Conference_Track__c, session.Conference_Track__c);
        system.assertEquals(proposal.Conference__c, session.Conference__c);
        system.assertEquals(proposal.Expected_Attendance__c, session.Expected_Attendance__c);
        system.assertEquals(proposal.International__c, session.International__c);
        system.assertEquals(proposal.Learning_Objective_1__c, session.Learning_Objective_1__c);
        system.assertEquals(proposal.Learning_Objective_2__c, session.Learning_Objective_2__c);
        
        system.assertEquals(proposal.Learning_Objective_3__c, session.Learning_Objective_3__c);
        system.assertEquals(proposal.Notes__c, session.Notes__c);
        system.assertEquals(proposal.Sponsorship_Level__c, session.Sponsorship_Level__c);
        system.assertEquals(proposal.Submitter__r.Account__c, session.Submitter__c);
        
        /*      
        proposal.Submitter__c, 
        proposal.Status__c,
        proposal.Sponsorship_Level__c,
        proposal.Session__c,
        proposal.Session_Length__c,
        proposal.Notes__c,
        proposal.Learning_Objective_3__c,
        proposal.Learning_Objective_2__c,
        proposal.Learning_Objective_1__c,
        proposal.International__c, 
        proposal.Expected_Attendance__c,
        proposal.Division__c,
        proposal.Date__c, 
        proposal.Counter__c,
        proposal.Conference__c,
        proposal.Conference_Track__c,
        proposal.Completed__c,
        proposal.Abstract__c,
        proposal.
*/
    }

    static testMethod void noConfProposalIdsValTest() {
        assertConstructorValidation(new Set<Id>(), ConferenceProposalToSessionConverter.NO_CONFERENCE_PROPOSAL_IDS_VAL_MSG);
    }
    
    static testMethod void nullConfProposalIdsValTest() {
        assertConstructorValidation(null, ConferenceProposalToSessionConverter.NO_CONFERENCE_PROPOSAL_IDS_VAL_MSG);
    }
    
    static testmethod void noConferenceProposalsFoundValTest(){
        Conference_Proposal__c submittedProposal = DataFactoryConferenceProposal.insertConferenceProposal();
        
        Id submittedProposalId = submittedProposal.Id;
        
        delete submittedProposal;
        
        assertConstructorValidation(new Set<Id>{ submittedProposalId }, ConferenceProposalToSessionConverter.NO_CONFERENCE_PROPOSALS_FOUND_VAL_MSG);
    }
    
    static testmethod void unacceptedProposalFoundValTest(){
        Conference_Proposal__c submittedProposal = DataFactoryConferenceProposal.insertConferenceProposal();
        
        assertConstructorValidation(new Set<Id>{ submittedProposal.Id }, ConferenceProposalToSessionConverter.NO_UNACCEPTED_CONFERENCE_PROPOSALS_ALLOWED_VAL_MSG);
    }
    
    static testmethod void alreadyConvertedProposalValTest(){
        Conference_Session__c session = DataFactoryConferenceSession.insertConferenceSession();
        Conference_Proposal__c submittedProposal = DataFactoryConferenceProposal.insertAcceptedConferenceProposal(session.Id);
        
        assertConstructorValidation(new Set<Id>{ submittedProposal.Id }, ConferenceProposalToSessionConverter.ALREADY_CONVERTED_PROPOSALS_FOUND_VAL_MSG);
    }
    
    static testmethod void allSpeakersNeedsAccountsValTest(){
        Conference_Proposal__c proposal = DataFactoryConferenceProposal.insertAcceptedConferenceProposal(null);
        
        Conference_Speaker__c additionalSpeaker = DataFactoryConferenceSpeaker.insertConferenceSpeakerWithoutAccount();
        
        DataFactoryConferenceProposalSpeaker.insertConferenceProposalSpeaker(proposal.Id, additionalSpeaker.Id);
        
        assertConstructorValidation(new Set<Id>{ proposal.Id }, ConferenceProposalToSessionConverter.ALL_SPEAKERS_MUST_HAVE_ACCOUNTS_VAL_MSG);
    }
    
    static testmethod void convertProposalTest(){
        Conference_Proposal__c proposal = DataFactoryConferenceProposal.createConferenceProposal(null, Constant.CONFERENCE_PROPOSAL_STATUS_ACCEPTED);
        proposal.Title__c = 'Test Proposal';
        //proposal.Sponsorship_Level__c = DataFactoryProductExt.
        proposal.Session_Length__c = '2.5 hours';
        proposal.Notes__c = 'Some notes';
        proposal.Learning_Objective_3__c = 'Learning Objective 3';
        proposal.Learning_Objective_2__c = 'Learning Objective 2';
        proposal.Learning_Objective_1__c = 'Learning Objective 1';
        proposal.International__c = false;
        proposal.Expected_Attendance__c = 100;
        proposal.Division__c = DataFactoryDivision.insertDivision().Id;
        proposal.Date__c = Date.Today().addDays(-3); 
        proposal.Counter__c = 1;
        proposal.Conference_Track__c = DataFactoryConferenceTrack.insertConferenceTrack(proposal.Conference__c).Id;
        proposal.Completed__c = true;
        proposal.Abstract__c = 'A very well written abstract that made it into the conference';
        
        insert proposal;
        
        Set<Id> proposalIdsToConvert = new Set<Id>{ proposal.Id };
        
        List<Conference_Session__c> sessions = new ConferenceProposalToSessionConverter(proposalIdsToConvert).convert();
        
        assertConvertedSessions(proposalIdsToConvert, sessions);
    }
}