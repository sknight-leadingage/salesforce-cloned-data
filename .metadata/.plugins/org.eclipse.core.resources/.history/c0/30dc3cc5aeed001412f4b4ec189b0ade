<apex:page controller="CongressionalDistrictReportController" showHeader="{!!isCheckedHideMenus}" sidebar="{!!isCheckedHideMenus}" renderAs="{!IF(isCheckedRenderAsPDF,'pdf', null)}" applyHtmlTag="{!!isCheckedHideMenus}">
    <head>
    <style type="text/css">
        .headline {
            font-family: Cambria, Times, "Times New Roman", serif;
            font-size: 24px;
            font-weight: bold;
            color: #365F91;
        }
        .subheadline {
            font-family: Cambria, Times, "Times New Roman", serif;
            font-size: 14px;
            font-weight: bold;
            color: #365F91;
        }
        .count {
            font-family: Cambria, Times, "Times New Roman", serif;
            font-size: 11px;
            font-weight: bold;
            color: #4F81BD;
        }
        .entry {
            break-inside: avoid-column;
            -webkit-column-break-inside: avoid;
            page-break-inside: avoid;
            height: 150px;
            width: 45%;
            padding: 5px 5px 5px 25px;
            overflow-y: hidden;
            float: left;
        }
        .business {
            font-family: Calibri, Arial, Helvetica, sans-serif;
            font-size: 16px;
            font-weight: bold;
        }
        .services {
            font-family: Calibri, Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-style: italic;
        }
        .address {
            font-family: Calibri, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        @media screen {
            div.divFooter {
                display: none;
            }
        }
    </style>
    <style type="text/css" media="print">
            .printhide {
                display: none;
            }
            .bPageTitle {
                display: none;
            }
            div.divFooter {
                font-family: Calibri, Arial, Helvetica, sans-serif;
                font-size: 12px;
                font-weight: bold;
                align: center;
                bottom: 0;
            }
    </style>
    </head>
  
	<body style="font-family: Calibri, Arial, Helvetica, sans-serif;">
    <apex:outputPanel rendered="{!IF(isCheckedRenderAsPDF,false,true)}">
    <apex:define name="sectionHeader">
        <div style="float: left;"><apex:sectionHeader title="Congressional District Report" help="#"/></div>
        <img style="float: right" src="{!$Resource.Report_Logo}" />
        <apex:pageMessages />
    </apex:define>
    
    <div style="float: none; clear: both">
    <apex:form id="options">
        <apex:selectList value="{!SelectedState}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select State --" />
            <apex:selectOptions value="{!states}"/>
        </apex:selectList>

        <apex:selectList value="{!SelectedDistrict}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select District --" />
            <apex:selectOptions value="{!districts}"/>
        </apex:selectList>

        <apex:selectList value="{!SelectedTypeProvider}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select Provider Type --" />
            <apex:selectOptions value="{!types}"/>
        </apex:selectList>
        
        <apex:selectList value="{!SelectedLayout}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select layout --" />
            <apex:selectOption itemValue="1" itemLabel=" -- Internal --"/>
            <apex:selectOption itemValue="2" itemLabel=" -- Senate offices --"/>
            <apex:selectOption itemValue="3" itemLabel=" -- Congressional offices --"/>
        </apex:selectList>

        <apex:commandButton action="{!Run}" value="Run Report"/>
        <br />
        Show as PDF (Check this box and click Run Report for well-formatted print output) <apex:inputCheckbox value="{!isCheckedRenderAsPDF}"/>
        <br />
        Hide Menus <apex:inputCheckbox value="{!isCheckedHideMenus}"/>
        <!-- Render as PDF <apex:inputCheckbox value="{!isCheckedRenderAsPDF}"/>  -->

    </apex:form>
    </div><div style="float: none; clear: both"></div>
    </apex:outputPanel>

        <img style="center; clear: both" src="{!$Resource.Report_Logo}" />

    <div style="text-align: center; clear: both">
    <p><span class="headline">LeadingAge Members: {!SelectedStateName} - {!SelectedDistrictOrdinal} ({!RecordsCount})</span></p><br />
    </div>
    
    <br />
    <br />
    <br />
    <br />

    <div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14pt">
        <apex:repeat value="{!records}" var="r" >
              
            <div style="page-break-inside: avoid; width: 45%; padding: 5px 5px 5px 15px; float: left;">
            <div style="position: relative; break-inside: avoid-column; -webkit-column-break-inside: avoid; min-height: 150px; max-height: 290px; overflow-y: hidden;">
            <span style="font-size: 16pt; font-weight: bold;"><apex:outputtext value="{!r.Name}" /></span><br />
			<apex:outputPanel rendered="{!IF(SelectedLayout=1,true,false)}">
            <span class="address"><apex:outputText rendered="{!IF(r.NU__Affiliates__r.size > 0, TRUE, FALSE)}">
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.Name}" />, &nbsp;
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.PersonTitle}" /> &nbsp;|&nbsp;
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.PersonEmail}" />
            </apex:outputText></span><br />
            </apex:outputPanel>
			<apex:outputPanel rendered="{!IF(SelectedLayout=1 || SelectedLayout=3,true,false)}">
            <span class="address"><apex:outputtext value="{!r.BillingStreet}" /><br />
            <apex:outputtext value="{!r.BillingCity}" />,&nbsp;<apex:outputtext value="{!r.BillingState}" />&nbsp;<apex:outputtext value="{!r.BillingPostalCode}" /></span><br />
            </apex:outputPanel>
            <apex:outputPanel rendered="{!IF(SelectedLayout=2,true,false)}">
            <span class="address">
            <apex:outputtext value="{!r.BillingCity}" />,&nbsp;<apex:outputtext value="{!r.BillingState}"/> </span><br />
            </apex:outputPanel>
            <span class="services"><apex:outputtext value="{!r.Provider_Type__c}" /></span>
            </div>
            </div>
        </apex:repeat>
    </div>

    <div style="float: none; clear: both; padding-top: 25px;">
        <div class="divFooter" style="text-align: center; clear: both; position: relative;">
        <p>2519 Connecticut Ave. NW Washington DC 20008 | P 202.783.2252 | Larry Minnix, CEO</p>
        </div>    
    </div><div style="float: none; clear: both"></div>
    </body>

</apex:page>