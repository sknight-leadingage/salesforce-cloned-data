/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the AccountQuerier class logic.
 */
@isTest
private class TestAccountRecordTypeUtil {
	static void assertAccountRecordTypes(List<RecordType> accountRecordTypes, Boolean isPersonAccountType){
		system.assert(NU.CollectionUtil.listHasElements(accountRecordTypes), 'No account record types found.');
		system.assert(isPersonAccountType != null, 'Is Person Account Type is null.');
		
		for (RecordType accountRT : accountRecordTypes){
			system.assertEquals(isPersonAccountType, accountRT.IsPersonType);
		}
	}
	
	static void assertIsRecordType(RecordType accountRecordType, String expectedRecordTypeName){
		system.assert(accountRecordType != null, 'The account record type is null.');
		system.assertEquals(expectedRecordTypeName, accountRecordType.Name, 'The account record type name is not the expected one.');
	}

    static testMethod void getBusinessAccountRecordTypesTest() {
        List<RecordType> businessAccountRecordTypes = AccountRecordTypeUtil.getBusinessAccountRecordTypes();
        assertAccountRecordTypes(businessAccountRecordTypes, false);
    }
    
    static testMethod void getPersonAccountRecordTypesTest() {
        List<RecordType> personAccountRecordTypes = AccountRecordTypeUtil.getPersonAccountRecordTypes();
        assertAccountRecordTypes(personAccountRecordTypes, true);
    }
    
    static testmethod void getCompanyRecordTypeTest(){
    	RecordType companyRT = AccountRecordTypeUtil.getCompanyRecordType();
    	assertIsRecordType(companyRT, Constant.ACCOUNT_RECORD_TYPE_NAME_COMPANY);
    }
    
    static testmethod void getCorporateAllianceSponsorRecordTypeTest(){
    	RecordType corpAllianceSponsorRT = AccountRecordTypeUtil.getCorporateAllianceSponsorRecordType();
    	assertIsRecordType(corpAllianceSponsorRT, Constant.ACCOUNT_RECORD_TYPE_NAME_CORPORATE_ALLIANCE_SPONSOR);
    }
    
    static testmethod void getMultiSiteRecordTypeTest(){
    	RecordType multiSiteRT = AccountRecordTypeUtil.getMultiSiteRecordType();
    	assertIsRecordType(multiSiteRT, Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE);
    }
    
    static testmethod void getStatePartnerRecordTypeTest(){
    	RecordType statePartnerRT = AccountRecordTypeUtil.getStatePartnerRecordType();
    	assertIsRecordType(statePartnerRT, Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER);
    }
    
    static testmethod void getStatePartnerPortalRecordTypeTest(){
    	RecordType statePartnerPortalRT = AccountRecordTypeUtil.getStatePartnerPortalRecordType();
    	assertIsRecordType(statePartnerPortalRT, Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER_PORTAL);
    }
}