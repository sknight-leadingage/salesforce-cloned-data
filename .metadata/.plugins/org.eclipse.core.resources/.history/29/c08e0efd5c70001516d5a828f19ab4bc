@isTest(SeeAllData=false)
public with sharing class TestAccountTrigger_Subscription {


static testmethod void testMembershipSusbscriptions() {
    
    //Create Test Suscriptions
    // Test Entity
    NU__Entity__c TestEntity = new NU__Entity__c(name='Test Entity');
    insert TestEntity;
    
    // Test General Ledger Account
    NU__GLAccount__c myGLAccount = new NU__GLAccount__c(name='xxx-112-xxx', NU__Entity__c = TestEntity.Id );
    insert myGLAccount;
    
    //Online Subscription Record Type
    Id Prod_Record_Type_Id = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName().get('Online Subscription').getRecordTypeId();
    
    // Test Subscription Products               
    NU__Product__c product1 = 
            new NU__Product__c(name='Test Member Defailt, Not Restricted', 
                             RecordTypeId = Prod_Record_Type_Id,
                             NU__Entity__c = TestEntity.Id,
                             NU__RevenueGLAccount__c = myGLAccount.id,
                             NU__DisplayOrder__c=1, 
                             NU__ListPrice__c=0, 
                             NU__QuantityMax__c=1,
                             Membership_Default__c = true, 
                             Membership_Restricted__c = false);     
    NU__Product__c product2 = 
            new NU__Product__c(name='Test Member Default, Member Restricted', 
                             RecordTypeId = Prod_Record_Type_Id,
                             NU__Entity__c = TestEntity.Id,
                             NU__RevenueGLAccount__c = myGLAccount.id,
                             NU__DisplayOrder__c=1, 
                             NU__ListPrice__c=0, 
                             NU__QuantityMax__c=1, 
                             Membership_Default__c = true,
                             Membership_Restricted__c = true);
                             
NU__Product__c product3 = 
            new NU__Product__c(name='Another Member Default, Not Restricted, Assigned PreAffiliation', 
                             RecordTypeId = Prod_Record_Type_Id,
                             NU__Entity__c = TestEntity.Id,
                             NU__RevenueGLAccount__c = myGLAccount.id,
                             NU__DisplayOrder__c=1, 
                             NU__ListPrice__c=0, 
                             NU__QuantityMax__c=1, 
                             Membership_Default__c = true, 
                             Membership_Restricted__c = false);                 
                             
 NU__Product__c product4 = 
        new NU__Product__c(name='Member Restricted, Will Lose, Then Gain', 
                         RecordTypeId = Prod_Record_Type_Id,
                         NU__Entity__c = TestEntity.Id,
                         NU__RevenueGLAccount__c = myGLAccount.id,
                         NU__DisplayOrder__c=1, 
                         NU__ListPrice__c=0, 
                         NU__QuantityMax__c=1, 
                         Membership_Default__c = false, 
                         Membership_Restricted__c = true);          
                         
NU__Product__c product5 = 
        new NU__Product__c(name='Member Restricted, Will Lose, Then Gain, Keep End Date', 
                         RecordTypeId = Prod_Record_Type_Id,
                         NU__Entity__c = TestEntity.Id,
                         NU__RevenueGLAccount__c = myGLAccount.id,
                         NU__DisplayOrder__c=1, 
                         NU__ListPrice__c=0, 
                         NU__QuantityMax__c=1, 
                         Membership_Default__c = false, 
                         Membership_Restricted__c = true);                                           
                                                                             
                                                                                     
    insert product1;
    insert product2;
    insert product3;
    insert product4;
    insert product5;
    
            
    //Create New Provider
    Account TestProvider = DataFactoryAccountExt.insertProviderAccount(900000);
    
    //Create Membership
    DataFactoryMembershipExt.insertCurrentProviderMembership(TestProvider.Id);
    
    //Create New Person, Attach to Company  
    Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
    
    
    //Give New Person a subscription prior to creating the affiuliation
    Online_Subscription__c ExistingSub = new Online_Subscription__c(Account__c = individualAccount.Id, Product__c = product3.Id, Status__c = Constant.SUBSCRIPTION_STATUS_ACTIVE, Start_Date__c = datetime.now() );
    insert ExistingSub; 
        
    NU__Affiliation__c aff = new NU__Affiliation__c(NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);   
    insert aff;
    
    Online_Subscription__c MemberSubNonDefault = new Online_Subscription__c(Account__c = individualAccount.Id, Product__c = product4.Id);
    insert MemberSubNonDefault; 
    
    DateTime dtEnd = DateTime.now().addDays(28);
    Online_Subscription__c MemberSubLimitedNonDefault = new Online_Subscription__c(Account__c = individualAccount.Id, Product__c = product5.Id, End_Date__c = dtEnd);
    insert MemberSubLimitedNonDefault;  
    
    
    //Select Member Default Subscriptions (selection includes products declared above plus current data)
    List<NU__Product__c> DefaultSubs = [select id, Name from NU__Product__c WHERE Membership_Default__c = true];
            
    //Select Member Restricted Subscriptions (selection includes products declared above plus current data)
    List<NU__Product__c> RestrictedSubs = [select id, Name from NU__Product__c WHERE Membership_Restricted__c = true];      
    
        
    ///Verify Defualt Member Subscriptions are added
    List<Online_Subscription__c> mySubs = [SELECT Id, Product__r.Name, Status__c FROM Online_Subscription__c WHERE Account__c = :individualAccount.Id]; // AND status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE];
    
    //Make sure there is an active subscription for each default product
    for(Online_Subscription__c s : mySubs)
    {       
        for(NU__Product__c p : DefaultSubs)
        {
            if (s.Product__c == p.id)
            {
                //System.Debug('GTC: CONFIRM SUB ADDED: ' + p.name + ' --- Status: ' + s.Status__c);
                system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
            }           
        }
        
        // Check Existing Subscription is still active
        if (s.Product__c == product3.Id)
        {   
            //System.Debug('GTC: CONFIRM EXISTING SUB: ' + product3.name + ' --- Status: ' + s.Status__c);
            if (s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE) {
                system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
            }
            else {
                system.assertEquals(false, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
            }
        }
        
        // Check Member Subscription is still active
        if (s.Product__c == product4.Id)
        {   
            //System.Debug('GTC: CONFIRM MEMBER SUB is active: ' + product4.name + ' --- Status: ' + s.Status__c);
            system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
        }
        
    }       
    //system.assertEquals(true, mySubs.size() > 0 );    
    //system.assertEquals(mySubs.size(), DefaultSubs.Size());   
            
    //Remove Affiliation
    delete aff;
    //aff.NU__Status__c = Constant.AFFILIATION_STATUS_INACTIVE;
                        
    //Verify Member Restricted Subsriptions are made inactive
    List<Online_Subscription__c> mySubs2 = [SELECT Id, Product__r.Name, Status__c, End_Date__c, System_End_Date__c 
          FROM Online_Subscription__c WHERE Account__c = :individualAccount.Id]; // AND status__c = :Constant.SUBSCRIPTION_STATUS_INACTIVE];
    
    for(Online_Subscription__c s : mySubs2)
    {       
        for(NU__Product__c p : RestrictedSubs)
        {
            if (s.Product__c == p.id)
            {
                //System.Debug('GTC: CONFIRM SUB REMOVED: ' + p.name + ' --- Status: ' + s.Status__c);
                system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE );
            }                                       
        }
        
        // Check Existing Subscription is still active
        if (s.Product__c == product3.Id)
        {
            //System.Debug('GTC: CONFIRM EXISTING SUB: ' + product3.name + ' --- Status: ' + s.Status__c);
            //system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
        }
        
        
        // Check Member Subscription is removed
        if (s.Product__c == product4.Id)
        {   
            //System.Debug('GTC: CONFIRM MEMBER SUB is removed: ' + product4.name + ' --- Status: ' + s.Status__c);
            system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE );
            
            //System.Debug('GTC: CONFIRM MEMBER SUB End Date is added: ' + product4.name + ' --- EndDate: ' + s.End_Date__c);
            system.assertEquals(true, s.End_Date__c != null );
            
            //System.Debug('GTC: CONFIRM MEMBER SUB  System End Date is added: ' + product4.name + ' --- SystemEndDate: ' + s.System_End_Date__c);
            system.assertEquals(true, s.System_End_Date__c != null );
            
            //System.Debug('GTC: CONFIRM MEMBER SUB  System End Date is the same as End Date: ' + product4.name + ' --- SystemEndDate: ' + s.System_End_Date__c + ' --- EndDate: ' + s.End_Date__c);
            system.assertEquals(true, s.System_End_Date__c == s.End_Date__c );
            
        }
                
        // Check Limted Member Subscription is removed
        if (s.Product__c == product5.Id)
        {   
            //System.Debug('GTC: CONFIRM LIMITED MEMBER SUB is removed: ' + product5.name + ' --- Status: ' + s.Status__c);
            system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_INACTIVE );
                        
            //System.Debug(Logginglevel.ERROR, 'GTC: CONFIRM LIMITED MEMBER SUB  System End Date is added: ' + product5.name + ' --- SystemEndDate: ' + s.System_End_Date__c);
            system.assertEquals(true, s.System_End_Date__c != null );
                        
        }
        
        
        
    }       
    
    
    //Add a new & different affiliation
    NU__Affiliation__c aff2 = new NU__Affiliation__c(NU__ParentAccount__c = TestProvider.Id, NU__Account__c = individualAccount.Id, NU__isPrimary__c = true, NU__Role__c = Constant.AFFILIATION_ROLE_LEADINGAGE_PRIME_CONTACT);
    insert aff2;
    
    List<Online_Subscription__c> mySubs3 = [SELECT Id, Product__r.Name, Status__c,  End_Date__c, System_End_Date__c
         FROM Online_Subscription__c WHERE Account__c = :individualAccount.Id AND status__c = :Constant.SUBSCRIPTION_STATUS_ACTIVE];
    
    for(Online_Subscription__c s : mySubs3)
    {       
        for(NU__Product__c p : RestrictedSubs)
        {
            if (s.Product__c == p.id)
            {
                //System.Debug('GTC: CONFIRM Re-Subscribe: ' + p.name + ' --- Status: ' + s.Status__c);
                system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
            }           
        }
        
        // Check Existing Subscription is still active
        if (s.Product__c == product3.Id)
        {
            //System.Debug('GTC: CONFIRM EXISTING SUB: ' + product3.name + ' --- Status: ' + s.Status__c);
            system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
        }
        
        
        // Check Member Subscription is Re Activated
        if (s.Product__c == product4.Id)
        {   
            //System.Debug('GTC: CONFIRM MEMBER SUB is Re Activated: ' + product4.name + ' --- Status: ' + s.Status__c);
            system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );
            
            //System.Debug('GTC: CONFIRM MEMBER SUB End Date is removed: ' + product4.name + ' --- EndDate: ' + s.End_Date__c);
            system.assertEquals(true, s.End_Date__c == null );
            
        }
        
        // Check Limted Member Subscription is Re Activsted and the original end date is preserved.
        if (s.Product__c == product5.Id)
        {   
            //System.Debug('GTC: CONFIRM LIMITED MEMBER SUB is Re Activated: ' + product5.name + ' --- Status: ' + s.Status__c);
            system.assertEquals(true, s.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE );         
            
            //System.Debug('GTC: CONFIRM LIMITED MEMBER SUB  System End Date is removed: ' + product5.name + ' --- SystemEndDate: ' + s.System_End_Date__c);
            system.assertEquals(true, s.System_End_Date__c == null );           
            
            System.Debug(LoggingLevel.WARN, 'GTC: CONFIRM LIMITED MEMBER SUB End Date is preserved: ' + product5.name + ' --- S.EndDate: ' + s.End_Date__c + ' --- dtEnd: ' + dtEnd);
            system.assertEquals(dtEnd, s.End_Date__c);          
        }
                
    }       
        
}

    static testmethod void duplicateSubscriptionsNotInDeleteListTest(){
        List<NU__Product__c> onlineSubProducts = DataFactoryProductExt.insertOnlineSubscriptionProducts(2);
        Set<Id> onlineSubProductIds = NU.CollectionUtil.getSobjectIds(onlineSubProducts);
        
        Account individual = DataFactoryAccountExt.insertIndividualAccount();
        
        List<Online_Subscription__c> currentSubs = DataFactoryOnlineSubscription.insertOnlineSubscriptions(individual.Id, onlineSubProductIds);
        
        DataFactoryMembershipExt.insertCurrentProviderMembership(individual.Id);
    }


}