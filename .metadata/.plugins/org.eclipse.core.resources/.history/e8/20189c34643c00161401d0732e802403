/*
* Test methods for the CustomStateProviderRevenueController
*/
@isTest(SeeAllData=true)

private class TestCustomStateProviderRevenueController {
    static testmethod void exerciseCustomStateProviderRevenueController(){
            PageReference testPr = Page.CustomStateProviderRevenue;
            Test.setCurrentPage(testPr);
            
            CustomStateProviderRevenueController cc = new CustomStateProviderRevenueController();
            
            User u = cc.pageUser;
            system.assertNotEquals(u, null, 'No User Record Found.');
            
                        
            //Create LeadingAge Staff User          

            User admin = [select id from user where profile.name = 'System Administrator' and IsActive = true limit 1];          
            admin.CompanyName = 'LeadingAge';
            
            
            Profile pfl_staff = [select id from profile where name='Standard User'];
            User StaffUser = new User(alias = 'u1', 
                                    CommunityNickname = 'u1',
                                    username='u1@leadingage.org',
                                    email='u1@leadingage.org',
                                    lastname='Staffer', 
                                    CompanyName ='LeadingAge',
                                    emailencodingkey='UTF-8',                                   
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',                                     
                                    country='United States',                                    
                                    timezonesidkey='America/Los_Angeles', 
                                    profileid = pfl_staff.Id);
            
            Profile pfl_state = [select id from profile where name='State Association Partner User'];
                                     
            User StateUser = new User(alias = 'u2', 
                                    CommunityNickname = 'u2',
                                    username='u2@state.org',
                                    email='u2@state.org',       
                                    lastname='Stater',                                                              
                                    emailencodingkey='UTF-8',                                   
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',                                    
                                    country='United States',                                    
                                    timezonesidkey='America/Los_Angeles', 
                                    profileid = pfl_state.Id);
            //StateUser.IsPortalEnabled = true;
            
                                                            
             
             //test from LeadingAge Staff point of view, e.g exercise dropdown, etc
             System.runAs(admin) {
                
                    //Check is Staff
                    system.assert(cc.isUserLeadingAgeStaff == true, 'User is not LeadingAge Stafff!');
                    
                    //Check DropDown is Populated w/ State Associations
                    system.assert(cc.getItems().size() > 0, 'State List should have multuple values. Size is: ' + cc.getItems().size());
                    
                    //Select a state                                    
                    cc.states = cc.getItems().get(7).getValue(); //Select LeadingAge Colorado
                    cc.State_onChange(); // get provider & mso data 

                    if (cc.ProviderAccounts.size() <= 0){
                        cc.states = cc.getItems().get(6).getValue(); //Select LeadingAge Colorado
                        cc.State_onChange(); // get provider & mso data 
                    }                    
                                    
                    integer year = Date.today().year();             
                    
                    //update Provider Account w. success
                    cc.ProviderAccounts[0].Program_Service_Revenue__c = 2000000.01;
                    cc.ProviderAccounts[0].Revenue_Year_Submitted__c = string.valueOf(year);             
                    cc.save();
                    system.assert(cc.displayErrorMSG == false, 'Save should occur without errors');
                    
                    //update MSO Account w. success
                    if (cc.MSOAccounts != null && cc.MSOAccounts.size() > 0) {
                        cc.MSOAccounts[0].Multi_Site_Program_Service_Revenue__c = 5000000.01;
                        cc.MSOAccounts[0].Revenue_Year_Submitted__c = string.valueOf(year);             
                        cc.save();
                        system.assert(cc.displayErrorMSG == false, 'Save should occur without errors');
                    }
                    
                    //update Provider Account w. invalid date
                    cc.ProviderAccounts[0].Program_Service_Revenue__c = 2000000.01;
                    cc.ProviderAccounts[0].Revenue_Year_Submitted__c = 'abcd';           
                    cc.save();
                    system.assert(cc.displayErrorMSG == true, 'Invalid Date, Should Not Save');
                    
                    //update MSO Account w. invalid date
                    if (cc.MSOAccounts != null && cc.MSOAccounts.size() > 0) {
                        cc.MSOAccounts[0].Multi_Site_Program_Service_Revenue__c = 5000000.01;
                        cc.MSOAccounts[0].Revenue_Year_Submitted__c = 'efgh';           
                        cc.save();
                        system.assert(cc.displayErrorMSG == true, 'Invalid Date, Should Not Save');
                    }
                    
                    //update Provider Account w. invalid date
                    cc.ProviderAccounts[0].Program_Service_Revenue__c = 2000000.01;
                    cc.ProviderAccounts[0].Revenue_Year_Submitted__c = string.valueOf(year + 10);            
                    cc.save();
                    system.assert(cc.displayErrorMSG == true, 'Future Date, Should Not Save');
                    
                    //update MSO Account w. invalid date
                    if (cc.MSOAccounts != null && cc.MSOAccounts.size() > 0) {
                        cc.MSOAccounts[0].Multi_Site_Program_Service_Revenue__c = 5000000.01;
                        cc.MSOAccounts[0].Revenue_Year_Submitted__c = string.valueOf(year + 8);            
                        cc.save();
                        system.assert(cc.displayErrorMSG == true, 'Future Date, Should Not Save');
                    }
                    
                    //update Provider Account w. invalid date
                    cc.ProviderAccounts[0].Program_Service_Revenue__c = 2000000.01;
                    cc.ProviderAccounts[0].Revenue_Year_Submitted__c = string.valueOf(year- 10);             
                    cc.save();
                    system.assert(cc.displayErrorMSG == true, 'Old Date, Should Not Save');
                    
                    //update MSO Account w. invalid date
                    if (cc.MSOAccounts != null && cc.MSOAccounts.size() > 0) {
                        cc.MSOAccounts[0].Multi_Site_Program_Service_Revenue__c = 5000000.01;
                        cc.MSOAccounts[0].Revenue_Year_Submitted__c = string.valueOf(year- 8);             
                        cc.save();
                        system.assert(cc.displayErrorMSG == true, 'Old Date, Should Not Save');
                    }
                
             }
             
             
             
             
            
            //This fails in the test class... I'm guessing its because the tests run under the system user, which doesn't meet any of the criteria on the page itself
            //  So we probably need to set up a test user in this method that looks like a LeadingAge Staff person and start the controller from there... or something? 
            //system.assert(cc.getItems().size() > 0, 'No data in States list!');
            
            if (cc.isUserLeadingAgeStaff){
                //test from LeadingAge Staff point of view, e.g exercise dropdown, etc              
            }
            else {
                //test from Portal POV, e.g. create (Or borrow) a portal user and log in as them (See: TestAccountTrigger for examples of setting up a portal user)
            }
            
            
            
            
    }
}