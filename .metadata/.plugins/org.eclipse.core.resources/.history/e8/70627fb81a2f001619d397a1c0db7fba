public with sharing class ConferenceProposalsController extends ConferenceManagementControllerBase {
    
    public ConferenceProposalsController() {
    	SearchCriteria.Title = ApexPages.CurrentPage().getParameters().get('title');
		SearchCriteria.FirstName = ApexPages.CurrentPage().getParameters().get('fn');
		SearchCriteria.LastName = ApexPages.CurrentPage().getParameters().get('sn');
		SearchCriteria.Company = ApexPages.CurrentPage().getParameters().get('cmpy');
		SearchCriteria.TrackName = ApexPages.CurrentPage().getParameters().get('trck');
		SearchCriteria.LearningObjectivesPhrase = ApexPages.CurrentPage().getParameters().get('lrno');
		SearchCriteria.NotesPhrase = ApexPages.CurrentPage().getParameters().get('notes');
		SearchCriteria.Status = ApexPages.CurrentPage().getParameters().get('sta');
    	
        string sJmp = ApexPages.CurrentPage().getParameters().get('jmp');
        if (sJmp != null) {
            RecordNum = integer.valueof(sJmp);
            GoToRecord();
        }
    }
    
    
    public override List<SObject> getConferenceObjects(){
        if(SearchCriteria.HasSearchCriteria){
            return ConferenceProposalQuerier.getConferenceProposalsByConferenceIdAndSearchCriteria(EventId, SearchCriteria);
        }
        
        return ConferenceProposalQuerier.getConferenceProposalsByConferenceId(EventId);
    }
    
    public Id selectedSpeakerId {get;set;}
    public PageReference selectItem() {
        PageReference pr = new PageReference('/' + selectedSpeakerId);
        pr.setRedirect(true);
        return pr;
    }

    public Conference_Proposal__c CurrentProposal{
        get{
            return (Conference_Proposal__c) CurrentConferenceObject;
        }
    }
    
    public ConferenceProposalSearchCriteria SearchCriteria { get; set; }
    
    {
        SearchCriteria = new ConferenceProposalSearchCriteria();
        
    }
    
    public List<Conference_Proposal__History> TrackChanges {
        get{
            List<Conference_Proposal__History> changes = getFieldHistory('Conference_Track__c');
            
            Integer size = changes.size();
            
            Schema.DescribeSObjectResult R = Conference_Track__c.SObjectType.getDescribe();
    
            String keyPrefix = R.getKeyPrefix();
            
            for (Integer index = size - 1; index >= 0; --index){
                Conference_Proposal__History history = changes[index];
                
                if (( history.OldValue != null && String.valueOf(history.OldValue).startsWith(keyPrefix) ) ||
                    ( history.NewValue != null && String.valueOf(history.NewValue).startsWith(keyPrefix) )){
                    changes.remove(index);
                }
            }
            
            return changes;
        }
    }
    
    public Integer TrackChangesCount{
        get { return TrackChanges.size(); }
    }
    
    public List<Conference_Proposal__History> StatusChanges {
        get{
            return getFieldHistory('Status__c');
        }
    }
    
    public Integer StatusChangesCount{
        get { return StatusChanges.size(); }
    }
    
    public List<Conference_Proposal__History> ProposalHistory {
        get {
            List<Conference_Proposal__History> history = new List<Conference_Proposal__History>();
            
            if (CurrentProposal != null){
                history = 
                [select id,
                        Field,
                        OldValue,
                        NewValue,
                        CreatedById,
                        CreatedDate
                   from Conference_Proposal__History
                  where ParentId = :CurrentProposal.Id
                  order by CreatedDate desc];
            }
            
            return history;
        }
    }
    
    public Boolean HasSearchCriteria {
        get { return SearchCriteria.HasSearchCriteria; }
    }
    
    public PageReference ConvertToSession(){
        
        try{
            ConferenceProposalToSessionConverter converter = new ConferenceProposalToSessionConverter(new Set<Id>{ CurrentProposal.Id });
            converter.convert();
            refresh();
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
        
        return null;
    }
    
    public integer RecordNum {get;set;}
    public void GoToRecord() {
        if (RecordNum != null && RecordNum > 0 && RecordNum <= ResultSize) {
            changeToPage(RecordNum);
            RecordNum = null;
        }
    }
    
    public void search(){
        refreshAtPage1();
    }
    
    public void clearSearch(){
        SearchCriteria.clearSearchCriteria();
        refreshAtPage1();
    }
    
    private List<Conference_Proposal__History> getFieldHistory(String field){
        List<Conference_Proposal__History> changes = new List<Conference_Proposal__History>();

        for (Conference_Proposal__History history : ProposalHistory){
            if (history.field == field){
                changes.add(history);
            }
        }

        return changes;
    }
}