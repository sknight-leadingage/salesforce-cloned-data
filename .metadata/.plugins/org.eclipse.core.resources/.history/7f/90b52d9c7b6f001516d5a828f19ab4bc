public with sharing class MembershipRetention {
	
	private static Set<Id> accountIds;
	private static Set<Id> membershipTypeIds;
	
	private static Map<Id,NU__Membership__c> membershipsToUpdate = new Map<Id,NU__Membership__c>();
	private static Map<Object,List<sObject>> accountMemberships;
	
	public static void Calculate(List<NU__Membership__c> memberships) {
		accountIds = NU.CollectionUtil.getLookupIds(memberships, 'NU__Account__c');
		membershipTypeIds = NU.CollectionUtil.getLookupIds(memberships, 'NU__MembershipType__c');
		
		// get all active memberships for the accounts getting new memberships
		List<NU__Membership__c> membershipsToEvaluate = [SELECT Id, NU__Account__c, NU__MembershipType__c, NU__StartDate__c, RetentionStatus__c 
			FROM NU__Membership__c
			WHERE NU__Status__c != :Constant.ORDER_ITEM_LINE_STATUS_CANCELLED
				AND NU__Account__c IN :accountIds
				AND NU__MembershipType__c IN :membershipTypeIds];
				
		accountMemberships = NU.CollectionUtil.groupSObjectsByField(membershipsToEvaluate, 'NU__Account__c');
		
		// evaluate the memberships to determine "New" or "Renewing"		
		for (NU__Membership__c membership : memberships) {
			List<sObject> existingMemberships = accountMemberships.get((Object)membership.NU__Account__c);
			NU__Membership__c membershipToUpdate = membership.clone(true);
			
			Boolean isRenewing = false;
			if (existingMemberships != null && existingMemberships.size() > 0) {
				// there are already existing memberships for the account - are they of the same membership type?
				List<sObject> existingMembershipTypeMemberships = findExistingMembership(existingMemberships, membershipToUpdate.NU__MembershipType__c);
				
				// there are existing memberships of the same membership type for the account - evaluate start dates
				if (existingMembershipTypeMemberships != null && existingMembershipTypeMemberships.size() > 0) {
					isRenewing = isCurrentMembershipRenewing(existingMembershipTypeMemberships, membershipToUpdate);
				}
			}
			
			membershipToUpdate.RetentionStatus__c = (isRenewing ? Constant.MEMBERSHIP_RETENTION_RENEWING : Constant.MEMBERSHIP_RETENTION_NEW);
			membershipsToUpdate.put(membershipToUpdate.Id,membershipToUpdate);
		}
		
		if (membershipsToUpdate.size() > 0) {
			update membershipsToUpdate.values();
		}
	}
	
	private static List<sObject> findExistingMembership(List<sObject> existingMemberships, Id membershipTypeId) {
		List<sObject> memberships = null;
		Map<Object,List<sObject>> membershipsByMembershipType = NU.CollectionUtil.groupSObjectsByField(existingMemberships, 'NU__MembershipType__c');
		
		if (membershipsByMembershipType != null && membershipsByMembershipType.get(membershipTypeId) != null) {
			memberships = membershipsByMembershipType.get(membershipTypeId);
		}
		
		return memberships;
	}
	
	private static Boolean isCurrentMembershipRenewing(List<sObject> memberships, NU__Membership__c membership) {
		Date startDate = membership.NU__StartDate__c;
		
		Integer numberOfMemberships = memberships.size();
		Integer numberAfterCurrent = 0;
		for (sObject s : memberships) {
			NU__Membership__c mem = (NU__Membership__c)s;
			
			// ignore if looking at the same record; also decrement size
			if (mem.Id == membership.Id) {
				numberOfMemberships--;
				continue;
			}			
			
			if (membershipsToUpdate.get(mem.Id) != null) {
				mem = membershipsToUpdate.get(mem.Id);
			}
			 
			// is the start date AFTER the current membership?			
			if (mem.NU__StartDate__c > startDate) {
				numberAfterCurrent++;
				
				// if this membership is tagged as "New", it shouldn't be
				setToRenewingIfNew(mem);
			}
		}
		
		// if all memberships start after the current membership, then the current membership should be tagged as "New"
		return (numberAfterCurrent == numberOfMemberships ? false : true);
	}
	
	private static void setToRenewingIfNew(NU__Membership__c mem) {
		// if this membership is tagged as "New", it shouldn't be
		if (mem.RetentionStatus__c == Constant.MEMBERSHIP_RETENTION_NEW) {
			mem.RetentionStatus__c = Constant.MEMBERSHIP_RETENTION_RENEWING;
			
			membershipsToUpdate.put(mem.Id, mem);
		}
	}
}