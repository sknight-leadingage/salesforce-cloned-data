public without sharing class MembershipTriggerHandler extends NU.TriggerHandlersBase {
    public override void onBeforeInsert(List<Sobject> newRecords) {
		processTriggerInsert(newRecords);
    }
    
	private void processTriggerInsert(List<Sobject> newRecords) {
		List<NU__Membership__c> lNewRecords = (List<NU__Membership__c>) newRecords;
		Set<Id> accountIds = NU.CollectionUtil.getLookupIds(newRecords, 'NU__Account__c');
	
		Map<Id, Account> accounts = AccountQuerier.getAccountsWithStatePartnerInformationByIds(accountIds);
		
		List<NU__MembershipType__c> providerMTs = MembershipTypeQuerier.getProviderMembershipTypes();
		Map<Id, NU__MembershipType__c> providerMTsMap = new Map<Id, NU__MembershipType__c>(providerMTs);
		Set<Id> providerMTIds = providerMTsMap.keySet();
		
		List<Account> accountsToUpdate = new List<Account>();
		
		for (NU__Membership__c newMembership : lNewRecords){
			Account account = accounts.get(newMembership.NU__Account__c);
			
			if (providerMTIds.contains(newMembership.NU__MembershipType__c) &&
			    ( // Rejoin
			      account.Provider_Lapsed_On__c < newMembership.NU__StartDate__c ||
			      // Renewal
			      account.Provider_Lapsed_On__c <= newMembership.NU__EndDate__c
			    )){
			    
			    account.Provider_Lapsed_On__c = null;
			    
			    accountsToUpdate.add(account);
		    }
		}
		
		if (accountsToUpdate.size() > 0){
			update accountsToUpdate;
		}
	}
}