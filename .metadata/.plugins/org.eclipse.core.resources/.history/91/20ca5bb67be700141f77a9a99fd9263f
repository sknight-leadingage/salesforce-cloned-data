<apex:page controller="InvoiceEstimatedAnnualDuesController"  contentType="{!PageCType}" cache="true">
<apex:stylesheet value="{!URLFOR($Resource.LeadingAgeStyle, 'LeadingAge.css')}"/>
<style type="text/css">
body .bPageTitle .ptBody h1.noSecondHeader {font-family: Arial, Helvetica, sans-serif; font-size: 24px; font-weight: normal}
td.pbTitle h2.mainTitle {font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold}
</style>

        <apex:define name="sectionHeader">
            <apex:sectionHeader title="Estimated Annual Dues (Pro Forma Invoice)" help="#" />
            <apex:pageMessages />
        </apex:define>  
        <apex:outputText style="font-size:14pt; font-weight:bold" value="Current Year: {!ThisYearCalculated}" rendered="{!ThisYearCalculated != 1}" /> <br/><br/>
    <apex:form >
    <apex:selectList value="{!selectedState}" multiselect="false" size="1" rendered="{!isUserLeadingAgeStaff}">
        <apex:selectOption itemValue="-1" itemLabel=" -- Select State --" />
        <apex:selectOptions value="{!items}"/>
        <apex:actionSupport event="onchange" action="{!State_onChange}" />
    </apex:selectList>
    <apex:outputText rendered="{!isUserLeadingAgeStaff}"><br/><br/></apex:outputText>
    <strong>INSTRUCTIONS: </strong>
    The dues-related data below was used to calculate the estimated dues for your association. <br/>
    You can access the online help system to get more information about estimated dues, and the terms used here-in.
    <br/> (*Changes to Program Service Revenue will be reflected within 24 hours*)
    <br/><br/>
    
    <div align="left">
    <apex:commandButton value="Excel View" action="{!SetPageType}" />
    </div><br /><br />
      <apex:pageBlock title="Providers">
          
          <apex:PageBlockTable value="{!estimatedDuesData}" var="dd" >
              
              <apex:column headervalue="Record ID">
                  <apex:outputlink value="/{!dd.accountDetail.id}" target="_blank">{!dd.accountDetail.LeadingAge_ID__c}</apex:outputlink>
              </apex:column>

              <apex:column headervalue="Company name">
                  <apex:outputtext value="{!dd.accountDetail.name}" />
                  <apex:outputtext rendered="{!IF(dd.accountDetail.Multi_Site_Enabled__c == true, true, false)}"><br /><i>(Dues paid by Corporate MSO)</i></apex:outputtext>
              </apex:column>
              
              <apex:column headervalue="Company type">
                  <apex:outputtext rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, true, false)}">Corporate&nbsp;</apex:outputtext>
                  <apex:outputfield value="{!dd.accountDetail.NU__RecordTypeName__c}" />
                  <apex:outputtext rendered="{!IF(dd.accountDetail.NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c == true, true, false)}">&dagger;</apex:outputtext>
              </apex:column>

              <apex:column headervalue="Parent Affilliation">
<!--               <apex:outputtext rendered="{!IF(dd.accountDetail.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == true, true, false)}">Corporate&nbsp;</apex:outputtext>               -->   
                  <apex:outputtext value="{!dd.accountDetail.Primary_Affiliation_Account_Name__c }" />
              </apex:column>

              <apex:column headervalue="Fiscal Year">
                  <apex:outputtext value="{!dd.accountDetail.Revenue_Year_Submitted__c}" />
                  <apex:outputtext rendered="{!IF(ISNUMBER(dd.accountDetail.Revenue_Year_Submitted__c), IF(VALUE(dd.accountDetail.Revenue_Year_Submitted__c) < ThisYearCalculated - 3, true, false), false)}">*</apex:outputtext>
              </apex:column>
              
             <apex:column headervalue="Program Revenue" style="text-align:right" headerClass="CurrencyElement">
                  <apex:outputfield rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, true, false)}" value="{!dd.accountDetail.Multi_Site_Program_Service_Revenue__c}" />
                  <apex:outputfield rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, false, true)}" value="{!dd.accountDetail.Program_Service_Revenue__c}" />
                  <apex:outputtext rendered="{!IF(dd.accountDetail.Program_Service_Revenue__c == null, true, false)}">**</apex:outputtext>
              </apex:column>
              
              <apex:column headervalue="Millage Dues" style="text-align:right" headerClass="CurrencyElement">
                  <apex:outputfield rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, true, false)}" value="{!dd.accountDetail.Multi_Site_Dues_Price__c}" />
                  <apex:outputfield rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, false, true)}" value="{!dd.accountDetail.Dues_Price__c}" />
              </apex:column>

              <apex:column headervalue="Current Year Dues" style="text-align:right" headerClass="CurrencyElement" rendered="{!IF(PageCType != '', false, true)}">
                  <apex:outputtext value="{!dd.SelectedPreviousYearMembershipSummary}" />
              </apex:column>
              <!--<apex:column headervalue="Multi-Site Enabled" style="text-align:right" headerClass="CurrencyElement">
                  <apex:outputfield value="{!dd.accountDetail.Multi_Site_Enabled__c}" />
              </apex:column>-->
<!--
              <apex:column headervalue="Estimated dues (OLD FORMULA)" style="text-align:right" headerClass="CurrencyElement">
                  <apex:outputfield rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, true, false)}" value="{!dd.accountDetail.Multi_Site_Dues_Price__c}" />
                  <apex:outputfield rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, false, IF(dd.accountDetail.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == true, false, true))}" value="{!dd.accountDetail.Dues_Price__c}" />
                  <apex:outputtext rendered="{!IF(dd.accountDetail.Multi_Site_Corporate__c == true, false, IF(dd.accountDetail.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == true, true, false))}" value="$0.00" />
                  <apex:outputtext rendered="{!IF(ISBLANK(dd.accountDetail.Dues_Price_Override__c), false, true)}" value="***" />
              </apex:column>
 -->

              <apex:column headervalue="Estimated Dues for Next Year" style="text-align:right" headerClass="CurrencyElement">
                <apex:outputText value="${0, number, ###,###.00}">
                    <apex:param value="{!dd.estimatedDues}"/>
                </apex:outputText>
                <!--<apex:outputtext rendered="{!IF(ISBLANK(dd.accountDetail.Dues_Price_Override__c), false, true)}" value="***" />-->
              </apex:column>

          </apex:PageBlockTable>
          
      <table border="0" cellspacing="5px">
          <tr>
            <td>Gross annual dues for all companies:</td>
            <td colspan="2" align="right">
                <apex:outputText value="${0, number, ###,###.00}">
                    <apex:param value="{!estimatedDuesDataGrossAnnualDues}"/>
                </apex:outputText>
            </td>
          </tr>
          <tr>
            <td>15% Collection fee:</td>
            <td colspan="2" align="right"><em>
                <apex:outputText value="${0, number, ###,###.00}">
                    <apex:param value="{!estimatedDuesDataServiceFee}"/>
                </apex:outputText></em>
            </td>
          </tr>
          <tr>
            <td>LeadingAge dues less collection fee:</td>
            <td colspan="2" align="right"><strong>
                <apex:outputText value="${0, number, ###,###.00}">
                    <apex:param value="{!estimatedDuesDataNetLeadingAgeDues}"/>
                </apex:outputText></strong>
            </td>
          </tr>
      </table>
               
      </apex:pageBlock>
  </apex:form>
    <br/><br/>
  <strong>Notes:</strong> <br/>
  * This member's fiscal year is outdated <br/>
  ** Program revenue has not been reported for this member <br/>
<!--  *** Denotes a member who paid a dues amount that is different from the LeadingAge calculated dues. <br/>-->
<!-- &dagger; Denotes a member who is billed for dues directly by LeadingAge per a special agreement. <br/>  -->
  
</apex:page>