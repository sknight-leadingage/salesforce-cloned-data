@isTest
private class TestBatchUpdateStatePartnerName {
	private static Account StatePartnerAccount;
	private static Account AltStatePartnerAccount;
	private static Account individualAccount;
	private static Account providerAccount;
	private static Account multiSiteAccount;
	private static Map<String,Id> PostalStateToStatePartnerIdMap;
	
	private static final string BOGUS_STATE_PARTNER_NAME_1 = 'LeadingAge Test SP 1';
	private static final string BOGUS_STATE_PARTNER_NAME_2 = 'LeadingAge Test SP 2';

	private static void Setup() {
    	StatePartnerAccount =  DataFactoryAccountExt.createStatePartnerAccount();
    	StatePartnerAccount.Name = 'LeadingAge New York';
    	insert StatePartnerAccount;
    	
    	AltStatePartnerAccount =  DataFactoryAccountExt.createStatePartnerAccount();
    	AltStatePartnerAccount.Name = 'LeadingAge Oklahoma';
    	AltStatePartnerAccount.BillingState = 'OK';
    	insert AltStatePartnerAccount;
    	
    	PostalStateToStatePartnerIdMap = AccountQuerier.PostalStateToStatePartnerIdMap;
    	
    	multiSiteAccount =  DataFactoryAccountExt.insertMultiSiteAccount(100000000);
        providerAccount = DataFactoryAccountExt.insertProviderAccount(10000);
        individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, multiSiteAccount.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, providerAccount.Id, true);
        
		system.assertEquals('NY', StatePartnerAccount.BillingState, 'StatePartnerAccount: Default BillingState "NY" not set!');
		system.assertEquals('NY', multiSiteAccount.BillingState, 'multiSiteAccount: Default BillingState "NY" not set!');
		system.assertEquals('NY', providerAccount.BillingState, 'providerAccount: Default BillingState "NY" not set!');
		system.assertEquals('NY', individualAccount.BillingState, 'individualAccount: Default BillingState "NY" not set!');
	}
	//, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c
	private static void ReQuery() {
		multiSiteAccount = [SELECT Id, Name, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Permissions__c FROM Account WHERE Id = :multiSiteAccount.id];
		providerAccount = [SELECT Id, Name, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Permissions__c FROM Account WHERE Id = :providerAccount.id];
		individualAccount = [SELECT Id, Name, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Permissions__c FROM Account WHERE Id = :individualAccount.id];
	}
	
	static testMethod void AcquireStatePartnerFromAddress() {
		Setup();
		
    	Test.startTest();
        BatchUpdateStatePartnerNames c = new BatchUpdateStatePartnerNames();
        Database.executeBatch(c);
        Test.stopTest();
        
        ReQuery();
        
        system.assertEquals(StatePartnerAccount.Id, multiSiteAccount.State_Partner_ID__c);
        system.assertEquals(StatePartnerAccount.Id, providerAccount.State_Partner_ID__c);
        system.assertEquals(StatePartnerAccount.Id, individualAccount.State_Partner_ID__c);
	}

	static testMethod void AcquireStatePartnerFromParent() {
		Setup();
		
        providerAccount.State_Partner_ID__c = AltStatePartnerAccount.Id;
        update providerAccount;
        
        individualAccount.State_Partner_ID__c = null;
        update individualAccount;
		
    	Test.startTest();
        BatchUpdateStatePartnerNames c = new BatchUpdateStatePartnerNames();
        Database.executeBatch(c);
        Test.stopTest();
        
        ReQuery();
        
        system.assertEquals(StatePartnerAccount.Id, multiSiteAccount.State_Partner_ID__c);
        system.assertEquals(AltStatePartnerAccount.Id, providerAccount.State_Partner_ID__c);
        
        if (individualAccount.State_Partner_ID__c == StatePartnerAccount.Id) {
        	system.assertEquals(StatePartnerAccount.Id, individualAccount.State_Partner_ID__c);
        }
        else if (individualAccount.State_Partner_ID__c == AltStatePartnerAccount.Id) {
        	system.assertEquals(AltStatePartnerAccount.Id, individualAccount.State_Partner_ID__c);
        }
        else {
        	system.assertNotEquals(null, individualAccount.State_Partner_ID__c, 'individualAccount\'s State Partner shouldn\'t be null.');
        }
	}

	static testMethod void AcquireStatePartnerFromParentClassic() {
		Setup();
		
        providerAccount.State_Partner_ID__c = AltStatePartnerAccount.Id;
        update providerAccount;
        
        NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger');
        individualAccount.State_Partner_ID__c = null;
        update individualAccount;
        NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger');
		
    	Test.startTest();
        BatchUpdateStatePartnerNames c = new BatchUpdateStatePartnerNames();
        Database.executeBatch(c);
        Test.stopTest();
        
        ReQuery();
        
        system.assertEquals(StatePartnerAccount.Id, multiSiteAccount.State_Partner_ID__c);
        system.assertEquals(AltStatePartnerAccount.Id, providerAccount.State_Partner_ID__c);
       	system.assertEquals(AltStatePartnerAccount.Id, individualAccount.State_Partner_ID__c);
	}

	static testMethod void AcquirePermissionsFromParentClassic() {
		Setup();
		
        providerAccount.State_Partner_ID__c = AltStatePartnerAccount.Id;
        update providerAccount;
        
        NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger');
        
        multiSiteAccount.State_Partner_Permissions__c = BOGUS_STATE_PARTNER_NAME_1;
        update multiSiteAccount;
        
        providerAccount.State_Partner_Permissions__c = BOGUS_STATE_PARTNER_NAME_2;
        update providerAccount;
        
        individualAccount.State_Partner_ID__c = null;
        update individualAccount;
        
        NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger');
		
    	Test.startTest();
        BatchUpdateStatePartnerNames c = new BatchUpdateStatePartnerNames();
        Database.executeBatch(c);
        Test.stopTest();
        
        ReQuery();
        
        system.assert( multiSiteAccount.State_Partner_Permissions__c.containsIgnoreCase(BOGUS_STATE_PARTNER_NAME_1));
        system.assert(providerAccount.State_Partner_Permissions__c.containsIgnoreCase(BOGUS_STATE_PARTNER_NAME_2) && providerAccount.State_Partner_Permissions__c.containsIgnoreCase(BOGUS_STATE_PARTNER_NAME_1));
       	system.assert(individualAccount.State_Partner_Permissions__c.containsIgnoreCase(BOGUS_STATE_PARTNER_NAME_2) && individualAccount.State_Partner_Permissions__c.containsIgnoreCase(BOGUS_STATE_PARTNER_NAME_1));
	}
}