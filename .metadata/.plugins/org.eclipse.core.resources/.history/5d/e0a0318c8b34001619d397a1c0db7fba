/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestOrderPurchaseSponsorship {

    static NU__Entity__c entity = null; 
    static NU__GLAccount__c gl = null;
    static Account billTo = null;
    static Account contact = null;
    static NU__PriceClass__c priceClass = null;
    
    static NU__Product__c sponsorshipProduct = null;
    static NU__Deal__c sponsorshipDeal = null;
    static DealItem__c sponsorshipDealItem = null;
    
    static NU__Cart__c cart = null;
    static NU__CartItem__c cartItem = null;
    static NU__CartItemLine__c cartItemLine = null;
    
    static Map<String, Schema.RecordTypeInfo> productRecordTypes = null;
    static Map<String, Schema.RecordTypeInfo> dealRecordTypes = null;
    
    private static void setupTest() {
        if (entity != null) return;
        
        entity = NU.DataFactoryEntity.insertEntity();
        gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);
        
        billTo = DataFactoryAccountExt.insertMultiSiteAccount();
        contact = DataFactoryAccountExt.insertIndividualAccount();
        
        priceClass = priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        
        // setup specific for sponsorships      
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo sponsorshipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        
        sponsorshipProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        sponsorshipDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, sponsorshipDealRTI.getRecordTypeId());
        
        sponsorshipDealItem = DataFactoryDealItem.insertDealItem(sponsorshipDeal.Id, sponsorshipProduct.Id, 500.00, 1);
        
        cart = new NU__Cart__c(
            NU__BillTo__c = billTo.Id,
            NU__TransactionDate__c = Date.today(),
            NU__Entity__c = entity.Id,
            Deal__c = sponsorshipDeal.Id
        );
        insert cart;
        
        Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
        cartItem = new NU__CartItem__c(
            NU__Cart__c = cart.Id,
            NU__Customer__c = billTo.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = rt.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP).getRecordTypeId()
        );
        insert cartItem;
        
        cartItemLine = new NU__CartItemLine__c(
            NU__CartItem__c = cartItem.Id,
            NU__IsInCart__c = true,
            NU__UnitPrice__c = sponsorshipDealItem.Price__c,
            NU__Product__c = sponsorshipDealItem.Product__c,
            NU__Quantity__c = sponsorshipDealItem.Quantity__c                            
        );
        
        SponsorshipCartItemLineData sponsorshipCartItemLineData = new SponsorshipCartItemLineData(sponsorshipDeal.Event__c, null, null);
        cartItemLine.NU__Data__c = JSON.serialize(sponsorshipCartItemLineData);
        insert cartItemLine;
    }
    
    static OrderPurchaseSponsorship createOrderPurchaseSponsorship(Id billToId, Id entityId){
    	PageReference pageRef = new PageReference('/apex/OrderSponsorshipProducts');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('billTo',billTo.Id);
        ApexPages.currentPage().getParameters().put('entity',entity.Id);
        OrderPurchaseSponsorship controller = new OrderPurchaseSponsorship();
        
        return controller;
    }

	static OrderPurchaseSponsorship createOrderPurchaseSponsorshipWithExistingCart(Id cartId, Id cartItemId){
    	PageReference pageRef = new PageReference('/apex/OrderSponsorshipProducts');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',cart.Id);
        ApexPages.currentPage().getParameters().put('cartItemId',cartItem.Id);
        OrderPurchaseSponsorship controller = new OrderPurchaseSponsorship();
        
        return controller;
    }
    
    // test methods
    static testMethod void testOrderPurchaseSponsorship_NewOrder() {
        setupTest();
        
        PageReference pageRef = new PageReference('/apex/OrderSponsorshipProducts');
        Test.setCurrentPage(pageRef);
        OrderPurchaseSponsorship controller = new OrderPurchaseSponsorship();

        // Test #1: New Sponsorship Order - nothing supplied
        System.assert(controller.Cart != null && controller.CartItem != null, 'New cart was not loaded.');
    }
    
    static testMethod void testOrderPurchaseSponsorship_NewOrderBillToAndEntity() {
        setupTest();

        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorship(billTo.Id, entity.Id);
        
        // Test #2: New Sponsorship Order - billTo & entity supplied
        System.assert(controller.Cart.NU__Entity__c == entity.Id && controller.Cart.NU__BillTo__c == billTo.Id, 'New cart was not loaded with supplied entity and billTo.');
    }
    
    static testMethod void testOrderPurchaseSponsorship_AlterExistingCart() {
        setupTest();
        
        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorshipWithExistingCart(cart.Id, cartItem.Id);
        
        // Test #3: Alter Existing Sponsorship Cart
        controller.getProductItemLines();
        controller.OnAccountSelected();
        System.debug(controller.getProductItemLines());
        System.assert(controller.Cart.NU__TransactionDate__c != null, 'Existing cart was not loaded properly.');
    }
    
    static testMethod void testOrderPurchaseSponsorship_CancelCart() {
        setupTest();
        
        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorshipWithExistingCart(cart.Id, cartItem.Id);
        
        // Test #4: Cancel Cart
        PageReference tempRef = controller.Cancel();
        System.assert(tempRef != null,'Something went wrong when canceling the sponsorship cart.');
    }
    
    static testMethod void testOrderPurchaseSponsorship_SubmitCartWithNoItems() {
        setupTest();
        
        cartItemLine.NU__IsInCart__c = false;
        update cartItemLine;
        
        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorshipWithExistingCart(cart.Id, cartItem.Id);
        
        // Test #5: Submit Cart (no items selected)
        PageReference tempRef = controller.Save();
        System.assert(tempRef == null,'Cart should not have been able to be submitted, but it was.');
        
        cartItemLine.NU__IsInCart__c = true;
        update cartItemLine;
    }
    
    static testMethod void testOrderPurchaseSponsorship_SubmitCart() {
        setupTest();
        
        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorshipWithExistingCart(cart.Id, cartItem.Id);
        
        // Test #6: Submit Cart
        PageReference tempRef = controller.Save();
        System.assert(tempRef != null, 'Something went wrong when submitting the sponsorship cart.');
    }
}