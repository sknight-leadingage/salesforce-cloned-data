<apex:page controller="ConferenceReports_Controller">
<apex:pageMessages id="msg" />
 <apex:form >
    <apex:pageBlock title="{! Conference.Name }: Reports Menu" mode="maindetail">
    <c:ConferenceNavComponent eventId="{! EventId }" />
    <br /><br />
    <h1>Brochure Development</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduPostIt}">Post-It Temple for Session Board<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduSpeakerConflict}">Speaker Conflicts<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduSessionComparison}">Companies in Program<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduSessionContentCategories}">Content Categories by Session<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduSessionListing}">Onsite Session Listing<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduSessionHistory}">Session Change Log / History (By Date or By Staff Name)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduBrochureCopyFormatted}">Brochure Copy Formatted<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduBrochureCopyTable}">Brochure Copy Table<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduBrochureSessionPlanner}">Brochure Session Planner<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
    </ul>
    <h1>CEs</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduCEsSessSummarySheetsDOM}">Session Summary Sheets with Domains of Practice<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduCEsDomPractice}">Domains of Practice: KS<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="domp" value="KS"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduCEsDomPractice}">Domains of Practice: MO<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="domp" value="MO"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduCEsDomPractice}">Domains of Practice: RCFE<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="domp" value="RCFE"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduCEsByCategory}">Sessions by CE Category<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
    </ul>
    <h1>Onsite Materials</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduOnsiteEstimatedAttendance}">Estimated Attendance<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduMaterials}">CE Verification Form (PA)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="cevp"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduMaterials}">CE Verification Form (AM)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="ceva"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduOnsiteEvalForms}">Evaluations Forms Templates<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduMaterials}">Monitor Assessment Forms<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="mona"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduMaterials}">Session Signs<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="sgn"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduMaterials}">Monitor Guidelines (AM/PEAK)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="mgap"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduMaterials}">Monitor Guidelines (PA)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="mgpa"/></apex:outputLink></li>
    </ul>
    <h1>Proposals</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduProposalListing}">Proposal Listing (Proposal #/Submitter/Company/Track)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduProposalStatus}">Proposal Status (Proposal #/Submitter/Company/Track/Sponsors)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduProposalsMostFields}">Proposals Data: Most Fields<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduProposalChangelog}">Proposal Change Log / History (By Status or By Track)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
    </ul>
    <h1>Sessions</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduSessionsSessionListing}">Session Listing (Session #/Timeslot/Track, with and without Proposal #)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduSessionsSessionProofs}">Session Proofs (Session #/Timeslot/Track/Edu Staff)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduCEsSessSummarySheetsDOM}">Session Summary Sheets (By Session #)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="sn"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduCEsSessSummarySheetsDOM}">Session Summary Sheets (By Timeslot)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="ts"/></apex:outputLink></li>
    </ul>
    <h1>Speakers</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduSpeakerListings}">Speaker Listing (By Session #/Speaker/Company/Timeslot)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
    </ul>
    <h1>Speaker Hub</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduExcelExport}">Audio Visual Requests - All Fields<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-av"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduExcelExport}">Audio Visual Requests - Complete/Incomplete<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-avc"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduHubListing}">Audio Visual Requests - Status<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-avs"/></apex:outputLink></li>

        <li><apex:outputLink value="{! $Page.EduExcelExport}">Speaker Profile - All Fields<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-sp"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduExcelExport}">Speaker Profile - Complete/Incomplete<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-spc"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduHubListing}">Speaker Profile - Status<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-sps"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduExcelExport}">Speaker Profile - CE Fields<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-spce"/></apex:outputLink></li>

        <li><apex:outputLink value="{! $Page.EduExcelExport}">Presentation Materials - Complete/Incomplete<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-pmc"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduHubListing}">Presentation Materials - Status<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub-pms"/></apex:outputLink></li>

        <li><apex:outputLink value="{! $Page.EduFormatted}">Report 7 - Appendix A<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="hub"/></apex:outputLink></li>
        
    </ul>
    <br />
    <h1>Other Reports</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduExcelExport}">CE Laser<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="ces"/></apex:outputLink></li>
    </ul>
    <br />
    <h1>Salesforce Reports</h1>
    <ul>
    <apex:repeat value="{!ReportNames}" var="a">
        <li><a href="/{!a.Id}?pv0={!URLENCODE(Conference.Name)}">{!a.Name}</a></li>
    </apex:repeat>
    </ul>
    <h1>Evaluations Reports</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Attendance Ranked by Track (Attendance_byTrack)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="sarbt"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsMailingLabels}">Mailing Labels<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Session Averages (Averages_bySession#)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="savgbys"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Speaker Averages (Averages_bySpeaker)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="savgbyspk"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Program Averages (CompositeScores_EdProgram)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="edprog"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Session Composite Scores (CompositeScores_Sessions)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="sesscs"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Sessions Ranked by Attendance (Ranked_byAttendance)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="srba"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Sessions Ranked by Overall Average (Ranked_byOverallAvg)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="sessoa"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Sessions Ranked by Speaker (Ranked_bySpeaker)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="srbys"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Sessions Ranked by Timeslot (Ranked_byTimeslot)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="sessoats"/></apex:outputLink></li>
        <li><apex:outputLink value="{! $Page.EduEvalsComposites}">Sessions Ranked by Track (Ranked_byTrack)<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="rpt" value="sessoat"/></apex:outputLink></li>
    </ul>
    <h1>Evaluations Reports: Custom Excel</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduExcelExport}">Evaluations Data: Most Fields<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/><apex:param name="mt" value="mfl"/></apex:outputLink></li>
    </ul>
    <h1>Evaluations Reports: Salesforce</h1>
    <ul>
    <apex:repeat value="{!EvalReportNames}" var="a">
        <li><a href="/{!a.Id}?pv0={!URLENCODE(Conference.Name)}">{!a.Name}</a></li>
    </apex:repeat>
    </ul>
    <h1>Conference Administration</h1>
    <ul>
        <li><apex:outputLink value="{! $Page.EduConfAdmin}">Call for Session<apex:param name="{!QSEventIdParamName}" value="{!EventId}"/></apex:outputLink></li>
    </ul>
    </apex:pageBlock>
 </apex:form>
</apex:page>