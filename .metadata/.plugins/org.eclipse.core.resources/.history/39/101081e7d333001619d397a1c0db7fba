<apex:page controller="MembersByStateReportController" showHeader="{!!isCheckedHideMenus}" sidebar="{!!isCheckedHideMenus}" renderAs="{!IF(isCheckedRenderAsPDF,'pdf', null)}" applyHtmlTag="{!!isCheckedHideMenus}">
    <head>
    <style type="text/css">
        .headline {
            font-family: "Times New Roman",Cambria, Times, serif;
            font-size: 24px;
            font-weight: bold;
            color: #365F91;
        }
        .subheadline {
            font-family: "Times New Roman",Cambria, Times, serif;
            font-size: 12px;
            font-weight: bold;
            color: #365F91;
        }
        .count {
            font-family: "Times New Roman",Cambria, Times, serif;
            font-size: 11px;
            font-weight: bold;
            color: #4F81BD;
        }
        .entry {
            break-inside: avoid-column;
            -webkit-column-break-inside: avoid;
            page-break-inside: avoid;
            height: 150px;
            width: 45%;
            padding: 5px 5px 5px 25px;
            overflow-y: hidden;
            float: left;
        }
        .business {
            font-family: "Times New Roman", Calibri, Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: bold;
        }
        .services {
            font-family: "Times New Roman",Calibri, Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-style: italic;
        }
        .address {
            font-family: "Times New Roman",Calibri, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        @media screen {
            div.divFooter {
                display: none;
            }
        }
    </style>
    <style type="text/css" media="print">
            .printhide {
                display: none;
            }
            .bPageTitle {
                display: none;
            }
            div.divFooter {
                font-family: "Times New Roman",Calibri, Arial, Helvetica, sans-serif;
                font-size: 12px;
                font-weight: bold;
                align: center;
                bottom: 0;
            }
    </style>
    </head>
    
    <body style="font-family: Calibri, Arial, Helvetica, sans-serif;">

    <apex:outputPanel rendered="{!IF(isCheckedRenderAsPDF,false,true)}">
    <apex:define name="sectionHeader">
        <div style="float: left;"><apex:sectionHeader title="Members By State Report" /></div>
        <img style="float: right" src="{!$Resource.Report_Logo}" />
        <apex:pageMessages />
    </apex:define>
    
    <div style="float: none; clear: both">
    <apex:form id="options">
        <apex:selectList value="{!SelectedState}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select State --" />
            <apex:selectOptions value="{!states}"/>
        </apex:selectList>

       <apex:selectList value="{!SelectedTypeProvider}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select Provider Type --" />
            <apex:selectOptions value="{!types}"/>
        </apex:selectList>        
        
        <apex:selectList value="{!SelectedLayout}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select layout --" />
            <apex:selectOption itemValue="1" itemLabel=" -- Internal --"/>
            <apex:selectOption itemValue="2" itemLabel=" -- Senate offices --"/>
            <apex:selectOption itemValue="3" itemLabel=" -- Congressional offices --"/>
        </apex:selectList>

        <apex:commandButton action="{!Run}" value="Run Report"/>
        
        <br />
        Show as PDF (Check this box and click Run Report for well-formatted print output) <apex:inputCheckbox value="{!isCheckedRenderAsPDF}"/>
        <br />
        
        If you would like to add to the headling between LeadingAge and Members: <apex:inputText value="{!HeadlineAddones}" id="HeadlineAddones" html-placeholder="   Headline Add ons "/>
        <br />
        Please type in the text you would like in the footer of the report: <apex:inputText style="width:300px" value="{!FooterText}" id="FooterText" html-placeholder="Ex:Name and Phone number, Email"/>

    </apex:form>
    </div><div style="float: none; clear: both"></div>
    </apex:outputPanel>

    <apex:outputPanel rendered="{!IF(isCheckedRenderAsPDF,true,false)}">

        <img style="center; clear: both" src="{!$Resource.Report_Logo}" />
    </apex:outputPanel>

    <div style="text-align: center; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 16pt;clear: both">
    <p><span class="headline">LeadingAge {!HeadlineAddones} Members: {!SelectedStateName} ({!RecordsCount})</span></p><br />

    </div>
    
    <br />



    <div style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt">
        <apex:repeat value="{!records}" var="r" >
              
            <div style="page-break-inside: avoid; width: 45%; padding: 5px 5px 5px 5px; float: left;">
            <div style="position: relative; break-inside: avoid-column; -webkit-column-break-inside: avoid; min-height: 100px; max-height: 150px; overflow-y: hidden;">
            <span style="font-size: 14pt; font-weight: bold;"><apex:outputtext value="{!r.Name}" /></span><br />
            <apex:outputPanel rendered="{!IF(SelectedLayout=1,true,false)}">
            <span class="address"><apex:outputText rendered="{!IF(r.NU__Affiliates__r.size > 0, TRUE, FALSE)}">
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.Name}" />, &nbsp;
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.PersonTitle}" /> &nbsp;|&nbsp;
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.PersonEmail}" />
            </apex:outputText></span><br />
            </apex:outputPanel>
            <apex:outputPanel rendered="{!IF(SelectedLayout=1 || SelectedLayout=3,true,false)}">
            <span class="address"><apex:outputtext value="{!r.BillingStreet}" /><br />
            <apex:outputtext value="{!r.BillingCity}" />,&nbsp;<apex:outputtext value="{!r.BillingState}" />&nbsp;<apex:outputtext value="{!r.BillingPostalCode}" /></span><br />
            </apex:outputPanel>
            <apex:outputPanel rendered="{!IF(SelectedLayout=2,true,false)}">
            <span class="address">
            <apex:outputtext value="{!r.BillingCity}" />,&nbsp;<apex:outputtext value="{!r.BillingState}"/> </span><br />
            </apex:outputPanel>
            <apex:outputPanel >
            <span class="services"><apex:outputtext value="{!r.Provider_Type__c}" /></span> <br />
            <span style="font-size: 12px;">
            Nursing Beds: <apex:outputtext value="{!NursingBeds[r.Id]}" /> <br />
            Assisted Living Units: <apex:outputtext value="{!AssistedLivingBeds[r.Id]}" /><br />
            Senior Housing Units:  <apex:outputtext value="{!SeniroHousingUnits[r.Id]}" /> <br/></span>
            </apex:outputPanel>
            <br/>
            </div>
            <br/>
            </div>
            <br/>
        </apex:repeat>
    </div>

    <div style="float: none; clear: both; padding-top: 25px;">
        <div class="divFooter" style="text-align: center; clear: both; position: relative;">
        <p>{!FooterText}</p>
        </div>    
    </div><div style="float: none; clear: both"></div>
    </body>

</apex:page>