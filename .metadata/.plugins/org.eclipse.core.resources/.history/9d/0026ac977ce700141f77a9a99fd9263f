<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>BankGLAccount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bank GL Account</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Only active General Ledgers can be used.</errorMessage>
            <filterItems>
                <field>GLAccount__c.Status__c</field>
                <operation>equals</operation>
                <value>Active</value>
            </filterItems>
            <infoMessage>Only active General Ledgers can be used.</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>GLAccount__c</referenceTo>
        <relationshipLabel>Bank Accounts</relationshipLabel>
        <relationshipName>BankAccounts</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Bank Accounts</relationshipLabel>
        <relationshipName>BankAccounts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Bank Account</label>
    <listViews>
        <fullName>NU_AllBankAccounts</fullName>
        <columns>NAME</columns>
        <columns>BankGLAccount__c</columns>
        <columns>Status__c</columns>
        <columns>Entity__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All Bank Accounts</label>
    </listViews>
    <nameField>
        <label>Bank Account Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Bank Accounts</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>BankGLAccount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Entity__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>BankGLAccount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Entity__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BankGLAccount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Entity__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Entity__c</searchFilterFields>
        <searchResultsAdditionalFields>BankGLAccount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Entity__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>BankAccountGLAccountEntityMustMatch</fullName>
        <active>true</active>
        <description>The Bank Account GL Account&apos;s Entity must match this one.</description>
        <errorConditionFormula>AND( IsBlank( Entity__c )=False,
IsBlank(  BankGLAccount__c  )=False,
Entity__c &lt;&gt; BankGLAccount__r.Entity__c )</errorConditionFormula>
        <errorDisplayField>BankGLAccount__c</errorDisplayField>
        <errorMessage>The Bank Account GL Account&apos;s Entity must match this one.</errorMessage>
    </validationRules>
</CustomObject>
