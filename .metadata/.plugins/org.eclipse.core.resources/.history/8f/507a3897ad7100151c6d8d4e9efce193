public with sharing class ViewRenewalNoticeController {

    public Date invoiceDateOverride {
        get {
            return RenewalNoticeUtil.invoiceDateOverride;
        }
    }

    public static Map<Id,List<MembershipBillingInfo>> getMembershipBillingInfos() {
        Map<Id,List<MembershipBillingInfo>> memberBillingInfos = new Map<Id,List<MembershipBillingInfo>>();

        for (RenewalWrapper rw : RenewalWrappers) {
            Id accountId = rw.account.Id;
            Decimal membershipPrice = 0.0;
            String year = String.valueOf(rw.startDate.year());

            // only update the membershipPrice if there is only one primary product; otherwise we don't know what the total price should be
            if (rw.primaryProducts != null && rw.primaryProducts.size() == 1) {
                membershipPrice += rw.primaryProducts.get(0).NU__ListPrice__c;
            }

            // add in the other required products as well
            for (NU__Product__c prod : rw.requiredProducts) {
                membershipPrice += prod.NU__ListPrice__c;
            }

            MembershipBillingInfo memberBillingInfo = new MembershipBillingInfo(accountId, membershipPrice, year, rw.membershipType.Id, null, null, null);

            // we can't have the same account twice in a renewal notice, so therefore there is no way they would be getting billed twice
            memberBillingInfos.put(accountId,new List<MembershipBillingInfo>{memberBillingInfo});
        }
        return memberBillingInfos;
    }

    private List<Account> accts = new List<Account>();
    public List<Account> Accounts {
        get { return accts; }
        set {
            if (membershipRelationship != null) {
                List<Account> tempAccounts = value;

                if (tempAccounts != null && tempAccounts.size() > 0) {
                    accts = tempAccounts;

                    Load();
                }
            }
        }
    }

    private Id accountIdPriv;
    public Id accountId {
        get {
            return accountIdPriv;
        }
        set {
            if (membershipRelationship != null) {
                accountIdPriv = value;

                if (accountIdPriv != null) {
                    Account tempAccount = new Account(Id = accountIdPriv);
                    Accounts = new List<Account>{tempAccount};
                }
            }
        }
    }

    public String membershipRelationship {
        get {
            return RenewalNoticeUtil.membershipRelationship;
        }
    }

    public class RenewalWrapper implements Comparable {

        public RenewalWrapper(Account account) {
            this.Account = account;
        }

        public Account account { get; set; }
        public Account primaryContact { get; set; }

        public Boolean useOrgAddresses { get; set; }

        public NU__Entity__c entity { get; set; }

        public List<NU__Product__c> primaryProducts { get; set; }
        public List<NU__Product__c> requiredProducts { get; set; }
        public List<NU__Product__c> optionalProducts { get; set; }

        public NU__MembershipType__c membershipType { get; set; }

        public List<String> creditCardsAccepted { get; set; }

        public Date startDate { get; set; }
        public Date endDate { get; set; }

        public Decimal balance { get; set; }

        public String accountNumber { get; set; }
        public Date invoiceDate { get; set; }

        public Integer compareTo(Object compareTo) {
            // Cast argument
            RenewalWrapper compareToAccount = (RenewalWrapper)compareTo;

            // The return value of 0 indicates that both elements are equal.

            Integer returnValue = 0;
            if (account.Name > compareToAccount.account.Name) {
                // Set return value to a positive value.

                returnValue = 1;
            } else if (account.Name < compareToAccount.account.Name) {
                // Set return value to a negative value.

                returnValue = -1;
            }

            return returnValue;
        }
    }
    public static List<RenewalWrapper> RenewalWrappers { get; set; }

    private void Load() {
        accts = GetAccountInformation(accts);
        if (Accounts == null || Accounts.size() == 0) {
            return;
        }

        Map<Id, RenewalWrapper> wrapperMap = new Map<Id, RenewalWrapper>();

        // query entity information
        Map<Id,NU__Entity__c> entityInformation = GetEntityInformation();
        Map<Id, List<String>> entityCreditCardsAccepted = GetEntityCreditCardIssuersAccepted();

        Map<Id,NU__MembershipType__c> membershipTypes = new Map<Id,NU__MembershipType__c>(RenewalNoticeUtil.Instance.getMembershipTypes());

        // get membership type product links, grouped by membership type
        Map<Object,List<SObject>> membershipTypeMtpls = NU.CollectionUtil.groupSObjectsByField(RenewalNoticeUtil.Instance.getMtpls(),'NU__MembershipType__c');

        // bulk query the membership types, products, accounts, and memberships so the custom pricing manager doesn't need to query each individually
        CustomPricingManager.PopulateProducts(NU.CollectionUtil.getLookupIds(RenewalNoticeUtil.Instance.getMtpls(), 'NU__Product__c'));
        CustomPricingManager.PopulateMembershipTypes(membershipTypes.keySet());
        CustomPricingManager.PopulateAccounts(Accounts);
        CustomPricingManager.PopulateAccountMemberships(Accounts);

        // populate the Renewal Wrappers
        for (Account account : Accounts) {
            RenewalWrapper rw = new RenewalWrapper(account);

            // determine the primary contact, if the account is not a person account
            if (!account.IsPersonAccount && account.NU__Affiliates__r != null && account.NU__Affiliates__r.size() > 0) {
                NU__Affiliation__c affiliation = account.NU__Affiliates__r[0];
                Account primaryContact = new Account(Id = affiliation.NU__Account__c,
                    Name = affiliation.NU__Account__r.Name,
                    BillingStreet = affiliation.NU__Account__r.BillingStreet,
                    BillingCity = affiliation.NU__Account__r.BillingCity,
                    BillingState = affiliation.NU__Account__r.BillingState,
                    BillingPostalCode = affiliation.NU__Account__r.BillingPostalCode,
                    BillingCountry = affiliation.NU__Account__r.BillingCountry);

                rw.primaryContact = primaryContact;

                rw.useOrgAddresses = isAddressInformationBlank(primaryContact);
            }
            else {
                rw.useOrgAddresses = true;
                rw.primaryContact = new Account();
            }

            NU__Membership__c membership = (NU__Membership__c)account.getSObject(membershipRelationship);
            NU__MembershipType__c membershipType = membershipTypes.get(membership.NU__MembershipType__c);
            rw.membershipType = membershipType;

            // populate entity information
            rw.entity = entityInformation.get(membership.NU__MembershipType__r.NU__Entity__c);
            rw.creditCardsAccepted = entityCreditCardsAccepted.get(rw.entity.Id);

            // get the products for the account
            List<NU__MembershipTypeProductLink__c> accountMtpls = new List<NU__MembershipTypeProductLink__c>();
            if (membershipTypeMtpls != null && membershipTypeMtpls.size() > 0
                && membershipTypeMtpls.get(membershipType.Id) != null) {

                accountMtpls = membershipTypeMtpls.get(membershipType.Id);
            }

            // remove unncessary primary products if we already know what the account's primary membership product should be
            if (!RenewalNoticeUtil.showAllProducts &&
                (membership.MembershipTypeProductName__c != null || membership.NU__OrderItemLine__r.NU__MembershipTypeProductLink__c != null)) {

                List<NU__MembershipTypeProductLink__c> updatedAccountMtpls = new List<NU__MembershipTypeProductLink__c>();

                for (NU__MembershipTypeProductLink__c mtpl : accountMtpls) {
                    // we only want the primary membership product and all products that are not primary
                    if (mtpl.Id == membership.NU__OrderItemLine__r.NU__MembershipTypeProductLink__c
                        || mtpl.NU__Product__r.Name == membership.MembershipTypeProductName__c
                        || mtpl.NU__Purpose__c != Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY) {
                        updatedAccountMtpls.add(mtpl);
                    }
                }

                accountMtpls = updatedAccountMtpls;
            }
            
            //Remove everything but the primary products to fix the non joint provider renewal notice
            if (!RenewalNoticeUtil.showAllProducts && membership.MembershipTypeProductName__c != null && membership.MembershipTypeProductName__c == Constant.NON_JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME){
                List<NU__MembershipTypeProductLink__c> updatedMfields = new List<NU__MembershipTypeProductLink__c>();

                for (NU__MembershipTypeProductLink__c mtpl1 : accountMtpls) {
                    if (mtpl1.NU__Purpose__c == Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY) {
                        updatedMfields.add(mtpl1);
                    }
                }

                accountMtpls = updatedMfields;
            }
            
            Map<Object,List<SObject>> mtplsByPurpose = NU.CollectionUtil.groupSObjectsByField(accountMtpls,'NU__Purpose__c');
            Integer numberOfPrimaryMtpls = (mtplsByPurpose.get(Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY) != null
                ? mtplsByPurpose.get(Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY).size()
                : 0);

            // get product pricing
            Map<Id,Decimal> pricing = GetProductPrices(NU.CollectionUtil.getLookupIds(accountMtpls,'NU__Product__c'), account.Id, membership.NU__MembershipType__c);
            rw.primaryProducts = new List<NU__Product__c>();
            rw.requiredProducts = new List<NU__Product__c>();
            rw.optionalProducts = new List<NU__Product__c>();

            // update products with the pricing
            rw.balance = 0.0;
            for (NU__MembershipTypeProductLink__c mtpl : accountMtpls) {
                Decimal price = pricing.get(mtpl.NU__Product__c);

                // only update the balance if there is only one primary product; otherwise we don't know what the total price should be
                if (numberOfPrimaryMtpls == 1) {
                    rw.balance += price;
                }

                NU__Product__c aProduct = new NU__Product__c(Id = mtpl.NU__Product__c, Name = mtpl.NU__Product__r.Name, NU__ListPrice__c = price);
                if (mtpl.NU__Purpose__c == Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_PRIMARY) {
                    rw.primaryProducts.add(aProduct);
                }
                else if (mtpl.NU__Purpose__c == Constant.MEMBERSHIP_TYPE_PRODUCT_LINK_PURPOSE_REQUIRED) {
                    rw.requiredProducts.add(aProduct);
                }
                else {
                    rw.optionalProducts.add(aProduct);
                }
            }

            // figure out start date & end dates
            rw.startDate = membership.NU__EndDate__c.addMonths(1).toStartOfMonth();
            rw.endDate = rw.startDate.addMonths((Integer)membership.NU__MembershipType__r.NU__Term__c).addDays(-1);

            rw.invoiceDate = (invoiceDateOverride != null ? invoiceDateOverride : Date.today());

            rw.accountNumber = (String)account.get('LeadingAge_ID__c');

            wrapperMap.put(account.Id, rw);
        }

        RenewalWrappers = wrapperMap.values();
        RenewalWrappers.sort();
    }

    private List<Account> GetAccountInformation(List<Account> accountsToValidate) {

        Map<Id,Account> mapAccountsToValidate = new Map<Id,Account>();
        mapAccountsToValidate.putAll(accountsToValidate);
        RenewalNoticeUtil.validatedAccounts = null;
        RenewalNoticeUtil.accountIds = mapAccountsToValidate.keySet();

        return RenewalNoticeUtil.Instance.getValidatedAccounts();
    }

    private Map<Id,NU__Entity__c> privEntityInformation = null;
    private Map<Id,NU__Entity__c> GetEntityInformation() {
        if (privEntityInformation == null) {
            privEntityInformation = new Map<Id,NU__Entity__c>([SELECT
                Id,
                Name,
                NU__LogoURL__c,
                NU__Street__c,
                NU__City__c,
                NU__State__c,
                NU__PostalCode__c,
                NU__Country__c,
                NU__Phone__c,
                NU__Website__c,
                NU__RemittanceStreet__c,
                NU__RemittanceCity__c,
                NU__RemittanceState__c,
                NU__RemittancePostalCode__c,
                NU__RemittanceCountry__c,
                NU__RemittanceEnabled__c,
                (SELECT NU__CreditCardIssuer__r.Name
                    FROM NU__CreditCardIssuers__r
                    WHERE NU__Status__c = :Constant.ENTITY_CREDIT_CARD_ISSUERS_ACTIVE
                    ORDER BY NU__CreditCardIssuer__r.Name
                )
                FROM NU__Entity__c
                WHERE NU__Status__c = :Constant.ENTITY_ACTIVE]);
        }
        return privEntityInformation;
    }

    private Map<Id, List<String>> entityCCIsAccepted = null;
    private Map<Id, List<String>> GetEntityCreditCardIssuersAccepted(){
        if (entityCCIsAccepted == null) {
            entityCCIsAccepted = new Map<Id, List<String>>();

            Map<Id,NU__Entity__c> entityInformation = GetEntityInformation();

            for (NU__Entity__c entity : entityInformation.values()) {
                if (entity.NU__CreditCardIssuers__r == null || entity.NU__CreditCardIssuers__r.size() == 0) {
                    continue;
                }

                List<String> entityCreditCardsAccepted = new List<String>();

                for (NU__EntityCreditCardIssuer__c entityCCI : entity.NU__CreditCardIssuers__r) {
                    entityCreditCardsAccepted.add(entityCCI.NU__CreditCardIssuer__r.Name);
                }

                entityCCIsAccepted.put(entity.Id, entityCreditCardsAccepted);
            }
        }

        return entityCCIsAccepted;
    }

    private Map<Id,Decimal> GetProductPrices(Set<Id> productIds, Id accountId, Id membershipTypeId) {
        // populate ProductPricingInfos
        List<NU.ProductPricingInfo> productPricingInfos = new List<NU.ProductPricingInfo>();
        for (Id productId : productIds) {
            NU.ProductPricingInfo ppi = new NU.ProductPricingInfo();
            ppi.ProductId = productId;
            ppi.Quantity = 1;
            productPricingInfos.add(ppi);
        }

        // create PriceClassRequest
        NU.PriceClassRequest pcr = new NU.PriceClassRequest();
        pcr.TransactionDate = DateTime.now();
        pcr.AccountId = accountId;

        // create ProductPricingRequest
        NU.ProductPricingRequest ppr = new NU.ProductPricingRequest();
        ppr.AccountId = accountId;
        ppr.TransactionDate = pcr.TransactionDate;
        ppr.ProductPricingInfos = productPricingInfos;
        ppr.PriceClassName = NU.PriceClassManager.getPricingManager().getPriceClass(pcr);
        ppr.MembershipTypeId = membershipTypeId;

        // get product prices
        return NU.PriceClassManager.getPricingManager().GetProductPrices(ppr);
    }

    private Boolean isAddressInformationBlank(Account account) {
        if (account.BillingStreet == null &&
            account.BillingCity == null &&
            account.BillingState == null &&
            account.BillingPostalCode == null &&
            account.BillingCountry == null) {
                return true;
            }

        return false;
    }
}