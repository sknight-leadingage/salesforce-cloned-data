/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /*
 Member Status Fields

We’ve added State_Partner_Member__c and LeadingAge_Member__c fields to the Account object: in the case of the State field, 
to add the ability for states to easily see if the account is a member, and for the LeadingAge field, to replace the formulas that currently do this job.  
Current code is implemented in the States Sandbox ⇒ AccountTriggerHandlers trigger, setStateAndLeadingMemberField method.

To test the current implementation, we need to set up the following test conditions:

Test Rules: General Setup
During the tests we’ll need to be able to create test data for: Entities (Which requires GLs), State Partner Logins, State Partner Accounts, Provider Accounts, 
Individual Accounts, Membership Types, Membership Products, Memberships, 

Rule #1
Test: Create a Provider account.
Assertion: Both fields should be Non-member.

Rule #2
Test: Create a Provider with a LeadingAge National membership.  
To do this in a test class, it’s probably necessary to create an Entity and ensure the Entity is named “LeadingAge” and also create an account record of type State Partner 
(E.g. insert a state partner), and change the name of the state partner record to “LeadingAge”, before creating the Provider and Membership/Membership Type entries.
Assertion: State Member field should be Non-member, LeadingAge Member field should be Member.

Rule #3
Test: Create a Provider with a LeadingAge National membership and then add a State Membership product to this.  
To create the State Membership, it’s probably necessary to set up an entity and name it as a random state, e.g. change its name to “LeadingAge Washington” 
and then set up the account that’s a State Partner record type, with the same state name, etc, before creating the membership/membership type.
Assertion: Both member fields should show Member.


Account individualAcct = DataFactoryAccountExt.insertIndividualAccount();
    	
NU__Affiliation__c primaryAffiliation = NU.DataFactoryAffiliation.insertAffiliation(individualAcct.Id, providerAcct.Id, true);
Rule #4
Test: taking the Provider record from rule #3, add an Individual with a Primary Affiliation to this Provider.
Assertion: Both member fields should show Member, for the Individual & Provider accounts.

Rule #5
Test: taking the records from rule #4, cancel the National membership.
Assertion: Member fields should read National field = Non-member, State field = Member, for the Individual & Provider accounts.

Rule #6
Test: taking the records from rule #5, add another Individual with Primary Affiliation to the Provider.
Assertion: Individual should show member fields: National field = Non-member, State field = Member.

Rule #7
Test: taking the records from rule #6, reinstate (Or add a new) National membership.
Assertion: Both member fields should show Member again, for the Provider and both child accounts.

Rule #8
Test: taking the records from rule #7, cancel the State membership.
Assertion: Both member fields should show National field = Member and State field = Non-member, for the Provider and both child accounts.
 
 */
@isTest
private class TestMemberStatusFields {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        NU__MembershipType__c providerMT = DatafactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        
        Id EntId = providerMT.NU__Entity__c;
        NU__Entity__c anyEntity = new NU__Entity__c();
        anyEntity = [SELECT Id, Name FROM NU__Entity__c WHERE Id = :EntId];
        anyEntity.Name = 'LeadingAge';
        anyEntity.NU__Status__c = 'Active';
        update anyEntity;
        system.assertNotEquals(null, anyEntity.Name);
        
    	NU__GLAccount__c glAccount = NU.DataFactoryGLAccount.insertRevenueGLAccount(anyEntity.Id);
    	Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount(15000000);
    	providerAccount.NU__PrimaryEntity__c = anyEntity.Id;
    	update providerAccount;
    	
    	Account StatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
    	StatePartner.Name = 'LeadingAge';
    	update StatePartner;
    	
    	//providerAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, StatePartner.Id);
    	/*
    	Rule #1
		Test: Create a Provider account.
		Assertion: Both fields should be Non-member.
    	
    	*/
    	
    	system.assertEquals(providerAccount.LeadingAge_Member__c, 'Non-member');
    	system.assertEquals(providerAccount.State_Partner_Member__c, 'Non-member');
    	
    	
    	/*
    	Rule #2
Test: Create a Provider with a LeadingAge National membership.  
To do this in a test class, it’s probably necessary to create an Entity and ensure the Entity is named “LeadingAge” and also create an account record of type State Partner 
(E.g. insert a state partner), and change the name of the state partner record to “LeadingAge”, before creating the Provider and Membership/Membership Type entries.
Assertion: State Member field should be Non-member, LeadingAge Member field should be Member.
    	
    	*/
    	
    	
    	
    	
    	NU__MembershipType__c JointStateProviderMembershipType;
        Id JointStateProviderProductId;
        JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
        NU__Membership__c insertedMembership = new TestProviderMembershipInserter().insertMembership(providerAccount.Id);
        
        
        system.assertEquals(providerAccount.State_Partner_Member__c, 'Member');
    	system.assertEquals(providerAccount.LeadingAge_Member__c, 'Member');
        
        if (JointStateProviderMembershipType != null){
            JointStateProviderProductId = JointBillingUtil.getJointStateProviderProductId(JointStateProviderMembershipType);
        }
        
        NU__Product__c productToPrice = [select id,
                     Name,
                     RecordType.Name,
                     NU__Entity__c,
                     NU__Entity__r.Name,
                     NU__Event__c,
                     NU__Event__r.Name,
                     NU__Event__r.NU__ShortName__c,
                     NU__EventSessionEndDate__c,
                     NU__EventSessionGroup__c,
                     NU__EventSessionGroup__r.Name,
                     NU__EventSessionSpecialVenueInstructions__c,
                     NU__EventSessionStartDate__c,
                     NU__IsEventBadge__c,
                     NU__IsFee__c,
                     NU__IsShippable__c,
                     NU__IsTaxable__c,
                     NU__ListPrice__c,
                     NU__ShortName__c,
                     NU__QuantityMax__c,
                     NU__Status__c,
                     (select NU__PriceClasses__c,
                             NU__DefaultPrice__c,
                             NU__EarlyPrice__c,
                             NU__LatePrice__c,
                             Name
                        from NU__SpecialPrices__r
                     )
                from NU__Product__c
                where id = :JointStateProviderProductId];
        

    }
}