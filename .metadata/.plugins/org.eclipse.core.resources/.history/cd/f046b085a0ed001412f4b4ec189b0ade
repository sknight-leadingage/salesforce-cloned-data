//email results of unit tests nightly at 6am
//following AutomatedTestingJobQueuer

//To run once in an anonymous window in Eclipse or Developer Console:
//Database.executeBatch(new AutomatedTestingJob());

//To schedule nightly in an anonymous window in Eclipse or Developer Console:
//AutomatedTestingJob.createDaily6AMScheduledJob();

//To unschedule, delete the job from the list of scheduled jobs.

global with sharing class AutomatedTestingJob implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        doExecute();
    }

    @future (callout=true)
    public static void doExecute(){
        processAsyncResults();
    }
    
    public static void processAsyncResults(){
        List<AutomatedTestingQueue__c> queuedTests = 
           [select id,
                   name,
                   AsyncId__c,
                   NumberOfTestClassesEnqueued__c
              from AutomatedTestingQueue__c
              order by createddate
             limit 5];
        
        if (NU.CollectionUtil.listHasElements(queuedTests)){
            Set<Id> AsyncIds = NU.CollectionUtil.getLookupIds(queuedTests, 'AsyncId__c');

            List<ApexTestQueueItem> queuedItems = checkClassStatus(AsyncIds);
            
            Map<Object, List<ApexTestQueueItem>> groupedTestsByJob = 
               NU.CollectionUtil.groupSObjectsByField(queuedItems, 'ParentJobId');
            
            Set<Id> completedAsyncIds = getCompletedAsyncJobs(groupedTestsByJob);
            List<AutomatedTestingQueue__c> queuedTestsToDelete = new List<AutomatedTestingQueue__c>();
            
            if (NU.CollectionUtil.setHasElements(completedAsyncIds)){
                
                List<ApexTestResult> testResults = checkMethodStatus(completedAsyncIds);
                
                Map<object, List<ApexTestResult>> groupedTestResultsByJob = 
                   NU.CollectionUtil.groupSObjectsByField(testResults, 'AsyncApexJobId');
                 
                
                for (List<ApexTestResult> jobTestResults : groupedTestResultsByJob.values()){
                    sendTestResultEmail(jobTestResults);
                }
                
                for (AutomatedTestingQueue__c queuedTest : queuedTests){
                    for (Id completedAsyncId : completedAsyncIds){
                        if (queuedTest.AsyncId__c == completedAsyncId){
                            queuedTestsToDelete.add(queuedTest);
                            break;
                        }
                    }
                }
                
                
            }

            for (AutomatedTestingQueue__c queuedTest : queuedTests){
                if (groupedTestsByJob != null && groupedTestsByJob.containsKey(queuedTest.AsyncId__c) == false){
                    queuedTestsToDelete.add(queuedTest);
                }
            }
            
            if (NU.CollectionUtil.listHasElements(queuedTestsToDelete)){
                delete queuedTestsToDelete;
            }
        }
        
    }

    public static List<ApexTestQueueItem> checkClassStatus(Set<ID> jobIds) {
        ApexTestQueueItem[] items = 
           [SELECT ApexClass.Name,
                   Status,
                   ExtendedStatus,
                   ParentJobId
            FROM ApexTestQueueItem 
            WHERE ParentJobId in :jobIds];
            
        for (ApexTestQueueItem item : items) {
            String extStatus = item.extendedstatus == null ? '' : item.extendedStatus;
            System.debug(item.ApexClass.Name + ': ' + item.Status + extStatus);
        }
        
        return items;
    }
    
    public static Set<Id> getCompletedAsyncJobs(Map<Object, List<ApexTestQueueItem>> groupedTestsByJob){
        Set<Id> completedAsyncJobs = new Set<Id>();
        
        for (List<ApexTestQueueItem> jobTests : groupedTestsByJob.values()){
            if (NU.CollectionUtil.listHasElements(jobTests) == false){
                continue;
            }
            
            Boolean allCompleted = true;
            
            Set<String> completedStatuses = new Set<String> { 'Completed', 'Aborted', 'Failed' };
            
            for (ApexTestQueueItem queuedTest : jobTests){
                system.debug('   getCompletedAsyncJobs::queuedTest ' + queuedTest);
                
                if (completedStatuses.contains(queuedTest.Status) == false){
                    allCompleted = false;
                    break;
                }
            }
            
            if (allCompleted == true){
                completedAsyncJobs.add(jobTests[0].ParentJobId);
            }
        }
        
        return completedAsyncJobs;
    }
    
    // Get the result for each test method that was executed. 
    
    public static List<ApexTestResult> checkMethodStatus(Set<ID> jobIds) {
        ApexTestResult[] results = 
           [SELECT Outcome,
                   MethodName,
                   Message,
                   StackTrace,
                   AsyncApexJobId, 
                   ApexClass.Name,
                   ApexClass.Body,
                   ApexClass.LengthWithoutComments,
                   ApexClass.NamespacePrefix,
                   ApexClass.Status,
                   ApexLogId,
                   ApexLog.DurationMilliseconds,
                   ApexLog.Operation,
                   ApexLog.Request,
                   ApexLog.Status,
                   ApexLog.Location,
                   ApexLog.Application
            FROM ApexTestResult 
            WHERE AsyncApexJobId in :jobIds];
            
        for (ApexTestResult atr : results) {
            System.debug(atr.ApexClass.Name + '.' + atr.MethodName + ': ' + atr.Outcome);
            if (atr.message != null) {
                System.debug(atr.Message + '\n at ' + atr.StackTrace);
            }
        }
        
        return results;
    }

    private static void sendTestResultEmail(List<ApexTestResult> jobTestResults){
        system.debug(' In sendTestResultEmail');
            
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        //TO DO: Make the email parameters data driven.
        String nimbleUserEmailAddress = 'errors+lead@nimbleams.com';
        String clientEmailAddress = null;
        String clientName = 'LeadingAge';
        
        if (clientEmailAddress == null) {
            mail.setToAddresses(new String[] { nimbleUserEmailAddress });
        }
        else {
            mail.setToAddresses(new String[] { nimbleUserEmailAddress, clientEmailAddress });
        }
        
        String emailSubject = clientName + ' Production Unit Test Results ' + Date.Today().format(); 
    
        mail.setSubject(emailSubject);

        String testResultEmailbody = getTestResultHtmlEmailBody(jobTestResults);

        mail.setHtmlBody(testResultEmailbody);
        Messaging.sendEmail(new Messaging.Email[] { mail });
        
        system.debug(' sent test results email');
    }
    
    private static String getTestResultHtmlEmailBody(List<ApexTestResult> jobTestResults){
        system.debug(' In getTestResultHtmlEmailBody');
        
        List<ApexTestResult> successTests = new List<ApexTestResult>();
        List<ApexTestResult> failedTests = new List<ApexTestResult>();
        
        for (ApexTestResult jobTestResult : jobTestResults){
            if (jobTestResult.Outcome == 'Pass'){
                successTests.add(jobTestResult);
            }
            else{
                failedTests.add(jobTestResult);
            }
        }
        
        Integer numTestsRun = successTests.size() + failedTests.size();
        Integer numFailures = failedTests.size();
        Integer successNum = numTestsRun - numFailures;
        
        if (successNum < 0){
            successNum = 0;
        }
        
        String testResultBody = '<table>';
        
        
        
        // Unfortunately, css has to be inlined because many email service providers now exclude external CSS
        // because it can pose a security risk.
        testResultBody += '<tr><td>Tests Run:</td><td style="text-align: right;">' + numTestsRun + '</td></tr>';
        testResultBody += '<tr><td>Failure Count:</td><td style="text-align: right;">' + numFailures + '</td></tr>';
        testResultBody += '<tr><td>Success Count:</td><td style="text-align: right;">' + successNum + '</td></tr>';
        
        testResultBody += '</table>';
                
        if (NU.CollectionUtil.listHasElements(failedTests)){
            
            testResultBody += '<div style="margin: 5px 0px; font-weight: bold;">Unit Test Failures</div>';
            
            testResultBody += '<table>';
            
            testResultBody += '<tr>';
            testResultBody += '<th style="text-align: left; padding-left: 5px;">Test Class</th>';
            testResultBody += '<th style="text-align: left; padding-left: 5px;">Unit Test</th>';
            testResultBody += '<th style="text-align: left; padding-left: 5px;">Message</th>';
            testResultBody += '<th style="text-align: left; padding-left: 5px;">Stack Trace</th>';
            testResultBody += '<th style="text-align: left; padding-left: 5px;">Time (Ms)</th>';
            testResultBody += '</tr>';
            
            for (ApexTestResult testFailure : failedTests){
                testResultBody += '<tr>';
                
                testResultBody += '<td style="padding: 5px; vertical-align: top;">' + testFailure.ApexClass.Name +'</td>';
                testResultBody += '<td style="padding: 5px; vertical-align: top;">' + testFailure.MethodName +'</td>';
                testResultBody += '<td style="padding: 5px; vertical-align: top;">' + testFailure.message +'</td>';
                testResultBody += '<td style="padding: 5px; vertical-align: top;">' + testFailure.stackTrace +'</td>';
                testResultBody += '<td style="padding: 5px; vertical-align: top;">' + testFailure.ApexLog.DurationMilliseconds +'</td>';
                
                testResultBody += '</tr>';
            }
            
            testResultBody += '</table>';
        }
        
        return testResultBody;
    }
    
    /*
        Schedule String Format: Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
    */
    
    public static void createDaily6AMScheduledJob(){
        AutomatedTestingJob atj = new AutomatedTestingJob();  
        string sch = '0 0 6 * * ?';  
        system.schedule('Process Queued Unit Tests 6 AM',sch,atj);
    }
    
    public static void createRunEveryHourScheduledJob(){
        AutomatedTestingJob atj = new AutomatedTestingJob();  
        string sch = '0 0 * * * ?';  
        system.schedule('Process Queued Unit Tests Every Hour',sch,atj);
    }
    
    public static void createEvery15MinuteScheduledJobs(){
        AutomatedTestingJob atj = new AutomatedTestingJob();  
        string sch = '0 0 * * * ?';  
        system.schedule('Process Queued Unit Tests Every Top Of The Hour',sch,atj);
        
        sch = '0 15 * * * ?';  
        system.schedule('Process Queued Unit Tests At Each Quarter After',sch,atj);
        
        sch = '0 30 * * * ?';  
        system.schedule('Process Queued Unit Tests At Each Bottom Of The Hour',sch,atj);
        
        sch = '0 45 * * * ?';  
        system.schedule('Process Queued Unit Tests At Each Quarter To The Hour',sch,atj);
    }
    
}