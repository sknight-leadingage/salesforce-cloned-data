<apex:page controller="ConferenceSessionsHubController">
<script>

function setFocusOnLoad() {}

function copyToClipboard (text) {
  window.prompt ("To copy to clipboard press Ctrl+C, Enter:", text);
}

function domClip(ctrl) {
    var skillsSelect = document.getElementById(ctrl);
    if (skillsSelect != null && skillsSelect.type != null && (skillsSelect.type == 'select-one' || skillsSelect.type == 'select-multiple')) {
        var selectedText = skillsSelect.options[skillsSelect.selectedIndex].text;
        copyToClipboard(selectedText);
    }
}

</script>

    <style>
        .pbTitle {
            width: 100% !important;
        }
        
        .pbSubheader {
            color: black !important; 
        }
        
        .sameTextBoxWidth {
            width: 95%;
        }
        
        .sameTextBoxHeight {
            
            height: 100px;
        }
    </style>

    <apex:pageMessages />

    <apex:pageMessage summary="{! Conference.Name } has no sessions."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria == false }" />
    
    <apex:pageMessage summary="No sessions found."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && HasSearchCriteria }" />
                      
    <apex:form id="ConferenceSessionsForm">
        <apex:pageBlock title="{! Conference.Name } Session : {! CurrentSession.Name }">
        
            <div style="clear: both; height: 40px;">
                <apex:panelGrid columns="8" rendered="{! ResultSize > 0 }" style="float: left">
                    <apex:commandLink action="{!first}" rendered="{! ResultSize > 1 && PageNumber != 1 }">First</apex:commandlink>
                    <apex:commandLink action="{!previous}" rendered="{!hasPrevious}">Previous</apex:commandlink>
                    <apex:outputText value="Record {! PageNumber } of {! ResultSize }" />
                    <apex:commandLink action="{!next}" rendered="{!hasNext}">Next</apex:commandlink>
                    <apex:commandLink action="{!last}" rendered="{! ResultSize > 1 && PageNumber != ResultSize }">Last</apex:commandlink>
                    <apex:outputText value=" &nbsp;&nbsp;&nbsp; " escape="false"/>
                    <apex:inputText value="{!RecordNum}" label="Record #" style="width: 25px;" rendered="{!ResultSize > 1}"/>
                    <apex:commandButton action="{!GoToRecord}" rendered="{!ResultSize > 1}" value="GoTo Record"/>
                </apex:panelGrid>
            
           
                <div style="float: right;">
                    <c:ConferenceNavComponent eventId="{! EventId }" />
                </div>
            </div>

            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; 
                <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; 
                <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>
            </div>
            
            <apex:pageBlockSection columns="2" title="Scheduling" rendered="{! ResultSize > 0 }">

                <apex:pageBlockSectionItem >
                    <apex:outputText value="{! $ObjectType.Conference_Session__c.Fields.Timeslot__c.Label }" />
                    <apex:outputPanel >
                        <apex:outputText value="{! CurrentSession.Timeslot__r.Timeslot_Code__c } - {! CurrentSession.Timeslot__r.Timeslot__c }" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <apex:outputField value="{! CurrentSession.Session_Number__c }" />

                <apex:outputField value="{! CurrentSession.Session_Length__c }" />

                <apex:outputField value="{! CurrentSession.Conference_Track__c }" />

            </apex:pageBlockSection>

            <apex:pageBlockSection columns="2" rendered="{! ResultSize > 0 }" title="Main Info">
            
                    <apex:outputField value="{! CurrentSession.Title__c }" />
                
                    <apex:outputField value="{! CurrentSessionProposal.Proposal_Number__c }" />
                
            </apex:pageBlockSection>

            <apex:pageBlockSection title="Requested A/V Equipment" columns="2" rendered="{! ResultSize > 0 }">
                <apex:inputField value="{!CurrentSession.No_Additional_AV__c}" />
                <apex:inputField value="{!CurrentSession.Wireless_Microphone__c}" />
                <apex:inputField value="{!CurrentSession.Aisle_Microphone__c}" />
                <apex:inputField value="{!CurrentSession.Flipchart_and_Markers__c}" />
                <apex:inputField value="{!CurrentSession.Wheelchair_Access_and_Ramp__c}" />
                <apex:inputField value="{!CurrentSession.Other_Setup__c}" />
                <apex:inputField value="{!CurrentSession.Other_Setup_Checkbox__c}" />
                <apex:outputField value="{!CurrentSession.Session_AV_Modified__c}" />
                <apex:inputField value="{!CurrentSession.Session_AV_Validated__c}" />
            </apex:pageBlockSection>

            <apex:pageBlockSection title="Presentation Uploads" columns="2" rendered="{! ResultSize > 0 }">
                <apex:outputField value="{!CurrentSession.Session_Uploads_Modified__c}" />
                <apex:inputField value="{!CurrentSession.Session_Uploads_Validated__c}" />

                <apex:outputText value="There are no uploaded presentations" rendered="{! CurrentSession.Attachments.size == 0 }" />
                
                <apex:pageBlockTable value="{! CurrentSession.Attachments }"
                                     var="a"
                                     rendered="{! CurrentSession.Attachments.size > 0 }"
                                     width="100%">
                                     
                    <apex:column headerValue="Attachment Name">
                      <apex:outputtext value="{! a.Name }" />
                    </apex:column>
                </apex:pageBlockTable>

            </apex:pageBlockSection>
            
            <div style="margin: 12px 0 12px 0; border-top: 2px solid #909090; padding: 8px 0 0 0;">
                <apex:outputText style="font-weight: bold; color: red;" value="Remember to" rendered="{! ResultSize > 0 }"/> &nbsp; <apex:commandButton action="{! Save }" value=" Save " rendered="{! ResultSize > 0 }" style="font-size: 16px" /> &nbsp; <apex:outputText style="font-weight: bold; color: red;" value="your changes!" rendered="{! ResultSize > 0 }"/>

                <apex:commandButton action="{! deleteCurrentConferenceObject }"
                                    value="Delete"
                                    rendered="{! $ObjectType.Conference_Session__c.Deletable && ResultSize > 0 }"
                                    onclick="return confirm('Are you sure?');"
                                    style="margin-left: 10px;" />
                                    
                <apex:commandButton action="{! lock }"
                                    value="Lock"
                                    rendered="{! CurrentSession.Locked__c == false && $ObjectType.Conference_Session__c.Fields.Locked__c.Updateable && ResultSize > 0 }"
                                    style="margin-left: 10px;" />
                <apex:commandButton action="{! unlock }"
                                    value="Unlock"
                                    rendered="{! CurrentSession.Locked__c == true && $ObjectType.Conference_Session__c.Fields.Locked__c.Updateable && ResultSize > 0 }"
                                    style="margin-left: 10px;" />
            </div>
        </apex:pageBlock>

        <apex:pageBlock title="Search">
            <apex:pageBlockSection title="Search" columns="2">
                <apex:inputText value="{! SearchCriteria.SessionNumber }" label="{! $ObjectType.Conference_Session__c.Fields.Session_Number__c.Label }" />
                <apex:inputText value="{! SearchCriteria.Title }" label="Session {! $ObjectType.Account.Fields.PersonTitle.Label }" />

<!--                
                <apex:inputText value="{! SearchCriteria.FirstName }" label="{! $ObjectType.Account.Fields.FirstName.Label }" />
                <apex:inputText value="{! SearchCriteria.LastName }" label="{! $ObjectType.Account.Fields.LastName.Label }" />
                <apex:inputText value="{! SearchCriteria.Company }" label="Company" />
                <apex:inputText value="{! SearchCriteria.EducationStaffFirstName }" label="Education Staff First Name" />
                <apex:inputText value="{! SearchCriteria.EducationStaffLastName }" label="Education Staff Last Name" />
                <apex:inputText value="{! SearchCriteria.LearningObjectivesPhrase }" label="Learning Objectives Phrase" />
                <apex:inputText value="{! SearchCriteria.NotesPhrase }" label="Notes Phrase" />
                <apex:inputText value="{! SearchCriteria.LeadingAgeID }" label="Speaker's LeadingAge ID" />
-->
                
                <apex:inputText value="{! SearchCriteria.TrackName }" label="{! $ObjectType.Conference_Track__c.Fields.Name.Label }" />
                <apex:inputText value="{! SearchCriteria.ProposalNumber }" label="Proposal Number" />
                <apex:inputText value="{! SearchCriteria.TimeslotCode }" label="Timeslot Code" /> 
                
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                    <apex:commandButton value="Search" action="{! search }" />
                    <apex:commandButton value="Clear Search"
                                        action="{! clearSearch }"
                                        style="margin-left: 10px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
    </apex:form>
</apex:page>