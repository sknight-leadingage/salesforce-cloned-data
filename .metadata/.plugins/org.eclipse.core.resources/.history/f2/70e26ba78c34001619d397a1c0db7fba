public with sharing class MembershipBilling {
	public static final String DUPLICATE_MEMBERSHIP_BILLING_FOUND = 'A membership billing record already exists for the supplied membership type and year.';
	
	public static final String MEMBERSHIP_BILLINGS_QUERY = 'SELECT Id, Year__c, MembershipType__c, AmountBilled__c, DonationAmountReceived__c, DuesAmountReceived__c, MembershipsBilled__c, MembershipsRenewed__c, TotalAmountReceived__c, TotalMemberships__c FROM MembershipBilling__c WHERE DoNotCalculateTotals__c = FALSE';
	
	public static Map<Id,List<MembershipBillingInfo>> memberBillingInfos = null;
	private static List<MembershipBillingInfo> memberBillingInfosList = new List<MembershipBillingInfo>();
	private static List<MembershipBillingInfo> memberBillingInfosList() {
		if (memberBillingInfosList.size() == 0 && memberBillingInfos != null) {
			for (List<MembershipBillingInfo> mbis : memberBillingInfos.values()) {
				memberBillingInfosList.addAll(mbis);
			}
		}
		return memberBillingInfosList;
	}
	
	/*
	* Main functionality for creating/updating the membership billing and membership billing line records from account information passed in
	*/
	private static Map<Id,MembershipBilling__c> membershipBillingsMap = null; // membership billings map
	private static Map<String,MembershipBilling__c> membershipBillingsStringMap = null; // membership billings string map
	private static List<MembershipBillingLine__c> membershipBillingLines = null; // membership billing lines
	private static Map<Object,List<SObject>> membershipBillingMBLs = null; // membership billing lines per membership billing
	private static Date billDate = Date.today();
	
	private static Set<Id> membershipTypes = null;
	private static Set<String> years = null;
	
	public static String Bill() {
		membershipTypes = new Set<Id>();
		years = new Set<String>();
		
		// determine the years and membership types for billing
		for (MembershipBillingInfo mbi : memberBillingInfosList()) {
			years.add(mbi.year);
			membershipTypes.add(mbi.membershipType);
		}
		
		// set the Membership Billing Id on the Membership Billing Infos; insert the Membership Billing records that don't exist
		populateMembershipBillingInfoMBIds();

		// get existing membership billing lines and create map to membership billing records
		createMBLsToMembershipBillingsMap();
		
		// determine if membership billing line records already exist or not for the accounts being billed
		// create the ones that don't and update the ones that do
		upsertMembershipBillingLines();
		
		// recalculate the totals on the membership billing records (in batch) - this is done via a nightly job, so does not need to run here
		/*if (membershipBillingsMap.size() > 0) {
			Database.executeBatch(new CalculateMembershipBillingTotals(membershipBillingsMap.keySet()), 1);
		}*/
		
		return 'There were ' + membershipBillingLines.size() + ' Membership Billing Line record(s) created/updated.';
	}
	
	// set the Membership Billing Id on the Membership Billing Infos; insert the Membership Billing records that don't exist
	private static void populateMembershipBillingInfoMBIds() {
		List<MembershipBilling__c> membershipBillingsToInsert = new List<MembershipBilling__c>();
		
		// get membership billing records for the years being billed
		membershipBillingsMap = new Map<Id,MembershipBilling__c>([SELECT Id, Year__c, MembershipType__c FROM MembershipBilling__c WHERE Year__c IN :years AND MembershipType__c IN :membershipTypes]);
		createMembershipBillingMap();
		
		for (MembershipBillingInfo mbi : memberBillingInfosList()) {
			if (mbi.membershipBilling == null) {
				Boolean isFound = false;
				
				String mapKey = mbi.year + '' + mbi.membershipType;
				if (membershipBillingsStringMap.get(mapKey) != null) {
					mbi.membershipBilling = membershipBillingsStringMap.get(mapKey).Id;
					isFound = true;
				}
				
				if (!isFound) {
					MembershipBilling__c memberBilling = new MembershipBilling__c(Year__c = mbi.year, MembershipType__c = mbi.membershipType);
					membershipBillingsToInsert.add(memberBilling);
					membershipBillingsStringMap.put(mapKey,memberBilling);
				}
			}
		}
		
		// insert membership billings, if necessary, and then loop through the Membership Billing Infos again
		if (membershipBillingsToInsert.size() > 0) {
			insert membershipBillingsToInsert;
			populateMembershipBillingInfoMBIds();
		}
	}
	
	// create map of membership billing records with a map key using the year and membership type
	private static void createMembershipBillingMap() {
		membershipBillingsStringMap = new Map<String,MembershipBilling__c>();
		for (MembershipBilling__c membershipBilling : membershipBillingsMap.values()) {
			membershipBillingsStringMap.put(membershipBilling.Year__c + '' + membershipBilling.MembershipType__c, membershipBilling);
		}
	}
	
	// get existing membership billing lines and create map to membership billing records
	private static void createMBLsToMembershipBillingsMap() {
		List<MembershipBillingLine__c> MBLs = [SELECT Id, 
			Account__c, 
			MembershipBilling__c, 
			MembershipOrderItemLine__c,
			DonationOrderItemLine__c,
			AmountBilled__c 
			FROM MembershipBillingLine__c 
			WHERE MembershipBilling__c IN :membershipBillingsMap.values()
				AND Account__c IN :memberBillingInfos.keySet()];
				
		// map MBLs to membership billing records
		membershipBillingMBLs = NU.CollectionUtil.groupSObjectsByField(MBLs,'MembershipBilling__c');
	}
	
	// determine if membership billing line records already exist or not for the accounts being billed
	// create the ones that don't and update the ones that do
	private static void upsertMembershipBillingLines() {
		membershipBillingLines = new List<MembershipBillingLine__c>();
		
		// loop through each Membership Billing Info
		for (MembershipBillingInfo mbi : memberBillingInfosList()) {
			
			MembershipBilling__c memberBilling = membershipBillingsMap.get(mbi.membershipBilling);
			if (memberBilling != null) {
				// get the existing MBLs
				List<MembershipBillingLine__c> existingMBLs = new List<MembershipBillingLine__c>();
				if (membershipBillingMBLs.get(memberBilling.Id) != null && membershipBillingMBLs.get(memberBilling.Id).size() > 0) {
					existingMBLs = membershipBillingMBLs.get(memberBilling.Id);
				}
				
				Id membershipOIL = null;
				Id donationOIL = null;
				Decimal membershipPrice = null;
				if (mbi.membershipPrice != null) {
					membershipPrice = mbi.membershipPrice;
				}
				membershipOIL = mbi.membershipOrderItemLine;
				donationOIL = mbi.donationOrderItemLine;
					
				// loop through existing MBLs - is the account already billed?
				Boolean isFound = checkExistingMBLs(mbi.accountId, existingMBLs, membershipPrice, membershipOIL, donationOIL);
					
				// the account is not already billed for the quarter, so create a new MSL
				if (!isFound) {
					membershipBillingLines.add(new MembershipBillingLine__c(Account__c = mbi.accountId, MembershipBilling__c = memberBilling.Id, AmountBilled__c = membershipPrice, BillDate__c = billDate, MembershipOrderItemLine__c = membershipOIL, DonationOrderItemLine__c = donationOIL));
				}
			}
		}
		
		// insert/update membership billing lines, if necessary
		if (membershipBillingLines.size() > 0) {
			upsert membershipBillingLines;
		}
	}
	
	// check to see if the membership billing line for the account already exists, and update pricing if necessary
	private static Boolean checkExistingMBLs(Id accountId, List<MembershipBillingLine__c> existingMBLs, Decimal membershipPrice, Id membershipOIL, Id donationOIL) {
		Boolean isFound = false;
		for (MembershipBillingLine__c msl : existingMBLs) {
			if (msl.Account__c == accountId) {
				// is the amount billed changing?
				if ((membershipOIL == null && donationOIL == null && membershipPrice > 0 && membershipPrice != msl.AmountBilled__c) ||
					((membershipOIL != null || donationOIL != null) && msl.AmountBilled__c == 0)) {
					msl.AmountBilled__c = membershipPrice;
					msl.BillDate__c = billDate;
				}
				
				if (membershipOIL != null) msl.MembershipOrderItemLine__c = membershipOIL;
				if (donationOIL != null) msl.DonationOrderItemLine__c = donationOIL;
						
				membershipBillingLines.add(msl);
					
				isFound = true;
				break;
			}
		}
		return isFound;
	}
	
	/*
	* Main functionality for updating Membership Statisic Line order item line lookup fields
	*/
	private static List<Id> cancelledMemberships = new List<Id>();
	private static List<Id> cancelledDonations = new List<Id>();
	
	public static void updateMembershipBillingLinesWithOILs(List<NU__OrderItemLine__c> orderItemLines) {
		memberBillingInfos = new Map<Id,List<MembershipBillingInfo>>();
		memberBillingInfosList = new List<MembershipBillingInfo>();
		
		// loop through all order item lines
		for (NU__OrderItemLine__c oil : orderItemLines) {	
			// is it a membership order item line?
			if (oil.NU__Membership__c != NULL && oil.NU__Donation__c == NULL) {
				Account account = new Account(Id = oil.NU__Membership__r.NU__Account__c);
				NU__Membership__c membership = new NU__Membership__c(Id = oil.NU__Membership__c, 
					NU__StartDate__c = oil.NU__Membership__r.NU__StartDate__c, 
					NU__MembershipType__c = oil.NU__Membership__r.NU__MembershipType__c);
				
				evaluateOrderItemLine(oil, account.Id, membership, TRUE);
			}
			
			// is it a donation order item line? (should be)
			else if (oil.NU__Donation__c != NULL) {
				Account account = new Account(Id = oil.NU__Donation__r.NU__Membership__r.NU__Account__c);
				NU__Membership__c membership = new NU__Membership__c(Id = oil.NU__Donation__r.NU__Membership__c, 
					NU__StartDate__c = oil.NU__Donation__r.NU__Membership__r.NU__StartDate__c, 
					NU__MembershipType__c = oil.NU__Donation__r.NU__Membership__r.NU__MembershipType__c);
				
				evaluateOrderItemLine(oil, account.Id, membership, FALSE);
			}
		}
		
		// clear the lookups for cancelled memberships and donation order item lines
		clearLookups();
		
		// "bill" the accounts - this will create MS/MSL records, if necessary, as well as set the membership and donation order item line lookups
		if (memberBillingInfos.size() > 0) {
			Bill();
		}
	}
	
	// evaluate the order item line passed in
	private static void evaluateOrderItemLine(NU__OrderItemLine__c oil, Id accountId, NU__Membership__c membership, Boolean isMembershipOIL) {
		// if it is active, populate the billing map to "bill" the account if they have not already been billed
		if (oil.NU__Status__c == Constant.ORDER_ITEM_LINE_STATUS_ACTIVE) {
			
			String year = String.valueOf(membership.NU__StartDate__c.year());
			List<MembershipBillingInfo> mbis = new List<MembershipBillingInfo>();
			
			// get membership billing info, if it already exists, for the given membership type and year
		   	MembershipBillingInfo memberBillingInfo = new MembershipBillingInfo();
		   	if (memberBillingInfos.get(accountId) != null) {
		   		mbis = memberBillingInfos.get(accountId);
		   		for (MembershipBillingInfo mbi : mbis) {
		   			if (mbi.year == year && mbi.membershipType == membership.NU__MembershipType__c) {
		   				memberBillingInfo = mbi;
		   				break;
		   			}
		   		}
		   	}
			    	
		   	memberBillingInfo.accountId = accountId;
		   	if (isMembershipOIL) {
		   		// set the membership price and membership oil
		   		memberBillingInfo.membershipPrice = oil.NU__TotalPrice__c;
		   		memberBillingInfo.membershipOrderItemLine = oil.Id;
		   	}
		   	else {
		   		// set the donation oil
		   		memberBillingInfo.donationOrderItemLine = oil.Id;
		   	}
		   	
		   	memberBillingInfo.year = year;
		   	memberBillingInfo.membershipType = membership.NU__MembershipType__c;
		   	mbis.add(memberBillingInfo);
		   	memberBillingInfos.put(accountId,mbis);
		}
				
		// if it is inactive, store the order item line Id in a list to clear the lookups in the MSL records later
		else {
			if (isMembershipOIL) cancelledMemberships.add(oil.Id);
			else cancelledDonations.add(oil.Id);
		}
	}
	
	// clear the lookups on the membership billing lines
	private static void clearLookups() {
		List<MembershipBillingLine__c> membershipMBLs = new List<MembershipBillingLine__c>();
		List<MembershipBillingLine__c> donationMBLs = new List<MembershipBillingLine__c>();
		
		Set<Id> mbToUpdate = new Set<Id>();
		
		// get the membership billing line records where we need to clear out the membership order item line
		if (cancelledMemberships.size() > 0) {
			membershipMBLs = [SELECT Id, MembershipBilling__c FROM MembershipBillingLine__c WHERE MembershipOrderItemLine__c IN :cancelledMemberships];
		}
		
		// clear the membership order item line lookups
		for (MembershipBillingLine__c msl : membershipMBLs) {
			msl.MembershipOrderItemLine__c = null;
			mbToUpdate.add(msl.MembershipBilling__c);
		}
		update membershipMBLs;
		
		// get the membership billing line records where we need to clear out the donation order item line
		if (cancelledDonations.size() > 0) {
			donationMBLs = [SELECT Id, MembershipBilling__c FROM MembershipBillingLine__c WHERE DonationOrderItemLine__c IN :cancelledDonations];
		}
		
		// clear the donation order item line lookups
		for (MembershipBillingLine__c msl : donationMBLs) {
			msl.DonationOrderItemLine__c = null;
			mbToUpdate.add(msl.MembershipBilling__c);
		}
		update donationMBLs;
		
		// recalculate the totals on the membership billing records that were updated (in batch) - this is done via a nightly job, so does not need to run here
		/*if (mbToUpdate.size() > 0) {
			Database.executeBatch(new CalculateMembershipBillingTotals(mbToUpdate), 1);
		}*/
	}
}