<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>NU Batches</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>AR__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>Revenue__c - TotalCashValue__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Difference between Total Cash Value and Revenue, updated on page load</inlineHelpText>
        <label>AR Impact</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CartCount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Count of how many incomplete orders are connected to this batch</inlineHelpText>
        <label>Cart Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DifferenceCashValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ExpectedTotalCashValue__c  -  TotalCashValue__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Difference between Total Cash Value and Expected Total Cash Value, updated on page load</inlineHelpText>
        <label>Difference Cash Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>DisplayName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TEXT(YEAR(TransactionDate__c)) &amp;
LPAD(TEXT(MONTH(TransactionDate__c)), 2, &apos;0&apos;) &amp;
LPAD(TEXT(DAY(TransactionDate__c)), 2, &apos;0&apos;) &amp;
&apos;-&apos; &amp;
Title__c &amp;
&apos;-&apos; &amp;
TEXT(VALUE(RIGHT(Name, LEN(Name) - FIND(Name, &apos; &apos;))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Batch date + Title + Batch Number</inlineHelpText>
        <label>Display Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Entity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Entity</label>
        <referenceTo>Entity__c</referenceTo>
        <relationshipLabel>Batches</relationshipLabel>
        <relationshipName>Batches</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExpectedTotalCashValue__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Manually set by accounting</inlineHelpText>
        <label>Expected Total Cash Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExportedOn__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Date batch status was Exported</inlineHelpText>
        <label>Exported On</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Health__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF (DifferenceCashValue__c != 0 || CartCount__c != 0,
IMAGE(&apos;/resource/1332258381000/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusError&apos;, &apos;Error&apos;),
IMAGE(&apos;/resource/1332258381000/&apos; + $Setup.Namespace__c.Prefix__c + &apos;StatusOk&apos;, &apos;Ok&apos;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Monitors open carts in batch and difference between Expected Total Cash Value and Total Cash Value</inlineHelpText>
        <label>Health</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PendingOn__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Date batch status was last set to Pending</inlineHelpText>
        <label>Pending On</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>PostedOn__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Date batch was Posted</inlineHelpText>
        <label>Posted On</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Revenue__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all Income transactions in batch, updated each time a new transaction is added</inlineHelpText>
        <label>Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Source</label>
        <picklist>
            <picklistValues>
                <fullName>Salesforce</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Self Service</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>-Open: Open for new orders and adjustments.
-Pending: Ready to be posted.  Cannot add new orders or make adjustments
-Posted: NO new data entry or adjustments allowed.
-Exported: Exported to GL</inlineHelpText>
        <label>Batch Status</label>
        <picklist>
            <picklistValues>
                <fullName>Open</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Pending</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Posted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Exported</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Appended to Batch Display Name field</inlineHelpText>
        <label>Title</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalCashValue__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all Payment and Inter-Entity transactions in batch, updated each time a new transaction is added</inlineHelpText>
        <label>Total Cash Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TransactionDate__c</fullName>
        <defaultValue>Today()</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Transaction Date</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Batch</label>
    <listViews>
        <fullName>AllBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>CartCount__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Batches</label>
    </listViews>
    <listViews>
        <fullName>AllExportedBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>ExportedOn__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Exported</value>
        </filters>
        <label>All Exported Batches</label>
    </listViews>
    <listViews>
        <fullName>AllOpenBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>CartCount__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </filters>
        <label>All Open Batches</label>
    </listViews>
    <listViews>
        <fullName>AllPendingBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>PendingOn__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>DisplayName__c</columns>
        <columns>CartCount__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </filters>
        <label>All Pending Batches</label>
    </listViews>
    <listViews>
        <fullName>AllPostedBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>PostedOn__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Posted</value>
        </filters>
        <label>All Posted Batches</label>
    </listViews>
    <listViews>
        <fullName>LastWeeksBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TransactionDate__c</field>
            <operation>equals</operation>
            <value>LAST_WEEK</value>
        </filters>
        <label>Batches - Last Week</label>
    </listViews>
    <listViews>
        <fullName>ThisWeeksBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Source__c</columns>
        <columns>TransactionDate__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>CartCount__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TransactionDate__c</field>
            <operation>equals</operation>
            <value>THIS_WEEK</value>
        </filters>
        <label>Batches - This Week</label>
    </listViews>
    <listViews>
        <fullName>TodaysBatches</fullName>
        <columns>NAME</columns>
        <columns>Health__c</columns>
        <columns>Status__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>TransactionDate__c</columns>
        <columns>ExpectedTotalCashValue__c</columns>
        <columns>TotalCashValue__c</columns>
        <columns>DifferenceCashValue__c</columns>
        <columns>CartCount__c</columns>
        <columns>DisplayName__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>TransactionDate__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>Batches - Today</label>
    </listViews>
    <nameField>
        <displayFormat>{0000000}</displayFormat>
        <label>Batch Id</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Batches</pluralLabel>
    <recordTypeTrackHistory>true</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Automatic</fullName>
        <active>true</active>
        <description>A batch that is automatically created by the system to capture transactions from a single day</description>
        <label>Automatic</label>
        <picklistValues>
            <picklist>Source__c</picklist>
            <values>
                <fullName>Salesforce</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Self Service</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Open</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Manual</fullName>
        <active>true</active>
        <description>A batch that is manually created by accounting to capture transactions for a single purpose</description>
        <label>Manual</label>
        <picklistValues>
            <picklist>Source__c</picklist>
            <values>
                <fullName>Salesforce</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Open</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <customTabListAdditionalFields>Health__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Title__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TransactionDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ExpectedTotalCashValue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalCashValue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>DifferenceCashValue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>AR__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <listViewButtons>Export</listViewButtons>
        <lookupDialogsAdditionalFields>Health__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RECORDTYPE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Source__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TransactionDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ExpectedTotalCashValue__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalCashValue__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>DifferenceCashValue__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AR__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RECORDTYPE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Health__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Title__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TransactionDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ExpectedTotalCashValue__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TotalCashValue__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>DifferenceCashValue__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>AR__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>RECORDTYPE</searchFilterFields>
        <searchFilterFields>Health__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Title__c</searchFilterFields>
        <searchFilterFields>TransactionDate__c</searchFilterFields>
        <searchFilterFields>ExpectedTotalCashValue__c</searchFilterFields>
        <searchFilterFields>TotalCashValue__c</searchFilterFields>
        <searchFilterFields>DifferenceCashValue__c</searchFilterFields>
        <searchFilterFields>AR__c</searchFilterFields>
        <searchResultsAdditionalFields>Health__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RECORDTYPE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Source__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TransactionDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ExpectedTotalCashValue__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TotalCashValue__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>DifferenceCashValue__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>AR__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Expected_Total_Must_Match_Total_Cash_Val</fullName>
        <active>true</active>
        <description>If Expected Total does not match total cash value, cannot set status to ready or post.</description>
        <errorConditionFormula>DifferenceCashValue__c &lt;&gt; 0 &amp;&amp; 
TEXT(Status__c) = &quot;Pending&quot;</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>Cannot set status to Pending until expected total value matches total value.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Export_Only_Posted_Batches</fullName>
        <active>true</active>
        <errorConditionFormula>AND( IsChanged(Status__c),
          Text(Status__c) = &apos;Exported&apos;,
          Text(PriorValue(Status__c)) &lt;&gt; &apos;Posted&apos;)</errorConditionFormula>
        <errorMessage>Only posted batches can be exported.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Exported_Cannot_Be_Reopened</fullName>
        <active>true</active>
        <description>An exported batch can&apos;t be reopened.</description>
        <errorConditionFormula>AND(ISChanged( Status__c ),
    Text( PriorValue( Status__c ) ) = &apos;Exported&apos;,
    OR( Text( Status__c ) = &apos;Open&apos;,
        Text( Status__c ) = &apos;Pending&apos;,
        Text( Status__c ) = &apos;Posted&apos;
    )
)</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>An exported batch can&apos;t be reopened.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Exported_On_Unchangeable_After_Export</fullName>
        <active>true</active>
        <errorConditionFormula>AND(IsChanged( ExportedOn__c),
          IsBlank(PriorValue( ExportedOn__c )),
            OR(IsChanged( Status__c )=False,
                    AND(Text( Status__c )&lt;&gt;&apos;Exported&apos;,
                              Text ( PriorValue( Status__c ) ) = &apos;Posted&apos;
                    )
            )
)</errorConditionFormula>
        <errorDisplayField>ExportedOn__c</errorDisplayField>
        <errorMessage>Exported On can only be changed via exporting and can not be changed directly.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Manual_Batch_Title</fullName>
        <active>true</active>
        <errorConditionFormula>$RecordType.Name  == &apos;Manual&apos; &amp;&amp; Title__c == null</errorConditionFormula>
        <errorDisplayField>Title__c</errorDisplayField>
        <errorMessage>Please enter a title for manual batches.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>New_Batches_Can_Only_Be_Open</fullName>
        <active>true</active>
        <errorConditionFormula>AND( ISNEW(),
           Text(  Status__c  ) &lt;&gt; &apos;Open&apos;)</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>New batches must be set to open.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Posted_CANT_BE_REOPENED</fullName>
        <active>true</active>
        <description>A posted batch can&apos;t be reopened.</description>
        <errorConditionFormula>AND(ISChanged( Status__c ),
    Text( PriorValue( Status__c ) ) = &apos;Posted&apos;,
    OR( Text( Status__c ) = &apos;Open&apos;,
        Text( Status__c ) = &apos;Pending&apos;
    )
)</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>A posted batch can&apos;t be reopened.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Posted_On_Unchangeable_After_Post</fullName>
        <active>true</active>
        <errorConditionFormula>AND(IsChanged( PostedOn__c ),
          IsBlank(PriorValue( PostedOn__c )),
            OR(IsChanged( Status__c )=False,
                    AND(Text( Status__c )&lt;&gt;&apos;Posted&apos;,
                              Text ( PriorValue( Status__c ) ) = &apos;Open&apos;,
                              Text ( PriorValue( Status__c ) ) = &apos;Pending&apos;
                    )
            )
)</errorConditionFormula>
        <errorDisplayField>PostedOn__c</errorDisplayField>
        <errorMessage>Posted On can only be changed via posting and can not be changed directly.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Transaction_Date_Editing_Forbidden</fullName>
        <active>true</active>
        <description>The transaction date can&apos;t be edited.</description>
        <errorConditionFormula>ISCHANGED(TransactionDate__c)</errorConditionFormula>
        <errorDisplayField>TransactionDate__c</errorDisplayField>
        <errorMessage>The transaction date can&apos;t be edited.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Export</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Export</masterLabel>
        <openType>sidebar</openType>
        <page>BatchExport</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>Post</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>Post</masterLabel>
        <openType>newWindow</openType>
        <page>BatchPost</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
    <webLinks>
        <fullName>ViewCarts</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>View Carts</masterLabel>
        <openType>sidebar</openType>
        <page>BatchCarts</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>ViewReport</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>View Report</masterLabel>
        <openType>newWindow</openType>
        <page>BatchReport</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
</CustomObject>
