global class MembershipTypeBatchUpdate implements Database.Batchable<sObject>, Database.Stateful {
	
	public static Integer batchSize = 200;
	
	private Set<Id> membershipTypeIds;
	private Map<Id,NU__MembershipType__c> membershipTypes;
	
	private static List<NU__Membership__c> updatedMemberships;
	
	public MembershipTypeBatchUpdate(Set<Id> updatedMembershipTypes) {
		if (updatedMembershipTypes != null && updatedMembershipTypes.size() > 0) {
			membershipTypeIds = updatedMembershipTypes;
			
			membershipTypes = new Map<Id,NU__MembershipType__c>([SELECT Id, Name, NU__GracePeriod__c FROM NU__MembershipType__c WHERE Id IN :membershipTypeIds]);
		}
	}

	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, NU__MembershipType__c, MembershipTypeName__c, MembershipTypeGracePeriod__c FROM NU__Membership__c WHERE NU__MembershipType__c IN :membershipTypeIds');
	}

	global void execute(Database.BatchableContext context, List<NU__Membership__c> membershipsToUpdate) {
		if (membershipTypes != null && membershipTypes.size() > 0) {
			updatedMemberships = new List<NU__Membership__c>();
			
			for (NU__Membership__c membership : membershipsToUpdate) {
				updateMembershipFields(membership);
			}
			
			if (updatedMemberships.size() > 0) {
				update updatedMemberships;
			}
		}
	}

	global void finish(Database.BatchableContext context) {
		//
	}
	
	private void updateMembershipFields(NU__Membership__c membership) {
		if (membershipTypes.get(membership.NU__MembershipType__c) != null) {		
			NU__MembershipType__c membershipType = membershipTypes.get(membership.NU__MembershipType__c);
			
			NU__Membership__c updatedMembership = new NU__Membership__c(Id = membership.Id, MembershipTypeName__c = membershipType.Name, MembershipTypeGracePeriod__c = membershipType.NU__GracePeriod__c);
			updatedMemberships.add(updatedMembership);
		}
	}
}