global class BatchSetEstimatedDues implements Database.Batchable<SObject> {
	
	public static void ScheduleAtMidnightDaily(){
		system.schedule('BatchSetEstimatedDues', '0 0 00 * * ?', new ScheduleBatchSetEstimatedDues());
	}
	
	global BatchSetEstimatedDues(){
	}
	
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT
                id,
                LeadingAge_ID__c,
                Name,
                NU__RecordTypeName__c,
                Provider_Join_On__c,
                Provider_Lapsed_On__c,
                Multi_Site_Corporate__c,
                NU__PrimaryAffiliation__r.RecordTypeId,
                NU__PrimaryAffiliation__r.RecordType.Name,
                NU__PrimaryAffiliation__r.NU__RecordTypeName__c,
                NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c,
                NU__PrimaryAffiliation__r.Multi_Site_Corporate__c,
                Primary_Affiliation_Account_Name__c,
                Revenue_Year_Submitted__c, 
                Program_Service_Revenue__c,
                Dues_Price__c,
                Dues_Price_Override__c,
                Multi_Site_Dues_Price__c,
                Multi_Site_Program_Service_Revenue__c,
                Multi_Site_Enabled__c,
                Provider_Member__c,
                LeadingAge_Member_Company__c,
                OwnerId,
                Owner.Name,
                State_Partner_ID__c
             FROM Account
             WHERE ((RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER and (NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c = false))
             OR (RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE and Multi_Site_Corporate__c = true))
             AND Provider_Member__c = 'Yes'
             ORDER BY name ASC]);
   }

   global void execute(Database.BatchableContext BC, List<SObject> batch){
   		List<Account> thisYearsDues = (List<Account>)batch; 
   	
        //prepare Custom Pricing Manager
        NU__MembershipType__c JointStateProviderMembershipType;
        Id JointStateProviderProductId;
        JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
        
        if (JointStateProviderMembershipType != null){
            JointStateProviderProductId = JointBillingUtil.getJointStateProviderProductId(JointStateProviderMembershipType);
        }
        
        JointBillingUtil.PopulateCustomPricingManagerCaches(thisYearsDues, JointStateProviderMembershipType.Id, JointStateProviderProductId);
        Date transactionDate = Date.Today();
        
        Map<Id,Account> stateBillingQtrs = new Map<Id,Account>([SELECT Id, Dues_Billing_Quarter__c FROM Account WHERE NU__RecordTypeName__c = :Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER]);
            
        List<Account> aUpdates = new List<Account>();
   		for (Account a : thisYearsDues) {
   			//Shortcut estimated dues calculations for Providers whose dues are paid by their MSO parent
   			if (a.Multi_Site_Enabled__c == true && a.NU__RecordTypeName__c == Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER) {
   				aUpdates.add(new Account(Id = a.Id, Estimated_Dues_Pro_Forma__c = 0));
   			}
   			else {
	   			Account bQtr = stateBillingQtrs.get(a.State_Partner_ID__c);
	   			if (bQtr != null && bQtr.Dues_Billing_Quarter__c != null){
			        Date startDate = calculateNextStartDateFromBillingQuarter(stateBillingQtrs.get(a.State_Partner_ID__c).Dues_Billing_Quarter__c);
			        Date endDate = calculateNextEndDate(startDate, (Integer) JointStateProviderMembershipType.NU__Term__c);
		   			
					Map<Id, Decimal> duesPricing = JointBillingUtil.getProviderPricing(transactionDate, a.id, JointStateProviderProductId, JointStateProviderMembershipType.Id, startDate, endDate, a.Provider_Join_On__c, true);
					aUpdates.add(new Account(Id = a.Id, Estimated_Dues_Pro_Forma__c = duesPricing.get(JointStateProviderProductId)));
	   			}
	   			else{
	   				aUpdates.add(new Account(Id = a.Id, Estimated_Dues_Pro_Forma__c = 0));
	   			}
   			}
   		}
   		
   		if (!aUpdates.isEmpty()){
   			update aUpdates;
   		}
   }

   global void finish(Database.BatchableContext BC){

   }
   
   private Integer ThisYearCalculated(Decimal SelectedStateBillingQuarter){
	    Integer y = Date.today().year();
	    Integer m = Date.today().month();
	    if (SelectedStateBillingQuarter != null) {
	        Decimal SelectedStateBillingQuarterStartingMonth = ((SelectedStateBillingQuarter - 1) * 3) + 1;
	        
	        if (SelectedStateBillingQuarterStartingMonth > m)
	           y = y - 1;
	        return y;
	    }
	    return 1;
   }
   
    private Date calculateNextStartDateFromBillingQuarter(Decimal billingQuarter){
        Date todaysDate = Date.Today();
        
        Integer nextYear = ThisYearCalculated(billingQuarter) + 1;
        
        Date nextJanuary = Date.NewInstance(nextYear, 1, 1);
        Date nextStartDate = nextJanuary;
        
        if (billingQuarter == 2){
            nextStartDate = Date.newInstance(nextYear, 4, 1);
        }
        else if (billingQuarter == 3){
            nextStartDate = Date.newInstance(nextYear, 7, 1);
        }
        else if (billingQuarter == 4){
            nextStartDate = Date.newInstance(nextYear, 10, 1);
        }
        
        return nextStartDate;
    }
    
    private Date calculateNextEndDate(Date startDate, Integer termInMonths){
        return startDate.addMonths(termInMonths).addDays(-1);
    }
}