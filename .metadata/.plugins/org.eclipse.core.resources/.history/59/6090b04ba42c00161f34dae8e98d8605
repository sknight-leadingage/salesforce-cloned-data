/*
   Without Sharing is needed so that a state user can still
   access the "State Partner" accounts even though they shouldn't
   have access to them. This allows it to be accessed programmatically
   where necessary.
*/
public without sharing class UserQuerier {
	
	public static void clearCurrentUserCache(){
		currentUserWithStateInfo = null;
	}
    
    private Static User currentUserWithStateInfo = null;
    public static User getCurrentUserWithStateInformation(){
        if (currentUserWithStateInfo == null){
        
            currentUserWithStateInfo =
                   [select id,
                           name,
                           State,
                           Contact.Account.State_Partner_Id__c,
                           ContactId,
                           IsPortalEnabled,
                           UserRoleId,
                           UserRole.Name
                      from User
                     where id = :UserInfo.getUserId()];
        }

        return currentUserWithStateInfo;
    }
    
    private static List<User> generalStatePartnerAndLeadingAgeIntegrationUsersPriv = null;
    public static List<User> getGeneralStatePartnerAndLeadingAgeIntegrationUsers(){
        if (generalStatePartnerAndLeadingAgeIntegrationUsersPriv == null){
            String generalLikeName = '%' + Constant.GENERAL + '%';
            
            
            generalStatePartnerAndLeadingAgeIntegrationUsersPriv = 
            [select id,
                   name,
                   Profile.Name,
                   CompanyName
              from User
             where (CompanyName = :Constant.LEADINGAGE_ENTITY_NAME and
                    Profile.Name = :Constant.INTEGRATION_USER_PROFILE_NAME)
                   
                   OR 
                   
                   (Name like :generalLikeName and
                    Profile.Name = :Constant.STATE_ASSOCIATION_PARTNER_USER_PROFILE_NAME)];
        }
        
        return generalStatePartnerAndLeadingAgeIntegrationUsersPriv;
    }
    
    private static User generalStatePartnerPriv = null;
    public static User GeneralStatePartnerUser{
        get{
            if (generalStatePartnerPriv == null){
                List<User> users = getGeneralStatePartnerAndLeadingAgeIntegrationUsers();
                
                for (User u : users){
                    if (u.Name.containsIgnoreCase(Constant.GENERAL) &&
                        u.Profile.Name == Constant.STATE_ASSOCIATION_PARTNER_USER_PROFILE_NAME){
                        generalStatePartnerPriv = u;
                        break;
                    }
                }
            }
            
            return generalStatePartnerPriv;
        }
    }
    
    private static User leadingAgeIntegrationUserPriv = null;
    public static User LeadingAgeIntegrationUser{
        get{
            if (leadingAgeIntegrationUserPriv == null){
                List<User> users = getGeneralStatePartnerAndLeadingAgeIntegrationUsers();
                
                for (User u : users){
                    if (u.CompanyName == Constant.LEADINGAGE_ENTITY_NAME &&
                        u.Profile.Name == Constant.INTEGRATION_USER_PROFILE_NAME){
                        leadingAgeIntegrationUserPriv = u;
                        break;
                    }
                }
            }
            
            return leadingAgeIntegrationUserPriv;
        }
    }
    
    private static Account userStatePartnerAccountPriv = null;
    private static Boolean alreadyQueriedUserStatePartnerAccount = false;
    
    public static User UserState{
    	 get{
           
               User currentUserInfo = getCurrentUserWithStateInformation();
               User ReturnedUserInfo = new User();
               
               if (currentUserInfo.UserRoleId != null){
                   String statePartnerName = 'LeadingAge ' + currentUserInfo.UserRole.Name;
                   
                   
                   List<User> UserStateInt = 
                   [select id,
                           name,
                           State
                      from User
                     where Id = :currentUserInfo.Id];
                   
                   if (UserStateInt.size() > 0){
                     ReturnedUserInfo = UserStateInt[0];
                   }
               }
            
            return ReturnedUserInfo;
               
           }
           
           
       }
    
    
    
    public static Account UserStatePartnerAccount{
       get{
           if (alreadyQueriedUserStatePartnerAccount == false){
               User currentUserInfo = getCurrentUserWithStateInformation();
               
               if (currentUserInfo.UserRoleId != null){
                   String statePartnerName = 'LeadingAge ' + currentUserInfo.UserRole.Name;
                   
                   
                   List<Account> statePartnerAccounts = 
                   [select id,
                           name,
                           State_Partner_Uses_Nimble_AMS__c
                      from Account
                     where name = :statePartnerName
                     and RecordType.Id = :Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID];
                   
                   if (statePartnerAccounts.size() > 0){
                     userStatePartnerAccountPriv = statePartnerAccounts[0];
                   }
               }
            
            
               alreadyQueriedUserStatePartnerAccount = true;
           }
           
           return userStatePartnerAccountPriv;
       }
    }
    
    public static Id UserStatePartnerAccountId {
        get {
            if (UserStatePartnerAccount != null){
                return UserStatePartnerAccount.Id;
            }
            
            return null;
        }
    }

    public static Boolean UserStatePartnerAccountUsesNimbleAMS {
        get {
            if (UserStatePartnerAccount != null){
                return UserStatePartnerAccount.State_Partner_Uses_Nimble_AMS__c;
            }
            
            return false;
        }
    }

}