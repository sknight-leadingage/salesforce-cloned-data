/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAccountStatusFlowdownTrigger {

	static void assertStatusFlowdown(String expectedAccountStatus, Set<Id> accountFlowedDownIds){
		system.assert(expectedAccountStatus != null, 'The expected account status is null');
		system.assert(NU.CollectionUtil.setHasElements(accountFlowedDownIds), 'The account floweddown ids is empty or null');
		
		List<Account> flowedDownAccounts = [select id, NU__Status__c from Account where id in :accountFlowedDownIds];
		
		for (Account flowedDownAccount : flowedDownAccounts){
			system.assertEquals(expectedAccountStatus, flowedDownAccount.NU__Status__c);
		}
	}
	
	static void updateAccountStatus(Account accountToUpdate, String newStatus){
		accountToUpdate.NU__Status__c = newStatus;
        update accountToUpdate;
	}

    static testMethod void inactiveAccountStatusFlowdownOneLevelTest() {
        Account activeCompanyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account activeIndividualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(activeIndividualAccount.Id, activeCompanyAccount.Id, true);
        
        updateAccountStatus(activeCompanyAccount, Constant.ACCOUNT_STATUS_INACTIVE);
        
        assertStatusFlowdown(activeCompanyAccount.NU__Status__c, new Set<Id>{ activeIndividualAccount.Id });
    }
    
    static testmethod void inactiveAccountStatusFlowdownTwoLevelsTest(){
    	Account activeMultiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount();
    	Account activeCompanyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account activeIndividualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(activeCompanyAccount.Id, activeMultiSiteAccount.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(activeIndividualAccount.Id, activeCompanyAccount.Id, true);
        
        updateAccountStatus(activeMultiSiteAccount, Constant.ACCOUNT_STATUS_INACTIVE);
        
        assertStatusFlowdown(activeMultiSiteAccount.NU__Status__c, new Set<Id>{ activeIndividualAccount.Id, activeCompanyAccount.Id });
    }
    
    static testmethod void reactivatedAccountStatusFlowdownOneLevelTest(){
    	Account activeCompanyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account activeIndividualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(activeIndividualAccount.Id, activeCompanyAccount.Id, true);
        
        updateAccountStatus(activeCompanyAccount, Constant.ACCOUNT_STATUS_INACTIVE);
        
        assertStatusFlowdown(activeCompanyAccount.NU__Status__c, new Set<Id>{ activeIndividualAccount.Id });
        
        updateAccountStatus(activeCompanyAccount, Constant.ACCOUNT_STATUS_ACTIVE);
        
        assertStatusFlowdown(activeCompanyAccount.NU__Status__c, new Set<Id>{ activeIndividualAccount.Id });
    }
    
    static testmethod void reactivatedAccountStatusFlowdownTwoLevelTest(){
    	Account activeMultiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount();
    	Account activeCompanyAccount = DataFactoryAccountExt.insertCompanyAccount();
        Account activeIndividualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        NU.DataFactoryAffiliation.insertAffiliation(activeCompanyAccount.Id, activeMultiSiteAccount.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(activeIndividualAccount.Id, activeCompanyAccount.Id, true);
        
        updateAccountStatus(activeMultiSiteAccount, Constant.ACCOUNT_STATUS_INACTIVE);
        
        assertStatusFlowdown(activeMultiSiteAccount.NU__Status__c, new Set<Id>{ activeIndividualAccount.Id, activeCompanyAccount.Id });
        
        updateAccountStatus(activeMultiSiteAccount, Constant.ACCOUNT_STATUS_ACTIVE);
        
        assertStatusFlowdown(activeMultiSiteAccount.NU__Status__c, new Set<Id>{ activeIndividualAccount.Id, activeCompanyAccount.Id });
    }
    
}