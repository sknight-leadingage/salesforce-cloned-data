public class MultisiteProviderMembershipSetter {
	public static final String NULL_MULTI_SITE_ORG_ACCOUNT_ID = 'The multi site org account id is null.';
	public static final String MULTI_SITE_ORG_NOT_FOUND = 'The given multi site org account was not found.';
	public static final String ACCOUNT_IS_NOT_AN_MSO = 'The account is not a multi site organization.';
	public static final Integer NUMBER_PROVIDER_MEMBERS_NEEDED_FOR_MSO_PROVIDER_MEMBERSHIP = 2;

	private Account MultiSiteOrg { get; set; }

	public MultiSiteProviderMembershipSetter(Id multiSiteOrgAccountId){
		if (multiSiteOrgAccountId == null){
			throw new ApplicationException(NULL_MULTI_SITE_ORG_ACCOUNT_ID);
		}
		
		MultiSiteOrg = AccountQuerier.getMultiSiteOrgWithProviderMembers(multiSiteOrgAccountId);
		
		if (MultiSiteOrg == null){
			throw new ApplicationException(MULTI_SITE_ORG_NOT_FOUND);
		}
		
		if (MultiSiteOrg.RecordTypeId != Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
			throw new ApplicationException(ACCOUNT_IS_NOT_AN_MSO);
		}
	}
	
	public void Set(){
		Integer providerMemberCount = calculateMSOProviderMembersCount(MultiSiteOrg);
		Integer providerNonMemberCount = calculateMSOProviderNonMembersCount(MultiSiteOrg);
		Boolean updateNeeded = false;
		
		if (MultiSiteOrg.Multi_Site_Provider_Member_Count__c != providerMemberCount){
			MultiSiteOrg.Multi_Site_Provider_Member_Count__c = providerMemberCount;
			updateNeeded = true;
		}
		
		if (MultiSiteOrg.Multi_Site_Provider_Member_Count__c >= NUMBER_PROVIDER_MEMBERS_NEEDED_FOR_MSO_PROVIDER_MEMBERSHIP){
			MultiSiteOrg.Provider_Membership__c = getLatestProviderMembershipId(MultiSiteOrg);
			updateNeeded = true;
		}
		else if (msoInheritedProviderMembershipFromItsProviders(MultiSiteOrg) &&
		         isNotCorporateMSO(MultiSiteOrg)){
			MultiSiteOrg.Provider_Membership__c = null;
			updateNeeded = true;
		}
		
		if (MultiSiteOrg.Multi_Site_Provider_Non_Member_Count__c != providerNonMemberCount){
			MultiSiteOrg.Multi_Site_Provider_Non_Member_Count__c = providerNonMemberCount;
			updateNeeded = true;
		}
		
		if (updateNeeded){
			update MultiSiteOrg;
		}
	}
	
	public static Id getLatestProviderMembershipId(Account msoWithProviderMembers){
		for (Account msoProvider : msoWithProviderMembers.NU__Accounts1__r){
			if (msoProvider.Provider_Membership__r.NU__Account__c == msoProvider.Id){
				return msoProvider.Provider_Membership__c;
			}
		}
		
		return null;
	}
	
	private Integer calculateMSOProviderMembersCount(Account msoWithProviderMembers){
		Integer msoProviderMemberCount = 0;
		
		for (Account msoProvider : msoWithProviderMembers.NU__Accounts1__r){
			if (msoProvider.Provider_Membership__r.NU__Account__c == msoProvider.Id){
				++msoProviderMemberCount;
			}
		}
		
		return msoProviderMemberCount;
	}
	
	private Integer calculateMSOProviderNonMembersCount(Account msoWithProviderMembers){
		Integer msoProviderNonMemberCount = 0;
		
		for (Account msoProvider : msoWithProviderMembers.NU__Accounts1__r){
			if (msoProvider.Provider_Membership__r.NU__Account__c != msoProvider.Id && msoProvider.RecordTypeId == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID){
				++msoProviderNonMemberCount;
			}
		}
		
		return msoProviderNonMemberCount;
	}
	
	private Boolean msoInheritedProviderMembershipFromItsProviders(Account msoWithProviderMembers){
		if (msoWithProviderMembers.Provider_Membership__c != null){
			
			for (NU__Membership__c msoMembership : msoWithProviderMembers.NU__Memberships__r){
				if (msoMembership.Id == msoWithProviderMembers.Provider_Membership__c){
					return false;
				}
			}

		}
		
		return true;
	}
	
	private Boolean isNotCorporateMSO(Account msoWithProviderMembers){
		return msoWithProviderMembers != null && msoWithProviderMembers.Multi_Site_Corporate__c == false;
	}
}