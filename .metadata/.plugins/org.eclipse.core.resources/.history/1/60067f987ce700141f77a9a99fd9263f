<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>CodeTemplate__c</fullName>
        <deprecated>false</deprecated>
        <description>The code value given to generated coupon codes. You may use a formula if you wish.

Formulas: Embrace in brackets
Examples:
SAVE{000000} = SAVE123456, SAVE323423
{AAAaaa} = ABCabc, GDJahf
COUP-{*****} = COUP-0AaB1</description>
        <externalId>false</externalId>
        <inlineHelpText>The code value given to generated coupon codes. You may use a formula if you wish.

Formulas: Embrace in brackets
Examples:
SAVE{000000} = SAVE123456, SAVE323423
{AAAaaa} = ABCabc, GDJahf
COUP-{*****} = COUP-0AaB1</inlineHelpText>
        <label>Code Template</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CouponProduct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Specifies the coupon product to use when a valid code is entered for a user&apos;s order</description>
        <externalId>false</externalId>
        <inlineHelpText>Specifies the coupon product to use when a valid code is entered for a user&apos;s order</inlineHelpText>
        <label>Coupon Product</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>The product must be a coupon product.</errorMessage>
            <filterItems>
                <field>Product__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Coupon</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product__c</referenceTo>
        <relationshipName>CouponRules</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DaysValid__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If greater than 0, this is the number of days that the coupon code is valid. Supersedes the End Date field for coupon codes.</inlineHelpText>
        <label>Days Valid</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <description>Describes the coupon rule.</description>
        <externalId>false</externalId>
        <inlineHelpText>Describes the coupon rule. For example, &quot;These coupons will allow the individual to get 25% off all membership orders.&quot;</inlineHelpText>
        <label>Description</label>
        <length>1000</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>DiscountPercentage__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>What should the discount be, in percent? Either this field or Discount Rate must be filled in.</description>
        <externalId>false</externalId>
        <inlineHelpText>What should the discount be, in percent? Either this field or Discount Rate must be filled in.</inlineHelpText>
        <label>Discount Percentage</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>DiscountRate__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>What should the discount be, in dollars? Either this field or Discount Percentage must be filled in.</description>
        <externalId>false</externalId>
        <inlineHelpText>What should the discount be, in dollars? Either this field or Discount Percentage must be filled in.</inlineHelpText>
        <label>Discount Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EndDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If specified, this is when all coupon codes will expire. Also signals when coupon codes will no longer be automatically generated.</inlineHelpText>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EntityName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>CouponProduct__r.Entity__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Entity Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>MaximumDiscount__c</fullName>
        <deprecated>false</deprecated>
        <description>If specified, this is the maximum discount that can be applied to an order. This is only used for Discount Percentage coupons.</description>
        <externalId>false</externalId>
        <inlineHelpText>If specified, this is the maximum discount that can be applied to an order. This is only used for Discount Percentage coupons.</inlineHelpText>
        <label>Maximum Discount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>StartDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>If specified, signals when coupon codes will begin to be automatically generated.</inlineHelpText>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>StatusFlag__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(!ISBLANK(Status__c), IMAGE(
&quot;/resource/&quot; + $Setup.Namespace__c.Prefix__c + &quot;StatusIcons/&quot; +
CASE(Status__c,
&quot;Future&quot;, &quot;Future&quot;,
&quot;Active&quot;, &quot;Active&quot;,
&quot;Inactive&quot;, &quot;Inactive&quot;,
&quot;blank&quot;) + &quot;.png&quot;,
&quot;Status Flag&quot;),
&apos;&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Blue = Future
Green = Current
Gray = Expired</inlineHelpText>
        <label>Status Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(!ISBLANK(EndDate__c) &amp;&amp; EndDate__c &lt; TODAY(), &apos;Inactive&apos;, 
    IF(!ISBLANK(StartDate__c) &amp;&amp; StartDate__c &gt; TODAY(), &apos;Future&apos;, &apos;Active&apos;)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Coupon Rule</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CouponProduct__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Coupon Rules</label>
    </listViews>
    <listViews>
        <fullName>All_Flat_Rate_Coupon_Rules</fullName>
        <columns>NAME</columns>
        <columns>CouponProduct__c</columns>
        <columns>DiscountRate__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>DiscountRate__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </filters>
        <label>All Flat Rate Coupon Rules</label>
    </listViews>
    <listViews>
        <fullName>All_Percentage_Rate_Coupon_Rules</fullName>
        <columns>NAME</columns>
        <columns>CouponProduct__c</columns>
        <columns>DiscountPercentage__c</columns>
        <columns>StartDate__c</columns>
        <columns>EndDate__c</columns>
        <columns>Status__c</columns>
        <columns>StatusFlag__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>DiscountPercentage__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </filters>
        <label>All Percentage Rate Coupon Rules</label>
    </listViews>
    <nameField>
        <label>Coupon Rule Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Coupon Rules</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>EntityName__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CouponProduct__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StatusFlag__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>EntityName__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CouponProduct__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StatusFlag__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>EntityName__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CouponProduct__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StatusFlag__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>CouponProduct__c</searchFilterFields>
        <searchFilterFields>EntityName__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchResultsAdditionalFields>EntityName__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CouponProduct__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>StatusFlag__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>BothDiscountPercentageAndRateNotAllowed</fullName>
        <active>true</active>
        <description>You cannot specify both a Discount Percentage and a Discount Rate.</description>
        <errorConditionFormula>DiscountPercentage__c &gt; 0 &amp;&amp; DiscountRate__c &gt; 0</errorConditionFormula>
        <errorMessage>You cannot specify both a Discount Percentage and a Discount Rate.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CouponProductRequired</fullName>
        <active>true</active>
        <description>The coupon product is required.</description>
        <errorConditionFormula>ISBLANK(CouponProduct__c)</errorConditionFormula>
        <errorDisplayField>CouponProduct__c</errorDisplayField>
        <errorMessage>The coupon product is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>DaysValidCannotBeNegative</fullName>
        <active>true</active>
        <description>The days valid field cannot be negative.</description>
        <errorConditionFormula>DaysValid__c &lt; 0</errorConditionFormula>
        <errorDisplayField>DaysValid__c</errorDisplayField>
        <errorMessage>The days valid field cannot be negative.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>DiscountPercentageCannotBeGreaterThan100</fullName>
        <active>true</active>
        <description>The Discount Percentage cannot be greater than 100%.</description>
        <errorConditionFormula>DiscountPercentage__c &gt; 1</errorConditionFormula>
        <errorDisplayField>DiscountPercentage__c</errorDisplayField>
        <errorMessage>The Discount Percentage cannot be greater than 100%.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>DiscountPercentageOrRateIsRequired</fullName>
        <active>true</active>
        <description>Either the Discount Percentage or the Discount Rate must be greater than 0.</description>
        <errorConditionFormula>DiscountPercentage__c &lt;= 0 &amp;&amp; DiscountRate__c &lt;= 0</errorConditionFormula>
        <errorMessage>Either the Discount Percentage or the Discount Rate must be greater than 0.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>MaximumDiscountMustBeGreaterThanZero</fullName>
        <active>true</active>
        <description>If specified, the maximum discount must be greater than 0 for percentage-based coupon rules.</description>
        <errorConditionFormula>!ISBLANK(MaximumDiscount__c) &amp;&amp; DiscountPercentage__c &gt; 0 &amp;&amp; MaximumDiscount__c &lt;= 0</errorConditionFormula>
        <errorDisplayField>MaximumDiscount__c</errorDisplayField>
        <errorMessage>If specified, the maximum discount must be greater than 0 for percentage-based coupon rules.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>StartDateBeforeEndDate</fullName>
        <active>true</active>
        <description>The Start Date must be before the End Date.</description>
        <errorConditionFormula>AND (ISBLANK( StartDate__c ) = False,
ISBLANK( EndDate__c ) = False,
StartDate__c &gt; EndDate__c)</errorConditionFormula>
        <errorMessage>The Start Date must be before the End Date.</errorMessage>
    </validationRules>
</CustomObject>
