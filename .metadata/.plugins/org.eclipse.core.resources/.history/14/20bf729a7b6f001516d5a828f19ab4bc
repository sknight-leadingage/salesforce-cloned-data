global class JointBillingSetCalculatedDues implements Database.Batchable<SObject>, Schedulable {
    
    global JointBillingSetCalculatedDues(){
    }
    
    public static void ScheduleAt4amDaily(){
        System.schedule('Set Calculated Dues', '0 0 4 * * ?', new JointBillingSetCalculatedDues());
    }
    
    global void execute(SchedulableContext context) {
        Database.executeBatch(new JointBillingSetCalculatedDues(), 50);
    }
    
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([SELECT
                id,
                LeadingAge_ID__c,
                Name,
                NU__RecordTypeName__c,
                Provider_Join_On__c,
                Provider_Lapsed_On__c,
                Multi_Site_Corporate__c,
                NU__PrimaryAffiliation__r.RecordTypeId,
                NU__PrimaryAffiliation__r.RecordType.Name,
                NU__PrimaryAffiliation__r.NU__RecordTypeName__c,
                NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c,
                NU__PrimaryAffiliation__r.Multi_Site_Corporate__c,
                Primary_Affiliation_Account_Name__c,
                Revenue_Year_Submitted__c, 
                Program_Service_Revenue__c,
                Dues_Price__c,
                Dues_Price_Override__c,
                Multi_Site_Dues_Price__c,
                Multi_Site_Program_Service_Revenue__c,
                Multi_Site_Enabled__c,
                Provider_Member__c,
                Provider_Membership__c,
                Provider_Membership__r.NU__StartDate__c,
                Provider_Membership__r.NU__EndDate__c,
                LeadingAge_Member_Company__c,
                OwnerId,
                Owner.Name,
                State_Partner_ID__c
             FROM Account
             WHERE ((RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER and (NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c = false))
             OR (RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE and Multi_Site_Corporate__c = true))
             AND Provider_Member__c = 'Yes'
             ORDER BY name ASC]);
   }
   
   global void execute(Database.BatchableContext BC, List<SObject> batch){
        List<Account> thisYearsDues = (List<Account>)batch; 
    
        //prepare Custom Pricing Manager
        NU__MembershipType__c JointStateProviderMembershipType;
        Id JointStateProviderProductId;
        JointStateProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
        
        if (JointStateProviderMembershipType != null){
            JointStateProviderProductId = JointBillingUtil.getJointStateProviderProductId(JointStateProviderMembershipType);
        }
        
        JointBillingUtil.PopulateCustomPricingManagerCaches(thisYearsDues, JointStateProviderMembershipType.Id, JointStateProviderProductId);
        Date transactionDate = Date.Today();
        
        Map<Id,Account> stateBillingQtrs = new Map<Id,Account>([SELECT Id, Dues_Billing_Quarter__c FROM Account WHERE NU__RecordTypeName__c = :Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER]);
            
        List<Account> aUpdates = new List<Account>();
        for (Account a : thisYearsDues) {
            Account bQtr = stateBillingQtrs.get(a.State_Partner_ID__c);
            if (bQtr != null && bQtr.Dues_Billing_Quarter__c != null && a.Provider_Membership__c != null){
                Map<Id, Decimal> duesPricing = JointBillingUtil.getProviderPricing(transactionDate, a.id, JointStateProviderProductId, JointStateProviderMembershipType.Id, a.Provider_Membership__r.NU__StartDate__c, a.Provider_Membership__r.NU__EndDate__c, a.Provider_Join_On__c);
                aUpdates.add(new Account(Id = a.Id, Calculated_Dues_Amount__c = duesPricing.get(JointStateProviderProductId)));
            }
            else{
                aUpdates.add(new Account(Id = a.Id, Calculated_Dues_Amount__c = 0));
            }
            
        }
        
        if (!aUpdates.isEmpty()){
            update aUpdates;
        }
   }

   global void finish(Database.BatchableContext BC){

   }
}