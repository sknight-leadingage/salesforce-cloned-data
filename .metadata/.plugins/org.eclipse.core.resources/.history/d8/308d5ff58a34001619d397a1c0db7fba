public class NimbleLeadConversionController {
    private final Lead Lead;

    public static final String CREATE_NO_COMPANY_ID = 'None';
    public static final String CREATE_NEW_COMPANY_ID = 'New';

    public Account Individual { get; set; }
    public String Company { get; set; }
    public String CompanyRecordType { get; set; }

    public String ErrorMessage { get; set; }

    public NimbleLeadConversionController(ApexPages.StandardController standardController) {
        Lead = getLead(standardController.getRecord().Id);
        Individual = new Account(OwnerId = UserInfo.getUserId());
    }

    public List<SelectOption> getCompanyOptions() {
        List<SelectOption> options = new List<SelectOption> {
            new SelectOption(CREATE_NO_COMPANY_ID, '--None--'),
            new SelectOption(CREATE_NEW_COMPANY_ID, 'Create New Account: ' + Lead.Company)
        };

        for (Account a : getCompaniesByName(Lead.Company)) {
            options.add(new SelectOption(a.Id, 'Attach to Existing: ' + a.Name + ' (' + a.LeadingAge_ID__c + ')'));
        }

        return options;
    }

    public List<SelectOption> getCompanyRecordTypeOptions() {
        List<SelectOption> options = new List<SelectOption>();

        for (RecordType rt : AccountRecordTypeUtil.getBusinessAccountRecordTypes()) {
            options.add(new SelectOption(rt.Id, rt.Name));
        }

        return options;
    }

    public PageReference convert() {
        RecordType individualType = AccountRecordTypeUtil.getIndividualRecordType();
        RecordType companyType = AccountRecordTypeUtil.getCompanyRecordType();

        // Get the "IsConverted" LeadStatus record
        LeadStatus convertStatus = [SELECT Id, MasterLabel
                                      FROM LeadStatus
                                     WHERE IsConverted = true
                                     LIMIT 1];

        Database.LeadConvert lc = new Database.LeadConvert();

        // Set parameters for the conversion process
        lc.setLeadId(Lead.Id);
        lc.setOwnerId(Individual.OwnerId);
        lc.setDoNotCreateOpportunity(true);
        lc.setConvertedStatus(convertStatus.MasterLabel);

        // If an existing company was selected, associate it with the
        // conversion process instead of creating a new Account
        if (existingCompanySelected()) {
            lc.setAccountId((Id) Company);
        }

        // Convert the Lead
        Database.LeadConvertResult lcr = Database.convertLead(lc);

        // If there was an error converting the Lead, show an error
        if (!lcr.isSuccess()) {
            ApexPages.Message msg = new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Error converting Lead'
            );
            ErrorMessage = '';

            for (Database.Error error : lcr.getErrors()) {
                ErrorMessage += 'Lead conversion error: ' + error.getMessage() + '\n';
            }

            ApexPages.addMessage(msg);
            return null;
        }

        // Get the Contact and Account that the Lead conversion created
        Contact contact = getContactById(lcr.getContactId());
        Account individualAcct;

        // Update the Account that the Lead conversion process created
        try {

            // If 'None' was selected for a Company, get the created Account
            // from the Contact so we can update it to an Individual record
            if (createNoCompanySelected()) {
                individualAcct = new Account(Id = contact.AccountId);

            // Otherwise, create a new Individual record and associate it with
            // the Contact and leave the Contact / Account association
            } else {
                individualAcct = new Account(Name = contact.FirstName);
                insert individualAcct;

                individualAcct = new Account(Id = individualAcct.Id);
                contact.AccountId = individualAcct.Id;

                update contact;

                // Update the company record type based on input
                if (CompanyRecordType != null) {
                    Account companyAcct = new Account(Id = lcr.getAccountId());
                    companyAcct.RecordTypeId = CompanyRecordType;

                    update companyAcct;
                }
            }

            // Set the Individual Account RecordType to an Individual
            individualAcct.RecordTypeId = individualType.Id;

            update individualAcct;

            // If a Company was selected, create an Affiliation between the
            // Individual and Account
            if (!createNoCompanySelected()) {
                NU__Affiliation__c affiliation = new NU__Affiliation__c(
                    NU__Account__c = individualAcct.Id,
                    NU__ParentAccount__c = lcr.getAccountId(),
                    NU__IsPrimary__c = true
                );

                insert affiliation;
            }

            // If the Lead had a different address, revert it back
            // Note, on person accounts:
            // Mailing Address (label) = Shipping (actual)
            // Billing Address (label) = Billing (actual)
            // Other Address (label) = Home (actual)
            if (Lead.Street != individualAcct.PersonMailingStreet
                || Lead.City != individualAcct.PersonMailingCity
                || Lead.State != individualAcct.PersonMailingState
                || Lead.PostalCode != individualAcct.PersonMailingPostalCode
                || Lead.Country != individualAcct.PersonMailingCountry) {

                individualAcct.ShippingStreet = Lead.Street;
                individualAcct.ShippingCity = Lead.City;
                individualAcct.ShippingState = Lead.State;
                individualAcct.ShippingPostalCode = Lead.PostalCode;
                individualAcct.ShippingCountry = Lead.Country;

                individualAcct.BillingStreet = Lead.Street;
                individualAcct.BillingCity = Lead.City;
                individualAcct.BillingState = Lead.State;
                individualAcct.BillingPostalCode = Lead.PostalCode;
                individualAcct.BillingCountry = Lead.Country;

                update individualAcct;
            }

        } catch (DMLException e) {
            ErrorMessage = e.getMessage() + '\n';
            ErrorMessage += e.getStackTraceString();

            ApexPages.Message msg = new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Error converting Lead: ' + ErrorMessage
            );

            ApexPages.addMessage(msg);
            return null;
        }

        // Redirect to the Individual account that was created
        return new PageReference('/' + individualAcct.Id);
    }

    private Contact getContactById(Id id) {
        return [SELECT Id, FirstName, LastName, AccountId
                  FROM Contact
                 WHERE Id = :id];
    }

    private Account getAccountById(Id id) {
        return [SELECT Id, NU__PersonContact__c, Name, PersonMailingStreet, PersonMailingCity, PersonMailingState, PersonMailingPostalCode
                  FROM Account
                 WHERE Id = :id];
    }

    private Account getAccount(Account account) {
        return [SELECT Id, NU__PersonContact__c
                  FROM Account
                 WHERE Id = :account.Id];
    }

    private Lead getLead(Id id) {
        return [SELECT Id,
                       Company,
                       FirstName,
                       LastName,
                       Email,
                       Phone,
                       Street,
                       City,
                       State,
                       PostalCode,
                       Country
                  FROM Lead
                 WHERE Id = :id];
    }

    public static List<Account> getCompaniesByName(String name) {
        return [SELECT Id, Name, LeadingAge_ID__c
                  FROM Account
                 WHERE Name = :name];
    }

    private Boolean createNoCompanySelected() {
        return Company == CREATE_NO_COMPANY_ID;
    }

    private Boolean createNewCompanySelected() {
        return Company == CREATE_NEW_COMPANY_ID;
    }

    private Boolean existingCompanySelected() {
        return Company != CREATE_NO_COMPANY_ID && Company != CREATE_NEW_COMPANY_ID;
    }
}