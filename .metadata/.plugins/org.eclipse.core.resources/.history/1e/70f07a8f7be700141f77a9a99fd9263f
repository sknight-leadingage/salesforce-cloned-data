// visualforce page controller for CongressionalDistrictReport.page
// outputs a printable list of Members for advocacy work from a specified US Congressional District
// Developed by NimbleUser 2014 (NF)

public class CongressionalDistrictReportController {
    public string SelectedState {get; set;} //the state to be included in the report
    public string SelectedDistrict {get; set;} //the district number within the state
    public string SelectedTypeProvider {get; set;} //the Provider type
    public string SelectedDistrictCode {get { return SelectedState + SelectedDistrict;} set;} //the state and district number string
    public integer SelectedLayout {get;set;} // to hide certin layout aspects
    public integer RecordsCount {get; set;} //the number of rows returned in the query
    public Boolean isCheckedHideMenus {get;set;} //checkbox for hiding SF menus in the output
    public Boolean isCheckedRenderAsPDF {get;set;}  //checkbox for rendering the page as a PDF (has some display quirks; hidden for now)

    //constructor
    public CongressionalDistrictReportController() {
    }

    //run refreshes the page and requeries the Records list
    public void Run() {
    }

	//get Provider type picklist
	   // Get a list of picklist values from an existing object field.
   private list<SelectOption> getPicklistValues(SObject obj, String fld)
   {
      list<SelectOption> options = new list<SelectOption>();
      // Get the object type of the SObject.
      Schema.sObjectType objType = obj.getSObjectType(); 
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fld).getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a : values)
      { 
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }
      return options;
   }
    //retrieve records for a selected distrct
    private List <Account> records_priv = null;
    public List <Account> records {
        get {
        	string provdertype = '';
        	if(SelectedTypeProvider != '-1' )
        	{
        		provdertype = 'AND Provider_Type__c INCLUDES(\'' + SelectedTypeProvider + '\') ';
        	}
            records_priv = Database.query('SELECT ' +
                'AL_Program_Services__c, ' +
                'BillingCity, ' +
                'BillingPostalCode, ' +
                'BillingState, ' +
                'BillingStreet, ' +
                'CCRC_Program_Services__c, ' +
                'HCBS_Program_Services__c, ' +
                'Housing_Program_Services__c, ' +
                'Id, ' +
                'isLeadingAgeMember__c, ' +
                'kwzd__KW_USHouseDistrict__c, ' +
                'kwzd__KW_USHouseDistrict__pc, ' +
                'Name, ' +
                'Nursing_Program_Services__c, ' +
                'Product_And_Services_Listing__c, ' +
                'Provider_Type__c, ' +
                'RecordType.Name, ' +
                '    (SELECT ' +
                '    Id, ' +
                '    Name, ' +
                '    NU__Account__r.Name, ' +
                '    NU__Account__r.PersonEmail, ' +
                '    NU__Account__r.PersonTitle, ' +
                '    NU__Role__c ' +
                '    FROM NU__Affiliates__r ' +
                '    WHERE NU__Role__c INCLUDES (\'LeadingAge Primary Contact\') ' +
                '    ) ' +
                'FROM Account ' +
                'WHERE kwzd__KW_USHouseDistrict__c =: SelectedDistrictCode ' +
                'AND (RecordType.Name != \'Individual\' AND RecordType.Name != \'Individual Associate\' AND RecordType.Name != \'Corporate Alliance/Sponsor\') ' +
                'AND isLeadingAgeMember__c = \'Yes\' ' +
                provdertype);
            RecordsCount = records_priv.size();
            ReformatServices();
            return records_priv;
        }
    }

    //combines several fields into one; cleans picklist items for display    
    private void ReformatServices() {
        for (Account a : records_priv) {
            if (a.Provider_Type__c != null) {
                a.Provider_Type__c = a.Provider_Type__c.replace(';',', ');
            }
            if (a.AL_Program_Services__c != null) {
                a.AL_Program_Services__c = a.AL_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.AL_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.AL_Program_Services__c;
                }
            }
            if (a.CCRC_Program_Services__c != null) {
                a.CCRC_Program_Services__c = a.CCRC_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.CCRC_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.CCRC_Program_Services__c;
                }
            }
            if (a.HCBS_Program_Services__c != null) {
                a.HCBS_Program_Services__c = a.HCBS_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.HCBS_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.HCBS_Program_Services__c;
                }
            }
            if (a.Housing_Program_Services__c != null) {
                a.Housing_Program_Services__c = a.Housing_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.Housing_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.Housing_Program_Services__c;
                }
            }
            if (a.Nursing_Program_Services__c != null) {
                a.Nursing_Program_Services__c = a.Nursing_Program_Services__c.replace(';',', ');
                if (a.Provider_Type__c != null) {
                    a.Provider_Type__c = a.Provider_Type__c + ', ' + a.Nursing_Program_Services__c;
                }
                else {
                    a.Provider_Type__c = a.Nursing_Program_Services__c;
                }
            }
            
            //truncate to 175 chars
            if (a.Provider_Type__c != null && a.Provider_Type__c.length() >= 175) {
                a.Provider_Type__c = a.Provider_Type__c.substring(0,175);
                a.Provider_Type__c = a.Provider_Type__c.substring(0,a.Provider_Type__c.lastIndexOf(' '));
                a.Provider_Type__c = a.Provider_Type__c + '...';
            }
        }
    }

    //outputs a (drop-down) list of States
    public List<SelectOption> getStates() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('AL', 'Alabama'));
        options.add(new SelectOption('AK', 'Alaska'));
        options.add(new SelectOption('AZ', 'Arizona'));
        options.add(new SelectOption('AR', 'Arkansas'));
        options.add(new SelectOption('CA', 'California'));
        options.add(new SelectOption('CO', 'Colorado'));
        options.add(new SelectOption('CT', 'Connecticut'));
        options.add(new SelectOption('DE', 'Delaware'));
        options.add(new SelectOption('FL', 'Florida'));
        options.add(new SelectOption('GA', 'Georgia'));
        options.add(new SelectOption('HI', 'Hawaii'));
        options.add(new SelectOption('ID', 'Idaho'));
        options.add(new SelectOption('IL', 'Illinois'));
        options.add(new SelectOption('IN', 'Indiana'));
        options.add(new SelectOption('IA', 'Iowa'));
        options.add(new SelectOption('KS', 'Kansas'));
        options.add(new SelectOption('KY', 'Kentucky'));
        options.add(new SelectOption('LA', 'Louisiana'));
        options.add(new SelectOption('ME', 'Maine'));
        options.add(new SelectOption('MD', 'Maryland'));
        options.add(new SelectOption('MA', 'Massachusetts'));
        options.add(new SelectOption('MI', 'Michigan'));
        options.add(new SelectOption('MN', 'Minnesota'));
        options.add(new SelectOption('MS', 'Mississippi'));
        options.add(new SelectOption('MO', 'Missouri'));
        options.add(new SelectOption('MT', 'Montana'));
        options.add(new SelectOption('NE', 'Nebraska'));
        options.add(new SelectOption('NV', 'Nevada'));
        options.add(new SelectOption('NH', 'New Hampshire'));
        options.add(new SelectOption('NJ', 'New Jersey'));
        options.add(new SelectOption('NM', 'New Mexico'));
        options.add(new SelectOption('NY', 'New York'));
        options.add(new SelectOption('NC', 'North Carolina'));
        options.add(new SelectOption('ND', 'North Dakota'));
        options.add(new SelectOption('OH', 'Ohio'));
        options.add(new SelectOption('OK', 'Oklahoma'));
        options.add(new SelectOption('OR', 'Oregon'));
        options.add(new SelectOption('PA', 'Pennsylvania'));
        options.add(new SelectOption('RI', 'Rhode Island'));
        options.add(new SelectOption('SC', 'South Carolina'));
        options.add(new SelectOption('SD', 'South Dakota'));
        options.add(new SelectOption('TN', 'Tennessee'));
        options.add(new SelectOption('TX', 'Texas'));
        options.add(new SelectOption('UT', 'Utah'));
        options.add(new SelectOption('VT', 'Vermont'));
        options.add(new SelectOption('VA', 'Virginia'));
        options.add(new SelectOption('WA', 'Washington'));
        options.add(new SelectOption('WV', 'West Virginia'));
        options.add(new SelectOption('WI', 'Wisconsin'));
        options.add(new SelectOption('WY', 'Wyoming'));
        return options;
    }

    //specify the full state name, given the selected abbreviation
    public string getSelectedStateName() {
        return SelectedState == 'AL' ? 'Alabama' :
        SelectedState == 'AL' ? 'Alabama' :
        SelectedState == 'AK' ? 'Alaska' :
        SelectedState == 'AZ' ? 'Arizona' :
        SelectedState == 'AR' ? 'Arkansas' :
        SelectedState == 'CA' ? 'California' :
        SelectedState == 'CO' ? 'Colorado' :
        SelectedState == 'CT' ? 'Connecticut' :
        SelectedState == 'DE' ? 'Delaware' :
        SelectedState == 'FL' ? 'Florida' :
        SelectedState == 'GA' ? 'Georgia' :
        SelectedState == 'HI' ? 'Hawaii' :
        SelectedState == 'ID' ? 'Idaho' :
        SelectedState == 'IL' ? 'Illinois' :
        SelectedState == 'IN' ? 'Indiana' :
        SelectedState == 'IA' ? 'Iowa' :
        SelectedState == 'KS' ? 'Kansas' :
        SelectedState == 'KY' ? 'Kentucky' :
        SelectedState == 'LA' ? 'Louisiana' :
        SelectedState == 'ME' ? 'Maine' :
        SelectedState == 'MD' ? 'Maryland' :
        SelectedState == 'MA' ? 'Massachusetts' :
        SelectedState == 'MI' ? 'Michigan' :
        SelectedState == 'MN' ? 'Minnesota' :
        SelectedState == 'MS' ? 'Mississippi' :
        SelectedState == 'MO' ? 'Missouri' :
        SelectedState == 'MT' ? 'Montana' :
        SelectedState == 'NE' ? 'Nebraska' :
        SelectedState == 'NV' ? 'Nevada' :
        SelectedState == 'NH' ? 'New Hampshire' :
        SelectedState == 'NJ' ? 'New Jersey' :
        SelectedState == 'NM' ? 'New Mexico' :
        SelectedState == 'NY' ? 'New York' :
        SelectedState == 'NC' ? 'North Carolina' :
        SelectedState == 'ND' ? 'North Dakota' :
        SelectedState == 'OH' ? 'Ohio' :
        SelectedState == 'OK' ? 'Oklahoma' :
        SelectedState == 'OR' ? 'Oregon' :
        SelectedState == 'PA' ? 'Pennsylvania' :
        SelectedState == 'RI' ? 'Rhode Island' :
        SelectedState == 'SC' ? 'South Carolina' :
        SelectedState == 'SD' ? 'South Dakota' :
        SelectedState == 'TN' ? 'Tennessee' :
        SelectedState == 'TX' ? 'Texas' :
        SelectedState == 'UT' ? 'Utah' :
        SelectedState == 'VT' ? 'Vermont' :
        SelectedState == 'VA' ? 'Virginia' :
        SelectedState == 'WA' ? 'Washington' :
        SelectedState == 'WV' ? 'West Virginia' :
        SelectedState == 'WI' ? 'Wisconsin' :
        SelectedState == 'WY' ? 'Wyoming' : '(not specified)';
    }
	//Get Provder types
	
	public List<SelectOption> getTypes(){
		list<SelectOption> typeOptions = getPicklistValues(new Account(Name = 'ProviderType'), 'Provider_Type__c');
		 return typeOptions;
	}
    //outputs a (drop-down) list of District Numbers
    public List<SelectOption> getDistricts() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('01', '1'));
        options.add(new SelectOption('02', '2'));
        options.add(new SelectOption('03', '3'));
        options.add(new SelectOption('04', '4'));
        options.add(new SelectOption('05', '5'));
        options.add(new SelectOption('06', '6'));
        options.add(new SelectOption('07', '7'));
        options.add(new SelectOption('08', '8'));
        options.add(new SelectOption('09', '9'));
        options.add(new SelectOption('10', '10'));
        options.add(new SelectOption('11', '11'));
        options.add(new SelectOption('12', '12'));
        options.add(new SelectOption('13', '13'));
        options.add(new SelectOption('14', '14'));
        options.add(new SelectOption('15', '15'));
        options.add(new SelectOption('16', '16'));
        options.add(new SelectOption('17', '17'));
        options.add(new SelectOption('18', '18'));
        options.add(new SelectOption('19', '19'));
        options.add(new SelectOption('20', '20'));
        options.add(new SelectOption('21', '21'));
        options.add(new SelectOption('22', '22'));
        options.add(new SelectOption('23', '23'));
        options.add(new SelectOption('24', '24'));
        options.add(new SelectOption('25', '25'));
        options.add(new SelectOption('26', '26'));
        options.add(new SelectOption('27', '27'));
        options.add(new SelectOption('28', '28'));
        options.add(new SelectOption('29', '29'));
        options.add(new SelectOption('30', '30'));
        options.add(new SelectOption('31', '31'));
        options.add(new SelectOption('32', '32'));
        options.add(new SelectOption('33', '33'));
        options.add(new SelectOption('34', '34'));
        options.add(new SelectOption('35', '35'));
        options.add(new SelectOption('36', '36'));
        options.add(new SelectOption('37', '37'));
        options.add(new SelectOption('38', '38'));
        options.add(new SelectOption('39', '39'));
        options.add(new SelectOption('40', '40'));
        options.add(new SelectOption('41', '41'));
        options.add(new SelectOption('42', '42'));
        options.add(new SelectOption('43', '43'));
        options.add(new SelectOption('44', '44'));
        options.add(new SelectOption('45', '45'));
        options.add(new SelectOption('46', '46'));
        options.add(new SelectOption('47', '47'));
        options.add(new SelectOption('48', '48'));
        options.add(new SelectOption('49', '49'));
        options.add(new SelectOption('50', '50'));
        options.add(new SelectOption('51', '51'));
        options.add(new SelectOption('52', '52'));
        options.add(new SelectOption('53', '53'));
        return options;  
    }

    //specify the ordinal name, given the selected cardinal number
    public string getSelectedDistrictOrdinal() {
        return SelectedDistrict == '01' || SelectedDistrict == '21' || SelectedDistrict == '31' || SelectedDistrict == '41' || SelectedDistrict == '51' ? integer.valueof(SelectedDistrict) + 'st' :
        SelectedDistrict == '02' || SelectedDistrict == '22' || SelectedDistrict == '32' || SelectedDistrict == '42' || SelectedDistrict == '52' ? integer.valueof(SelectedDistrict) + 'nd' :
        SelectedDistrict == '03' || SelectedDistrict == '23' || SelectedDistrict == '33' || SelectedDistrict == '43' || SelectedDistrict == '53' ? integer.valueof(SelectedDistrict) + 'rd' :
        SelectedDistrict == null ? null : integer.valueof(SelectedDistrict) + 'th';
    }

}