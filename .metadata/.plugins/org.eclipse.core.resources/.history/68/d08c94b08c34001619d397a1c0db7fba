/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestIAHSAMembershipPricer {
	
	static Account companyAccount = null;
	static NU__MembershipType__c iahsaMT = DatafactoryMembershipTypeExt.insertIAHSAMembershipType();
   	static NU__Product__c membershipProduct = NU.DataFactoryProduct.insertDefaultMembershipProduct(iahsaMT.NU__Entity__c);
   	static NU__MembershipTypeProductLink__c primaryMTPL = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(iahsaMT.Id, membershipProduct.Id);
	
	static void loadData(){
		companyAccount = DataFactoryAccountExt.insertCompanyAccount();
    	iahsaMT = DatafactoryMembershipTypeExt.insertIAHSAMembershipType();
    	membershipProduct = NU.DataFactoryProduct.insertDefaultMembershipProduct(iahsaMT.NU__Entity__c);
        primaryMTPL = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(iahsaMT.Id, membershipProduct.Id);
	}

    static testMethod void firstQuarterProratingTest() {
        loadData();
    	
    	Decimal expectedIAHSADuesPrice = membershipProduct.NU__ListPrice__c;
    	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = Date.NewInstance(2013, 2, 1);
    	
    	IAHSAMembershipPricer.testingJoinDate = joinDate;
    	
    	TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
    	    companyAccount.Id,
    	    membershipProduct,
    	    NU.Constant.PRICE_CLASS_DEFAULT,
    	    Date.Today(),
    	    iahsaMT.Id,
    	    expectedIAHSADuesPrice,
    	    startDate,
    	    endDate);
    }
    
    static testMethod void secondQuarterProratingTest() {
        loadData();
    	
    	Decimal expectedIAHSADuesPrice = membershipProduct.NU__ListPrice__c * IAHSAMembershipPricer.SECOND_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT;
    	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = Date.NewInstance(2013, 4, 1);
    	
    	IAHSAMembershipPricer.testingJoinDate = joinDate;
    	
    	TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
    	    companyAccount.Id,
    	    membershipProduct,
    	    NU.Constant.PRICE_CLASS_DEFAULT,
    	    Date.Today(),
    	    iahsaMT.Id,
    	    expectedIAHSADuesPrice,
    	    startDate,
    	    endDate);
    }
    
    static testMethod void thirdQuarterProratingTest() {
        loadData();
    	
    	Decimal expectedIAHSADuesPrice = membershipProduct.NU__ListPrice__c * IAHSAMembershipPricer.THIRD_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT;
    	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = Date.NewInstance(2013, 7, 1);
    	
    	IAHSAMembershipPricer.testingJoinDate = joinDate;
    	
    	TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
    	    companyAccount.Id,
    	    membershipProduct,
    	    NU.Constant.PRICE_CLASS_DEFAULT,
    	    Date.Today(),
    	    iahsaMT.Id,
    	    expectedIAHSADuesPrice,
    	    startDate,
    	    endDate);
    }
    
    static testMethod void fourthQuarterProratingTest() {
        loadData();
    	
    	Decimal expectedIAHSADuesPrice = membershipProduct.NU__ListPrice__c * IAHSAMembershipPricer.THIRD_QUARTER_IAHSA_PRORATION_DISCOUNT_PERCENT;
    	
    	Date startDate = Date.NewInstance(2013, 1, 1);
    	Date endDate = Date.NewInstance(2013, 12, 31);
    	Date joinDate = Date.NewInstance(2013, 9, 1);
    	
    	IAHSAMembershipPricer.testingJoinDate = joinDate;
    	
    	TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
    	    companyAccount.Id,
    	    membershipProduct,
    	    NU.Constant.PRICE_CLASS_DEFAULT,
    	    Date.Today(),
    	    iahsaMT.Id,
    	    expectedIAHSADuesPrice,
    	    startDate,
    	    endDate);
    }
}