/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCongressionalDistrictReportControllr {

    static CongressionalDistrictReportController controller = null;
    static MembersByStateReportController controllerByState = null;

    static testMethod void RunReport() {
        
        //create a test account in New York district 1
        Id rtProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        Account a = new Account (
            name='Test',
            kwzd__KW_USHouseDistrict__c = 'NY01',
            CAST_Gold_Partner__c = true,
            RecordTypeId = rtProvider,
            Provider_Type__c='Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            AL_Program_Services__c = 'Morbi vehicula lacinia venenatis.',
            CCRC_Program_Services__c = ' Cras lobortis tortor id tellus aliquam, vitae interdum turpis volutpat posuere.',
            HCBS_Program_Services__c = 'Vivamus ut ipsum ac justo aliquam suscipit quis quis mauris.',
            Housing_Program_Services__c = 'Etiam et ligula non ipsum condimentum blandit feugiat in metus.',
            Nursing_Program_Services__c = 'Proin sollicitudin nunc eu felis pharetra.');
        insert a;
        
        Test.startTest();

        //create the controller and set the drop-down items to test values
        controller = new CongressionalDistrictReportController();
        controller.SelectedState = 'NY';
        controller.SelectedLayout = 1;
        controller.SelectedDistrict = '01';
        controller.SelectedTypeProvider = '-1';

        //call all public methods in the controller
        controller.Run();
        List<Account> rlist = controller.records;
        controller.getStates();
        controller.getSelectedStateName();
        controller.getDistricts();
        controller.getSelectedDistrictOrdinal();

        Test.stopTest();

        //assertions
        List<Account> alist = [SELECT name, RecordType.name, Provider_Type__c, kwzd__KW_USHouseDistrict__c, CAST_Gold_Partner__c, isLeadingAgeMember__c FROM Account];
        System.assertEquals(alist.size(), controller.RecordsCount); // one record exists in both the query above and in the controller version
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error)); // no errors thrown
    }


    static testMethod void RunReportByState() {
        
        //create a test account in New York district 1
        Id rtProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        Account a = new Account (
            name='Test',
            kwzd__KW_USHouseDistrict__c = 'NY01',
            BillingState = 'NY',
            CAST_Gold_Partner__c = true,
            RecordTypeId = rtProvider,
            Provider_Type__c='Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            AL_Program_Services__c = 'Morbi vehicula lacinia venenatis.',
            CCRC_Program_Services__c = ' Cras lobortis tortor id tellus aliquam, vitae interdum turpis volutpat posuere.',
            HCBS_Program_Services__c = 'Vivamus ut ipsum ac justo aliquam suscipit quis quis mauris.',
            Housing_Program_Services__c = 'Etiam et ligula non ipsum condimentum blandit feugiat in metus.',
            Nursing_Program_Services__c = 'Proin sollicitudin nunc eu felis pharetra.');
        insert a;
        
        Test.startTest();

        //create the controller and set the drop-down items to test values
        controllerByState = new MembersByStateReportController();
        controllerByState.SelectedState = 'NY';
        controllerByState.SelectedLayout = 1;
        controllerByState.SelectedTypeProvider = '-1';

        //call all public methods in the controller
        controllerByState.Run();
        List<Account> rlist = controllerByState.records;
        controllerByState.getStates();
        controllerByState.getSelectedStateName();
        Test.stopTest();

        //assertions
        List<Account> alist = [SELECT name, RecordType.name, Provider_Type__c, kwzd__KW_USHouseDistrict__c, CAST_Gold_Partner__c, isLeadingAgeMember__c FROM Account];
        System.assertEquals(alist.size(), controllerByState.RecordsCount); // one record exists in both the query above and in the controller version
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.Error)); // no errors thrown
    }
}