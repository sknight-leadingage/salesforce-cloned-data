/**
 * @author NimbleUser
 * @date Updated: 1/30/13
 * @description This class provides common Account Record Type utility functions and methods and can be
 * used throughout the org.
 */
public class AccountRecordTypeUtil {
	
	private static List<RecordType> busAcctRecTypes = null;
	private static List<RecordType> personAcctRecTypes = null;

	private static Map<Id, RecordType> busAcctRecTypesMap = null;
	public static Map<Id, RecordType> BusinessAcctRecTypesMap{
		get{
			if (busAcctRecTypesMap == null){
				busAcctRecTypesMap = new Map<Id, RecordType>( getBusinessAccountRecordTypes() );
			}
			
			return busAcctRecTypesMap;
		}
	}
	
	private static Map<Id, RecordType> busAcctRecTypesWithoutStatePartnerMap = null;
	public static Map<Id, RecordType> BusinessAcctRecTypesWithoutStatePartnerMap{
		get{
			if (busAcctRecTypesWithoutStatePartnerMap == null){
				busAcctRecTypesWithoutStatePartnerMap = BusinessAcctRecTypesMap.deepClone();
				busAcctRecTypesWithoutStatePartnerMap.remove( Constant.ACCOUNT_STATE_PARTNER_RECORD_TYPE_ID );
			}
			
			return busAcctRecTypesWithoutStatePartnerMap;
		}
	}
	
	private static Map<String, RecordType> recordTypesByNameMapPriv = null;
	public static Map<String, RecordType> RecordTypesByName{
		get{
			if (recordTypesByNameMapPriv == null){
				recordTypesByNameMapPriv = new Map<String, RecordType>();
				
				List<RecordType> accountRecordTypes = BusinessAcctRecTypesMap.values();
				accountRecordTypes.addall( getPersonAccountRecordTypes() );
				
				for (RecordType accountRT : accountRecordTypes){
					recordTypesByNameMapPriv.put(accountRT.Name, accountRT);
				}
			}
			
			return recordTypesByNameMapPriv;
		}
	}

	public static List<RecordType> getBusinessAccountRecordTypes(){
		if (busAcctRecTypes == null){
			busAcctRecTypes = getAccountRecordTypes(false);
		}
		
		return busAcctRecTypes;
	}
	
	public static List<RecordType> getPersonAccountRecordTypes(){
		if (personAcctRecTypes == null){
			personAcctRecTypes = getAccountRecordTypes(true);
		}
		
		return personAcctRecTypes;
	}
	
	public static RecordType getIndividualRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_INDIVIDUAL);
	}
	
	public static RecordType getIndividualAssociateRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_INDIVIDUAL_ASSOCIATE);
	}
	
	public static RecordType getCompanyRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_COMPANY);
	}
	
	public static RecordType getCorporateAllianceSponsorRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_CORPORATE_ALLIANCE_SPONSOR);
	}
	
	public static RecordType getMultiSiteRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE);
	}
	
	public static RecordType getProviderRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_PROVIDER);
	}
	
	public static RecordType getStatePartnerRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER);
	}
	
	public static RecordType getStatePartnerPortalRecordType(){
		return RecordTypesByName.get(Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER_PORTAL);
	}

	private static List<RecordType> getAccountRecordTypes(Boolean isPersonAccountType){
		return [select id,
		               name,
		               SObjectType,
		               IsPersonType
		          from RecordType
		         where SobjectType = 'Account'
		           and IsActive = true
		           and IsPersonType = :isPersonAccountType];
	}
}