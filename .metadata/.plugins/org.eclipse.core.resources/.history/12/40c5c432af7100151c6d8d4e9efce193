global class BatchUpdateProviderLapsedOn implements Database.Batchable<sobject>
{
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator('SELECT Id, Name, LeadingAge_ID__c, Provider_Lapsed_On__c, Cancellation_Reason__c FROM Account WHERE Provider_Lapsed_On__c != null AND Provider_Lapsed_On__c <= TODAY ORDER BY Provider_Lapsed_On__c');
   }
   global void execute(Database.BatchableContext BC,List<sObject> scope)
   {
		Map<Id,Sobject> lAcc = new Map<Id,Sobject>(scope);
		List<NU__Membership__c> lMShips = [SELECT Id, NU__Account__c, Membership_Date__c, NU__StartDate__c, NU__EndDate__c, NU__Status__c, Cancellation_Reason__c, NU__OrderItemLine__c, NU__OrderItemLine__r.NU__DeferredSchedule__c FROM NU__Membership__c WHERE NU__Account__c IN :lAcc.keySet()];
		
		List<Account> lUpdateAcc = new List<Account>();
		List<NU__Membership__c> lUpdateMShips = new List<NU__Membership__c>();

		integer x = 0;
		Account a = null;
		for (NU__Membership__c m : lMShips) {
	        a = (Account)lAcc.get(m.NU__Account__c);
	        if (a.Provider_Lapsed_On__c >= m.NU__StartDate__c && a.Provider_Lapsed_On__c <= m.NU__EndDate__c) {
				x = 0;
				for(Account ac : lUpdateAcc) {
						if (ac.Id == a.Id) {
							x = 1;
							break;
						}
				}
				if (x == 0) {
		                    lUpdateAcc.add(new Account(Id = a.Id, Provider_Lapsed_On__c = null, Cancellation_Reason__c = null));
				}
				
				//Only update when the membership doesn't have a deferred schedule
				if (m.NU__OrderItemLine__c == null || (m.NU__OrderItemLine__c != null && m.NU__OrderItemLine__r.NU__DeferredSchedule__c == null)) {
					Integer numberOfDays = Date.daysInMonth(a.Provider_Lapsed_On__c.year(), a.Provider_Lapsed_On__c.month());
					Date dMbrEndDate = Date.newInstance(a.Provider_Lapsed_On__c.year(), a.Provider_Lapsed_On__c.month(), numberOfDays);
		            lUpdateMShips.add(new NU__Membership__c(Id = m.Id, Last_Provider_Lapsed_On__c = a.Provider_Lapsed_On__c, Cancellation_Reason__c = a.Cancellation_Reason__c, NU__EndDate__c = dMbrEndDate));
				}
	        }
		}

		for (Id af : lAcc.keySet()) {
        	x = 0;
            for (Account aff : lUpdateAcc) {
            	if (aff.Id == af) {
                	x=1;
                    break;
                }
            }
            if (x == 0) {
	            lUpdateAcc.add(new Account(Id = af, Provider_Lapsed_On__c = null));
	            //System.debug('Account NF in MShips: ' + af);
            }
		}

		if (lUpdateAcc.size() > 0 || lUpdateMShips.size() > 0) {
			NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger_Subscriptions');
			if (lUpdateAcc.size() > 0) {
		        update lUpdateAcc;
			}
	
			if (lUpdateMShips.size() > 0) {
		        update lUpdateMShips;
			}
			NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger_Subscriptions');
		}
   }

	global void finish(Database.BatchableContext BC)
	{
		//Ensure the job only runs once when temp scheduled... https://help.salesforce.com/apex/HTViewSolution?id=000002809&language=en_US
		AsyncApexJob a = [SELECT Id FROM AsyncApexJob WHERE Id = :BC.getJobId()];
		system.abortJob(a.id);
	}
}