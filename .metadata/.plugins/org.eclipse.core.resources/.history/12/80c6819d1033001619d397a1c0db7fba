public class JointStateProviderOrderManager {
	Id JointBillingId { get; set; }
	
	public Joint_Billing__c JointBilling { get; set; }
	
	public NU__MembershipType__c JointProviderMembershipType { get; set; }
	
	Id BatchId { get; set; }
	
	Date TransactionDate { get; set; }
	
	public JointStateProviderOrderManager(Id jointBillingIdParm, Id batchIdParm, Date transactionDateParm){
		JointBillingId = jointBillingIdParm;
		BatchId = batchIdParm;
		TransactionDate = transactionDateParm;
		
		JointBilling = getJointBilling(JointBillingId);
		JointProviderMembershipType = MembershipTypeQuerier.getJointStateProviderMembershipType();
	}
	
	public NU__Order__c createOrder(){
		System.SavePoint sp = Database.setSavePoint();
		
		NU__Order__c orderToCreate = createOrderHelper();
		insert orderToCreate;
		
		NU__OrderItem__c orderItemToCreate = createOrderItem(orderToCreate.Id);
		insert orderItemToCreate;
		
		NU__OrderItemLine__c oilToCreate = createOrderItemLine(orderItemToCreate.Id);
		insert oilToCreate;
		
		Joint_Billing__c jbToUpdate = new Joint_Billing__c(
			Id = JointBilling.Id,
			Order__c = orderToCreate.Id,
			Start_Date__c = JointBilling.Start_Date__c,
			End_Date__c = JointBilling.End_Date__c
		);
		
		update jbToUpdate;
		
		Account statePartnerAccount = new Account(Id = JointBilling.State_Partner__c);
		
		NU__Membership__c statePartnerMembership = DataFactoryMembershipExt.createMembership(statePartnerAccount, jbToUpdate, JointProviderMembershipType);
		
		if (JointBilling.State_Partner__r.RecordTypeId == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID){
			statePartnerMembership = 
			[select id,
			        name,
			        NU__Account__c,
			        NU__StartDate__c,
			        NU__EndDate__c
			   from NU__Membership__c
			  where NU__Account__c = :JointBilling.State_Partner__c
			    and NU__StartDate__c = :statePartnerMembership.NU__StartDate__c
			    and NU__EndDate__c = :statePartnerMembership.NU__EndDate__c
			    and NU__MembershipType__c = :statePartnerMembership.NU__MembershipType__c];
		}
		
		statePartnerMembership.NU__OrderItemLine__c = oilToCreate.Id;
		
		upsert statePartnerMembership;
		
		oilToCreate.NU__Membership__c = statePartnerMembership.Id;
		update oilToCreate;
		
		
		if (BatchId == null){
			NU__Batch__c insertedAutomaticBatch = insertAutomaticBatch();
			BatchId = insertedAutomaticBatch.Id;
		}
		
		try{
			NU.TransactionGenerator.TransactionGenerationResult tgr = generateTransactions(oilToCreate, null);
			
			if (tgr == null ||
			    NU.OperationResult.isSuccessful(tgr.Result) == false){
				Database.rollback(sp);
			}
		}
		catch(Exception ex){
			Database.rollback(sp);
			throw ex;
		}
		
		return orderToCreate;
	}
	
	public NU__Order__c updateOrder(){		
		Decimal billedAmount = JointBilling.Billed_Amount__c;
		Decimal currentAmount = JointBilling.Net_Amount__c;
		Decimal adjustmentAmount = currentAmount - billedAmount;
		
		if (adjustmentAmount == 0){
			return null;
		}
		
		System.SavePoint sp = Database.setSavePoint();
		
		NU__OrderItem__c jointStateOrderItem = getJointStateOrderItem(JointBilling.Order__c);
		NU__OrderItemLine__c jointStateOriginalOIL = jointStateOrderItem.NU__OrderItemLines__r[0];
		
		NU__OrderItemLine__c jointStateAdjustedOIL = createAdjustmentOIL(jointStateOriginalOIL, adjustmentAmount);
		update jointStateAdjustedOIL;
		
		if (BatchId == null){
			NU__Batch__c insertedAutomaticBatch = insertAutomaticBatch();
			BatchId = insertedAutomaticBatch.Id;
		}
		
		try{
			NU.TransactionGenerator.TransactionGenerationResult tgr = generateTransactions(jointStateAdjustedOIL, jointStateOriginalOIL);
			
			if (tgr == null ||
			    NU.OperationResult.isSuccessful(tgr.Result) == false){
				Database.rollback(sp);
			}
		}
		catch(Exception ex){
			Database.rollback(sp);
			throw ex;
		}
		
		
		return JointBilling.Order__r;
	}
	
	private Joint_Billing__c getJointBilling(Id jointBillingId){
		return [select id,
		               name,
		               State_Partner__c,
		               State_Partner__r.RecordTypeId,
		               Amount__c,
		               Service_Fee__c,
		               Net_Amount__c,
		               Order__c,
		               Order__r.Id,
		               Order__r.Name,
		               Billed_Amount__c,
		               Start_Date__c,
		               End_Date__c
		          from Joint_Billing__c
		         where id = :jointBillingId];
	}
	
	private NU__OrderItem__c getJointStateOrderItem(Id orderId){
		NU__OrderItem__c orderItem = null;
		List<NU__OrderItem__c> orderItems = database.query('SELECT id, ' +
		               'name, ' +
		               'NU__RecordTypeName__c, ' +
		               '(SELECT id, ' +
		                       'name, ' +
		                       'NU__UnitPrice__c, ' +
		                       'NU__Product__c, ' +
		                       'NU__OrderItem__c, ' +
		                       'NU__Quantity__c, ' +
		                       'NU__TotalPrice__c ' +
		                  'FROM NU__OrderItemLines__r) ' +
		          'FROM NU__OrderItem__c ' +
		         'WHERE NU__Order__c = :orderId');
		if (orderItems.size() == 1) {
			orderItem = orderItems[0]; 
		  } else if (orderItems.size() > 1) {
		  	for(NU__OrderItem__c o : orderItems) {
		  		if (o.NU__RecordTypeName__c == Constant.ORDER_RECORD_TYPE_MEMBERSHIP) {
		  			orderItem = o;
		  			break;
		  		}
		  	}
		  	if (orderItem == null) {
		  		orderItem = orderItems[0];
		  	}
		}
		return orderItem;
	}
	
	private NU__Order__c createOrderHelper(){
		NU__Order__c order = new NU__Order__c(
			NU__BillTo__c = JointBilling.State_Partner__c,
			NU__TransactionDate__c = TransactionDate,
			NU__Entity__c = JointProviderMembershipType.NU__Entity__c,
			NU__InvoiceTerm__c = JointProviderMembershipType.NU__Entity__r.NU__InvoiceTerm__c
		);
		
		return order;
	}
	
	private NU__OrderItem__c createOrderItem(Id orderId){
		NU__PriceClass__c defaultPriceClass = [select id from NU__PriceClass__c where name = :NU.Constant.PRICE_CLASS_DEFAULT];
		
		Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__OrderItem__c.getRecordTypeInfosByName();
        Id membershipRecordTypeId = rt.get(Constant.ORDER_RECORD_TYPE_MEMBERSHIP).RecordTypeId;
		
		NU__OrderItem__c orderItem = new NU__OrderItem__c(
			NU__Order__c = orderId,
			NU__Customer__c = JointBilling.State_Partner__c,
			NU__PriceClass__c = defaultPriceClass.Id,
			RecordTypeId = membershipRecordTypeId
		);
		
		return orderItem;
	}
	
	private NU__OrderItemLine__c createOrderItemLine(Id orderItemId){
		NU__OrderItemLine__c oil = new NU__OrderItemLine__c(
			NU__OrderItem__c = orderItemId,
			NU__Quantity__c = 1,
			NU__UnitPrice__c = JointBilling.Net_Amount__c,
			NU__Product__c = JointProviderMembershipType.NU__MembershipTypeProductLinks__r[0].NU__Product__c
		);
		
		return oil;
	}
	
	private NU__OrderItemLine__c createAdjustmentOIL(NU__OrderItemLine__c jointStateOriginalOIL, Decimal adjustmentAmount){
		NU__OrderItemLine__c adjustmentOIL = 
		  // deep clone with id ignoring readonly timestamps and autonumbers
		  jointStateOriginalOIL.clone(true, true, false, false);
		
		adjustmentOIL.NU__UnitPrice__c += adjustmentAmount;
		adjustmentOIL.NU__AdjustmentDate__c = TransactionDate;
		
		return adjustmentOIL;
	}
	
	private NU__Batch__c insertAutomaticBatch(){
        Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__Batch__c.getRecordTypeInfosByName();
        
        NU__Batch__c automaticBatch = new NU__Batch__c(
            RecordTypeId = rt.get(Constant.BATCH_RECORD_TYPE_AUTOMATIC).RecordTypeId,
            NU__Entity__c = JointProviderMembershipType.NU__Entity__c,
            //NU__Title__c = AccrualDuesUtil.BATCH_TITLE + membershipType.Name,
            NU__TransactionDate__c = TransactionDate,
            NU__Status__c = Constant.BATCH_STATUS_OPEN
        );
            
        insert automaticBatch;
        
        //batch.NU__Status__c = Constant.BATCH_STATUS_PENDING.BATCH_STATUS_PENDING;
        //update batch;
        
        return automaticBatch;
	}
	
	// create transactions
    private NU.TransactionGenerator.TransactionGenerationResult generateTransactions(NU__OrderItemLine__c orderItemLine, NU__OrderItemLine__c origOrderItemLine) {
        NU.TransactionGenerator.TransactionGenerationRequest tgReq = new NU.TransactionGenerator.TransactionGenerationRequest();
        NU.TransactionGenerator.TransactionGenerationResult tgr = new NU.TransactionGenerator.TransactionGenerationResult();
        
        tgReq.orderItemLines.add(orderItemLine);
        tgReq.batchId = BatchId;
        tgReq.transactionDate = TransactionDate;
        
        if (origOrderItemLine != null){
        	tgReq.previousOrderItemLines.add(origOrderItemLine);
        }
        
        system.debug('   generateTransactions::tgReq ' + tgReq);
        
        tgr = new NU.TransactionGenerator().generate(tgReq);
        
        return tgr;
    }
}