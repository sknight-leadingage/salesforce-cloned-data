public with sharing class EduConfAdmin_Controller extends EduReportsControllerBase {
    public override string getPageTitle() {
        if (RTypeSel == null) {
            return 'Call for Session (Conference Admin)';
        }
        else {
            return RTypeSel;
        }
    }
    
    public String[] getTDs() {
        string sBlock = '';
        for(integer i=0;i<17;i++) {
            sBlock += '<td bgcolor="#DBDBDB">&nbsp;</td>';
        }
        return new String[]{sBlock,sBlock,sBlock,sBlock};
    }
    
    public string getLineBreak() {
        return '\n';
    }
    
    public string SORT_SUBMIT_DATE = 'Submit Date';
    public string SORT_NAME = 'Name';
    private string msrt{get;set;}
    public string getMainSort() {
        msrt = ApexPages.CurrentPage().getParameters().get('msrt');
        if (msrt == null) {
            msrt = SORT_SUBMIT_DATE;
        }
        return msrt;
    }
    public string getSubmitSortLink() {
        return '/apex/EduConfAdmin?eventId=' + EventId + '&msrt=' + SORT_SUBMIT_DATE;
    }
    public string getNameSortLink() {
        return '/apex/EduConfAdmin?eventId=' + EventId + '&msrt=' + SORT_NAME;
    }

    
    public string CADMIN_TOTALS = 'Total Submissions by Education Tracks';
    public string CADMIN_DOC = 'View Document Format';
    public string CADMIN_TBL = 'View Table Format';

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(CADMIN_TOTALS, CADMIN_TOTALS));
        options.add(new SelectOption(CADMIN_DOC, CADMIN_DOC));
        options.add(new SelectOption(CADMIN_TBL, CADMIN_TBL));
        return options; 
    }
    
    transient List<Conference_Proposal__c> ProposalsHolder { get; set; }
    
    public List<Conference_Proposal__c> getProposals() {
        if (ProposalsHolder == null || ProposalsHolder.size() == 0) {
            SetQuery();
        }
        return ProposalsHolder;
    }
    
/*    public override void SetConRefreshData() {
        ProposalsHolder = setCon.getRecords();
    }
    */
    public List<AggregateResult> TrackTotals = new List<AggregateResult>();
    public Map<String, String> TrackTotalsMap { get; set; }
    
    public integer getTotalProposals() {
        if (ProposalsHolder != null) {
             return ProposalsHolder.size();
        }
        else {
            return 0;
        }
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == CADMIN_TOTALS) {
            WizardStep = 2;
        } else if (RTypeSel == CADMIN_DOC) {
            WizardStep = 3;
        } else if (RTypeSel == CADMIN_TBL) {
            WizardStep = 4;
        }
        SetQuery();
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
        SetQuery();
    }
    // ******* END: Wizard Setup Code *********
    
    public EduConfAdmin_Controller() {
        RptRenderAs = '';
        PageCType = '';
        iWizardMax = 4;
        WizardStep = 1;
        SetConPageSize = 40;
        SetQuery();
    }
    
    public void SetQuery() {
        string QueryBase = '';
    
        if (RTypeSel == null || RTypeSel == '') {
            QueryBase = 'SELECT Proposal_Number__c, CreatedDate, Title__c, Conference_Track__r.Name, Submitter__r.Company_Name_Rollup__c, Submitter__r.Email__c, Submitter__r.Full_Name__c, Conference__r.NU__ShortName__c FROM Conference_Proposal__c WHERE Conference__c = :EventId';
            if (getMainSort() == SORT_SUBMIT_DATE) {
                QueryBase += ' ORDER BY CreatedDate';
            }
            else if (getMainSort() == SORT_NAME) {
                QueryBase += ' ORDER BY Submitter__r.Full_Name__c';
            }
            
            ProposalsHolder = database.Query(QueryBase);
            QueryBase = '';
        }
        if (RTypeSel == CADMIN_TOTALS) {
            QueryBase = '';
            
            TrackTotals = [SELECT Conference_Track__r.Name QName, COUNT(Conference_Track__c) QTotal FROM Conference_Proposal__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != null  GROUP BY Conference_Track__r.Name ORDER BY Conference_Track__r.Name];
            
            TrackTotalsMap = new Map<String,String>();
            TrackTotalsMap.clear();
            for (AggregateResult ar: TrackTotals)  {  
                TrackTotalsMap.put(String.valueOf(ar.get('QName')),String.valueOf(ar.get('QTotal')));
            }
        } else if (RTypeSel == CADMIN_DOC) {
            QueryBase = 'SELECT Abstract__c,QF_Explanation__c,CreatedDate,Education_Level__c,International__c,Learning_Objective_1__c,Learning_Objective_2__c,Learning_Objective_3__c,Presentation_Structure__c,Proposal_Number__c,Session_Length__c,Technology_Use__c,Title__c,Aging_Services_Continuum__r.Name,Conference_Track__r.Name, Conference__r.NU__ShortName__c, (SELECT Speaker__r.Id,Speaker__r.Academic_Institution_Year__c,Speaker__r.Academic_Institution__c,Speaker__r.Address_1__c,Speaker__r.City__c,Speaker__r.Company_Name_Rollup__c,Speaker__r.Company__c,Speaker__r.Country__c,Speaker__r.Email__c,Speaker__r.First_Name__c,Speaker__r.Full_Name__c,Speaker__r.Highest_Degree_Level__c,Speaker__r.Knowledge_and_Professional_Experience__c,Speaker__r.Last_Name__c,Speaker__r.Major_Discipline__c,Speaker__r.Phone__c,Speaker__r.Postal_Code__c,Speaker__r.State__c,Speaker__r.Title__c FROM Conference_Proposal_Speakers__r ORDER BY Speaker__r.CreatedDate) FROM Conference_Proposal__c WHERE Conference__c = :EventId ORDER BY Proposal_Number__c';
        } else if (RTypeSel == CADMIN_TBL) {
            QueryBase = 'SELECT Abstract__c,QF_Explanation__c,CreatedDate,Education_Level__c,International__c,Learning_Objective_1__c,Learning_Objective_2__c,Learning_Objective_3__c,Presentation_Structure__c,Proposal_Number__c,Session_Length__c,Technology_Use__c,Title__c,Aging_Services_Continuum__r.Name,Conference_Track__r.Name, Conference__r.NU__ShortName__c, (SELECT Speaker__r.Id,Speaker__r.Academic_Institution_Year__c,Speaker__r.Academic_Institution__c,Speaker__r.Address_1__c,Speaker__r.City__c,Speaker__r.Company_Name_Rollup__c,Speaker__r.Company__c,Speaker__r.Country__c,Speaker__r.Email__c,Speaker__r.First_Name__c,Speaker__r.Full_Name__c,Speaker__r.Highest_Degree_Level__c,Speaker__r.Knowledge_and_Professional_Experience__c,Speaker__r.Last_Name__c,Speaker__r.Major_Discipline__c,Speaker__r.Phone__c,Speaker__r.Postal_Code__c,Speaker__r.State__c,Speaker__r.Title__c FROM Conference_Proposal_Speakers__r ORDER BY Speaker__r.CreatedDate) FROM Conference_Proposal__c WHERE Conference__c = :EventId ORDER BY Proposal_Number__c';
        }

        SetConQuery = QueryBase;
        if (SetConQuery != '') {
            ProposalsHolder = database.Query(SetConQuery);
            QueryBase = '';
/*
            ResetCon();
            setCon.setPageSize(SetConPageSize);
            setCon.first();
            SetConRefreshData();
*/
        }
    }
}