/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
public class TestBatchUpdateCurrentYearAwardedPoints
{
    static final Integer YEAR = Date.today().year();

    public static testMethod void testBatch() 
    {
      //create entity, gl, product record types, event, product
      NU__Entity__c entity = NU.DataFactoryEntity.insertEntity();
      NU__GLAccount__c gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);
      Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
      List <Account> accounts = new List<Account>();
      NU__Event__c evt = NU.DataFactoryEvent.insertEvent(entity.Id);
      NU__Product__c prod = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_EXHIBITOR, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_EXHIBITOR);
      
      List<NU__Membership__c> membershipList = new List<NU__Membership__c>();
      List<Exhibitor__c> exhs = new List<Exhibitor__c>();
      
      evt.Current_Year_Annual_Event__c = true;
      update evt;
       
        //create 3 new accounts
      for(integer i = 0; i < 3; i++)
      {
            Account a = new Account(Name = string.valueOf(i), Current_Year_Priority_Points__c = 0);  
            accounts.add(a);
      }
       
      insert accounts;
      
      //set up for memberships
      NU__MembershipType__c membershipType = NU.DataFactoryMembershipType.insertDefaultMembershipType(entity.Id);
      
      //loop through all accounts
      for(Account a :accounts)
      {
          if(a.Name == '2')
              break;
              
          //create a membership record for first two accounts
          NU__Membership__c membership = NU.DataFactoryMembership.createMembership(a.Id, membershipType.Id, Date.newInstance(year, 1, 1), Date.newInstance(year, 12, 31));
          membershipList.add(membership);

          //create an exhibitor record for first two accounts
          Exhibitor__c exh = new Exhibitor__c(Account__c = a.Id, Event__c = evt.Id, External_ID__c = a.Id, Product__c = prod.Id);
          exhs.add(exh);
      }
      
      insert membershipList;
      insert exhs;
       
      Test.StartTest();
      ID batchprocessid = Database.executeBatch(new BatchUpdateCurrentYearAwardedPoints());
      Test.StopTest();

      List<Account> aList = [SELECT NU__Member__c, Provider_Member__c, NU__Membership__c, NU__LapsedOn__c, Current_Year_Priority_Points__c FROM Account];
      System.debug('listsize' + aList.size());
      for (Account a : aList)
      {
        System.debug ('point' + a.Current_Year_Priority_Points__c + 'isMember' + a.NU__Member__c + '/' + a.Provider_Member__c + '/' + a.NU__Membership__c + '/' + a.NU__LapsedOn__c);
      }
    
      System.AssertEquals(2, database.countquery('SELECT COUNT() FROM Account WHERE Current_Year_Priority_Points__c = 1'));  
       
   }
}