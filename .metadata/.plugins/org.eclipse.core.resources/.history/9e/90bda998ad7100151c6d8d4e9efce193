<apex:page controller="ConferenceSpeakersHubController">
<script>

function setFocusOnLoad() {}

</script>

    <style>
        .pbSubheader {
            color: black !important; 
        }
    </style>

    <apex:pageMessages />
    
    <apex:pageMessage summary="There are no speakers for the {! Conference.Name } conference."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && hasSpeakerSearchCriteria == false }" />
    
    <apex:pageMessage summary="No speakers found matching your search criteria."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && hasSpeakerSearchCriteria }" />
    
    <apex:form >
        <apex:pageBlock title="Speakers">
            <div style="clear: both; height: 50px;">
                <apex:panelGrid columns="5" rendered="{! ResultSize > 0 }" style="float: left">
                    <apex:commandLink action="{!first}" rendered="{! ResultSize > 1 && PageNumber != 1 }">First</apex:commandlink>
                    <apex:commandLink action="{!previous}" rendered="{!hasPrevious}">Previous</apex:commandlink>
                    <apex:outputText value="Record {! PageNumber } of {! ResultSize }" />
                    <apex:commandLink action="{!next}" rendered="{!hasNext}">Next</apex:commandlink>
                    <apex:commandLink action="{!last}" rendered="{! ResultSize > 1 && PageNumber != ResultSize }">Last</apex:commandlink>
                </apex:panelGrid>
            
           
                <div style="float: right;">
                    <c:ConferenceNavComponent eventId="{! EventId }" />
                </div>
            </div>
        
            <apex:pageBlockSection title="Speaker Information" rendered="{! ResultSize > 0 }">

                <apex:outputField value="{! CurrentSpeaker.NU__FullName__c}" />
                <apex:outputField value="{! CurrentSpeaker.PersonTitle}" />
                <apex:outputField value="{! CurrentSpeaker.Speaker_Company_Name_Rollup__c}" />
                <apex:outputField value="{! CurrentSpeaker.Speaker_Full_Address__c}" />
                <apex:outputField value="{! CurrentSpeaker.Phone}" />
                <apex:outputField value="{! CurrentSpeaker.NU__PersonEmail__c}" />
                <apex:outputField value="{! CurrentSpeaker.LeadingAge_Id__c }" />
                <apex:outputField value="{! CurrentSpeaker.isLeadingAgeMember__c }" />

            </apex:pageBlockSection>

            <apex:pageBlockSection title="Academic and Professional Background" rendered="{! ResultSize > 0 }">

                <apex:outputField value="{! CurrentSpeaker.Academic_Institution__c}" />
                <apex:outputField value="{! CurrentSpeaker.Academic_Institution_Year__c}" />
                <apex:outputField value="{! CurrentSpeaker.Highest_Degree_Level__c}" />
                <apex:outputField value="{! CurrentSpeaker.Knowledge_and_Professional_Experience__c}" />
                <apex:outputField value="{! CurrentSpeaker.Major_Discipline__c}" />
                <apex:outputField value="{! CurrentSpeaker.No_Degree__c}" />
                <apex:outputField value="{! CurrentSpeaker.Speaker_Bio_Modified__c}" />
                <apex:outputField value="{! CurrentSpeaker.Speaker_Bio_Validated__c}" />

            </apex:pageBlockSection>

            <apex:pageBlockSection title="{! Conference.Name } Session Info"
                                   columns="1"
                                   rendered="{! ResultSize > 0 }">
                <apex:outputText value="This speaker currently is not speaking at this conference." 
                                 rendered="{! CurrentSpeakerEventSessions.size == 0 }" />
            
                <apex:pageBlockTable value="{! CurrentSpeakerEventSessions }"
                                     var="eventSession"
                                     rendered="{! CurrentSpeakerEventSessions.size > 0 }">
                                     
                    <apex:column value="{! eventSession.Session__r.Session_Title_Full__c }" />
                    <apex:column value="{! eventSession.Role__c }" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            
        </apex:pageBlock>
        
        <apex:pageBlock title="Search">
            <apex:pageBlockSection title="Speaker Search">
                <apex:inputText value="{!FirstNameToSearch}" label="First Name" />
                <apex:inputText value="{!LastNameToSearch}" label="Last Name" />
                <apex:inputText value="{!CompanyToSearch}" label="Company" />
                <apex:inputText value="{!ZipCodeToSearch}" label="Zip Code" />
                <apex:inputText value="{!PhoneToSearch}" label="Phone" />
                <apex:inputText value="{!LeadingAgeIDToSearch}" label="LeadingAge ID" />
                
                <apex:pageBlockSectionItem />
                
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:commandButton value="Search" action="{! searchForSpeakers }" />
                        <apex:commandButton value="Clear Search"
                                            action="{! clearSearch }"
                                            style="margin-left: 10px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>