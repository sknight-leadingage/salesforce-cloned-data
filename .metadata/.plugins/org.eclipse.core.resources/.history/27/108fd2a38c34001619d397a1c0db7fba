public with sharing class EduSessionsSessionListing_Controller extends EduReportsControllerBase {
    public override string getPageTitle() {
        if (RTypeSel == null) {
            return 'Session Listing';
        }
        else {
            return RTypeSel;
        }
    }
    
    public string SLIST_SNUMBER = 'Session Listing (By Session #)';
    public string SLIST_STIMESLOT = 'Session Listing (By Timeslot)';
    public string SLIST_STRACK = 'Session Listing (By Track)';
    public string SLIST_PTIMESLOT = 'Session Listing w/Proposal # (By Timeslot)';
    public string SLIST_PTRACK = 'Session Listing w/Proposal # (By Track)';
    public string SLIST_PSTAFF = 'Session Listing w/Proposal # (By Edu Staff)';

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(SLIST_SNUMBER, SLIST_SNUMBER));
        options.add(new SelectOption(SLIST_STIMESLOT, SLIST_STIMESLOT));
        options.add(new SelectOption(SLIST_STRACK, SLIST_STRACK));
        options.add(new SelectOption(SLIST_PTIMESLOT, SLIST_PTIMESLOT));
        options.add(new SelectOption(SLIST_PTRACK, SLIST_PTRACK));
        options.add(new SelectOption(SLIST_PSTAFF, SLIST_PSTAFF));
        return options; 
    }
    
    public List<Conference_Session__c> SessionInfo { get; set; }
    public integer SessionInfoSize {
        get {
            if (SessionInfo == null) {
                return 0;
            } else {
                return SessionInfo.size();
            }
        }
    }
    
    public List<Conference_Proposal__c> ProposalInfo { get; set; }
    public integer ProposalInfoSize {
        get {
            if (ProposalInfo == null) {
                return 0;
            } else {
                return ProposalInfo.size();
            }
        }
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == SLIST_SNUMBER) {
            WizardStep = 2;
        } else if (RTypeSel == SLIST_STIMESLOT) {
            WizardStep = 3;
        } else if (RTypeSel == SLIST_STRACK) {
            WizardStep = 4;
        } else if (RTypeSel == SLIST_PTIMESLOT) {
            WizardStep = 5;
        } else if (RTypeSel == SLIST_PTRACK) {
            WizardStep = 6;
        } else if (RTypeSel == SLIST_PSTAFF) {
            WizardStep = 7;
        }
        SetQuery();
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    
    public EduSessionsSessionListing_Controller(){
        iWizardMax = 7;
        WizardStep = 1;
    }
    
    public void SetQuery() {
        GroupHeaderController.SecondGroupTotals = null;
        if (RTypeSel == SLIST_SNUMBER) {
            GroupHeaderController.GroupTotals = null;
            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c];
        } else if (RTypeSel == SLIST_STIMESLOT) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Timeslot__r.Timeslot_Code__c FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Timeslot__r.Timeslot_Code__c];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('Timeslot_Code__c'), (integer)ar.get('sc'));
            }
            SessionInfo = [SELECT Session_Number__c, Conference_Track__r.Name, Timeslot__r.Timeslot_Code__c, Timeslot__r.Timeslot__c, Title__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Conference_Track__r.Name, Session_Number_Numeric__c];
        } else if (RTypeSel == SLIST_STRACK) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Conference_Track__r.Name afn FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Conference_Track__r.Name ORDER BY Conference_Track__r.Name];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('afn'), (integer)ar.get('sc'));
            }
            SessionInfo = [SELECT Session_Number__c, Title__c, Timeslot__r.Timeslot__c, Timeslot__r.Timeslot_Code__c, Conference_Track__r.Name FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Conference_Track__r.Name,  Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c];
        } else if (RTypeSel == SLIST_PTIMESLOT) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Timeslot__r.Timeslot_Code__c FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Timeslot__r.Timeslot_Code__c ORDER BY Timeslot__r.Timeslot_Code__c];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('Timeslot_Code__c'), (integer)ar.get('sc'));
            }
            //ProposalInfo = [SELECT Session__r.Session_Number__c, Session__r.Conference_Track__r.Name, Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Timeslot__r.Timeslot__c, Session__r.Title__c, Proposal_Number__c FROM Conference_Proposal__c WHERE Session__r.Conference__c = :EventId AND Session__c != null ORDER BY Session__r.Timeslot__r.Timeslot_Code__c, Session__r.Conference_Track__r.Name, Session__r.Session_Number_Numeric__c];
            SessionInfo = [SELECT Session_Number__c, Proposal__r.Proposal_Number__c, Conference_Track__r.Name, Timeslot__r.Timeslot_Code__c, Timeslot__r.Timeslot__c, Title__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Conference_Track__r.Name, Session_Number_Numeric__c];
        } else if (RTypeSel == SLIST_PTRACK) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Conference_Track__r.Name afn FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Conference_Track__r.Name ORDER BY Conference_Track__r.Name];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('afn'), (integer)ar.get('sc'));
            }
            SessionInfo = [SELECT Session_Number__c, Proposal__r.Proposal_Number__c, Title__c, Timeslot__r.Timeslot__c, Timeslot__r.Timeslot_Code__c, Conference_Track__r.Name FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Conference_Track__r.Name,  Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c];
        } else if (RTypeSel == SLIST_PSTAFF) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Education_Staff__r.Name afn FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Education_Staff__r.Name];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('afn'), (integer)ar.get('sc'));
            }
            GroupHeaderController.SecondGroupTotals = new Map<string,integer>();
            List<AggregateResult> STotals = [SELECT COUNT(Id) sc, Conference_Track__r.Name tfn, Education_Staff__r.Name sfn FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Conference_Track__r.Name, Education_Staff__r.Name];
            for (AggregateResult ar : STotals ) {
                GroupHeaderController.SecondGroupTotals.put((string)ar.get('tfn') + (string)ar.get('sfn'), (integer)ar.get('sc'));
            }
            SessionInfo = [SELECT Education_Staff__r.Name, Proposal__r.Proposal_Number__c, Conference_Track__r.Name, Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Education_Staff__r.Name, Conference_Track__r.Name, Session_Number_Numeric__c];
        }
    }
}