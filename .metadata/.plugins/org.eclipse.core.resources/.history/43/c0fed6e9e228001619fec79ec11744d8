/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceSessionsController {

    static Conference_Session__c session = null;
    
    static ConferenceSessionsController loadController(){
        PageReference pageRef = Page.ConferenceSessions;
        Test.setCurrentPage(pageRef);
        
        session = DataFactoryConferenceSession.insertConferenceSession();
        
        pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, session.Conference__c);
        
        ConferenceSessionsController sessionsController = new ConferenceSessionsController();
        
        return sessionsController;
    }

    static testMethod void currentSessionTest() {
        ConferenceSessionsController sessionsController = loadController();
        
        Conference_Session__c currentSession = sessionsController.CurrentSession;
        
        system.assert(currentSession != null);
        system.assert(currentSession.Id == session.Id);
    }
    
    static testmethod void CurrentSessionProposalNullTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.CurrentSessionProposal == null);
    }
    
    static testmethod void searchTitleTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.SearchCriteria.Title = session.Title__c;
        
        sessionsController.search();
        system.assert(sessionsController.CurrentSession != null);
    }
    
    static testmethod void clearSearchTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.SearchCriteria.Title = session.Title__c;
        
        system.assert(sessionsController.HasSearchCriteria == true);
        
        sessionsController.clearSearch();
        
        system.assert(sessionsController.HasSearchCriteria == false);
    }
    
    static testmethod void lockTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.lock();
        
        system.assertEquals(true, sessionsController.CurrentSession.Locked__c);
    }
    
    static testmethod void unlockTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.lock();
        sessionsController.unlock();
        
        system.assertEquals(false, sessionsController.CurrentSession.Locked__c);
    }
    
    static testmethod void ConferenceTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.Conference != null);
    }
    
    static testmethod void hasNextTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.hasNext == false);
    }
    
    static testmethod void hasPreviousTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.hasPrevious == false);
    }
    
    static testmethod void ResultSizeTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.ResultSize > 0);
    }
    
    static testmethod void firstTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.first();
    }
    
    static testmethod void lastTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.last();
    }
    
    static testmethod void previousTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.previous();
    }
    
    static testmethod void nextTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.next();
    }
    
    static testmethod void cancelTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.cancel();
    }
    
    static testmethod void deleteTest(){
        ConferenceSessionsController sessionsController = loadController();
        sessionsController.deleteCurrentConferenceObject();
        
        List<Conference_Session__c> sessions = [select id from Conference_Session__c where id = :session.Id];
        system.assert(sessions.size() == 0); 
    }
    
    static testmethod void SessionHistoryTest(){
        ConferenceSessionsController sessionsController = loadController();
        
        system.assert(sessionsController.SessionHistory.size() == 0);
    }
    
    static testmethod void noDivisionsTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.DivisionSelectOpts.size() == 0);
    }
    
    static testmethod void noConferenceTimeSlotSelectOptsTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.ConferenceTimeslotSelectOpts.size() == 0);
    }

    static testmethod void oneCurrentSessionSpeakersTest(){
        ConferenceSessionsController sessionsController = loadController();
        system.assert(sessionsController.CurrentSessionSpeakers.size() == 1);
    }
}