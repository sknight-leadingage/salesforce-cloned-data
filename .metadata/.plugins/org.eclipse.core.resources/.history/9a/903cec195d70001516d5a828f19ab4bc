/**
 * @author NimbleUser
 * @date Updated: 3/22/13
 * @description This class provides creates Conference Session and Conference Session Speaker
 * records from the given Conference Proposal Records.
 */
public without sharing class ConferenceProposalToSessionConverter {
    public static final String NO_CONFERENCE_PROPOSAL_IDS_VAL_MSG = 'There are no conference proposal ids.';
    public static final String NO_UNACCEPTED_CONFERENCE_PROPOSALS_ALLOWED_VAL_MSG = 'Only Accepted & Maybe proposals can be made into sessions.';
    public static final String NO_CONFERENCE_PROPOSALS_FOUND_VAL_MSG = 'No conference proposal found with the given ids.';
    public static final String ALREADY_CONVERTED_PROPOSALS_FOUND_VAL_MSG = 'One or more proposals already have sessions.';
    public static final String ALL_SPEAKERS_MUST_HAVE_ACCOUNTS_VAL_MSG = 'Every speaker record must be associated with an account.';
    
    
    private List<Conference_Proposal__c> proposalsToConvert { get; set; }

    public ConferenceProposalToSessionConverter(Set<Id> conferenceProposalIds){
        if (NU.CollectionUtil.setHasElements(conferenceProposalIds) == false){
            throw new ApplicationException(NO_CONFERENCE_PROPOSAL_IDS_VAL_MSG);
        }
        
        system.debug('    conferenceProposalIds ' + conferenceProposalIds);
        
        proposalsToConvert = ConferenceProposalQuerier.getConferenceProposalsByIds(conferenceProposalIds);
        
        if (NU.CollectionUtil.listHasElements(proposalsToConvert) == false){
            throw new ApplicationException(NO_CONFERENCE_PROPOSALS_FOUND_VAL_MSG);
        }
        
        List<Conference_Proposal__c> unAcceptedProposals = new List<Conference_Proposal__c>();
        List<Conference_Proposal__c> alreadyConvertedProposals = new List<Conference_Proposal__c>();
        List<Conference_Proposal_Speaker__c> speakersWithoutAccounts = new List<Conference_Proposal_Speaker__c>();
        
        for (Conference_Proposal__c proposal : proposalsToConvert){
            if (isUnAcceptedProposal(proposal)){
                unAcceptedProposals.add(proposal);
            }
            
            if (isAlreadyConvertedProposal(proposal)){
                system.debug('   proposal is already converted. ' + proposal);
                
                alreadyConvertedProposals.add(proposal);
            }
            
            for (Conference_Proposal_Speaker__c propSpeaker : proposal.Conference_Proposal_Speakers__r){
                if (isSpeakerWithoutAccount(propSpeaker)){
                    speakersWithoutAccounts.add(propSpeaker);
                }
            }
        }
        
        if (NU.CollectionUtil.listHasElements(unAcceptedProposals)){
            throw new ApplicationException(NO_UNACCEPTED_CONFERENCE_PROPOSALS_ALLOWED_VAL_MSG);
        }
        
        if (NU.CollectionUtil.listHasElements(alreadyConvertedProposals)){
            system.debug('   alreadyConvertedProposals' + alreadyConvertedProposals);
            
            throw new ApplicationException(ALREADY_CONVERTED_PROPOSALS_FOUND_VAL_MSG);
        }
        
        if (NU.CollectionUtil.listHasElements(speakersWithoutAccounts)){
            throw new ApplicationException(ALL_SPEAKERS_MUST_HAVE_ACCOUNTS_VAL_MSG);
        }
    }
    
    public List<Conference_Session__c> convert(){
        List<Conference_Session__c> sessions = new List<Conference_Session__c>();
        List<Conference_Session_Speaker__c> sessionSpeakers = new List<Conference_Session_Speaker__c>();
        List<SessionContainer> sessionContainers = new List<SessionContainer>();
        
        for (Conference_Proposal__c proposal : proposalsToConvert){
            SessionContainer sc = new SessionContainer();
            
            Conference_Session__c convertedSession = createConvertedSession(proposal);
            sc.Session = convertedSession;
            sc.proposal = proposal;
            
            sessions.add(convertedSession);
            
            for (Conference_Proposal_Speaker__c propSpeaker : proposal.Conference_Proposal_Speakers__r){
                Conference_Session_Speaker__c sessionSpeaker = createConvertedSessionSpeaker(propSpeaker, proposal);
                
                sc.sessionSpeakers.add(sessionSpeaker);
                sessionSpeakers.add(sessionSpeaker);
            }
            
            sessionContainers.add(sc);
        }
        
        insert sessions;
        
        updateRecords(sessionContainers);
        
        insert sessionSpeakers;
        update proposalsToConvert;
        
        return sessions;
    }
    
    private Boolean isUnAcceptedProposal(Conference_Proposal__c confProposal){
        return confProposal != null &&
               confProposal.Status__c != Constant.CONFERENCE_PROPOSAL_STATUS_ACCEPTED && confProposal.Status__c != Constant.CONFERENCE_PROPOSAL_STATUS_MAYBE;
    }
    
    private Boolean isAlreadyConvertedProposal(Conference_Proposal__c confProposal){
        system.debug('   isAlreadyConvertedProposal::confProposal ' + confProposal);
        
        return confProposal != null &&
               confProposal.Session__c != null;
    }
    
    private Boolean isSpeakerWithoutAccount(Conference_Proposal_Speaker__c proposalSpeaker){
        return proposalSpeaker != null &&
               proposalSpeaker.Speaker__r.Account__c == null;
    }
    
    private Conference_Session__c createConvertedSession(Conference_Proposal__c proposal){
        return new Conference_Session__c(
            Abstract__c = proposal.Abstract__c,
            Conference__c = proposal.Conference__c,
            Conference_Track__c = proposal.Conference_Track__c,
            Expected_Attendance__c = proposal.Expected_Attendance__c,
            International__c = proposal.International__c,
            Learning_Objective_1__c = proposal.Learning_Objective_1__c,
            Learning_Objective_2__c = proposal.Learning_Objective_2__c,
            Learning_Objective_3__c = proposal.Learning_Objective_3__c,
            Notes__c = proposal.Notes__c,
            Sponsorship_Level__c = proposal.Sponsorship_Level__c,
            Submitter__c = proposal.Submitter__r.Account__c,
            Title__c = proposal.Title__c,
            Proposal__c = proposal.Id
        );
    }
    
    private Conference_Session_Speaker__c createConvertedSessionSpeaker(Conference_Proposal_Speaker__c proposalSpeaker, Conference_Proposal__c proposal){
        Conference_Session_Speaker__c csp = new Conference_Session_Speaker__c(
            Speaker__c = proposalSpeaker.Speaker__r.Account__c,
            Role__c = Constant.CONFERENCE_SPEAKER_ROLE_SPEAKER
        );
        
        if (proposalSpeaker.Speaker__r.Account__c == proposal.Submitter__r.Account__c) {
        	csp.Role__c = Constant.CONFERENCE_SPEAKER_ROLE_KEY_CONTACT;
        }
        
        return csp;
    }
    
    private void updateRecords(List<SessionContainer> sessionContainers){
        for (SessionContainer sc : sessionContainers){
            sc.proposal.Session__c = sc.session.Id;
            
            for (Conference_Session_Speaker__c ss : sc.sessionSpeakers){
                ss.Session__c = sc.session.Id;
            }
        }
    }
    
    private class SessionContainer {
        public Conference_Proposal__c proposal { get; set; }
        
        public Conference_Session__c session { get; set; }
        
        public List<Conference_Session_Speaker__c> sessionSpeakers = new List<Conference_Session_Speaker__c>();
    }
}