<apex:component access="global" controller="ViewARStatementController">

    <apex:attribute name="accts" description="accounts" type="account[]" assignTo="{!Accounts}"/>
    <apex:attribute name="attn" description="Attention to" type="account" assignTo="{!AttentiontoinComponent}"/>

    <div style="font-family:verdana,arial,sans-serif;font-size:10px; margin-top: 5px;">
    
        <apex:repeat value="{!AccountWrappers}" var="aw">
    
            <h1 style="float:right;padding-right:10px; padding-top: 0px; margin-top: 0px;">STATEMENT OF ACCOUNT</h1>
            
            <div style="padding:0px 10px">
            
                <apex:outputPanel rendered="{!!ISBLANK(Entity.NU__LogoURL__c)}" layout="none">
                        <apex:image value="{!Entity.NU__LogoURL__c}" style="display:inline;vertical-align:middle;float:left;padding-right:10px" />
                </apex:outputPanel>

                <div style="float:left">
                    {!Entity.Name}<br/>
                    {!Entity.NU__Street__c}<br/>
                    {!Entity.NU__City__c}, {!Entity.NU__State__c} {!Entity.NU__PostalCode__c}<br/>
                    {!Entity.NU__Country__c}<br/>
                    
                    <apex:outputPanel rendered="{!!ISBLANK(Entity.NU__Phone__c)}">
                    {!Entity.NU__Phone__c}<br/>
                    </apex:outputPanel>
                    
                    <apex:outputPanel rendered="{!!ISBLANK(Entity.NU__Website__c)}">
                    {!Entity.NU__Website__c}<br/>
                    </apex:outputPanel>
                </div>

                <div style="clear:both"/>

                <table style="float:right;border-collapse:collapse;" cellspacing="0" cellpadding="5">
                    <tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">Account Number</td>
                        <td style="border:thin solid black"><apex:outputText value="{!aw.accountNumber}" /></td>
                    </tr>
                    <tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">Statement Date</td>
                        <td style="border:thin solid black"><apex:outputText value="{0,date,M/d/yyyy}"><apex:param value="{!InvoiceDate}"/></apex:outputText></td>
                    </tr>
                </table>
                
                <div style="margin:40px 0px; padding-left: 100px;">
                    <b>Billing Address</b><br/>
                    <apex:outputText rendered="{! IsBlank(AttentiontoinComponent.Name) = false}" escape="false">
                     Attn: {!AttentiontoinComponent.Name} <br />
                    </apex:outputText>
                    {!aw.Account.Name}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(aw.Account.Name)}"/>
                    {!aw.Account.BillingStreet}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(aw.Account.BillingStreet)}"/>
                    {!aw.Account.BillingCity}
                    <apex:outputText value=", " rendered="{!!ISBLANK(aw.Account.BillingCity) && (!ISBLANK(aw.Account.BillingState) || ISBLANK(aw.Account.BillingPostalCode))}"/>
                    {!aw.Account.BillingState} {!aw.Account.BillingPostalCode}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(aw.Account.BillingCity) || !ISBLANK(aw.Account.BillingState) || !ISBLANK(aw.Account.BillingPostalCode)}"/>
                    {!aw.Account.BillingCountry}
                </div>

                <div style="clear:both"/>
                
                <p style="text-align:right;">Amount Enclosed: $_____________________</p>

                <apex:outputPanel >
                    <table style="border-collapse:collapse;width:100%;margin-top:20px" cellspacing="0" cellpadding="5">
                        <tr>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Date</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Invoice #</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Description</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Amount</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Credits</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Balance</td>
                        </tr>
                        
                        <apex:repeat var="o" value="{!aw.Orders}">
                            <tr>
                                <td style="border-left:thin solid black;border-right: thin solid black;border-top:thin solid black;font-weight:bold;vertical-align:top;"><apex:outputText value="{0,date,M/d/yyyy}"><apex:param value="{!o.NU__TransactionDate__c}"/></apex:outputText></td>
                                <td style="border-left:thin solid black;border-right: thin solid black;border-top:thin solid black;font-weight:bold;vertical-align:top;"><apex:outputText value="{!o.NU__InvoiceNumber__c}" /></td>
                                <td style="border-left:thin solid black;border-right: thin solid black;border-top:thin solid black;vertical-align:top;">
                                    <apex:outputText value="{!o.NU__InvoiceDescription__c}" />
                                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(o.NU__InvoiceDescription__c)}" />
                                    <apex:repeat var="oi" value="{!aw.OrderItemsMap[o.Id]}">
                                        <span style="font-weight:bold;"><apex:outputText value="{!oi.NU__Order__r.NU__BillTo__r.Name}" rendered="{!!ISBLANK(oi.NU__Order__r.NU__BillTo__c)}" /> -</span>
                                        <span style="font-weight:bold;"><apex:outputText value="{!oi.RecordType.Name}" /> Products:</span>
                                        <apex:repeat var="oil" value="{!oi.NU__OrderItemLines__r}">
                                            <br /><span style="padding-left: 25px;"><apex:outputText value="{!oil.NU__Product__r.Name}" /></span>
                                        </apex:repeat>
                                        <!-- <strong><apex:outputText value="{!oi.RecordType.Name}" /> Total:</strong> $<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oi.NU__GrandTotal__c}" /></apex:outputText><br />
                                        <strong>Payments Applied:</strong> $<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oi.NU__TotalPayment__c}" /></apex:outputText><br />
                                        <strong>Balance Remaining:</strong> $<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oi.NU__Balance__c}" /></apex:outputText>-->
                                    </apex:repeat>
                                </td>
                                <td style="border-left:thin solid black;border-right: thin solid black;border-top:thin solid black;text-align:right;font-weight:bold;vertical-align:top;">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!o.NU__GrandTotal__c}" /></apex:outputText></td>
                                <td style="border-left:thin solid black;border-right: thin solid black;border-top:thin solid black;text-align:right;font-weight:bold;vertical-align:top;">
                                <table style="border-collapse:collapse;width:100%" cellspacing="0" cellpadding="2"> <tr>

                                <apex:repeat var="oicr" value="{!aw.OrderItemsMap[o.Id]}">
                                        <td style="text-align:center;font-weight:bold;vertical-align:top;">Payment Date<br/>============
                                                <apex:repeat var="oilcr" value="{!oicr.NU__PaymentLines__r}">
                 
                                                        <br/>
                                                       <apex:outputText value="{0,date,M/d/yyyy}"><apex:param value="{! oilcr.NU__Payment__r.NU__PaymentDate__c }"/></apex:outputText><br />
                                                    </apex:repeat>
                                        </td>
                                        <td style="text-align:right;font-weight:bold;vertical-align:top;">Payment Amount <br/>============
                                            <apex:repeat var="oilcr" value="{!oicr.NU__PaymentLines__r}">
                                                <br/>
                                                $<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{! oilcr.NU__PaymentAmount__c }" /></apex:outputText>
                                                <br/>
        
                                            </apex:repeat>      
                                            <span style="font-weight:bold;">______________ <br/>
                                         $<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!o.NU__TotalPayment__c}" /></apex:outputText>
                                         </span>                            
                                        </td>   
                                </apex:repeat>
                                </tr>
                                </table>
                                 </td>
                                <td style="border-left:thin solid black;border-right: thin solid black;border-top:thin solid black;text-align:right;font-weight:bold;vertical-align:top;">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!o.NU__Balance__c}" /></apex:outputText></td>
                            </tr>
                            <!-- <apex:repeat var="oi" value="{!aw.OrderItemsMap[o.Id]}">
                                <tr>
                                    <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                    <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                    <td style="border-left:thin solid black;border-right: thin solid black;"><apex:outputText value="{!oi.RecordType.Name}" /> Products:</td>
                                    <td style="border-left:thin solid black;border-right: thin solid black;text-align:right">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oi.NU__GrandTotal__c}" /></apex:outputText></td>
                                    <td style="border-left:thin solid black;border-right: thin solid black;text-align:right">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oi.NU__TotalPayment__c}" /></apex:outputText></td>
                                    <td style="border-left:thin solid black;border-right: thin solid black;text-align:right">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oi.NU__Balance__c}" /></apex:outputText></td>
                                </tr>
                                <apex:repeat var="oil" value="{!oi.NU__OrderItemLines__r}">
                                    <tr>
                                        <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                        <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                        <td style="border-left:thin solid black;border-right: thin solid black;padding-left: 25px;"><apex:outputText value="{!oil.NU__Product__r.Name}" /></td>
                                        <td style="border-left:thin solid black;border-right: thin solid black;text-align:right">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!oil.NU__TotalPrice__c}" /></apex:outputText></td>
                                        <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                        <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                        <td style="border-left:thin solid black;border-right: thin solid black;">&nbsp;</td>
                                    </tr>
                                </apex:repeat>
                            </apex:repeat>-->
                        </apex:repeat>
                        
                        <tr>
                            <td colspan="3" style="border:thin solid black;">{!aw.message}</td>
                            <td colspan="3" style="border:thin solid black;font-weight:bold;text-align:right;font-size:1.5em;">Total Due: $<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!aw.balance}" /></apex:outputText></td>
                        </tr>
                    </table>
                </apex:outputPanel>
                
                <table style="border-collapse:collapse;width:100%;margin-top:20px" cellspacing="0" cellpadding="5">
                    <tr>
                        <apex:repeat var="arAging" value="{!EntityARAgings}">
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">{!arAging.Name}</td>
                        </apex:repeat>
                    </tr>
                    <tr>
                        <apex:repeat var="arAging" value="{!EntityARAgings}">
                            <td style="border:thin solid black;text-align:right">$<apex:outputText value="{0,number,#,##0.00}"><apex:param value="{!aw.arAgingBalances[arAging.Id]}" /></apex:outputText></td>
                        </apex:repeat>
                    </tr>
                </table>
            </div>
            
            <div style="page-break-after:always"/>
        
        </apex:repeat>

    </div>
</apex:component>