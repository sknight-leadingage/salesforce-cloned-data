@isTest
private class TestConvertDeal {
  
    static NU__Entity__c entity = null; 
    static NU__GLAccount__c gl = null;
    static Account billTo = null;
    static Account contact = null;
    static NU__Order__c testOrder = null;
    static NU__PriceClass__c priceClass = null;
    static NU__MembershipType__c membershipType = null;
    
    static NU__Product__c sponsorshipProduct = null;
    static NU__Product__c membershipProduct = null;
    static NU__Product__c advertisingProduct = null;
    static NU__Product__c exhibitorProduct = null;
    
    static NU__MembershipTypeProductLink__c membershipTypeProductLink = null;
    
    static NU__Deal__c sponsorshipDeal = null;
    static NU__Deal__c advertisingDeal = null;
    static NU__Deal__c membershipDeal = null;
    static NU__Deal__c exhibitorDeal = null;
    
    static DealItem__c sponsorshipDealItem = null;
    static DealItem__c membershipDealItem = null;
    static DealItem__c advertisingDealItem = null;
    static DealItem__c exhibitorDealItem = null;
    
    static Map<String, Schema.RecordTypeInfo> productRecordTypes = null;
    static Map<String, Schema.RecordTypeInfo> dealRecordTypes = null;
    
    private static void setupTest() {
        if (entity != null) return;
        
        entity = NU.DataFactoryEntity.insertEntity();
        gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);
        
        billTo = DataFactoryAccountExt.insertMultiSiteAccount();
        contact = DataFactoryAccountExt.insertIndividualAccount();
        
        priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        
        membershipType = DataFactoryMembershipTypeExt.insertProviderMembershipType(entity.Id);
        
        
        
        // setup specific for converting deals
        testOrder = NU.DataFactoryOrder.insertOrder(entity.Id, billTo.Id);
        
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo sponsorshipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        Schema.RecordTypeInfo membershipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_MEMBERSHIP);
        Schema.RecordTypeInfo advertisingDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_ADVERTISING);
        Schema.RecordTypeInfo exhibitorDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_EXHIBITOR);
        
        sponsorshipProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        membershipProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_MEMBERSHIP, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_MEMBERSHIP);
        advertisingProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_ADVERTISING, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_ADVERTISING);
        exhibitorProduct = DataFactoryProductExt.insertProduct(Constant.ORDER_RECORD_TYPE_EXHIBITOR, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_EXHIBITOR);
        
        
        membershipTypeProductLink = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(membershipType.Id, membershipProduct.Id);
        
        sponsorshipDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, sponsorshipDealRTI.getRecordTypeId());
        
        advertisingDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_ADVERTISING, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, advertisingDealRTI.getRecordTypeId());
        exhibitorDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_EXHIBITOR, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, exhibitorDealRTI.getRecordTypeId());
        
        membershipDeal = DataFactoryDeal.createDeal(Constant.ORDER_RECORD_TYPE_MEMBERSHIP, billTo.Id, contact.Id, Constant.DEAL_STATUS_READY_FOR_CONVERT, membershipDealRTI.getRecordTypeId());
        membershipDeal.NU__StartDate__c = Date.parse('1/1/2013');
        membershipDeal.NU__EndDate__c = Date.parse('12/31/2013');
        membershipDeal.Membership_Type__c = membershipType.Id;
        insert membershipDeal;
        
        sponsorshipDealItem = DataFactoryDealItem.insertDealItem(sponsorshipDeal.Id, sponsorshipProduct.Id);
        membershipDealItem = DataFactoryDealItem.insertDealItem(membershipDeal.Id, membershipProduct.Id);
        advertisingDealItem = DataFactoryDealItem.insertDealItem(advertisingDeal.Id, advertisingProduct.Id);
        exhibitorDealItem = DataFactoryDealItem.insertDealItem(exhibitorDeal.Id, exhibitorProduct.Id);
        
        exhibitorDealItem.Booked_By__c = 'Awesome Booker';
        exhibitorDealItem.Booked_Date__c = Date.today();
        exhibitorDealItem.Booth_Number__c = 'Booth 123';
        
        update exhibitorDealItem;
    }
    
    static void testConvertDealHelper(NU__Deal__c testDeal){
        PageReference pageRef = new PageReference('/apex/ConvertDeal');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('dealId',testDeal.Id);
        
        // Test #5: Sponsorship Deal
        ConvertDeal controller = new ConvertDeal(new ApexPages.StandardController(new NU__Deal__c()));
        controller.redirect();
        
        assert(testDeal);
    }
    
    // test methods
    static testMethod void testConvertDeal_NoValidDealID() {
        setupTest();

        PageReference pageRef = new PageReference('/apex/ConvertDeal');
        Test.setCurrentPage(pageRef);
        
        // Test #1: No Valid Deal ID    
        ApexPages.currentPage().getParameters().put('dealId','adsfsdf');
        ConvertDeal controller = new ConvertDeal(new ApexPages.StandardController(new NU__Deal__c()));
        controller.redirect();
        
        System.assert(ApexPages.getMessages().size() > 0 && ApexPages.getMessages().get(0).getDetail().contains('No deal'));
    }
    
    static testMethod void testConvertDeal_IncorrectStatus() {  
        setupTest();
        
        PageReference pageRef = new PageReference('/apex/ConvertDeal');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('dealId',sponsorshipDeal.Id);
        
        // Test #2: Incorrect Status
        sponsorshipDeal.NU__DealStatus__c = 'New';
        update sponsorshipDeal;
        
        ConvertDeal controller = new ConvertDeal(new ApexPages.StandardController(new NU__Deal__c()));
        controller.redirect();
        
        System.assert(ApexPages.getMessages().size() > 0 && ApexPages.getMessages().get(0).getDetail().contains('"' + Constant.DEAL_STATUS_READY_FOR_CONVERT + '"'));
    }
    
    static testMethod void testConvertDeal_ExistingOrder() {    
        setupTest();
        
        PageReference pageRef = new PageReference('/apex/ConvertDeal');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('dealId',sponsorshipDeal.Id);
        
        // Test #3: Existing Order
        sponsorshipDeal.NU__DealStatus__c = Constant.DEAL_STATUS_READY_FOR_CONVERT;
        sponsorshipDeal.Order__c = testOrder.Id;
        update sponsorshipDeal;

        ConvertDeal controller = new ConvertDeal(new ApexPages.StandardController(new NU__Deal__c()));
        controller.redirect();
        
        System.assert(ApexPages.getMessages().size() > 0 && ApexPages.getMessages().get(0).getDetail().contains('existing order'));
        
        sponsorshipDeal.Order__c = null;
        update sponsorshipDeal;
    }
    
    static testMethod void testConvertDeal_AdvertisingDeal() {
    	setupTest();
        testConvertDealHelper(advertisingDeal);
    }
    
    static testMethod void testConvertDeal_SponsorshipDeal() {
    	setupTest();
    	testConvertDealHelper(sponsorshipDeal);
    }
    
    static testMethod void testConvertDeal_MembershipDeal() {
    	setupTest();
    	testConvertDealHelper(membershipDeal);
    }
    
    static testMethod void testConvertDeal_ExhibitorDeal() {
    	setupTest();
    	testConvertDealHelper(exhibitorDeal);
    }
    
    static testMethod void testConvertDeal_MembershipDealNoMTPL() { 
        setupTest();
        
        PageReference pageRef = new PageReference('/apex/ConvertDeal');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('dealId',membershipDeal.Id);
        
        // Test #7a: Membership Deal - no membership type product link found or membership type supplied
        membershipTypeProductLink.NU__Status__c = 'Inactive';
        update membershipTypeProductLink;
        
        ConvertDeal controller = new ConvertDeal(new ApexPages.StandardController(new NU__Deal__c()));
        controller.redirect();
        
        System.assert(ApexPages.getMessages().size() > 0 && ApexPages.getMessages().get(0).getDetail().contains('membership type product link'));   
        
        membershipTypeProductLink.NU__Status__c = 'Active';
        update membershipTypeProductLink;   
    }
    
    private static void assert(NU__Deal__c deal) {
        Map<Id, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosById();
        
        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        
        system.assert(pageMessages == null || pageMessages.size() == 0, 'The following error occurred: ' + pageMessages);
        
        NU__Deal__c dealQueried = [select id, name, Total_Price__c from NU__Deal__c where id = :deal.Id];
        
        List<NU__Cart__c> carts = [SELECT Id,
            NU__Total__c
            FROM NU__Cart__c
            WHERE Deal__c = :deal.Id];
        List<NU__CartItem__c> cartItems = [SELECT Id,
            RecordType.Name,
            NU__PriceClass__r.Name
            FROM NU__CartItem__c
            WHERE NU__Cart__c IN :carts];

        System.assert(carts != null && carts.size() > 0, 'Expected ' + rt.get(deal.RecordTypeId).getName() + ' cart to be found, but none found.');


        System.assertEquals(dealQueried.Total_Price__c, carts[0].NU__Total__c);

        System.assert(cartItems != null && cartItems.size() > 0, 'Expected ' + rt.get(deal.RecordTypeId).getName() + ' cart items to be found, but none found.');
        System.assert(cartItems[0].RecordType.Name == rt.get(deal.RecordTypeId).getName(), 'Expected cart item to have a record type of ' + rt.get(deal.RecordTypeId).getName() + '.');
        System.assert(cartItems[0].NU__PriceClass__r.Name == NU.Constant.PRICE_CLASS_DEFAULT, 'Expected cart item to have a price class of ' + NU.Constant.PRICE_CLASS_DEFAULT + '.');
    }
}