public with sharing class ConferenceTimeslotTriggerHandlers extends NU.TriggerHandlersBase {
	// "Simple Date Format" documentation can be found at
	// http://docs.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html
	
	static final String SESSION_DATE_FORMAT_STR = 'EEEE, MMMM d, yyyy';
	static final String SESSION_DAY_FORMAT_STR = 'EEEE';
	static final String SESSION_TIME_FORMAT_STR = 'h:mm a';
	
	private String calculateSessionTime(DateTime startTime, DateTime endTime){
		String startTimeStr = startTime.format(SESSION_TIME_FORMAT_STR);
		String endTimeStr = endTime.format(SESSION_TIME_FORMAT_STR);
		
		// Remove the first AM or PM if both the start and end
		// are either AM or PM
		if ((startTimeStr.containsIgnoreCase('AM') &&
		    endTimeStr.containsIgnoreCase('AM'))
		    
		    ||
		    
		    (startTimeStr.containsIgnoreCase('PM') &&
		    endTimeStr.containsIgnoreCase('PM'))){
		    
		    startTimeStr = startTimeStr.split(' ')[0];
	    }
	    
	    String startAndEndTime = startTimeStr + ' - ' + endTimeStr;
		
		startAndEndTime = startAndEndTime.replace('AM', 'a.m.').replace('PM', 'p.m.');
		
		system.debug('    startAndEndTime ' + startAndEndTime);
		
		return startAndEndTime;
	}
	
	public void populateDay(Conference_Timeslot__c timeslot){		
		timeslot.Day__c = timeslot.Start__c.format(SESSION_DAY_FORMAT_STR);
	}
	
	public void populateSessionDate(Conference_Timeslot__c timeslot){
		timeslot.Session_Date__c = timeslot.Start__c.format(SESSION_DATE_FORMAT_STR);
	}
	
	public void populateSessionTime(Conference_Timeslot__c timeslot){
		timeslot.Session_Time__c = calculateSessionTime(timeSlot.Start__c, timeSlot.End__c);
	}
	
	public void populateTimeSlot(Conference_Timeslot__c timeslot){
		String beginningTimeSlot = timeslot.Start__c.format(SESSION_DATE_FORMAT_STR);
		String sessionTime = calculateSessionTime(timeslot.Start__c, timeslot.End__c);
		
		timeslot.TimeSlot__c = beginningTimeSlot + ', ' + sessionTime;
	}
	
	public void populateShortSlot(Conference_Timeslot__c timeslot){
		String dayStr = timeslot.Start__c.format(SESSION_DAY_FORMAT_STR);
		String timeStr = calculateSessionTime(timeslot.Start__c, timeslot.End__c);
		
		timeslot.Short_Slot__c = dayStr + ', ' + timeStr;
	}
	
	public override void onBeforeInsert(List<sObject> newRecords){
		List<Conference_Timeslot__c> newTimeslots = (List<Conference_Timeslot__c>) newRecords; 
		
        for (Conference_Timeslot__c newTimeSlot : newTimeslots){
        	if (newTimeSlot.Start__c != null){
        		if (newTimeSlot.Day__c == null){
        			populateDay(newTimeSlot);
        		}
        		
        		if (newTimeSlot.Session_Date__c == null){
        			populateSessionDate(newTimeSlot);
        		}
        		
        		if (newTimeSlot.Session_Time__c == null &&
        		    newTimeSlot.End__c != null){
        			populateSessionTime(newTimeSlot);
        		}
        		
        		if (newTimeSlot.TimeSlot__c == null &&
        		    newTimeSlot.End__c != null){
        		    populateTimeSlot(newTimeSlot);
    		    }
    		    
    		    if (newTimeSlot.Short_Slot__c == null &&
    		        newTimeSlot.End__c != null){
    		        populateShortSlot(newTimeSlot);
		        }
        	}
        }
    }
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap){
    	for (Conference_Timeslot__c oldTimeSlot : (List<Conference_Timeslot__c>) oldRecordMap.values()){
    		Conference_Timeslot__c newTimeSlot = (Conference_Timeslot__c) newRecordMap.get(oldTimeSlot.Id);
    		
    		if (oldTimeSlot.Start__c != newTimeSlot.Start__c &&
    		    newTimeSlot.Start__c != null){

    		    populateDay(newTimeSlot);
    		    populateSessionDate(newTimeSlot);
    		    
    		    if (newTimeSlot.End__c != null){
    		    	populateSessionTime(newTimeSlot);
    		    	populateTimeSlot(newTimeSlot);
    		    	populateShortSlot(newTimeSlot);
    		    }
		    }
		    
		    if (oldTimeSlot.End__c != newTimeSlot.End__c &&
		        newTimeSlot.End__c != null && newTimeSlot.Start__c != null){
		        populateSessionTime(newTimeSlot);
		    	populateTimeSlot(newTimeSlot);
		    	populateShortSlot(newTimeSlot);
	        }
    	}
    }
}