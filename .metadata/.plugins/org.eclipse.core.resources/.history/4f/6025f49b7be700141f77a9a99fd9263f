// visualforce page controller for InvoiceQuarterlyDuesReport.page (Quarterly Invoice Dues Report tab) in the
// State Partner Portal and also available to LeadingAge staff in Salesforce.
// The setup code is very similar to other State Partner Portal pages and could be refactored into
// a generic set of controls for all similar pages -NF
 // Production folder: 00ld0000001kqQD, Staging folder: 00lK0000000h42y

public class InvoiceQuarterlyDuesController {
    public string SelectedState {get; set;} //the state to be included in the report
    public integer SelectedYear {get; set;} //binds year value
    public string SelectedYearString {get{return string.valueOf(SelectedYear);}} //string representation of SelectedYear
    private integer SelectedPrevYear {get; set;} //binds year value
    public integer SelectedQuarter {get; set;} //binds quarter value
    private User currentUser {get; set;}
    private User p_pageUser = null;

    //stores a combination of account details, dues for this year, and last year; for a single account
    public class providerRecord1 {
        public Joint_Billing_Item__c accountDetail {get; set;}
        public Joint_Billing_Item_Line__c SelectedYearMembership {get; set;}
        public Joint_Billing_Item__c SelectedPreviousYearMembershipSummary {get; set;}
        public Joint_Billing_Item_Line__c SelectedPreviousYearMembership {get; set;}
    }

    //constructor
    public InvoiceQuarterlyDuesController() {
        //get user information    
        Id userId = UserInfo.getUserId();
        currentUser = [Select id, name, Contact.Account.State_Partner_Id__c, Profile.Name from User where id = :userId];

        //get form information
        if (!isUserLeadingAgeStaff)
        {
            SelectedState = String.ValueOf(currentUser.Contact.Account.State_Partner_Id__r.Name);
        }
        //run report
        //no action required; visualforce page calls allDuesData, joinedDuesData, lapsedDuesData, and adjustedDuesData
    }

    //outputs a (drop-down) list of State Partners, visible to LeadingAge Staff only 
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Account> lData = [
            SELECT ID, 
                Name, 
                BillingState 
            FROM Account 
            WHERE RecordType.DeveloperName = 'State_Partner'
            ORDER BY Name];
        for (Account a: lData) {
            options.add(new SelectOption(a.Name, a.Name));
        }
        return options;
    }
    
    // returns a list of n years, starting at year m
    private List<SelectOption> populateYears(Integer startingYear, Integer yearsToAdd) {
        List<SelectOption> years = new List<SelectOption>();
        for (Integer x = 0; x < yearsToAdd; x++) {
            years.add(new SelectOption(String.valueOf(startingYear), String.valueOf(startingYear)));
            startingYear++;
        }
        
        return years;
    }
    
    //outputs a (drop-down) list of available years
    public List<SelectOption> getYears() {
        List<SelectOption> options = new List<SelectOption>();
           options = populateYears(Date.Today().addYears(-1).year(), 2);
           return options;  
    }
    
    //outputs a (drop-down) list of quarters
    public List<SelectOption> getQuarters() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1','Jan-Mar'));
        options.add(new SelectOption('2','Apr-June'));
        options.add(new SelectOption('3','July-Sep'));
        options.add(new SelectOption('4','Oct-Dec'));
        return options;
    }

    //the run button triggers a refresh of the page, which recalculates the tables based on the form fields
    public void Run() {
        SelectedPrevYear = SelectedYear - 1;
        
    }

    //retireve information about the user
    private User pageUser
    {
        get
        {
            if (p_pageUser == null)
            {
                p_pageUser = [SELECT ID, CompanyName, Contact.Account.ID FROM User WHERE ID = :UserInfo.getUserID()];
            }
            return p_pageUser;
        }
    }

    //determine if the user has access to all state partner reports, or just a single state partner report
    //When logged in as a State Partner Portal user, use the user --> Contact --> Account.State_Partner_Id
    //to determine the corresponding "State Partner" account.

    //SK TODO 20130308: Need to find out how to do this by id... or if this is even correct

    public boolean isUserLeadingAgeStaff
    {
        get
        {
            return (pageUser.CompanyName != null && pageUser.CompanyName == 'LeadingAge');
        }
    }

    //retrieve joined account records for a selected state, plus joint billing data for this year and last year
    private List <providerRecord1> joinedDuesData_priv = null;
    public List <providerRecord1> joinedDuesData {
        get {
            List<Joint_Billing_Item__c> thisYearsDues = getProviderAccountsForYear('joined', SelectedYear);
            Map<Id, Joint_Billing_Item__c> prevYearsDues = getJBIMap(getProviderAccountsForYear('joined', SelectedPrevYear));

            joinedDuesData_priv = getProviderRecordList(thisYearsDues, prevYearsDues, 'joined');
            return joinedDuesData_priv;
        }
    }

    //totals for joined accounts
    public Decimal joinedDuesDataGrossAnnualDues {
        get {
            Decimal grossAD = 0;
            
            for (providerRecord1 a : joinedDuesData_priv){
                if (a.SelectedYearMembership.Amount__c != null && a.accountDetail.Account__r.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == false)
                    grossAD += (a.SelectedYearMembership.Amount__c);
            }
            
            return grossAD;
        }
    }

    public Decimal joinedDuesDataServiceFee {
        get {
            return joinedDuesDataGrossAnnualDues * -0.15;
        }
    }
    
    public Decimal joinedDuesDataNetLeadingAgeDues {
        get {
            return joinedDuesDataGrossAnnualDues + joinedDuesDataServiceFee;
        }
    }

    //retrieve lapsed account records for a selected state, plus joint billing data for this year and last year
    private List <providerRecord1> lapsedDuesData_priv = null;
    public List <providerRecord1> lapsedDuesData {
        get {
            List<Joint_Billing_Item__c> thisYearsDues = getProviderAccountsForYear('lapsed', SelectedYear);
            Map<Id, Joint_Billing_Item__c> prevYearsDues = getJBIMap(getProviderAccountsForYear('lapsed', SelectedPrevYear));

            lapsedDuesData_priv = getProviderRecordList(thisYearsDues, prevYearsDues, 'lapsed');
            return lapsedDuesData_priv;
        }
    }

    //totals for lapsed accounts
    public Decimal lapsedDuesDataGrossAnnualDues {
        get {
            Decimal grossAD = 0;
            
            for (providerRecord1 a : lapsedDuesData_priv){
                if (a.SelectedYearMembership.Amount__c != null && a.accountDetail.Account__r.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == false)
                    grossAD += (a.SelectedYearMembership.Amount__c);
            }
            
            return grossAD;
        }
    }

    public Decimal lapsedDuesDataServiceFee {
        get {
            return lapsedDuesDataGrossAnnualDues * -0.15;
        }
    }
    
    public Decimal lapsedDuesDataNetLeadingAgeDues {
        get {
            return lapsedDuesDataGrossAnnualDues + lapsedDuesDataServiceFee;
        }
    }

    //retrieve adjusted account records for a selected state, plus joint billing data for this year and last year
    private List <providerRecord1> adjustedDuesData_priv = null;
    public List <providerRecord1> adjustedDuesData {
        get {
            List<Joint_Billing_Item__c> thisYearsDues = getProviderAccountsForYear('adjusted', SelectedYear);
            Map<Id, Joint_Billing_Item__c> prevYearsDues = getJBIMap(getProviderAccountsForYear('adjusted', SelectedPrevYear));

            adjustedDuesData_priv = getProviderRecordList(thisYearsDues, prevYearsDues, 'adjusted');
            return adjustedDuesData_priv;
        }
    }

    //totals for adjusted accounts
    public Decimal adjustedDuesDataGrossAnnualDues {
        get {
            Decimal grossAD = 0;
            
            for (providerRecord1 a : adjustedDuesData_priv){
                if (a.SelectedYearMembership.Amount__c != null && a.accountDetail.Account__r.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == false)
                    grossAD += (a.SelectedYearMembership.Amount__c);
            }
            
            return grossAD;
        }
    }

    public Decimal adjustedDuesDataServiceFee {
        get {
            return adjustedDuesDataGrossAnnualDues * -0.15;
        }
    }
    
    public Decimal adjustedDuesDataNetLeadingAgeDues {
        get {
            return adjustedDuesDataGrossAnnualDues + adjustedDuesDataServiceFee;
        }
    }

    //this section can be uncommented for testing purposes

    //retrieve all account records for a selected state, plus joint billing data for this year and last year
//    private List <providerRecord1> allDuesData_priv = null;
//    public List <providerRecord1> allDuesData {
//        get {
//            List<Joint_Billing_Item__c> thisYearsDues = getProviderAccountsForYear('all', SelectedYear);
//            Map<Id, Joint_Billing_Item__c> prevYearsDues = getJBIMap(getProviderAccountsForYear('all', SelectedPrevYear));

//            allDuesData_priv = getProviderRecordList(thisYearsDues, prevYearsDues);
//            return allDuesData_priv;
//        }
//    }

    //totals for all accounts
 //   public Decimal allDuesDataGrossAnnualDues {
 //       get {
 //           Decimal grossAD = 0;
 //           
 //           for (providerRecord1 a : allDuesData_priv){
 //               if (a.SelectedYearMembership.Amount__c != null && a.accountDetail.Account__r.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c == false)
 //                   grossAD += (a.SelectedYearMembership.Amount__c);
 //           }

//           return grossAD;
//        }
//    }

//    public Decimal allDuesDataServiceFee {
//        get {
//            return allDuesDataGrossAnnualDues * -0.15;
//        }
//    }
    
//    public Decimal allDuesDataNetLeadingAgeDues {
//        get {
//            return allDuesDataGrossAnnualDues + allDuesDataServiceFee;
//        }
//    }

    //end for testing

    public Decimal grandTotal {
        get {
            return joinedDuesDataNetLeadingAgeDues + lapsedDuesDataNetLeadingAgeDues + adjustedDuesDataNetLeadingAgeDues;
        }
    }



    //build a list of provider accounts from a combination of account, and dues for this year and last year
    private List <providerRecord1> getProviderRecordList (List<Joint_Billing_Item__c> thisYearsDues, Map<Id, Joint_Billing_Item__c> prevYearsDues, string sMode) {
            List <providerRecord1> prList = new List <providerRecord1>();
            
            Decimal itemLineTotal = 0.00;
            for (Joint_Billing_Item__c a : thisYearsDues) {
                providerRecord1 pr = new providerRecord1();
                itemLineTotal = 0.00;
                
                pr.accountDetail = a;
                if (!a.Joint_Billing_Item_Lines__r.isEmpty())
                    pr.SelectedYearMembership = a.Joint_Billing_Item_Lines__r [0];
                
                if (prevYearsDues.containsKey(a.Account__r.Id)) {
                    Joint_Billing_Item__c jbi = prevYearsDues.get(a.Account__r.Id);
                    if (jbi != null)
                        pr.SelectedPreviousYearMembershipSummary = jbi;
                    if (!jbi.Joint_Billing_Item_Lines__r.isEmpty())
                        pr.SelectedPreviousYearMembership = jbi.Joint_Billing_Item_Lines__r [0];
                }
                
                if (!a.Joint_Billing_Item_Lines__r.isEmpty()) {
                    Joint_Billing_Item_Line__c itemLine = new Joint_Billing_Item_Line__c();
                    for (Joint_Billing_Item_Line__c b : a.Joint_Billing_Item_Lines__r) {
                        if (sMode == 'joined') {
                            if (b.Type__c == 'New') {
                                pr.SelectedYearMembership = b;
                            }
                        }
                        else if (sMode == 'lapsed' || sMode == 'adjusted') {
                            if (b.Type__c == 'Adjustment') {
                                if (b.Amount__c != null) {
                                    itemLineTotal = itemLineTotal + b.Amount__c;
                                    itemLine = b;
                                }
                            }
                        }
                    }
                    if (sMode == 'lapsed' || sMode == 'adjusted') {
                        itemLine.Amount__c = itemLineTotal;
                        pr.SelectedYearMembership = itemLine;
                    }
                }



                //lapsed
  //              pr.SelectedYearMembership = a.Joint_Billing_Item_Lines__r [0];
  //              Joint_Billing_Item__c jbi = prevYearsDues.get(a.Account__r.Id);
  //              if (jbi.Joint_Billing_Item_Lines__r.size() == 2) {
  //                  pr.SelectedPreviousYearMembership = jbi.Joint_Billing_Item_Lines__r [1];
  //              }

                
                //adjusted
  //              pr.SelectedYearMembership = a.Joint_Billing_Item_Lines__r [0];
  //              Joint_Billing_Item__c jbi = prevYearsDues.get(a.Account__r.Id);
  //              if (jbi.Joint_Billing_Item_Lines__r.size() == 2) {
  //                  pr.SelectedPreviousYearMembership = jbi.Joint_Billing_Item_Lines__r [1];
  //              }
                
                
                prList.add(pr);
            }
            
            return prList;
    }

    //create map from a list, with account as the key
    private Map<Id, Joint_Billing_Item__c> getJBIMap(List<Joint_Billing_item__c> jbiList) {
        Map<Id, Joint_Billing_Item__c> jbiMap = new Map<Id, Joint_Billing_Item__c>();
 
        for (Joint_Billing_Item__c a : jbiList){
            jbiMap.put(a.Account__r.Id, a);
        }

        return jbiMap;
    }

    //retrieve accounts for a specified year
    private List<Joint_Billing_Item__c> getProviderAccountsForYear(string action, integer yr) {
        if (SelectedState == null || SelectedState == '-1')
        {
            return new List<Joint_Billing_Item__c>(); 
        }
        else
        {
            String queryInnerWhere = '';
            if (action == 'joined') {
                queryInnerWhere += 'AND CALENDAR_YEAR(Date__c) = :yr ';
                queryInnerWhere += 'AND CALENDAR_QUARTER(Date__c) = :SelectedQuarter ';
            }
            if (action == 'adjusted' || action == 'lapsed') {
                queryInnerWhere += 'AND Type__c = \'Adjustment\' ';
                queryInnerWhere += 'AND CALENDAR_YEAR(Date__c) = :yr ';
                queryInnerWhere += 'AND CALENDAR_QUARTER(Date__c) = :SelectedQuarter ';
            }
            String querySelect = 'SELECT ' +
                'Account__r.id, ' +
                'Account__r.LeadingAge_ID__c, ' +
                'Account__r.Name, ' +
                'Account__r.NU__RecordTypeName__c, ' +
                'Account__r.Provider_Join_On__c, ' +
                'Account__r.Provider_Lapsed_On__c, ' +
                'Account__r.Multi_Site_Corporate__c, ' +
                'Account__r.NU__PrimaryAffiliation__r.RecordTypeId, ' +
                'Account__r.NU__PrimaryAffiliation__r.RecordType.Name, ' +
                'Account__r.NU__PrimaryAffiliation__r.NU__RecordTypeName__c, ' +
                'Account__r.NU__PrimaryAffiliation__r.Multi_Site_Bill_Jointly__c, ' +
                'Account__r.NU__PrimaryAffiliation__r.Multi_Site_Corporate__c, ' +
                'Account__r.Provider_Membership__r.Last_Provider_Lapsed_On__c, ' +
                'Id, ' +
                'Start_Date__c, ' +
                'End_Date__c, ' +
                'Amount__c, ' +
                'Bill_Date__c, ' +
                '(Select Id, ' +
                '   Name, ' +
                '   Date__c, ' +
                '   Type__c, ' +
                '   Revenue_Year_Submitted__c, ' +
                '   Program_Service_Revenue__c, ' +
                '   Dues_Price__c, ' +
                '   Amount__c, ' +
                '   Category__c ' +
                '    FROM Joint_Billing_Item_Lines__r ' +
                '    WHERE (CALENDAR_YEAR(Joint_Billing_Item__r.Start_Date__c) = :yr) ' +
                queryInnerWhere +
                //'    ORDER BY Joint_Billing_Item__r.Start_Date__c desc ' +
                '    ORDER BY Joint_Billing_Item__r.Start_Date__c desc, Date__c asc ' +
                ') ' +
                'FROM Joint_Billing_Item__c ';

            String queryWhere = 'WHERE (Account__r.RecordTypeId != \'' +
                Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID + '\' OR (Account__r.RecordTypeId = \'' +
                Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID + '\' AND Account__r.Multi_Site_Enabled__c  = true) OR (Account__r.RecordTypeId = \'' + 
                Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID + '\' AND Account__r.Multi_Site_Corporate__c = true) ) AND Joint_Billing__r.State_Partner__r.Name = :SelectedState ';
            queryWHERE += 'AND CALENDAR_YEAR(Start_Date__c) = :yr ';
            if (action == 'joined') {
                //queryWhere += 'AND CALENDAR_YEAR(Account__r.Provider_Join_On__c) = :SelectedYear ';
                //queryWhere += 'AND CALENDAR_QUARTER(Account__r.Provider_Join_On__c) = :SelectedQuarter ';
                queryWhere += 'AND (Id in (SELECT Joint_Billing_Item__c FROM Joint_Billing_Item_Line__c ';
                queryWhere += 'WHERE (CALENDAR_YEAR(Joint_Billing_Item__r.Start_Date__c) = :yr) ';
                queryWhere += 'AND Type__c = \'New\' ';
                queryWhere += 'AND CALENDAR_YEAR(Date__c) = :yr ';
                queryWhere += 'AND CALENDAR_QUARTER(Date__c) = :SelectedQuarter)) ';
                //queryWhere += 'AND Date_After_Bill_Date__c = true)) ';
            }
            if (action == 'lapsed') {
                queryWhere += 'AND (';
                queryWhere += '(CALENDAR_YEAR(Account__r.Provider_Lapsed_On__c) = :SelectedYear AND CALENDAR_QUARTER(Account__r.Provider_Lapsed_On__c) = :SelectedQuarter) OR ';
                queryWhere += '(CALENDAR_YEAR(Account__r.Provider_Membership__r.Last_Provider_Lapsed_On__c) = :SelectedYear AND CALENDAR_QUARTER(Account__r.Provider_Membership__r.Last_Provider_Lapsed_On__c) = :SelectedQuarter) OR ';
                queryWhere += '(CALENDAR_YEAR(Account__r.NU__Membership__r.Last_Provider_Lapsed_On__c) = :SelectedYear AND CALENDAR_QUARTER(Account__r.NU__Membership__r.Last_Provider_Lapsed_On__c) = :SelectedQuarter) ';
                queryWhere += ') ';
            }
            if (action == 'adjusted') {
                queryWhere += 'AND (Id in (SELECT Joint_Billing_Item__c FROM Joint_Billing_Item_Line__c ';
                queryWhere += 'WHERE (CALENDAR_YEAR(Joint_Billing_Item__r.Start_Date__c) = :yr) ';
                queryWhere += 'AND Type__c = \'Adjustment\' ';
                queryWhere += 'AND CALENDAR_YEAR(Date__c) = :yr ';
                queryWhere += 'AND CALENDAR_QUARTER(Date__c) = :SelectedQuarter)) ';
                queryWhere += 'AND (Account__r.Provider_Membership__r.Last_Provider_Lapsed_On__c = NULL)';
            }

            String queryOrder = 'ORDER BY Account__r.Name ASC';
            
            String queryFull = querySelect + queryWhere + queryOrder;
            System.debug ('full query (action:' + action  + '/state' + SelectedState  + '/year' + yr  + '/selectedyear' + SelectedYear  + '/selectedquarter' + SelectedQuarter  + '): ' + queryFull);
            List<Joint_Billing_Item__c> jbiList = (List<Joint_Billing_Item__c>) Database.query(queryFull);
            
            return jbiList;
        }
    }
    
    //retrieve document for a selected state
    public List<Document> getQuarterlyDocument(){
        if (selectedState == null || selectedState == '-1')
        {
            return new List<Document>(); 
        }
        else
        {
      String queryName = (SelectedState.replace(' ', '_') + '_' + SelectedYearString.replace(' ', '') + 'Q' + selectedQuarter );
            return (List<Document>)Database.query('SELECT FolderId, Id, Name, LastModifiedDate FROM Document Where (FolderId = \'00lK0000000h42y\' or FolderId = \'00ld0000001kqQD\')  and name = :queryName'); //:queryName
        }
    }


    
}