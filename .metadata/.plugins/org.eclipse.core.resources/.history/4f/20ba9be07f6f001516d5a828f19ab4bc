public with sharing class KYInvoice {
	private static final String ORDER_ITEM_ORDER_FIELD_NAME = 'NU__Order__c';
	private static final String ACTIVE = 'Active';
	private static final String ORDER_STATUS_CANCELLED = 'Cancelled';

	public class OrderWrapper {
		
		public OrderWrapper(NU__Order__c order) {
			this.Order = order;
		}
		
		
		public NU__Order__c Order { get; set; }
		
		public List<NU__OrderItem__c> orderItems { get; set; }

		public List<String> creditCardsAccepted { get; set; }
	}
     
    private List<NU__Order__c> os;
    public List<NU__Order__c> Orders {
        get { return os; }
        set {
            if (value != null) {
                os = value;
                Load();
            }   
        }
    }
    
    private Id orderIdPriv;
    public Id orderId{
    	get{ return orderIdPriv; }
    	set{
    		if (value != null){
    			orderIdPriv = value;
    			Orders = getOrdersByIds(new Set<Id>{ orderIdPriv });
    		}
    	}
    }

    public List<OrderWrapper> OrderWrappers { get; set; }
    public Map<string,decimal> bedcounts {get; set;}
    
    private List<NU__Order__c> QueryOrders() {
    	return [select
            NU__Balance__c,
            NU__TotalPayment__c,
            NU__TotalTax__c,
            NU__TotalShipping__c,
            NU__SubTotal__c,
            NU__GrandTotal__c,
            Invoice_Attention_To__c,
            NU__InvoiceDescription__c,
            NU__InvoiceNumber__c,
            NU__InvoiceDate__c,
            NU__InvoiceTerm__c,
            NU__InvoiceDueDate__c,
            NU__InvoiceEmail__c,
            NU__PurchaseOrderNumber__c,
            NU__BillTo__r.Name,
            NU__BillTo__r.NU__PersonContact__r.Email,
            NU__BillTo__r.LeadingAge_Id__c,
            NU__BillTo__r.NU__PrimaryAffiliation__r.Name,
            NU__BillTo__r.BillingStreet,
            NU__BillTo__r.BillingCity,
            NU__BillTo__r.BillingState,
            NU__BillTo__r.BillingPostalCode,
            NU__BillTo__r.BillingCountry,
            NU__BillTo__r.ShippingStreet,
            NU__BillTo__r.ShippingCity,
            NU__BillTo__r.ShippingState,
            NU__BillTo__r.ShippingPostalCode,
            NU__BillTo__r.ShippingCountry,
    	    NU__BillTo__r.For_Profit__c,
    	    NU__BillTo__r.Number_Of_Intermedicate_Care_Beds__c,
    	    NU__BillTo__r.Number_Of_Nursing_Beds__c,
    	    NU__BillTo__r.Number_Of_Personal_Care_Beds__c,
    	    NU__BillTo__r.Number_Of_Assisted_Living_Units__c,
    	    NU__BillTo__r.Number_Of_Independent_Living_Units__c,
            NU__Entity__r.Name,
            NU__Entity__r.NU__LogoURL__c,
            NU__Entity__r.NU__ShortName__c,
            NU__Entity__r.NU__Street__c,
            NU__Entity__r.NU__City__c,
            NU__Entity__r.NU__State__c,
            NU__Entity__r.NU__PostalCode__c,
            NU__Entity__r.NU__Country__c,
            NU__Entity__r.NU__RemittanceStreet__c,
            NU__Entity__r.NU__RemittanceCity__c,
            NU__Entity__r.NU__RemittanceState__c,
            NU__Entity__r.NU__RemittancePostalCode__c,
            NU__Entity__r.NU__RemittanceCountry__c,
            NU__Entity__r.NU__RemittanceEnabled__c,
            NU__Entity__r.NU__RemittanceText__c,
            NU__Entity__r.NU__Phone__c,
            NU__Entity__r.NU__Website__c,
            NU__Entity__r.FP_Assisted_Living_Beds__c,
            NU__Entity__r.FP_Independent_Living_Units__c,
            NU__Entity__r.FP_Intermedicate_Care_Beds__c,
            NU__Entity__r.FP_Nursing_Home_Beds__c,
            NU__Entity__r.FP_Personal_Care_Beds__c,
            NU__Entity__r.FP_Skilled_Nursing_Facility_Units__c,
            NU__Entity__r.NP_Assisted_Living_Beds__c,
            NU__Entity__r.NP_Independent_Living_Units__c,
            NU__Entity__r.NP_Intermedicate_Care_Beds__c,
            NU__Entity__r.NP_Nursing_Home_Beds__c,
            NU__Entity__r.NP_Personal_Care_Beds__c,
            NU__Entity__r.NP_Skilled_Nursing_Facility_Units__c,
            NU__Entity__r.Invoice_Footer__c
            from NU__Order__c
            where Id in :Orders];
    }

    private Map<Id, List<String>> QueryEntityCreditCardIssuersAccepted(){
    	Map<Id, List<String>> entityCCIsAccepted = new Map<Id, List<String>>();
    	
    	List<NU__Entity__c> entities = 
    	 [select id,
    	         name,
    	         (select NU__CreditCardIssuer__r.Name
    	            from NU__CreditCardIssuers__r
    	           where NU__Status__c = :ACTIVE
    	           order by NU__CreditCardIssuer__r.Name
    	         )
    	    from NU__Entity__c];

    	for (NU__Entity__c entity : entities){
    		if (NU.CollectionUtil.listHasElements(entity.NU__CreditCardIssuers__r) == false){
    			continue;
    		}
    		
    		List<String> entityCreditCardsAccepted = new List<String>();
    		
    		for (NU__EntityCreditCardIssuer__c entityCCI : entity.NU__CreditCardIssuers__r){
    			entityCreditCardsAccepted.add(entityCCI.NU__CreditCardIssuer__r.Name);
    		}

			entityCCIsAccepted.put(entity.Id, entityCreditCardsAccepted);
    	}

    	return entityCCIsAccepted;
    }

    private Map<Object, List<NU__OrderItem__c>> QueryOrderOrderItems(List<NU__Order__c> orders){
    	Map<Object, List<NU__OrderItem__c>> orderItemsMap = new Map<Object, List<NU__OrderItem__c>>();

    	List<NU__OrderItem__c> orderItems =
    	    [select Name,
                    NU__TotalTax__c,
                    NU__GrandTotal__c,
                    NU__Customer__c,
                    NU__Customer__r.Name,
                    NU__Customer__r.NU__JoinOn__c,
                    NU__Customer__r.Id,
                    NU__Order__c,
                    NU__Order__r.NU__Entity__c,
                    NU__Order__r.NU__BillTo__c,
                    NU__Order__r.NU__PurchaseOrderNumber__c,
                    NU__Order__r.NU__InvoiceNumber__c,
                    NU__Order__r.NU__InvoiceDescription__c,
                    NU__Order__r.NU__InvoiceDate__c,
                    NU__Order__r.NU__InvoiceDaysOutstanding__c,
                    NU__Order__r.NU__InvoiceDueDate__c,
                    NU__Order__r.NU__InvoiceEmail__c,
                    NU__Order__r.NU__InvoiceGenerated__c,
                    NU__Order__r.NU__InvoiceTerm__c,
                    NU__PriceClass__c,
                    RecordTypeId,
                    RecordType.Name,
                    NU__TotalShipping__c,
                    NU__TotalShippingAndTax__c,
	                NU__IsShipped__c,
	                NU__SalesTax__c,
                    NU__SalesTax__r.NU__TaxRate__c,
                    NU__TaxableAmount__c,
                    NU__Status__c,
                    NU__SubTotal__c,
                    NU__TotalPayment__c,
                    NU__Balance__c,
                    NU__TransactionDate__c,
                    NU__ARGLAccount__c,
                    NU__ShippingGLAccount__c,
                    (select
                        Id,
                        Name,
                        NU__Product__c,
                        NU__Product__r.Name,
                        NU__Product__r.NU__TrackInventory__c,
                        NU__Product__r.NU__InventoryOnHand__c,
                        NU__Product__r.NU__RecordTypeName__c,
                        NU__Product__r.NU__Event__c,
                        NU__Product__r.NU__IsEventBadge__c,
                        NU__Product__r.NU__IsFee__c,
                        NU__Product__r.NU__ListPrice__c,
                        NU__TransactionDate__c,
                        NU__AdjustmentDate__c,
                        NU__UnitPrice__c,
                        NU__Quantity__c,
                        NU__TotalPrice__c,
                        NU__OrderItem__c,
                        NU__OrderItem__r.Name,
                        NU__OrderItem__r.NU__Order__c,
    					NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c,
                        NU__Status__c,
                        NU__EventBadge__c,
                        NU__EventBadge__r.Id,
                        NU__EventBadge__r.Name,
                        NU__EventBadge__r.NU__BadgeType__c,
                        NU__EventBadge__r.NU__BadgeClass__c,
                        NU__EventBadge__r.NU__City__c,
                        NU__EventBadge__r.NU__Company__c,
                        NU__EventBadge__r.NU__Designation__c,
                        NU__EventBadge__r.NU__FirstName__c,
                        NU__EventBadge__r.NU__CasualName__c,
                        NU__EventBadge__r.NU__LastName__c,
                        NU__EventBadge__r.NU__MiddleName__c,
                        NU__EventBadge__r.NU__Name__c,
                        NU__EventBadge__r.NU__ProfessionalTitle__c,
                        NU__EventBadge__r.NU__Registration2__c,
                        NU__EventBadge__r.NU__Salutation__c,
                        NU__EventBadge__r.NU__State__c,
                        NU__EventBadge__r.NU__Suffix__c,
                        NU__MembershipTypeProductLink__c,
                        NU__MembershipTypeProductLink__r.NU__Purpose__c,
                        NU__MembershipTypeProductLink__r.NU__Stage__c,
                        NU__MembershipTypeProductLink__r.NU__TermOverrideMonths__c,
                        NU__MembershipTypeProductLink__r.NU__Product__c,
                        NU__MembershipTypeProductLink__r.NU__MembershipType__c,
                        NU__MembershipTypeProductLink__r.NU__Product__r.Name,
                        NU__MembershipTypeProductLink__r.NU__Product__r.NU__ListPrice__c,
                        NU__Membership__r.NU__Account__r.Name,
                        NU__Membership__r.NU__MembershipType__r.Name,
                        NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c,
                        NU__Membership__r.NU__StartDate__c,
                        NU__Membership__r.NU__EndDate__c,
                        NU__Subscription__c,
                        NU__Subscription__r.Name,
                        NU__Subscription__r.NU__StartDate__c,
                        NU__Subscription__r.NU__EndDate__c,
                        Exhibitor__c,
                        Exhibitor__r.Id,
                        Exhibitor__r.Booth_Number__c
                        from NU__OrderItemLines__r
                        where NU__Status__c != :ORDER_STATUS_CANCELLED
                        order by NU__Product__r.NU__DisplayOrder__c, Name
                    ),
                    (Select
                        Id,
                        Name,
                        NU__Account__c,
                        NU__Account__r.Name,
                        NU__Account__r.Id,
                        NU__EventName__c,
                        NU__Event__c,
                        NU__Event__r.Name,
                        NU__Event__r.NU__ShortName__c,
                        NU__Event__r.NU__InvoiceText__c,
                        NU__Event__r.Id,
                        NU__FullName__c,
                        NU__Status__c,
                        NU__OrderItem__c,
                        NU__OrderItem__r.Name,
                        NU__RegistrantAddress__c
                       from NU__Registrations2__r
                      where NU__Status__c = :ACTIVE)
				from NU__OrderItem__c
				where NU__Order__c in :orders
				  and NU__Status__c = :ACTIVE
				order by RecordType.Name, NU__Customer__r.Name];

    	orderItemsMap = NU.CollectionUtil.groupSObjectsByField(orderItems, ORDER_ITEM_ORDER_FIELD_NAME);

    	return orderItemsMap;
    }
    
    private List<NU__Order__c> getOrdersByIds(Set<Id> orderIds){
    	return [Select Name,
				NU__SubTotal__c,
	            NU__TotalPayment__c,
	            NU__TotalShipping__c,
	            NU__TotalTax__c,
	            NU__TotalShippingAndTax__c,
	            NU__Balance__c,
	            NU__InvoiceAgingScheduleCategory__c,
	            NU__InvoiceARAging__c,
	            NU__InvoiceDate__c,
	            NU__InvoiceDaysOutstanding__c,
	            NU__InvoiceDueDate__c,
	            NU__InvoiceEmail__c,
	            NU__InvoiceGenerated__c,
	            NU__InvoiceDescription__c,
                NU__InvoiceNumber__c,
                NU__InvoiceTerm__c,
                NU__BillToPrimaryAffiliation__c,
                NU__PurchaseOrderNumber__c,
	            NU__GrandTotal__c,
	            NU__ExternalId__c,
	            NU__Entity__c,
	            NU__Entity__r.NU__ShortName__c,
	            NU__Entity__r.Name,
	            NU__BillTo__c,
	            NU__AdjustmentDate__c,
	            NU__AdditionalEmail__c,
	            Id
		    From NU__Order__c
	       where id in :orderIds];
    }

    private void Load() {
    	Map<Id, OrderWrapper> wrapperMap = new Map<Id, OrderWrapper>();
    	Map<Id, List<String>> entityCreditCardsAccepted = QueryEntityCreditCardIssuersAccepted();

    	List<NU__Order__c> orders = QueryOrders();
    	Map<Object, List<NU__OrderItem__c>> orderOrderItems = QueryOrderOrderItems(orders);

    	for (NU__Order__c o : orders) {
    		OrderWrapper ow = new OrderWrapper(o);
    		
			ow.creditCardsAccepted = entityCreditCardsAccepted.get(o.NU__Entity__c);
			ow.OrderItems = orderOrderItems.get(o.Id);
		 	
            wrapperMap.put(o.Id, ow);
            //bedcounts.put('Assisted Living Bed',o.NU__BillTo__r.Number_Of_Assisted_Living_Units__c);
        }

        OrderWrappers = wrapperMap.values();
    }
}