global class SsoSoapApi 
{
   webService static SsoAccountData GetAccountInfo(String accountId) 
   {
       List<SsoAccountData> accts = null;
       List<String> ids = new List<String>();
       SsoAccountData data = null;
       
       ids.add(accountId);
       accts = GetAccountList(ids);
        
       if (accts.size() > 0)
       {
           data = accts[0];
       }
           
               
        return data;
    }
    
    webService static List<SsoAccountData> GetAccounts(List<String> accountIds) 
    {
       return GetAccountList(accountIds);
    }
    
    private static List<SsoAccountData> GetAccountList(List<String> accountIds) 
    {
        List<SsoAccountData> accounts = new List<SsoAccountData>();
       List<Account> accts = [SELECT Id, FirstName, LastName, PersonEmail, PersonTitle, NU__SecurityGroup__c, 
                                PersonBirthDate, NU__ExternalID__c, LeadingAge_ID__c, NU__MembershipType__c, isLeadingAgeMember__c,  Provider_Member__c,  
                                NU__FullName__c, Phone, BillingStreet,BillingState, BillingCity, BillingPostalCode,
                                NU__PrimaryAffiliation__r.Name,NU__PrimaryAffiliation__r.BillingStreet, NU__PrimaryAffiliation__r.BillingCity,
                                NU__PrimaryAffiliation__r.BillingState, NU__PrimaryAffiliation__r.BillingPostalCode,
                                NU__PrimaryAffiliation__r.LeadingAge_ID__c, NU__PrimaryAffiliation__r.Id,
                                NU__PrimaryAffiliation__r.Phone, NU__PrimaryAffiliation__r.Website                                     
                                FROM Account WHERE Id IN :accountIds];
       
       if (accts.size() > 0)
       {
           for(Account acct: accts)
           {
               SsoAccountData data = new SsoAccountData();
               
               //Default fields
               data.Id = acct.Id;
               data.FirstName = acct.FirstName;
               data.LastName = acct.LastName;
               data.Email = acct.PersonEmail;
               data.SecurityGroup = acct.NU__SecurityGroup__c;
               
               //Additional fields
               Map<String, Object> items = new Map<String, Object>();
               items.put('BirthDate',acct.PersonBirthDate);
               items.put('ExternalId',acct.NU__ExternalID__c);
               items.put('LeadingAgeId',acct.LeadingAge_ID__c);               
               items.put('MembershipType',acct.NU__MembershipType__c);
               items.put('Member', acct.isLeadingAgeMember__c == 'Yes' ? true : false);
               items.put('ProviderMember', acct.Provider_Member__c == 'Yes' ? true : false);
               items.put('Title',acct.PersonTitle);   
               items.put('FullName',acct.NU__FullName__c);
               items.put('Phone',acct.Phone);
               items.put('Street',acct.BillingStreet);
               items.put('City',acct.BillingCity);
               items.put('State',acct.BillingState);
               items.put('PostalCode',acct.BillingPostalCode);
               items.put('CompanyName',acct.NU__PrimaryAffiliation__r.Name);
               items.put('CompanySalesForceId',acct.NU__PrimaryAffiliation__r.Id);
               items.put('CompanyLeadingAgeId',acct.NU__PrimaryAffiliation__r.LeadingAge_ID__c);
               items.put('CompanyStreet',acct.NU__PrimaryAffiliation__r.BillingStreet);
               items.put('CompanyCity',acct.NU__PrimaryAffiliation__r.BillingCity);
               items.put('CompanyState',acct.NU__PrimaryAffiliation__r.BillingState);
               items.put('CompanyPostalCode',acct.NU__PrimaryAffiliation__r.BillingPostalCode);
               items.put('CompanyPhone',acct.NU__PrimaryAffiliation__r.Phone);
               items.put('CompanyWebsite',acct.NU__PrimaryAffiliation__r.Website);
               
                          
               
               
               data.Attributes = JSON.serialize(items);
               
               accounts.add(data);
           }
       }
               
        return accounts;
    }
        
    global class SsoAccountData
    {
        webService ID Id {get;set;}
        webService String FirstName {get;set;}
        webService String LastName {get;set;}
        webService String Email {get;set;}
        webService String SecurityGroup {get;set;}
        webService String Attributes{get;set;}
    }
}