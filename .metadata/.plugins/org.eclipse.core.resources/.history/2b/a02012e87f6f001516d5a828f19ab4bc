public with sharing class ViewARStatementController {
	private class AccountWrapper implements Comparable {
		
		public Account account { get; set; }
		
		public List<NU__Order__c> Orders { get; set; }
		public Map<Id, List<NU__OrderItem__c>> OrderItemsMap { get; set; }
		
		public Map<Id,Decimal> arAgingBalances { get; set; }
		public Decimal balance { get; set; }
		
		public String accountNumber { get; set; }
		public String message { get; set; }

		public Integer compareTo(Object compareTo) {
	        // Cast argument 
	    	AccountWrapper compareToAccount = (AccountWrapper)compareTo;
	        
	        // The return value of 0 indicates that both elements are equal. 
	    
	        Integer returnValue = 0;
	        if (account.Name > compareToAccount.account.Name) {
	            // Set return value to a positive value. 
	    
	            returnValue = 1;
	        } else if (account.Name < compareToAccount.account.Name) {
	            // Set return value to a negative value. 
	    
	            returnValue = -1;
	       
	        }
	        
	        return returnValue;       
	    }
	}
    
    public Account AttentiontoinComponent { get; set;}
    private List<Account> privAccounts;
    public List<Account> Accounts {
        get { return privAccounts; }
        set {
            if (value != null) {
                privAccounts = value;
                Load();
            }   
        }
    }
    
    public List<AccountWrapper> AccountWrappers { get; set; }
    
    public NU__Entity__c Entity {
    	get {
    		return ARStatementUtil.Instance.Entity;
    	}
    }
    
    private Map<Id,NU__ARAging__c> privEntityARAgingsMap = null;
    private Map<Id,NU__ARAging__c> EntityARAgingsMap {
    	get {
    		if (privEntityARAgingsMap == null) {
    			privEntityARAgingsMap = new Map<Id,NU__ARAging__c>(EntityARAgings);
    		}
    		return privEntityARAgingsMap;
    	}
    }
    public List<NU__ARAging__c> EntityARAgings {
    	get {
    		return ARStatementUtil.Instance.EntityARAgings;
    	}
    }
    
    public Date InvoiceDate {
    	get {
    		if (ARStatementUtil.invoiceDateOverride != null) {
    			return ARStatementUtil.invoiceDateOverride;
    		}
    		else {
    			return Date.today();
    		}
    	}
    }
    
    /* Called when the accounts have been passed in to the controller. Generates the AccountWrappers to display on the page. */
    private void Load() {
    	Map<Id, AccountWrapper> wrapperMap = new Map<Id, AccountWrapper>();
    	
    	// get the list of valid accounts; the orders mapped to those accounts; and the order items/order item lines mapped to those orders
    	List<Account> accountList = ARStatementUtil.Instance.getValidAccounts(Accounts);
    	Map<Object, List<SObject>> OrdersByAccountMap = ARStatementUtil.Instance.OrdersByAccountMap();
    	Map<Object, List<SObject>> OrderItemsByOrderMap = ARStatementUtil.Instance.OrderItemsByOrderMap();
    			
	    for (Account account : accountList) {
	    	if (OrdersByAccountMap.get(account.Id) != null) {
		    	AccountWrapper aw = new AccountWrapper();
				
				// populate the orders for the account
				aw.Orders = OrdersByAccountMap.get(account.Id);
				aw.OrderItemsMap = new Map<Id,List<NU__OrderItem__c>>();
				
				// initialize the AR Aging balances to $0 for the account
				aw.arAgingBalances = initializeARAgingBalances();
				aw.balance = 0.0;
				
				// loop through all of the orders for the account that meet the criteria 
				// (they all have an AR balance greater than the minimum balance specified when generating the statements, and are of the selected entity)
				for (NU__Order__c anOrder : aw.Orders) {
					Id arAgingId = anOrder.NU__InvoiceARAging__c;
					if (arAgingId == null && EntityARAgings != null && EntityARAgings.size() > 0) {
						arAgingId = EntityARAgings.get(0).Id; // if the AR Aging has not yet been set for the Order - set it to the first AR Aging in the list
					}
					
					Decimal balance = anOrder.NU__Balance__c;
					aw.balance += anOrder.NU__Balance__c;

					// populate account information if not loaded (during the first loop)
					if (aw.account == null) {
						aw.account = new Account(Id = anOrder.NU__BillTo__c, Name = anOrder.NU__BillTo__r.Name, 
							BillingStreet = anOrder.NU__BillTo__r.BillingStreet, BillingCity = anOrder.NU__BillTo__r.BillingCity, 
							BillingState = anOrder.NU__BillTo__r.BillingState, BillingPostalCode = anOrder.NU__BillTo__r.BillingPostalCode, 
							BillingCountry = anOrder.NU__BillTo__r.BillingCountry);
						aw.accountNumber = String.valueOf(anOrder.NU__BillTo__r.get('LeadingAge_Id__c'));
					}
						
					// add the order balance to the correct AR Aging bucket
					aw.arAgingBalances.put(arAgingId, increaseARAgingBalance(aw.arAgingBalances, arAgingId, balance));
						
					// populate the order items map
					if (OrderItemsByOrderMap.get(anOrder.Id) != null) {
						aw.OrderItemsMap.put(anOrder.Id, OrderItemsByOrderMap.get(anOrder.Id));
					}
				}
				
				aw.message = findOldestARAgingMessage(aw.arAgingBalances);
		
		        wrapperMap.put(account.Id, aw);
	    	}
	    }

        AccountWrappers = wrapperMap.values();
        AccountWrappers.sort();
    }
    
    /* Returns a map of all of the AR Agings for the selected Entity, with a $0 balance for each */
    private Map<Id,Decimal> arAgingBalances = null;
    private Map<Id,Decimal> initializeARAgingBalances() {
    	if (arAgingBalances == null || arAgingBalances.size() == 0) {
	    	arAgingBalances = new Map<Id,Decimal>();
	    	
	    	for (NU__ARAging__c arAging : EntityARAgings) {
	    		arAgingBalances.put(arAging.Id, 0.0);
	    	}
    	}
    	
    	return arAgingBalances.clone();
    }
    
    /* Returns a decimal that represents the new balance added to the correct AR Aging bucket */
    private Decimal increaseARAgingBalance(Map<Id,Decimal> arAgingBalances, Id arAgingId, Decimal balance) {
    	if (arAgingId == null 
    		&& EntityARAgings != null && EntityARAgings.size() > 0) {
			
			arAgingId = EntityARAgings.get(0).Id; // if the AR Aging has not yet been set for the Order - set it to the first AR Aging in the list
		}
				
		Decimal newBalance = 0.0;
		// get the previous value in the AR Aging bucket, if set
		if (arAgingBalances != null && arAgingBalances.get(arAgingId) != null) {
			newBalance = arAgingBalances.get(arAgingId);
		}
				
		newBalance += balance;
		return newBalance;
    }
    
    /* Returns the message on the oldest AR Aging record for which there is a balance greater than $0. */
    private String findOldestARAgingMessage(Map<Id,Decimal> arAgingBalances) {
    	String message = '';
    	
    	if (EntityARAgings != null && EntityARAgings.size() > 0) {
    	
	    	// loop through the account's AR Aging balances (in order from newest to oldest)
	    	for (Id arAgingId : arAgingBalances.keySet()) {
	    		Decimal value = arAgingBalances.get(arAgingId);
	    		
	    		// if the value is greater than 0, grab that message (replacing a newer message, if set)
	    		if (value > 0.0) {
	    			if (EntityARAgingsMap.get(arAgingId) != null) {
		    			message = EntityARAgingsMap.get(arAgingId).NU__Message__c;
	    			}
	    		}
	    	}
    	
    	}
    	
    	return message;
    }
}