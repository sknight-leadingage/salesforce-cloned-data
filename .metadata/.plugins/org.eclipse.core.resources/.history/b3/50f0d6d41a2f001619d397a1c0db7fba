<apex:page title="Invoice"
           standardController="NU__Order__c"
           renderAs="{!render}"
           extensions="InvoiceSet"
           showHeader="false"
           sidebar="false"
           standardStylesheets="false"
           id="page">

    <style type="text/css">
        body { margin: 0px; }
        img { border: 0px; }
        .ui-dialog {
            font-size: 75%;
        }
        #buttons {
            margin: 10px;
            font-size: 75%;
            font-family: Arial, Helvetica, sans-serif;
        }
        .btn {
            padding: 4px 3px;
            color: #333;
            margin: 1px;
            border: 1px solid #B5B5B5;
            border-bottom-color: #7F7F7F;
            background: #E8E8E9 url('/img/alohaSkin/btn_sprite.png') repeat-x right top;
            font-weight: bold;
            font-size: .9em;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }
        .btn:hover {
            background-position: right -30px;
        }
        .btn:active {
            background-position: right -60px;
            border-color: #585858;
            border-bottom-color: #939393;
        }
        .message{-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;background-color:#FFC;border-style:solid;border-width:1px;padding:6px 8px 6px 6px;margin:4px 20px}
        .errorM3{border-color:#C00}
        .errorM3 .msgIcon{background-image:url(/img/msg_icons/error24.png);background-position:0 0;width:24px;height:24px}
        .warningM3{border-color:#f90}
        .warningM3 .msgIcon{background-image:url(/img/msg_icons/warning24.png);background-position:0 0;width:24px;height:24px}
        .infoM3{border-color:#39f}
        .infoM3 .msgIcon{background-image:url(/img/msg_icons/info24.png);background-position:0 0;width:24px;height:24px}
        .confirmM3{border-color:#390}
        .confirmM3 .msgIcon{background-image:url(/img/msg_icons/confirm24.png);background-position:0 0;width:24px;height:24px}
        .message .messageText{margin-left:8px}
        .message .messageTable .messageCell{vertical-align:middle}
        .message .messageTable .messageCell h4{margin:0}
        .errorMsg{color:#d74c3b}
        
        .leftmargin { margin-left: 10px; }
        
        .ui-progressbar .ui-progressbar-value {
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAMAAABw8qpSAAAAPFBMVEUazhobzxsd0R0d0x0e1R4f1x8f2B8g2yAh3SEj3yMj4CMk4iQl5CUm5yYm6CYn6ico7Cgq7ior8Ssr8iue9ffCAAAAGklEQVQI12MQZmKgCvwPhDAaOwTLMTPQBgAAZr8RdC6ci8cAAAAASUVORK5CYII=);
            border: thin solid #00aa00;
        }
        #nu-email {
            margin-top: 20px;
        }
        #nu-progressbar {
            width: 300px;
            height: 15px;
        }
        
        .ui-dialog.pleaseWait .ui-dialog-titlebar { background: #8DC63F; border-color: #8DC63F; color: white; text-align: center; }
        .ui-dialog.pleaseWait .ui-dialog-titlebar .ui-dialog-title { margin-right: 0px;float: none; }
    </style>
    <style type="text/css" media="print">
        #buttons { display:none; visibility: hidden; }
    </style>
    <apex:stylesheet value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/smoothness/jquery-ui.css" />
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" />
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" />
    <script type="text/javascript">
    
        var invoices = [
            <apex:repeat value="{!Orders}" var="i">
                ["{!i.Id}", "{!i.Name}", "{!i.NU__InvoiceEmail__c}", "{!i.NU__BillTo__r.NU__PersonEmail__c}"],
            </apex:repeat>
        ];
    
    
        function msgError(msg, bullets) {
            var html = "<div class='message errorM3'><table border='0' cellpadding='0' cellspacing='0' class='messageTable' " +
                    "style='padding:0px;margin:0px;'><tr valign='top'><td><img alt='ERROR' class='msgIcon' src='/s.gif' title='ERROR'>" +
                    "</td><td class='messageCell'><div class='messageText'><span style='color:#cc0000'><h4>Error:</h4></span>" +
                    msg + "<br></div></td></tr>";
            if (bullets) {
                html += "<tr><td></td><td><span><ul style='padding-left:10px;padding-top:0px;margin:0px'>";
                for (var i = 0; i < bullets.length; i++) {
                    html += "<li style='padding-top:5px'>" + bullets[i] + "</li>";
                }
                html += "</ul></span></td></tr>";
            }
            return html + "</table></div>";
        }
        function msgSuccess(msg) {
            return $("<div class='message confirmM3'><table border='0' cellpadding='0' cellspacing='0' class='messageTable' " +
                    "style='padding:0px;margin:0px;'><tr valign='top'><td><img alt='CONFIRM' class='msgIcon' src='/s.gif' title='CONFIRM'>" +
                    "</td><td class='messageCell'><div class='messageText'><span><h4>Success:</h4></span>" +
                    msg + "<br></div></td></tr></table></div>");
        }
        function startCheckStatus() {
            var jobId = $("#nu-jobId").text();
            if (!jobId) { return; }
            $("#nu-messages").empty();
            $("#nu-progressbar").progressbar({value: 0});
            $("#nu-progress").show();
            checkStatus(jobId);
        }
        function checkStatus(jobId) {
            {!ClassPrefix}InvoiceSet.getJobStatus(jobId, function(result, event) {
                if (event.status) {
                    if (result.Results) {
                        var successes = 0;
                        var errors = [];
                        for (var i = 0; i < result.Results.length; i++) {
                            var r = result.Results[i];
                            if (r.{!FieldPrefix}Success__c) {
                                successes++;
                            } else {
                                var inv = invoices[i];
                                var email = inv[2];
                                if (email.length && inv[3].length) {
                                    email += ',';
                                }
                                if (inv[3].length) {
                                    email += inv[3];
                                }
                                if (email.length) {
                                    email = "<i>(" + email + ")</i> ";
                                }
                                var error = "<a href='/" + inv[0] + "'>" + inv[1] + "</a> " + email + "- [" + r.{!FieldPrefix}StatusCode__c + "] ";
                                if (r.{!FieldPrefix}Message__c) {
                                    error += r.{!FieldPrefix}Message__c;
                                }
                                errors.push(error);
                            }
                        }
                        
                        $("#nu-messages").append(msgSuccess("Sent " + successes + " email(s) successfully"));
                        if (errors.length) {
                            $("#nu-messages").append(msgError("Failed to send " + errors.length + " emails", errors));
                        }
                        $("#nu-progress").hide();
                    } else {
                        $("#nu-progressbar").progressbar({value: result.ProcessedBatches / result.TotalBatches * 100});
                        setTimeout(function() { checkStatus(jobId) }, 1000);
                    }
                    r = result;
                } else {
                    $("#nu-messages").empty().append(msgError(event.message));
                    $("#nu-progress").hide();
                }
            });
        }
    </script>
    <apex:outputPanel rendered="{!!RenderPDF}">
    <div id="buttons">
        <apex:form id="InvoiceForm">
            <apex:pageMessages escape="false" />
            <div id="nu-messages"></div>
            <apex:outputPanel >
                <script type="text/javascript">
                    <apex:outputText value="{!invoiceDialogScript}" />
                </script>
            </apex:outputPanel>

            <apex:outputText value="{!$ObjectType.NU__Order__c.Fields.NU__InvoiceDate__c.Label}" />
            <apex:inputField value="{!dummyOrder.NU__InvoiceDate__c}" />
            
            <div>
                
                <apex:outputText value="{!$ObjectType.NU__Order__c.Fields.Invoice_Attention_To__c.Label}" />&nbsp;&nbsp;
                <apex:inputField value="{!dummyOrder.Invoice_Attention_To__c}" /> &nbsp;&nbsp;
                or if applicable available Invoice/Prime contact:&nbsp;&nbsp; <apex:selectList id="AdditionalInvoiceContacts"
                                     multiselect="false"
                                     size="1"
                                     value="{! AdditionalInvoiceContactsSelected }">
                            <apex:selectOption itemLabel="Please select a name" itemValue="" />
                            <apex:selectOptions value="{! AdditionalInvoiceContactsOpts }"/>
                        </apex:selectList>

            </div>
            
            <apex:actionStatus onStart="showPleaseWait()" onStop="hidePleaseWait();startCheckStatus()" id="AS" />

            <div class="buttons" style="clear: both; margin-top: 10px;">
                <apex:commandButton value="PDF"
                    action="{!PDF}"
                    styleClass="btn"
                    title="Sets the Invoice Date, if specified, and the Invoice Generated to true on all orders and then rerenders the page as a PDF." />
                <apex:commandButton value="Email"
                    action="{!Email}"
                    styleClass="btn leftmargin"
                    rendered="{!ShowEmailButton}"
                    title="Sets the Invoice Date, if specified, and the Invoice Generated to true on all orders and then emails the system Invoice Contact along with either the Bill To and/or Invoice email from the order."
                    status="AS"
                    rerender="InvoiceForm" />
                <apex:commandButton value="Print"
                    action="{!Print}"
                    styleClass="btn leftmargin"
                    title="Sets the Invoice Date, if specified, and the Invoice Generated to true on all orders and then allows a user to print them." />
                <apex:commandButton value="Update Order(s) Attention To"
                    action="{!updateAttentionTo}"
                    styleClass="btn leftmargin"
                    title="Sets the Invoice Attention To, if specified, on all orders so it can be shown in the Billing Address." />
                <apex:commandButton value="Cancel"
                    action="{!cancel}"
                    styleClass="btn leftmargin"
                    title="Cancel invoicing and return to the last page." />
            </div>
            
            <div id="nu-email">
                <span id="nu-jobId" style="display:none">{!JobId}</span>
                <div id="nu-progress">
                    Sending emails...
                    <div id="nu-progressbar"></div>
                </div>
                <table id="nu-results"></table>
                <script type="text/javascript">
                        $("#nu-progress").hide()
                        $("#nu-results").hide();
                </script>
            </div>
        </apex:form>
        <hr/>
        
        <div id="pleaseWaitModal" class="pleaseWaitModalHidden" title="Please wait...">
            <div style="text-align: center;">
                <apex:image value="{!URLFOR($Resource.NU__Loader)}" />
            </div>
        </div>
        <script type="text/javascript">
            $('#pleaseWaitModal').dialog({
                width: 155,
                modal: true,
                autoOpen: false,
                draggable: false,
                resizable: false,
                closeOnEscape: false,
                dialogClass: 'pleaseWait',
                open: function(event, ui) { $('.ui-dialog-titlebar-close').hide(); $('#pleaseWaitModal').removeClass('pleaseWaitModalHidden'); }
            });
            function showPleaseWait() {
                $('#pleaseWaitModal').dialog('open');
            }
            function hidePleaseWait() {
                $('#pleaseWaitModal').dialog('close');
            }
        </script>
    </div>
    </apex:outputPanel>
    <c:KentuckyInvoice id="KyComponent" orderlist="{!Orders}" />
    <!--  <c:CustomInvoice id="InvoiceComponent" orders="{!Orders}" />-->
</apex:page>