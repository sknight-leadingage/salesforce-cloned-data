/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestAdvertisingPricer {

	static void assertExpectedAdvertisingPrice(
		Id accountId,
		NU__Product__c prod,
		String priceClass,
		Decimal expectedPrice
	){
		TestMembershipPricingUtil.assertExpectedMembershipPricingCalculated(
		   accountId,
	       prod,
	       priceClass,
	       Date.Today(),
	       null,
	       expectedPrice);
	}

    static testMethod void corporateAllianceDiscountAppliedTest() {
    	Account individual = DataFactoryAccountExt.insertIndividualAccount();
    	
    	DataFactoryMembershipExt.insertCurrentCorporateAllianceMembership(individual.Id);

    	NU__Product__c advertisingProduct = DataFactoryProductExt.insertAdvertisingProduct();
    	
        Decimal expectedDiscountPrice = AdvertisingPricer.applyCorporateAllianceDiscount(advertisingProduct.NU__ListPrice__c);
        
        assertExpectedAdvertisingPrice(individual.Id, advertisingProduct, NU.Constant.PRICE_CLASS_MEMBER, expectedDiscountPrice);
    }
    
    static testmethod void noCorporateDiscountAppliedTest(){
    	Account individual = DataFactoryAccountExt.insertIndividualAccount();

    	NU__Product__c advertisingProduct = DataFactoryProductExt.insertAdvertisingProduct();
        
        assertExpectedAdvertisingPrice(individual.Id, advertisingProduct, NU.Constant.PRICE_CLASS_MEMBER, advertisingProduct.NU__ListPrice__c);
    }
}