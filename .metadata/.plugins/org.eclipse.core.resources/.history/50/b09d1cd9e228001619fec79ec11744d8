public with sharing abstract class ConferenceManagementControllerBase {
    public static final String EVENT_ID_QUERY_STRING_NAME = 'eventId';
    public static final String SPEAKER_ID_QUERY_STRING_NAME = 'SpeakerId';
    
    public ConferenceManagementControllerBase(){
        currentPageNum = 1;
    }
    
    public Id EventId{
        get{
            return ApexPages.CurrentPage().getParameters().get(EVENT_ID_QUERY_STRING_NAME); 
        }
    }
    
    private NU__Event__c conferencePriv = null;
    public NU__Event__c Conference{
        get{
            if (conferencePriv == null){
                conferencePriv = EventQuerier.getConferenceById(EventId);
            }
            
            return conferencePriv;
        }
    }
    
    public abstract List<Sobject> getConferenceObjects();
    
    public SObject CurrentConferenceObjectPriv { get; set; }
    
    public SObject CurrentConferenceObject{
        get{
            if (CurrentConferenceObjectPriv == null){
            
                List<SObject> objects = con.getRecords();
                
                if (objects.size() > 0){
                    CurrentConferenceObjectPriv = objects[0];
                }
            }
            
            return CurrentConferenceObjectPriv;
        }
        set { CurrentConferenceObjectPriv = value; }
    }
    
    public List<Conference_Available_Track__c> AvailableTracks{
        get{
            return [select id,
                           Conference_Track__c,
                           Conference_Track__r.Name
                      from Conference_Available_Track__c
                     where Conference__c = :EventId];
        }
    }
    
    public List<SelectOption> AvailableTracksSelectOpts{
        get{
            List<SelectOption> opts = new List<SelectOption>();
            
            List<Conference_Available_Track__c> temp = new List<Conference_Available_Track__c>();
            temp.addAll(AvailableTracks);
            temp.sort();
            
            for (Conference_Available_Track__c availTrack : temp){
                SelectOption opt = new SelectOption(availTrack.Conference_Track__c, availTrack.Conference_Track__r.Name);
                opts.add(opt);
            }
            
//            opts.sort();
            
            return opts;
        }
    }
    
    
    // Grabbed the following paging code from http://blog.jeffdouglas.com/2009/07/14/visualforce-page-with-pagination/
    
    // instantiate the StandardSetController from a query locator
    Transient public ApexPages.Standardsetcontroller transientController { get; set; }
    
    public ApexPages.StandardSetController con {
        get {
            if(transientController == null) {
                transientController = new ApexPages.StandardSetController( getConferenceObjects() );

                // sets the number of records in each page set
                transientController.setPageSize(1);
            }
            return transientController;
        }
        set { transientController = value; }
    }
 
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return currentPageNum < ResultSize;
        }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return currentPageNum > 1;
        }
        set;
    }
 
    // returns the page number of the current page set
    public Integer currentPageNum { get; set; }
    
    public Integer pageNumber {
        get {
            return currentPageNum;
        }
        set {
            currentPageNum = value;
            con.SetPageNumber( currentPageNum );
        }
    }
    
    public Integer ResultSize {
        get {
            return con.getResultSize();
        }
    }
    
    public virtual void onPageChange() { }
    
    public void changeToPage(Integer newPageNumber){
        pageNumber = newPageNumber;
        CurrentConferenceObject = null;
        onPageChange();
    }
 
    // returns the first page of records
    public void first() {
        changeToPage(1);
    }
 
    // returns the last page of records
    public void last() {
        changeToPage(ResultSize);
    }
 
    // returns the previous page of records
    public void previous() {
        changeToPage(currentPageNum - 1);
    }
 
    // returns the next page of records
    public void next() {
        changeToPage(currentPageNum + 1);
    }
 
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
    }
    
    public virtual void save(){
        try{
            upsert CurrentConferenceObject;
            refresh();
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    
    public virtual void deleteCurrentConferenceObject(){
        try{
            delete CurrentConferenceObject;
            
            if (hasNext == false){
                PageNumber = PageNumber - 1;
            }
            
            refresh();
        }
        catch(Exception Ex){
            ApexPages.addMessages(ex);
        }
    }
    
    public void refresh(){
        CurrentConferenceObject = null;
        Integer currentPageNumber = pageNumber;
        con = null;
        pageNumber = currentPageNumber;
    }
    
    public void refreshAtPage1(){
        refresh();
        pageNumber = 1;
    }
}