public with sharing class CartItemLineTriggerHandlers extends NU.TriggerHandlersBase {
    public override void onBeforeInsert(List<sObject> newRecords){
    	List<NU__CartItemLine__c> newCartItemLines = (List<NU__CartItemLine__c>)newRecords;
    	
    	/* LF 5/1/2014: NU Case 38810. When an exhibitor order
           is adjusted and the cart is created, the exhibitor information
           is not populated on the corresponding exhibitor cart item lines.
           When that cart is resubmitted, the exhibitor information is cleared
           because the order submission code doesn't have the exhibitor info.
           
           To resolve this, the exhibitor info has to be populated on the
           exhibitor cart item lines so the order submission code has it.
    	*/
    	addMissingExhibitorInformationOnOrderAdjustment(newCartItemLines);
    }
    
    private void addMissingExhibitorInformationOnOrderAdjustment(List<NU__CartItemLine__c> newCartItemLines){
    	Set<Id> orderItemLineIds = NU.CollectionUtil.getLookupIds(newCartItemLines, 'NU__OrderItemLine__c');
    	
    	if (NU.CollectionUtil.setHasElements(orderItemLineIds)){
    		Map<Id, NU__OrderItemLine__c> oils = getOrderItemLinesMap(orderItemLineIds);
    		
    		for (NU__CartItemLine__c newCIL : newCartItemLines){
    			NU__OrderItemLine__c oil = oils.get(newCIL.NU__OrderItemLine__c);
    			
    			if (oil != null &&
    			    oil.Exhibitor__c != null){
    				
    				newCIL.Booked_By__c = oil.Exhibitor__r.Booked_By__c;
    				newCIL.Booked_Date__c = oil.Exhibitor__r.Booked_Date__c;
    				newCIL.Booth_Number__c = oil.Exhibitor__r.Booth_Number__c;
    			}
    		}
    	}
    }
    
    private Map<Id, NU__OrderItemLine__c> getOrderItemLinesMap(Set<Id> orderItemLineIds){
    	return new Map<Id, NU__OrderItemLine__c>(
    	       [SELECT id,
    	               Name,
    	               Exhibitor__c,
    	               Exhibitor__r.Booked_By__c,
    	               Exhibitor__r.Booked_Date__c,
    	               Exhibitor__r.Booth_Number__c
    	          FROM NU__OrderItemLine__c
    	         WHERE Id in :orderItemLineIds]); 
    }
}