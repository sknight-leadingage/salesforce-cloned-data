public with sharing abstract class OrderPurchaseBase {
    private Boolean initialized = false;
    
    private Map<String,String> params = ApexPages.currentPage().getParameters();
    
    public Boolean getIsEdit() { 
        return params.containsKey('cartItemId');
    }
    
    public String Subheader {
        get {
            if (Cart.NU__BillTo__c == null) {
                return ' ';
            }
            return (Cart.NU__Order__c == null ? 'Order for ' : 'Adjustment for ') + Cart.NU__BillTo__r.Name;
        }
    }
    
    public Boolean IsAdjustmentCartItem {
    	get { return CartItem != null && CartItem.NU__OrderItem__c != null; }
    }
    
    public NU__Cart__c Cart { get; set; }
    public NU__CartItem__c CartItem { get; set; }
    
    public virtual Integer getUnitPriceMaxLength() { return 8; }
    public virtual Integer getQuantityMaxLength() { return 6; }
    
    public abstract String getRecordType();
    
    public virtual PageReference Cancel() {
        PageReference ref = new PageReference('/apex/NU__Order' + getParams());
        ref.setRedirect(true);
        return ref;
    }
    
    public virtual PageReference Save() {
        System.SavePoint sp = Database.setSavePoint();
        
        try {
            SaveCart();
            SaveCartItem();

            String itemsSaved = SaveItemLines();
                
            if (itemsSaved == null) {
                PageReference ref = new PageReference('/apex/NU__Order' + getParams());
                ref.setRedirect(true);
                return ref;
            }
            else {
                ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error, itemsSaved);
                ApexPages.addMessage(pageMsg);
                
                Database.rollback(sp);
                RollbackCart();
                RollbackCartItem();
            }
        }
        catch (Exception e) {
            ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error, e.getMessage());
            ApexPages.addMessage(pageMsg);
            
            Database.rollback(sp);
            RollbackCart();
            RollbackCartItem();
            //RollbackItemLines(); - not needed?
        }
        
        return null;
    }
    
    public String getParams() {
        String result = ''; 
        if (Cart.Id != null) {
            result += '&id=' + Cart.Id;
        }
        if (params.get('orderItemId') != null && Cart.Id == null) {
            result += '&orderItemId=' + (Id)params.get('orderItemId');
        }
        if (result.length() > 0) {
            result = '?' + result.substring(1);
        }
        return result;
    }
    
    private NU__Cart__c cartClone;
    public void SaveCart() {
        cartClone = Cart.clone(true);
        upsert Cart; // returns UpsertResult?
        //return CartManager.Instance.saveCarts(new List<Cart__c> { Cart });
    }
    
    public void RollbackCart() {
        if (cartClone != null) {
            Cart = cartClone;
            cartClone = null;
        }
    }
    
    private NU__CartItem__c cartItemClone;
    public virtual void SaveCartItem() {
        if (CartItem.Id == null) {
            CartItem.NU__Cart__c = Cart.Id;
        }
        cartItemClone = CartItem.clone(true);
        
        upsert CartItem; // returns UpsertResult?
        //return CartItemManager.Instance.saveCartItems(new list<CartItem__c> { CartItem });
    }
    
    public virtual void RollbackCartItem() {
        if (cartItemClone != null) {
            CartItem = cartItemClone;
            cartItemClone = null;
        }
    }
    
    public virtual String SaveItemLines() {
        Id cartItemId = CartItem.Id;
        List<NU__CartItemLine__c> cartItemLines = getProductItemLines();
        List<NU__CartItemLine__c> itemLinesToAdd = new List<NU__CartItemLine__c>();
        List<NU__CartItemLine__c> itemLinesToRemove = new List<NU__CartItemLine__c>();
        
        for (NU__CartItemLine__c itemLine : cartItemLines) {
            if (itemLine.Id == null) {
                itemLine.NU__CartItem__c = cartItemId;
            }
            
            if (itemLine.NU__IsInCart__c) {
                itemLinesToAdd.add(itemLine);
            } else if (itemLine.Id != null) {
                itemLinesToRemove.add(itemLine);
            }
        }
        
        if (itemLinesToAdd.size() > 0) {
            //result = saveCartItemLines(itemLinesToAdd);
            upsert itemLinesToAdd; // returns UpsertResult?
            
            if (itemLinesToRemove.size() > 0) {
                //result = removeCartItemLines(itemLinesToRemove);
                delete itemLinesToRemove; // returns DeleteResult?
            }
            return null;            
        } else {
            return 'At least one item must be selected';
        }
    }
    
    public OrderPurchaseBase() {      
        if (params.get('id') == null) {
            Cart = new NU__Cart__c();
            if (params.get('billTo') != null) {
                Cart.NU__BillTo__c = params.get('billTo');
                Cart.NU__BillTo__r = [SELECT Name FROM Account WHERE Id = :Cart.NU__BillTo__c];
            }
            
            if (params.get('entity') != null) {
                Cart.NU__Entity__c = params.get('entity');
            }
            else {
                List<NU__Entity__c> entities = [SELECT Id, Name
                   FROM NU__Entity__c];
                if (entities != null && entities.size() > 0) {
                    Cart.NU__Entity__c = entities[0].Id;
                }
            }
        }
        else {
            Cart = [SELECT
                NU__BillTo__c,
                NU__BillTo__r.Name,
                NU__Order__c,
                NU__Entity__c,
                NU__TransactionDate__c
                FROM NU__Cart__c
                WHERE Id = :params.get('id')];
        }
            
        if (!getIsEdit()) {
            Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
            CartItem = new NU__CartItem__c(NU__Cart__c = params.get('id'), RecordTypeId = rt.get( getRecordType() ).getRecordTypeId());
            CartItem.NU__Customer__c = Cart.NU__BillTo__c;
            updatePriceClass();
        }
        else if (getIsEdit()) {
            //Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
            CartItem = new NU__CartItem__c(Id = params.get('cartItemId'));
            CartItem.NU__Customer__c = Cart.NU__BillTo__c;
            updatePriceClass();
        }
    }
    
    public void OnAccountSelected() {
        updatePriceClass();
        OnPriceClassSelected();
    }
    
    public void OnPriceClassSelected() {
        if (CartItem.NU__PriceClass__c != null && productItemLines != null) {
            List<NU__Product__c> products = new List<NU__Product__c>();
            List<NU__CartItemLine__c> allProductItemLines = new List<NU__CartItemLine__c>();
            products = getActiveEntityOrderTypeProducts();
            allProductItemLines = getProductPricing(products);
            
            for (NU__CartItemLine__c product : productItemLines) {
                for (NU__CartItemLine__c productPrice : allProductItemLines) {
                    if (product.NU__Product__c == productPrice.NU__Product__c) {
                        product.NU__UnitPrice__c = productPrice.NU__UnitPrice__c;
                    }
                }
            }
        }
    }
    
    public virtual List<NU__Product__c> getActiveEntityOrderTypeProducts(){
    	return [SELECT NU__TrackInventory__c,
                	   NU__InventoryOnHand__c
                  FROM NU__Product__c
                 WHERE NU__Status__c = 'Active'
                   AND NU__Entity__c = :Cart.NU__Entity__c
                   AND RecordType.Name = :getRecordType()];
    }
    
    protected List<NU__CartItemLine__c> productItemLines = null;
    public virtual List<NU__CartItemLine__c> getProductItemLines() {
        List<NU__Product__c> products = new List<NU__Product__c>();
        List<NU__CartItemLine__c> allProductItemLines = new List<NU__CartItemLine__c>();
        if (productItemLines != null) {
            return productItemLines;
        }
        
        if (CartItem.Id != null || (CartItem.NU__PriceClass__c != null && productItemLines == null)) {
            products = getActiveEntityOrderTypeProducts();
            allProductItemLines = getProductPricing(products);
            
            if (CartItem.Id != null) { // editing cart item
                List<NU__CartItemLine__c> currentItemLines = [SELECT Id,
                    NU__Product__c,
                    NU__CartItem__c,
                    NU__Product__r.NU__TrackInventory__c,
                    NU__Product__r.NU__InventoryOnHand__c,
                    NU__Quantity__c,
                    NU__IsInCart__c,
                    NU__UnitPrice__c,
                    NU__Data__c,
                    Booth_Number__c,
                    Booked_By__c,
                    Booked_Date__c,
                    NU__OrderItemLine__r.Exhibitor__r.Booked_By__c,
                    NU__OrderItemLine__r.Exhibitor__r.Booked_Date__c,
                    NU__OrderItemLine__r.Exhibitor__r.Booth_Number__c
                    FROM NU__CartItemLine__c
                    WHERE NU__CartItem__r.Id = :CartItem.Id];
                
                Integer i = 0;  
                for (NU__CartItemLine__c product : allProductItemLines) {
                    for (NU__CartItemLine__c currentIL : currentItemLines) {
                        if (product.NU__Product__c == currentIL.NU__Product__c) {
                            allProductItemLines.set(i, currentIL);
                        } 
                    }
                    i++;
                }
            }
            
            productItemLines = allProductItemLines;
        }
        return productItemLines;
    }
    
    public virtual void ReloadProductItemLines(){
    	productItemLines = null;
    }
    
    public List<NU__CartItemLine__c> getProductPricing(List<NU__Product__c> products) {
        List<NU__CartItemLine__c> productCILs = new List<NU__CartItemLine__c>();
        List<NU.ProductPricingInfo> productPricingInfos = new List<NU.ProductPricingInfo>();
        for (NU__Product__c product : products) {
            NU.ProductPricingInfo ppi = new NU.ProductPricingInfo();
            ppi.ProductId = product.Id;
            ppi.Quantity = 1;
            productPricingInfos.add(ppi);
        }
            
        NU.PriceClassRequest pcr = getPriceClassRequest();
        NU.ProductPricingRequest ppr = getProductPricingRequest(pcr);
        ppr.ProductPricingInfos = productPricingInfos;
        
        Map<Id,Decimal> unitPrices = NU.PriceClassManager.getPricingManager().GetProductPrices(ppr);
            
        for (NU__Product__c product : products) {
            Decimal unitPrice = unitPrices.get(product.Id);
            productCILs.add(new NU__CartItemLine__c(NU__Product__c = product.Id, NU__Quantity__c = 1, NU__UnitPrice__c = unitPrice));
        }
        
        return productCILs;
    }
    
    private List<SelectOption> priceClassOptions = null;
    public virtual List<SelectOption> getPriceClassOptions() {
        if (priceClassOptions == null) {
            priceClassOptions = new List<SelectOption>();
            for (NU__PriceClass__c priceClass : getPriceClasses().values()) {
                priceClassOptions.add(new SelectOption(priceClass.Id, priceClass.Name));
            }
        }
        return priceClassOptions;
    }
    
    private Map<String, NU__PriceClass__c> priceClassesByName = null;
    public virtual Map<String, NU__PriceClass__c> getPriceClassesByName() {
        if (priceClassesByName == null) {
            priceClassesByName = new Map<String, NU__PriceClass__c>();
            for (NU__PriceClass__c priceClass : getPriceClasses().values()) {
                priceClassesByName.put(priceClass.Name, priceClass);
            }
        }
        return priceClassesByName;
    }
    
    private Map<Id, NU__PriceClass__c> priceClasses = null;
    public virtual Map<Id, NU__PriceClass__c> getPriceClasses() {
        if (priceClasses == null) {
            priceClasses = new Map<Id, NU__PriceClass__c>([SELECT Name FROM NU__PriceClass__c]);
        }
        return priceClasses;
    }
    
    public void updatePriceClass() {
        if (CartItem.NU__Customer__c != null) {
            NU.PriceClassRequest pcr = getPriceClassRequest();
            CartItem.NU__PriceClass__c = getPriceClassesByName().get(NU.PriceClassManager.getPricingManager().getPriceClass(pcr)).Id;
        }
    }
    
    public NU.PriceClassRequest getPriceClassRequest() {
        NU.PriceClassRequest pcr = new NU.PriceClassRequest();
        pcr.TransactionDate = Cart.NU__TransactionDate__c;
        pcr.AccountId = CartItem.NU__Customer__c;
        pcr.CartId = Cart.Id;
        return pcr;
    }
    
    public NU.ProductPricingRequest getProductPricingRequest(NU.PriceClassRequest pcr) {
        NU.ProductPricingRequest ppr = new NU.ProductPricingRequest();
        //ppr.ProductPricingInfos = productPricingInfos;
        ppr.AccountId = CartItem.NU__Customer__c;
        ppr.TransactionDate = Cart.NU__TransactionDate__c;
        ppr.CartId = Cart.Id;
        ppr.PriceClassName = getPriceClassesByName().get(NU.PriceClassManager.getPricingManager().getPriceClass(pcr)).Name;
        return ppr;
    }
}