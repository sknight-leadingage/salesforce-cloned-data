/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBatchUpdateStatePartnerName {

    static testMethod void StartUnitTest() {
    	Account StatePartnerAccount =  DataFactoryAccountExt.insertStatePartnerAccount();
    	StatePartnerAccount.Name = 'LeadingAge Test';
    	update StatePartnerAccount;
    	Account multiSiteAccount =  DataFactoryAccountExt.insertMultiSiteAccount(100000000);
        Account providerAccount = DataFactoryAccountExt.insertProviderAccount(10000);
        Account individualAccount = DataFactoryAccountExt.insertIndividualAccount();
        
        //NU.DataFactoryAffiliation.insertAffiliation(multiSiteAccount.Id, StatePartnerAccount.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(providerAccount.Id, multiSiteAccount.Id, true);
        NU.DataFactoryAffiliation.insertAffiliation(individualAccount.Id, providerAccount.Id, true);
        
        multiSiteAccount.State_Partner_ID__c = StatePartnerAccount.id; 
        update multiSiteAccount;
        providerAccount.State_Partner_ID__c = StatePartnerAccount.id; 
        update providerAccount;
        
        Account mso = [SELECT  Id, RecordTypeId, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c 
							   FROM Account  WHERE Id = :multiSiteAccount.Id];
        Account prov = [SELECT  Id, RecordTypeId, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							    NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
							    NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							    NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c 
							    FROM Account  WHERE Id = :providerAccount.Id];
        Account ind = [SELECT  Id, RecordTypeId, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c 
							   FROM Account  WHERE Id = :individualAccount.Id];
        
        
        System.assertNotEquals(null, ind.NU__PrimaryAffiliation__c);
        System.assertNotEquals(null, prov.NU__PrimaryAffiliation__c);

        
        
        System.assertEquals(null,mso.State_Partner_Name__c);
        
        System.assertEquals(null,prov.State_Partner_Name__c);
        System.assertEquals(null,ind.State_Partner_Name__c);
        System.assertEquals(null,prov.Parent_State_Partner_Name__c);
        System.assertEquals(null,ind.Parent_State_Partner_Name__c);
        System.assertEquals(null,prov.Grandparent_State_Partner_Name__c);
        System.assertEquals(null,ind.Grandparent_State_Partner_Name__c);
        

        System.assertEquals(null,prov.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c);
        System.assertEquals(null,ind.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c);
        System.assertEquals(null,prov.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c);
        System.assertEquals(null,ind.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c);        

		Test.startTest();
        BatchUpdateStatePartnerNames c = new BatchUpdateStatePartnerNames();
        Database.executeBatch(c);
        Test.stopTest();
        
        Account mso2 = [SELECT  Id, RecordTypeId, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c 
							   FROM Account  WHERE Id = :multiSiteAccount.Id];
        Account prov2 = [SELECT  Id, RecordTypeId, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							    NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
							    NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							    NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c 
							    FROM Account  WHERE Id = :providerAccount.Id];
        Account ind2 = [SELECT  Id, RecordTypeId, State_Partner_ID__c, State_Partner_ID__r.Name, State_Partner_Name__c, Parent_State_Partner_Name__c, Grandparent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c,
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c, 
							   NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Parent_State_Partner_Name__c, NU__PrimaryAffiliation__r.NU__PrimaryAffiliation__r.Grandparent_State_Partner_Name__c 
							   FROM Account  WHERE Id = :individualAccount.Id];
		
        System.assertEquals( 'LeadingAge Test', mso2.State_Partner_Name__c);					   
        System.assertEquals('LeadingAge Test',prov2.State_Partner_Name__c);
        System.assertEquals(null,ind2.State_Partner_Name__c);
        System.assertEquals('LeadingAge Test',prov2.Parent_State_Partner_Name__c);
        System.assertEquals('LeadingAge Test',ind2.Parent_State_Partner_Name__c);
        System.assertEquals(null,prov2.Grandparent_State_Partner_Name__c);
        System.assertEquals('LeadingAge Test',ind2.Grandparent_State_Partner_Name__c);
        

        System.assertEquals('LeadingAge Test',prov2.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c);
        System.assertEquals('LeadingAge Test',ind2.NU__PrimaryAffiliationRecord__r.Parent_State_Partner_Name__c);
        System.assertEquals(null,prov2.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c);
        System.assertEquals('LeadingAge Test',ind2.NU__PrimaryAffiliationRecord__r.Grandparent_State_Partner_Name__c);         
    }
}