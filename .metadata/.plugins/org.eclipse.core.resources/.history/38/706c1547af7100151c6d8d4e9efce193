<apex:page controller="EduLinkSpeaker_Controller">
<style>
    div.pbSubheader h3 { color: #000000; }
</style>
<apex:pageMessages />
    <apex:form id="ConfernceProposalsForm">
        <apex:pageBlock title="{! Conference.Name }: Speaker Link">
            <!-- <p>On this page you'll take a speaker submitted during the Call for Sessions process and find their account in Nimble AMS, or create an account if the speaker is new to LeadingAge.</p> -->
            <apex:pageBlockSection columns="1" title="Why this Page?">
                <p>When a proposal is submitted through the Call for Sessions process with additional speakers, we store all the info the submitter enters about the additional speakers but we don't know who those people are in Nimble AMS.  We need to know that because all the reporting and other parts of the Session Planner rely on every speaker having an Account record in Nimble.  This page is designed to allow you to tell the Session Planner that, for example, the additional speaker entered during Call for Sessions as Bill Smith is in Nimble AMS as William Smith, III.</p>
            </apex:pageBlockSection>
            <br />
            <apex:pageBlockSection columns="2" title="Section 1: Speaker Details Entered in Call for Sessions">
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Full_Name__c }" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Email__c }" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Company_Name_Rollup__c }" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Address__c }" />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="2" title="Section 1A: Speaker Already Linked" rendered="{! ProposalSpeaker.Speaker__r.Account__c != null }">
                <apex:outputText value="<p>This speaker is already linked to a Nimble AMS account.  Please review the details.</p><br />" escape="false" />
                <apex:outputText value="<p>If you want to link this speaker to a different Nimble AMS account then proceed to the below section.</p><br />" escape="false" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Account__r.Name }" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Account__r.PersonEmail }" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Account__r.Speaker_Company_Name_Rollup__c }" />
                <apex:outputField value="{! ProposalSpeaker.Speaker__r.Account__r.Speaker_Address__c }" />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="1" title="Section 2, Option 1: Find this Speaker's Nimble AMS Account Below" id="FindNimbleAccount">
                <apex:outputText value="<p>Below is a list of Accounts that <strong>MAY</strong> match the speaker listed in section 1.  Remember the objective is to find the Account record that belongs to the person listed in section 1.  If one of the matches below looks like it is that person's record, then click on the name in this list to assign the lookup.  If there aren't any records below that are this speaker's record, then you should try to find them using the Nimble Global Search (Search box at the very top of the page) and if that doesn't turn up any good matches, you'll need to create a new account for the person and then return to this page to assign them.</p><br />" escape="false" />
                
                <apex:pageBlockTable value="{!MatchedSpeakerList}" var="item">
                    <apex:column headerValue="Speaker Full Name">
                      <apex:outputtext style="cursor: pointer;" value="{! item.Name }" />
                      <apex:actionSupport action="{!selectItem}" event="onclick" rerender="FindNimbleAccount">
                          <apex:param name="selAccId" value="{!item.Id}" assignTo="{!selectedSpeakerId}" />
                      </apex:actionSupport>
                    </apex:column>
                    <apex:column value="{!item.LeadingAge_ID__c}" />
                    <apex:column value="{!item.PersonEmail}" />
                    <apex:column value="{!item.Speaker_Company_Name_Rollup__c}" />
                </apex:pageBlockTable>

                <apex:outputText value="<p>To assign a lookup from this list, simply click the speaker's name in the list.</p><br />" escape="false" />
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="1" title="Section 2, Option 2: Manually Link to the Account">
                <apex:outputText value="<p>If you already know the correct LeadingAge ID for the person listed in section 1, you can enter it below and click <strong>Save</strong> to assign the lookup.</br >Note that you only need to use one of the options in section 2 of this page to assign a lookup.</p><br />" escape="false" />
                <apex:inputText value="{!LeadingAgeManualLinkID}" title="LeadingAge ID" label="LeadingAge ID"/>
                <!--
                <apex:pageblockSectionItem >
                    <apex:inputCheckbox value="{!ResetSpeakerCallRecord}" label="Reset Call for Session Speaker Entry" /> Check this box to create a new Call for Session Speaker record based on the Account record that you're assigning.  Most of the time you should <strong>NOT</strong> check this box.  It is mainly used to reset the entered information when the original submitter record needs to be changed.
                </apex:pageblockSectionItem>
                -->
                <apex:pageblockSectionItem >
                    <apex:outputLabel value=" " />
                    <apex:commandButton action="{!ManuallyLinkID}" value="Save"/>
                </apex:pageblockSectionItem>
            </apex:pageBlockSection>
        </apex:pageblock>
    </apex:form>
</apex:page>