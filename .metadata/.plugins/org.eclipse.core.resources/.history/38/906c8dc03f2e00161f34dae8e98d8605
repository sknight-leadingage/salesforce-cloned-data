public with sharing class EduHubListing_Controller extends EduReportsControllerBase {
    public List<Conference_Session__c> SessionData {get;set;}
    
    public EduHubListing_Controller() {
        iWizardMax = 6;
        WizardStep = 1;
    }

    public override string getPageTitle() {
        string sPT = '';
        if (Conference != null) {
            sPT = Conference.Name;
        }
        if (getMType() == 'hub-sps') {
            sPT = sPT + ': Speaker Profile Status';
        }
        if (getMType() == 'hub-avs') {
            sPT = sPT + ': Audio Visual Requests Status';
        }
        if (getMType() == 'hub-pms') {
            sPT = sPT + ': Presentation Materials Status';
        }
        if (RTypeSel == null) {
            sPT = sPT + ': Session Listing';
        }
        else {
			sPT = sPT + ': ' + RTypeSel;
        }
        return sPT;
    }

    public string SLIST_SNUMBER = 'Session Listing (Order by Session #)';
    public string SLIST_STIMESLOT = 'Session Listing (Group by Timeslot)';
    public string SLIST_STITLE = 'Session Listing (Order by Title)';
    public string SLIST_SSTAFF = 'Session Listing (Group by Edu Staff)';
    public string SLIST_SSTATUS = 'Session Listing (Group by Completed Status)';

    public override List<SelectOption> getRTypeItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(SLIST_SNUMBER, SLIST_SNUMBER));
        options.add(new SelectOption(SLIST_STIMESLOT, SLIST_STIMESLOT));
        options.add(new SelectOption(SLIST_STITLE, SLIST_STITLE));
        options.add(new SelectOption(SLIST_SSTAFF, SLIST_SSTAFF));
        options.add(new SelectOption(SLIST_SSTATUS, SLIST_SSTATUS));
        return options; 
    }

    public void SetQuery() {
        GroupHeaderController.SecondGroupTotals = null;
        if (RTypeSel == SLIST_SNUMBER) {
            GroupHeaderController.GroupTotals = null;
	        if (getMType() == 'hub-sps') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, (SELECT Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Bio_Validated__c FROM Conference_Session_Speakers__r WHERE Role__c = 'Speaker' OR Role__c = 'Key Contact') FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-avs') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-pms') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_Uploads_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Number_Numeric__c];
	        }
        }
        else if (RTypeSel == SLIST_STIMESLOT) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Timeslot__r.Timeslot_Code__c g FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Timeslot__r.Timeslot_Code__c ORDER BY Timeslot__r.Timeslot_Code__c];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('g'), (integer)ar.get('sc'));
            }
	        if (getMType() == 'hub-sps') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, (SELECT Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Bio_Validated__c FROM Conference_Session_Speakers__r WHERE Role__c = 'Speaker' OR Role__c = 'Key Contact') FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-avs') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-pms') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_Uploads_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c];
	        }
        }
        else if (RTypeSel == SLIST_STITLE) {
            GroupHeaderController.GroupTotals = null;
	        if (getMType() == 'hub-sps') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, (SELECT Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Bio_Validated__c FROM Conference_Session_Speakers__r WHERE Role__c = 'Speaker' OR Role__c = 'Key Contact') FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Title__c];
	        }
	        if (getMType() == 'hub-avs') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Title__c];
	        }
	        if (getMType() == 'hub-pms') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_Uploads_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Title__c];
	        }
        }
        else if (RTypeSel == SLIST_SSTAFF) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Education_Staff__r.Name g FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Education_Staff__r.Name ORDER BY Education_Staff__r.Name];
            for (AggregateResult ar : Totals ) {
                GroupHeaderController.GroupTotals.put((string)ar.get('g'), (integer)ar.get('sc'));
            }
	        if (getMType() == 'hub-sps') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, (SELECT Speaker__r.FirstName,Speaker__r.LastName,Speaker__r.Speaker_Bio_Validated__c FROM Conference_Session_Speakers__r WHERE Role__c = 'Speaker' OR Role__c = 'Key Contact') FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Education_Staff__r.Name, Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-avs') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Education_Staff__r.Name, Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-pms') {
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_Uploads_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Education_Staff__r.Name, Session_Number_Numeric__c];
	        }
        }
        else if (RTypeSel == SLIST_SSTATUS) {
            GroupHeaderController.GroupTotals = new Map<string,integer>();
	        if (getMType() == 'hub-sps') {
	            SessionInfo = null;
	        }
	        if (getMType() == 'hub-avs') {
	            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Session_AV_Validated__c g FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Session_AV_Validated__c ORDER BY Session_AV_Validated__c];
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_AV_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_AV_Validated__c, Session_Number_Numeric__c];
	        }
	        if (getMType() == 'hub-pms') {
	            List<AggregateResult> Totals = [SELECT COUNT(Id) sc, Session_Uploads_Validated__c g FROM Conference_Session__c WHERE Conference__c = :EventId GROUP BY Session_Uploads_Validated__c ORDER BY Session_Uploads_Validated__c];
	            SessionInfo = [SELECT Session_Number__c, Timeslot__r.Timeslot_Code__c, Title__c, Education_Staff__r.Name, Session_Uploads_Validated__c FROM Conference_Session__c WHERE Conference__c = :EventId ORDER BY Session_Uploads_Validated__c, Session_Number_Numeric__c];
	        }
        }
    }

    private string mtype{get;set;}
    public string getMType() {
        if (mtype == null || mtype == '') {
            mtype = ApexPages.CurrentPage().getParameters().get('mt');
            if (mtype == null) {
                mtype = '';
            }
        }
        return mtype;
    }
    public void setMType(string s) {
        mtype = s;
    }

    public List<Conference_Session__c> SessionInfo { get; set; }
    public integer SessionInfoSize {
        get {
            if (SessionInfo == null) {
                return 0;
            } else {
                return SessionInfo.size();
            }
        }
    }
    
    public List<Conference_Proposal__c> ProposalInfo { get; set; }
    public integer ProposalInfoSize {
        get {
            if (ProposalInfo == null) {
                return 0;
            } else {
                return ProposalInfo.size();
            }
        }
    }
    
    // ******* BEGIN: Wizard Setup Code *********
    public override void WizardNext(){
        if (RTypeSel == SLIST_SNUMBER) {
            WizardStep = 2;
        } else if (RTypeSel == SLIST_STIMESLOT) {
            WizardStep = 3;
        } else if (RTypeSel == SLIST_STITLE) {
            WizardStep = 4;
        } else if (RTypeSel == SLIST_SSTAFF) {
            WizardStep = 5;
        } else if (RTypeSel == SLIST_SSTATUS) {
            WizardStep = 6;
        }
        SetQuery();
    }
    
    public override void WizardBack(){
        WizardStep = 1;
        RTypeSel = null;
    }
    // ******* END: Wizard Setup Code *********
    


}