<apex:page controller="CustomStateProviderRevenueController">
<apex:stylesheet value="{!URLFOR($Resource.LeadingAgeStyle, 'LeadingAge.css')}"/>
    <apex:form >
    <h1>Program Revenue Entry Form</h1>
    <br/><br/>

<!--
    <p>User Ids:
    currentUser.Id: {!currentUser.Id}<br />
    currentUser.Contact.Id: {!currentUser.Contact.Id}<br />
    currentUser.Contact.Account.Id: {!currentUser.Contact.Account.Id}<br />
    StatePartnerId: {!StatePartnerId}</p>
-->
    
    <apex:selectList value="{!states}" multiselect="false" size="1" rendered="{!isUserLeadingAgeStaff}">
        <apex:selectOption itemValue="-1" itemLabel=" -- Select State --" />
        <apex:selectOptions value="{!items}"/>
        <apex:actionSupport event="onchange" action="{!State_onChange}" />
    </apex:selectList>
    <apex:outputText rendered="{!isUserLeadingAgeStaff}"><br/><br/></apex:outputText>
    <apex:outputText rendered="true" escape="false" value="{!debugString}"><br/><br/></apex:outputText>
    <strong>INSTRUCTIONS: </strong>
    This form can be used to easily update program revenue information for all your members at once. When you are done entering data, click the "Save Changes" button at the bottom of the page. You can visit this page as often as you like, providing more data each time. Once you've provided program revenue data for a given company, you cannot change that company's data.
    <br/><br/>
    <apex:pageMessages showDetail="true" />
    <br/><br/>
    <apex:outputText rendered="{!displayConfirmMSG}"><span style="color:red;font-weight:bold;'">Thank you for submitted your changes.</span><br/><br/></apex:outputText>
    <span style="color:red;font-weight:bold;'">
        <apex:outputText rendered="{!displayErrorMSG}">The following companies could not be saved. Please review your submission below:.<br/></apex:outputText>
        <apex:outputText rendered="{!displayErrorMSG}" escape="false" value="{!CompanyError}"></apex:outputText>
    </span>    
    
      <apex:pageBlock title="Program Revenue">
          <apex:commandButton action="{!Save}" value="Save"/>
          <br /><br />
          <!--MSO Accounts-->
          <apex:PageBlockTable value="{!MSOAccounts}" var="a" rendered="{!showMSOList}" >
              
              <apex:column headervalue="MSO Account">
                  <apex:outputlink value="/{!a.id}" target="_blank">{!a.name}</apex:outputlink>
              </apex:column>
              
              <apex:column headervalue="City"  width="150px">
                  <apex:outputtext value="{!a.BillingCity}" />
              </apex:column>
              <apex:column headervalue="State"  width="50px">
                  <apex:outputtext value="{!a.BillingState}" />
              </apex:column>               
                         
              <apex:column headervalue="{! $ObjectType.Account.Fields.Revenue_Year_Submitted__c.Label}" width="150px">
                  <apex:inputText value="{!a.Revenue_Year_Submitted__c}" size="4" maxlength="4" />
              </apex:column>
              <apex:column headervalue="{! $ObjectType.Account.Fields.Program_Service_Revenue__c.Label} ($)"  width="150px">
                  <apex:inputfield value="{!a.Multi_Site_Program_Service_Revenue__c}"  />
              </apex:column>
            
              <apex:column headervalue=""  width="200px">
                  <apex:outputtext escape="false" styleClass="error_ProgramRev" value="{! if( IsBlank(a.Multi_Site_Program_Service_Revenue__c ), 'Missing Program Revenue<br/>', '') }"/>
                  <apex:outputtext escape="false" styleClass="error_ProgramRev" value="{! if( IsBlank(a.Revenue_Year_Submitted__c) || a.Revenue_Year_Submitted__c == null || a.Revenue_Year_Submitted__c == '', 'Missing Year Submitted', '') }"/>                  
                  <apex:outputtext escape="false" styleClass="error_ProgramRev" value="{! if( a.Revenue_Year_Submitted__c != null && isNumber(a.Revenue_Year_Submitted__c) && a.Revenue_Year_Submitted__c != '' && ( VALUE(a.Revenue_Year_Submitted__c) < YEAR(TODAY()) - 3 || VALUE(a.Revenue_Year_Submitted__c) > YEAR(TODAY())  )   , 'Year Submitted is out of date', '') }"/>
              </apex:column>    
              <br /><br />                                  
          </apex:PageBlockTable>
          <!-- End of MSO Accounts-->
     
          
          <apex:PageBlockTable value="{!ProviderAccounts}" var="a" >
              
              <apex:column headervalue="Provider Account">
                  <apex:outputlink value="/{!a.id}" target="_blank">{!a.name}</apex:outputlink>
              </apex:column>
              
              <apex:column headervalue="City"  width="150px">
                  <apex:outputtext value="{!a.BillingCity}" />
              </apex:column>
              <apex:column headervalue="State"  width="50px">
                  <apex:outputtext value="{!a.BillingState}" />
              </apex:column>               
                         
              <apex:column headervalue="{! $ObjectType.Account.Fields.Revenue_Year_Submitted__c.Label}" width="150px">
                  <apex:inputText value="{!a.Revenue_Year_Submitted__c}" size="4" maxlength="4" />
              </apex:column>
              <apex:column headervalue="{! $ObjectType.Account.Fields.Program_Service_Revenue__c.Label} ($)"  width="150px">
                  <apex:inputfield value="{!a.Program_Service_Revenue__c}"  />
              </apex:column>
            
              <apex:column headervalue=""  width="200px">
                  <apex:outputtext escape="false" styleClass="error_ProgramRev" value="{! if( IsBlank(a.Program_Service_Revenue__c ), 'Missing Program Revenue<br/>', '') }"/>
                  <apex:outputtext escape="false" styleClass="error_ProgramRev" value="{! if( IsBlank(a.Revenue_Year_Submitted__c) || a.Revenue_Year_Submitted__c == null || a.Revenue_Year_Submitted__c == '', 'Missing Year Submitted', '') }"/>                  
                  <apex:outputtext escape="false" styleClass="error_ProgramRev" value="{! if( a.Revenue_Year_Submitted__c != null && isNumber(a.Revenue_Year_Submitted__c) && a.Revenue_Year_Submitted__c != '' && ( VALUE(a.Revenue_Year_Submitted__c) < YEAR(TODAY()) - 3 || VALUE(a.Revenue_Year_Submitted__c) > YEAR(TODAY())  )   , 'Year Submitted is out of date', '') }"/>
              </apex:column>
                                          
          </apex:PageBlockTable>
     
          <apex:commandButton action="{!Save}" value="Save"/>
      </apex:pageBlock>
  </apex:form>
</apex:page>