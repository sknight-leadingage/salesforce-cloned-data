/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestWebsiteActionTriggerHandler {

    static testMethod void RunTest() {
        
        //create a test account
        Id rtProvider = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
        Account a = new Account (
            name='Test',
            CAST_Gold_Partner__c = true,
            RecordTypeId = rtProvider,
            Provider_Type__c='Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            AL_Program_Services__c = 'Morbi vehicula lacinia venenatis.',
            CCRC_Program_Services__c = ' Cras lobortis tortor id tellus aliquam, vitae interdum turpis volutpat posuere.',
            HCBS_Program_Services__c = 'Vivamus ut ipsum ac justo aliquam suscipit quis quis mauris.',
            Housing_Program_Services__c = 'Etiam et ligula non ipsum condimentum blandit feugiat in metus.',
            Nursing_Program_Services__c = 'Proin sollicitudin nunc eu felis pharetra.');
        insert a;
        
        Test.startTest();

        //create a test website action
		Website_Action__c wa = new Website_Action__c (
			Account__c = a.Id,
			Code__c = 'DIRECTORY_VIEW',
			Description__c = 'Directory Views',
			Visits__c = 1
		);
		insert wa;

		//query account record
		List<Account> aList1 = [SELECT name, Directory_Page_Views__c FROM Account];
		Account a1 = aList1[0];

		//update the website action
		wa.Visits__c = 2;
		update wa;

		//query account record
		List<Account> aList2 = [SELECT name, Directory_Page_Views__c FROM Account];
		Account a2 = aList2[0];

        Test.stopTest();

        //assert the page view account is incremented from null to 1
        System.assertEquals(a1.Directory_Page_Views__c, 1);

        //assert the page view account is incremented from 1 to 2
        System.assertEquals(a2.Directory_Page_Views__c, 2);

    }

}