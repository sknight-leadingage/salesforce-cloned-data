trigger DealItemTrigger on DealItem__c (before insert, before update) {
    List<DealItem__c> dealItems = Trigger.new;
    
    Set<Id> dealIds = new Set<Id>();
    Set<Id> productIds = new Set<id>();
    Map<Id, Set<Id>> dealProducts = new Map<Id, Set<Id>>();
    for (DealItem__c dealItem : dealItems) { // is one of the deal items you are inserting a duplicate of one of the others you are inserting?
        if (dealItem.Product__c != null && dealItem.Deal__c != null) {
            productIds.add(dealItem.Product__c);
            dealIds.add(dealItem.Deal__c);
            
            if (dealProducts != null && dealProducts.containsKey(dealItem.Deal__c)) {
                if (dealProducts.get(dealItem.Deal__c).contains(dealItem.Product__c)) {
                    dealItem.Product__c.addError('The product already exists for this deal.');
                }
                else {
                    dealProducts.get(dealItem.Deal__c).add(dealItem.Product__c);
                }
            }
            else {
                dealProducts.put(dealItem.Deal__c, new Set<Id>{dealItem.Product__c});
            }
        }
    }
    
    Map<Id, NU__Deal__c> deals = new Map<Id, NU__Deal__c>([SELECT Id,
        NU__BillTo__c,
        RecordTypeId,
        Membership_Type__c,
        Event__c,
        Event__r.NU__Entity__c,
        (SELECT Id,
            Product__c,
            Product__r.NU__Entity__c
            FROM Deal_Items__r
            order by createddate)
        FROM NU__Deal__c
        WHERE Id in :dealIds]);
    
    Map<Id, NU__Product__c> products = new Map<Id, NU__Product__c>([SELECT Id,
        NU__RecordTypeName__c,
        NU__Entity__c
        FROM NU__Product__c
        WHERE Id IN :productIds]);
        
    Map<String, NU__PriceClass__c> priceClassesByName = new Map<String, NU__PriceClass__c>();

    for (NU__PriceClass__c priceClass : [SELECT Name FROM NU__PriceClass__c]) {
        priceClassesByName.put(priceClass.Name, priceClass);
    }
    
    Map<Id, NU__MembershipType__c> membershipTypes = MembershipTypeQuerier.getAllMembershipTypesAndTheirActiveProductLinks();
    
    for (DealItem__c dealItem : dealItems) {
        
        if (dealItem.Product__c != null & dealItem.Deal__c != null) {
            NU__Deal__c deal = deals.get(dealItem.Deal__c);
            NU__Product__c product = products.get(dealItem.Product__c);
            
            // does the product already exist in the deal?
            List<DealItem__c> currentDealItems = deal.Deal_Items__r;
            for (DealItem__c cdi : currentDealItems) {
                if (dealItem.Product__c == cdi.Product__c && cdi.Id != dealItem.Id) {
                    dealItem.Product__c.addError('The product already exists for this deal.');
                }
            }
            
            if (deal.Event__c != null &&
                deal.Event__r.NU__Entity__c != product.NU__Entity__c){
                dealItem.Product__c.addError('All products selected must match the event\'s entity.');
            }

			// Ensure that all products in the deal are from the same entity as 
			// decided upon the first deal item selected.
			DealItem__c firstDealItem = deal.Deal_Items__r.size() > 0 ? deal.Deal_Items__r[0] : null;

			if (firstDealItem != null &&
			    product.NU__Entity__c != firstDealItem.Product__r.NU__Entity__c){
            	dealItem.Product__c.addError('All products selected must be from the same entity.');
            }
            
            
            
            Map<Id, Schema.RecordTypeInfo> dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosById();
            
            String dealRecordType = dealRecordTypes.get(deal.RecordTypeId).getName();
            String productRecordType = products.get(product.Id).NU__RecordTypeName__c;
            
            if (dealRecordType == Constant.ORDER_RECORD_TYPE_MEMBERSHIP){
            	NU__MembershipType__c mt = membershipTypes.get(deal.Membership_Type__c);
            	
            	NU__MembershipTypeProductLink__c membershipTypeProductLink = null;
    		
	            for (NU__MembershipTypeProductLink__c mtpl : mt.NU__MembershipTypeProductLinks__r) {
	                if (mtpl.NU__Product__c == dealItem.Product__c) {
	                    membershipTypeProductLink = mtpl;
	                    break;
	                }
	            }
            	
            	if (membershipTypeProductLink == null){
            		dealItem.addError('No valid membership type product link found. Please make sure that there is a membership type product link for the supplied membership type, deal products, and stage.');
            	}
            }
            
            if (dealRecordType != productRecordType) {
                dealItem.Product__c.addError('The product must be a ' + dealRecordType + ' product for this deal.');
            }
            else if (dealItem.Price__c == null) {
                List<NU__Product__c> productList = new List<NU__Product__c>();
                productList.add(product);
                        
                List<NU.ProductPricingInfo> productPricingInfos = new List<NU.ProductPricingInfo>();
                for (NU__Product__c oneProduct : productList) {
                    NU.ProductPricingInfo ppi = new NU.ProductPricingInfo();
                    ppi.ProductId = oneProduct.Id;
                    ppi.Quantity = 1;
                    productPricingInfos.add(ppi);
                }
                            
                NU.PriceClassRequest pcr = new NU.PriceClassRequest();
                pcr.TransactionDate = DateTime.now();
                pcr.AccountId = deals.get(dealItem.Deal__c).NU__BillTo__c;
                        
                
                
                NU.ProductPricingRequest ppr = new NU.ProductPricingRequest();
                ppr.AccountId = deals.get(dealItem.Deal__c).NU__BillTo__c;
                ppr.TransactionDate = DateTime.now();
                ppr.ProductPricingInfos = productPricingInfos;
                ppr.PriceClassName = priceClassesByName.get(NU.PriceClassManager.getPricingManager().getPriceClass(pcr)).Name;
                ppr.MembershipTypeId = deal.Membership_Type__c;
                            
                Map<Id,Decimal> unitPrices = NU.PriceClassManager.getPricingManager().GetProductPrices(ppr);
                dealItem.Price__c = unitPrices.get(productList.get(0).Id);
            }
        }
    }
}