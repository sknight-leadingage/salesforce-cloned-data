<apex:component controller="OrderPurchaseSponsorship" allowDML="true">

<!-- <apex:attribute name="c" type="SponsorshipOrderController" description="OrderController" assignTo="{!Controller}"/>-->
    
    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd();NUBind()" id="ASBind"/>
    <apex:actionStatus onStart="onAjaxStart()" onStop="onAjaxEnd()" id="AS"/>
    
    <apex:actionRegion >

    <div class="bPageTitle">
        <div class="ptBody">
            <div class="content">
                <img src="/s.gif" class="pageTitleIcon" style="background-image: url(/img/icon/cash32.png)"/>
                <h1 class="pageType">{!Subheader}&nbsp;</h1>
                <h2 class="pageDescription">{!IF(IsEDIT, 'Edit', 'Add')} Sponsorship</h2>
            </div>
        </div>
    </div>
    
    <apex:pageMessages escape="false" id="Msgs" />
    
    <apex:pageBlock mode="maindetail" Id="Inputs">
    
        <apex:pageBlockSection columns="1">
            <apex:inputField value="{!CartItem.NU__Customer__c}" rendered="{!!IsEdit}" id="Account">
                <apex:actionSupport event="onchange" action="{!OnAccountSelected}" rerender="Msgs,Inputs,ItemLines,Buttons" status="ASBind"/>
            </apex:inputField>
            <apex:outputField value="{!CartItem.NU__Customer__c}" rendered="{!IsEdit}"/>
         
         <apex:pageBlockSectionItem rendered="{!CartItem.NU__Customer__c != null}" id="PriceClass">
                <apex:outputText value="{!$ObjectType.NU__OrderItem__c.Fields.NU__PriceClass__c.Label}" />
                <apex:selectList multiselect="false" size="1" value="{!CartItem.NU__PriceClass__c}">
                    <apex:selectOption itemvalue="" itemLabel="Please select a price class" rendered="{!CartItem.NU__PriceClass__c == null}"/>
                    <apex:selectOptions value="{!PriceClassOptions}"/>
                    <apex:actionSupport event="onchange" action="{!OnPriceClassSelected}" rerender="Msgs,ItemLines,Buttons" status="ASBind" />
                </apex:selectList>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        
        <apex:pageBlockSection title="Sponsorship Products" columns="1" rendered="{!CartItem.NU__Customer__c != null && CartItem.NU__PriceClass__c != null}">
            <apex:pageBlockTable value="{!ProductItemLines}" var="oli" id="ItemLines">
                <apex:column >
                    <apex:facet name="header">Purchase</apex:facet>
                    <apex:inputField value="{!oli.NU__IsInCart__c}" styleClass="{!IF(oli.Id == null && oli.NU__OrderItemLine__c == null,'selectableRow','unselectableRow')}" rendered="{!!oli.NU__Product__r.NU__TrackInventory__c || oli.NU__Product__r.NU__InventoryOnHand__c > 0}"/>
                </apex:column>
                <apex:column value="{!oli.NU__Product__c}"/>
                
                <apex:column styleClass="number" headerClass="number">
		            <apex:facet name="header"><apex:outputText value="{! $ObjectType.NU__OrderItemLine__c.Fields.NU__Quantity__c.Label }" /></apex:facet>
		            <div style="display: inline-block">
						<div class="requiredInput">
							<div class="requiredBlock"></div>
		            		<apex:inputField value="{!oli.NU__Quantity__c}"
		            						 styleClass="inputQuantity"
		            						 required="false"
		            						 onkeyup="calcPrice(this)"
		            						 onblur="calcPrice(this)"
		            						 onfocus="this.maxLength = {! QuantityMaxLength };" />
     					</div>
     				</div>
		        </apex:column>
                
                <apex:column styleClass="number" headerClass="number">
                    <apex:facet name="header">Unit Price</apex:facet>
                    <div style="display: inline-block"><div class="requiredInput"><div class="requiredBlock"></div>
                         <apex:inputField value="{!oli.NU__UnitPrice__c}"
                                          required="false"
                                          styleClass="inputPrice"
                                          onfocus="this.maxLength = {! UnitPriceMaxLength };"
                                          onkeyup="calcPrice(this)"
		            					  onblur="calcPrice(this)" /></div></div>
                </apex:column>
                <apex:column styleClass="number priceCOL" headerClass="number priceCOL">
		        	<apex:facet name="header">Price</apex:facet>
		        	<span class="outputPrice">
		        		<apex:outputText value="{0,number,$0.00}">
		        			<apex:param value="{!oli.NU__UnitPrice__c * oli.NU__Quantity__c}"/>
		        		</apex:outputText>
		        	</span>
		        </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlockSection>
        
        <apex:pageBlockButtons location="both" id="Buttons">
            <apex:commandButton value="Save" styleClass="green" action="{!Save}" rerender="Msgs" rendered="{!CartItem.NU__Customer__c != null && CartItem.NU__PriceClass__c != null}" status="AS"/>
            <apex:commandButton value="Cancel" action="{!Cancel}" rerender="Msgs,Main" status="AS"/>
        </apex:pageBlockButtons>
         
     </apex:pageBlock>
        
     </apex:actionRegion>
</apex:component>