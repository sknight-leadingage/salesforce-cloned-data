/**
 * @author NimbleAMS
 * @date Updated: 5/22/2013
 * @description Exposes methods for the Microsoft Dynamics SL (Solomon) Batch Exporter. Implements IBatchExporter.
 */
 global class CustomDynamicsSLBatchExporter implements NU.IBatchExporter {
    public static final String EXPORT_FOLDER_NAME = 'GL Export';

    public static final String DETAIL_FILE_GL_ACCOUNT_COL = 'Acct';
    public static final String DETAIL_FILE_GL_ACCOUNT_SUB_COL = 'Sub';
    public static final String DETAIL_FILE_EMPTY_COL = '';
    public static final String DETAIL_FILE_TRANS_DATE_COL = 'Date';
    public static final String DETAIL_FILE_DEBIT_COL = 'Debit';
    public static final String DETAIL_FILE_CREDIT_COL = 'Credit';
    public static final String DETAIL_FILE_DESCRIPTION_COL = 'Description';
    //public static final String DETAIL_FILE_CASH_TOTAL_COL = 'Cash Total';
    //public static final String DETAIL_FILE_DEBIT_TOTAL_COL = 'Debit Total';
    //public static final String DETAIL_FILE_CREDIT_TOTAL_COL = 'Credit Total';
    
    public static final String NO_BATCH_IDS_TO_EXPORT_ERR_MSG = 'No batch ids were given to export.';

    //NU constants
    public static final String NEWLINE = '\r';
    public static final String TAB_DELIMITER = '\t';
    public static final String COMMA_DELIMITER = ',';
    public static final String TRANSACTION_TYPE_AR = 'AR';
    public static final String TRANSACTION_TYPE_PAYMENT = 'Payment';
    public static final String TRANSACTION_TYPE_INCOME = 'Income';
    public static final String TRANSACTION_CATEGORY_DEBIT = 'Debit';
    public static final String TRANSACTION_CATEGORY_CREDIT = 'Credit';

    public class Row
    {
        public Date transactionDate { get; set; }
        public String transactionDateString { get; set; }
        public String batchNumber { get; set; }
        public Integer rowNumber { get; set; }
        public String glAccount { get; set; }       
        public String glAccount_major { get; set; }       
        public String glAccount_minor { get; set; }       
        public String company { get; set; }
        public String title { get; set; }
        public String detail { get; set; }
        public Decimal debit { get; set; }
        public String debit_string { get { return debit == 0 ? '' : debit.toPlainString(); } }
        public Decimal credit { get; set; }
        public String credit_string { get { return credit == 0 ? '' : credit.toPlainString(); } }
        public Decimal value { get; set; }

        public Row () {
            detail = '';
        }

    }
    
    public List<NU__Batch__c> SelectedBatches { get; set; }
    
    public Decimal TotalDebit { get; set; }
    public Decimal TotalCredit { get; set; }
    
    private String ExportFileContents { get; set; }
    private Document detailDocument { get; set; }
    
    private String FileNameBase {
        get{
            return 'GLExport-' + SelectedBatches[0].NU__Entity__r.Name + '-' +  DateTime.now().format().replace(' ', '-');
        }
    }

    private String DetailFileName{
        get{
            return FileNameBase + 'sf_export.csv';
        }
    }
    
    /**
     * @description Returns the Component Output Panel of the Batch Export Preview View for the Set of Batch Ids passed in.
     * @param Set<Id> Set of Batch Id records
     * @return Component.Apex.OutputPanel
     */
    global Component.Apex.OutputPanel getPreviewComponent(Set<Id> batchesToExportIds){
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        
        List<NU__Batch__c> batches = getSObjectsByIdsQuery(batchesToExportIds);
    
        Component.Apex.PageBlock detailPB = new Component.Apex.PageBlock();
        detailPB.title = 'Detail File';

        List<Row> batchesRows = getRowsFromBatches(batchesToExportIds);

        Component.Apex.PageblockTable previewTable = new Component.Apex.PageblockTable();
        previewTable.value = batchesRows;
        previewTable.var = 'row';

        addPreviewColumn(previewTable, '{!row.glAccount_major}', DETAIL_FILE_GL_ACCOUNT_COL);
        addPreviewColumn(previewTable, '{!row.glAccount_minor}', DETAIL_FILE_GL_ACCOUNT_SUB_COL);
        addPreviewColumn(previewTable, '{!row.TransactionDateString}', DETAIL_FILE_TRANS_DATE_COL);
        
        Component.Apex.Column debitColumn = addPreviewColumn(previewTable, '{!row.debit_string}', DETAIL_FILE_DEBIT_COL);
        rightAlignColumn(debitColumn);
        Component.Apex.Column creditColumn = addPreviewColumn(previewTable, '{!row.credit_string}', DETAIL_FILE_CREDIT_COL);
        rightAlignColumn(creditColumn);
        
        addPreviewColumn(previewTable, '{!row.detail}', DETAIL_FILE_DESCRIPTION_COL);
        
        detailPB.childComponents.add(previewTable);
        
        op.ChildComponents.add(detailPB);
        
        return op;
    }

    /**
     * @description Performs the export on the specified Set of Batch Ids and returns a BatchExportResult.
     * If no Ids are passed in, the BatchExportResult returned will contain an error message.
     * @param Set<Id> Set of Batch Id records
     * @return a BatchExportResult
     */
    global NU.BatchExportResult export(Set<Id> batchesToExportIds){
        NU.BatchExportResult ber = new NU.BatchExportResult();
        
        if (NU.CollectionUtil.setHasElements(batchesToExportIds) == false){
            NU.OperationResult.addErrorMessage(ber.Result, NO_BATCH_IDS_TO_EXPORT_ERR_MSG);
            return ber;
        }
        
        SelectedBatches = getSObjectsByIdsQuery(batchesToExportIds);
        
        String detailFileContents = getDetailFileContents(batchesToExportIds);
        insertExportDocuments(detailFileContents);
        
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        
        Component.Apex.PageMessage pm = new Component.Apex.PageMessage();
        
        pm.Summary = 'The batch(es) were successfully exported.';
        pm.strength = 2;
        pm.severity = 'Info';
        
        op.ChildComponents.add(pm);
        
        Component.Apex.PageBlock exportFilesPB = new Component.Apex.PageBlock();
        
        exportFilesPB.title = 'Great Plains SL Export File';
        
        Component.Apex.OutputLink detailOL = new Component.Apex.OutputLink();

        detailOL.value = '/' + detailDocument.Id;
        detailOL.target = '_blank';
        
        Component.Apex.OutputText detailOT = new Component.Apex.OutputText();
        detailOT.value = 'Detail File';
        
        detailOL.ChildComponents.add(detailOT);
        
        op.ChildComponents.add(exportFilesPB);
        
        exportFilesPB.ChildComponents.add(detailOL);
        
        ber.SuccessView = op;
        ber.Result = new NU.OperationResult();
        
        return ber;
    }

    private void rightAlignColumn(Component.Apex.Column column){
        column.headerStyle = 'text-align: right;';
        column.style = column.headerStyle;
    }
    
    private Component.Apex.Column addPreviewColumn(Component.Apex.PageBlockTable previewTable, String columnExpression, String headerValue){
        Component.Apex.Column column = new Component.Apex.Column();
        column.expressions.value = columnExpression;
        
        if (NU.StringUtil.isNullOrEmpty(headerValue) == false){
            column.headerValue = headerValue;
        }

        previewTable.childComponents.add(column);
        
        return column;
    }
    
    private List<Row> getRowsFromBatches(Set<Id> batchIds){
        List<Row> rows = new List<Row>();
        
        List<NU__Transaction__c> trans =
        [select id,
                name,
                NU__Batch__c,
                NU__Batch__r.Name,
                NU__Date__c,
                NU__GLAccount__c,
                NU__GLAccount__r.Name,
                NU__Amount__c,
                NU__Entity__c,
                NU__Entity__r.Name,
                NU__Customer__r.Name,
                NU__OrderItemLine__r.NU__Product__r.RecordType.Name,
                NU__OrderItemLine__r.NU__Product__r.Name,
                NU__OrderItemLine__r.NU__Product__r.NU__ShortName__c,
                NU__OrderItemLine__r.NU__Product__r.NU__Event__r.NU__ShortName__c,
                NU__Type__c,
                NU__ProductCode__c,
                NU__PaymentLine__r.NU__Payment__r.NU__EntityPaymentMethod__r.NU__PaymentMethod__r.Name,
                NU__Category__c
           from NU__Transaction__c
          where NU__Batch__c in :batchIds
          order by NU__Date__c, NU__Batch__r.Name, NU__GLAccount__r.Name, name];
        
        rows = buildRows(trans);
          
        return rows;
    }
    
    public List<Row> buildRows(List<NU__Transaction__c> trans)
    {
        List<Row> myrows = new List<Row>();
        
        for (NU__Transaction__c lineItem : trans)
        {           
            Row row = new Row();
            row.transactionDate = lineItem.NU__Date__c;
            row.transactionDateString = lineItem.NU__Date__c.month() + '/' + lineItem.NU__Date__c.day() + '/' + string.valueof(lineItem.NU__Date__c.year()).right(2);
            row.batchNumber = lineItem.NU__Batch__r.Name;
            row.glAccount = lineItem.NU__GLAccount__r.Name;
            //row.glAccount = lineItem.NU__GLAccount__r.Name.remove('-');
            row.glAccount_major = row.glAccount.left(4);
            row.glAccount_minor = row.glAccount.substring(5);
            row.value = lineItem.NU__Amount__c;
            row.company = lineItem.NU__Entity__r.Name;

            //handle description formatting rules 
            row.detail = '';
            
            //if it's a payment, then 'PAY', followed by account name
            if (lineItem.NU__Type__c == TRANSACTION_TYPE_PAYMENT)
            {
                row.detail = 'PAY, ' + lineItem.NU__Customer__r.Name;               
            }
            //if it's a receivable, then 'AR', followed by account name
            else if (lineItem.NU__Type__c == TRANSACTION_TYPE_AR)
            {
                row.detail = 'AR, ' + lineItem.NU__Customer__r.Name;                
            }
            else {
            
                //prepend event short name, if product is related to an event
                if (lineItem.NU__OrderItemLine__r.NU__Product__r.NU__Event__r.NU__ShortName__c != null)
                {
                    row.detail = lineItem.NU__OrderItemLine__r.NU__Product__r.NU__Event__r.NU__ShortName__c + '/';
                }

                //if a product name exists, append the name, then account name
                if (lineItem.NU__OrderItemLine__r.NU__Product__r.Name != null)
                {
                    row.detail += lineItem.NU__OrderItemLine__r.NU__Product__r.NU__ShortName__c + ', ' + lineItem.NU__Customer__r.Name;
                }
                //otherwise, append the GL, then account name
                else {
                    row.detail += lineItem.NU__GLAccount__r.Name    + ', ' + lineItem.NU__Customer__r.Name;
                }
            }
            
            if (lineItem.NU__Category__c == TRANSACTION_CATEGORY_CREDIT){
                row.value = lineItem.NU__Amount__c * -1;    
            }

            //if there is already a matching row, just combine them
            Boolean found = false;
            for (Row r : myrows)
            {
                if (
                    r.transactionDate == row.transactionDate &&
                    r.batchNumber == row.batchNumber &&
                    r.glAccount == row.glAccount &&
                    r.company == row.company &&
                    r.title == row.title &&
                    r.detail == row.detail)
                {
                    found = true;
                    r.value += row.value;
                    break;
                }
            }
            if (!found)
            {
                myrows.add(row);
            }
        }

        //remove empty rows, split value into debit/credit, add row numbers
        TotalDebit = 0;
        TotalCredit = 0;
        for (Integer i = 0; i < myrows.size(); i++)
        {
            Row r = myrows[i];
            if (r.value == 0)
            {
                myrows.remove(i);
                i--;
            }
            else
            {
                if (r.value > 0)
                {
                    r.debit = r.value;
                    r.credit = 0;
                }
                else
                {
                    r.debit = 0;
                    r.credit = -r.value;
                }
                TotalDebit += r.debit;
                TotalCredit += r.credit;
                r.rowNumber = i + 1;
            }
        }
        
        return myrows;
    }
    
    private String getDetailFileContents(Set<Id> batchesToExportIds){

        String dr_output = '';
        String cr_output = '';

        List<Row> exportRows = getRowsFromBatches(batchesToExportIds);
        String body = getDetailFileColumnLine();

        Integer scale = NU__Transaction__c.NU__Amount__c.getDescribe().getScale();

        for (Row r : exportRows){
            
            dr_output = r.debit.toPlainString();
            if(r.debit == 0)
                dr_output = '';
            cr_output = r.credit.toPlainString();
            if(r.credit == 0)
                cr_output = '';

            body += 
              NEWLINE +
              r.glAccount_major.escapeCSV() + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              r.glAccount_minor.escapeCSV() + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              r.transactionDateString.escapeCSV() + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              '' + COMMA_DELIMITER +
              dr_output + COMMA_DELIMITER +
              cr_output + COMMA_DELIMITER +
              r.detail.escapeCSV();
              ///'' + COMMA_DELIMITER +
              ///'' + COMMA_DELIMITER +
              ///'' + COMMA_DELIMITER;
              
        }
        
        ExportFileContents = body;

        return body;
    }
    
    private String getDetailFileColumnLine(){
        return DETAIL_FILE_GL_ACCOUNT_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_GL_ACCOUNT_SUB_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_TRANS_DATE_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_EMPTY_COL + COMMA_DELIMITER +
               DETAIL_FILE_DEBIT_COL + COMMA_DELIMITER +
               DETAIL_FILE_CREDIT_COL + COMMA_DELIMITER +
               DETAIL_FILE_DESCRIPTION_COL;
               //DETAIL_FILE_CASH_TOTAL_COL + COMMA_DELIMITER +
               //DETAIL_FILE_DEBIT_TOTAL_COL + COMMA_DELIMITER +
               //DETAIL_FILE_CREDIT_TOTAL_COL;
    }
    
    private void insertExportDocuments(String detailContents){
        Folder exportFolder = [select id from Folder where name = :EXPORT_FOLDER_NAME];

        String txtContentType = 'text/csv';
    
        detailDocument = new Document(
            Body = Blob.valueOf(detailContents),
            ContentType = txtContentType,
            Name = DetailFileName,
            FolderId = exportFolder.Id
        );
        
        List<Document> exportDocs = new List<Document>{ detailDocument };

        insert exportDocs;
    }
    
    public List<SObject> getSObjectsByIdsQuery(Set<Id> ids) {
        return 
         [select
                Name,
                NU__DisplayName__c,
                NU__AR__c,
                NU__ExpectedTotalCashValue__c,
                NU__DifferenceCashValue__c,
                NU__ExportedOn__c,
                NU__Entity__c,
                NU__Entity__r.Name,
                NU__Entity__r.NU__BatchExportConfiguration__c,
                NU__Entity__r.NU__BatchExportConfiguration__r.Name,
                NU__Entity__r.NU__BatchExportConfiguration__r.NU__ExportGenerator__c,
                NU__Health__c,
                NU__PendingOn__c,
                NU__PostedOn__c,
                NU__Revenue__c,
                NU__Source__c,
                NU__Status__c,
                NU__Title__c,
                NU__TotalCashValue__c,
                NU__TransactionDate__c,
                NU__CartCount__c,
                Id,
                RecordTypeId,
                RecordType.Name,
                (Select id
                   from NU__Carts__r),
                (Select id,
                        name
                   from NU__Transactions__r)
           from NU__Batch__c
          where Id in :ids];
    }
}