/*
* Test Code for Education / SessionPlanner Report Systems
*/
@isTest
private class TestEducationReportSystem {

    static testmethod void EduCEReports() {
        EduCEReports_Controller ccc = new EduCEReports_Controller();
        
        system.assertNotEquals('', ccc.getPageTitle());
        system.assertEquals(0, ccc.CEList.size());
    }

    static testmethod void ExerciseGroupHeaderController() {
        GroupHeaderController ghc = new GroupHeaderController();
        system.assertEquals('(Blank)', ghc.getHeaderLocalTitle());

        string strSecDT = 'Test Second (Display)';
        ghc.SecHeader = 'Test Second';
        ghc.SecHeaderDisplay = strSecDT;
        system.assertEquals(strSecDT, ghc.getHeaderSecondTitle());
        
        string strThirdT = 'Test Third';
        ghc.ThirdHeader = strThirdT;
        system.assertEquals('<div style="padding: 2px 0 6px 0;"><h2>'+strThirdT+'</h2></div>', ghc.getHeaderThirdTitle());
        
        string strAbc = 'abc';
        ghc.CurHeader = strAbc;
        ghc.CurHeaderDisplay = strAbc+' #SOQLCOUNT#';
        GroupHeaderController.GroupTotals = new Map<string,integer>();
        GroupHeaderController.GroupTotals.put(strAbc, 1);
        system.assertEquals(strAbc+' 1', ghc.getHeaderLocalTitle());

        ghc.CurHeader = 'abcd';
        system.assertEquals(strAbc+' 0', ghc.getHeaderLocalTitle());
        
        ghc.SecHeader = strAbc;
        ghc.SecHeaderDisplay = strAbc+' #SOQLCOUNT#';
        GroupHeaderController.SecondGroupTotals = new Map<string,integer>();
        GroupHeaderController.SecondGroupTotals.put(strAbc, 1);
        system.assertEquals(strAbc+' 1', ghc.getHeaderSecondTitle());

        ghc.SecHeader = 'abcd';
        system.assertEquals(strAbc+' 0', ghc.getHeaderSecondTitle());
    }

    static testmethod void ExerciseEduReportsControllerBase() {
        EduReportsControllerBase ccc = new EduReportsControllerBase();
        ccc.setPageTitle('A TEST');
        
        system.assertEquals('A TEST', ccc.getPageTitle());
        system.assertNotEquals(null, ccc.getRTypeItems());
        
        system.assertEquals(1, ccc.WizardMax);
        
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.PrintOutput();
        system.assertEquals('pdf', ccc.RptRenderAs);
        
        PageReference pr = ccc.viewConferenceReports();
        system.assertEquals(pr.getUrl(), ccc.viewConferenceReports().getUrl());
        
        ccc.SetConPageSize = 225;
        ccc.SetConQuery = 'SELECT Id FROM Conference_Session__c LIMIT 10';
        
        List<Conference_Session__c> csc = ccc.setCon.getRecords();
        system.assertEquals(false, ccc.SetConHasPrev);
        system.assertEquals(false, ccc.SetConHasNext);
        
        ccc.SetConRepageNext();
        ccc.SetConRepagePrev();
        ccc.SetConRepageLast();
        ccc.SetConRepageFirst();
        
        ccc.RptRenderAs = '';
        ccc.PageCType = '';
        system.assertEquals('', ccc.getRenderType());
        
        ccc.SetPageType();
        system.assertEquals('NonWebOutput', ccc.getRenderType());
    }

    static testmethod void EduSessionHistoryByDate(){
        PageReference testPr = Page.EduSessionHistory;
        Test.setCurrentPage(testPr);
        
        EduSessionHistory_Controller ccc = new EduSessionHistory_Controller();
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        
        ccc.RTypeSel = 'By Date';
        ccc.WizardNext();
        
        system.assertEquals(2, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduSessionHistoryByStaffName(){
        PageReference testPr = Page.EduSessionHistory;
        Test.setCurrentPage(testPr);
        
        EduSessionHistory_Controller ccc = new EduSessionHistory_Controller();
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        
        ccc.RTypeSel = 'By Assigned Staff';
        ccc.WizardNext();
        
        system.assertEquals(3, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    
    static testmethod void EduSessionHistoryByChangedName(){
        PageReference testPr = Page.EduSessionHistory;
        Test.setCurrentPage(testPr);
        
        EduSessionHistory_Controller ccc = new EduSessionHistory_Controller();
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        
        ccc.RTypeSel = 'By Person Making the Change';
        ccc.WizardNext();
        
        system.assertEquals(4, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduSessionHistorySundry() {
        EduSessionHistory_Controller ccc = new EduSessionHistory_Controller();
        system.assertEquals(4, ccc.WizardMax);
        system.assertEquals('', ccc.getPageTitle());
        ccc.RTypeSel = 'By Staff Name';
        system.assertNotEquals('', ccc.getPageTitle());
        system.assertNotEquals(null, ccc.getConferenceObjects());
    }
    
/**BEGIN: EduProposalChangelog ******/
    static testmethod void EduProposalChangelogByStatus(){
        PageReference testPr = Page.EduProposalChangelog;
        Test.setCurrentPage(testPr);
        
        EduProposalChangelog_Controller ccc = new EduProposalChangelog_Controller();
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Proposal History Report types list!');
        
        ccc.RTypeSel = 'By Status';
        ccc.WizardNext();
        
        system.assertEquals(2, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduProposalChangelogTrack(){
        PageReference testPr = Page.EduProposalChangelog;
        Test.setCurrentPage(testPr);
        
        EduProposalChangelog_Controller ccc = new EduProposalChangelog_Controller();
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Proposal History Report types list!');
        
        ccc.RTypeSel = 'By Track';
        ccc.WizardNext();
        
        system.assertEquals(3, ccc.WizardStep);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduProposalChangelogSundry() {
        EduProposalChangelog_Controller ccc = new EduProposalChangelog_Controller();
        system.assertEquals(3, ccc.WizardMax);
        system.assertEquals('', ccc.getPageTitle());
        ccc.RTypeSel = 'By Status';
        system.assertNotEquals('', ccc.getPageTitle());
        system.assertNotEquals(null, ccc.getConferenceObjects());
    }
/***END: EduProposalChangelog ***/
    
    static testmethod void EduSessionListing() {
        EduSessionListing_Controller ccc = new EduSessionListing_Controller();
        system.assertEquals(0, ccc.SessionData.size());
        system.assertNotEquals(null, ccc.getConferenceObjects());
    }
    
    static testmethod void ConferenceReports() {
        ConferenceReports_Controller ccc = new ConferenceReports_Controller();
        system.assertEquals(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, ccc.getQSEventIdParamName());
        system.assertNotEquals(null, ccc.getConferenceObjects());
    }
    
    static testmethod void EduSessionsSessionListing() {
        EduSessionsSessionListing_Controller ccc =  new EduSessionsSessionListing_Controller();
        
        ccc.SessionInfo = null;
        system.assertEquals(0, ccc.SessionInfoSize);
        
        ccc.ProposalInfo = null;
        system.assertEquals(0, ccc.ProposalInfoSize);

        system.assertEquals(1, ccc.WizardStep);
        system.assertNotEquals(null, ccc.getConferenceObjects());
        
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        ccc.RTypeSel = null;
        system.assertNotEquals('', ccc.getPageTitle());
        ccc.RTypeSel = ccc.SLIST_SNUMBER;
        system.assertNotEquals('', ccc.getPageTitle());
        
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        
        system.assertNotEquals(null, ccc.SessionInfo);
        system.assertEquals(0, ccc.SessionInfo.size());
        system.assertEquals(0, ccc.SessionInfoSize);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_STIMESLOT;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_STRACK;
        ccc.WizardNext();
        system.assertEquals(4, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_PTIMESLOT;
        ccc.WizardNext();
        system.assertEquals(5, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_PTRACK;
        ccc.WizardNext();
        system.assertEquals(6, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_PSTAFF;
        ccc.WizardNext();
        system.assertEquals(7, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduSessionsSessionProofs() {
        EduSessionsSessionProofs_Controller ccc = new EduSessionsSessionProofs_Controller();
        ccc.RTypeSel = null;
        system.assertEquals('Session Proofs', ccc.getPageTitle());

        system.assertEquals(1, ccc.WizardStep);
        system.assertNotEquals(null, ccc.getConferenceObjects());
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        
        ccc.RTypeSel = ccc.SPROOFS_SNUMBER;
        system.assertEquals(ccc.SPROOFS_SNUMBER, ccc.getPageTitle());
        
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        system.assertNotEquals(null, ccc.SessionInfo);
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);

        ccc.RTypeSel = ccc.SPROOFS_STIMESLOT;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        system.assertNotEquals(null, ccc.SessionInfo);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SPROOFS_STRACK;
        ccc.WizardNext();
        system.assertEquals(4, ccc.WizardStep);
        system.assertNotEquals(null, ccc.SessionInfo);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SPROOFS_SSTAFF;
        ccc.WizardNext();
        system.assertEquals(5, ccc.WizardStep);
        system.assertNotEquals(null, ccc.SessionInfo);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduCEsByCategory() {
        EduCEsByCategory_Controller ccc = new EduCEsByCategory_Controller();
        
        system.assertNotEquals('', ccc.getPageTitle());
        system.assertNotEquals(null, ccc.getConferenceObjects());
        system.assertNotEquals(null, ccc.CEList);
    }
    
    static testmethod void EduSessionComparison() {
        EduSessionComparison_Controller ccc = new EduSessionComparison_Controller();
        ccc.CEList = new List<Conference_Session_Speaker__c>();
        system.assertEquals(0, ccc.CEListSize);
        
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
    }
    
    static testmethod void EduSpeakerConflict() {
        EduSpeakerConflict_Controller ccc = new EduSpeakerConflict_Controller();
        ccc.SessionData = new List<Conference_Session_Speaker__c>();
        system.assertEquals(0, ccc.SessionDataSize);
        
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
    }
    
    static testmethod void EduSessionContentCategories() {
        EduSessionContentCategories_Controller ccc = new EduSessionContentCategories_Controller();

        ccc.RTypeSel = null;
        string sTitle = '';
        sTitle = ccc.getPageTitle();
        system.assertEquals(sTitle, ccc.getPageTitle());

        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.CAT_SNUMBER;
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);

        ccc.RTypeSel = ccc.CAT_CATEGORY;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);

    }
    
    static testmethod void EduEvalsMailingLabels() {
        EduEvalsMailingLabels_Controller ccc = new EduEvalsMailingLabels_Controller();

        system.assertEquals(0, ccc.SpeakerList.size());
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        
        system.assertEquals(0, ccc.getRecordCount());
        system.assertEquals(0, ccc.getRowsLeftSide());
    }
    
    static testmethod void EduCEsSessSummarySheetsDOM() {
        PageReference testPr = Page.EduCEsSessSummarySheetsDOM;
        Test.setCurrentPage(testPr);
        ApexPages.currentPage().getParameters().put('EventId', 'a0cd0000002vbGeAAI');

        EduCEsSessSummarySheetsDOM_Controller ccc = new EduCEsSessSummarySheetsDOM_Controller();
        
        system.assertEquals(0, ccc.SumList.size());
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        string strOrS = ccc.getPageSubtitle();
        system.assertEquals(strOrS, ccc.getPageSubtitle());
        ccc.SetQuery();
        
        ApexPages.currentPage().getParameters().put('mt', 'sn');
        system.assertEquals(0, ccc.SumList.size());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        strOrS = ccc.getPageSubtitle();
        system.assertEquals(strOrS, ccc.getPageSubtitle());
        ccc.SetQuery();
        
        ApexPages.currentPage().getParameters().put('mt', 'ts');
        system.assertEquals(0, ccc.SumList.size());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        strOrS = ccc.getPageSubtitle();
        system.assertEquals(strOrS, ccc.getPageSubtitle());
        ccc.SetQuery();
    }
    
    static testmethod void EduCEsDomPractice() {
        EduCEsDomPractice_Controller ccc = new EduCEsDomPractice_Controller();

        system.assertEquals(0, ccc.SumList.size());
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        string strOrS = ccc.getPageSubtitle();
        system.assertEquals(strOrS, ccc.getPageSubtitle());
        
        ccc.setDOMPractice('KS');
        ccc.SetQuery();
        
        ccc.setDOMPractice('MO');
        ccc.SetQuery();
        
        ccc.setDOMPractice('RCFE');
        ccc.SetQuery();
    }
 
    static testmethod void EduOnsiteEvalForms() {
        EduOnsiteEvalForms_Controller ccc = new EduOnsiteEvalForms_Controller();

        system.assertEquals(0, ccc.SumList.size());
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
    }
    
    static testmethod void EduEvalsComposites() {
        PageReference pageRef = Page.EduEvalsComposites;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('EventId', 'a0cd0000002vbGeAAI');
        
        //ccc.setReport('edprog');
        ApexPages.CurrentPage().getParameters().put('rpt', 'edprog');
        
        EduEvalsComposites_Controller ccc = new EduEvalsComposites_Controller();
        ccc.SetConPageSize = 255;
        
        ccc.setReport('sesscs');
        system.assertNotEquals('', ccc.getReport());
        string sps1 = ccc.getPagePrintSize();
        system.assertEquals(sps1, ccc.getPagePrintSize());
        string strOr1 = ccc.getPageTitle();
        system.assertEquals(strOr1, ccc.getPageTitle());
        string spts1 = ccc.getPageTitleTop();
        system.assertEquals(spts1, ccc.getPageTitleTop());
        ccc.SetQuery();        
 
        ccc = null;
        ccc = new EduEvalsComposites_Controller();
        
        system.assertNotEquals('', ccc.getReport());
        string sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        string strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        string spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();

        //ccc.setReport('sessoa');
        ApexPages.CurrentPage().getParameters().put('rpt', 'sessoa');
        
        ccc = null;
        ccc = new EduEvalsComposites_Controller();
        
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();

        ApexPages.CurrentPage().getParameters().put('rpt', 'sessoat');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();
        
        ApexPages.CurrentPage().getParameters().put('rpt', 'sessoats');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();

        ApexPages.CurrentPage().getParameters().put('rpt', 'savgbys');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();

        ApexPages.CurrentPage().getParameters().put('rpt', 'sesscs');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();
        
        ccc.SetConRefreshData();
        if (ccc.DataList == null) {
            system.assertEquals(null, ccc.DataList);
        }
        else {
            system.assertEquals(0, ccc.DataList.size());
        }
        
        ccc.setReport('rndval');
        system.assertEquals('rndval', ccc.getReport());
        
        ApexPages.CurrentPage().getParameters().put('rpt', 'srba');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();
        
        ApexPages.CurrentPage().getParameters().put('rpt', 'srbys');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();
        
        ApexPages.CurrentPage().getParameters().put('rpt', 'savgbyspk');
        system.assertNotEquals('', ccc.getReport());
        sps = ccc.getPagePrintSize();
        system.assertEquals(sps, ccc.getPagePrintSize());
        strOr = ccc.getPageTitle();
        system.assertEquals(strOr, ccc.getPageTitle());
        spts = ccc.getPageTitleTop();
        system.assertEquals(spts, ccc.getPageTitleTop());
        ccc.SetQuery();
        
        ccc.SetConRefreshData();
        if (ccc.DataListSpk == null) {
            system.assertEquals(null, ccc.DataListSpk);
        }
        else {
            system.assertEquals(0, ccc.DataListSpk.size());
        }
    }
    
    static testmethod void EduExcelExport_Controller() {
        PageReference testPr = Page.EduExcelExport;
        Test.setCurrentPage(testPr);
        EduExcelExport_Controller ccc = new EduExcelExport_Controller();

        ccc.RptRenderAs = '';
        system.assertEquals('', ccc.getPageTitle());
        
        string sTitle = '';
        ApexPages.currentPage().getParameters().put('mt', 'mfl');
        system.assertNotEquals('', ccc.getMType());
        sTitle = ccc.getPageTitle();
        system.assertEquals(sTitle, ccc.getPageTitle());
        ccc.SetQuery();
        ccc.SetPageType();
        system.assertNotEquals('', ccc.PageCType);
        
        ApexPages.currentPage().getParameters().put('mt', 'ces');
        sTitle = ccc.getPageTitle();
        system.assertEquals(sTitle, ccc.getPageTitle());
        ccc.SetQuery();
        ccc.SetPageType();
        system.assertNotEquals('', ccc.PageCType);
        
        ccc.getSpeakerList();
        ccc.getSessionList();
        ccc.getCategoryList();
        ccc.setMType('s');
        
        ApexPages.currentPage().getParameters().put('mt', 'hub-av');
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-avc');
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-sp');
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-spc');
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-spce');
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-pmc');
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetQuery();
        
        
    }
    
    static testmethod void EduMaterials_Controller(){
        PageReference testPr = Page.EduMaterials;
        Test.setCurrentPage(testPr);
        ApexPages.currentPage().getParameters().put('EventId', 'a0cd0000002vbGeAAI');
        EduMaterials_Controller ccc = new EduMaterials_Controller();

        string sTest = '';
        system.assertEquals('', ccc.getPageSubtitle());
        
        ApexPages.currentPage().getParameters().put('mt', 'sgn');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();
        
        ApexPages.currentPage().getParameters().put('mt', 'mgap');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();

        ApexPages.currentPage().getParameters().put('mt', 'mgpa');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();
        
        ApexPages.currentPage().getParameters().put('mt', 'ceva');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();
        
        ApexPages.currentPage().getParameters().put('mt', 'cevp');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();
        
        ApexPages.currentPage().getParameters().put('mt', 'xpta');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();
        
        ApexPages.currentPage().getParameters().put('mt', 'mona');
        sTest = ccc.getPageSubtitle();
        system.assertEquals(sTest, ccc.getPageSubtitle());
        ccc.SetQuery();
        ccc.NeedsData();
        
        sTest = 'ffsdfsdf';
        ccc.setMType(sTest);
        system.assertEquals(sTest, ccc.getMType());
    }
    
    static testmethod void EduSpeakerListings_Controller() {
        EduSpeakerListings_Controller ccc = new EduSpeakerListings_Controller();
        ccc.RTypeSel = null;
        string sTitle = '';
        sTitle = ccc.getPageTitle();
        system.assertEquals(sTitle, ccc.getPageTitle());

        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_SNUMBER;
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        sTitle = ccc.getPageSubtitle();
        system.assertEquals(sTitle, ccc.getPageSubtitle());
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);

        ccc.RTypeSel = ccc.SLIST_SPEAKER;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        sTitle = ccc.getPageSubtitle();
        system.assertEquals(sTitle, ccc.getPageSubtitle());
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_COMPANY;
        ccc.WizardNext();
        system.assertEquals(4, ccc.WizardStep);
        sTitle = ccc.getPageSubtitle();
        system.assertEquals(sTitle, ccc.getPageSubtitle());
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.SLIST_TIMESLOT;
        ccc.WizardNext();
        system.assertEquals(5, ccc.WizardStep);
        sTitle = ccc.getPageSubtitle();
        system.assertEquals(sTitle, ccc.getPageSubtitle());
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduConfAdmin_Controller() {
        PageReference testPr = Page.EduConfAdmin;
        Test.setCurrentPage(testPr);
        ApexPages.currentPage().getParameters().put('EventId', 'a0cd0000002vbGeAAI');
        EduConfAdmin_Controller ccc = new EduConfAdmin_Controller();
        
        ccc.RTypeSel = null;
        string s = ccc.getPageTitle();
        system.assertEquals(s, ccc.getPageTitle());
        
        s = ccc.getLineBreak();
        system.assertEquals(s, ccc.getLineBreak());
        
        String[] ses = ccc.getTDs();
        system.assertNotEquals(null, ses);
        
        s = ccc.getMainSort();
        system.assertEquals(ccc.SORT_SUBMIT_DATE, s);
        
        ApexPages.currentPage().getParameters().put('msrt', ccc.SORT_NAME);
        s = ccc.getMainSort();
        system.assertEquals(ccc.SORT_NAME, s);
        
        s = ccc.getSubmitSortLink();
        system.assertEquals(s, ccc.getSubmitSortLink());
        
        s = ccc.getNameSortLink();
        system.assertEquals(s, ccc.getNameSortLink());
        
        ccc.RTypeSel = null;
        ccc.WizardStep = 1;
        ccc.SetQuery();

        ccc.RTypeSel = ccc.CADMIN_TOTALS;
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.CADMIN_DOC;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.CADMIN_TBL;
        ccc.WizardNext();
        system.assertEquals(4, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduProposalListing() {
        EduProposalListing_Controller ccc =  new EduProposalListing_Controller();

        system.assertEquals(1, ccc.WizardStep);
        system.assertNotEquals(null, ccc.getConferenceObjects());
        
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        ccc.RTypeSel = null;
        system.assertNotEquals('', ccc.getPageTitle());
        ccc.RTypeSel = ccc.PROPLIST_SNUMBER;
        system.assertNotEquals('', ccc.getPageTitle());
        
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        
        system.assertNotEquals(null, ccc.PropInfo);
        system.assertEquals(0, ccc.PropInfo.size());
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_SSUBMITTER;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_SCOMPANY;
        ccc.WizardNext();
        system.assertEquals(4, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_STRACK;
        ccc.WizardNext();
        system.assertEquals(5, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduProposalStatus() {
        EduProposalStatus_Controller ccc =  new EduProposalStatus_Controller();

        system.assertEquals(1, ccc.WizardStep);
        system.assertNotEquals(null, ccc.getConferenceObjects());
        
        system.assert(ccc.getRTypeItems().size() > 0, 'No data in Session History Report types list!');
        ccc.RTypeSel = null;
        system.assertNotEquals('', ccc.getPageTitle());
        ccc.RTypeSel = ccc.PROPLIST_SNUMBER;
        system.assertNotEquals('', ccc.getPageTitle());
        
        ccc.WizardNext();
        system.assertEquals(2, ccc.WizardStep);
        
        system.assertNotEquals(null, ccc.PropInfo);
        system.assertEquals(0, ccc.PropInfo.size());
        
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_SSUBMITTER;
        ccc.WizardNext();
        system.assertEquals(3, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_SCOMPANY;
        ccc.WizardNext();
        system.assertEquals(4, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_STRACK;
        ccc.WizardNext();
        system.assertEquals(5, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
        
        ccc.RTypeSel = ccc.PROPLIST_SSPONSOR;
        ccc.WizardNext();
        system.assertEquals(6, ccc.WizardStep);
        ccc.WizardBack();
        system.assertEquals(1, ccc.WizardStep);
    }
    
    static testmethod void EduProposalsMostFields() {
        EduProposalsMostFields_Controller ccc = new EduProposalsMostFields_Controller();
        ccc.SetPageType();
        ccc.DataList = new List<Conference_Proposal__c>();
        system.assertEquals(0, ccc.DataList.size());
    }
    
    static testmethod void EduPostIt() {
        EduPostIt_Controller ccc = new EduPostIt_Controller();
        system.assertEquals('', ccc.getPageTitle());
        ccc.SumList = new List<Conference_Session__c>();
        system.assertEquals(0, ccc.SumList.size());
    }

/* Reports created in 2014 */

    static testmethod void EduBrochureCopyFormatted_Controller() {
        EduBrochureCopyFormatted_Controller ccc = new EduBrochureCopyFormatted_Controller();
        system.assertEquals(0, ccc.SessionData.size());
        system.assertNotEquals(null, ccc.getConferenceObjects());
    }

    static testmethod void EduBrochureCopyTable_Controller() {
        EduBrochureCopyTable_Controller ccc = new EduBrochureCopyTable_Controller();
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetConRefreshData();
        ccc.SetQuery();
    }

    static testmethod void EduBrochureSessionPlanner_Controller() {
        EduBrochureSessionPlanner_Controller ccc = new EduBrochureSessionPlanner_Controller();
        system.assertEquals(0, ccc.SessionData.size());
        system.assertNotEquals(null, ccc.getConferenceObjects());
    }

    static testmethod void EduEvalsMostFields_Controller() {
        EduEvalsMostFields_Controller ccc = new EduEvalsMostFields_Controller();
        system.assertNotEquals(null, ccc.getConferenceObjects());
        ccc.SetPageType();
        ccc.getPageTitle();
        ccc.SetConRefreshData();
        ccc.SetQuery();
    }

    static testmethod void EduFormatted_Controller() {
        EduFormatted_Controller ccc = new EduFormatted_Controller();
        system.assertNotEquals(null, ccc.getConferenceObjects());
        ccc.getPageTitle();
        ccc.getRTypeItems();
        ccc.getMType();
        ccc.setMType('a');
        Integer i = ccc.SessionInfoSize;
        ccc.SetQuery();
        Integer j = ccc.ProposalInfoSize;
        ccc.WizardNext();
        ccc.WizardBack();
    }

    static testmethod void EduHubListing_Controller() {
        EduHubListing_Controller ccc = new EduHubListing_Controller();
        system.assertNotEquals(null, ccc.getConferenceObjects());
        ccc.getPageTitle();
        ccc.getRTypeItems();
        ccc.SetQuery();
        ccc.getMType();
        ccc.setMType('a');
        Integer i = ccc.SessionInfoSize;
        Integer j = ccc.ProposalInfoSize;
        ccc.WizardNext();
        ccc.WizardBack();

        ApexPages.currentPage().getParameters().put('mt', 'hub-sps');
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-avs');
        ccc.SetQuery();
        ApexPages.currentPage().getParameters().put('mt', 'hub-pms');
        ccc.SetQuery();

    }

    static testmethod void EduOnsiteEstimatedAttendance_Controller() {
        EduOnsiteEstimatedAttendance_Controller ccc = new EduOnsiteEstimatedAttendance_Controller();
        system.assertNotEquals(null, ccc.getConferenceObjects());
        ccc.getPageTitle();
        ccc.getRTypeItems();
        ccc.WizardNext();
        ccc.WizardBack();
        ccc.SetQuery();
    }

}