public with sharing class ManualJointBillingUpdaterController {
	public static final String NO_JOINT_BILLINGS_TO_UPDATE = 'There are no joint billings to update. Please do a bill run first.';
	public static final String JOINT_BILLING_UPDATED_SUCCESSFULLY = 'The latest joint billing has been successfully updated.';
	
	public Account StatePartner { get; set; }
	
	public ManualJointBillingUpdaterController(ApexPages.StandardController standardController){		
		StatePartner = [select id, name from Account where id = :standardController.getId()];
	}
	
	public void updateStateProviderJointBilling(){
		List<Joint_Billing__c> latestJointBillings = 
		[select id,
		        name,
		        State_Partner__c,
		        Start_Date__c
		   from Joint_Billing__c
		  where State_Partner__c = :StatePartner.Id
		  order by Start_Date__c desc
		  limit 1];
		  
		if (latestJointBillings.size() == 0){
			ApexPages.Message noJointBillingsToUpdateMsg = new ApexPages.Message(ApexPages.Severity.Info, NO_JOINT_BILLINGS_TO_UPDATE);
			ApexPages.addMessage(noJointBillingsToUpdateMsg);
			return;
		}
		
		try{
			JointBillingUpdater jbu = new JointBillingUpdater();
			
			jbu.updateStatePartnerJointBilling(latestJointBillings[0]);
			
			ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.Info, JOINT_BILLING_UPDATED_SUCCESSFULLY);
			ApexPages.addMessage(successMsg);
		}
		catch(Exception ex){
			ApexPages.addMessages(ex);
			
			if (Test.isRunningTest()){
				throw ex;
			}
		}
	}
}