<apex:page standardController="Account" extensions="NU.ManageAccount">
    <apex:pageMessages escape="false" />
    <style type="text/css">
        #manageAccount {
            width: 100%;
        }
        #manageAccountMain, #manageAccountSocial {
            vertical-align: top;
            width: 50%;
        }
        #manageAccount .data2Col, #manageAccount .labelCol {
            border: none;
        }
        .loginName {
            text-align:center;
            padding: 4px 16px;
            display: block;
            border: thin solid black;
        }
        .loginName.loginFacebook {
            background-color: #3662A0;
            border-top: thin solid #467FCE;
            border-left: thin solid #467FCE;
            border-bottom: thin solid #1A314F;
            border-right: thin solid #1A314F;
            color: white;
        }
        .loginName.loginLinkedIn {
            background-color: #EEEEEE;
            border-top: thin solid #F4F4F4;
            border-left: thin solid #F4F4F4;
            border-bottom: thin solid #CCCCCC;
            border-right: thin solid #CCCCCC;
        }
    </style>
    <script type="text/javascript">
        function setFocusOnLoad() { }
        function confirmSocialDelete(service) {
            return confirm("Are you sure you want to delete this account's " + service + " login?");
        }
    </script>
    <apex:form >
        <table id="manageAccount">
            <tr>
                <td id="manageAccountMain">
                    <apex:pageBlock mode="maindetail">
                        <apex:pageBlockSection columns="1" >
                            <apex:pageBlockSectionItem helpText="{!IF(LoginType != 'Email', 'Username must be at least 3 characters long.', null)}">
                                Username
                                <apex:outputPanel layout="none">
                                    <apex:inputText value="{!Username}" maxlength="128" styleClass="{!IF(UsernameInvalid, 'error', '')}" rendered="{!LoginType == 'Username'}" />
                                    <apex:outputText value="{!Username}" rendered="{!LoginType == 'Email'}" />
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem helpText="Password must be at least {!MinimumPasswordLength} characters long, and contain at least one digit and one alphabetic character.">
                                Password
                                <apex:inputText value="{!Password}" styleClass="{!IF(PasswordInvalid, 'error', '')}" />
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem rendered='{!EntityCount > 1}'>
                                {!$ObjectType.Account.fields.NU__PrimaryEntity__c.Label}
                                <apex:selectList size="1" value="{!Account.NU__PrimaryEntity__c}">
                                    <apex:selectOptions value="{!EntityOptions}"/>
                                </apex:selectList>
                            </apex:pageBlockSectionItem>
                            <apex:pageBlockSectionItem >
                                {!$ObjectType.Account.fields.NU__LastLogin__c.Label}
                                <apex:outputPanel layout="none">
                                    <apex:outputField value="{!Account.NU__LastLogin__c}" rendered="{!!ISBLANK(Account.NU__LastLogin__c)}"/>
                                    <apex:outputText value="never" rendered="{!ISBLANK(Account.NU__LastLogin__c)}"/>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                        <apex:pageBlockButtons location="bottom">
                            <apex:commandButton value="Save" action="{!Save}"/>
                        </apex:pageBlockButtons>
                    </apex:pageBlock>
                </td>
                <td id="manageAccountSocial">
                    <apex:dataTable value="{!SocialLogins}" var="login" styleClass="socialLogins" cellspacing="8">
                        <apex:column value="{!login.Name}" styleClass="loginName login{!login.Name}"/>
                        <apex:column styleClass="lastLogin">
                            {!'Last Login: '}
                            <apex:outputField value="{!login.NU__LastLogin__c}"/>
                        </apex:column>
                        <apex:column styleClass="delLogin">
                            <apex:commandLink value="Del" action="{!DeleteSocialLogin}" onclick="return confirmSocialDelete('{!login.Name}')">
                                <apex:param value="{!login.Id}" assignTo="{!SelectedSocialLogin}" name="socialLoginId"/>
                            </apex:commandLink>
                        </apex:column>
                    </apex:dataTable>
                </td>
            </tr>
        </table>
    </apex:form>
</apex:page>