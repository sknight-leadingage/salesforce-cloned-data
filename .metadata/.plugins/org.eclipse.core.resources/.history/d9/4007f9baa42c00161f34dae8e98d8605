public without sharing class OnlineSubscriptionTriggerHandler extends NU.TriggerHandlersBase
{
    public override void onBeforeInsert(List<sObject> newRecords) {
        DoBoth(newRecords);
    }
    
    public override void onBeforeUpdate(Map<Id, sObject> newRecordMap, Map<Id, sObject> oldRecordMap) {
        List<Online_Subscription__c> vars = newRecordMap.values();
        DoBoth(vars);
    }
    
    public void DoBoth(List<Online_Subscription__c> triggerValues) {
        Set<Id> accountIds = NU.CollectionUtil.getLookupIds(triggerValues, 'Account__c');
    
        List<Account> subscribers =
        
        [select id,
                name,
                (Select Id,
                        Status__c,
                        Product__c
                   from Online_Subscriptions__r)
           from Account
          where id in :accountIds];
          
        Map<Id, Account> subscribersMap = new Map<Id, Account>( subscribers );
        
        for (Online_Subscription__c newSub: triggerValues){
            Account subscriber = subscribersMap.get(newSub.Account__c);
            
            // SK 20130619: Possible easy fix for users not being able to set I or H
            if ((newSub.System_End_Date__c == null && UserInfo.getUserName().startsWith('guestdesk@leadingage.org') == false && UserInfo.getUserName().startsWith('swapdesk@leadingage.org') == false && UserInfo.getUserName() != 'api@leadingage.org' && UserInfo.getUserName() != 'api2@leadingage.org') && (newSub.Status__c != Constant.SUBSCRIPTION_STATUS_ACTIVE && newSub.Status__c != Constant.SUBSCRIPTION_STATUS_UNSUBSCRIBE)) {
                newSub.Status__c = Constant.SUBSCRIPTION_STATUS_UNSUBSCRIBE;
            }
            
            //Make sure we clear out the system end date if a sub is manually taken care of
            if (newSub.System_End_Date__c != null && (newSub.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE || newSub.Status__c == Constant.SUBSCRIPTION_STATUS_UNSUBSCRIBE)) {
                newSub.System_End_Date__c = null;
            }
            
            for (Online_Subscription__c mySub : subscriber.Online_Subscriptions__r)
            {
                // Prevent Duplicate Additions
                if (mySub.Id != newSub.Id && mySub.Product__c == newSub.Product__c &&  mySub.Status__c == Constant.SUBSCRIPTION_STATUS_ACTIVE)
                {
                    newSub.Product__c.addError('This user has an existing subscription for this product.');
                }
            }
        }
    }
}