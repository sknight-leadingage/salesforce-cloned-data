/**
 * @author NimbleUser
 * @date Updated: 1/31/13
 * @description This class has various test methods to test the Associate membership flowdown logic.
 */
@isTest
private class TestAssociateMembershipFlowdown {

    static testmethod void associateMembershipFlowDownOneLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownOneLevelTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipFlowDownTwoLevelsTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownTwoLevelsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipFlowDownOneLevelMultipleAcctsTest(){
    	
    	TestMembershipFlowdownUtil.membershipFlowDownOneLevelMultipleAcctsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipFlowDownTwoLevelsMultipleAcctsTest(){
    	
		TestMembershipFlowdownUtil.membershipFlowDownTwoLevelsMultipleAcctsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNullFlowDownOneLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipNullFlowDownOneLevelTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNullFlowDownTwoLevelTest(){
    	
    	TestMembershipFlowdownUtil.membershipNullFlowDownTwoLevelTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownFromPersonToPersonTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownFromPersonToPersonTest(
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownFromPersonToBusinessTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownFromPersonToBusinessTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownToNonPrimaryAffiliationsTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownToNonPrimaryAffiliationsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void membershipNoFlowdownWithIndividualMembershipTypeTest(){
    	
    	TestMembershipFlowdownUtil.membershipNoFlowdownToNonPrimaryAffiliationsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestFakePersonProviderMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }
    
    static testmethod void newPrimaryAffiliationTest() {
    	
    	TestMembershipFlowdownUtil.newPrimaryAffiliationTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newPrimaryAffiliationFlowdownTwoLevelsTest(){
    	
    	TestMembershipFlowdownUtil.newPrimaryAffiliationFlowdownTwoLevelsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newIndivToIndivPrimaryAffilWithNoFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.newIndivToIndivPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void newIndivToOrgPrimaryAffilWithNoFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.newIndivToOrgPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deletePrimaryAffilErasingMembershipFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.deletePrimaryAffilErasingMembershipFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deletePrimaryAffilErasingTwoMembershipFlowdownLevelsTest(){
    	
    	TestMembershipFlowdownUtil.deletePrimaryAffilErasingTwoMembershipFlowdownLevelsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void deleteNonPrimaryAffilWithNoErasingMembershipFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.deleteNonPrimaryAffilWithNoErasingMembershipFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

    static testmethod void changeAffiliationToPrimaryWithoutExistingFlowdownTest(){
    	
    	TestMembershipFlowdownUtil.changeAffiliationToPrimaryWithoutExistingFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }

	static testmethod void changeAffiliationToNonPrimaryWithExistingFlowdownTest(){
		
		TestMembershipFlowdownUtil.changeAffiliationToNonPrimaryWithExistingFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD);
    }


	static testmethod void joinOnNewPrimaryAffiliationTest(){
		
		TestMembershipFlowdownUtil.joinOnNewPrimaryAffiliationTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnnewPrimaryAffiliationFlowdownTwoLevelsTest(){
		
		TestMembershipFlowdownUtil.joinOnnewPrimaryAffiliationFlowdownTwoLevelsTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnNewIndivToIndivPrimaryAffilWithNoFlowdownTest(){
		
		TestMembershipFlowdownUtil.joinOnNewIndivToIndivPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnNewIndivToOrgPrimaryAffilWithNoFlowdownTest(){
		
		TestMembershipFlowdownUtil.joinOnNewIndivToOrgPrimaryAffilWithNoFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void deletePrimaryAffilErasingJoinOnFlowdownTest(){
		
		TestMembershipFlowdownUtil.deletePrimaryAffilErasingJoinOnFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void changeAffiliationToPrimaryWithoutExistingJoinOnFlowdownTest(){
		
		TestMembershipFlowdownUtil.changeAffiliationToPrimaryWithoutExistingJoinOnFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
    
    static testmethod void joinOnChangeAffiliationToNonPrimaryWithExistingFlowdownTest(){
		
		TestMembershipFlowdownUtil.joinOnChangeAffiliationToNonPrimaryWithExistingFlowdownTest(
    	    Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID,
    	    Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID,
    	    new TestAssociateMembershipInserter(),
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_MEMBERSHIP_FIELD,
    	    Constant.ASSOCIATE_MEMBERSHIP_TYPE_ACCOUNT_JOINON_FIELD);
    }
}