/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCustomDynamicsSLBatchExporter {

	private static final Integer YEAR = Date.today().year();

    private static NU__Entity__c entity = null;
	private static Account member_account = null;
	private static NU__MembershipType__c membershipType = null;
	private static NU__Membership__c membership = null;

	private static NU__Product__c membershipProduct = null;
    private static NU__MembershipTypeProductLink__c mtpl = null;
    private static NU__PriceClass__c priceClass = null;
    private static NU__Order__c order1 = null;
    private static NU__OrderItem__c order_item1 = null;
	private static NU__OrderItemLine__c oil1 = null;

	private static NU__Batch__c batch = null;
	private static NU.TransactionGenerator.TransactionGenerationRequest tgr = null;
	private static NU.TransactionGenerator tg = null;
	private static NU.TransactionGenerator.TransactionGenerationResult tgResult = null;

	private static void setupTest() {
		//build one entity and an individual account
		entity = NU.DataFactoryEntity.insertEntity();
		member_account = NU.DataFactoryAccount.insertIndividualAccount();

		//define a membership type
		membershipType = NU.DataFactoryMembershipType.createDefaultMembershipType(entity.Id);
		membershipType.Name = 'Member';
		insert membershipType;

		//define a membership product, product link, and associated gls
		membershipProduct = NU.DataFactoryProduct.createMembershipProduct('Member Product', 100.00, entity.Id);
		insert membershipProduct;
		mtpl = NU.DataFactoryMembershipTypeProductLink.insertMembershipTypeProdLink(membershipType.Id, membershipProduct.Id);
		
		//define a price class
		priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();

		//make one individual account an active member using an order
		order1 = new NU__Order__c (
            NU__Entity__c = entity.Id,
            NU__BillTo__c = member_account.Id,
            NU__InvoiceDate__c = Date.today(),
            NU__InvoiceTerm__c = entity.NU__InvoiceTerm__c,
            NU__TransactionDate__c = Date.today()
        );
        insert order1;
		order_item1 = new NU__OrderItem__c (
            NU__Customer__c = Member_account.Id,
            NU__Order__c = order1.Id,
            NU__PriceClass__c = priceClass.Id
        );
		insert order_item1;
		oil1 = new NU__OrderItemLine__c (
            NU__MembershipTypeProductLink__c = mtpl.Id,
            NU__OrderItem__c = order_item1.Id,
            NU__UnitPrice__c = 100.00,
            NU__Quantity__c = 1,
            NU__Product__c = membershipProduct.Id
        );
		insert oil1;
		membership = new NU__Membership__c (
            NU__Account__c = member_account.Id,
            NU__EndDate__c = Date.newInstance(YEAR, 12, 31),
            NU__MembershipType__c = membershipType.Id,
            NU__OrderItemLine__c = oil1.Id,
            NU__StartDate__c = Date.newInstance(YEAR, 1, 1)
        );
		insert membership;
		
		//build batch and transactions
		batch = NU.DataFactoryBatch.insertManualBatch(entity.Id);
		tgr = new NU.TransactionGenerator.TransactionGenerationRequest();
		tgr.BatchId = batch.Id;
		tgr.TransactionDate = Date.today();
		tgr.OrderItemLines.add(oil1);
		tg = new NU.TransactionGenerator();
		tgResult = tg.Generate(tgr);
		
		//post the batch
		batch.NU__status__c = 'Posted';
		update batch;	
	}

    static testmethod void customDynamicsSLPreviewTest(){
    	setupTest();
    	
    	CustomDynamicsSLBatchExporter SLBatchExporter = new CustomDynamicsSLBatchExporter();
    	
    	Component.Apex.OutputPanel outputPanel = SLBatchExporter.getPreviewComponent(new Set<Id>{ batch.Id });
    	
    	system.assert(outputPanel != null);
    	//system.assert(OperationResult.isSuccessful(exportResult.Result));
    	//system.assert(exportResult.SuccessView != null);
    }

    static testmethod void customDynamicsSLExportTest(){
    	setupTest();
    	
    	CustomDynamicsSLBatchExporter SLBatchExporter = new CustomDynamicsSLBatchExporter();
    	NU.BatchExportResult exportResult = SLBatchExporter.export(new Set<Id>{ batch.Id });
    	
    	system.assert(exportResult != null);
    	system.assert(NU.OperationResult.isSuccessful(exportResult.Result));
    	system.assert(exportResult.SuccessView != null);
    }
}