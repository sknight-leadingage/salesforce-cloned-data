<apex:component access="global" controller="KYInvoice">

    <apex:attribute name="orderlist" description="orders" type="NU__Order__c[]" assignTo="{!Orders}"/>
    <apex:attribute name="order_Id" description="order Id" type="Id" assignTo="{!orderId}" />

    <div style="font-family:verdana,arial,sans-serif;font-size:10px; margin-top: 5px;">
    
        <apex:repeat value="{!OrderWrappers}" var="ow">
    
            <h1 style="float:right;padding-right:10px; padding-top: 0px; margin-top: 0px;">INVOICE</h1>
            
            <div style="padding:0px 10px">
            
                <apex:outputPanel rendered="{!!ISBLANK(ow.Order.NU__Entity__r.NU__LogoURL__c)}" layout="none">
                        <apex:image value="{!ow.Order.NU__Entity__r.NU__LogoURL__c}" style="display:inline;vertical-align:middle;float:left;padding-right:10px" />
                </apex:outputPanel>

                <div style="float:left">
                    {!ow.Order.NU__Entity__r.Name}<br/>
                    {!ow.Order.NU__Entity__r.NU__Street__c}<br/>
                    {!ow.Order.NU__Entity__r.NU__City__c}, {!ow.Order.NU__Entity__r.NU__State__c} {!ow.Order.NU__Entity__r.NU__PostalCode__c}<br/>
                    {!ow.Order.NU__Entity__r.NU__Country__c}<br/>
                    
                    <apex:outputPanel rendered="{!!IsBlank(ow.Order.NU__Entity__r.NU__Phone__c)}">
                    {!ow.Order.NU__Entity__r.NU__Phone__c}<br/>
                    </apex:outputPanel>
                    
                    <apex:outputPanel rendered="{!!IsBlank(ow.Order.NU__Entity__r.NU__Website__c)}">
                    {!ow.Order.NU__Entity__r.NU__Website__c}<br/>
                    </apex:outputPanel>
                </div>

                <div style="clear:both"/>

                <table style="float:right;border-collapse:collapse;" cellspacing="0" cellpadding="5">
                    <tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">Invoice Number</td>
                        <td style="border:thin solid black">{!ow.Order.NU__InvoiceNumber__c}</td>
                    </tr><tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">Invoice Date</td>
                        <td style="border:thin solid black"><apex:outputText value="{0,date,M/d/yyyy}"><apex:param value="{!ow.Order.NU__InvoiceDate__c}"/></apex:outputText></td>
                    </tr><tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">Invoice Term</td>
                        <td style="border:thin solid black"><apex:outputField value="{!ow.Order.NU__InvoiceTerm__c}" /></td>
                    </tr><tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">Due Date</td>
                        <td style="border:thin solid black"><apex:outputText value="{0,date,M/d/yyyy}"><apex:param value="{!ow.Order.NU__InvoiceDueDate__c}"/></apex:outputText></td>
                    </tr>
                    
                    <apex:outputPanel rendered="{!!IsBlank(ow.Order.NU__PurchaseOrderNumber__c)}">
                    <tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">PO Number</td>
                        <td style="border:thin solid black">{!ow.Order.NU__PurchaseOrderNumber__c}</td>
                    </tr>
                    </apex:outputPanel>
                    
                    <tr>
                        <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:right">LeadingAge Id</td>
                        <td style="border:thin solid black">{!ow.Order.NU__BillTo__r.LeadingAge_ID__c}</td>
                    </tr>
                </table>
                
                <div style="margin:40px 0px; padding-left: 100px; font-size: 1.7em;">
                    <b>Billing Address</b><br/>
                    <apex:outputText rendered="{! IsBlank(ow.Order.Invoice_Attention_To__c) = false}" escape="false">
                        Attn: {!ow.Order.Invoice_Attention_To__c} <br />
                    </apex:outputText>
                    
                    {!ow.Order.NU__BillTo__r.Name}<br/>
                    {!ow.Order.NU__BillTo__r.NU__PrimaryAffiliation__r.Name}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.NU__PrimaryAffiliation__c)}"/>
                    {!ow.Order.NU__BillTo__r.BillingStreet}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.BillingStreet)}"/>
                    {!ow.Order.NU__BillTo__r.BillingCity}
                    <apex:outputText value=", " rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.BillingCity) && (!ISBLANK(ow.Order.NU__BillTo__r.BillingState) || !ISBLANK(ow.Order.NU__BillTo__r.BillingPostalCode))}"/>
                    {!ow.Order.NU__BillTo__r.BillingState} {!ow.Order.NU__BillTo__r.BillingPostalCode}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.BillingStreet) || !ISBLANK(ow.Order.NU__BillTo__r.BillingCity) || !ISBLANK(ow.Order.NU__BIllTo__r.BillingPostalCode)}"/>
                    {!ow.Order.NU__BillTo__r.BillingCountry}
                </div>

                <div style="margin:40px 0px; padding-left: 100px; font-size: 1.7em;">
                    <b>Shipping Address</b><br/>
                    {!ow.Order.NU__BillTo__r.Name}<br/>
                    {!ow.Order.NU__BillTo__r.NU__PrimaryAffiliation__r.Name}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.NU__PrimaryAffiliation__c)}"/>
                    {!ow.Order.NU__BillTo__r.ShippingStreet}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.ShippingStreet)}"/>
                    {!ow.Order.NU__BillTo__r.ShippingCity}
                    <apex:outputText value=", " rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.ShippingCity) && (!ISBLANK(ow.Order.NU__BillTo__r.ShippingState) || !ISBLANK(ow.Order.NU__BillTo__r.ShippingPostalCode))}"/>
                    {!ow.Order.NU__BillTo__r.ShippingState} {!ow.Order.NU__BillTo__r.ShippingPostalCode}
                    <apex:outputText value="<br/>" escape="false" rendered="{!!ISBLANK(ow.Order.NU__BillTo__r.ShippingStreet) || !ISBLANK(ow.Order.NU__BillTo__r.ShippingCity) || !ISBLANK(ow.Order.NU__BillTo__r.ShippingPostalCode)}"/>
                    {!ow.Order.NU__BillTo__r.ShippingCountry}
                </div>

                <div style="clear:both"/>

                <apex:outputPanel rendered="{!ow.OrderItems.size > 0}">
                    <table style="border-collapse:collapse;width:100%;margin-top:20px" cellspacing="0" cellpadding="5">
                        <tr>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Product Name</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Quantity</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">List Price</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Sales Price</td>
                            
                            <apex:outputPanel rendered="{!ow.Order.NU__TotalTax__c != 0}">
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Tax Rate</td>
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Tax Value</td>
                            </apex:outputPanel>
                            
                            <td style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center">Net Value</td>
                        </tr>
                        
                        <apex:repeat var="orderItem" value="{!ow.orderItems}">
                            <tr >

                                <td colspan="7" style="font-size: 1.2em; border:thin solid black;" >
                                    <apex:outputPanel rendered="{!IF(contains(ow.Order.NU__Entity__r.Name, 'Kentucky') || contains(orderItem.NU__Customer__r.Name, 'Kentucky') , false, true)}">
                                    <apex:outputField value="{!orderItem.RecordType.Name}" />

                                    <span style="padding-left: 10px; font-weight: bold;">Customer</span>
                                    <apex:outputText value="{!orderItem.NU__Customer__r.Name}" style="padding-left: 10px;" />
                                    
                                    <apex:outputText value="Event" style="padding-left: 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Registration'}" />
                                    <apex:outputText value="{!orderItem.NU__Registrations2__r[0].NU__Event__r.Name}" rendered="{!orderItem.RecordType.Name == 'Registration'}" style="padding-left: 10px;" />
                                    <apex:outputText value="Event Note(s): {!orderItem.NU__Registrations2__r[0].NU__Event__r.NU__InvoiceText__c}" rendered="{!orderItem.RecordType.Name == 'Registration' && IsBlank(orderItem.NU__Registrations2__r[0].NU__Event__r.NU__InvoiceText__c) = False}" style="display: block; margin-top: 10px;" />
                                    
                                    
                                    <apex:outputText value="Membership Type" style="padding-left: 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    <apex:outputText value="{!orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Name}" rendered="{!orderItem.RecordType.Name == 'Membership'}" style="padding-left: 10px;" />

									</apex:outputPanel>
									
                                    <apex:outputText value="Term" style="padding: 0px 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    <apex:outputField value="{!orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__StartDate__c}" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    <apex:outputText value="-" rendered="{!orderItem.RecordType.Name == 'Membership'}" style="padding: 0px 5px;" />
                                    <apex:outputField value="{!orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__EndDate__c}" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    
                                    <apex:outputText value="Dues Price" style="padding: 0px 10px; font-weight: bold;" rendered="{! orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c }" />
                                    <apex:outputField value="{!orderItem.NU__GrandTotal__c}" rendered="{! orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c }" />
                                </td>
                            </tr>
                            <apex:repeat var="oil" value="{!orderItem.NU__OrderItemLines__r}" rendered="{! orderItem.RecordType.Name <> 'Membership' || orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c == false }">
                                <tr>
                                    <td style="border:thin solid black; padding-left: 30px;">
                                        <apex:outputField value="{!oil.NU__Product__r.Name}" />
                                        <apex:outputText value=" ( {!oil.NU__EventBadge__r.NU__Name__c} )" rendered="{! !IsBlank(oil.NU__EventBadge__r.NU__Name__c) && oil.NU__Product__r.NU__IsEventBadge__c == true }" />
                                        
                                        <apex:outputText value="Term" style="padding: 0px 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Subscription'}" />
                                        <apex:outputField value="{!oil.NU__Subscription__r.NU__StartDate__c}" rendered="{!orderItem.RecordType.Name == 'Subscription'}" />
                                        <apex:outputText value="-" rendered="{!orderItem.RecordType.Name == 'Subscription'}" style="padding: 0px 5px;" />
                                        <apex:outputField value="{!oil.NU__Subscription__r.NU__EndDate__c}" rendered="{!orderItem.RecordType.Name == 'Subscription'}" />
                                        
                                        <apex:outputText value="{! $ObjectType.Exhibitor__c.Fields.Booth_Number__c.Label }" style="padding: 0px 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Exhibitor' && IsBlank(oil.Exhibitor__c) == false}" />
                                        <apex:outputField value="{!oil.Exhibitor__r.Booth_Number__c}" rendered="{!orderItem.RecordType.Name == 'Exhibitor'  && IsBlank(oil.Exhibitor__c) == false}" />
                                    </td>
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__Quantity__c}" /></td>
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__Product__r.NU__ListPrice__c}" /></td>
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__UnitPrice__c}" /></td>
                                    
                                    <apex:outputPanel rendered="{!ow.Order.NU__TotalTax__c != 0}">
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c}" /></td>
                                    <td style="border:thin solid black;text-align:right">$<apex:outputText value="{0,number,0.00}"><apex:param value="{!oil.NU__UnitPrice__c * oil.NU__Quantity__c * BLANKVALUE(oil.NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c, 0) / 100.0}" /></apex:outputText></td>
                                    </apex:outputPanel>
                                    
                                    <td style="border:thin solid black;text-align:right">$<apex:outputText value="{0,number,0.00}"><apex:param value="{!oil.NU__UnitPrice__c * oil.NU__Quantity__c * (100 + BLANKVALUE(oil.NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c, 0)) / 100.0}" /></apex:outputText></td>
                                </tr>
                            </apex:repeat>
                        </apex:repeat>
                        
                        <!-- ****************************** Bed Counts ***********************************************-->
                                                <apex:repeat var="orderItem" value="{!ow.orderItems}">
                                               
                          <!--<tr >

                                <td colspan="7" style="font-size: 1.2em; border:thin solid black;" >
                                    <apex:outputPanel rendered="{!IF(contains(ow.Order.NU__Entity__r.Name, 'Kentucky') || contains(orderItem.NU__Customer__r.Name, 'Kentucky') , false, true)}">
                                    <apex:outputField value="{!orderItem.RecordType.Name}" />

                                    <span style="padding-left: 10px; font-weight: bold;">Customer</span>
                                    <apex:outputText value="{!orderItem.NU__Customer__r.Name}" style="padding-left: 10px;" />
                                    
                                    <apex:outputText value="Event" style="padding-left: 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Registration'}" />
                                    <apex:outputText value="{!orderItem.NU__Registrations2__r[0].NU__Event__r.Name}" rendered="{!orderItem.RecordType.Name == 'Registration'}" style="padding-left: 10px;" />
                                    <apex:outputText value="Event Note(s): {!orderItem.NU__Registrations2__r[0].NU__Event__r.NU__InvoiceText__c}" rendered="{!orderItem.RecordType.Name == 'Registration' && IsBlank(orderItem.NU__Registrations2__r[0].NU__Event__r.NU__InvoiceText__c) = False}" style="display: block; margin-top: 10px;" />
                                    
                                    
                                    <apex:outputText value="Membership Type" style="padding-left: 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    <apex:outputText value="{!orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Name}" rendered="{!orderItem.RecordType.Name == 'Membership'}" style="padding-left: 10px;" />

									</apex:outputPanel>
									
                                    <apex:outputText value="Term" style="padding: 0px 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    <apex:outputField value="{!orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__StartDate__c}" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    <apex:outputText value="-" rendered="{!orderItem.RecordType.Name == 'Membership'}" style="padding: 0px 5px;" />
                                    <apex:outputField value="{!orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__EndDate__c}" rendered="{!orderItem.RecordType.Name == 'Membership'}" />
                                    
                                    <apex:outputText value="Dues Price" style="padding: 0px 10px; font-weight: bold;" rendered="{! orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c }" />
                                    <apex:outputField value="{!orderItem.NU__GrandTotal__c}" rendered="{! orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c }" />
                                </td>
                            </tr>-->
                            <apex:repeat var="oil" value="{!orderItem.NU__OrderItemLines__r}" rendered="{! orderItem.RecordType.Name <> 'Membership' || orderItem.NU__OrderItemLines__r[0].NU__Membership__r.NU__MembershipType__r.Combine_Dues_on_Invoice__c == false }">
                                <tr>
                                    <td style="border:thin solid black; padding-left: 30px;">
                                        <apex:outputField value="{!oil.NU__Product__r.Name}" />
                                        <apex:outputText value=" ( {!oil.NU__EventBadge__r.NU__Name__c} )" rendered="{! !IsBlank(oil.NU__EventBadge__r.NU__Name__c) && oil.NU__Product__r.NU__IsEventBadge__c == true }" />
                                        
                                        <apex:outputText value="Term" style="padding: 0px 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Subscription'}" />
                                        <apex:outputField value="{!oil.NU__Subscription__r.NU__StartDate__c}" rendered="{!orderItem.RecordType.Name == 'Subscription'}" />
                                        <apex:outputText value="-" rendered="{!orderItem.RecordType.Name == 'Subscription'}" style="padding: 0px 5px;" />
                                        <apex:outputField value="{!oil.NU__Subscription__r.NU__EndDate__c}" rendered="{!orderItem.RecordType.Name == 'Subscription'}" />
                                        
                                        <apex:outputText value="{! $ObjectType.Exhibitor__c.Fields.Booth_Number__c.Label }" style="padding: 0px 10px; font-weight: bold;" rendered="{!orderItem.RecordType.Name == 'Exhibitor' && IsBlank(oil.Exhibitor__c) == false}" />
                                        <apex:outputField value="{!oil.Exhibitor__r.Booth_Number__c}" rendered="{!orderItem.RecordType.Name == 'Exhibitor'  && IsBlank(oil.Exhibitor__c) == false}" />
                                    </td>
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__Quantity__c}" /></td>
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__Product__r.NU__ListPrice__c}" /></td>
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__UnitPrice__c}" /></td>
                                    
                                    <apex:outputPanel rendered="{!ow.Order.NU__TotalTax__c != 0}">
                                    <td style="border:thin solid black;text-align:right"><apex:outputField value="{!oil.NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c}" /></td>
                                    <td style="border:thin solid black;text-align:right">$<apex:outputText value="{0,number,0.00}"><apex:param value="{!oil.NU__UnitPrice__c * oil.NU__Quantity__c * BLANKVALUE(oil.NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c, 0) / 100.0}" /></apex:outputText></td>
                                    </apex:outputPanel>
                                    
                                    <td style="border:thin solid black;text-align:right">$<apex:outputText value="{0,number,0.00}"><apex:param value="{!oil.NU__UnitPrice__c * oil.NU__Quantity__c * (100 + BLANKVALUE(oil.NU__OrderItem__r.NU__SalesTax__r.NU__TaxRate__c, 0)) / 100.0}" /></apex:outputText></td>
                                </tr>
                            </apex:repeat>
                        </apex:repeat>
                    </table>
                </apex:outputPanel>
                
                <div style="position: relative; margin: 20px 0px;">

                    <table style="border-collapse:collapse; width: 100%; page-break-inside:avoid;" cellspacing="0" cellpadding="0">
                        <tr>
                            <apex:outputPanel layout="none" rendered="{!!ISBLANK(ow.Order.NU__InvoiceDescription__c)}">
                                <td style="vertical-align: top;">
                                    <table style="border-collapse:collapse;" cellspacing="0" cellpadding="5">
                                        <tr style="border:thin solid black;background-color:#EEE;font-weight:bold;text-align:center"><td>Description</td></tr>
                                        <tr style="border:thin solid black;"><td>{!ow.Order.NU__InvoiceDescription__c}</td></tr>
                                    </table>
                                </td>
                            </apex:outputPanel>

                            <td style="vertical-align: top; padding-left: 50px;">

                                <table style="border-collapse:collapse; float: right;" cellspacing="0" cellpadding="5">
                                    <apex:outputPanel rendered="{! ow.Order.NU__TotalTax__c != 0 || ow.Order.NU__TotalShipping__c != 0}">
                                    <tr>
                                        <td style="text-align:right;font-weight:bold">Sub Total</td>
                                        <td style="border:thin solid black;text-align:right;">
                                            <apex:outputField value="{!ow.Order.NU__SubTotal__c}" />
                                        </td>
                                    </tr>
                                    </apex:outputPanel>

                                    <apex:outputPanel rendered="{!ow.Order.NU__TotalTax__c != 0}">
                                    <tr>
                                        <td style="text-align:right;font-weight:bold">Tax</td>
                                        <td style="border:thin solid black;text-align:right">
                                            <apex:outputField value="{!ow.Order.NU__TotalTax__c}" />
                                        </td>
                                    </tr>
                                    </apex:outputPanel>
                                    
                                    <apex:outputPanel rendered="{!ow.Order.NU__TotalShipping__c != 0}">
                                    <tr>
                                        <td style="text-align:right;font-weight:bold">Shipping</td>
                                        <td style="border:thin solid black;text-align:right">
                                            <apex:outputField value="{!ow.Order.NU__TotalShipping__c}" />
                                        </td>
                                    </tr>
                                    </apex:outputPanel>
                                    
                                    <tr>
                                        <td style="text-align:right;font-weight:bold">Total</td>
                                        <td style="border:thin solid black;text-align:right">
                                            <apex:outputField value="{!ow.Order.NU__GrandTotal__c}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;font-weight:bold">Payment</td>
                                        <td style="border:thin solid black;text-align:right">
                                            <apex:outputField value="{!ow.Order.NU__TotalPayment__c}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right;font-weight:bold; font-size: 2.0em;">Balance</td>
                                        <td style="border:thin solid black;text-align:right;background-color:#EEE; font-size: 2.0em;">
                                            <apex:outputField value="{!ow.Order.NU__Balance__c}" />
                                        </td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <br>
                </br>
                <apex:outputPanel rendered="{!ISBLANK(ow.Order.NU__Entity__r.Invoice_Footer__c) = false}" layout="none">
                 <div id="Footer" style="border: 1px solid black; margin-left: auto; margin-right: auto; padding: 5px; text-align: left; font-size: 1em; font-style: italic;">   {!ow.Order.NU__Entity__r.Invoice_Footer__c}  </div>
                </apex:outputPanel>
                
                <c:InvoiceRemittance invoiceNumber="{!ow.Order.NU__InvoiceNumber__c}"
                   name="{!ow.Order.NU__BillTo__r.Name}"
                   balance="{!ow.Order.NU__Balance__c}"
                   creditCardsAccepted="{!ow.creditCardsAccepted}"
                   street="{!ow.Order.NU__Entity__r.NU__RemittanceStreet__c}"
                   city="{!ow.Order.NU__Entity__r.NU__RemittanceCity__c}"
                   state="{!ow.Order.NU__Entity__r.NU__RemittanceState__c}"
                   postalCode="{!ow.Order.NU__Entity__r.NU__RemittancePostalCode__c}"
                   country="{!ow.Order.NU__Entity__r.NU__RemittanceCountry__c}"
                   entity="{!ow.Order.NU__Entity__r}"
                   leadingageid="{! ow.Order.NU__BillTo__r.LeadingAge_Id__c }"
                   rendered="{!ow.Order.NU__Entity__r.NU__RemittanceEnabled__c}" />
            </div>
            
            <div style="page-break-after:always"/>
        
        </apex:repeat>

    </div>
</apex:component>