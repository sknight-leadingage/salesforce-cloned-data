<apex:page renderAs="{!RptRenderAs}" controller="EduSpeakerListings_Controller">
<head>
<style type="text/css" media="print">
    @page {
        @top-center {
            content: element(header);
        }
        
        margin: 22px 0 0 0; padding: 0;
        font-family: Arial,Tahoma,san-serif;
        font-size: 14px;
    }
    
    @page :first {
        @top-center {
            content: element(header);
        }
        
        margin: 0; padding: 0;
        font-family: Arial,Tahoma,san-serif;
        font-size: 14px;
    }
    
    div.header {
        width: 100%; font-style: italic; text-align: center; font-size: 12px;
    }
    
    h3 { font-size: 16px; }
</style>
</head>
<apex:pageMessages id="msg" />
 <apex:form >
       <apex:outputPanel rendered="{!IF(RptRenderAs == '', true, false)}">
           <apex:commandButton action="{! viewConferenceReports }" value="Reports Main Menu" /> &nbsp; <apex:commandButton value="Print View" action="{!PrintOutput}" />
           <br /><br />
            <apex:commandButton action="{!WizardBack}" value="Back" rendered="{!IF(WizardStep > 1,true,false)}" immediate="true" />
             &nbsp; 
            <apex:commandButton action="{!WizardNext}" value="Next" rendered="{!IF(WizardStep < 2,true,false)}"/>
           <br />
       </apex:outputPanel>
       
       <p><center><h3>{!PageTitle}<br />{!PageSubtitle}</h3></center></p>
       
        <apex:outputPanel rendered="{!IF(WizardStep=1,true,false)}">
            <br />
            <apex:outputLabel value="Report Type" />
            <apex:outputPanel >
                <div class="requiredInput">
                    <div class="requiredBlock"></div>
                    <apex:selectRadio value="{!RTypeSel}" label="Please select a Report Type from the list">
                        <apex:selectOptions value="{!RTypeItems}"></apex:selectOptions>
                    </apex:selectRadio>
                </div>
            </apex:outputPanel>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!IF(RptRenderAs = '' && WizardStep != 1,true,false)}">
            <div style="width: 99%; padding: 8px;"><apex:commandLink action="{!SetConRepageFirst}" value="First Page of Records" rendered="{!setConHasPrev}"/> &nbsp; <apex:commandLink action="{!SetConRepagePrev}" value="Previous Page of Records" rendered="{!setConHasPrev}"/> &nbsp; <apex:commandLink action="{!SetConRepageNext}" value="Next Page of Records"  rendered="{!setConHasNext}"/> &nbsp; <apex:commandLink action="{!SetConRepageLast}" value="Last Page of Records" rendered="{!setConHasNext}"/></div>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!IF(WizardStep=2,true,false)}">
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tr><td colspan="4" width="100%"><hr size="2" width="100%" /></td></tr>
                <tr>
                    <td width="35%">Speaker Name</td>
                    <td width="15%">Session</td>
                    <td width="15%">Activity</td>
                    <td width="35%">Notes</td>
                </tr>
                <tr><td colspan="4" width="100%"><hr size="2" width="100%" /></td></tr>
            <apex:repeat value="{!SumList}" var="a">
                <tr>
                    <td width="35%">{!a.Speaker__r.LastName}, {!a.Speaker__r.FirstName}</td>
                    <td width="15%">{!a.Session__r.Session_Number__c}-{!a.Session__r.Timeslot__r.Timeslot_Code__c}</td>
                    <td width="15%">{!a.Role__c}</td>
                    <td width="35%">{!a.Session__r.Notes__c}</td>
                </tr>
            </apex:repeat>
            </table>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!IF(WizardStep=3,true,false)}">
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tr><td colspan="4" width="100%"><hr size="2" width="100%" /></td></tr>
                <tr>
                    <td width="55%">Speaker Name</td>
                    <td width="15%">Session</td>
                    <td width="15%">Activity</td>
                    <td width="15%">LeadingAge ID</td>
                </tr>
                <tr><td colspan="4" width="100%"><hr size="2" width="100%" /></td></tr>
            <apex:repeat value="{!SumList}" var="a">
                <tr>
                    <td width="55%">{!a.Speaker__r.LastName}, {!a.Speaker__r.FirstName}</td>
                    <td width="15%">{!a.Session__r.Session_Number__c}-{!a.Session__r.Timeslot__r.Timeslot_Code__c}</td>
                    <td width="15%">{!a.Role__c}</td>
                    <td width="15%">{!a.Speaker__r.LeadingAge_ID__c}</td>
                </tr>
            </apex:repeat>
            </table>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!IF(WizardStep=4,true,false)}">
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tr><td colspan="3" width="100%"><hr size="2" width="100%" /></td></tr>
                <tr>
                    <td width="35%">Speaker</td>
                    <td width="50%">Company</td>
                    <td width="15%">Session #</td>
                </tr>
                <tr><td colspan="3" width="100%"><hr size="2" width="100%" /></td></tr>
            <apex:repeat value="{!SumList}" var="a">
                <tr>
                    <td width="35%">{!a.Speaker__r.LastName}, {!a.Speaker__r.FirstName}</td>
                    <td width="50%">{!a.Speaker__r.Speaker_Company_Name_Rollup__c}</td>
                    <td width="15%">{!a.Session__r.Session_Number__c}-{!a.Session__r.Timeslot__r.Timeslot_Code__c}</td>
                </tr>
            </apex:repeat>
            </table>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(WizardStep=5,true,false)}">
            <table border="0" cellspacing="0" cellpadding="3" width="100%">
                <tr><td colspan="5" width="100%"><hr size="2" width="100%" /></td></tr>
                <tr>
                    <td width="25%">Speaker Name</td>
                    <td width="5%">Session</td>
                    <td width="20%">Timeslot</td>
                    <td width="15%">Activity</td>
                    <td width="35%">Notes</td>
                </tr>
                <tr><td colspan="5" width="100%"><hr size="2" width="100%" /></td></tr>
            <apex:repeat value="{!SumList}" var="a">
                <tr>
                    <td width="25%">{!a.Speaker__r.LastName}, {!a.Speaker__r.FirstName}</td>
                    <td width="5%">{!a.Session__r.Session_Number__c}-{!a.Session__r.Timeslot__r.Timeslot_Code__c}</td>
                    <td width="20%">{!a.Session__r.Timeslot__r.Session_Date__c} {!a.Session__r.Timeslot__r.Session_Time__c}</td>
                    <td width="15%">{!a.Role__c}</td>
                    <td width="35%">{!a.Session__r.Notes__c}</td>
                </tr>
            </apex:repeat>
            </table>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF(RptRenderAs = '' && WizardStep != 1,true,false)}">
            <div style="padding: 8px;"><apex:commandLink action="{!SetConRepageFirst}" value="First Page of Records" rendered="{!setConHasPrev}"/> &nbsp; <apex:commandLink action="{!SetConRepagePrev}" value="Previous Page of Records" rendered="{!setConHasPrev}"/> &nbsp; <apex:commandLink action="{!SetConRepageNext}" value="Next Page of Records"  rendered="{!setConHasNext}"/> &nbsp; <apex:commandLink action="{!SetConRepageLast}" value="Last Page of Records" rendered="{!setConHasNext}"/></div>
        </apex:outputPanel>
 </apex:form>
</apex:page>