public with sharing class EduEvalsComposites_Controller extends EduReportsControllerBase {
    private string srpt{get;set;}
    public string getReport() {
        if (srpt == null || srpt == '') {
            srpt = ApexPages.CurrentPage().getParameters().get('rpt');
        }
        return srpt;
    }
    public void setReport(string s) {
        srpt = s;
    }
    
    public string getPagePrintSize() {
        if (getReport() == 'sesscs')
        {
            return 'portrait';
        }
        else
        {
            return 'landscape';
        }
    }
    
    public List<Conference_Session__c> DataList { get;set; }
    public List<Conference_Session_Speaker__c> DataListSpk { get;set; }
    public AggregateResult[] groupedResults { get;set; }
    public AggregateResult[] groupedResultsTotals { get;set; }
    
    public override string getPageTitle() {
        string sTitle = '';
        if (getReport() == 'edprog')
        {
            sTitle = 'Program Averages'; //CompositeScores_EdProgram
        }
        else if (getReport() == 'sessoa')
        {
            sTitle = 'Sessions Ranked by Overall Average'; //Ranked_byOverallAvg
        }
        else if (getReport() == 'sessoat')
        {
            sTitle = 'Sessions Ranked by Track'; //Ranked_byTrack
        }
        else if (getReport() == 'sessoats') {
            sTitle = 'Sessions Ranked by Timeslot'; //Ranked_byTimeslot
        }
        else if (getReport() == 'sesscs')
        {
            sTitle = 'Session Composite Scores'; //CompositeScores_Sessions
        }
        else if (getReport() == 'srba')
        {
            sTitle = 'Sessions Ranked by Attendance'; //Ranked_byAttendance
        }
        else if (getReport() == 'srbys')
        {
            sTitle = 'Sessions Ranked by Speaker'; //Ranked_bySpeaker
        }
        else if (getReport() == 'sarbt') {
            sTitle = 'Sessions Ranked by Track'; //Attendance_byTrack
        }
        else if (getReport() == 'savgbys') {
            sTitle = 'Session Averages'; //Averages_bySession#
        }
        else if (getReport() == 'savgbyspk') {
            sTitle = 'Speaker Averages'; //Averages_bySession#
        }
        return Conference != null ? Conference.Name + ': ' + sTitle : '';
    }
    
    public string getPageTitleTop() {
        if (RptRenderAs == 'pdf')
        {
            return '';
        }
        else
        {
            string sTitle = '';
            if (getReport() == 'edprog')
            {
                sTitle = 'Program Averages'; //CompositeScores_EdProgram
            }
            else if (getReport() == 'sessoa') {
                sTitle = 'Sessions Ranked by Overall Average'; //Ranked_byOverallAvg
            }
            else if (getReport() == 'sessoat') {
                sTitle = 'Sessions Ranked by Track'; //Ranked_byTrack
            }
            else if (getReport() == 'sessoats') {
                sTitle = 'Sessions Ranked by Timeslot'; //Ranked_byTimeslot
            }
            else if (getReport() == 'sesscs')
            {
              sTitle = 'Session Composite Scores'; //CompositeScores_Sessions
            }
            else if (getReport() == 'srba')
            {
                sTitle = 'Sessions Ranked by Attendance'; //Ranked_byAttendance
            }
            else if (getReport() == 'srbys')
            {
                sTitle = 'Sessions Ranked by Speaker'; //Ranked_bySpeaker
            }
            else if (getReport() == 'sarbt') {
                sTitle = 'Sessions Ranked by Track'; //Attendance_byTrack
            }
            else if (getReport() == 'savgbys') {
                sTitle = 'Session Averages'; //Averages_bySession#
            }
            else if (getReport() == 'savgbyspk') {
                sTitle = 'Speaker Averages'; //Averages_bySession#
            }
            return Conference != null ? Conference.Name + ': ' + sTitle : '';
        }
    }
    
    public EduEvalsComposites_Controller() {
        if (getReport() == 'edprog')
        {
            groupedResults = [SELECT Conference_Track__r.Name, COUNT(Session_Number__c) numsess, AVG(Expected_Attendance__c) aexp, AVG(Actual_Attendance__c) aact, AVG(Survey_Count__c) ascount, AVG(Avg_Overall__c) aov, AVG(Avg_Quality__c) aq, AVG(Avg_Content__c) ac, AVG(Avg_Instruction__c) ai, AVG(Avg_Relevance__c) ar FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != 'General Session' GROUP BY Conference_Track__r.Name];
            groupedResultsTotals = [SELECT COUNT(Session_Number__c) numsess, AVG(Expected_Attendance__c) aexp, AVG(Actual_Attendance__c) aact, AVG(Survey_Count__c) ascount, AVG(Avg_Overall__c) aov, AVG(Avg_Quality__c) aq, AVG(Avg_Content__c) ac, AVG(Avg_Instruction__c) ai, AVG(Avg_Relevance__c) ar FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != 'General Session'];
        }
        else
        {
           SetConPageSize = 255;
           SetQuery();
        }
    }
    
    public override void SetConRefreshData() {
        if (getReport() != 'edprog')
        {
            if (getReport() == 'srbys' || getReport() == 'savgbyspk') {
                DataListSpk = setCon.getRecords();
            }
            else {
                DataList = setCon.getRecords();
            }
        }
    }
    
    public void SetQuery() {
        SetConQuery = '';
        if (getReport() == 'sessoa')
        {
            SetConQuery = 'SELECT Session_Title_Full__c, Avg_Overall__c, Conference_Track__r.Name, Survey_Count__c, Avg_Quality__c, Avg_Content__c, Avg_Instruction__c, Avg_Relevance__c FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Avg_Overall__c DESC';
        }
        else if (getReport() == 'sessoat')
        {
            SetConQuery = 'SELECT Session_Title_Full__c, Avg_Overall__c, Conference_Track__r.Name, Survey_Count__c, Avg_Quality__c, Avg_Content__c, Avg_Instruction__c, Avg_Relevance__c FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Conference_Track__r.Name ASC, Avg_Overall__c DESC';
        }
        else if (getReport() == 'sessoats')
        {
            SetConQuery = 'SELECT Session_Title_Full__c, Avg_Overall__c, Conference_Track__r.Name, Survey_Count__c, Avg_Quality__c, Avg_Content__c, Avg_Instruction__c, Avg_Relevance__c, Timeslot__r.Timeslot_Code__c, Timeslot__r.Short_Slot__c FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Timeslot__r.Timeslot_Code__c ASC, Avg_Overall__c DESC, Session_Number_Numeric__c ASC';
        }
        else if (getReport() == 'sesscs')
        {
            SetConQuery = 'SELECT Session_Title_Full__c, Survey_Count__c, Avg_Overall__c, Avg_Quality__c, Avg_Content__c, Avg_Instruction__c, Avg_Relevance__c, (SELECT Speaker__r.LeadingAge_ID__c, Speaker__r.Name, Topic_Score__c, Audience_Score__c, Presentation_Score__c, Info_Score__c, Overall_Average_Score__c FROM Conference_Session_Speakers__r WHERE Role__c = \'Speaker\' OR Role__c = \'Key Contact\' ORDER BY Speaker__r.LastName) FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Session_Number_Numeric__c';
        }
        else if (getReport() == 'srba')
        {
            SetConQuery = 'SELECT Session_Title_Full__c, Avg_Overall__c, Conference_Track__r.Name, Survey_Count__c, Expected_Attendance__c, Actual_Attendance__c FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Actual_Attendance__c DESC';
        }
        else if (getReport() == 'srbys')
        {
            SetConQuery = 'SELECT Speaker__r.Name,Role__c,Session__r.Session_Title_Full__c,Overall_Average_Score__c,Session__r.Conference_Track__r.Name,Session__r.Survey_Count__c,Topic_Score__c,Audience_Score__c,Presentation_Score__c,Info_Score__c FROM Conference_Session_Speaker__c WHERE Session__r.Conference__c = :EventId AND Session__r.Conference_Track__r.Name != \'General Session\' AND (Role__c = \'Key Contact\' OR Role__c = \'Speaker\') ORDER BY Overall_Average_Score__c DESC,Session__r.Timeslot__r.Timeslot_Code__c,Session__r.Session_Number_Numeric__c, Speaker__r.LastName, Speaker__r.FirstName';
        }
        else if (getReport() == 'sarbt') {
            SetConQuery = 'SELECT Session_Title_Full__c, Conference_Track__r.Name, Actual_Attendance__c FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Conference_Track__r.Name, Avg_Overall__c DESC';
        }
        else if (getReport() == 'savgbys') {
            SetConQuery = 'SELECT Session_Title_Full__c, Avg_Overall__c, Conference_Track__r.Name, Survey_Count__c, Avg_Quality__c, Avg_Content__c, Avg_Instruction__c, Avg_Relevance__c FROM Conference_Session__c WHERE Conference__c = :EventId AND Conference_Track__r.Name != \'General Session\' ORDER BY Timeslot__r.Timeslot_Code__c, Session_Number_Numeric__c';
        }
        else if (getReport() == 'savgbyspk') {
            SetConQuery = 'SELECT Speaker__r.Name,Role__c,Session__r.Session_Title_Full__c,Overall_Average_Score__c,Session__r.Conference_Track__r.Name,Session__r.Survey_Count__c,Topic_Score__c,Audience_Score__c,Presentation_Score__c,Info_Score__c FROM Conference_Session_Speaker__c WHERE Session__r.Conference__c = :EventId AND Session__r.Conference_Track__r.Name != \'General Session\' AND (Role__c = \'Key Contact\' OR Role__c = \'Speaker\') ORDER BY Speaker__r.LastName, Speaker__r.FirstName, Session__r.Timeslot__r.Timeslot_Code__c,Session__r.Session_Number_Numeric__c';
        }
        
        if (SetConQuery != '')
        {
            setCon.setPageSize(SetConPageSize);
            setCon.first();
            SetConRefreshData();
        }
    }
}