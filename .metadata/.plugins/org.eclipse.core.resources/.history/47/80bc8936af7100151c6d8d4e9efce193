public with sharing class DataFactoryProductExt {
	
	public static NU__Product__c createProduct(String productName, Id entityId, Id revenueGLAccountId, Map<String, Schema.RecordTypeInfo> productRecordTypes, String recordType) {
        NU__Product__c product = new NU__Product__c(
            Name = productName,
            NU__Entity__c = entityId,
            NU__ListPrice__c = 500,
            NU__Status__c = 'Active',
            NU__RevenueGLAccount__c = revenueGLAccountId,
            RecordTypeId = productRecordTypes.get(recordType).getRecordTypeId());
        return product;
    }
	
	public static NU__Product__c insertProduct(String productName, Id entityId, Id revenueGLAccountId, Map<String, Schema.RecordTypeInfo> productRecordTypes, String recordType) {
        NU__Product__c product = createProduct(productName, entityId, revenueGLAccountId, productRecordTypes, recordType);
        insert product;
        return product;
    }
    
    public static NU__Product__c insertAdvertisingProduct(){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
    	NU__GLAccount__c glAccount = NU.DataFactoryGLAccount.insertRevenueGLAccount(anyEntity.Id);
    	
    	Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
    	
    	NU__Product__c product = createProduct(Constant.ORDER_RECORD_TYPE_ADVERTISING, anyEntity.Id, glAccount.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_ADVERTISING);

        insert product;
        return product;
    }
    
    public static List<NU__Product__c> insertOnlineSubscriptionProducts(Integer numToInsert){
    	NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
    	NU__GLAccount__c glAccount = NU.DataFactoryGLAccount.insertRevenueGLAccount(anyEntity.Id);
    	
    	Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
    	
    	List<NU__Product__c> subscriptionProds = new List<NU__Product__c>();
    	
    	for (Integer i = 0; i < numToInsert; ++i){
    		NU__Product__c onlineSubscriptionProd = createProduct(Constant.ORDER_RECORD_TYPE_ONLINE_SUBSCRIPTION + (i + 1), anyEntity.Id, glAccount.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_ONLINE_SUBSCRIPTION);
    		
    		subscriptionProds.add(onlineSubscriptionProd);
    	}

        insert subscriptionProds;
        return subscriptionProds;
    }
}