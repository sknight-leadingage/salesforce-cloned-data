public with sharing class ConferenceSessionQuerier {
    public static List<Conference_Session__c> getConferenceSessionsByConferenceId(Id conferenceId){
        return [select AV_Requirements__c,
                       Abstract__c,
                       Activity_Workers_Certification__c,
                       Actual_Attendance__c,
                       Aisle_Microphone__c,
                       CA_RCFE__c,
                       CFRE_Certification__c,
                       Company_Name__c,
                       Conference_Track__c,
                       Conference__c,
                       Constituencies__c,
                       Developer__c,
                       Division__c,
                       Domain__c,
                       Education_Staff__c,
                       EventTime__c,
                       Expected_Attendance__c,
                       External_Id__c,
                       Flipchart_and_Markers__c,
                       Florida__c,
                       Id,
                       International__c,
                       Kansas__c,
                       Learning_Objective_1__c,
                       Learning_Objective_2__c,
                       Learning_Objective_3__c,
                       Locked__c,
                       Missouri__c,
                       NAB__c,
                       Name,
                       NASBA__c,
                       No_Additional_AV__c,
                       Notes__c,
                       Originator__c,
                       Other_Setup__c,
                       Other_Setup_Checkbox__c,
                       Proposal__c,
                       Proposal__r.Proposal_Number__c,
                       PTF_Code__c,
                       PTF_Hours__c,
                       Recommended_Number_Of_Readers__c,
                       Reporting_Requirements__c,
                       Room__c,
                       Scan_In_Out_Paramters__c,
                       Session_AV_Modified__c,
                       Session_AV_Validated__c,
                       Session_Uploads_Modified__c,
                       Session_Uploads_Validated__c,
                       Session_Length__c,
                       Session_Length_History__c,
                       Session_Number_Sort__c,
                       Session_Number__c,
                       Session_Title_Full__c,
                       Sponsorship_Level__c,
                       Status__c,
                       Submitter_Full_Name__c,
                       Submitter__r.FirstName,
                       Submitter__r.LastName,
                       Submitter__r.Speaker_Company_1__c,
                       Submitter__r.Speaker_Company_Name_Rollup__c,
                       Submitter__r.PersonTitle,
                       Submitter__r.LeadingAge_ID__c,
                       Submitter__c,
                       Timeslot__c,
                       Timeslot__r.Timeslot__c,
                       Timeslot__r.Timeslot_Code__c,
                       Title__c,
                       Wheelchair_Access_and_Ramp__c,
                       Wireless_Microphone__c,
                       (Select id,
                               name,
                               Proposal_Number__c
                          from Conference_Proposals__r),
                       (Select id,
                               name,
                               Speaker__c,
                               Speaker__r.NU__FullName__c,
                               Speaker__r.PersonTitle,
                               Speaker__r.Speaker_Company_1__c,
                               Speaker__r.Speaker_Company_Name_Rollup__c,
                               Speaker__r.BillingCity,
                               Speaker__r.BillingState,
                               Speaker__r.BillingCountry,
                               Speaker__r.Leadingage_Id__c,
                               Order__c,
                               Role__c,
                               Overall_Average_Score__c,
                               Topic_Score__c,
                               Audience_Score__c,
                               Presentation_Score__c,
                               Info_Score__c
                          from Conference_Session_Speakers__r
                          order by Speaker__r.LastName, Speaker__r.FirstName),
                       (Select id,
                               name,
                               Conference_Session__c,
                               Conference_Session__r.Name,
                               Conference_Content_Category__c,
                               Conference_Content_Category__r.Name
                          from Conference_Content_Category_Sessions__r),
                       (Select id,
                               name,
                               Conference_Session__c,
                               Conference_Session__r.Name,
                               CE_Category__c,
                               CE_Category__r.Name
                          from CE_Conference_Sessions__r),
                       (SELECT id,
                               Name,
                               ParentId
                          from Attachments)
                  from Conference_Session__c
                 where Conference__c = :conferenceId
                order by Session_Number_Numeric__c, Session_Number__c, Session_Number_Sort__c];
    }
    
    public static List<Conference_Session__c> getConferenceSessionsByConferenceIdAndSearchCriteria(Id conferenceId, ConferenceSessionSearchCriteria searchCriteria){
        String query = 
               'select AV_Requirements__c, ' +
                       'Abstract__c, ' +
                       'Activity_Workers_Certification__c, ' +
                       'Actual_Attendance__c, ' +
                       'Aisle_Microphone__c, ' +
                       'CA_RCFE__c, ' +
                       'CFRE_Certification__c, ' +
                       'Company_Name__c, ' +
                       'Conference_Track__c, ' +
                       'Conference__c, ' +
                       'Constituencies__c, ' +
                       'Developer__c, ' +
                       'Division__c, ' +
                       'Domain__c, ' +
                       'Education_Staff__c, ' +
                       'EventTime__c, ' +
                       'Expected_Attendance__c, ' +
                       'External_Id__c, ' +
                       'Flipchart_and_Markers__c, ' +
                       'Florida__c, ' +
                       'Id, ' +
                       'International__c, ' +
                       'Kansas__c, ' +
                       'Learning_Objective_1__c, ' +
                       'Learning_Objective_2__c, ' +
                       'Learning_Objective_3__c, ' +
                       'Locked__c, ' +
                       'Missouri__c, ' +
                       'NAB__c, ' +
                       'Name, ' +
                       'NASBA__c, ' +
                       'No_Additional_AV__c, ' +
                       'Notes__c, ' +
                       'Originator__c, ' +
                       'Other_Setup__c, ' +
                       'Other_Setup_Checkbox__c, ' +
                       'Proposal__c, ' +
                       'Proposal__r.Proposal_Number__c, ' +
                       'PTF_Code__c, ' +
                       'PTF_Hours__c, ' +
                       'Recommended_Number_Of_Readers__c, ' +
                       'Reporting_Requirements__c, ' +
                       'Room__c, ' +
                       'Scan_In_Out_Paramters__c, ' +
                       'Session_AV_Modified__c, ' +
                       'Session_AV_Validated__c, ' +
                       'Session_Uploads_Modified__c, ' +
                       'Session_Uploads_Validated__c, ' +
                       'Session_Length__c, ' +
                       'Session_Length_History__c, ' +
                       'Session_Number_Sort__c, ' +
                       'Session_Number__c, ' +
                       'Session_Title_Full__c, ' +
                       'Sponsorship_Level__c, ' +
                       'Status__c, ' +
                       'Submitter_Full_Name__c, ' +
                       'Submitter__r.FirstName, ' +
                       'Submitter__r.LastName, ' +
                       'Submitter__r.Speaker_Company_1__c, ' +
                       'Submitter__r.Speaker_Company_Name_Rollup__c, ' + 
                       'Submitter__r.PersonTitle, ' +
                       'Submitter__r.LeadingAge_ID__c, ' +
                       'Submitter__c, ' +
                       'Timeslot__c, ' +
                       'Timeslot__r.Timeslot__c, ' +
                       'Timeslot__r.Timeslot_Code__c, ' +
                       'Title__c, ' +
                       'Wheelchair_Access_and_Ramp__c, ' +
                       'Wireless_Microphone__c, ' +
                       '(Select id,  ' +
                               'name, ' +
                               'Proposal_Number__c ' +
                          'from Conference_Proposals__r), ' +
                       '(Select id, ' +
                               'name, ' +
                               'Speaker__c, ' +
                               'Speaker__r.NU__FullName__c, ' +
                               'Speaker__r.PersonTitle, ' +
                               'Speaker__r.Speaker_Company_1__c, ' +
                               'Speaker__r.Speaker_Company_Name_Rollup__c, ' +
                               'Speaker__r.BillingCity, ' +
                               'Speaker__r.BillingState, ' +
                               'Speaker__r.BillingCountry, ' +
                               'Speaker__r.Leadingage_Id__c, ' +
                               'Order__c, ' +
                               'Role__c, ' +
                               'Overall_Average_Score__c, ' +
                               'Topic_Score__c, ' +
                               'Audience_Score__c, ' +
                               'Presentation_Score__c, ' +
                               'Info_Score__c ' +
                          'from Conference_Session_Speakers__r ' +
                          'order by Speaker__r.LastName, Speaker__r.FirstName), ' +
                       '(Select id, ' +
                               'name, ' +
                               'Conference_Session__c, ' +
                               'Conference_Session__r.Name, ' +
                               'Conference_Content_Category__c, ' +
                               'Conference_Content_Category__r.Name ' +
                          'from Conference_Content_Category_Sessions__r), ' +
                       '(Select id, ' +
                               'name, ' +
                               'Conference_Session__c, ' +
                               'Conference_Session__r.Name, ' +
                               'CE_Category__c, ' +
                               'CE_Category__r.Name ' +
                          'from CE_Conference_Sessions__r), ' +
                       '(SELECT id, ' +
                               'Name, ' +
                               'ParentId ' +
                          'from Attachments) ' +
                  'from Conference_Session__c ' +
                 'where Conference__c = :conferenceId ';

        if (String.isNotBlank(SearchCriteria.SessionNumber)){
            String sessionNumber = SearchCriteria.SessionNumber;
            query += ' and Session_Number__c = :sessionNumber ';
        }
        
        if (String.isNotBlank(SearchCriteria.ProposalNumber)){
            Decimal proposalNumber = Decimal.valueOf(SearchCriteria.ProposalNumber);
            query += ' and Proposal__r.Proposal_Number__c = :proposalNumber ';
        }
        
        if (String.isNotBlank(SearchCriteria.Company)){
            String company = SearchCriteria.Company;
            //query += ' and Submitter__r.Speaker_Company_1__c = :company ';
            query += ' and Submitter__r.Speaker_Company_Name_Rollup__c LIKE \'%' + String.escapeSingleQuotes(company) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.FirstName)){
            String firstName = SearchCriteria.FirstName;
            //query += ' and ( Submitter__r.FirstName LIKE \'' + String.escapeSingleQuotes(firstName) + '%\' ';
            //query += ' OR Id IN (SELECT Session__c FROM Conference_Session_Speaker__c WHERE Speaker__r.FirstName LIKE \'' + String.escapeSingleQuotes(firstName) + '%\') ) ';
            query += ' and Id IN (SELECT Session__c FROM Conference_Session_Speaker__c WHERE Speaker__r.FirstName LIKE \'' + String.escapeSingleQuotes(firstName) + '%\') ';
        }
        
        if (String.isNotBlank(SearchCriteria.LastName)){
            String lastName = SearchCriteria.LastName;
            //query += ' and ( Submitter__r.LastName LIKE \'%' + String.escapeSingleQuotes(lastName) + '%\' ';
            //query += ' OR Id IN (SELECT Session__c FROM Conference_Session_Speaker__c WHERE Speaker__r.LastName LIKE \'%' + String.escapeSingleQuotes(lastName) + '%\') ) ';
            query += ' AND Id IN (SELECT Session__c FROM Conference_Session_Speaker__c WHERE Speaker__r.LastName LIKE \'%' + String.escapeSingleQuotes(lastName) + '%\') ';
        }
        
        if (String.isNotBlank(SearchCriteria.Title)){
            String title = SearchCriteria.Title;
            query += ' and Title__c like \'%' + String.escapeSingleQuotes(title) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.TrackName)){
            String trackName = SearchCriteria.TrackName;
            query += ' and Conference_Track__r.Name LIKE \'%' + String.escapeSingleQuotes(trackName) + '%\' ';
        }
        
        if (String.isNotBlank(SearchCriteria.EducationStaffFirstName)){
            String staffFirstName = SearchCriteria.EducationStaffFirstName;
            query += ' and Education_Staff__r.FirstName = :staffFirstName ';
        }
        
        if (String.isNotBlank(SearchCriteria.EducationStaffLastName)){
            String staffLastName = SearchCriteria.EducationStaffLastName;
            query += ' and Education_Staff__r.LastName = :staffLastName ';
        }
        
        if (String.isNotBlank(SearchCriteria.LeadingAgeID)) {
            String laid = SearchCriteria.LeadingAgeID;
            query += ' and Id IN (SELECT Session__c FROM Conference_Session_Speaker__c WHERE Speaker__r.Leadingage_Id__c = :laid) ';
        }

        if (String.isNotBlank(SearchCriteria.TimeslotCode)) {
            String tsc = SearchCriteria.TimeslotCode;
            query += ' and Timeslot__r.Timeslot_Code__c = :tsc ';
        }
        
        query += ' order by Session_Number_Sort__c, Session_Number__c';
        
        List<Conference_Session__c> sessions = Database.query(query);
        
        if (String.isNotBlank(SearchCriteria.NotesPhrase)){
            Integer sessionsCount = sessions.size();
            
            for (Integer index = sessionsCount - 1; index >= 0; --index){
                Conference_Session__c session = sessions[index];
                
                if (String.isBlank(session.Notes__c) ||
                    session.Notes__c.containsIgnoreCase(SearchCriteria.NotesPhrase) == false){
                    sessions.remove(index);
                }
            }
        }
        
        if (String.isNotBlank(SearchCriteria.LearningObjectivesPhrase)){
            Integer sessionsCount = sessions.size();
            
            for (Integer index = sessionsCount - 1; index >= 0; --index){
                Conference_Session__c session = sessions[index];
                
                if ((String.isBlank(session.Learning_Objective_1__c) ||
                    session.Learning_Objective_1__c.containsIgnoreCase(SearchCriteria.LearningObjectivesPhrase) == false) &&
                    
                    (String.isBlank(session.Learning_Objective_2__c) ||
                    session.Learning_Objective_2__c.containsIgnoreCase(SearchCriteria.LearningObjectivesPhrase) == false) &&
                    
                    (String.isBlank(session.Learning_Objective_3__c) ||
                    session.Learning_Objective_3__c.containsIgnoreCase(SearchCriteria.LearningObjectivesPhrase) == false)){
                        
                    sessions.remove(index);
                }
            }
        }

        return sessions;
    }
}