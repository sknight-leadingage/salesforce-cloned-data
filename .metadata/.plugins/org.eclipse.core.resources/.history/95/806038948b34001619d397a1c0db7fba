<apex:page controller="ManageAccount_Controller">
<apex:stylesheet value="{!URLFOR($Resource.LeadingAgeStyle, 'LeadingAge.css')}"/>
<apex:pageMessages id="msg" />
 <apex:form >
 <br />
     <apex:pageBlock mode="maindetail" title="Manage Account">
     <br />
     
     <!-- Individual account management for status activation and Affiliation -->
     
     <apex:outputPanel rendered="{!IF(WizardStep=1,true,false)}">
     <table border="0" cellspacing="5px">
         <tr>
             <td width="10%">Acccount Name: </td>
             <td><apex:outputLink value="/{!account.Id}"><apex:outputText value="{!account.Name}"/></apex:outputLink></td>
         </tr>
         <tr>
             <td>Record Type: </td>
             <td><apex:outputText value="{!account.NU__RecordTypeName__c}" /></td>
         </tr>
         <tr>
             <td>Account Status: </td>
             <td><apex:outputText value="{!account.NU__Status__c}" /></td>
         </tr>
         <tr>
             <td colspan="2">
				<p>You are about to change the status of this account.  What status would you like to change it to?</p>
        		<apex:selectRadio value="{!accStatus}" layout="pageDirection">
					<apex:selectOptions value="{!accStatuses}"/>
				</apex:selectRadio>
             </td>
         </tr>
     </table>
     </apex:outputPanel>  
     
     <apex:outputPanel rendered="{!IF(WizardStep=2,true,false)}">
     You are changing the status of this account to: <apex:outputText style="font-weight:700; color:red" value="{!accStatus}" /><br />
     <apex:outputPanel rendered="{!IF(account.NU__Status__c =='Inactive' && LEFT(accStatus, 6) = 'Active',true,false)}">
     	<br />You are Activating a previously Inactive account.  Here is a list of the account's previous affiliations.  If you know that one of these should be the account's primary affiliation, please select it so it can also be activated.  If you're not sure, please leave blank or select the 'Do not change' option.<br /> 
		<apex:selectRadio value="{!affEnable}" layout="pageDirection">
			<apex:selectOption itemValue="none" itemLabel="(Do not change Affiliation, I will update manually)"/>
			<apex:selectOptions value="{!affEnableList}"/>
		</apex:selectRadio>
     </apex:outputPanel>
     To confirm the change, click the <strong>Next</strong> button below.  To pick a different status, use the back button.  To cancel this action and return to the Account record, <apex:outputLink value="/{!account.Id}">click here</apex:outputLink>.
     </apex:outputPanel>
     
     <apex:outputPanel rendered="{!IF(WizardStep=3,true,false)}">           
            <apex:outputPanel >
            <apex:pageMessage summary="Success" severity="confirm" strength="3" />
            <apex:pageMessages />
                <p>Congratulations! You have successfully updated the account status for &nbsp;<apex:outputLink value="/{!account.Id}"><apex:outputText value="{!account.Name}"/></apex:outputLink>.</p>           
            </apex:outputPanel>
            <apex:outputPanel rendered="{!IF(ISBLANK(affEnable) || affEnable == 'none',true,false)}">
                 <br /><apex:outputText style="font-weight:700; color:red" value="NOTE: You will need to change affiliations status for this individual." />
             </apex:outputPanel>
        </apex:outputPanel>
     
      <!-- Joint Billing Panels -->
      
         <apex:outputPanel rendered="{!IF(WizardStep=10,true,false)}" >

					<apex:outputPanel rendered="{!IF(account.NU__Status__c == 'Inactive',true,false)}" >
					<apex:pageMessage summary="The Account is Inactive, any changes you make to this account will not apply unless you re-active the account." severity="warning" strength="3" />
                    <apex:pageMessages />
					</apex:outputPanel>
					
		     <table border="0" cellspacing="5px">
		         <tr>
		             <td width="10%">Acccount Name: </td>
		             <td><apex:outputText value="{!account.name}" /></td>
		         </tr>
		         <tr>
		             <td>Record Type: </td>
		             <td><apex:outputText value="{!account.NU__RecordTypeName__c}" /></td>
		         </tr>
		         <tr>
		             <td>Account Status: </td>
		             <td><apex:outputText value="{!account.NU__Status__c}" /></td>
		         </tr>
		          <tr>
		             <td>Joint Billing type: </td>
		             <td><apex:outputText value="{!account.Billing_Type__c}" /></td>
		         </tr>
		         
		         <tr>
		             <td colspan="2">
		             
				       <apex:pageBlock mode="edit">
				         <apex:pageBlockSection title="Select the Joint Billing Type:">
				                      <apex:OutputPanel >
				                                 <apex:selectList value="{!selectedBillingType}" size="1" multiselect="false"  >
				                                          <apex:selectOptions value="{!ListOfBillingType}" />
				                                 </apex:selectList>
				                        </apex:OutputPanel>
				          </apex:pageBlockSection>
				      </apex:pageBlock>
		             
		
		             </td>
		         </tr>
		     </table>
     </apex:outputPanel> 
     <apex:outputPanel rendered="{!IF(WizardStep=11,true,false)}" >
             <apex:outputPanel rendered="{!IF(account.NU__Status__c == 'Inactive',true,false)}" >
					<apex:pageMessage summary="The Account is Inactive, any changes you make to this account will not apply unless you re-active the account." severity="warning" strength="3" />
                    <apex:pageMessages />
			 </apex:outputPanel>
		     <table border="0" cellspacing="5px">
		         <tr>
		             <td width="10%">Acccount Name: </td>
		             <td><apex:outputText value="{!account.name}" /></td>
		         </tr>
		         <tr>
		             <td>Record Type: </td>
		             <td><apex:outputText value="{!account.NU__RecordTypeName__c}" /></td>
		         </tr>
		         <tr>
		             <td>Account Status: </td>
		             <td><apex:outputText value="{!account.NU__Status__c}" /></td>
		         </tr>
		         <tr>
		             <td>Joint Billing type: </td>
		             <td><apex:outputText value="{!account.Billing_Type__c}" /></td>
		         </tr>
		         <tr>
		             <td colspan="2">
		             <p>What Action would you like to take with this account's Joint Billing?</p>
        <apex:selectRadio value="{!actionTaken}">
            <apex:selectOptions value="{!items}"/>
            </apex:selectRadio><p/>

		
		             </td>
		         </tr>
		     </table>
     </apex:outputPanel>
     <apex:outputPanel rendered="{!IF(WizardStep=12,true,false)}" >
             <apex:outputPanel rendered="{!IF(account.NU__Status__c == 'Inactive',true,false)}" >
					<apex:pageMessage summary="The Account is Inactive, any changes you make to this account will not apply unless you re-active the account." severity="warning" strength="3" />
                    <apex:pageMessages />
			 </apex:outputPanel>
		     <table border="0" cellspacing="5px">
		         <tr>
		             <td width="10%">Acccount Name: </td>
		             <td><apex:outputText value="{!account.name}" /></td>
		         </tr>
		         <tr>
		             <td>Record Type: </td>
		             <td><apex:outputText value="{!account.NU__RecordTypeName__c}" /></td>
		         </tr>
		         <tr>
		             <td>Account Status: </td>
		             <td><apex:outputText value="{!account.NU__Status__c}" /></td>
		         </tr>
		          <tr>
		             <td>Joint Billing type: </td>
		             <td><apex:outputText value="{!account.Billing_Type__c}" /></td>
		         </tr>		         
		         <tr>
		             <td colspan="2">
                 <apex:pageBlock title="Joint Billing Adjustment" mode="edit">
                 				             
 				     <apex:pageBlockSection title="Program Service Revenue" columns="2" rendered="{!PSRNeed}" >
		
		                <apex:inputField label="Program Service Revenue" value="{!account.Program_Service_Revenue__c}"/>
		                <apex:inputField label="Program Service Revenue Year" value="{!account.Revenue_Year_Submitted__c}"/>
		                
		            </apex:pageBlockSection>
		            
		            <apex:pageBlockSection title="Joint Billing Amount Adjustment" columns="2" rendered="{!Adjustment}" >
		            <apex:inputField label="Amount Adjustment" value="{!account.Dues_Price_Override__c}"/>
		                <apex:inputField label="Amount Adjustment Reason" value="{!account.Dues_Price_Override_Reason__c}"/>
		            </apex:pageBlockSection>
		        </apex:pageBlock>
		        
		             </td>
		         </tr>
		     </table>
     </apex:outputPanel>
         <apex:outputPanel rendered="{!IF(WizardStep=13,true,false)}">
             <apex:outputPanel rendered="{!IF(account.NU__Status__c == 'Inactive',true,false)}" >
					<apex:pageMessage summary="The Account is Inactive, any changes you make to this account will not apply unless you re-active the account." severity="warning" strength="3" />
                    <apex:pageMessages />
			 </apex:outputPanel>
        <apex:pageBlock title="Cancel Joint Billing" mode="edit">
                 				             
 				     <apex:pageBlockSection title="Why Are you Cancelling The Joint Billing?" columns="2" >
		
		                                <apex:OutputPanel >
				                                 <apex:selectList value="{!selectedCancellation}" size="1" multiselect="false"  >
				                                          <apex:selectOptions value="{!ListOfCancellationType}" />
				                                 </apex:selectList>
				                        </apex:OutputPanel>

		            </apex:pageBlockSection>
		        </apex:pageBlock>
     </apex:outputPanel>
     <apex:outputPanel rendered="{!IF(WizardStep=15 && actionTaken == 'Change Billing Type',true,false)}">
        <!-- Success Message Section -->
                    <apex:outputPanel >
                    <apex:pageMessage summary="Success" severity="confirm" strength="3" />
                    <apex:pageMessages />
                <p>Congratulation. You have successfully Finished the Joint Billing Process and the Joint Billing type and Adjustment have been made for &nbsp;<apex:outputLink value="/{!account.Id}"><apex:outputText value="{!account.Name}"/></apex:outputLink>.</p>        
                
            </apex:outputPanel>
            <apex:outputPanel rendered="{!MessageNeeded}" >
            <apex:outputText value="{!Message}" />
            </apex:outputPanel>
     </apex:outputPanel>
     <apex:outputPanel rendered="{!IF(WizardStep=15 && actionTaken == 'none',true,false)}">
        <!-- Success Message Section -->
                    <apex:outputPanel >
                    <apex:pageMessage summary="Success" severity="confirm" strength="3" />
                    <apex:pageMessages />
                <p>Congratulation. You have successfully Finished the Joint Billing Process and it will be reflected tomorrow for &nbsp;<apex:outputLink value="/{!account.Id}"><apex:outputText value="{!account.Name}"/></apex:outputLink>.</p>        
                
            </apex:outputPanel>
            <apex:outputPanel rendered="{!MessageNeeded}" >
            <apex:outputText value="{!Message}" />
            </apex:outputPanel>
     </apex:outputPanel>
     <apex:outputPanel rendered="{!IF(WizardStep=15 && (actionTaken == 'Override Amount' || actionTaken == 'Cancel Membership' ),true,false)}">
        <!-- Success Message Section -->
                    <apex:outputPanel >
                    <apex:pageMessage summary="Success" severity="confirm" strength="3" />
                    <apex:pageMessages />
                <p>Congratulation. You have successfully Finished the Joint Billing Process and it will be reflected tomorrow for &nbsp;<apex:outputLink value="/{!account.Id}"><apex:outputText value="{!account.Name}"/></apex:outputLink>.</p>        
                
            </apex:outputPanel>
            <apex:outputPanel rendered="{!MessageNeeded}" >
            <apex:outputText value="{!Message}" />
            </apex:outputPanel>
     </apex:outputPanel>           
     </apex:pageBlock>
     
     <apex:commandButton action="{!WizardBack}" value="Back" rendered="{!IF(WizardStep > 1 && WizardStep != WizardMax && WizardLastStep != null && Done != true,true,false)}" immediate="true" />    
    <apex:commandButton action="{!WizardNext}" value="Next" rendered="{!IF(Done != true,true,false)}"/>

 </apex:form>
</apex:page>