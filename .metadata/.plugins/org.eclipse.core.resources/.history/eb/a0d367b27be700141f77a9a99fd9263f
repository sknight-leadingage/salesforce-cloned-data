<apex:page renderas="{!RptRenderAs}" controller="EduConfAdmin_Controller">
<apex:pageMessages id="msg" />
 <apex:form >
    <apex:pageBlock title="{! Conference.Name }: {!PageTitle}" mode="maindetail">
       <apex:commandButton action="{! viewConferenceReports }" value="Reports Main Menu" rendered="{!IF(RptRenderAs == '', true, false)}" /> &nbsp; <apex:commandButton value="Print View" action="{!PrintOutput}" rendered="{!IF(RptRenderAs == '' && WizardStep > 1, true, false)}" />
 &nbsp; 
    <apex:commandButton action="{!WizardBack}" value="Back" rendered="{!IF(WizardStep > 1,true,false)}" immediate="true" />

    
    
        <apex:outputPanel rendered="{!IF(WizardStep=1,true,false)}">
            <apex:PageBlockSection columns="1">
               <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Report Type" />
                    <apex:outputPanel >
                        <div class="requiredInput">
                            <div class="requiredBlock"></div>
                            <apex:selectRadio value="{!RTypeSel}" label="Please select a Report Type from the list">
                                <apex:selectOptions value="{!RTypeItems}"></apex:selectOptions>
                            </apex:selectRadio>
                        </div>
                        <div style="font-size: 12pt; padding: 12px;">
                            <apex:commandButton action="{!WizardNext}" value="Next" rendered="{!IF(WizardStep < 2,true,false)}"/>
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:PageBlockSection>
            
            <br />
            
        <div style="font-size: 11pt;" width="100%">
            <strong>Total Submissions: {!TotalProposals}<br />Sort by: {!MainSort}</strong>
        </div>
        
            <br />
            
            <table border="1" cellspacing="0" cellpadding="2" width="100%">
                <tr>
                    <th bgcolor="#BFBFBF" width="5%">ID</th>
                    <th bgcolor="#BFBFBF" width="10%"><a href="{!SubmitSortLink}" style="color: #1096C6;">Submit Date</a></th>
                    <th bgcolor="#BFBFBF" width="15%"><a href="{!NameSortLink}" style="color: #1096C6;">Name</a></th>
                    <th bgcolor="#BFBFBF" width="15%">Organization</th>
                    <th bgcolor="#BFBFBF" width="10%">Email</th>
                    <th bgcolor="#BFBFBF" width="30%">Proposal Title</th>
                    <th bgcolor="#BFBFBF" width="15%">Education Tracks</th>
                </tr>
                <apex:repeat value="{!Proposals}" var="a">
                <tr>
                    <td bgcolor="#DBDBDB" width="5%">{!a.Proposal_Number__c}</td>
                    <td bgcolor="#DBDBDB" width="10%">{!a.CreatedDate}</td>
                    <td bgcolor="#DBDBDB" width="15%">{!a.Submitter__r.Full_Name__c}</td>
                    <td bgcolor="#DBDBDB" width="15%">{!a.Submitter__r.Company_Name_Rollup__c}</td>
                    <td bgcolor="#DBDBDB" width="10%"><a href="mailto:{!a.Submitter__r.Email__c}">{!a.Submitter__r.Email__c}</a></td>
                    <td bgcolor="#DBDBDB" width="30%">{!a.Title__c}</td>
                    <td bgcolor="#DBDBDB" width="15%">{!a.Conference_Track__r.Name}</td>
                </tr>
                </apex:repeat>
            </table>
        </apex:outputPanel>
    </apex:pageBlock>
    
    <apex:outputPanel rendered="{!IF(WizardStep=2,true,false)}">
    <table border="1" cellspacing="0" cellpadding="2">
        <tr>
            <th bgcolor="#BFBFBF">Track Name</th>
            <th bgcolor="#BFBFBF">Total Submission</th>
        </tr>
        <apex:repeat value="{!TrackTotalsMap}" var="a">
        <tr>
            <td bgcolor="#DBDBDB">{!a}</td>
            <td bgcolor="#DBDBDB">{!TrackTotalsMap[a]}</td>
        </tr>
        </apex:repeat>
    </table>
    </apex:outputPanel>

    <apex:outputPanel rendered="{!IF(WizardStep=3,true,false)}">
    <apex:repeat value="{!Proposals}" var="a">
        <div style="page-break-after: always;">
        <table border="1" cellspacing="0" cellpadding="2" width="100%">
            <tr>
                <td width="100%">
                    <table border="0" cellspacing="4" cellpadding="4" width="100%">
                        <tr>
                            <td colspan="2" align="right" width="100%">ID # {!a.Proposal_Number__c}</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" width="100%">
                                <span style="font-weight: bold; font-size: 12pt;">{!a.Title__c}</span>
                                <p><apex:outputText escape="false" value="{!SUBSTITUTE(a.Abstract__c, LineBreak, '<br />')}"></apex:outputText></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Session Learning Objectives:</strong></td>
                            <td valign="top" align="left" width="70%">
                                Objective 1:<br />{!a.Learning_Objective_1__c}<br /><br />
                                Objective 2:<br />{!a.Learning_Objective_2__c}<br /><br />
                                Objective 3:<br />{!a.Learning_Objective_3__c}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Session Length:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.Session_Length__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Presentation Delivery:</strong></td>
                            <td valign="top" align="left" width="70%">
                                <apex:outputText escape="false" value="{!SUBSTITUTE(a.Presentation_Structure__c, LineBreak, '<br />')}"></apex:outputText>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Education Level:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.Education_Level__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Education Track:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.Conference_Track__r.Name}</td>
                        </tr>
                        <apex:outputPanel rendered="{!IF(LEFT(a.Conference__r.NU__ShortName__c, 2) = 'PA',false,true)}">
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>International Focus:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.International__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Technology Use:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.Technology_Use__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Aging Services Continuum:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.Aging_Services_Continuum__r.Name}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Quality First Explanation:</strong></td>
                            <td valign="top" align="left" width="70%">{!a.QF_Explanation__c}</td>
                        </tr>
                        </apex:outputPanel>
            <apex:variable value="{!0}" var="rowNum"/>
            <apex:repeat value="{!a.Conference_Proposal_Speakers__r}" var="b">
                        <apex:outputText escape="false" value="" rendered="{!IF(rowNum > 0,true,false)}">
                        <tr>
                            <td colspan="2" align="left" width="100%"><span style="font-weight: bold; font-size: 12pt;">Additional Speaker #{!rowNum}</span></td>
                        </tr>
                        </apex:outputText>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Name:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Full_Name__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Job Title:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Title__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Organization:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Company_Name_Rollup__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Address:</strong></td>
                            <td valign="top" align="left" width="70%">
                                {!b.Speaker__r.Address_1__c}<br />
                                {!b.Speaker__r.City__c}, {!b.Speaker__r.State__c}, {!b.Speaker__r.Postal_Code__c}<br />
                                {!b.Speaker__r.Country__c}
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Phone:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Phone__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Email:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Email__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Education:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Highest_Degree_Level__c}, {!b.Speaker__r.Academic_Institution__c}, {!b.Speaker__r.Academic_Institution_Year__c}<br />{!b.Speaker__r.Major_Discipline__c}</td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="30%"><strong>Knowledge/Professional Experience:</strong></td>
                            <td valign="top" align="left" width="70%">{!b.Speaker__r.Knowledge_and_Professional_Experience__c}</td>
                        </tr>
                <apex:variable var="rowNum" value="{!rowNum + 1}"/>
            </apex:repeat>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        </div>
    </apex:repeat>
    </apex:outputPanel>
    
    <apex:outputPanel rendered="{!IF(WizardStep=4,true,false)}">
    <table border="1" cellspacing="0" cellpadding="2">
        <tr>
            <th bgcolor="#BFBFBF">ID</th>
            <th bgcolor="#BFBFBF">Session Title</th>
            <th bgcolor="#BFBFBF">Session Narrative</th>
            <th bgcolor="#BFBFBF">Objective 1</th>
            <th bgcolor="#BFBFBF">Objective 2</th>
            <th bgcolor="#BFBFBF">Objective 3</th>
            <th bgcolor="#BFBFBF">Session Length</th>
            <th bgcolor="#BFBFBF">Presenttion Delivery</th>
            <th bgcolor="#BFBFBF">Education Level</th>
            <th bgcolor="#BFBFBF">Track Name</th>
            <apex:outputPanel rendered="{!IF(LEFT(Conference.NU__ShortName__c, 2) = 'PA',false,true)}">
            <th bgcolor="#BFBFBF">International Focus</th>
            <th bgcolor="#BFBFBF">Technology Use</th>
            <th bgcolor="#BFBFBF">Aging Services Continuum</th>
            <th bgcolor="#BFBFBF">Quality First Alignment</th>
            </apex:outputPanel>
            <th bgcolor="#BFBFBF">First Name</th>
            <th bgcolor="#BFBFBF">Last Name</th>
            <th bgcolor="#BFBFBF">Full Name</th>
            <th bgcolor="#BFBFBF">Job Title</th>
            <th bgcolor="#BFBFBF">Organization</th>
            <th bgcolor="#BFBFBF">Address</th>
            <th bgcolor="#BFBFBF">City</th>
            <th bgcolor="#BFBFBF">State</th>
            <th bgcolor="#BFBFBF">Country</th>
            <th bgcolor="#BFBFBF">Zip</th>
            <th bgcolor="#BFBFBF">City, State, Zip</th>
            <th bgcolor="#BFBFBF">Phone</th>
            <th bgcolor="#BFBFBF">Email</th>
            <th bgcolor="#BFBFBF">Highest Degree</th>
            <th bgcolor="#BFBFBF">Academic Institution</th>
            <th bgcolor="#BFBFBF">Major Discipline</th>
            <th bgcolor="#BFBFBF">Knowledge/Professional Experience</th>
            <th bgcolor="#BFBFBF">First Name # 1</th>
            <th bgcolor="#BFBFBF">Last Name # 1</th>
            <th bgcolor="#BFBFBF">Full Name # 1</th>
            <th bgcolor="#BFBFBF">Job Title # 1</th>
            <th bgcolor="#BFBFBF">Organization # 1</th>
            <th bgcolor="#BFBFBF">Address # 1</th>
            <th bgcolor="#BFBFBF">City # 1</th>
            <th bgcolor="#BFBFBF">State # 1</th>
            <th bgcolor="#BFBFBF">Country # 1</th>
            <th bgcolor="#BFBFBF">Zip # 1</th>
            <th bgcolor="#BFBFBF">City, State, Zip # 1</th>
            <th bgcolor="#BFBFBF">Phone # 1</th>
            <th bgcolor="#BFBFBF">Email # 1</th>
            <th bgcolor="#BFBFBF">Highest Degree # 1</th>
            <th bgcolor="#BFBFBF">Academic Institution # 1</th>
            <th bgcolor="#BFBFBF">Major Discipline # 1</th>
            <th bgcolor="#BFBFBF">Knowledge/Professional Experience # 1</th>
            <th bgcolor="#BFBFBF">First Name # 2</th>
            <th bgcolor="#BFBFBF">Last Name # 2</th>
            <th bgcolor="#BFBFBF">Full Name # 2</th>
            <th bgcolor="#BFBFBF">Job Title # 2</th>
            <th bgcolor="#BFBFBF">Organization # 2</th>
            <th bgcolor="#BFBFBF">Address # 2</th>
            <th bgcolor="#BFBFBF">City # 2</th>
            <th bgcolor="#BFBFBF">State # 2</th>
            <th bgcolor="#BFBFBF">Country # 2</th>
            <th bgcolor="#BFBFBF">Zip # 2</th>
            <th bgcolor="#BFBFBF">City, State, Zip # 2</th>
            <th bgcolor="#BFBFBF">Phone # 2</th>
            <th bgcolor="#BFBFBF">Email # 2</th>
            <th bgcolor="#BFBFBF">Highest Degree # 2</th>
            <th bgcolor="#BFBFBF">Academic Institution # 2</th>
            <th bgcolor="#BFBFBF">Major Discipline # 2</th>
            <th bgcolor="#BFBFBF">Knowledge/Professional Experience # 2</th>
            <th bgcolor="#BFBFBF">First Name # 3</th>
            <th bgcolor="#BFBFBF">Last Name # 3</th>
            <th bgcolor="#BFBFBF">Full Name # 3</th>
            <th bgcolor="#BFBFBF">Job Title # 3</th>
            <th bgcolor="#BFBFBF">Organization # 3</th>
            <th bgcolor="#BFBFBF">Address # 3</th>
            <th bgcolor="#BFBFBF">City # 3</th>
            <th bgcolor="#BFBFBF">State # 3</th>
            <th bgcolor="#BFBFBF">Country # 3</th>
            <th bgcolor="#BFBFBF">Zip # 3</th>
            <th bgcolor="#BFBFBF">City, State, Zip # 3</th>
            <th bgcolor="#BFBFBF">Phone # 3</th>
            <th bgcolor="#BFBFBF">Email # 3</th>
            <th bgcolor="#BFBFBF">Highest Degree # 3</th>
            <th bgcolor="#BFBFBF">Academic Institution # 3</th>
            <th bgcolor="#BFBFBF">Major Discipline # 3</th>
            <th bgcolor="#BFBFBF">Knowledge/Professional Experience # 3</th>
            <th bgcolor="#BFBFBF">Date &amp; Time</th>
        </tr>
        <apex:repeat value="{!Proposals}" var="a">
        <tr>
            <td bgcolor="#DBDBDB">{!a.Proposal_Number__c}</td>
            <td bgcolor="#DBDBDB">{!a.Title__c}</td>
            <td bgcolor="#DBDBDB">{!a.Abstract__c}</td>
            <td bgcolor="#DBDBDB">{!a.Learning_Objective_1__c}</td>
            <td bgcolor="#DBDBDB">{!a.Learning_Objective_2__c}</td>
            <td bgcolor="#DBDBDB">{!a.Learning_Objective_3__c}</td>
            <td bgcolor="#DBDBDB">{!a.Session_Length__c}</td>
            <td bgcolor="#DBDBDB">{!a.Presentation_Structure__c}</td>
            <td bgcolor="#DBDBDB">{!a.Education_Level__c}</td>
            <td bgcolor="#DBDBDB">{!a.Conference_Track__r.Name}</td>
            <apex:outputPanel rendered="{!IF(LEFT(a.Conference__r.NU__ShortName__c, 2) = 'PA',false,true)}">
            <td bgcolor="#DBDBDB">{!a.International__c}</td>
            <td bgcolor="#DBDBDB">{!a.Technology_Use__c}</td>
            <td bgcolor="#DBDBDB">{!a.Aging_Services_Continuum__r.Name}</td>
            <td bgcolor="#DBDBDB">{!a.QF_Explanation__c}</td>
            </apex:outputPanel>
            <apex:variable value="{!0}" var="rowNum"/>
            <apex:repeat value="{!a.Conference_Proposal_Speakers__r}" var="b">
                <td bgcolor="#DBDBDB">{!b.Speaker__r.First_Name__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Last_Name__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Full_Name__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Title__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Company_Name_Rollup__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Address_1__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.City__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.State__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Country__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Postal_Code__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.State__c}, {!b.Speaker__r.Country__c}, {!b.Speaker__r.Postal_Code__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Phone__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Email__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Highest_Degree_Level__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Academic_Institution__c}, {!b.Speaker__r.Academic_Institution_Year__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Major_Discipline__c}</td>
                <td bgcolor="#DBDBDB">{!b.Speaker__r.Knowledge_and_Professional_Experience__c}</td>
                <apex:variable var="rowNum" value="{!rowNum + 1}"/>
            </apex:repeat>
            <apex:repeat value="{!TDs}" var="c" first="{!rowNum}" rows="{!4-rowNum}">
                <apex:outputText escape="false" value="{!c}"></apex:outputText>
            </apex:repeat>
            <td bgcolor="#DBDBDB">{!a.CreatedDate}</td>
        </tr>
        </apex:repeat>
    </table>
    </apex:outputPanel>
    
    

    
    <apex:commandButton action="{!WizardBack}" value="Back" rendered="{!IF(WizardStep > 1 && WizardStep != WizardMax,true,false)}" immediate="true" />    
    <apex:commandButton action="{!WizardNext}" value="Next" rendered="{!IF(WizardStep < 2,true,false)}"/>
 </apex:form>
</apex:page>