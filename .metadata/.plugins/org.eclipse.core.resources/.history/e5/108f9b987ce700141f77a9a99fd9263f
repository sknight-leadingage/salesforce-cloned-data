<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>CardNumberErrorMessage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>The error message to display if the credit card number entered doesn&apos;t fall within the credit card issuer&apos;s allowed range as identified through the regular expression.</inlineHelpText>
        <label>Card Number Error Message</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CardNumberIdentificationRegEx__c</fullName>
        <deprecated>false</deprecated>
        <description>The regular expression used to identify this credit card issuer by credit card number.</description>
        <externalId>false</externalId>
        <inlineHelpText>The regular expression used to identify this credit card issuer by credit card number.</inlineHelpText>
        <label>Card Number Identification Reg Ex</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CardNumberRegularExpression__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Regular Expression used to validate a credit card number from this issuer. It is used when creating a credit card payment to ensure that the credit card number entered falls within a credit card issuer&apos;s allowable credit card number range.</inlineHelpText>
        <label>Card Number Regular Expression</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <deprecated>false</deprecated>
        <description>Optional reference to a record in another system.</description>
        <externalId>true</externalId>
        <inlineHelpText>Optional reference to a record in another system.</inlineHelpText>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>ImageStaticResourceName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Enter the URL of a SF Static Resource. This image displays on the credit card payment modal window.</inlineHelpText>
        <label>Image Static Resource URL</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Credit Card Issuer</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CardNumberRegularExpression__c</columns>
        <columns>CardNumberErrorMessage__c</columns>
        <columns>ImageStaticResourceName__c</columns>
        <columns>CardNumberIdentificationRegEx__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Credit Card Issuers</label>
    </listViews>
    <listViews>
        <fullName>All_Credit_Card_Issuers</fullName>
        <columns>NAME</columns>
        <columns>CardNumberRegularExpression__c</columns>
        <columns>CardNumberErrorMessage__c</columns>
        <columns>ImageStaticResourceName__c</columns>
        <columns>CardNumberIdentificationRegEx__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Credit Card Issuers</label>
    </listViews>
    <nameField>
        <label>Credit Card Issuer Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Credit Card Issuers</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
</CustomObject>
