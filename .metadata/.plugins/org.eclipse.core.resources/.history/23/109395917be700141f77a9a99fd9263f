/**
 * @author NimbleUser
 * @date Updated: 3/22/13
 * @description This class provides various functions for creating and inserting Conference Proposals
 * It's primarily used for test code, but could be used elsewhere. 
 * The create* functions instantiate Conference Proposals without inserting them. 
 * The insert* functions create Conference Proposals and insert them.
 */
public with sharing class DataFactoryConferenceSession {

    private static Integer counter = 0;

    public static Conference_Session__c createConferenceSession(){
        NU__Event__c conference = NU.DataFactoryEvent.insertEvent();
        Conference_Speaker__c speaker = DataFactoryConferenceSpeaker.insertConferenceSpeaker();
        
        Conference_Track__c track = DataFactoryConferenceTrack.insertConferenceTrack(conference.Id);
        
        return createConferenceSession(conference.Id, speaker.Account__c, track.Id);
    }
    
    public static Conference_Session__c createConferenceSession(Id conferenceId, Id submitterId, Id trackId){
        return new Conference_Session__c(
            Conference__c = conferenceId,
            Submitter__c = submitterId,
            Title__c = 'Test Proposal' + String.valueOf(++counter),
            Conference_Track__c = trackId
        );
    }
    
    public static Conference_Session__c insertConferenceSession(){
        Conference_Session__c confSessionToInsert = createConferenceSession();

        insert confSessionToInsert;
        
        Conference_Session_Speaker__c sessionSpeaker = new Conference_Session_Speaker__c(
        	Session__c = confSessionToInsert.Id,
        	Speaker__c = confSessionToInsert.Submitter__c
        );
        
        insert sessionSpeaker;
        
        return confSessionToInsert;
    }
    
    public static List<Conference_Session__c> insertConferenceSessions(Integer numberToInsert){
        List<Conference_Session__c> sessionsToInsert = new List<Conference_Session__c>();
        
        NU__Event__c conference = NU.DataFactoryEvent.insertEvent();
        Conference_Speaker__c speaker = DataFactoryConferenceSpeaker.insertConferenceSpeaker();
        Conference_Track__c track = DataFactoryConferenceTrack.insertConferenceTrack(conference.Id);
        
        for (Integer i = 0; i < numberToInsert; ++i){
            Conference_Session__c sessionToInsert = createConferenceSession(conference.Id, speaker.Account__c, track.Id);
            
            sessionsToInsert.add(sessionToInsert);
        }
        
        insert sessionsToInsert;
        return sessionsToInsert;
    }
}