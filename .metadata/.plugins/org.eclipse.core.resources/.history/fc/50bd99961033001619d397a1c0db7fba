/*
    When logged in as a State Partner Portal user, use the user --> Contact --> Account.State_Partner_Id
    to determine the corresponding "State Partner" account.
*/

public class CustomStateProviderRevenueController {
    public string debugString {get; set;}
    public string states {get; set; }
    public List <Account> ProviderAccounts { get; set;}
    private List <Account> ProviderAccounts_orig { get; set;}
    public List <Account> MSOAccounts { get; set;}
    private List <Account> MSOAccounts_orig { get; set;}
    public User currentUser {get; set;}
    private User p_pageUser = null;
    public boolean displayConfirmMSG {get; set;}
    public boolean displayErrorMSG {get; set;}
    public string CompanyError {get; set;}  
    public boolean MSOdisplayErrorMSG {get; set;}
    public string MSOCompanyError {get; set;}   
    public boolean showMSOList {get; set;}
                
    public User pageUser
    {
        get
        {
            if (p_pageUser == null)
            {
                p_pageUser = [SELECT ID, CompanyName, Contact.Account.ID FROM User WHERE ID = :UserInfo.getUserID()];
            }
            return p_pageUser;
        }
    }
    private Id StatePartnerAccountID_priv = null;
    public Id StatePartnerAccountID
    {
        get {                           
                        if (StatePartnerAccountID_priv == null){
                                //User CurrentUser = [SELECT Contact.Account.ID FROM User WHERE ID = :UserInfo.getUserID()];
                                //StatePartnerAccountID_priv = CurrentUser.Contact.Account.ID;
                                StatePartnerAccountID_priv = pageUser.Contact.Account.ID;
                        }
                        return StatePartnerAccountID_priv;
                }
                
    }
    
    public boolean isUserLeadingAgeStaff
    {
        get
        {
            return (pageUser.CompanyName != null && pageUser.CompanyName == 'LeadingAge');   //SK TODO 20130308: Need to find out how to do this by id... or if this is even correct
        }
    }
     
    
    private List<Account> getProviderAccounts(){
       if (states != null && states != '-1')
       {
        return [SELECT id,
                name,
                Program_Service_Revenue__c,
                Revenue_Year_Submitted__c, 
                BillingCity, 
                BillingState
         FROM Account
         WHERE RecordType.DeveloperName = 'Provider' AND LeadingAge_Member_Company__c = 'Member' AND State_Partner_ID__c = :states ORDER BY name ASC];
        }
        
         return new List<Account>(); 
                               
    }
    
    private List<Account> getMSOAccounts(){
       if (states != null && states != '-1')
       {
        List <Account> MSOList = [SELECT id,
                name,
                Multi_Site_Program_Service_Revenue__c,
                Revenue_Year_Submitted__c, 
                BillingCity, 
                BillingState
         FROM Account
         WHERE Multi_Site_Program_Service_Revenue__c != null AND RecordType.Name = :Constant.ACCOUNT_RECORD_TYPE_NAME_MULTI_SITE AND Multi_Site_Corporate__c = true AND State_Partner_ID__c = :states ORDER BY name ASC];
         
         if (MSOList.size() == 0)
             { showMSOList = false; }
         else
             { showMSOList = true;}
         
         return MSOList;
         
        }
        
         return new List<Account>(); 
                               
    }
    
    
    public void State_onChange(){
          ProviderAccounts = getProviderAccounts();   
          ProviderAccounts_orig = ProviderAccounts.deepClone();    
          MSOAccounts = getMSOAccounts();   
          MSOAccounts_orig = MSOAccounts.deepClone(); 
                  
    }
    
    public string StatePartnerId { get; set; }
    
        
    public CustomStateProviderRevenueController(){    
        Id userId = UserInfo.getUserId();
        currentUser = [Select id, name, CompanyName, Contact.Account.State_Partner_Id__c, Profile.Name from User where id = :userId];
        
        StatePartnerId = currentUser.Contact.Account.State_Partner_Id__c;
        if (isUserLeadingAgeStaff == false && (StatePartnerId == null || StatePartnerId == '')) {
            List<Account> lGetStpId = [SELECT Id FROM Account WHERE Name = :currentUser.CompanyName AND NU__RecordTypeName__c = :Constant.ACCOUNT_RECORD_TYPE_NAME_STATE_PARTNER];
            if (lGetStpId.size() > 0) {
                StatePartnerId = lGetStpId[0].Id;
            }
        }
        
        states = String.ValueOf(StatePartnerId);
        
        ProviderAccounts = getProviderAccounts();     
        ProviderAccounts_orig = ProviderAccounts.deepClone();
        MSOAccounts = getMSOAccounts();     
        MSOAccounts_orig = MSOAccounts.deepClone();
        
        displayConfirmMSG = false;
                   
    }

    public void Save()  
    {
        try
            {               
                
            List <Account> UpdatedAccounts = new List<Account>(); 
            //loop through ProviderAccounts and check versus original values, add to list if change.  
                
            string submittedYear;
            string originalYear;
            double submittedRevenue;
            double originalRevenue;
            CompanyError = '';
            displayErrorMSG = false;
            
                            
            for(Integer i=0;i<ProviderAccounts.size();i++)
            {
                
                submittedYear = ProviderAccounts[i].Revenue_Year_Submitted__c;
                originalYear = ProviderAccounts_orig[i].Revenue_Year_Submitted__c;
                submittedRevenue = ProviderAccounts[i].Program_Service_Revenue__c;
                originalRevenue = ProviderAccounts_orig[i].Program_Service_Revenue__c;           
                
                if (submittedYear!= null && submittedRevenue != null)
                {                                   
                    if (submittedYear != originalYear || submittedRevenue != originalRevenue)
                    {
                        if (isValid(submittedYear, originalYear, submittedRevenue, originalRevenue))  //only update if valid data
                        {
                            //changed values
                            UpdatedAccounts.add(ProviderAccounts[i]);
                            //debugString = debugString + 'UPDATED ACCOUNT: ' + ProviderAccounts[i].Name + '<br/>'; 
                        }
                        else
                        {
                                   displayErrorMSG = true;
                                   CompanyError +=  ProviderAccounts[i].Name + '<br/>';
                        }                   
                    }        
                    }                                   
            }   
            
            
            //MSO saving
            List <Account> UpdatedMSOAccounts = new List<Account>(); 
                 
            string submittedMSOYear;
            string originalMSOYear;
            double submittedMSORevenue;
            double originalMSORevenue;
            MSOCompanyError = '';
            MSOdisplayErrorMSG = false;
            
                            
            for(Integer i=0;i<MSOAccounts.size();i++)
            {
                
                submittedMSOYear = MSOAccounts[i].Revenue_Year_Submitted__c;
                originalMSOYear = MSOAccounts_orig[i].Revenue_Year_Submitted__c;
                submittedMSORevenue = MSOAccounts[i].Multi_Site_Program_Service_Revenue__c;
                originalMSORevenue = MSOAccounts_orig[i].Multi_Site_Program_Service_Revenue__c;           
                
                if (submittedMSOYear!= null && submittedMSORevenue != null)
                {                                   
                    if (submittedMSOYear != originalMSOYear || submittedMSORevenue != originalMSORevenue)
                    {
                        if (isValid(submittedMSOYear, originalMSOYear, submittedMSORevenue, originalMSORevenue))  //only update if valid data
                        {
                            //changed values
                            UpdatedMSOAccounts.add(MSOAccounts[i]);
                            //debugString = debugString + 'UPDATED MSO ACCOUNT: ' + MSOAccounts[i].Name + '<br/>'; 
                        }
                        else
                        {
                                   MSOdisplayErrorMSG = true;
                                   MSOCompanyError +=  MSOAccounts[i].Name + '<br/>';
                        }                   
                    }        
                    }                                   
            }   
            //End of MSO saving
              
            CompanyError += '<br/><br/>';  
            MSOCompanyError += '<br/><br/>';             
              
            //only update changed accounts
            NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('LeadingAgeAccountTrigger');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTriggers');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('CompanyManagerTrigger');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountStatusFlowdownTrigger');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger_Subscriptions');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('AccountTrigger_ProgramRevenue');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AccountTriggerHandlers');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('NU.AffiliationTriggerHandlers');
            NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggerHandlers');
            update UpdatedAccounts;
            update UpdatedMSOAccounts;
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('LeadingAgeAccountTrigger');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTriggers');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('CompanyManagerTrigger');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountStatusFlowdownTrigger');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger_Subscriptions');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('AccountTrigger_ProgramRevenue');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AccountTriggerHandlers');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('NU.AffiliationTriggerHandlers');
            NU.TriggerHandlerManager.reenableTriggerForThisRequest('AffiliationTriggerHandlers');
            displayConfirmMSG = true;

        }
        catch (DMLException e)
        {
            ApexPages.addMessages(e); 
        }
        finally
        {
        }
    }


    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Account> lData = [SELECT ID, Name, BillingState FROM Account WHERE RecordType.DeveloperName = 'State_Partner' ORDER BY Name];
        //List<Account> lData = [SELECT ID, BillingState FROM Account WHERE RecordType.DeveloperName = 'State_Partner_Portal' AND BillingState != NULL ORDER BY BillingState];

        for(Account a: lData)
        {
            options.add(new SelectOption(a.ID, a.Name));
        }        
                
        return options;
    }
 
    
    private boolean isValid(string submitedYear, string originalYear, double submitedRevenue, double OriginalRevenue)
    {    
        
        // make sure year submited is actuall an integer
        try {
              Integer x = Integer.valueOf(submitedYear);
            }
        Catch (exception e) {
            return false;
        }
        
        
        // If the original year has a value, do no allow user to clear out the value
        if ((originalYear != null || originalYear !='') && (submitedYear == null || submitedYear == ''))
            {
                return false;               
            }
            
        // If the original year has a value, do no allow user to submit a date that is too far in the past.
        // must be in the past 3 years
        if ((originalYear != null || originalYear !='') && (integer.valueof(submitedYear) < datetime.now().year() - 3))
            {
                return false;               
            }
            
            
        // If the original year has a value, do no allow user to submit a date that is in the future.
        // must be in the past 3 years
        if ((originalYear != null || originalYear !='') && (integer.valueof(submitedYear) > datetime.now().year()))
            {
                return false;               
            }
            
        // If the original revenue has a value, do no allow user to clear out the value
        if ((originalRevenue != null || originalRevenue != 0 ) && (submitedRevenue == null || submitedRevenue == 0))
            {
                return false;               
            }
            
        return true;
    }
    
    

}