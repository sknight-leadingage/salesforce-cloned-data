/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)

private class TestCorporateAllianceMPriorityTriggers {
	
	static NU__Entity__c entity = null; 
    static NU__GLAccount__c gl = null;
    static Account CAcompany = new Account(NU__PersonEmail__c = 'test@test.org');
    static Account contact = new Account(NU__PersonEmail__c = 'test@test.org');
    static NU__PriceClass__c priceClass = null;
    
    static NU__Product__c sponsorshipProduct = null;
    static NU__Deal__c sponsorshipDeal = null;
    static DealItem__c sponsorshipDealItem = null;
    
    static NU__Cart__c cart = null;
    static NU__CartItem__c cartItem = null;
    static NU__CartItemLine__c cartItemLine = null;
    
    static Map<String, Schema.RecordTypeInfo> productRecordTypes = null;
    static Map<String, Schema.RecordTypeInfo> dealRecordTypes = null;

   private static void setupTest() {
        if (entity != null) return;
        
        entity = NU.DataFactoryEntity.insertEntity();
        gl = NU.DataFactoryGLAccount.insertRevenueGLAccount(entity.Id);

        CAcompany = DataFactoryAccountExt.insertBusinessAccount(Constant.ACCOUNT_CORPORATE_ALLIANCE_SPONSOR_RECORD_TYPE_ID);
		
        contact = DataFactoryAccountExt.insertIndividualAccount();
        
        priceClass = priceClass = NU.DataFactoryPriceClass.insertDefaultPriceClass();
        
        // setup specific for sponsorships      
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
        dealRecordTypes = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosByName();
        
        Schema.RecordTypeInfo sponsorshipDealRTI = dealRecordTypes.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        String strProductName = 'AM2015/Annual Meeting Sponsorship';
        sponsorshipProduct = DataFactoryProductExt.insertProduct(strProductName, entity.Id, gl.Id, productRecordTypes, Constant.ORDER_RECORD_TYPE_SPONSORSHIP);
        sponsorshipDeal = DataFactoryDeal.insertDeal(Constant.ORDER_RECORD_TYPE_SPONSORSHIP, CAcompany.Id, contact.Id, Constant.DEAL_STATUS_NEW, sponsorshipDealRTI.getRecordTypeId());
        
        sponsorshipDealItem = DataFactoryDealItem.insertDealItem(sponsorshipDeal.Id, sponsorshipProduct.Id, 500.00, 1);
        
        cart = new NU__Cart__c(
            NU__BillTo__c = CAcompany.Id,
            NU__TransactionDate__c = Date.today(),
            NU__Entity__c = entity.Id,
            Deal__c = sponsorshipDeal.Id
        );
        insert cart;
        
        Map<String, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__CartItem__c.getRecordTypeInfosByName();
        cartItem = new NU__CartItem__c(
            NU__Cart__c = cart.Id,
            NU__Customer__c = CAcompany.Id,
            NU__PriceClass__c = priceClass.Id,
            RecordTypeId = rt.get(Constant.ORDER_RECORD_TYPE_SPONSORSHIP).getRecordTypeId()
        );
        insert cartItem;
        
        cartItemLine = new NU__CartItemLine__c(
            NU__CartItem__c = cartItem.Id,
            NU__IsInCart__c = true,
            NU__UnitPrice__c = sponsorshipDealItem.Price__c,
            NU__Product__c = sponsorshipDealItem.Product__c,
            NU__Quantity__c = sponsorshipDealItem.Quantity__c                            
        );
        
        SponsorshipCartItemLineData sponsorshipCartItemLineData = new SponsorshipCartItemLineData(sponsorshipDeal.Event__c, null, null);
        cartItemLine.NU__Data__c = JSON.serialize(sponsorshipCartItemLineData);
        insert cartItemLine;
              
    }
    
  	static OrderPurchaseSponsorship createOrderPurchaseSponsorship(Id billToId, Id entityId){
    	PageReference pageRef = new PageReference('/apex/OrderSponsorshipProducts');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('billTo',billToId);
        ApexPages.currentPage().getParameters().put('entity',entityId);
        OrderPurchaseSponsorship controller = new OrderPurchaseSponsorship();
        
        return controller;
    }
    
   static testMethod void myUnitTest() {
 
        setupTest();
        
        Id CompanyId = CAcompany.Id;
        
        System.assertEquals(null,CAcompany.Membership_1__c);
        
        productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
 		String strProductName = 'AM2015/Annual Meeting Sponsorship';
        NU__Product__c newProduct = DataFactoryProductExt.insertProduct(strProductName, entity.Id, gl.Id, productRecordTypes, 'Membership');

        NU__MembershipType__c CastMembershipType = DataFactoryMembershipTypeExt.createCASTMembershipType(entity.Id);
        insert CastMembershipType;
        
        NU__Membership__c CurrentMembership = DataFactoryMembershipExt.createCurrentCalendarYearMembership(CAcompany.Id, CastMembershipType.Id);
        insert CurrentMembership;
         //New Sponsorship Order - billTo == CAcompany & entity supplied      
        OrderPurchaseSponsorship controller = createOrderPurchaseSponsorship(CompanyId, entity.Id);
        
        // Submit Cart
        PageReference tempRef = controller.Save();

        CAcompany.CAST_Membership__c = CurrentMembership.Id;
        CAcompany.NU__Membership__c = CurrentMembership.Id;
        CAcompany.Membership_1__c = 'AM2015/Annual Meeting Sponsorship';
        CAcompany.Membership_2__c = 'AM2015/Annual Meeting Sponsorship';
        update CAcompany;
        CAcompany = new Account();
        CAcompany = [SELECT Id, Membership_1__c,Membership_2__c  FROM Account WHERE Id = :CompanyId LIMIT 1];        
        
        /*System.assertEquals('AM2015/Annual Meeting Sponsorship',CAcompany.Membership_1__c); */

    /*    
        CurrentMembership.MembershipTypeProductName__c = 'CAST Business Associate';
        update CurrentMembership;
        
        CAcompany.Phone = '1111111111';
        update CAcompany;
        
        CAcompany = new Account();
        CAcompany = [SELECT Id, Membership_1__c FROM Account WHERE Id = :CompanyId LIMIT 1];
        */
        //NU__Entity__c anyEntity = NU.DataFactoryEntity.insertEntity();
        //NU__GLAccount__c glAccount = NU.DataFactoryGLAccount.insertRevenueGLAccount(anyEntity.Id);
        //Map<String, Schema.RecordTypeInfo> productRecordTypes = Schema.SObjectType.NU__Product__c.getRecordTypeInfosByName();
 		//String strProductName = 'CAST Business Associate';
        //NU__Product__c newProduct = DataFactoryProductExt.createProduct(strProductName, anyEntity.Id, glAccount.Id, productRecordTypes, 'Membership');
    }
}