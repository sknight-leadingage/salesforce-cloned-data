<apex:page controller="CongressionalDistrictReportController" showHeader="{!!isCheckedHideMenus}" sidebar="{!!isCheckedHideMenus}" renderAs="{!IF(isCheckedRenderAsPDF,'pdf', null)}" applyHtmlTag="{!!isCheckedHideMenus}">
    <head>
    <style type="text/css">
        .headline {
            font-family: "Times New Roman", Cambria, Times, "Times New Roman", serif;
            font-size: 24px;
            font-weight: bold;
            color: #365F91;
        }
        .subheadline {
            font-family: "Times New Roman", Cambria, Times, "Times New Roman", serif;
            font-size: 14px;
            font-weight: bold;
            color: #365F91;
        }
        .count {
            font-family: "Times New Roman", Cambria, Times, "Times New Roman", serif;
            font-size: 11px;
            font-weight: bold;
            color: #4F81BD;
        }
        .entry {
            break-inside: avoid-column;
            -webkit-column-break-inside: avoid;
            page-break-inside: avoid;
            height: 150px;
            width: 45%;
            padding: 5px 5px 5px 25px;
            overflow-y: hidden;
            float: left;
        }
        .business {
            font-family: "Times New Roman", Calibri, Arial, Helvetica, sans-serif;
            font-size: 16px;
            font-weight: bold;
        }
        .services {
            font-family: "Times New Roman", Calibri, Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-style: italic;
        }
        .address {
            font-family: "Times New Roman", Calibri, Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        @media screen {
            div.divFooter {
                display: none;
            }
        }
    </style>
    <style type="text/css" media="print">
            .printhide {
                display: none;
            }
            .bPageTitle {
                display: none;
            }
            div.divFooter {
                font-family: Calibri, Arial, Helvetica, sans-serif;
                font-size: 12px;
                font-weight: bold;
                align: center;
                bottom: 0;
            }
    </style>
    </head>
  
	<body style="font-family: Calibri, Arial, Helvetica, sans-serif;">
    <apex:outputPanel rendered="{!IF(isCheckedRenderAsPDF,false,true)}">
    <apex:define name="sectionHeader">
        <div style="float: left;"><apex:sectionHeader title="Congressional District Report" help="#"/></div>
        <img style="float: right" src="{!$Resource.Report_Logo}" />
        <apex:pageMessages />
    </apex:define>
    
    <div style="float: none; clear: both">
    <apex:form id="options">
        <apex:selectList value="{!SelectedState}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select State --" />
            <apex:selectOptions value="{!states}"/>
        </apex:selectList>

        <apex:selectList value="{!SelectedDistrict}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select District --" />
            <apex:selectOptions value="{!districts}"/>
        </apex:selectList>

        <apex:selectList value="{!SelectedTypeProvider}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select Provider Type --" />
            <apex:selectOptions value="{!types}"/>
        </apex:selectList>
        
        <apex:selectList value="{!SelectedLayout}" multiselect="false" size="1">
            <apex:selectOption itemValue="-1" itemLabel=" -- Select layout --" />
            <apex:selectOption itemValue="1" itemLabel=" -- Internal --"/>
            <apex:selectOption itemValue="2" itemLabel=" -- Senate offices --"/>
            <apex:selectOption itemValue="3" itemLabel=" -- Congressional offices --"/>
        </apex:selectList>

        <apex:commandButton action="{!Run}" value="Run Report"/>
        <br />
        Show as PDF (Check this box and click Run Report for well-formatted print output) <apex:inputCheckbox value="{!isCheckedRenderAsPDF}"/>
        <br />
        If you would like to add to the headling between LeadingAge and Members: <apex:inputText value="{!HeadlineAddones}" id="HeadlineAddones" html-placeholder="   Headline Add ons "/>
        <br />
        Please type in the text you would like in the footer of the report: <apex:inputText style="width:300px" value="{!FooterText}" id="FooterText" html-placeholder="Ex:Name and Phone number, Email"/>
        <br />
        Hide Menus <apex:inputCheckbox value="{!isCheckedHideMenus}"/>
        <!-- Render as PDF <apex:inputCheckbox value="{!isCheckedRenderAsPDF}"/>  -->

    </apex:form>
    </div><div style="float: none; clear: both"></div>
    </apex:outputPanel>

        <img style="center; clear: both" src="{!$Resource.Report_Logo}" />

    <div style="text-align: center; font-size: 16px; clear: both">
    <p><span class="headline">LeadingAge {!HeadlineAddones} Members: {!SelectedStateName} - {!SelectedDistrictOrdinal} Congressional District</span></p><br />
    </div>
    
    <br />


    <div style="font-family:  Arial, Calibri,Helvetica, sans-serif; font-size: 14px">
        <apex:repeat value="{!records}" var="r" >
              
            <div style="page-break-inside: avoid; width: 45%; padding: 5px 5px 5px 15px; float: left;">
            <div style="position: relative; break-inside: avoid-column; -webkit-column-break-inside: avoid; min-height: 100px; max-height: 150px; overflow-y: hidden;">
            <span style="font-size: 14px; font-weight: bold;"><apex:outputtext value="{!r.Name}" /></span><br />
			<apex:outputPanel rendered="{!IF(SelectedLayout=1,true,false)}">
            <span class="address"><apex:outputText rendered="{!IF(r.NU__Affiliates__r.size > 0, TRUE, FALSE)}">
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.Name}" />, &nbsp;
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.PersonTitle}" /> &nbsp;|&nbsp;
            <apex:outputtext value="{!r.NU__Affiliates__r[0].NU__Account__r.PersonEmail}" />
            </apex:outputText></span><br />
            </apex:outputPanel>
			<apex:outputPanel rendered="{!IF(SelectedLayout=1 || SelectedLayout=3,true,false)}">
            <span class="address"><apex:outputtext value="{!r.BillingStreet}" /><br />
            <apex:outputtext value="{!r.BillingCity}" />,&nbsp;<apex:outputtext value="{!r.BillingState}" />&nbsp;<apex:outputtext value="{!r.BillingPostalCode}" /></span><br />
            </apex:outputPanel>
            <apex:outputPanel rendered="{!IF(SelectedLayout=2,true,false)}">
            <span class="address">
            <apex:outputtext value="{!r.BillingCity}" />,&nbsp;<apex:outputtext value="{!r.BillingState}"/> </span><br />
            </apex:outputPanel>
            <span class="services"><apex:outputtext value="{!r.Provider_Type__c}" /></span> <br />
            <span style="font-size: 12px;">
            Nursing Beds: <apex:outputtext value="{!NursingBeds[r.Id]}" /> <br />
            Assisted Living Units: <apex:outputtext value="{!AssistedLivingBeds[r.Id]}" /><br />
            Senior Housing Units:  <apex:outputtext value="{!SeniroHousingUnits[r.Id]}" /> <br/></span>
            
            <!--  Nursing Beds: <apex:outputtext value="{!SUM(r.SNF_Total_License_Beds__c,r.SNF_Dual_Certified_Beds__c,r.Number_Of_Nursing_Beds__c,r.ICF_Beds__c,r.Number_of_Personal_Care_Beds__c,r.Personal_Care_Beds__c,r.SNF_Medicare_Beds__c,r.SNF_Medicaid_Beds__c)}" /> <br />
            Assisted Living Units: <apex:outputtext value="{!SUM(r.Number_of_Independent_Living_Units__c,r.Number_Of_Assisted_Living_Units__c )}" /><br />
            Senior Housing Units:  <apex:outputtext value="{!SUM(r.ILU_Other__c,r.ILU_Market_Rate__c,r.ILU_Tax_Credit_Income_Rest__c,r.ILU_Fed_Subsidized__c,r.Total_Number_Of_HUD_Units__c,r.Total_Housing_Units__c)}" /> <br/></span>
            -->
            
            </div>
            </div>
        </apex:repeat>
    </div>

    <div style="float: none; clear: both; padding-top: 25px;">
        <div class="divFooter" style="text-align: center; clear: both; position: relative;">
        <p>{!FooterText}</p>
        </div>    
    </div><div style="float: none; clear: both"></div>
    </body>

</apex:page>