public with sharing class LeadershipAcademyCoreMgmtInd_Controller {
    public static final String ACADEMY_YEAR_PARAM = 'acadyear';
    
    private Academy_Applicant__c applicantRecord = null;
    public Academy_Applicant__c AcademyCoreMgmtInd {
    	get	{
    		if (applicantRecord == null) {
    			applicantRecord = getAcademyCoreMgmtInd(); 
    		}
			return applicantRecord; 
    	}
    	set {
    		applicantRecord = value;
    	}
    }
    
    public LeadershipAcademyCoreMgmtInd_Controller(){
        if (AcademyCoreMgmtInd != null && AppStatus == null) {
            AppStatus = AcademyCoreMgmtInd.Application_Status__c;
        }                    
        currentPageNum = 1;
    }
    
    public string AcademyYear
    {
        get
        {
            return ApexPages.CurrentPage().getParameters().get(ACADEMY_YEAR_PARAM);
        }
   
    }
    
    public Id AppId{
        get{
            return ApexPages.CurrentPage().getParameters().get('acc');
        }
    }
    
     
    public string AppStatus {get;set;}
    public List<SelectOption> getAppStatuses(){
            List<SelectOption> options = new List<SelectOption>();
            
            Schema.DescribeFieldResult fieldResult = Schema.Academy_Applicant__c.fields.Application_Status__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
               options.add(new SelectOption('','--None--'));
               for(Schema.PicklistEntry p : ple)
                   options.add(new SelectOption(p.getValue(), p.getValue())); 
               return options;
    }
        
    private Academy_Applicant__c getAcademyCoreMgmtInd(){
    	List<Academy_Applicant__c> conItems = (List<Academy_Applicant__c>) con.getRecords();
    	if (conItems != null && conItems.size() > 0) {
    		return conItems[0];
    	}
    	else {
    		return null;
    	}
    }
    
    public void ProcessSave()
    {
        try
        {
			if (AcademyCoreMgmtInd != null) {
                applicantRecord.Application_Status__c = AppStatus;
                Academy_Applicant__c aa = new Academy_Applicant__c(Id = AppId, Application_Status__c = AppStatus);
                update aa;

                if (applicantRecord.Academy_Status__c != null) {
                    update applicantRecord.Academy_Status__r;
                }
                
                refresh();
            }
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }

    public void AddtoClass()
    {
        try{
            Academy_Applicant__c app = getAcademyCoreMgmtInd(); //Get first one in list, because typically there's only one anyway

            if(app.Academy_Status__c == null) {
                //if we don't have a lookup on Applicant to Status then create one
                Academy_Status__c AcademyStatus = new Academy_Status__c(
                    Account__c = app.Account__c,
                    Academy_Class_Status__c = 'Applicant',
                    Academy_Year__c = AcademyYear
                );
                insert AcademyStatus;
               
                //Now we have an Academy Status record, create the link to it in the Applicant record
                app.Academy_Status__c = AcademyStatus.Id;
                update app;
                
                //AcademyCoreMgmtInd = null;
                //AcademyCoreMgmtInd = getAcademyCoreMgmtInd();
                refresh(); 
            }
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
    }
    
    
     // Grabbed the following paging code from http://blog.jeffdouglas.com/2009/07/14/visualforce-page-with-pagination/
    
    // instantiate the StandardSetController from a query locator
    Transient public ApexPages.Standardsetcontroller transientController { get; set; }
    
    public ApexPages.StandardSetController con {
        get {
            if(transientController == null) {
                transientController = new ApexPages.StandardSetController(
					[SELECT Id, Name, Account__c, Account__r.Name, 
	                    Academy_Year__c, Submission_Status__c, Application_Status__c, Submit_Date__c,
	                    Academy_Status__c,
	                    Academy_Status__r.Academy_Class_Status__c, Academy_Status__r.Academy_Start_Date__c, Academy_Status__r.Academy_End_Date__c, 
	                    Academy_Status__r.Academy_Year__c, Academy_Status__r.Cohort__c, Academy_Status__r.Financial_Assistance__c
	                    FROM Academy_Applicant__c
	                    WHERE Submission_Status__c = 'Complete' AND Academy_Year__c = :AcademyYear]
					//[SELECT Id FROM Academy_Applicant__c WHERE Submission_Status__c = 'Complete' AND Academy_Year__c = :AcademyYear]
                );

                // sets the number of records in each page set
                transientController.setPageSize(1);
            }
            return transientController;
        }
        set { transientController = value; }
    }
 
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return currentPageNum < ResultSize;
        }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return currentPageNum > 1;
        }
        set;
    }
 
    // returns the page number of the current page set
    public Integer currentPageNum { get; set; }
    
    public Integer pageNumber {
        get {
            return currentPageNum;
        }
        set {
            currentPageNum = value;
            con.SetPageNumber( currentPageNum );
        }
    }
    
    public Integer ResultSize {
        get {
            return con.getResultSize();
        }
    }
    
    public virtual void onPageChange() {
    	applicantRecord = null;
    	AppStatus = AcademyCoreMgmtInd.Application_Status__c;
    }
    
    public void changeToPage(Integer newPageNumber){
        pageNumber = newPageNumber;
        onPageChange();
    }
 
    // returns the first page of records
    public void first() {
        changeToPage(1);
    }
 
    // returns the last page of records
    public void last() {
        changeToPage(ResultSize);
    }
 
    // returns the previous page of records
    public void previous() {
    	con.previous();
        changeToPage(currentPageNum - 1);
        
    }
 
    // returns the next page of records
    public void next() {
    	con.next();
        changeToPage(currentPageNum + 1);
        
    }
 
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
    }
    
    public void refresh(){
        Integer currentPageNumber = pageNumber;
        con = null;
        transientController = null;
        applicantRecord = null;
        pageNumber = currentPageNumber;
    }
    
    public void refreshAtPage1(){
        refresh();
        pageNumber = 1;
    }



    
}