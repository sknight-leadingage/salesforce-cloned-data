<apex:page controller="ConferenceSpeakersController">
<script>

function setFocusOnLoad() {}

</script>

    <style>
        .pbSubheader {
            color: black !important; 
        }
    </style>

    <apex:pageMessages />
    
    <apex:pageMessage summary="There are no speakers for the {! Conference.Name } conference."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && hasSpeakerSearchCriteria == false }" />
    
    <apex:pageMessage summary="No speakers found matching your search criteria."
                      severity="Info"
                      strength="2"
                      rendered="{! ResultSize == 0 && hasSpeakerSearchCriteria }" />
    
    <apex:form >
        <apex:pageBlock title="Speakers">
            <div style="clear: both; height: 50px;">
                <apex:panelGrid columns="5" rendered="{! ResultSize > 0 }" style="float: left">
                    <apex:commandLink action="{!first}" rendered="{! ResultSize > 1 && PageNumber != 1 }">First</apex:commandlink>
                    <apex:commandLink action="{!previous}" rendered="{!hasPrevious}">Previous</apex:commandlink>
                    <apex:outputText value="Record {! PageNumber } of {! ResultSize }" />
                    <apex:commandLink action="{!next}" rendered="{!hasNext}">Next</apex:commandlink>
                    <apex:commandLink action="{!last}" rendered="{! ResultSize > 1 && PageNumber != ResultSize }">Last</apex:commandlink>
                </apex:panelGrid>
            
           
                <div style="float: right;">
                    <c:ConferenceNavComponent eventId="{! EventId }" />
                </div>
            </div>
        
            <apex:pageBlockSection title="Speaker Information" rendered="{! ResultSize > 0 }">

                <apex:outputField value="{! CurrentSpeaker.NU__FullName__c}" />
                <apex:outputField value="{! CurrentSpeaker.PersonTitle}" />
                <apex:outputField value="{! CurrentSpeaker.Speaker_Company_Name_Rollup__c}" />
                <apex:outputField value="{! CurrentSpeaker.Speaker_Full_Address__c}" />
                <apex:outputField value="{! CurrentSpeaker.Phone}" />
                <apex:outputField value="{! CurrentSpeaker.NU__PersonEmail__c}" />
                <apex:outputField value="{! CurrentSpeaker.LeadingAge_Id__c }" />
                <apex:outputField value="{! CurrentSpeaker.isLeadingAgeMember__c }" />

            </apex:pageBlockSection>

            <apex:pageBlockSection title="{! Conference.Name } Session Info"
                                   columns="1"
                                   rendered="{! ResultSize > 0 }">
                <apex:outputText value="This speaker currently is not speaking at this conference." 
                                 rendered="{! CurrentSpeakerEventSessions.size == 0 }" />
                                 
                <apex:pageBlockSectionItem >
                    Session
                    
                    <apex:outputPanel >
                        <apex:selectList id="EventSessionSelectList"
                                     multiselect="false"
                                     size="1"
                                     value="{! SessionSpeakerToAdd.Session__c }">
                            <apex:selectoption itemLabel="Please select a session" itemValue="" />
                            <apex:selectOptions value="{! AvailableEventSessionsForCurrentSpeakerSelectOpts }"/>
                        </apex:selectList>
                        
                        <apex:outputText value="{! $ObjectType.Conference_Session_Speaker__c.Fields.Role__c.Label }"
                                         style="padding: 0px 10px;" />
                        
                        <apex:inputField value="{! SessionSpeakerToAdd.Role__c }" />
                        
                        <apex:commandButton value="Add and Save Changes"
                                            action="{! addSessionSpeaker }"
                                            style="margin-left: 20px;" />
                    
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            
            
                <apex:pageBlockTable value="{! CurrentSpeakerEventSessions }"
                                     var="eventSession"
                                     rendered="{! CurrentSpeakerEventSessions.size > 0 }">
                                     
                    <apex:column headerValue="Remove from Session" rendered="{!$ObjectType.Conference_Session_Speaker__c.Deletable}" width="225">
                        <apex:commandLink value="Remove Speaker from Session"
                                          action="{! deleteSessionSpeaker }" onclick="return confirm('Are you sure you want to remove this speaker from selected session?');">
                            <apex:param name="q"
                                        value="{!eventSession.Id}"
                                        assignTo="{! SessionSpeakerIdToDelete}" />
                        </apex:commandLink>
                    </apex:column>
                    <apex:column value="{! eventSession.Session__r.Session_Title_Full__c }" />
                    <apex:column value="{! eventSession.Role__c }" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Speaker History" columns="1"
                                   rendered="{! ResultSize > 0 }">
                <apex:outputText value="This speaker has no session history."
                                 rendered="{! CurrentSpeaker.Session_Speakers__r.size == 0 }" />
                                 
                <apex:pageBlockTable value="{! CurrentSpeaker.Session_Speakers__r }"
                                     var="Session"
                                     rendered="{! CurrentSpeaker.Session_Speakers__r.size > 0 }">
                    <apex:column value="{! Session.Session__r.Conference__c }" />
                    <apex:column value="{! Session.Session__r.Session_Title_Full__c }" />
                    <apex:column value="{! Session.Overall_Average_Score__c }" />
                    <apex:column value="{! Session.Topic_Score__c }" />
                    <apex:column value="{! Session.Audience_Score__c }" />
                    <apex:column value="{! Session.Presentation_Score__c }" />
                    <apex:column value="{! Session.Info_Score__c }" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock title="Search">
            <apex:pageBlockSection title="Speaker Search">
                <apex:inputText value="{!FirstNameToSearch}" label="First Name" />
                <apex:inputText value="{!LastNameToSearch}" label="Last Name" />
                <apex:inputText value="{!CompanyToSearch}" label="Company" />
                <apex:inputText value="{!ZipCodeToSearch}" label="Zip Code" />
                <apex:inputText value="{!PhoneToSearch}" label="Phone" />
                <apex:inputText value="{!LeadingAgeIDToSearch}" label="LeadingAge ID" />
                
                <apex:pageBlockSectionItem />
                
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:commandButton value="Search" action="{! searchForSpeakers }" />
                        <apex:commandButton value="Clear Search"
                                            action="{! clearSearch }"
                                            style="margin-left: 10px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>