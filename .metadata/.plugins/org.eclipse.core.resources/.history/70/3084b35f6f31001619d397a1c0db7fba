/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestJointBillingItemLineTriggerHandlers {

	static Account StatePartner = null;
    static Account ProviderAccount = null;
    static Account ProviderAccountQueried = null;
    
    static NU__MembershipType__c JointStateProviderMT = null;
    static NU__Membership__c JointStateProviderMembership = null;
    
    static Joint_Billing__c JB = null;
    static Joint_Billing__c JBQueried = null;
    static Date JoinDateToUse = null;
    
    private static void loadTestData(){
        StatePartner = DataFactoryAccountExt.insertStatePartnerAccount();
        ProviderAccount = DataFactoryAccountExt.insertProviderLinkedToStatePartnerAccount(500000, StatePartner.Id, JoinDateToUse);
        ProviderAccountQueried = AccountQuerier.getAccountById(providerAccount.Id);
        
        JointStateProviderMT = DataFactoryMembershipTypeExt.insertJointStateProviderMembershipTypeWithProduct();
        JointStateProviderMembership = DataFactoryMembershipExt.insertCurrentProviderMembership(providerAccount.Id, jointStateProviderMT);
        
        JB = TestJointStateProviderBiller.runJointBilling(StatePartner.Id, JointStateProviderMembership.NU__EndDate__c);
        JBQueried = getJointBillingByUnInsertedJointBillingRecord(JB);
    }
    
    static Joint_Billing__c getJointBillingByUnInsertedJointBillingRecord(Joint_Billing__c insertedWithoutIDJB){
    	return [select id,
                       name,
                       Amount__c,
                       Bill_Date__c,
                       Start_Date__c,
                       End_Date__c,
                       (select id,
                               name,
                               Amount__c,
                               Account__c,
                               Joint_Billing__r.Start_Date__c
                          from Joint_Billing_Items__r)
                  from Joint_Billing__c
                 where Bill_Date__c = :insertedWithoutIDJB.Bill_Date__c
                   and Start_Date__c = :insertedWithoutIDJB.Start_Date__c
                   and End_Date__c = :insertedWithoutIDJB.End_Date__c
                   and State_Partner__c = :insertedWithoutIDJB.State_Partner__c];
    }
    
    static Joint_Billing__c getJointBillingById(Id jointBillingId){
        return [select id,
                       name,
                       Amount__c,
                       Bill_Date__c,
                       Start_Date__c,
                       End_Date__c,
                       (select id,
                               name,
                               Amount__c,
                               Account__c,
                               Joint_Billing__r.Start_Date__c
                          from Joint_Billing_Items__r)
                  from Joint_Billing__c
                 where id = :jointBillingId];
    }
    
    static void assertMembershipAmountUpdated(Id jointBillingItemId, Date membershipStartDate, Decimal expectedDuesAmount){
    	Joint_Billing_Item__c jointBillingItem = 
    	[select id,
               name,
               Amount__c,
               Account__c,
               Joint_Billing__r.Start_Date__c
           from Joint_Billing_Item__c
          where id = :jointBillingItemId];
          
        NU__Membership__c providerJointBillingMembership = 
        [select id,
                name,
                NU__Account__c,
                NU__Amount__c,
                NU__ExternalAmount__c
           from NU__Membership__c
          where NU__Account__c = :jointBillingItem.Account__c
            and NU__MembershipType__r.Name = :Constant.JOINT_STATE_PROVIDER_MEMBERSHIP_TYPE_NAME
            and NU__StartDate__c = :membershipStartDate];
        
        system.assertEquals(expectedDuesAmount, providerJointBillingMembership.NU__Amount__c, 'The provider\'s membership amount was not updated to ' + expectedDuesAmount + ' from ' + providerJointBillingMembership.NU__Amount__c);
    }

    static testMethod void newAdjustmentLineMembershipAmountUpdatedTest() {
        loadTestData();
        
        Joint_Billing_Item__c jbi = JBQueried.Joint_Billing_Items__r[0];
        
        Decimal initialDuesAmount = jbi.Amount__c;
        Decimal duesAdjustmentAmount = 1000;
        
        Joint_Billing_Item_Line__c adjustmentLine = JointBillingUtil.createAdjustmentJointBillingItemLine(duesAdjustmentAmount, ProviderAccountQueried, jbi.Id);
        insert adjustmentLine;
        
        Decimal expectedDuesAmount = initialDuesAmount + duesAdjustmentAmount;
        
        assertMembershipAmountUpdated(jbi.Id, JBQueried.Start_Date__c, expectedDuesAmount);
    }
    
    static testMethod void updateExistingJointBillingItemLineMembershipAmountUpdatedTest() {
        loadTestData();
        
        Joint_Billing_Item__c jbi = JBQueried.Joint_Billing_Items__r[0];
        
        Decimal initialDuesAmount = jbi.Amount__c;
        Decimal duesAdjustmentAmount = 1000;
        
        Joint_Billing_Item_Line__c jbilToUpdate = [select id, Amount__c from Joint_Billing_Item_Line__c where Joint_Billing_Item__c = :jbi.Id limit 1];
        
        jbilToUpdate.Amount__c += duesAdjustmentAmount;
        update jbilToUpdate;
        
        Decimal expectedDuesAmount = initialDuesAmount + duesAdjustmentAmount;
        
        assertMembershipAmountUpdated(jbi.Id, JBQueried.Start_Date__c, expectedDuesAmount);
    }
    
    static testmethod void deleteJointBillingItemLineMembershipAmountUpdatedTest(){
    	loadTestData();
        
        Joint_Billing_Item__c jbi = JBQueried.Joint_Billing_Items__r[0];
        
        Joint_Billing_Item_Line__c initialJBILToDelete = [select id, Amount__c from Joint_Billing_Item_Line__c where Joint_Billing_Item__c = :jbi.Id limit 1];
        
        Decimal initialDuesAmount = jbi.Amount__c;
        Decimal duesAdjustmentAmount = 1000;
        
        Joint_Billing_Item_Line__c adjustmentLine = JointBillingUtil.createAdjustmentJointBillingItemLine(duesAdjustmentAmount, ProviderAccountQueried, jbi.Id);
        insert adjustmentLine;
        
        delete initialJBILToDelete;
        
        assertMembershipAmountUpdated(jbi.Id, JBQueried.Start_Date__c, duesAdjustmentAmount);
    }
}