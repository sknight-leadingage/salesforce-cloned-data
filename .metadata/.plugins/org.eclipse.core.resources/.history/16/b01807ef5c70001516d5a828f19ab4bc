global without sharing class AccountFamilyTree {
	/*
	*
	* BEGIN: Code to prevent Trigger recursion:
	*
	* 		http://salesforce.stackexchange.com/questions/109/workflow-rule-causing-trigger-to-fire-twice
	*/
	private static Map<String,Boolean> singletonMap;
	
	global static Boolean hasAlreadyExecuted(String ClassNameOrExecutionName){  
	    if(singletonMap != null) {
	        Boolean alreadyExecuted = singletonMap.get(ClassNameOrExecutionName);
	        if(alreadyExecuted != null) {
	            return alreadyExecuted;
	        }           
	    }
	    //By default return false
	    return false;
	}
	
	global static void setAlreadyExecuted(String ClassNameOrExecutionName) {
	    if(singletonMap == null) {
	        singletonMap = new Map<String,Boolean>();
	    }
	    singletonMap.put(ClassNameOrExecutionName,true);
	}
	/* END: Trigger recursion prevention code */
	
	global static void setStatePartnerFieldsForStateUsers(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts) {
		//Manage the (unwanted) recursion
		if (!AccountFamilyTree.hasAlreadyExecuted('setStatePartnerFieldsForStateUsers')) {
			AccountFamilyTree.setAlreadyExecuted('setStatePartnerFieldsForStateUsers');
			
	    	Map<Id,Account> processedAccounts = new Map<Id,Account>();
	    	Account oldAccount = null;
	    	
	    	//State_Partner_ID__r.Name is null in the maps passed in from the trigger, so we have to query the name fields separately
	    	Set<Id> ks = newAccounts.keySet();
	    	Account queriedAccount = null;
	    	Map<Id,Account> mapNewAccountsQueryingData = new Map<Id,Account>((List<Account>)database.query('SELECT Id, State_Partner_ID__c, State_Partner_ID__r.Name, RecordType.Id, NU__PrimaryAffiliation__c, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.State_Partner_ID__c, NU__PrimaryAffiliation__r.State_Partner_ID__r.Name FROM Account WHERE Id = :ks'));
	    	
	    	Map<Id,Account> allTopChildAccounts = new Map<Id,Account>((List<Account>)database.query('SELECT Id, State_Partner_Name__c, NU__PrimaryAffiliation__c, State_Partner_ID__c, RecordType.Id FROM Account WHERE NU__PrimaryAffiliation__c = :ks'));
	    	Set<Id> ksc = allTopChildAccounts.keySet();
	    	List<Account> allChildBelowTopAccounts = database.query('SELECT Id, NU__PrimaryAffiliation__c, RecordType.Id FROM Account WHERE NU__PrimaryAffiliation__c = :ksc');
	    	
	    	long lCpuBailout = mapNewAccountsQueryingData.size() + allTopChildAccounts.size() + allChildBelowTopAccounts.size();
	    	
	    	if (lCpuBailout > 37) {
	    		system.debug('   datatree assumed to be too large. lCpuBailout at ' + lCpuBailout);
	    	}
	    	else {
		    	//Begin comparison of previous Account record to updated Account record
		    	system.debug('   datatree under 200. lCpuBailout at ' + lCpuBailout);
		    	for(Account updatedAccount : newAccounts.values()) {
		    		oldAccount = oldAccounts.get(updatedAccount.Id);
		    		queriedAccount = mapNewAccountsQueryingData.get(updatedAccount.Id);
		    		
		    		//If State_Partner_Name__c has changed...
		    		if ((oldAccount != null && updatedAccount.State_Partner_ID__c != oldAccount.State_Partner_ID__c) || oldAccount == null) {
						//State Partner Name field is always going to be the name of your State Partner regardless of Record Type
						processedAccounts.put(updatedAccount.Id, new Account(Id = updatedAccount.Id, State_Partner_Name__c = queriedAccount.State_Partner_ID__r.Name));
						
	    				//Now figure out the cascade
	    				//Note that due to the way Salesforce works, we have to query all children of all accounts outside the for loop and then find the ones we're working with here
	    				
	    				if (queriedAccount.RecordType.Id == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID || queriedAccount.RecordType.Id == Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID) {
	    					//If the top-level account is a provider, we assume we're only updating Individual-like accounts
	    					for(Account u : allTopChildAccounts.values()) {
	    						if (u.NU__PrimaryAffiliation__c == updatedAccount.Id) {
									processedAccounts.put(u.Id, new Account(Id = u.Id, Parent_State_Partner_Name__c = queriedAccount.State_Partner_ID__r.Name));
	    						}
	    					}
	    				}
	    				
	    				if (queriedAccount.RecordType.Id == Constant.ACCOUNT_MULTI_SITE_RECORD_TYPE_ID) {
	    					Account toUpdate = null;
	    					
	    					//For MSO, we still have to set the Name fields for the top-level children, but we recognize that Providers and Individuals can exist at the MSO level
	    					for(Account un : allTopChildAccounts.values()) {
	    						if (un.NU__PrimaryAffiliation__c == updatedAccount.Id) {
		    						//If you're directly attached to the MSO then your Parent name is the MSO SP ID Name regardless of record type
		    						toUpdate = new Account(Id = un.Id, Parent_State_Partner_Name__c = queriedAccount.State_Partner_ID__r.Name);
		    						if (un.State_Partner_ID__c == null && (un.RecordType.Id == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID || un.RecordType.Id == Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID)) {
		    							//Provider-style records should keep their 'direct' state partner, e.g. where they have a State Partner ID set
		    							//However if they don't have that value set then we can at least set the name field
										toUpdate = new Account(Id = un.Id, State_Partner_Name__c = queriedAccount.State_Partner_ID__r.Name, Parent_State_Partner_Name__c = queriedAccount.State_Partner_ID__r.Name);
		    						}
		    						processedAccounts.put(toUpdate.Id, toUpdate);
		    						
		    						for (Account unc : allChildBelowTopAccounts) {
		    							if (unc.NU__PrimaryAffiliation__c == un.Id) {
		    								//These are children of the children, so we can assume that they're individuals
		    								processedAccounts.put(unc.Id, new Account(Id = unc.Id,
		    									Grandparent_State_Partner_Name__c = queriedAccount.State_Partner_ID__r.Name,
		    									Parent_State_Partner_Name__c = un.State_Partner_Name__c == null ? queriedAccount.State_Partner_ID__r.Name : un.State_Partner_Name__c));
		    							}
		    						}
	    						}
	    					}
	    					
	    					
	    				}
		    		}
		    		
		    		if ((updatedAccount.NU__PrimaryAffiliation__c != null) && (oldAccount != null && updatedAccount.NU__PrimaryAffiliation__c != oldAccount.NU__PrimaryAffiliation__c) || oldAccount == null) {
		    			//If my affiliation has changed then:
		    			//If I'm a Provider or Company then:
		    			//1. My Parent State Partner Name should be the same as the State_Partner_ID__r.Name of my Primary Affiliation
		    			//2. The Grandparent State Partner Name of *my children* should be the State_Partner_ID__r.Name of *my* Primary Affiliation
		    			//If I'm an Individual then:
		    			//1. My Parent State Partner Name should be the same as the State_Partner_ID__r.Name of my Primary Affiliation
		    			Account meQueried = null;
		    			Account toUpdate = null;
		    			Account toUpdate1 = null;
		    			for (Account me : newAccounts.values()) {
		    				meQueried = mapNewAccountsQueryingData.get(me.Id);
		    				if (meQueried.RecordType.Id == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID
		    						|| meQueried.RecordType.Id == Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID
		    						|| meQueried.RecordType.Id == Constant.ACCOUNT_INDIVIDUAL_RECORD_TYPE_ID) {
		    					
		    					toUpdate = hasBeenProcessedForUpdate(processedAccounts, me);
		    					toUpdate.Parent_State_Partner_Name__c = meQueried.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name;
		    					processedAccounts.put(toUpdate.Id, toUpdate);
		    					
		    					if (meQueried.RecordType.Id == Constant.ACCOUNT_PROVIDER_RECORD_TYPE_ID || meQueried.RecordType.Id == Constant.ACCOUNT_COMPANY_RECORD_TYPE_ID) {
			    					for(Account u : allTopChildAccounts.values()) {
			    						if (u.NU__PrimaryAffiliation__c == me.Id) {
			    							toUpdate1 = hasBeenProcessedForUpdate(processedAccounts, u);
			    							toUpdate1.Grandparent_State_Partner_Name__c = meQueried.NU__PrimaryAffiliation__r.State_Partner_ID__r.Name;
			    							processedAccounts.put(toUpdate1.Id, toUpdate1);
			    						}
			    					}
		    					}
		    					
		    				}
		    			}
		    		}
		    		
		    	}
		    	
		    	if (processedAccounts.size() > 0) {
		    		update processedAccounts.values();
		    	}
			}
		}
	}
	
	global static Account hasBeenProcessedForUpdate(Map<Id,Account> m, Account a) {
		if (m.containsKey(a.Id)) {
			return m.get(a.Id);
		} else {
			return new Account(Id = a.Id);
		}
	}
}