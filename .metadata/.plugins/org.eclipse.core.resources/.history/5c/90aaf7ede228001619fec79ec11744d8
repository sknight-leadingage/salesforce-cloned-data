@isTest
private class TestNimbleLeadConversionController {
    static final String DEFAULT_LEAD_FIRST_NAME = 'John';
    static final String DEFAULT_LEAD_LAST_NAME = 'Smith';
    static final String DEFAULT_LEAD_EMAIL = 'jsmith@company.com';
    static final String DEFAULT_LEAD_COMPANY_NAME = 'ABC Inc.';

    static final String DEFAULT_LEAD_PHONE = '123-456-7890';
    static final String DEFAULT_LEAD_STREET = '1 Main St.';
    static final String DEFAULT_LEAD_CITY = 'Anywhere';
    static final String DEFAULT_LEAD_STATE = 'NY';
    static final String DEFAULT_LEAD_POSTAL_CODE = '12345';
    static final String DEFAULT_LEAD_COUNTRY = 'United States';

    static ApexPages.StandardController standardController;
    static NimbleLeadConversionController controller;

    static Lead Lead;
    static User currentUser;

    private static void setupTest() {
        Lead = new Lead(
            FirstName = DEFAULT_LEAD_FIRST_NAME,
            LastName = DEFAULT_LEAD_LAST_NAME,
            Email = DEFAULT_LEAD_EMAIL,
            Company = DEFAULT_LEAD_COMPANY_NAME,
            Phone = DEFAULT_LEAD_PHONE,
            Street = DEFAULT_LEAD_STREET,
            City = DEFAULT_LEAD_CITY,
            State = DEFAULT_LEAD_STATE,
            PostalCode = DEFAULT_LEAD_POSTAL_CODE,
            Country = DEFAULT_LEAD_COUNTRY
        );

        insert Lead;

        standardController = new ApexPages.StandardController(Lead);
        controller = new NimbleLeadConversionController(standardController);
    }

    static testMethod void testGetCompanyOptionsIncludesNoneAndNewOptions() {
        setupTest();

        List<SelectOption> options = controller.getCompanyOptions();
        Set<String> optionValueSet = new Set<String>();

        for (SelectOption so : options) {
            optionValueSet.add(so.getValue());
        }

        system.assert(
            optionValueSet.contains(NimbleLeadConversionController.CREATE_NO_COMPANY_ID),
            'Expected company options to contain an option for "None"'
        );

        system.assert(
            optionValueSet.contains(NimbleLeadConversionController.CREATE_NEW_COMPANY_ID),
            'Expected company options to contain an option for "Create New"'
        );
    }

    static testMethod void testGetCompanyOptionsIncludesMatchingCompanies() {
        setupTest();

        List<Account> companies = NU.DataFactoryAccount.createAccountOrganizations(2);
        companies[0].Name = DEFAULT_LEAD_COMPANY_NAME;

        Test.startTest();
        insert companies;
        Test.stopTest();

        List<SelectOption> options = controller.getCompanyOptions();
        Set<String> expectedOptionValues = new Set<String>();
        Set<String> actualOptionValues = new Set<String>();

        for (SelectOption so : options) {
            expectedOptionValues.add(so.getValue());
        }

        for (Account company : NimbleLeadConversionController.getCompaniesByName(DEFAULT_LEAD_COMPANY_NAME)) {
            actualOptionValues.add(company.Id);
        }

        system.assert(
            expectedOptionValues.containsAll(actualOptionValues),
            'Expected company options to include matching company names from the Lead'
        );
    }

    static testMethod void testLeadIsConverted() {
        setupTest();

        PageReference pr = controller.convert();
        system.assert(pr != null, controller.ErrorMessage);

        Account individualAcct = getIndividualAccounts()[0];

        reloadLead();
        system.assertEquals(true, Lead.IsConverted);
    }

    static testMethod void testConvertCreatesIndividualAccount() {
        setupTest();

        controller.Company = NimbleLeadConversionController.CREATE_NO_COMPANY_ID;

        PageReference pr = controller.convert();
        system.assert(pr != null, controller.ErrorMessage);

        assertOneIndividual();

        List<Account> createdCompanyAccounts = getCompanyAccounts();
        system.assertEquals(0, createdCompanyAccounts.size());
    }

    static testMethod void testConvertCreatesNewCompanyAndPrimaryAffiliation() {
        setupTest();

        controller.Company = NimbleLeadConversionController.CREATE_NEW_COMPANY_ID;

        PageReference pr = controller.convert();
        system.assert(pr != null, controller.ErrorMessage);

        assertOneIndividual();

        List<Account> createdCompanyAccounts = getCompanyAccounts();
        system.assertEquals(1, createdCompanyAccounts.size());

        Account companyAcct = createdCompanyAccounts[0];
        system.assertEquals(companyAcct.Name, DEFAULT_LEAD_COMPANY_NAME);

        Account individualAcct = getIndividualAccounts()[0];

        assertAffiliation(individualAcct, companyAcct);
    }

    static testMethod void testConvertAssociatesIndividualWithExistingCompany() {
        setupTest();

        Account company = DataFactoryAccountExt.insertCompanyAccount();

        company.Name = DEFAULT_LEAD_COMPANY_NAME;
        company.BillingStreet = 'Main St.';
        company.BillingCity = 'Rochester';
        company.BillingState = 'NY';
        company.BillingPostalCode = '45678';

        update company;

        Integer companyCountBeforeTest = getCompanyAccounts().size();

        controller.Company = company.Id;

        PageReference pr = controller.convert();
        system.assert(pr != null, controller.ErrorMessage);

        assertOneIndividual();

        Integer companyCountAfterTest = getCompanyAccounts().size();

        system.assertEquals(companyCountAfterTest, companyCountBeforeTest);

        Account individualAcct = getIndividualAccounts()[0];
        Account companyAcct = getCompanyAccounts()[0];

        assertAffiliation(individualAcct, companyAcct);
    }

    private static void assertAffiliation(Account individualAcct, Account companyAcct) {
        List<NU__Affiliation__c> createdAffiliations = getAffiliations();

        system.assertEquals(createdAffiliations.size(), 1);

        NU__Affiliation__c affiliation = createdAffiliations[0];        
        system.assertEquals(individualAcct.Id, affiliation.NU__Account__c);
        system.assertEquals(companyAcct.Id, affiliation.NU__ParentAccount__c);
        system.assertEquals(true, affiliation.NU__IsPrimary__c);
    }

    private static void assertOneIndividual() {
        List<Account> createdIndividualAccounts = getIndividualAccounts();
        system.assertEquals(createdIndividualAccounts.size(), 1);

        Account createdAccount = createdIndividualAccounts[0];

        system.debug('Created Account: ' + createdAccount);
        system.debug('Lead: ' + Lead);
        system.assertEquals(DEFAULT_LEAD_FIRST_NAME, createdAccount.FirstName);
        system.assertEquals(DEFAULT_LEAD_LAST_NAME, createdAccount.LastName);
        system.assertEquals(DEFAULT_LEAD_EMAIL, createdAccount.PersonEmail);
        system.assertEquals(DEFAULT_LEAD_STREET, createdAccount.PersonMailingStreet);
        system.assertEquals(DEFAULT_LEAD_CITY, createdAccount.PersonMailingCity);
        system.assertEquals(DEFAULT_LEAD_STATE, createdAccount.PersonMailingState);
        system.assertEquals(DEFAULT_LEAD_POSTAL_CODE, createdAccount.PersonMailingPostalCode);
        system.assertEquals(DEFAULT_LEAD_COUNTRY, createdAccount.PersonMailingCountry);
    }

    private static void reloadLead() {
        Id id = Lead.Id;
        Lead = [SELECT FirstName,
                       LastName,
                       Email,
                       Company,
                       Phone,
                       Street,
                       City,
                       State,
                       PostalCode,
                       Country,
                       Status,
                       IsConverted,
                       ConvertedContactId,
                       ConvertedDate
                  FROM Lead
                 WHERE Id = :id];
    }

    private static List<Account> getCompanyAccounts() {
        return [SELECT Id, Name
                  FROM Account
                 WHERE IsPersonAccount = false];
    }

    private static List<Account> getIndividualAccounts() {
        return [SELECT Id,
                       FirstName,
                       LastName,
                       Phone,
                       PersonEmail,
                       PersonHomePhone,
                       PersonMailingStreet,
                       PersonMailingCity,
                       PersonMailingState,
                       PersonMailingPostalCode,
                       PersonMailingCountry
                  FROM Account
                 WHERE RecordTypeId = :AccountRecordTypeUtil.getIndividualRecordType().Id];
    }

    private static List<NU__Affiliation__c> getAffiliations() {
        return [SELECT NU__Account__c, NU__ParentAccount__c, NU__IsPrimary__c
                  FROM NU__Affiliation__c];
    }
}