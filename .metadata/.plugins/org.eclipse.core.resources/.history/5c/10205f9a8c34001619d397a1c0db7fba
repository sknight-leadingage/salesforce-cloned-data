global with sharing class AdvertisingOrderItemTypeCartSubmitter implements NU.IOrderItemTypeCartSubmitter{
    Map<Id, Advertising__c> cartItemLineAdvertisingMap;
    Map<Advertising__c, NU__OrderItemLine__c> advertisingsToUpdateMap;
    
    global NU.OperationResult beforeOrderItemSaved(NU__CartItem__c cartItem, NU__OrderItem__c orderItem) {  
        if (cartItemLineAdvertisingMap == null) {
            cartItemLineAdvertisingMap = new Map<Id, Advertising__c>();
        }
        
        List<Id> ids = new List<Id>();
        for (NU__CartItemLine__c cartItemLine : cartItem.NU__CartItemLines__r) {
            ids.add(cartItemLine.Id);
        }
        List<NU__CartItemLine__c> advertisingCartItemLines = [SELECT Id,
            NU__OrderItemLine__r.Advertising__c,
            NU__Product__c,
            NU__Data__c
            FROM NU__CartItemLine__c
            WHERE Id IN :ids];


        for (NU__CartItemLine__c cartItemLine : advertisingCartItemLines) {
            Advertising__c advertising = createAdvertisingFromCartItemLine(cartItem, cartItemLine);
            cartItemLineAdvertisingMap.put(cartItemLine.Id, advertising);
        }

        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global NU.OperationResult afterOrderItemsSaved() {

        if (cartItemLineAdvertisingMap != null && cartItemLineAdvertisingMap.isEmpty() == false) {
            System.debug(cartItemLineAdvertisingMap.values());
            upsert cartItemLineAdvertisingMap.values();
        }

        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global void beforeOrderItemLineSaved(NU__CartItemLine__c cartItemLine, NU__OrderItemLine__c orderItemLineToSave) {
        if (advertisingsToUpdateMap == null) {
            advertisingsToUpdateMap = new Map<Advertising__c, NU__OrderItemLine__c>();
        }
        
        if (cartItemLineAdvertisingMap != null && cartItemLineAdvertisingMap.containsKey(cartItemLine.Id)){
            Advertising__c advertising = cartItemLineAdvertisingMap.get(cartItemLine.Id);
            orderItemLineToSave.Advertising__c = advertising.Id;
            advertisingsToUpdateMap.put(advertising, orderItemLineToSave);
        }
    }

    global NU.OperationResult afterOrderItemLinesSaved() {

        if (advertisingsToUpdateMap != null && !advertisingsToUpdateMap.isEmpty()) {
            for (Advertising__c advertising : advertisingsToUpdateMap.keySet()) {
                advertising.Order_Item_Line__c = advertisingsToUpdateMap.get(advertising).Id;
            }
            
            List<Advertising__c> advertisingsToUpdate = new List<Advertising__c>(advertisingsToUpdateMap.keySet());
            upsert advertisingsToUpdate;
        }
        return (NU.OperationResult) NU.OperationResult.class.newInstance();
    }
    
    global void populateCartSubmissionResult(NU.CartSubmissionResult result) {
        
    }
    
    private Advertising__c createAdvertisingFromCartItemLine(NU__CartItem__c advertisingCI, NU__CartItemLine__c advertisingCIL) {
        Id advertisingId = null;
        
        if (advertisingCIL.NU__OrderItemLine__c != null) {
            advertisingId = advertisingCIL.NU__OrderItemLine__r.Advertising__c;
        }
        
        Advertising__c advertising = new Advertising__c(
                Id = advertisingId,
                Account__c = advertisingCI.NU__Customer__c,
                Product__c = advertisingCIL.NU__Product__c
            );
        
        return advertising;
    }
}