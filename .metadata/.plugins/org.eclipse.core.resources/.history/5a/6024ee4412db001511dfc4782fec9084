public with sharing class PageUtil {
	public static final string OP_MSG_LEVEL_UNSUPPORTED_FMT = 'The operation message level, {0}, is unsupported. Please implement the logic to handle it.';
	
	public static List<SelectOption> getSelectOptionsFromSObjects(List<SObject> sObjects){
		List<SelectOption> selectOpts = new List<SelectOption>();
		
		if (sObjects != null){
			for (sObject sobj : sObjects){
				String sobjName = String.valueOf(sobj.get('Name'));
				SelectOption so = new SelectOption(sobj.Id, sobjName);
				selectOpts.add(so);
			}
		}
		
		return selectOpts;
	}
	
	public static void AddResultMessagesToPage(NU.OperationResult result){
		if (result != null && result.Messages != null){
		
			for (NU.OperationMessage om : result.Messages){
				addPageMessage(om);
	    	}
		}
	}
	
	public static void addPageMessage(NU.OperationMessage opMsg){
		ApexPages.Message pageMsg = null;
		
		if (opMsg.Level == NU.OperationMessageLevel.ERROR){
			pageMsg = getErrorMessage(opMsg);
		}
		else if (opMsg.Level == NU.OperationMessageLevel.INFO){
			pageMsg = getInfoMessage(opMsg);
		}
		else if (opMsg.Level == NU.OperationMessageLevel.WARNING){
			pageMsg = getWarningMessage(opMsg);
		}
		else{
			List<string> formatParms = new List<String> { String.valueOf(opMsg.Level) };
			String unsupportedMsg = String.format(OP_MSG_LEVEL_UNSUPPORTED_FMT, formatParms);
			
			throw new ApplicationException(unsupportedMsg);
		}
	        		
		ApexPages.addMessage(pageMsg);
	}
	
	public static ApexPages.Message getErrorMessage(NU.OperationMessage opMsg){
		ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.Error,
		                                                opMsg.Message);

		return pageMsg;
	}
	
	public static ApexPages.Message getInfoMessage(NU.OperationMessage opMsg){
		ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.INFO,
		                                                opMsg.Message);

		return pageMsg;
	}
	
	public static ApexPages.Message getWarningMessage(NU.OperationMessage opMsg){
		ApexPages.Message pageMsg = new ApexPages.Message(ApexPages.Severity.WARNING,
		                                                opMsg.Message);

		return pageMsg;
	}
	
	public static ApexPages.Message createPageMessage(ApexPages.Severity severity, String msg){
		if (severity != null &&
			string.isBlank(msg) == false){
		    return new ApexPages.Message(severity, msg);
	    }
	    
	    return null;
	}
	
	public static ApexPages.Message createInfoPageMessage(String infoMessage){
		return createPageMessage(ApexPages.Severity.Info, infoMessage);
	}
	
	public static ApexPages.Message createWarningPageMessage(String warningMessage){
		return createPageMessage(ApexPages.Severity.Warning, warningMessage);
	}
	
	public static ApexPages.Message createErrorPageMessage(String errorMessage){
		return createPageMessage(ApexPages.Severity.Error, errorMessage);
	}
	
	public static void addErrorMessageToPage(String errorMsg){
		ApexPages.Message errorMsgToAdd = createErrorPageMessage(errorMsg);
		
		if (errorMsgToAdd != null){
			ApexPages.addMessage(errorMsgToAdd);
		}
	}
}