<apex:page title="Renewal Notice" renderAs="{!render}" standardController="Account" extensions="RenewalNoticeController" showHeader="false" sidebar="false" standardStylesheets="false" recordsetvar="a">
    <style type="text/css">
        body { margin: 0px; }
        img { border: 0px; }
        .ui-dialog {
        	font-size: 75%;
        }
        #buttons {
            margin: 10px;
            font-size: 75%;
            font-family: Arial, Helvetica, sans-serif;
        }
        .btn {
            padding: 4px 3px;
            color: #333;
            margin: 1px;
            border: 1px solid #B5B5B5;
            border-bottom-color: #7F7F7F;
            background: #E8E8E9 url('/img/alohaSkin/btn_sprite.png') repeat-x right top;
            font-weight: bold;
            font-size: .9em;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }
        .btn:hover {
            background-position: right -30px;
        }
        .btn:active {
            background-position: right -60px;
            border-color: #585858;
            border-bottom-color: #939393;
        }

        .message{-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px;background-color:#FFC;border-style:solid;border-width:1px;padding:6px 8px 6px 6px;margin:4px 20px}
		.errorM3{border-color:#C00}
		.errorM3 .msgIcon{background-image:url(/img/msg_icons/error24.png);background-position:0 0;width:24px;height:24px}
		.warningM3{border-color:#f90}
		.warningM3 .msgIcon{background-image:url(/img/msg_icons/warning24.png);background-position:0 0;width:24px;height:24px}
		.infoM3{border-color:#39f}
		.infoM3 .msgIcon{background-image:url(/img/msg_icons/info24.png);background-position:0 0;width:24px;height:24px}
		.confirmM3{border-color:#390}
		.confirmM3 .msgIcon{background-image:url(/img/msg_icons/confirm24.png);background-position:0 0;width:24px;height:24px}
		.message .messageText{margin-left:8px}
		.message .messageTable .messageCell{vertical-align:middle}
		.message .messageTable .messageCell h4{margin:0}
		.errorMsg{color:#d74c3b}
        
        .leftmargin { margin-left: 10px; }
        
        .ui-progressbar .ui-progressbar-value {
        	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAMAAABw8qpSAAAAPFBMVEUazhobzxsd0R0d0x0e1R4f1x8f2B8g2yAh3SEj3yMj4CMk4iQl5CUm5yYm6CYn6ico7Cgq7ior8Ssr8iue9ffCAAAAGklEQVQI12MQZmKgCvwPhDAaOwTLMTPQBgAAZr8RdC6ci8cAAAAASUVORK5CYII=);
        	border: thin solid #00aa00;
        }
        #nu-email {
        	margin-top: 20px;
        }
        #nu-progressbar {
        	width: 300px;
        	height: 15px;
        }
        .ui-dialog.pleaseWait .ui-dialog-titlebar { background: #8DC63F; border-color: #8DC63F; color: white; text-align: center; }
        .ui-dialog.pleaseWait .ui-dialog-titlebar .ui-dialog-title { margin-right: 0px;float: none; }
        
        .requiredBlock {
		    background-color: rgb(204, 0, 0);
		    width: 3px;
		}
    </style>
    <style type="text/css" media="print">
        #buttons { display:none; visibility: hidden; }
    </style>
    <apex:stylesheet value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/smoothness/jquery-ui.css" />
	<apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" />
	<apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" />
	<script type="text/javascript">
	
		var accts = [
			<apex:repeat value="{!AccountsToEmailList}" var="i">
				["{!i.Id}","{!i.Name}"],
			</apex:repeat>
		];
	
	
		function msgError(msg, bullets) {
			var html = "<div class='message errorM3'><table border='0' cellpadding='0' cellspacing='0' class='messageTable' " +
					"style='padding:0px;margin:0px;'><tr valign='top'><td><img alt='ERROR' class='msgIcon' src='/s.gif' title='ERROR'>" +
					"</td><td class='messageCell'><div class='messageText'><span style='color:#cc0000'><h4>Error:</h4></span>" +
					msg + "<br></div></td></tr>";
			if (bullets) {
				html += "<tr><td></td><td><span><ul style='padding-left:10px;padding-top:0px;margin:0px'>";
				for (var i = 0; i < bullets.length; i++) {
					html += "<li style='padding-top:5px'>" + bullets[i] + "</li>";
				}
				html += "</ul></span></td></tr>";
			}
			return html + "</table></div>";
		}
		function msgSuccess(msg) {
			return $("<div class='message confirmM3'><table border='0' cellpadding='0' cellspacing='0' class='messageTable' " +
					"style='padding:0px;margin:0px;'><tr valign='top'><td><img alt='CONFIRM' class='msgIcon' src='/s.gif' title='CONFIRM'>" +
					"</td><td class='messageCell'><div class='messageText'><span><h4>Success:</h4></span>" +
					msg + "<br></div></td></tr></table></div>");
		}
		function startCheckStatus() {
			var jobId = $("#nu-jobId").text();
			if (!jobId) { return; }
			$("#nu-messages").empty();
			$("#nu-progressbar").progressbar({value: 0});
			$("#nu-progress").show();
			checkStatus(jobId);
		}
		function checkStatus(jobId) {
			RenewalNoticeController.getJobStatus(jobId, function(result, event) {
				if (event.status) {
					if (result.Results) {
						var successes = 0;
						var errors = [];
						for (var i = 0; i < result.Results.length; i++) {
							var r = result.Results[i];
							if (r.Success__c) {
								successes++;
							} else {
								var act = accts[i];
								var error = "<a href='/" + act[0] + "'>" + act[1] + "</a> - [" + r.StatusCode__c + "] ";
								if (r.Message__c) {
									error += r.Message__c;
								}
								errors.push(error);
							}
						}
						
						$("#nu-messages").append(msgSuccess("Sent " + successes + " email(s) successfully"));
						if (errors.length) {
							$("#nu-messages").append(msgError("Failed to send " + errors.length + " emails", errors));
						}
						$("#nu-progress").hide();
					} else {
						$("#nu-progressbar").progressbar({value: result.ProcessedBatches / result.TotalBatches * 100});
						setTimeout(function() { checkStatus(jobId) }, 1000);
					}
					r = result;
				} else {
					$("#nu-messages").empty().append(msgError(event.message));
					$("#nu-progress").hide();
				}
			});
		}
	</script>
    
    <apex:outputPanel rendered="{!!RenderPDF}">
	<div id="buttons">
        <apex:form id="RenewalForm">    
            <apex:pageMessages id="msgs" escape="false" />
            <div id="nu-messages"></div>
            
            <apex:actionStatus onStart="showPleaseWait()" onStop="hidePleaseWait();startCheckStatus()" id="AS" />
                
            <apex:outputPanel id="printDialog">
                <script type="text/javascript">
                    <apex:outputText value="{!renewalDialogScript}" />
                </script>
            </apex:outputPanel>
            
            <apex:actionRegion >
		        <apex:outputLabel >Membership Renewal to Generate:&nbsp;</apex:outputLabel>
			    <span class="requiredBlock">&nbsp;</span><apex:selectList size="1" value="{!membershipRelationship}">
			        <apex:selectOption itemValue="" itemLabel="Select..."/>
			        <apex:selectOptions value="{!MembershipOptions}"/>
			    </apex:selectList><br /><br />
		        <apex:outputLabel >For Business Accounts, which primary contact should the renewal be addressed?</apex:outputLabel><br />
		        <apex:selectList size="1" value="{!affiliationRole}">
		            <apex:selectOption itemValue="" itemLabel="Select..."/>
		            <apex:selectOptions value="{!RoleOptions}"/>
		        </apex:selectList><br /><br />
		        <apex:outputLabel >Invoice Date Override:&nbsp;</apex:outputLabel>
		        <apex:inputField value="{!DummyOrder.NU__InvoiceDate__c}" /><br /><br />
		        <apex:outputLabel >Show all possible primary products?&nbsp;</apex:outputLabel>
		        <apex:inputCheckbox value="{!showAllProducts}" />
	        </apex:actionRegion>
                
            <apex:outputPanel id="RenewalNoticeActionButtons">
                <div class="buttons" style="clear: both; margin-top: 10px;">
                	<apex:commandButton value="Generate"
			           	action="{!generate}"
			           	rerender="RenewalNoticeActionButtons,RenewalComponent,msgs"
			           	styleClass="btn"
			           	title="Generates renewals based on criteria specified above." />
			        <apex:commandButton value="Cancel"
                        action="{!cancel}"
                        styleClass="btn leftmargin"
                        title="Cancel and return to the last page." /><br /><br />
	                <apex:commandButton value="PDF"
				   	    action="{!PDF}"
				   	    styleClass="btn"
				   	    rendered="{!CanRender}"
				   	    title="Adds log history on all Accounts and then rerenders the page as a PDF." />
                    <apex:commandButton value="Email"
                        action="{!Email}"
                        styleClass="btn leftmargin"
                        rendered="{!CanRender && CanEmail}"
                        rerender="RenewalForm"
                        status="AS"
                        title="Adds log history on all Accounts and then emails the renewal notice to the system Invoice Contact along with the primary contact of the Account." />
                    <apex:commandButton value="Print"
                        action="{!Print}"
                        styleClass="btn leftmargin"
                        rendered="{!CanRender}"
                        rerender="RenewalForm"
                        title="Adds log history on all Accounts and opens dialog window to print the renewal notices." />
                    <apex:commandButton value="Bill Accounts"
                        action="{!Bill}"
                        styleClass="btn leftmargin"
                        rendered="{!CanRender}"
                        rerender="RenewalForm"
                        title="Marks the accounts as billed for their membership renewals."
                        onClick="if(!confirm('Are you sure? Membership billing line records will be created and/or updated.')) return false;" />
                </div>
            </apex:outputPanel>
            
            <div id="nu-email">
				<span id="nu-jobId" style="display:none">{!BatchEmailJobId}</span>
				<div id="nu-progress">
					Sending emails...
					<div id="nu-progressbar"></div>
				</div>
				<table id="nu-results"></table>
				<script type="text/javascript">
						$("#nu-progress").hide()
						$("#nu-results").hide();
				</script>
			</div>
        </apex:form>
        <hr/>
        
        <div id="pleaseWaitModal" class="pleaseWaitModalHidden" title="Please wait...">
	        <div style="text-align: center;">
	            <apex:image value="{!URLFOR($Resource.NU__Loader)}" />
	        </div>
	    </div>
	    <script type="text/javascript">
		    $('#pleaseWaitModal').dialog({
		        width: 155, 
		        modal: true,
		        autoOpen: false,
		        draggable: false,
		        resizable: false,
		        closeOnEscape: false,
		        dialogClass: 'pleaseWait',
		        open: function(event, ui) { $('.ui-dialog-titlebar-close').hide(); $('#pleaseWaitModal').removeClass('pleaseWaitModalHidden'); }
		    });
		    function showPleaseWait() {
				$('#pleaseWaitModal').dialog('open');
		    }
		    function hidePleaseWait() {
				$('#pleaseWaitModal').dialog('close');
		    }
	    </script>
    </div>
    </apex:outputPanel>

	<c:ViewRenewalNotice id="RenewalComponent" accts="{!IF(CanRender,Accounts,null)}" />
</apex:page>