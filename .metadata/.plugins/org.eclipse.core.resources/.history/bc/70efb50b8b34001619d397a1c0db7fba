trigger DealTrigger on NU__Deal__c (before update, before insert, after insert) {
    List<NU__Deal__c> deals = Trigger.new;
    Map<Id, Schema.RecordTypeInfo> rt = Schema.SObjectType.NU__Deal__c.getRecordTypeInfosById();
    
    List<Id> accountIds = new List<Id>();
    for (NU__Deal__c deal : deals) {
        if (deal.NU__BillTo__c != null) {
            accountIds.add(deal.NU__BillTo__c);
        }
    }

    Map<Id, Account> accounts = new Map<Id, Account>([SELECT Id,
        NU__RecordTypeName__c//,
        //NU__MemberThru__c
        FROM Account
        WHERE Id IN :accountIds]);
    
    if (Trigger.isBefore) {
        for (NU__Deal__c deal : deals) {
            String recordType = rt.get(deal.RecordTypeId).getName();
            
            Account billTo;
            if (deal.NU__BillTo__c != null) {
                billTo = accounts.get(deal.NU__BillTo__c);
            }
            
            if (recordType == 'Membership' || recordType == 'Subscription') { // need to do validation on start date and end dates, as well as the member type
                if (recordType == 'Membership') {
                    if (deal.Membership_Type__c == null) {
                        deal.Membership_Type__c.addError('The Membership Type field is required.');
                    }
                }
                if (deal.NU__StartDate__c != null && deal.NU__EndDate__c != null) {
                    if (deal.NU__StartDate__c > deal.NU__EndDate__c) {
                        deal.addError('The Start Date must be before the End Date.');
                    }
                }
                if (deal.NU__StartDate__c != null) {
                    Date startDate = deal.NU__StartDate__c;
                    if (startDate != startDate.toStartOfMonth()) {
                        deal.NU__StartDate__c.addError('The Start Date must be the first day of the month.');
                    } // TODO: should be looking at the correct Member Thru field depending on the membership field
                    /*else if (recordType == 'Membership' && billTo != null && deal.Order__c == null && startDate < billTo.NU__MemberThru__c) {
                        deal.NU__StartDate__c.addError('The Start Date overlaps with an existing membership.');
                    }*/
                }
                if (deal.NU__EndDate__c != null) {
                    Date endDate = deal.NU__EndDate__c;
                    Integer daysInMonth = Date.daysInMonth(endDate.year(), endDate.month()); 
                    if (endDate.day() != daysInMonth) {
                        deal.NU__EndDate__c.addError('The End Date must be the last day of the month.');
                    } // TODO: should be looking at the correct Member Thru field depending on the membership field
                    /*else if (recordType == 'Membership' && billTo != null && deal.Order__c == null && endDate < billTo.NU__MemberThru__c) {
                        deal.NU__EndDate__c.addError('The End Date overlaps with an existing membership.');
                    }*/
                }
                
            }
        }
        
        // If an 
        if (Trigger.isUpdate){
        	List<NU__Deal__c> dealsWithChangedEvents = new List<NU__Deal__c>();
        	Set<Id> newEventIds = new Set<Id>();
        	
        	for (NU__Deal__c oldDeal : Trigger.Old){
        		NU__Deal__c newDeal = Trigger.newMap.get(oldDeal.Id);
        		
        		if (oldDeal.Event__c != newDeal.Event__c &&
        		    newDeal.Event__c != null){
        		    dealsWithChangedEvents.add(newDeal);
        		    newEventIds.add(newDeal.Event__c);
    		    }
        	}
        	
        	if (dealsWithChangedEvents.isEmpty()){
        		return;
        	}
        	
        	Map<Id, NU__Deal__c> dealsMap = new Map<Id, NU__Deal__c>(
        		[select id,
        		        (Select id,
        		                name,
        		                Product__r.NU__Entity__c
        		           from Deal_Items__r)
        		   from NU__Deal__c
        		  where id in :dealsWithChangedEvents]
        	);
        	
        	Map<Id, NU__Event__c> newEventsMap = new Map<Id, NU__Event__c>(
        		[select id,
        		        name,
        		        NU__Entity__c
        		   from NU__Event__c
        		  where id in :newEventIds]
        	);
        	
        	for (NU__Deal__c dealWithChangedEvent : dealsWithChangedEvents){
        		NU__Deal__c dealWithChangedEventQueried = dealsMap.get(dealWithChangedEvent.Id);
        		
        		if (dealWithChangedEventQueried.Deal_Items__r != null && dealWithChangedEventQueried.Deal_Items__r.size() > 0){
        			NU__Event__c newEvent = newEventsMap.get(dealWithChangedEvent.Event__c);
        			Id dealItemEntity = dealWithChangedEventQueried.Deal_Items__r[0].Product__r.NU__Entity__c;
        			
        			if (newEvent.NU__Entity__c != dealItemEntity){
        				dealWithChangedEvent.addError(Constant.DEAL_EVENT_ENTITY_DIFFERENT_FROM_DEAL_ITEMS_ENTITY_VAL_MSG);
        			}
        		}
        	}
        }
    }
}