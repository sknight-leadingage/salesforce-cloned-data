public with sharing class ARStatementUtil {
	public ARStatementUtil() {}
	
	private static ARStatementUtil m_Instance;
    public static ARStatementUtil Instance {
        get {
            if (m_Instance == null) {
                m_Instance = new ARStatementUtil();
            }
            return m_Instance;
            
        }
    }

    public static Date invoiceDateOverride = null;
	public static Decimal minimumBalanceOverride = 0.00;
	public static Id entityId = null;
	
	/* Of the list of Accounts passed in, determine which ones are valid. Valid accounts must have an AR balance greater than the minimum balance specified
	 * for the selected Entity. */
	private Set<Id> accountIds = null;
	public List<Account> getValidAccounts(List<Account> accountsToValidate) {
		List<Account> validAccounts = new List<Account>();
		Map<Id,Account> mapAccounts = new Map<Id,Account>();
		mapAccounts.putAll(accountsToValidate);
		
		accountIds = mapAccounts.keySet();
		
		// get all active orders for the specified accounts that meet the criteria
		Map<Object, List<NU__Order__c>> ordersByAccountMap = OrdersByAccountMap();
		
		for (Account account : accountsToValidate) {
			// if the account has active orders, then it is a valid account 
			if (ordersByAccountMap.get(account.Id) != null && ordersByAccountMap.get(account.Id).size() > 0) {
				validAccounts.add(account);
			}
		}
		
		return validAccounts;		
	}
	
	/* Returns a map of Orders, grouped by Account Id */
	private Map<Object, List<NU__Order__c>> privOrdersByAccountMap = null;
	public Map<Object, List<NU__Order__c>> OrdersByAccountMap() {
		if ((privOrdersByAccountMap == null || privOrdersByAccountMap.size() == 0) && accountIds != null) {
			privOrdersByAccountMap = NU.CollectionUtil.groupSObjectsByField(ordersQuery(accountIds), 'NU__BillTo__c');
		}
		
		return privOrdersByAccountMap;
	}
	
	/* Executes a SOQL query and returns a list of active Orders for the specified Accounts 
	 * that have an AR balance greater than the minimum balance specified for the selected Entity. */
	private List<NU__Order__c> privOrders = null;
	private List<NU__Order__c> ordersQuery(Set<Id> accountIds) {
		privOrders = new List<NU__Order__c>();
		
		if (entityId != null) {
			String queryString = 'SELECT Id, NU__GrandTotal__c, NU__Balance__c, NU__TotalPayment__c, NU__BillTo__c, NU__BillTo__r.Name, ' + 
				'NU__BillTo__r.BillingStreet, NU__BillTo__r.BillingCity, NU__BillTo__r.BillingState, NU__BillTo__r.BillingPostalCode, NU__BillTo__r.BillingCountry, ' +
				'NU__BillTo__r.LeadingAge_Id__c, NU__InvoiceARAging__c, NU__InvoiceNumber__c, NU__InvoiceDescription__c, NU__TransactionDate__c ' +
				'FROM NU__Order__c WHERE NU__Entity__c = \'' + entityId + '\' AND NU__BillTo__c IN :accountIds AND NU__Balance__c ' + (minimumBalanceOverride == 0.0 ? '>' : '>=') + ' ' + minimumBalanceOverride +
				' AND NU__Status__c != \'' + Constant.ORDER_STATUS_CANCELLED + '\' ORDER BY NU__InvoiceNumber__c DESC';
			
			privOrders = Database.query(queryString);
		}
		
		return privOrders;
	}
	
	/* Returns a map of Order Items, grouped by Order Id */
	private Map<Object, List<NU__OrderItem__c>> privOrderItemsByOrderMap = null;
	public Map<Object, List<NU__OrderItem__c>> OrderItemsByOrderMap() {
		if ((privOrderItemsByOrderMap == null || privOrderItemsByOrderMap.size() == 0) && privOrders != null) {
			Map<Id,NU__Order__c> ordersMap = new Map<Id,NU__Order__c>();
			ordersMap.putAll(privOrders);
			
			privOrderItemsByOrderMap = NU.CollectionUtil.groupSObjectsByField(orderItemsQuery(ordersMap.keySet()), 'NU__Order__c');
		}
		
		return privOrderItemsByOrderMap;
	}
	
	/* Executes a SOQL query and returns a list of active Order Items with Order Item Lines for the specified Orders. */
	private List<NU__OrderItem__c> orderItemsQuery(Set<Id> orderIds) {
		List<NU__OrderItem__c> orderItems = new List<NU__OrderItem__c>();
		
		String queryString = 'SELECT Id, NU__Order__c, RecordType.Name, NU__GrandTotal__c, NU__Balance__c, NU__TotalPayment__c, ' +
				'(SELECT Id, NU__TotalPrice__c, NU__Product__c, NU__Product__r.Name FROM NU__OrderItemLines__r WHERE NU__Status__c != \'' + Constant.ORDER_ITEM_LINE_STATUS_CANCELLED + '\') ' +
			'FROM NU__OrderItem__c WHERE NU__Status__c != \'' + Constant.ORDER_ITEM_LINE_STATUS_CANCELLED + '\' AND NU__Order__c IN :orderIds';
			
			orderItems = Database.query(queryString);
		
		return orderItems;
	}
	
	/* Returns Entity information based on the Entity Id that is set. */
	public NU__Entity__c Entity {
		get {
			if (entityId != null) {
				if (entities == null) {
		    		entities = entityQuery();
		    	}
		    	return entities.get(entityId);
			}
			else {
				return null;
			}
		}	
	}
	
	/* Returns a list of AR Agings based on the Entity Id that is set. */
	private Map<Id,List<NU__ARAging__c>> privARAgings = null;
	public List<NU__ARAging__c> EntityARAgings {
		get {
			if (privARAgings == null) {
		        privARAgings = new Map<Id,List<NU__ARAging__c>>();
		        
		        if (entities == null) {
		    		entities = entityQuery();
		    	}
		    	
		    	for (NU__Entity__c entity : entities.values()) {
		    		privARAgings.put(entity.Id, entity.NU__ARAgings__r);
		    	}
	    	}
		    
		    if (entityId != null) {
		    	return privARAgings.get(entityId);
		    }
	
	        return new List<NU__ARAging__c>();
		}
    }
	
	/* Returns a list of Select Options to display to the user to select which Entity the AR Statements should be generated. */
	private List<SelectOption> privEntityOptions;
    public List<SelectOption> EntityOptions {
    	get {
    		if (privEntityOptions == null) {
    			privEntityOptions = calculateEntityOptions();
    		}
    		return privEntityOptions;
    	}
    }
    
    /* Builds and returns a list of Select Options, mapping the Entity Name to the Entity Id. */
    private Map<Id,NU__Entity__c> entities = null;
    private List<SelectOption> calculateEntityOptions() {
    	if (entities == null) {
    		entities = entityQuery();
    	}
    	
    	Set<SelectOption> setOptions = new Set<SelectOption>();
		for (NU__Entity__c entity : entities.values()) {
			setOptions.add(new SelectOption(entity.Id, entity.Name));
		}
		
		List<SelectOption> options = new List<SelectOption>(setOptions);
		options.sort();
		
		return options;
    }
    
    /* Executes a SOQL query and a returns a map of active Entity information, as well as related AR Aging information. */
    private Map<Id,NU__Entity__c> entityQuery() {
		return new Map<Id,NU__Entity__c>([SELECT
	            Id,
	            Name,
	            NU__LogoURL__c,
	            NU__Street__c,
	            NU__City__c,
	            NU__State__c,
	            NU__PostalCode__c,
	            NU__Country__c,
	            NU__Phone__c,
	            NU__Website__c,
	            (SELECT Id, Name, NU__Message__c FROM NU__ARAgings__r ORDER BY NU__Days2__c ASC)
	            FROM NU__Entity__c
	            WHERE NU__Status__c = :Constant.ENTITY_ACTIVE]);
	}
}