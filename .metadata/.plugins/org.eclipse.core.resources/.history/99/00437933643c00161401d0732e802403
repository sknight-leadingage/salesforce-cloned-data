/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestConferenceProposalsController {

    static Conference_Proposal__c proposal = null;
    
    static ConferenceProposalsController loadController(){
        PageReference pageRef = Page.ConferenceProposals;
        Test.setCurrentPage(pageRef);
        
        proposal = DataFactoryConferenceProposal.insertConferenceProposal();
        
        pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, proposal.Conference__c);
        
        ConferenceProposalsController proposalController = new ConferenceProposalsController();
        
        return proposalController;
    }

    static testMethod void CurrentProposalTest() {
        ConferenceProposalsController proposalController = loadController();
        
        system.assert(proposalController.CurrentProposal != null);
    }
    
    static testMethod void proposalHistoryTest() {
        ConferenceProposalsController proposalController = loadController();
        
        system.assert(proposalController.ProposalHistory != null);
    }
    
    static testMethod void statusChangesNotNullTest() {
        ConferenceProposalsController proposalController = loadController();
        
        system.assert(proposalController.StatusChanges != null);
    }
    
    static testMethod void statusChangesCountNotNullTest() {
        ConferenceProposalsController proposalController = loadController();
        
        system.assert(proposalController.StatusChangesCount != null);
    }
    
    static testMethod void trackChangesCountNotNullTest() {
        ConferenceProposalsController proposalController = loadController();
        
        system.assert(proposalController.TrackChangesCount != null);
    }
    
    static testmethod void titleSearchTest(){
        ConferenceProposalsController proposalController = loadController();
        proposalController.SearchCriteria.Title = proposalController.CurrentProposal.Title__c;
        
        proposalController.search();
        
        system.assert(proposalController.CurrentProposal != null);
    }
    
    static testmethod void clearSearchTest(){
        ConferenceProposalsController proposalController = loadController();
        proposalController.SearchCriteria.Title = proposalController.CurrentProposal.Title__c;
        
        system.assert(proposalController.HasSearchCriteria == true);
        
        proposalController.clearSearch();
        
        system.assert(proposalController.HasSearchCriteria == false);
    }
    
    static testmethod void convertToSession(){
        ConferenceProposalsController proposalController = loadController();
        proposalController.ConvertToSession();
    }
    
    static testmethod void exerciseGoToRecord(){
        ConferenceProposalsController proposalController = loadController();
        proposalController.RecordNum = 1;
        system.assertEquals(1, proposalController.RecordNum);
        proposalController.GoToRecord();
    }
    
    static testmethod void exercise_EduLinkSpeaker_Controller() {
        PageReference pageRef = Page.EduLinkSpeaker;
        Test.setCurrentPage(pageRef);
        proposal = DataFactoryConferenceProposal.insertConferenceProposal();
        pageRef.getParameters().put(ConferenceManagementControllerBase.EVENT_ID_QUERY_STRING_NAME, proposal.Conference__c);
        //Conference_Proposal_Speaker__c psid = [SELECT Id FROM Conference_Proposal_Speaker__c WHERE Speaker__r.Account__c = :proposal.Submitter__c LIMIT 1];
        
        Conference_Proposal_Speaker__c psid = DataFactoryConferenceProposalSpeaker.insertConferenceProposalSpeaker(proposal.Id, proposal.Submitter__c);
        
        pageRef.getParameters().put('ps', psid.Id); //<--- Conference_Proposal_Speaker__c ID not account!

        EduLinkSpeaker_Controller ccc = new EduLinkSpeaker_Controller();
        
        string s = ccc.getPropSpkr();
        system.assertEquals(s, ccc.getPropSpkr());
        
        List<SObject> b = ccc.getConferenceObjects();
        
        if (b == null) {
            system.assertEquals(null, b);
        } else {
            integer blistsize = b.size();
            system.assertEquals(blistsize, b.size());
        }
 
        Conference_Proposal_Speaker__c c = ccc.ProposalSpeaker;
        
        List<Account> d = ccc.getMatchedSpeakerList();

        ccc.LeadingAgeManualLinkID = c.Speaker__r.Account__r.LeadingAge_ID__c;
        ccc.ManuallyLinkID();
                
        ccc.setPropSpkr('bogus');
    }
}