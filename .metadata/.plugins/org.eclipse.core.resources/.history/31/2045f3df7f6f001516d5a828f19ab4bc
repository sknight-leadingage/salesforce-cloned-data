public with sharing class InvoiceBatchEmailSender implements Database.Batchable<Email>, Database.Stateful {
	
	private static final String ASYNC_JOB_COMPLETED = 'Completed';

	private List<Email> emailList;
	private Integer index = 0;

	public static Id Send(List<Email> emails) {
		Integer batchSize = Limits.getLimitEmailInvocations();
		return Database.executeBatch(new InvoiceBatchEmailSender(emails), batchSize);
	}

	private InvoiceBatchEmailSender(List<Email> emails) {
		emailList = emails;
	}

	public Iterable<Email> start(Database.BatchableContext context) {
		return emailList;
	}
	
	public void execute(Database.BatchableContext context, List<Email> messages) {
		List<Messaging.Email> emails = new List<Messaging.Email>();
		for (Email email : messages) {
			emails.add(email.getEmail());
		}
		
		Id jobId = context.getJobId();
		List<SendEmailResult__c> results = new list<SendEmailResult__c>();
		
		for (Messaging.SendEmailResult result : Messaging.sendEmail(emails, false)) {
			SendEmailResult__c temp = new SendEmailResult__c(
					Index__c = index++,
					JobId__c = jobId,
					Success__c = result.Success);
			if (!result.Success) {
				temp.Message__c = result.Errors[0].Message;
				temp.StatusCode__c = String.valueOf(result.Errors[0].StatusCode);
			}
			results.add(temp);
		}
		
		insert results;
	}
	
	public void finish(Database.BatchableContext context) {
	}
	
	public static JobStatus getJobStatus(Id jobId) {
		if (jobId == null) {
			return null;
		}
		AsyncApexJob job = [select
				CompletedDate,
				Status,
				TotalJobItems,
				JobItemsProcessed
				from AsyncApexJob
				where Id = :jobId];
		if (job != null) {
			List<SendEmailResult__c> results = null;
			if (job.CompletedDate != null) {
				results = [select
						Success__c,
						Message__c,
						StatusCode__c
						from SendEmailResult__c
						where JobId__c = :jobId
						order by Index__c];
				delete results;
			}
			return new JobStatus(results, job.JobItemsProcessed, job.TotalJobItems);
		}
		return null;
	}
	
	@future
	private static void deleteOldResults() {
		delete [select Id from SendEmailResult__c where CreatedDate <= :DateTime.now().addDays(-1) limit :Limits.getLimitDMLRows()];
	}
	
	public static void checkOldResults() {
		if ([select Id from SendEmailResult__c where CreatedDate <= :DateTime.now().addDays(-1) limit 1].size() > 0) {
			deleteOldResults();
		}
	}
	
	public class JobStatus
	{
		public JobStatus(List<SendEmailResult__c> results, Integer processedBatches, Integer totalBatches) {
			this.Results = results;
			this.ProcessedBatches = processedBatches;
			this.TotalBatches = totalBatches;
		}
		
		public List<SendEmailResult__c> Results { get; set; }
		public Integer ProcessedBatches { get; set; }
		public Integer TotalBatches { get; set; }
	}
}