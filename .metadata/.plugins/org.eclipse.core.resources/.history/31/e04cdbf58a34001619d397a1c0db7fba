public with sharing class OrderPurchaseExhibitor extends OrderPurchaseBase {
	public override String getRecordType(){
    	return Constant.ORDER_RECORD_TYPE_EXHIBITOR;
    }
    
    public Id ExhibitorEventId { get; set; }
    
    public NU__Event__c selectedEvent {
    	get{
    		Map<Id, NU__Event__c> exhibitorEventsMap = new Map<Id, NU__Event__c>( ExhibitorEvents );
    		
    		return ExhibitorEventId != null ? exhibitorEventsMap.get(ExhibitorEventId) : null;
    	}
    }
    
    private List<NU__Event__c> exhibitorEventsPriv = null;
    public List<NU__Event__c> ExhibitorEvents {
    	get {
    		if (exhibitorEventsPriv == null){
    			exhibitorEventsPriv = 
    			[select id,
    			        name
    			   from NU__Event__c
    			  where id in (select Event__c
    			                 from NU__Product__c
    			                where RecordType.Name = :Constant.ORDER_RECORD_TYPE_EXHIBITOR
    			                  and NU__Status__c = :Constant.ORDER_ITEM_LINE_STATUS_ACTIVE)
    			  order by name];
    		}
    		
    		return exhibitorEventsPriv;
    	}
    }
    
    public List<SelectOption> ExhibitorEventSelectOpts {
    	get { return PageUtil.getSelectOptionsFromSObjects(ExhibitorEvents); }
    }
    
    protected List<NU__CartItemLine__c> CurrentSavedCartItemLines { get; set; }
    
    public OrderPurchaseExhibitor(){
    	populateCurrentSavedCartItemLines();
    	
    	if (NU.CollectionUtil.listHasElements(CurrentSavedCartItemLines)){
    		ExhibitorEventId = CurrentSavedCartItemLines[0].NU__Product__r.Event__c;
    	}
    }
    
    private void populateCurrentSavedCartItemLines(){
    	CurrentSavedCartItemLines = 
    	    [SELECT Id,
                    NU__Product__c,
                    NU__Product__r.Event__c,
                    NU__CartItem__c,
                    NU__Product__r.NU__TrackInventory__c,
                    NU__Product__r.NU__InventoryOnHand__c,
                    NU__Quantity__c,
                    NU__IsInCart__c,
                    NU__UnitPrice__c,
                    NU__Data__c,
                    Booth_Number__c,
                    Booked_By__c,
                    Booked_Date__c,
                    NU__OrderItemLine__r.Exhibitor__r.Booked_By__c,
                    NU__OrderItemLine__r.Exhibitor__r.Booked_Date__c,
                    NU__OrderItemLine__r.Exhibitor__r.Booth_Number__c
                    FROM NU__CartItemLine__c
                    WHERE NU__CartItem__r.Id = :CartItem.Id];
    }
    
    public void onEventSelected(){
    	ReloadProductItemLines();
    }
    
    public override List<NU__Product__c> getActiveEntityOrderTypeProducts(){
    	return [SELECT id,
    				   name,
    				   NU__TrackInventory__c,
                	   NU__InventoryOnHand__c
                  FROM NU__Product__c
                 WHERE NU__Status__c = 'Active'
                   AND NU__Entity__c = :Cart.NU__Entity__c
                   AND Event__c = :ExhibitorEventId
                   AND RecordType.Name = :getRecordType()];
    }
    
    public override List<NU__CartItemLine__c> getProductItemLines(){
    	List<NU__Product__c> products = new List<NU__Product__c>();
        List<NU__CartItemLine__c> allProductItemLines = new List<NU__CartItemLine__c>();
        
        if (productItemLines != null) {
            return productItemLines;
        }
        
        if (CartItem.Id != null || (ExhibitorEventId != null && CartItem.NU__PriceClass__c != null && productItemLines == null)) {
            products = getActiveEntityOrderTypeProducts();
            allProductItemLines = getProductPricing(products);
            
            if (CartItem.Id != null) { // editing cart item
            	populateCurrentSavedCartItemLines();
                
                Integer i = 0;  
                for (NU__CartItemLine__c product : allProductItemLines) {
                    for (NU__CartItemLine__c currentIL : CurrentSavedCartItemLines) {
                        if (product.NU__Product__c == currentIL.NU__Product__c) {
                        	if (currentIL.NU__OrderItemLine__r.Exhibitor__r.Booked_By__c != null &&
                        	    currentIL.Booked_By__c == null){
                        	    currentIL.Booked_By__c = currentIL.NU__OrderItemLine__r.Exhibitor__r.Booked_By__c;
                    	    }
                    	    
                    	    if (currentIL.NU__OrderItemLine__r.Exhibitor__r.Booked_Date__c != null &&
                        	    currentIL.Booked_Date__c == null){
                        	    currentIL.Booked_Date__c = currentIL.NU__OrderItemLine__r.Exhibitor__r.Booked_Date__c;
                    	    }
                    	    
                    	    if (currentIL.NU__OrderItemLine__r.Exhibitor__r.Booth_Number__c != null &&
                        	    currentIL.Booth_Number__c == null){
                        	    currentIL.Booth_Number__c = currentIL.NU__OrderItemLine__r.Exhibitor__r.Booth_Number__c;
                    	    }
                        	
                            allProductItemLines.set(i, currentIL);
                        } 
                    }
                    i++;
                }
            }
            
            productItemLines = allProductItemLines;
        }
        
        return productItemLines;
    }
}