global with sharing class CalculateMembershipBillingTotals implements Database.Batchable<sObject>, Schedulable {
    
    public static String CRON_EXP = '0 0 2 * * ?';
    
    // for specifying the Membership Billing Id's to query - set when MembershipBilling.Bill() is run, to only update the membership billing records where MSL's have been added
    public Set<Id> membershipBillingIds;
    
    public CalculateMembershipBillingTotals() { }
    
    public CalculateMembershipBillingTotals(Set<Id> ids) {
        membershipBillingIds = ids;
    }
    
    public CalculateMembershipBillingTotals(Set<Object> objs) {
        if (membershipBillingIds == null) {
            membershipBillingIds = new Set<Id>();
        }
        
        for (Object obj : objs) {
            membershipBillingIds.add((Id)obj);
        }
    }
    
    public Id recordId;
    public CalculateMembershipBillingTotals(ApexPages.StandardController controller) {
        recordId = controller.getId();
    }

    global static String schedule() {
        return System.schedule('Calculate Membership Billing Totals', CRON_EXP, new CalculateMembershipBillingTotals());
    }
    
    global void execute(SchedulableContext context) {
        Database.executeBatch(new CalculateMembershipBillingTotals(), 1);
    }

    global Database.QueryLocator start(Database.BatchableContext context) {
        if (membershipBillingIds != null && membershipBillingIds.size() > 0) {
            return Database.getQueryLocator(MembershipBilling.MEMBERSHIP_BILLINGS_QUERY + ' AND Id IN :membershipBillingIds');
        }
        return Database.getQueryLocator(MembershipBilling.MEMBERSHIP_BILLINGS_QUERY);
    }
    
    global void execute(Database.BatchableContext context, List<MembershipBilling__c> membershipBillings) {
        if (membershipBillings.size() > 0) {
            Calculate(membershipBillings.get(0));
        }
    }
    
    global void finish(Database.BatchableContext context) {
    	if (Test.isRunningTest() == false){
			BatchApexUtil.sendErrorEmailWhenJobFinishesWithErrors( context.getJobId() );
		}
    }
    
    private static Decimal amountBilled; // for renewing/billed memberships only
    private static Decimal duesAmountReceived; // for renewing/billed memberships only
    private static Integer membershipsBilled; // for renewing/billed memberships only
    private static Integer membershipsRenewed; // for renewing/billed memberships only
    
    private static Decimal donationAmountReceived;
    private static Decimal totalAmountReceived;
    private static Integer totalMemberships;
    
    public PageReference DoCalculate() {
        if (recordId != null) {
            List<MembershipBilling__c> membershipBillings = [SELECT Id, Year__c, MembershipType__c, AmountBilled__c, DonationAmountReceived__c, DuesAmountReceived__c, MembershipsBilled__c, MembershipsRenewed__c, TotalAmountReceived__c, TotalMemberships__c FROM MembershipBilling__c WHERE Id = :recordId AND DoNotCalculateTotals__c = FALSE];
            
            if (membershipBillings.size() > 0) {
                Calculate(membershipBillings.get(0));
            }
            return new PageReference('/' + (String)recordId);
        }
        return new PageReference('/home/home.jsp');
    }
    
    public static void Calculate(MembershipBilling__c membershipBilling) {
        MembershipBilling__c membershipBillingToUpdate = new MembershipBilling__c(Id = membershipBilling.Id, Year__c = membershipBilling.Year__c, MembershipType__c = membershipBilling.MembershipType__c);
        
        // get all membership billing lines for the membership billing
        List<MembershipBillingLine__c> membershipBillingLines = [SELECT Id, 
            AmountBilled__c, 
            DonationAmountReceived__c, 
            DuesAmountReceived__c,
            DonationOrderItemLine__c,
            MembershipOrderItemLine__c,
            MembershipRetentionStatus__c
            FROM MembershipBillingLine__c 
            WHERE MembershipBilling__c = :membershipBilling.Id];
        
        membershipBillingToUpdate = membershipBillingLinesRollup(membershipBillingLines, membershipBillingToUpdate);
            
        // perform check - only update if any values have changed
        if (valuesChanged(membershipBilling, membershipBillingToUpdate)) {
            update membershipBillingToUpdate;
        }
    }
    
    private static MembershipBilling__c membershipBillingLinesRollup(List<MembershipBillingLine__c> membershipBillingLines, MembershipBilling__c membershipBillingToUpdate) {
        amountBilled = 0.0;
        duesAmountReceived = 0.0;
        membershipsBilled = 0;
        membershipsRenewed = 0;
        
        donationAmountReceived = 0.0;
        totalAmountReceived = 0.0;
        totalMemberships = 0;
    
        for (MembershipBillingLine__c mbl : membershipBillingLines) {
            // is there a donation?
            if (mbl.DonationOrderItemLine__c != null) {
                // increment the total donation amount
                donationAmountReceived += mbl.DonationAmountReceived__c;
            }
            
            // is there a membership?
            if (mbl.MembershipOrderItemLine__c != null) {
                // increment the number of new/renewing memberships
                totalMemberships++;
                
                // increment the total membership amount
                totalAmountReceived += mbl.DuesAmountReceived__c;
                
                // is the membership a renewal?
                if (mbl.MembershipRetentionStatus__c == Constant.MEMBERSHIP_RETENTION_RENEWING) {
                    membershipsRenewed++;
                    duesAmountReceived += mbl.DuesAmountReceived__c;
                }
            }
            
            // is it a billed/renewing billing line?
            if (mbl.MembershipRetentionStatus__c != Constant.MEMBERSHIP_RETENTION_NEW) {
                // increment the number of renewing memberships billed
                membershipsBilled++;
                                
                // increment the amount billed
                amountBilled += mbl.AmountBilled__c;
            }
        }
        
        // set the values
        membershipBillingToUpdate.AmountBilled__c = amountBilled;
        membershipBillingToUpdate.DonationAmountReceived__c = donationAmountReceived;
        membershipBillingToUpdate.DuesAmountReceived__c = duesAmountReceived;
        membershipBillingToUpdate.MembershipsBilled__c = membershipsBilled;
        membershipBillingToUpdate.MembershipsRenewed__c = membershipsRenewed;
        membershipBillingToUpdate.TotalMemberships__c = totalMemberships;
        membershipBillingToUpdate.TotalAmountReceived__c = totalAmountReceived;
        
        return membershipBillingToUpdate;
    }
    
    private static Boolean valuesChanged(MembershipBilling__c oldMs, MembershipBilling__c newMs) {
        return ((newMs.AmountBilled__c != oldMs.AmountBilled__c)
            || (newMs.DonationAmountReceived__c != oldMs.DonationAmountReceived__c)
            || (newMs.DuesAmountReceived__c != oldMs.DuesAmountReceived__c)
            || (newMs.MembershipsBilled__c != oldMs.MembershipsBilled__c)
            || (newMs.MembershipsRenewed__c != oldMs.MembershipsRenewed__c)
            || (newMs.TotalAmountReceived__c != oldMs.TotalAmountReceived__c)
            || (newMs.TotalMemberships__c != oldMs.TotalMemberships__c));
    }
}