/* This batch job updates all the affiliations' Parent State Partner Name and
   Grandparent State Partner Name field daily so that they remain updated after
   the affiliation records are created.
 */

global class BatchUpdateAffiliationStateNames implements Database.Batchable<SObject>, Schedulable {
	
   global static void scheduleAt5AM() {
      System.schedule('Batch Update Affiliation State Names', '0 0 5 * * ?', new BatchUpdateAffiliationStateNames());
   }
	
   global void execute(SchedulableContext context) {
      Database.executeBatch(new BatchUpdateAffiliationStateNames());
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(
          [SELECT Id,
          		  NU__Account__c,
          		  NU__Account__r.State_Partner_Permissions__c,
          		  State_Partner_Permissions__c,
                  NU__ParentAccount__r.State_Partner_Id__r.Name,
                  NU__ParentAccount__r.State_Partner_Permissions__c,
                  NU__ParentAccount__r.NU__PrimaryAffiliation__r.State_Partner_Id__r.Name,
                  NU__ParentAccount__r.NU__PrimaryAffiliation__r.State_Partner_Permissions__c
             FROM NU__Affiliation__c
            WHERE Update_State_Names_Needed__c = true]);
   }

   global void execute(Database.BatchableContext BC, List<NU__Affiliation__c> affiliations){
   	  NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTrigger');
      NU.TriggerHandlerManager.disableTriggerForThisRequest('AffiliationTriggers');
   	
      for(NU__Affiliation__c affil : affiliations){
      	
      	if(affil.NU__Account__c != null)
          affil.State_Partner_Permissions__c = affil.NU__Account__r.State_Partner_Permissions__c;
          
      }
      
      update affiliations;
   }

   global void finish(Database.BatchableContext BC){

   }
}