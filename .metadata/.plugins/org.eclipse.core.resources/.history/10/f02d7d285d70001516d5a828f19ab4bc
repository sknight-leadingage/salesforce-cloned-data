/**
 * @author NimbleUser
 * @date Updated: 2/6/13
 * @description This class has various tests to ensure the multisite membership pricing works.
 */
@isTest
private class TestMultiSiteMembershipPricing {

    static testMethod void overridePriceTest() {
        Account multiSiteAccount = DataFactoryAccountExt.createMultiSiteAccount(800000);
        multiSiteAccount.Dues_Price_Override__c = 2000;
        
        insert multiSiteAccount;
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, multiSiteAccount.Dues_Price_Override__c);
    }
    
    static testmethod void trialMembershipNotApplicableTest(){    	
    	Account multiSiteAccount = DataFactoryAccountExt.createMultiSiteAccount(800000);
        multiSiteAccount.Trial_Membership__c = true;
        
        insert multiSiteAccount;
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, 350);
    }
    
    static testmethod void underConstructionTest(){
    	Account multiSiteAccount = DataFactoryAccountExt.createMultiSiteAccount(10000000);
        multiSiteAccount.Under_Construction__c = true;
        
        insert multiSiteAccount;
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, 3550);
    }
    
    static testmethod void zeroProgramServiceRevenueTest(){
    	Account multiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount(0);
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, 0);
    }
    
    static testmethod void threeHundredEightDuesPriceTest(){
    	Decimal desiredDuesPrice = 380;
    	Decimal threeHundredEightDuesProgramServiceRevenue = desiredDuesPrice / 0.0004;
    	
    	Account multiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount(threeHundredEightDuesProgramServiceRevenue);
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, desiredDuesPrice);
    }
    
    static testmethod void fiveMillionProgramServiceRevenueTest(){
    	Account multiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount(5000000);
        
        Decimal expectedProviderDuesPricing = (multiSiteAccount.Multi_Site_Program_Service_Revenue__c * 0.00035) + 50;
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, expectedProviderDuesPricing);
    }
    
    static testmethod void fifteenMillionProgramServiceRevenueTest(){
    	Account multiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount(15000000);
        
        Decimal expectedProviderDuesPricing = (multiSiteAccount.Multi_Site_Program_Service_Revenue__c * 0.00030) + 550;
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, expectedProviderDuesPricing);
    }
    
    static testmethod void minDuesPriceTest(){
    	Decimal minDuesProgramServiceRevenue = 200 / 0.0004;
    	
    	Account multiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount(minDuesProgramServiceRevenue);
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, 350);
    }

    static testmethod void noMaxDuesPriceTest(){
    	Decimal desiredDuesPriceLevel = 10000;
    	Decimal multiSiteProgramServiceRevenue = desiredDuesPriceLevel / 0.0003;
    	
    	Account multiSiteAccount = DataFactoryAccountExt.insertMultiSiteAccount(multiSiteProgramServiceRevenue);
        
        Decimal expectedDuesPrice = desiredDuesPriceLevel + 550;
        
        TestMembershipPricingUtil.assertProviderPricing(multiSiteAccount, expectedDuesPrice);
    }
}